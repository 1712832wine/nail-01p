<main class="main">
    <div class="page-wrapper">
        {{-- CAROUSEL --}}
        <div class="section-slider-wrap">
            <section class="section-slider">
                <div class="slider-width-height"
                    style="display: inline-block; width: 100%; height: 1px; overflow: hidden;">
                    <div class="fixed" style="width: 100%"></div>
                    <img src="{{ asset('storage') }}/photos/{{ $imgs_carousel[0]['url'] }}"
                        style="width: 100%; height: auto" alt="" />
                    <!-- END USE FOR CALCULATOR START WIDTH HEIGHT -->
                </div>

                <div class="slider-pro" id="my-slider" style="display: none">
                    <div class="sp-slides">
                        @foreach ($imgs_carousel as $img)
                            <div class="sp-slide">
                                <a href="{{ route('home') }}" target="_self" title="slide7.png">
                                    <img class="sp-image" src="{{ asset('storage') }}/photos/{{ $img['url'] }}"
                                        alt="slider.png" />
                                </a>
                            </div>
                        @endforeach
                    </div>
                </div>

                <div class="slider-pro" id="my-slider-fixed-height" style="display: none">
                    @foreach ($imgs_carousel as $img)
                        <div class="sp-slide">
                            <a href="{{ route('home') }}" target="_self" title="slide7.png">
                                <img class="sp-image" src="{{ asset('storage') }}/photos/{{ $img['url'] }}"
                                    alt="slider.png" />
                            </a>
                        </div>
                    @endforeach
                </div>
            </section>
        </div>
        {{-- ABOUT --}}
        <section class="section-about about-us">
            <div class="left-image"></div>
            <div class="container">
                @if ($about_article)
                    <div class="row clearfix">
                        <div class="content-column col-md-6 col-sm-12 col-xs-12">
                            <div class="inner-column">
                                <h3 class="title">About Us</h3>
                                <h2 itemprop="name">{{ $about_article['title'] }}</h2>
                                {!! $about_article['content'] !!}
                                <a href="{{ route('about') }}" class="theme-btn btn-style-two" title="More Info">
                                    View More<i class="fa fa-angle-double-right"></i>
                                </a>
                            </div>
                        </div>
                        <div class="image-column col-md-6 col-sm-12 col-xs-12">
                            <div class="inner-column">
                                <div class="image">
                                    <img src="{{ asset('storage') }}/photos/{{ json_decode($about_article['image'])[0] }}"
                                        caption="false" />
                                </div>
                            </div>
                        </div>
                    </div>
                @endif
            </div>
        </section>
        {{-- SERVICES --}}
        <section class="section-service services-section">
            <div class="container">
                <div class="sec-title text-center">
                    <span class="title">Blooming Nails & Spa</span>
                    <h2 itemprop="name">Our Services</h2>
                    <div class="separator">
                        <span class="flaticon-flower"></span>
                    </div>
                </div>
                <div>
                    <div class="service-items">
                        <div class="row">
                            @foreach ($services as $service)
                                <div class="break-row break-row-1"></div>
                                <div class="col-lg-3 col-md-4 col-sm-6">
                                    <a itemprop="url"
                                        href="{{ route('services-detail', ['slug' => strtolower(trim(preg_replace('/[^A-Za-z0-9-]+/', '-', $service['name']))), 'service_id' => $service['id']]) }}"
                                        title="{{ $service['name'] }}">
                                        <div class="service-item">
                                            <div class="image-box">
                                                <div class="image service-i-img">
                                                    <img itemprop="image"
                                                        src="{{ asset('storage') }}/photos/{{ json_decode($service['images'])[0] }}"
                                                        class="imgrps" alt="{{ $service['name'] }}" />
                                                </div>
                                            </div>
                                            <h3 class="service-i-name" itemprop="text">
                                                {{ $service['name'] }}
                                            </h3>
                                            <div class="service-i-description" itemprop="description">
                                                {{ $service['description'] }}
                                            </div>
                                        </div>
                                    </a>
                                </div>
                            @endforeach
                        </div>
                    </div>
                </div>
            </div>
        </section>
        {{-- COUNTER --}}
        <section class="section-countdown fun-fact-section">
            <div class="container">
                <div class="row clearfix">
                    @foreach (json_decode($counter['features']) as $index => $item)
                        <div class="counter-column col-md-3 col-sm-6 col-xs-6">
                            <div class="count-box">
                                @php
                                    $icons = explode(',', json_decode($counter['extras'])[0]->desc);
                                @endphp
                                <span class="icon {{ $icons[$index] }}"></span>
                                <span class="count-text" data-speed="3000" data-stop="{{ $item->desc }}">0</span>
                                <h4 class="counter-title">{{ $item->name }}</h4>
                            </div>
                        </div>
                    @endforeach
                </div>
            </div>
        </section>
        {{-- BUSINESS HOUR --}}
        <section class="section-openhours faq-section">
            <div class="container">
                <div class="row clearfix">
                    <div class="accordion-column col-md-6 col-sm-12 col-xs-12">
                        <div class="inner-column">
                            <div class="sec-title xs-text-center">
                                <span class="title">Blooming Nails & Spa</span>
                                <h2>{{ $home_article['name'] }}</h2>
                                <div class="separator">
                                    <span class="flaticon-flower"></span>
                                </div>
                            </div>
                            <div>
                                <div class="foh-wrap">
                                    @foreach (json_decode($home_article['features']) as $item)
                                        <div class="foh-row" itemprop="openingHours" content="{{ $item->desc }}">
                                            <span class="foh-date">{{ $item->name }}</span>
                                            <span class="foh-time">{{ $item->desc }}</span>
                                        </div>
                                    @endforeach
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="image-column col-md-6 col-sm-12 col-xs-12 xs-none">
                        <div class="inner-column">
                            <div class="image-box">
                                <img src="{{ asset('frontend') }}/uploads/fnail0f0l5bhq/filemanager/dreamstime_s_137458790C.jpg"
                                    caption="false" />
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        {{-- TO SERVICES --}}
        <section class="section-slogan call-to-action">
            <div class="container">
                <div class="row clearfix">
                    <div class="title-column col-md-9 col-sm-12 col-xs-12">
                        <div class="inner-column">
                            <figure>
                                <img src="{{ asset('storage') }}/photos/{{ json_decode($home_article2['image'])[0] }}"
                                    alt="stones.png" itemprop="image" caption="false" />
                            </figure>
                            {!! $home_article2['content'] !!}
                        </div>
                    </div>
                    <div class="btn-column col-md-3 col-sm-12 col-xs-12">
                        <div class="inner-column">
                            <a href="{{ route('services') }}" class="theme-btn btn-style-two">Our Services <i
                                    class="fa fa-angle-double-right"></i></a>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        {{-- GALLERY --}}
        <section class="section-gallery gallery-section">
            <div class="container">
                <div class="mixitup-gallery">
                    <div class="inner-container clearfix">
                        <div class="sec-title text-center">
                            <h2 itemprop="name">Our Photos</h2>
                        </div>
                        <div>
                            <div class="filters text-center clearfix">

                                <ul class="filter-tabs filter-btns clearfix" id="category_tab">
                                    @foreach ($galleries as $index => $gallery)
                                        <li class="tab filter @if ($index===0) active @endif" data-role=" button"
                                            data-filter="{{ $gallery['name'] }}" data-id="{{ $gallery['id'] }}">
                                            {{ $gallery['name'] }}
                                        </li>
                                    @endforeach
                                </ul>
                            </div>
                            <div class="clearfix m-gallery-content" id="gallery_content" data-isNewTemplate="true">
                                <div class="clearfix m-gallery-listing listing"></div>
                                <div class="clearfix m-gallery-paging paging"></div>
                            </div>
                            {{-- <div class="filter-list row clearfix">
                                @foreach ($galleries_lists[$item_id] as $image)
                                    <div class="gallery-block mix all categoryID_4 col-xs-6 col-sm-4 col-md-3">
                                        <div class="pointer m-magnific-popup" data-group="gallery-4"
                                            title="Nails Design"
                                            href="{{ asset('storage') }}/photos/{{ $image['url'] }}">
                                            <div class="image-box">
                                                <figure>
                                                    <div class="m-image-bg" style="
                                                        background-image: url('{{ asset('storage') }}/photos/{{ $image['url'] }}');
                                                    ">
                                                        <img itemprop="image"
                                                            src="{{ asset('storage') }}/photos/{{ $image['url'] }}"
                                                            alt="Nails Design" />
                                                    </div>
                                                </figure>
                                                <div class="content-box">
                                                    <div><span class="fa fa-arrows-alt"></span></div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                @endforeach
                            </div> --}}
                        </div>
                    </div>
                </div>
            </div>
        </section>
        {{-- CUSTOMERS --}}
        <div class="section-testimonial">
            <div class="container">
                <div class="sec-title text-center">
                    <h2>Our Customers</h2>
                </div>
                <div class="testimonial-content">
                    <div class="items-testimonial testimonial-carousel owl-carousel">
                        @foreach ($home_carousel as $item)
                            <div class="item">
                                <div class="single-testimonial-item">
                                    <div class="testimonial-content">
                                        {!! $item['content'] !!}
                                        <h4 class="name" itemprop="name">{{ $item['title'] }}</h4>
                                    </div>
                                    <div class="testimonial-img">
                                        @if (count(json_decode($item['image'])) > 0)
                                            <img itemprop="image"
                                                src="{{ asset('storage') }}/photos/{{ json_decode($item['image'])[0] }}"
                                                alt="testimonial-1.jpg" caption="false" />
                                        @endif
                                    </div>
                                </div>
                            </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
        <section id="embed_Home_Popup">
            <style>
                .popup-slider-container {
                    overflow: hidden;
                    max-width: 100vw;
                    margin: auto;
                    padding: 0;
                }

                .popup-carousel .item-carousel {
                    padding: 0;
                }

                .popup-carousel .owl-nav {
                    text-align: center;
                }

                .popup-carousel .owl-prev,
                .popup-carousel .owl-next {
                    display: inline-block;
                    border: 1px solid #f5f5f5;
                    background-color: #fff;
                    color: #ccc;
                    margin: 7px 7px 15px;
                    padding: 0;
                    width: 45px;
                    height: 30px;
                    line-height: 1;
                    font-size: 30px;
                }

                .white-popup {
                    padding: 15px;
                }

            </style>

            <!-- Infinity Slider Pop-up-->
            <section id="popup_infinity_embed_9v5er87j">
                <div class="popup_infinity_item white-popup popup-style mfp-hide">
                    <div class="popup-slider-container">
                        <ul class="popup-carousel owl-carousel owl-theme">
                            @foreach ($list_coupons as $item)
                                <li class="item-carousel">
                                    <div class="item-image">
                                        <a href="{{ route('coupons') }}" title="Popup Slider coupons">
                                            <img src="{{ asset('storage') }}/photos/{{ $item['url'] }}"
                                                alt="Coupon 1" caption="false" />
                                        </a>
                                    </div>
                                </li>
                            @endforeach
                            <!--Need to add more a slide, then duplicate item contains class ".item-carousel" above-->
                        </ul>
                    </div>
                </div>
                <div class="popup_infinity_item white-popup popup-style mfp-hide">
                    <div>
                        <a href="{{ route('coupons') }}" title="Popup Slider coupons">
                            <img src="{{ asset('storage') }}/photos/{{ $img_popup2['url'] }}" alt="PopUp 2"
                                caption="false" />
                        </a>
                    </div>
                </div>
                <!--Need to add more a pop-up, then duplicate item contains class ".popup_infinity_item" above-->
            </section>
            <script>
                $(document).ready(function() {
                    /*Definite Variable*/
                    const popup_infinity_id_embed_9v5er87j =
                        "#popup_infinity_embed_9v5er87j";
                    let popup_infinity_cnt_embed_9v5er87j = 0;
                    let popup_infinity_list_embed_9v5er87j = [];
                    /*Definite Function*/
                    function open_popup_infinity_embed_9v5er87j() {
                        /*Deny Loop & Decrease Count*/
                        if (
                            popup_infinity_cnt_embed_9v5er87j <= 0
                        ) {
                            return false;
                        }
                        popup_infinity_cnt_embed_9v5er87j--;
                        /*Get && Open Popup*/
                        let popupList =
                            popup_infinity_list_embed_9v5er87j;
                        let popupID = "";
                        for (let i in popupList) {
                            popupID = popupList[i];
                            popupList.splice(i, 1);
                            if ($(popupID).length > 0) {
                                break;
                            } else {
                                console.log("Popup " + popupID + " Missed");
                            }
                        }
                        if (!popupID) {
                            return false;
                        }
                        $.magnificPopup.close();
                        $.magnificPopup.open({
                            items: {
                                src: popupID,
                                type: "inline"
                            },
                            callbacks: {
                                open: function() {
                                    $(popupID)
                                        .css("width", "100%")
                                        .find(".popup-carousel")
                                        .owlCarousel({
                                            margin: 0,
                                            loop: true,
                                            smartSpeed: 750,
                                            autoplay: true,
                                            autoplayTimeout: 2750,
                                            nav: true,
                                            navText: [
                                                '<i class="fa fa-caret-left"></i>',
                                                '<i class="fa fa-caret-right"></i>',
                                            ],
                                            dots: false,
                                            items: 1,
                                        });
                                },
                                afterClose: function() {
                                    open_popup_infinity_embed_9v5er87j();
                                },
                            },
                        });
                    }

                    function init_popup_infinity_embed_9v5er87j(popup_infinity_id) {
                        let popup_infinity_Obj = $(
                            popup_infinity_id + " .popup_infinity_item"
                        );
                        let item_cnt = 0;
                        popup_infinity_Obj.each(function() {
                            item_cnt++;
                            $(this).attr(
                                "id",
                                "popup_infinity_item_embed_9v5er87j_" + item_cnt
                            );
                            popup_infinity_list_embed_9v5er87j.push(
                                "#popup_infinity_item_embed_9v5er87j_" + item_cnt
                            );
                        });
                        popup_infinity_cnt_embed_9v5er87j = item_cnt;
                        /*Call Open Popup*/
                        open_popup_infinity_embed_9v5er87j();
                    }
                    /*Call Init*/
                    init_popup_infinity_embed_9v5er87j(
                        popup_infinity_id_embed_9v5er87j
                    );
                });

            </script>
        </section>
    </div>
</main>
