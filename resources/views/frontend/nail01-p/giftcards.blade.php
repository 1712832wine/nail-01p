<main class="main">
    <section class="page-title">
        <div class="container">
            <div class="inner-container clearfix">
                <h2 itemprop="name">Giftcards</h2>
                <ul class="bread-crumb clearfix">
                    <li><a href="/" itemprop="url" title="Home">Home</a></li>
                    <li>Giftcards</li>
                </ul>
            </div>
        </div>
    </section>
    <section class="page-content section-padding our-gift">
        <div class="left-image">
            <img itemprop="image" src="{{ asset('frontend') }}/themes/fnail01p/assets/images/side-img-left.png"
                alt="side-img-left.png" />
        </div>
        <div class="auto-container">
            <h3 class="title">Blooming Nails & Spa</h3>
            <h2 itemprop="name">Our Giftcards</h2>
            <div>
                <div class="row">
                    <div class="col-md-8">
                        {{-- CARD --}}
                        <div class="block-giftcards clearfix" id="boxPaymentItems">
                            <div class="title_gift">
                                <i class="fa fa-location-arrow" aria-hidden="true"></i>
                                Please choose a design and add input e-gift information:
                            </div>
                            <div class="row">
                                @foreach ($giftcards as $index => $item)
                                    @if (count(json_decode($item['images'])) > 0)
                                        <div class="col-6 col-xs-6 col-md-3 pd-fix">
                                            <div data-image="{{ asset('storage') }}/photos/{{ json_decode($item['images'])[0] }}"
                                                wire:click="changeCard({{ $item['id'] }})"
                                                class="box_img_giftcard pointer paymentItem @if ($selected===$item['id']) active @endif">
                                                <img itemprop="image" class="img-responsive"
                                                    src="{{ asset('storage') }}/photos/{{ json_decode($item['images'])[0] }}"
                                                    alt="" />
                                                <div class="circle_check">
                                                </div>
                                                <div class="price-promotion">15% Off</div>
                                                <div class="img_popup">
                                                    <img src="{{ asset('storage') }}/photos/{{ json_decode($item['images'])[0] }}"
                                                        alt="" />
                                                </div>
                                            </div>
                                        </div>
                                    @endif
                                @endforeach
                            </div>
                        </div>

                        <div class="row">
                            {{-- INFOR --}}
                            <div class="col-md-6">
                                <div class="block-Payment">
                                    <form id="formPayment" name="formPayment" wire:submit.prevent="checkPaypal()"
                                        class="form-horizontal">
                                        <div class="box-payer clearfix">
                                            <div class="title_gift">
                                                <i class="fa fa-id-card" aria-hidden="true"></i>
                                                Payer Information
                                            </div>
                                            <div class="row gift-price-quantity">
                                                <div class="col-6 col-xs-6 gift-price">
                                                    <div class="group-select">
                                                        <label>Amount (required)</label>
                                                        <div class="relative w100">
                                                            <input class="form-control" type="text" title="Amount"
                                                                id="custom_price" name="custom_price" data-id="0"
                                                                data-price_custom="0" data-price_min="0"
                                                                data-price_max="50" value="30"
                                                                onkeypress="return check_enter_number(event, this);"
                                                                data-validation="[NOTEMPTY]"
                                                                data-validation-message="Please enter the amount"
                                                                autocomplete="off" placeholder=""  />
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-6 col-xs-6 gift-quantity">
                                                    <div class="group-select">
                                                        <label>Quantity (required)</label>
                                                        <div class="relative w100">
                                                            <input class="form-control" type="number"
                                                                title="custom quantity" id="custom_quantity"
                                                                wire:model="quantity" name="custom_quantity" data-id="0"
                                                                min="1" value="1"
                                                                onkeypress="return check_enter_number(event,this);"
                                                                data-validation="[NOTEMPTY]"
                                                                data-validation-message="Please enter the quantity"
                                                                autocomplete="off" placeholder="" />
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-12 col-xs-12 gift-price-note" id="custom_price_note"
                                                    style="display: none">
                                                </div>
                                            </div>
                                            <div class="group-select gift-ship-name">
                                                <label>Full name (required)</label>
                                                <div class="relative w100">
                                                    <input type="text" class="form-control" maxlength="70"
                                                        wire:model="from.name" title="Full name" name="ship_full_name"
                                                        id="ship_full_name" placeholder="Full name" value=""
                                                        data-validation="[NOTEMPTY]"
                                                        data-validation-message="Please enter your full name!" required
                                                        autocomplete="off" />
                                                    @error('from.name') <span class="error">{{ $message }}</span>
                                                    @enderror
                                                </div>
                                            </div>
                                            <div class="group-select gift-ship-email">
                                                <label>Email (required)</label>
                                                <div class="relative w100">
                                                    <input type="text" class="form-control" name="ship_email"
                                                        wire:model.defer="from.email" title="Email" id="ship_email"
                                                        placeholder="Email" value="" data-validation="[EMAIL]"
                                                        data-validation-message="Please enter your email!" required
                                                        autocomplete="off" />
                                                    @error('from.email') <span
                                                            class="error text-danger">{{ $message }}</span>
                                                    @enderror
                                                </div>
                                            </div>
                                            <div class="group-select gift-ship-phone">
                                                <label>Phone</label>
                                                <div class="relative w100">
                                                    <input type="text" class="form-control inputPhone"
                                                        wire:model.defer="from.phone" id="ship_phone" title="Phone"
                                                        placeholder="Phone" value="" autocomplete="off" />
                                                </div>
                                            </div>
                                        </div>

                                        <div class="
                                            group-select group-checkbox
                                            clearfix
                                            gift-send-to-friend
                                                 ">
                                            <input wire:model="send_to_relative" title="send to friend" type="checkbox"
                                                name="send_to_friend" id="send_to_friend" value="1" class="pointer"
                                                style="margin-left: 0" />
                                            <label class="pointer" for="send_to_friend">Send gift card to
                                                relatives</label>
                                        </div>

                                        @if ($send_to_relative)
                                            <div class="box_recipient box-recipient clearfix" id="boxRecipient">
                                                <div class="title_gift">
                                                    <i class="fa fa-users" aria-hidden="true"></i>
                                                    Recipient information
                                                </div>
                                                <div class="group-select gift-recipient-name">
                                                    <label>Recipient's Name</label>
                                                    <div class="relative w100">
                                                        <input wire:model="to.name" type="text" class="form-control"
                                                            maxlength="70" title="Recipient's Name"
                                                            name="recipient_name" id="recipient_name"
                                                            placeholder="Recipient name" value="" autocomplete="off" />
                                                    </div>
                                                </div>
                                                <div class="group-select gift-recipient-email">
                                                    <label>Recipient's Email</label>
                                                    <div class="relative w100">
                                                        <input type="text" wire:model.defer="to.email"
                                                            class="form-control" name="recipient_email"
                                                            id="recipient_email" title="Recipient" placeholder="Email"
                                                            value="" autocomplete="off" />
                                                    </div>
                                                </div>
                                                <div class="group-select gift-recipient-message">
                                                    <label>Message</label>
                                                    <div class="relative w100">
                                                        <textarea class="form-control" name="recipient_message"
                                                            wire:model="to.message" title="Message"
                                                            id="recipient_message"
                                                            placeholder="Message send to Recipient"
                                                            onkeyup="change_content(this, '#preview_message');"
                                                            maxlength="300"></textarea>
                                                    </div>
                                                </div>
                                            </div>
                                        @endif


                                        <button type="submit" title="Checkout with Paypal"
                                            class="btn btn-main btn-style-four btn_payment">
                                            <span>Checkout with Paypal</span>
                                        </button>

                                        <!-- Tpl note_payment -->
                                        <p>
                                            By clicking 'Checkout with Paypal' you agree to our
                                            privacy policy and terms of service. You also agree
                                            to receive periodic email updates, discounts, and
                                            special offers.
                                        </p>
                                        <!-- Hidden data -->
                                        <input type="hidden" name="type_page" value="1" />
                                    </form>
                                </div>
                            </div>
                            {{-- YOUR CART --}}
                            <div class="col-md-6">
                                <div class="box-cart clearfix" id="boxCart">
                                    <div class="title_gift">
                                        <i class="fa fa-cart-plus" aria-hidden="true"></i>
                                        Your cart
                                    </div>
                                    <figure class="mybox-order">
                                        <ul>
                                            <li class="cart-item">
                                                <div class="item-info">
                                                    <div class="item-image" id="cart_image">
                                                        @if ($selected_img)
                                                            <img
                                                                src="{{ asset('storage') }}/photos/{{ $selected_img }}" />
                                                        @else
                                                            <img src="{{ asset('frontend') }}/public/library/global/no-photo.jpg"
                                                                alt="No photo" />
                                                        @endif

                                                    </div>
                                                    <div>
                                                        <span class="item-quantity"
                                                            id="cart_quantity">{{ $quantity }}</span>
                                                        <span> x </span>
                                                        <span class="item-name"
                                                            id="cart_name">{{ $selected_name }}</span>


                                                    </div>
                                                </div>
                                            </li>
                                            <li class="cart-subtotal">
                                                <label>Subtotal</label>

                                                @if ($subtotal !== 'N/A')
                                                    <span class="money pull-right"
                                                        id="cart_subtotal">${{ $subtotal }}</span>
                                                @else
                                                    <span class="money pull-right"
                                                        id="cart_subtotal">{{ $subtotal }}</span>
                                                @endif



                                            </li>
                                            <li class="cart-discount">
                                                <label>Discount/Bonus</label>
                                                <span class="money pull-right"
                                                    id="cart_discount">{{ $discount }}</span>
                                            </li>
                                            <li class="cart-vat">
                                                <label>Tax/Fee</label>
                                                <span class="money pull-right" id="cart_tax">{{ $tax }}</span>
                                            </li>
                                            <li class="cart-vat">
                                                <label>Total</label>
                                                @if ($total !== 'N/A')
                                                    <span class="money pull-right"
                                                        id="cart_total">${{ $total }}</span>
                                                @else
                                                    <span class="money pull-right"
                                                        id="cart_total">{{ $total }}</span>
                                                @endif
                                            </li>
                                        </ul>
                                    </figure>

                                    <!-- Tpl note_shipping -->
                                    <figure class="note">
                                        <p itemprop="name" class="sanb" style="font-size: 16px; font-weight: bold">
                                            When will my order be shipped?
                                        </p>
                                        <p itemprop="description">
                                            Your e-Gift card will be sent immediately after we
                                            receive your order
                                        </p>
                                    </figure>
                                </div>
                            </div>
                        </div>
                    </div>

                    {{-- PREVIEW --}}
                    <div class="col-md-4">
                        <div class="block-preview" id="boxPreview">
                            <div class="title_gift">
                                <i class="fa fa-television" aria-hidden="true"></i>
                                Preview
                            </div>
                            <div class="box_preview">
                                <ul class="information">
                                    <li>
                                        <span class="center">Blooming Nails & Spa</span>
                                    </li>
                                    <li>
                                        <span class="left">Address:</span>
                                        <span class="right">11011 Richmond Ave,<br />
                                            Ste 250 <br />
                                            Houston, TX 77042</span>
                                    </li>
                                    <li>
                                        <span class="left">Phone:</span>
                                        <span class="right">651-362-0535‬</span>
                                    </li>
                                </ul>
                                <div class="preview_image" id="preview_image">
                                    @if ($selected_img)
                                        <img src="{{ asset('storage') }}/photos/{{ $selected_img }}" />
                                    @else
                                        <img src="{{ asset('frontend') }}/public/library/global/no-photo.jpg"
                                            alt="No photo" />
                                    @endif


                                </div>
                                <div class="info_send">
                                    <ul class="information">
                                        <li>
                                            <div class="row">
                                                <div class="col-6 col-xs-6">
                                                    <span class="left">Amount:</span>
                                                    <span class="right preview_amount"
                                                        id="preview_amount">${{ $amount }}</span>
                                                </div>
                                                <div class="col-6 col-xs-6">
                                                    <span class="left">Quantity:</span>
                                                    <span class="right preview_quantity"
                                                        id="preview_quantity">{{ $quantity }}</span>
                                                </div>
                                            </div>
                                        </li>
                                        <li>
                                            <span class="left">From:</span>
                                            <span class="right preview_from"
                                                id="preview_from">{{ $from['name'] }}</span>
                                        </li>
                                        <li>
                                            <span class="left">To:</span>
                                            <span class="right preview_to" id="preview_to">{{ $to['name'] }}</span>
                                        </li>
                                        <li>
                                            <span class="left">Message:</span>
                                            <span class="right preview_message"
                                                id="preview_message">{{ $to['message'] }}</span>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</main>
