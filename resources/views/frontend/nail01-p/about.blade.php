<main class="main">
    <!-- Tpl main about -->
    <section class="page-title">
        <div class="container">
            <div class="inner-container clearfix">
                <h2 itemprop="name">About Us</h2>
                <ul class="bread-crumb clearfix">
                    <li><a href="{{ route('home') }}" itemprop="url" title="Home">Home</a></li>
                    <li>About Us</li>
                </ul>
            </div>
        </div>
    </section>
    <section class="section-padding about-us">
        <div class="left-image"><img src="{{ asset('frontend') }}/themes/fnail01p/assets/images/side-img-1.png"
                alt="" /></div>
        <div class="auto-container">
            @foreach ($articles as $item)
                <div class="row clearfix">
                    <div class="content-column col-md-6 col-sm-12 col-xs-12">
                        <div class="inner-column">
                            <h3 class="title">About Us</h3>
                            <h2 itemprop="name">{{ $item['title'] }}</h2>
                            {!! $item['content'] !!}
                        </div>
                    </div>
                    @if (count(json_decode($item['image'])) > 0)
                        <div class="image-column col-md-6 col-sm-12 col-xs-12">
                            <div class="inner-column">
                                <div class="image"><img itemprop="image"
                                        src="{{ asset('storage') }}/photos/{{ json_decode($item['image'])[0] }}"
                                        alt="image-1.png" caption="false" /></div>
                            </div>
                        </div>
                    @endif

                </div>
            @endforeach

        </div>
    </section>
</main>
