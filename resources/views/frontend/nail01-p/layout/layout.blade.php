<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" dir="ltr" lang="{{ str_replace('_', '-', app()->getLocale()) }}"
    xmlns:og="http://ogp.me/ns#" xmlns:fb="http://www.facebook.com/2008/fbml" itemscope
    itemtype="https://schema.org/NailSalon">

<head>
    <!-- Language -->
    <meta http-equiv="content-language" content="en" />
    <link rel="alternate" href="http://fnail01p.fbm.company" hreflang="x-default" />
    <link rel="alternate" href="http://fnail01p.fbm.company" hreflang="en-US" />

    <!-- Meta limit -->

    <!-- Meta Tags -->
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width,initial-scale=1.0" />
    <meta http-equiv="content-type" content="text/html; charset=UTF-8" />
    <meta name="description" content="" />
    <meta name="keywords" content="" />
    <meta name="author" content="" />

    <!-- OG -->
    <meta property="og:title" content="" />
    <meta property="og:description" content="" />
    <meta property="og:image" content="" />
    <meta property="og:url" content="http://fnail01p.fbm.company/" />
    <meta property="og:type" content="" />
    <meta property="og:site_name" content="Blooming Nails & Spa" />

    <!-- Dublin Core -->
    <link rel="schema.DC" href="http://purl.org/dc/elements/1.1/" />
    <meta name="DC.title" content="" />
    <meta name="DC.identifier" content="http://fnail01p.fbm.company/" />
    <meta name="DC.description" content="" />
    <meta name="DC.subject" content="" />
    <meta name="DC.language" scheme="UTF-8" content="en-us" />
    <meta itemprop="priceRange" name="priceRange" content="" />
    <!-- GEO meta -->
    <meta name="geo.region" content="" />
    <meta name="geo.placename" content="" />
    <meta name="geo.position" content="" />
    <meta name="ICBM" content="" />
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <!-- Page Title -->
    <title>Blooming Nails & Spa</title>
    <base href="/themes/fnail01p/assets/" />

    <!-- canonical -->
    <link rel="canonical" href="http://fnail01p.fbm.company" />

    <!-- Favicons -->
    <link rel="icon" href="http://fnail01p.fbm.company/uploads/fnail0f0l5bhq/attach/1603679639_favicon_fvc.png"
        type="image/png" />
    <link rel="shortcut icon" href="http://fnail01p.fbm.company/uploads/fnail0f0l5bhq/attach/1603679639_favicon_fvc.png"
        type="image/png" />
    <link rel="apple-touch-icon"
        href="http://fnail01p.fbm.company/uploads/fnail0f0l5bhq/attach/1603679639_favicon_fvc.png" type="image/png" />

    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Lato:300,400,700,900" rel="stylesheet" />
    <link
        href="https://fonts.googleapis.com/css?family=Montserrat:300,300i,400,400i,500,500i,600,600i,700,700i,800,800i,900,900i|Tangerine:400,700"
        rel="stylesheet" />

    <!-- CSS -->
    <link rel="stylesheet" type="text/css"
        href="{{ asset('frontend') }}/public/library/jquery-ui/jquery-ui.min.css" />
    <link rel="stylesheet" type="text/css"
        href="{{ asset('frontend') }}/public/library/bootstrap/v3.3.7/bootstrap.min.css" />
    <link rel="stylesheet" type="text/css"
        href="{{ asset('
frontend') }}/public/library/bootstrap-datetimepicker/bootstrap-datetimepicker.min.css" />
    <link rel="stylesheet" type="text/css" href="{{ asset('frontend') }}/public/library/validation/validation.css" />
    <link rel="stylesheet" type="text/css" href="{{ asset('frontend') }}/public/library/animate/animate.css" />
    <link rel="stylesheet" type="text/css" href="{{ asset('frontend') }}/public/library/hover/hover.min.css" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.css"
        integrity="sha512-5A8nwdMOWrSz20fDsjczgUidUBR8liPYU+WymTZP1lmY9G6Oc7HlZv156XqnsgNUzTyMefFTcsFH/tnJE/+xBg=="
        crossorigin="anonymous" referrerpolicy="no-referrer" />
    <link rel="stylesheet" type="text/css"
        href='{{ asset('frontend') }}/public/library/font-flaticon/css/font-flaticon.css'>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/flat-ui/2.3.0/css/flat-ui.min.css"
        integrity="sha512-6f7HT84a/AplPkpSRSKWqbseRTG4aRrhadjZezYQ0oVk/B+nm/US5KzQkyyOyh0Mn9cyDdChRdS9qaxJRHayww=="
        crossorigin="anonymous" referrerpolicy="no-referrer" />
    <link rel="stylesheet" type="text/css" href="{{ asset('frontend') }}/public/library/meanmenu/meanmenu.css" />
    <link rel="stylesheet" type="text/css"
        href="{{ asset('frontend') }}/public/library/translatemenu/translatemenu.css" />
    <link rel="stylesheet" type="text/css"
        href="{{ asset('frontend') }}/public/library/slider-pro/slider-pro.min.css" />
    <link rel="stylesheet" type="text/css"
        href="{{ asset('frontend') }}/public/library/slider-slick/slider-slick.css" />
    <link rel="stylesheet" type="text/css"
        href="{{ asset('frontend') }}/public/library/owl-carousel/v2.2.1/owl.carousel.min.css" />
    <link rel="stylesheet" type="text/css"
        href="{{ asset('frontend') }}/public/library/magnific-popup/magnific-popup.css" />
    <link rel="stylesheet" type="text/css"
        href="{{ asset('frontend') }}/public/library/instagram/instagram-feed.css" />
    <link rel="stylesheet" type="text/css" href="{{ asset('frontend') }}/public/library/signature/signature.css" />
    <link rel="stylesheet" type="text/css" href="{{ asset('frontend') }}/public/library/pnotify/pnotify.css" />
    <link rel="stylesheet" type="text/css" href="{{ asset('frontend') }}/public/library/popover/popover.css" />

    <link rel="stylesheet" type="text/css"
        href="{{ asset('frontend') }}/themes/fnail01p/assets/css/ie10-viewport-bug-workaround.css" />
    <link rel="stylesheet" type="text/css"
        href="{{ asset('frontend') }}/themes/fnail01p/assets/css/jquery.timepicker.min.css" />
    <link rel="stylesheet" type="text/css"
        href="{{ asset('frontend') }}/themes/fnail01p/assets/css/dropdown-submenu.css" />
    <link rel="stylesheet" type="text/css"
        href="{{ asset('frontend') }}/themes/fnail01p/assets/css/navigation.css" />
    <link rel="stylesheet" type="text/css" href="{{ asset('frontend') }}/themes/fnail01p/assets/css/style.css" />
    <link rel="stylesheet" type="text/css"
        href="{{ asset('frontend') }}/themes/fnail01p/assets/css/responsive.css" />
    <link rel="stylesheet" type="text/css" href="{{ asset('frontend') }}/public/library/global/css/global.css" />
    <link rel="stylesheet" type="text/css" href="{{ asset('frontend') }}/public/library/global/css/location.css" />
    <link rel="stylesheet" type="text/css" href="{{ asset('frontend') }}/public/library/global/css/news.css" />
    <link rel="stylesheet" type="text/css"
        href="{{ asset('frontend') }}/public/library/global/css/giftcards.css?v=1.0" />
    <link rel="stylesheet" type="text/css"
        href="{{ asset('frontend') }}/public/library/global/css/cart-payment.css?v=1.0" />
    <link rel="stylesheet" type="text/css" href="{{ asset('frontend') }}/public/library/global/css/consent.css" />
    <link rel="stylesheet" type="text/css"
        href="{{ asset('frontend') }}/themes/fnail01p/assets/custom/css/custom.css?v=1.3" />
    <link rel="stylesheet" type="text/css"
        href="{{ asset('frontend') }}/themes/fnail01p/assets/custom/css/custom-2.css" />

    <!-- CSS EXTEND -->
    <style type="text/css">
        .section-countdown {
            background-attachment: scroll;
        }

        .main-menu .navigation>li {
            margin-left: 24px;
        }

        .contact-section .map-column .map-outer {
            padding-top: 150px;
        }

        .contact-section .map-column .info-box {
            max-width: 420px;
        }

        div#back-top {
            display: none !important;
        }

        ul.list-line.social img {
            width: 40px;
        }

        .main-footer .footer-bottom .copyright-text {
            padding: 20px 0px;
        }

        .about-us .content-column .inner-column {
            padding-top: 0px;
        }

        .about-us .title {
            font-size: 50px;
            margin-bottom: 0px;
        }

        .about-us p {
            text-align: justify;
        }

        .about-us .image-column .inner-column {
            margin: auto;
        }

        .sec-title .title {
            font-size: 50px;
        }

        .services-section {
            padding: 60px 0;
        }

        .about-us,
        .faq-section,
        .gallery-section,
        .subscribe-section {
            padding: 70px 0 70px;
        }

        .contact-section {
            padding-top: 120px;
        }

        .call-to-action p {
            text-align: justify;
        }

        .contact-section .heading {
            margin-bottom: 20px;
            margin-top: 20px;
        }

        .contact-section .heading h2 {
            position: relative;
            font-size: 28px;
        }

        .main-header .logo-box {
            padding: 10px 0px;
        }

        .main-menu .navigation>li {
            margin-left: 20px;
        }

        .faq-section .image-column .inner-column {
            padding-top: 0;
        }

        .section-service {
            background-size: auto;
            background-attachment: scroll;
        }

        .fun-fact-section:before {
            background: #282333;
        }

        @media (min-width: 1200px) {
            .service-items .col-lg-3.col-md-4.col-sm-6 {
                width: 33.333%;
            }
        }

        .main-header .header-lower {
            background-color: rgb(255 255 255 / 30%);
        }

        .page-content h2 {
            font-size: 24px;
            color: #8ab010;
        }

        @media only screen and (max-width: 768px) {
            .detail-price-name {
                font-size: 14px;
            }

            .detail-price-number {
                font-size: 15px;
            }
        }

        .detail-price-number {
            font-size: 16px;
        }

        .page-title {
            background-image: url('{{ asset('frontend') }}/uploads/fnail0f0l5bhq/filemanager/banner-page.jpg') !important;
        }

        .page-content h2 {
            font-size: 50px;
            font-family: "Tangerine", cursive;
        }

        h2.service-name {
            font-size: 40px;
        }

        .single-testimonial-item .testimonial-content {
            padding: 15px 100px;
        }

        .main-header .header-lower {
            background-color: rgb(255 255 255 / 83%);
        }

        .section-testimonial {
            position: relative;
            z-index: 1;
            padding: 120px 0 30px 0px;
            background-position: top center;
        }

        .section-testimonial {
            background-attachment: inherit;
        }

        .section-testimonial:before {
            background-color: rgb(255 255 255 / 0%);
        }

        .contact-section {
            padding: 70px 0 70px 0;
        }

    </style>

    <!-- JS -->
    <script type="text/javascript" src="{{ asset('frontend') }}/public/library/jquery/v2.2.4/jquery.min.js"></script>
    <script type="text/javascript" src="{{ asset('frontend') }}/public/library/jquery-ui/jquery-ui.min.js"></script>
    <script type="text/javascript" src="{{ asset('frontend') }}/public/library/jquery-ui/jquery-migrate.min.js">
    </script>
    <script type="text/javascript" src="{{ asset('frontend') }}/public/library/bootstrap/v3.3.7/bootstrap.min.js">
    </script>
    <script type="text/javascript"
        src="{{ asset('frontend') }}/public/library/waiting-modal-dialog/waiting-modal-dialog.js"></script>
    <script type="text/javascript" src="{{ asset('frontend') }}/public/library/moment/moment-with-locales.js">
    </script>
    <script type="text/javascript"
        src="{{ asset('frontend') }}/public/library/bootstrap-datetimepicker/bootstrap-datetimepicker.js"></script>
    <script type="text/javascript" src="{{ asset('frontend') }}/public/library/validation/validation.min.js">
    </script>
    <script type="text/javascript" src="{{ asset('frontend') }}/public/library/mask/mask.min.js"></script>
    <script type="text/javascript" src="{{ asset('frontend') }}/public/library/meanmenu/meanmenu.js"></script>
    <script type="text/javascript" src="{{ asset('frontend') }}/public/library/translatemenu/translatemenu.js?v=1.0">
    </script>
    <script type="text/javascript" src="{{ asset('frontend') }}/public/library/slider-pro/slider-pro.min.js">
    </script>
    <script type="text/javascript" src="{{ asset('frontend') }}/public/library/slider-slick/slider-slick.js">
    </script>
    <script type="text/javascript"
        src="{{ asset('frontend') }}/public/library/owl-carousel/v2.2.1/owl.carousel.min.js"></script>
    <script type="text/javascript" src="{{ asset('frontend') }}/public/library/magnific-popup/magnific-popup.min.js">
    </script>
    <script type="text/javascript" src="{{ asset('frontend') }}/public/library/instagram/instagram-feed.js"></script>
    <script type="text/javascript" src="{{ asset('frontend') }}/public/library/signature/signature.min.js"></script>
    <script type="text/javascript" src="{{ asset('frontend') }}/public/library/pnotify/pnotify.js"></script>
    <script type="text/javascript" src="{{ asset('frontend') }}/public/library/popover/popover.js"></script>
    <script type="text/javascript" src="{{ asset('frontend') }}/themes/fnail01p/assets/js/appear.js"></script>
    <script type="text/javascript" src="{{ asset('frontend') }}/themes/fnail01p/assets/js/mixitup.js"></script>
    <script type="text/javascript" src="{{ asset('frontend') }}/public/library/global/js/global.js?v=1.0"></script>
    <script type="text/javascript" src="{{ asset('frontend') }}/public/library/global/js/js_php.js"></script>

    <script type="text/javascript">
        let webForm = {
            required: "(required)",
            optional: "(optional)",
            any_person: "Any person",
            price: "Price",
            morning: "Morning",
            afternoon: "Afternoon",
            sunday: "Sunday",
            monday: "Monday",
            tuesday: "Tuesday",
            wednesday: "Wednesday",
            thursday: "Thursday",
            friday: "Friday",
            saturday: "Saturday",
            jan: "Jan",
            feb: "Feb",
            mar: "Mar",
            apr: "Apr",
            may: "May",
            jun: "Jun",
            jul: "Jul",
            aug: "Aug",
            sep: "Sep",
            oct: "Oct",
            nov: "Nov",
            dec: "Dec",
            contact_name: "Your name",
            contact_name_placeholder: "",
            contact_name_maxlength: "76",
            contact_email: "Your email address",
            contact_email_placeholder: "",
            contact_email_maxlength: "76",
            contact_phone: "Your phone",
            contact_phone_placeholder: "",
            contact_phone_maxlength: "16",
            contact_subject: "Your subject",
            contact_subject_placeholder: "",
            contact_subject_maxlength: "251",
            contact_message: "Your message",
            contact_message_placeholder: "",
            contact_message_maxlength: "501",
            contact_btn_send: "Send Us",
            contact_name_err: "Please enter your name",
            contact_email_err: "Please enter your email",
            contact_phone_err: "Please enter your phone",
            contact_subject_err: "Please enter your subject",
            contact_message_err: "Please enter your message",
            booking_name: "Your name",
            booking_name_placeholder: "",
            booking_name_maxlength: "76",
            booking_phone: "Your phone",
            booking_phone_placeholder: "",
            booking_phone_maxlength: "16",
            booking_email: "Your email",
            booking_email_placeholder: "",
            booking_email_maxlength: "76",
            booking_service: "Service",
            booking_service_placeholder: "Select service",
            booking_menu: "Menu",
            booking_menu_placeholder: "Select menu",
            booking_technician: "Technician",
            booking_technician_placeholder: "Select technician",
            booking_person_number: "Number",
            booking_date: "Date",
            booking_date_placeholder: "",
            booking_date_maxlength: "16",
            booking_hours: "Hour",
            booking_hours_placeholder: "Select hour",
            booking_note: "Note",
            booking_note_maxlength: "201",
            booking_note_placeholder: "(Max length 200 character)",
            booking_store: "Storefront",
            booking_store_placeholder: "Select storefront",
            booking_add_another_service: "Add another service",
            booking_information: "Appointment Information",
            booking_order_information: "Order Information",
            booking_popup_message: "Message",
            booking_popup_confirm: "Confirm booking information ?",
            booking_popup_confirm_description: "We will send a text message to you via the number below after we confirm the calendar for your booking.",
            booking_order_popup_confirm: "Confirm order information ?",
            booking_order_popup_confirm_description: "We will send a text message to you via the number below after we confirm the calendar for your order.",
            booking_btn_send: "Send appointment now",
            booking_btn_search: "Search",
            booking_btn_booking: "Booking",
            booking_btn_confirm: "Confirm",
            booking_btn_cancel: "Cancel",
            booking_hours_expired: "Has expired",
            booking_name_err: "Please enter your name",
            booking_phone_err: "Please enter your phone",
            booking_email_err: "Please enter your email",
            booking_service_err: "Please choose a service",
            booking_menu_err: "Please choose a menu",
            booking_technician_err: "Please choose a technician",
            booking_date_err: "Please choose date",
            booking_hours_err: "Please choose hour",
            booking_get_hours_timeout: "Network timeout, Please click the button search to try again",
        };
        let webBooking = {
            enable: true,
            minDate: "05/23/2021",
            requiredTechnician: false,
            requiredEmail: false,
            requiredHour: true,
            isRestaurant: false,
        };
        let webFormat = {
            dateFormat: "MM/DD/YYYY",
            datePosition: "1,0,2",
            phoneFormat: "(000) 000-0000",
        };
        let webGlobal = {
            site: "idx",
            siteAct: "",
            siteSubAct: "",
            noPhoto: "/public/library/global/no-photo.jpg",
            isTablet: false,
            isMobile: false,
            enableRecaptcha: false,
        };
        let facebook_embed = {
            id_fanpage: "https://www.facebook.com/FastboyMarketingAgency",
            appId: "",
            width: 450,
            height: 400,
            tabs: "timeline",
            show_facepile: true,
            small_header: true,
            likebox_enable: true,
            likebox_show_faces: false,
            likebox_stream: true,
        };
        let google_id_fanpage = "";
        let twitter_id_fanpage = "";

    </script>
    <meta name="csrf-token" content="{{ csrf_token() }}" />
</head>

<body class="site-idx site-act-">

    <!-- SOCIAL AND SEO -->
    <div id="fb-root"></div>
    <div style="height: 0; overflow: hidden">
        <h1 itemprop="name"></h1>
    </div>
    <div class="body-bg"></div>
    <section id="menu_mobile_wrap_v1">

        <input type="hidden" name="activeFreezeHeader" value="1" />

        @include('frontend.nail01-p.component.header',
        ['header'=>
        App\Models\Product::where('category_id',
        App\Models\Category::where('name','Header And Footer')->first()['id'])->first()])

        {{ $slot }}

        @include('frontend.nail01-p.component.footer',['header'=>
        App\Models\Product::where('category_id',
        App\Models\Category::where('name','Header And Footer')->first()['id'])->first()]))
    </section>

    <script src="//maxcdn.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@9"></script>
    <script>
        const SwalModal = (icon, title, html) => {
            Swal.fire({
                icon,
                title,
                html
            })
        }

        const SwalConfirm = (icon, title, html, confirmButtonText, method, params, callback) => {
            Swal.fire({
                icon,
                title,
                html,
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText,
                reverseButtons: true,
            }).then(result => {
                if (result.value) {
                    return livewire.emit(method, params)
                }

                if (callback) {
                    return livewire.emit(callback)
                }
            })
        }

        const SwalAlert = (icon, title, timeout = 7000) => {
            const Toast = Swal.mixin({
                toast: true,
                position: 'top-end',
                showConfirmButton: false,
                timer: timeout,
                onOpen: toast => {
                    toast.addEventListener('mouseenter', Swal.stopTimer)
                    toast.addEventListener('mouseleave', Swal.resumeTimer)
                }
            })

            Toast.fire({
                icon,
                title
            })
        }

        document.addEventListener('DOMContentLoaded', () => {
            this.livewire.on('swal:modal', data => {
                SwalModal(data.icon, data.title, data.text)
            })

            this.livewire.on('swal:confirm', data => {
                SwalConfirm(data.icon, data.title, data.text, data.confirmText, data.method, data
                    .params, data.callback)
            })

            this.livewire.on('swal:alert', data => {
                SwalAlert(data.icon, data.title, data.timeout)
            })
        })

    </script>
    <!-- AUTO REMOVE BOOKING BUTTON -->
    <script type="text/javascript">
        if (!webBooking.enable) {
            $(".btn_make_appointment").remove();
        }

    </script>
    <!-- JS -->
    <script type="text/javascript" src="{{ asset('frontend') }}/public/library/global/js/global-init.js"></script>
    <script type="text/javascript" src="{{ asset('frontend') }}/public/library/global/js/consent.js"></script>
    <script type="text/javascript" src="{{ asset('frontend') }}/themes/fnail01p/assets/js/script.js"></script>
    <script type="text/javascript" src="{{ asset('frontend') }}/themes/fnail01p/assets/custom/js/app.js?v=1.6">
    </script>
    <script type="text/javascript"></script>
    <script>
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

    </script>
    @livewireScripts
    @stack('modals')
</body>

</html>
