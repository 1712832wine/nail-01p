<main class="main">
    <!-- Use for get current id -->
    <input type="hidden" name="group_id" value="{{ $service_id }}" />
    <!-- Tpl main service -->
    <!--# Support:# render.service_data.service, render.service_data2.service-->
    <section class="page-title" style="background-image: url(&#39;images/background11.jpg&#39;)">
        <div class="container">
            <div class="inner-container clearfix">
                <h2 itemprop="name">Services</h2>
                <ul class="bread-crumb clearfix">
                    <li><a href="{{ route('home') }}" itemprop="url" title="Home">Home</a></li>
                    <li>Services</li>
                </ul>
            </div>
        </div>
    </section>
    <section class="page-content section-padding our-service">
        <div class="left-image">
            <img itemprop="image" src="{{ asset('frontend') }}/themes/fnail01p/assets/images/side-img-left.png"
                alt="side-img-left.png" />
        </div>
        <div class="auto-container">
            <div class="service-container animation_sroll_button">
                <div class="row">
                    <div class="col-md-7 col-md-push-5 order-md-2">
                        <div class="service-btn-group text-right btn-wrapper">
                            <a href="/book.html" class="btn btn-main btn-style-four btn_make_appointment"
                                title="Make an appointment">
                                <i class="fa fa-calendar"></i> Make an appointment </a>&nbsp
                            <a href="tel:651-362-0535‬" class="btn btn-main btn-style-four" title="Call now">
                                <i class="fa fa-phone"></i> Call now
                            </a>
                        </div>
                    </div>
                    <div class="col-md-5 col-md-pull-7 order-md-1">
                        <h3 class="title">Blooming Nails & Spa</h3>
                        <h2 itemprop="name">{{ $intro['title'] }}</h2>
                    </div>
                </div>
                <div class="service-head-desc">
                    {!! $intro['content'] !!}
                </div>
                <div class="clearfix animation_sroll_to_service service-list">

                    @foreach ($services as $index => $service)
                        @if ($index % 2)
                            <div class="row clearfix" id="sci_{{ $service['id'] }}">
                                {{-- AVATAR --}}
                                <div class="service-line"></div>
                                @if (count(json_decode($service['images'])) > 0)
                                    <div
                                        class="col-sm-4 col-md-4 order-md-2 col-sm-push-8 col-md-push-8 left service-image-wrap">
                                        <div class="service-image circle">
                                            <div class="service-image-bg"
                                                style="background-image: url('{{ asset('storage') }}/photos/{{ json_decode($service['images'])[0] }}');">
                                                <img class="img-responsive"
                                                    src="{{ asset('storage') }}/photos/{{ json_decode($service['images'])[0] }}"
                                                    alt="Nail Enhancement" />
                                            </div>
                                        </div>
                                    </div>
                                @endif

                                <div class="col-sm-8 col-md-8 order-md-1 col-sm-pull-4 col-md-pull-4">
                                    <div class="clearfix service-list">
                                        <h2 class="service-name">{{ $service['name'] }}</h2>
                                        <div class="service-desc">{{ $service['description'] }}</div>
                                        @foreach (json_decode($service['features']) as $item)
                                            <div class="detail-item item-131">
                                                <div class="detail-price-item">
                                                    <span class="detail-price-name">{{ $item->name }}</span>
                                                    <span class="detail-price-dots"></span>
                                                    <span class="detail-price-number">
                                                        <span class="current">{{ $item->desc }}</span>
                                                    </span>
                                                </div>
                                            </div>
                                        @endforeach
                                    </div>
                                </div>
                            </div>
                        @else
                            <div class="row clearfix" id="sci_{{ $service['id'] }}">
                                {{-- AVATAR --}}
                                @if (count(json_decode($service['images'])) > 0)
                                    <div class="col-sm-4 col-md-4 right service-image-wrap">
                                        <div class="service-image circle">
                                            <div class="service-image-bg"
                                                style="background-image: url('{{ asset('storage') }}/photos/{{ json_decode($service['images'])[0] }}');">
                                                <img class="img-responsive"
                                                    src="{{ asset('storage') }}/photos/{{ json_decode($service['images'])[0] }}"
                                                    alt="Artificial Nails / Acrylic" />
                                            </div>
                                        </div>
                                    </div>
                                @endif
                                <div class="col-sm-8 col-md-8">
                                    <div class="clearfix service-list">
                                        <h2 class="service-name">{{ $service['name'] }}</h2>
                                        <div class="service-desc">{{ $service['description'] }}</div>
                                        @foreach (json_decode($service['features']) as $item)
                                            <div class="detail-item">
                                                <div class="detail-price-item">
                                                    <span class="detail-price-name">{{ $item->name }}</span>
                                                    <span class="detail-price-dots"></span>
                                                    <span class="detail-price-number">
                                                        <span class="current">{{ $item->desc }}</span>
                                                    </span>
                                                </div>
                                            </div>
                                        @endforeach
                                    </div>
                                </div>
                            </div>
                        @endif

                    @endforeach
                </div>
            </div>
        </div>
    </section>
</main>
