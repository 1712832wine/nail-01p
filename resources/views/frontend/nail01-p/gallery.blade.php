<main class="main">
    <section class="page-title">
        <div class="container">
            <div class="inner-container clearfix">
                <h2 itemprop="name">Gallery</h2>
                <ul class="bread-crumb clearfix">
                    <li><a href="{{ route('home') }}" itemprop="url" title="Home">Home</a></li>
                    <li>Gallery</li>
                </ul>
            </div>
        </div>
    </section>
    <section class="page-content section-padding our-gallery">
        <div class="left-image">
            <img itemprop="image" src="{{ asset('frontend') }}/themes/fnail01p/assets/images/side-img-left.png"
                alt="side-img-left.png" />
        </div>
        <div class="auto-container">
            <h3 class="title">Blooming Nails & Spa</h3>
            <h2 itemprop="name">Our Gallery</h2>
            <div>
                <ul class="clearfix m-category-tab" id="category_tab">
                    @foreach ($albums as $index => $album)
                        <li class="tab @if ($index===0) active @endif"
                            data-id="{{ $album['id'] }}">
                            <span itemprop="name">{{ $album['name'] }}</span>
                        </li>
                    @endforeach
                </ul>
                <div class="clearfix m-gallery-content" id="gallery_content" data-isNewTemplate="true">
                    <div class="clearfix m-gallery-listing listing"></div>
                    <div class="clearfix m-gallery-paging paging"></div>
                </div>
    </section>

</main>
