<main class="main">
    <!-- Tpl main contact -->

    <section class="page-title">
        <div class="container">
            <div class="inner-container clearfix">
                <h2 itemprop="name">Contact Us</h2>
                <ul class="bread-crumb clearfix">
                    <li><a href="/" itemprop="url" title="Home">Home</a></li>
                    <li>Contact Us</li>
                </ul>
            </div>
        </div>
    </section>
    <section class="page-content section-padding our-contact contact-page-section">
        <div class="left-image">
            <img itemprop="image" src="{{ asset('frontend') }}/themes/fnail01p/assets/images/side-img-left.png"
                alt="side-img-left.png" />
        </div>
        <div class="auto-container form-section">
            <h3 class="title">Blooming Nails & Spa</h3>
            <h2 itemprop="name">Contact Us</h2>
            <div class="row clearfix">
                <div class="form-column col-lg-8 col-md-7 col-sm-12 col-xs-12">
                    <div class="contact-form-two">
                        @if (session('status'))
                            <div class="alert alert-success" role="alert">
                                <button type="button" class="close" data-dismiss="alert">×</button>
                                {{ session('status') }}
                            </div>
                        @elseif(session('failed'))
                            <div class="alert alert-danger" role="alert">
                                <button type="button" class="close" data-dismiss="alert">×</button>
                                {{ session('failed') }}
                            </div>
                        @endif
                        <form method="post" name="send_contact" id="send_contact" action="{{ route('send_contact') }}"
                            class="form-horizontal">
                            @csrf
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="group-select">
                                        <label>Your name (required)</label>
                                        <div class="relative w100">
                                            <input title="Your name" type="text" name="contactname"
                                                data-validation="[NOTEMPTY]"
                                                data-validation-message="Please enter your name" class="form-control"
                                                autocomplete="off" placeholder="" maxlength="76" />
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="group-select">
                                        <label>Your email address (required)</label>
                                        <div class="relative w100">
                                            <input title="Your email address" type="text" name="contactemail"
                                                data-validation="[EMAIL]"
                                                data-validation-message="Please enter your email" class="form-control"
                                                autocomplete="off" placeholder="" maxlength="76" />
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="group-select">
                                <label>Your subject (required)</label>
                                <div class="relative w100">
                                    <input title="Your subject" type="text" name="contactsubject"
                                        data-validation="[NOTEMPTY]" data-validation-message="Please enter your subject"
                                        class="form-control" autocomplete="off" placeholder="" maxlength="251" />
                                </div>
                            </div>
                            <div class="group-select">
                                <label>Your message (required)</label>
                                <div class="relative w100">
                                    <textarea title="Your message" name="contactcontent" data-validation="[NOTEMPTY]"
                                        data-validation-message="Please enter your message" class="form-control"
                                        autocomplete="off" rows="10" placeholder="" maxlength="501"></textarea>
                                </div>
                            </div>
                            <div class="form-group text-center">
                                <button type="submit" class="btn theme-btn btn-style-five btn_contact">
                                    Send Us <i class="fa fa-angle-double-right"></i>
                                </button>
                            </div>
                        </form>
                    </div>
                </div>
                <div class="contact-column col-lg-4 col-md-5 col-sm-12 col-xs-12">
                    <div class="inner-column">
                        <ul class="contact-info">
                            <li>
                                <span class="icon fa fa-paper-plane-o"></span>
                                <strong>Address:</strong>
                                <p>
                                    <span itemprop="address" itemscope itemtype="http://schema.org/PostalAddress">
                                        <span itemprop="streetAddress" class="address">11011 Richmond Ave,<br />
                                            Ste 250 <br />
                                            Houston, TX 77042</span>
                                    </span>
                                </p>
                            </li>
                            <li>
                                <span class="icon fa fa-mobile"></span>
                                <strong>Phone:</strong>
                                <div>
                                    <a href="tel:651-362-0535‬" title="Call Us">
                                        <span itemprop="telephone" class="phone">651-362-0535‬</span>
                                    </a>
                                    <br />
                                    <a href="tel:" title="Call Us">
                                        <span itemprop="telephone" class="phone"></span>
                                    </a>
                                </div>
                            </li>
                            <li>
                                <span class="icon fa fa-envelope-o"></span>
                                <strong>Email</strong>
                                <div>
                                    <a href="mailto:web@fastboy.net" title="Mail Us">
                                        <span itemprop="email" class="email">web@fastboy.net</span>
                                    </a>
                                    <br />
                                    <a href="mailto:" title="Mail Us">
                                        <span itemprop="email" class="email"></span>
                                    </a>
                                </div>
                            </li>
                            <li>
                                <span class="icon fa fa-clock-o"></span>
                                <strong>Open Hours:</strong>
                                <div>
                                    <div class="foh-wrap">
                                        <!-- Normal Day -->
                                        <div class="foh-row short" itemprop="openingHours"
                                            content="Mon - Fri 10:00 am - 7:00 pm">
                                            <span class="foh-date">Mon - Fri:</span>
                                            <span class="foh-time">10:00 am - 7:00 pm</span>
                                        </div>

                                        <!-- Normal Day -->
                                        <div class="foh-row short" itemprop="openingHours"
                                            content="Saturday 10:00 am - 6:00 pm">
                                            <span class="foh-date">Saturday:</span>
                                            <span class="foh-time">10:00 am - 6:00 pm</span>
                                        </div>

                                        <!-- Normal Day -->
                                        <div class="foh-row short" itemprop="openingHours"
                                            content="Sunday 11:00 am - 5:00 pm">
                                            <span class="foh-date">Sunday:</span>
                                            <span class="foh-time">11:00 am - 5:00 pm</span>
                                        </div>
                                    </div>
                                </div>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="map-section">
                <div class="google-maps" id="google-map">
                    <iframe
                        src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3464.7391505606774!2d-95.57299108550652!3d29.727314340440024!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x8640ddc6ed681049%3A0x74ace03ed50728e8!2sFast+Boy+Marketing!5e0!3m2!1sen!2s!4v1510909605272"
                        width="100%" height="450" frameborder="0" style="border: 0" allowfullscreen></iframe>
                </div>
            </div>
        </div>
    </section>
</main>
