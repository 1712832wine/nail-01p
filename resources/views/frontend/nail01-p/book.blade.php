<main class="main">
    <section class="page-title">
        <div class="container">
            <div class="inner-container clearfix">
                <h2 itemprop="name">Booking</h2>
                <ul class="bread-crumb clearfix">
                    <li><a href="/" itemprop="url" title="Home">Home</a></li>
                    <li>Booking</li>
                </ul>
            </div>
        </div>
    </section>
    <section class="page-content section-padding our-service">
        <div class="left-image">
            <img itemprop="image" src="{{ asset('frontend') }}/themes/fnail01p/assets/images/side-img-left.png"
                alt="side-img-left.png" />
        </div>
        <div class="auto-container">
            <h3 class="title">Blooming Nails & Spa</h3>
            <h2 itemprop="name">Booking</h2>
            <div>
                <section id="boxBookingForm" class="box-booking-form">
                    <form id="formBooking" class="form-booking" name="formBooking" action="/book/add" method="post">
                        <div class="row">
                            <div class="col-sm-6 col-md-4 col-lg-3 booking-date">
                                <div class="group-select">
                                    <label>Date (required)</label>
                                    <div class="relative w100 form-input-group">
                                        <input type="text" class="form-control form-text booking_date"
                                            autocomplete="off" name="booking_date" value="" data-validation="[NOTEMPTY]"
                                            data-validation-message="Please choose date" title="Booking date"
                                            placeholder="" maxlength="16" />
                                        <span class="fa fa-calendar calendar form-icon"></span>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-12 col-md-12 col-lg-9">
                                <div class="row booking-service-staff booking-item" id="bookingItem_0">
                                    <div class="col-sm-12 col-md-7 col-lg-8 booking-service">
                                        <div class="group-select">
                                            <label>Service (required)</label>
                                            <div class="relative w100">
                                                <select class="form-control booking_service" name="product_id[]"
                                                    data-validation="[NOTEMPTY]"
                                                    data-validation-message="Please choose a service"
                                                    title="Booking service">
                                                    <option value="">Select service</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-12 col-md-5 col-lg-4 booking-staff">
                                        <div class="group-select">
                                            <label>Technician (optional)</label>
                                            <div class="relative w100">
                                                <select class="form-control booking_staff" name="staff_id[]"
                                                    title="Booking staff">
                                                    <option value="">Select technician</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <!-- Button Add Item -->
                                <div class="clearfix">
                                    <div class="add-services pointer booking_item_add">
                                        <i class="fa fa-plus-circle"></i> Add another service
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-12 col-md-12 col-lg-3">
                                <div class="group-select">
                                    <label>&nbsp;</label>
                                    <div class="relative w100">
                                        <button class="btn btn-search search_booking" type="button">
                                            Search
                                        </button>
                                    </div>

                                    <!-- Hidden data -->
                                    <input type="hidden" name="booking_hours" class="booking_hours" value="" />
                                    <input type="hidden" name="booking_name" class="booking_name" value="" />
                                    <input type="hidden" name="booking_phone" class="booking_phone" value="" />
                                    <input type="hidden" name="booking_email" class="booking_email" value="" />
                                    <input type="hidden" name="notelist" class="notelist" value="" />
                                    <input type="hidden" name="store_id" class="store_id" value="" />

                                    <input type="hidden" name="booking_area_code" class="booking_area_code" value="1" />
                                    <input type="hidden" name="booking_form_email" class="booking_form_email"
                                        value="0" />
                                    <input type="hidden" name="nocaptcha" class="nocaptcha" value="1" />
                                    <input type="hidden" name="g-recaptcha-response" class="g-recaptcha-response"
                                        value="" />
                                </div>
                            </div>
                        </div>
                    </form>

                    <script type="text/javascript">
                    let today = new Date();
                        let currenday ='0'+(today.getMonth()+1) +'\\/'+ today.getDate()+'\\/'+today.getFullYear();
                         currenday='05\/22\/2021';
                        console.log(currenday);
                        $(document).ready(function() {
                            /* Init Booking */
                            webBookingForm.init(
                                '{"58":{"id":58,"name":"Artificial Nails \/ Acrylic","services":[113,114,115,116,117,118,119,120,121,122,123,124,125,126,127,128,129,130]},"59":{"id":59,"name":"Nail Enhancement ","services":[131,132,133,134,135,136,137,138,139,140,141,142,143,144,145,146,147]},"60":{"id":60,"name":"Kids Menu","services":[149,151,150,152,153]},"61":{"id":61,"name":"Hair","services":[154,155,156,157,158,159,160,161,162,163,164,165,166,167,168]},"62":{"id":62,"name":"Waxing","services":[169,170,171,172,173,174,175,176,177,178,179,180,181,182,183,184]},"63":{"id":63,"name":"Facials","services":[185,186,187,188]}}',
                                '{"113":{"id":113,"name":"Acrylic Full-Set","price":"$30 &amp; Up","staffs":null,"schedule":false,"unit":"person","scheduleDay":null},"114":{"id":114,"name":"Acrylic Fill","price":"$20 ","staffs":null,"schedule":false,"unit":"person","scheduleDay":null},"115":{"id":115,"name":"Gel Full-Set","price":"$45 ","staffs":null,"schedule":false,"unit":"person","scheduleDay":null},"116":{"id":116,"name":"Gell Fill","price":"$35 ","staffs":null,"schedule":false,"unit":"person","scheduleDay":null},"117":{"id":117,"name":"Gell Back-Fill","price":"$45 ","staffs":null,"schedule":false,"unit":"person","scheduleDay":null},"118":{"id":118,"name":"Solar Regular Full-Set","price":"$35 ","staffs":null,"schedule":false,"unit":"person","scheduleDay":null},"119":{"id":119,"name":"Solar Full-Set Color","price":"$50 ","staffs":null,"schedule":false,"unit":"person","scheduleDay":null},"120":{"id":120,"name":"Solar Fill","price":"$25 ","staffs":null,"schedule":false,"unit":"person","scheduleDay":null},"121":{"id":121,"name":"Solar Full-Set P &amp; W","price":"$45 ","staffs":null,"schedule":false,"unit":"person","scheduleDay":null},"122":{"id":122,"name":"Solar Full-Set Color Tip P &amp; W","price":"$50 ","staffs":null,"schedule":false,"unit":"person","scheduleDay":null},"123":{"id":123,"name":"Solar Back-Fill P &amp; W","price":"$40 ","staffs":null,"schedule":false,"unit":"person","scheduleDay":null},"124":{"id":124,"name":"Solar Back-Fill Color Tip P &amp; W","price":"$45 ","staffs":null,"schedule":false,"unit":"person","scheduleDay":null},"125":{"id":125,"name":"Gel Coating","price":"$5 extra","staffs":null,"schedule":false,"unit":"person","scheduleDay":null},"126":{"id":126,"name":"Acrylic Nails Take Off","price":"$10 ","staffs":null,"schedule":false,"unit":"person","scheduleDay":null},"127":{"id":127,"name":"Nails Repair","price":"$5 each","staffs":null,"schedule":false,"unit":"person","scheduleDay":null},"128":{"id":128,"name":"Gelish or Shellac","price":"$15 extra","staffs":null,"schedule":false,"unit":"person","scheduleDay":null},"129":{"id":129,"name":"*Any service with French will cost $5 extra","price":"$0 ","staffs":null,"schedule":false,"unit":"person","scheduleDay":null},"130":{"id":130,"name":"*Long Nail will be charge extra","price":"$0 ","staffs":null,"schedule":false,"unit":"person","scheduleDay":null},"131":{"id":131,"name":"Express Manicure","price":"$15 ","staffs":null,"schedule":false,"unit":"person","scheduleDay":null},"132":{"id":132,"name":"Manicure","price":"$20 ","staffs":null,"schedule":false,"unit":"person","scheduleDay":null},"133":{"id":133,"name":"Deluxe Manicure","price":"$25 ","staffs":null,"schedule":false,"unit":"person","scheduleDay":null},"134":{"id":134,"name":"Spa Pedicure","price":"$25 ","staffs":null,"schedule":false,"unit":"person","scheduleDay":null},"135":{"id":135,"name":"Deluxe Pedicure (40 min)","price":"$35 ","staffs":null,"schedule":false,"unit":"person","scheduleDay":null},"136":{"id":136,"name":"Signature Pedicure (1 hour)","price":"$45 ","staffs":null,"schedule":false,"unit":"person","scheduleDay":null},"137":{"id":137,"name":"Mani &amp; Spa Pedi","price":"$45 ","staffs":null,"schedule":false,"unit":"person","scheduleDay":null},"138":{"id":138,"name":"Nails Polish Change","price":"$8 ","staffs":null,"schedule":false,"unit":"person","scheduleDay":null},"139":{"id":139,"name":"Toes Polish Change","price":"$10 ","staffs":null,"schedule":false,"unit":"person","scheduleDay":null},"140":{"id":140,"name":"Nail Cut &amp; File","price":"$10 ","staffs":null,"schedule":false,"unit":"person","scheduleDay":null},"141":{"id":141,"name":"Nails Arts (2 designs)","price":"$5 &amp; Up","staffs":null,"schedule":false,"unit":"person","scheduleDay":null},"142":{"id":142,"name":"Take Off &amp; Manicure","price":"$25 ","staffs":null,"schedule":false,"unit":"person","scheduleDay":null},"143":{"id":143,"name":"Shellac Color","price":"$30 ","staffs":null,"schedule":false,"unit":"person","scheduleDay":null},"144":{"id":144,"name":"Shellac Polish Change","price":"$20 ","staffs":null,"schedule":false,"unit":"person","scheduleDay":null},"145":{"id":145,"name":"Take Off Shellac","price":"$10 ","staffs":null,"schedule":false,"unit":"person","scheduleDay":null},"146":{"id":146,"name":"Color W\/French Tip","price":"$8 ","staffs":null,"schedule":false,"unit":"person","scheduleDay":null},"147":{"id":147,"name":"Any service with paraffin wax will cost","price":"$5 extra","staffs":null,"schedule":false,"unit":"person","scheduleDay":null},"149":{"id":149,"name":"Kid Mani","price":"$13 ","staffs":null,"schedule":false,"unit":"person","scheduleDay":null},"151":{"id":151,"name":"Kid Pedi","price":"$19 ","staffs":null,"schedule":false,"unit":"person","scheduleDay":null},"150":{"id":150,"name":"Kid Mani &amp; Pedi","price":"$29 ","staffs":null,"schedule":false,"unit":"person","scheduleDay":null},"152":{"id":152,"name":"Polish Change Hands","price":"$8 ","staffs":null,"schedule":false,"unit":"person","scheduleDay":null},"153":{"id":153,"name":"Polish Change Toes","price":"$9 ","staffs":null,"schedule":false,"unit":"person","scheduleDay":null},"154":{"id":154,"name":"Women Cut","price":"$30 &amp; Up","staffs":null,"schedule":false,"unit":"person","scheduleDay":null},"155":{"id":155,"name":"Men Cut","price":"$20 ","staffs":null,"schedule":false,"unit":"person","scheduleDay":null},"156":{"id":156,"name":"Girl\/Boy under 10 Cut","price":"$18 ","staffs":null,"schedule":false,"unit":"person","scheduleDay":null},"157":{"id":157,"name":"Bang Trim","price":"$5 &amp; Up","staffs":null,"schedule":false,"unit":"person","scheduleDay":null},"158":{"id":158,"name":"Women All-Over Color","price":"$65 &amp; Up","staffs":null,"schedule":false,"unit":"person","scheduleDay":null},"159":{"id":159,"name":"Women Color Touch-Up","price":"$55 &amp; Up","staffs":null,"schedule":false,"unit":"person","scheduleDay":null},"160":{"id":160,"name":"Women Highlights","price":"$75 &amp; Up","staffs":null,"schedule":false,"unit":"person","scheduleDay":null},"161":{"id":161,"name":"Women Partial Highlights","price":"$55 &amp; Up","staffs":null,"schedule":false,"unit":"person","scheduleDay":null},"162":{"id":162,"name":"Women Partial Lowlights","price":"$55 &amp; Up","staffs":null,"schedule":false,"unit":"person","scheduleDay":null},"163":{"id":163,"name":"Perm","price":"$80 &amp; Up","staffs":null,"schedule":false,"unit":"person","scheduleDay":null},"164":{"id":164,"name":"Men Highlights","price":"$50 &amp; Up","staffs":null,"schedule":false,"unit":"person","scheduleDay":null},"165":{"id":165,"name":"Men Color","price":"$50 &amp; Up","staffs":null,"schedule":false,"unit":"person","scheduleDay":null},"166":{"id":166,"name":"Shampoo","price":"$5 ","staffs":null,"schedule":false,"unit":"person","scheduleDay":null},"167":{"id":167,"name":"Shampoo &amp; Style","price":"$35 &amp; Up","staffs":null,"schedule":false,"unit":"person","scheduleDay":null},"168":{"id":168,"name":"Up-do","price":"$50 &amp; Up","staffs":null,"schedule":false,"unit":"person","scheduleDay":null},"169":{"id":169,"name":"Eyebrow","price":"$12 ","staffs":null,"schedule":false,"unit":"person","scheduleDay":null},"170":{"id":170,"name":"Upper Lip","price":"$10 ","staffs":null,"schedule":false,"unit":"person","scheduleDay":null},"171":{"id":171,"name":"Eyebrow &amp; Upper Lip","price":"$20 ","staffs":null,"schedule":false,"unit":"person","scheduleDay":null},"172":{"id":172,"name":"Eyebrow, Upper Lip, Chin","price":"$28 ","staffs":null,"schedule":false,"unit":"person","scheduleDay":null},"173":{"id":173,"name":"Under Arms","price":"$20 ","staffs":null,"schedule":false,"unit":"person","scheduleDay":null},"174":{"id":174,"name":"Face Wax","price":"$40 ","staffs":null,"schedule":false,"unit":"person","scheduleDay":null},"175":{"id":175,"name":"Chin","price":"$12 ","staffs":null,"schedule":false,"unit":"person","scheduleDay":null},"176":{"id":176,"name":"Chest Or Back","price":"$45 &amp; Up","staffs":null,"schedule":false,"unit":"person","scheduleDay":null},"177":{"id":177,"name":"Half Legs","price":"$40 &amp; Up","staffs":null,"schedule":false,"unit":"person","scheduleDay":null},"178":{"id":178,"name":"Full Legs","price":"$65 &amp; Up","staffs":null,"schedule":false,"unit":"person","scheduleDay":null},"179":{"id":179,"name":"Bikini","price":"$40 &amp; Up","staffs":null,"schedule":false,"unit":"person","scheduleDay":null},"180":{"id":180,"name":"Brazillian","price":"$55 &amp; Up","staffs":null,"schedule":false,"unit":"person","scheduleDay":null},"181":{"id":181,"name":"Arms","price":"$45 &amp; Up","staffs":null,"schedule":false,"unit":"person","scheduleDay":null},"182":{"id":182,"name":"Half Arms","price":"$35 &amp; Up","staffs":null,"schedule":false,"unit":"person","scheduleDay":null},"183":{"id":183,"name":"Eye Lashes Extension","price":"$150 &amp; Up","staffs":null,"schedule":false,"unit":"person","scheduleDay":null},"184":{"id":184,"name":"Eye Lashes Extension Refill","price":"$65 &amp; Up","staffs":null,"schedule":false,"unit":"person","scheduleDay":null},"185":{"id":185,"name":"Express Facial","price":"$45 (30 minutes)","staffs":null,"schedule":false,"unit":"person","scheduleDay":null},"186":{"id":186,"name":"Basic Facial","price":"$60 (60 minutes)","staffs":null,"schedule":false,"unit":"person","scheduleDay":null},"187":{"id":187,"name":"Reflexology","price":"$60 (60 minutes)","staffs":null,"schedule":false,"unit":"person","scheduleDay":null},"188":{"id":188,"name":"Make Up","price":"$55 ","staffs":null,"schedule":false,"unit":"person","scheduleDay":null}}',
                                "null",
                                '{"listperson":[],"notelist":[],"booking_date":"'+currenday+'","booking_hours":[""]}'
                            );
                        });

                    </script>
                </section>

                <section id="boxBookingInfo" class="box-booking-info relative" style="display: none">
                    <h3 class="booking-info-title">Appointment Information</h3>

                    <!-- Service, Staff -->
                    <div id="boxServiceStaff" class="box-service-staff">
                        <div class="service-staff">
                            <div class="service-staff-avatar">
                                <img class="img-responsive"
                                    src="{{ asset('frontend')}}/public/library/global/no-photo.jpg"
                                    alt="Any person" />
                            </div>
                            <div class="service-staff-info">
                                <h5>Any Technician</h5>
                                <p>Any Service</p>
                                <p>Price: N/A</p>
                            </div>
                        </div>
                        <div class="service-staff">
                            <div class="service-staff-avatar no-photo">
                                <img class="img-responsive"
                                    src="{{ asset('frontend')}}/public/library/global/no-photo.jpg"
                                    alt="Any person" />
                            </div>
                            <div class="service-staff-info">
                                <h5>Any Technician</h5>
                                <p>Any Service</p>
                                <p>Price: N/A</p>
                            </div>
                        </div>
                    </div>

                    <!-- Date, Time List -->
                    <div id="boxDateTime" class="box-date-time">
                        <h4 class="date-info" id="dateInfo"></h4>
                        <div class="time-info">
                            <h5>
                                Morning <span class="time-note" id="timeAMNote"></span>
                            </h5>
                            <ul class="time-items" id="timeAMHtml">

                            </ul>
                        </div>
                        <div class="time-info">
                            <h5>
                                Afternoon
                                <span class="time-note" id="timePMNote">N/A</span>
                            </h5>
                            <ul class="time-items" id="timePMHtml">
                            </ul>
                        </div>
                    </div>
                </section>

                <section id="popupBookingConfirm" class="popup-booking-confirm white-popup mfp-hide border-style">
                    <section id="boxBookingConfirm" class="box-booking-confirm relative">
                        <h3 class="booking-confirm-title">
                            Confirm booking information ?
                        </h3>
                        <div class="booking-confirm-note">
                            We will send a text message to you via the number below
                            after we confirm the calendar for your booking.
                        </div>
                        <script src="https://www.google.com/recaptcha/api.js?hl=en" async defer></script>
                        <!-- Google reCaptcha -->
                        <script type="text/javascript">
                            function ezyCaptcha_formBookingConfirm(token, is_submit) {
                                is_submit = 1;
                                if ($("#password").length) {
                                    //$("input:password").val(md5(clean_input($("#password").val())));
                                }
                                return true;
                            }

                        </script>
                        <form id="formBookingConfirm" class="form-booking-confirm" name="formBookingConfirm"
                            method="post" action="/book" enctype="multipart/form-data">
                            <div class="row">
                                <div class="col-md-6 booking-name">
                                    <div class="group-select">
                                        <label>Your name (required)</label>
                                        <div class="relative w100">
                                            <input type="text" class="form-control" name="booking_name" value=""
                                                data-validation="[NOTEMPTY]"
                                                data-validation-message="Please enter your name" placeholder=""
                                                maxlength="76" title="Booking name" />
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6 booking-phone">
                                    <div class="group-select">
                                        <label>Your phone (required)</label>
                                        <div class="relative w100">
                                            <input type="text" class="form-control inputPhone" pattern="\d*"
                                                name="booking_phone" data-validation="[NOTEMPTY]"
                                                data-validation-message="Please enter your phone" placeholder=""
                                                maxlength="16" title="Booking phone" />
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-12 booking-email">
                                    <div style="display: none" class="group-select">
                                        <label>Your email (optional)</label>
                                        <div class="relative w100">
                                            <input type="text" class="form-control" name="booking_email" value=""
                                                placeholder="" maxlength="76" title="Booking email" />
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-12 booking-notelist">
                                    <div class="group-select">
                                        <label>Note (optional)</label>
                                        <div class="relative w100">
                                            <textarea class="form-control" rows="5" name="notelist"
                                                placeholder="(Max length 200 character)" maxlength="201"
                                                title="Booking note"></textarea>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-12 booking-store">
                                    <input type="hidden" name="choose_store" class="booking_store" value="0" />
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-8 order-md-2 col-md-push-4">
                                    <button class="btn btn-confirm btn_confirm" type="button">
                                        Confirm
                                    </button>
                                </div>
                                <div class="col-md-4 order-md-1 col-md-pull-8">
                                    <button class="btn btn-cancel btn_cancel" type="button">
                                        Cancel
                                    </button>
                                </div>
                            </div>
                        </form>
                    </section>
                </section>
            </div>
        </div>
    </section>
</main>
