<footer class="footer">
    <!-- Tpl footer main -->
    <!--# Support:# render.logo.layouts, render.openhours_data.layouts, render.openhoursshort_data.layouts# render.social_icons.layouts, render.social_icons2.layouts, render.social_fanpage.layouts, render.google_maps.layouts-->
    <section class="main-footer">
        <section class="contact-section"
            style="background-image: url({{ asset('storage') }}/photos/{{ json_decode($header['images'])[1] }}">
            <div class="container">
                <div class="row clearfix">
                    <div class="form-column col-md-4 col-sm-12 col-xs-12">
                        <div class="heading">
                            <h2>Contact Us</h2>
                        </div>
                        @php
                            $features = json_decode($header['features']);
                            $extras = json_decode($header['extras']);
                        @endphp
                        <div class="fci-wrap">
                            @if (count($features) > 0)
                                <div class="fci-row">
                                    <span class="fci-title"><i
                                            class="fa {{ $features[0]->name }} icons-phone"></i></span>
                                    <span class="fci-content adr-ft">
                                        <a href="{{ route('home') }}" title="Address">
                                            <span itemprop="telephone" class="phone">{{ $features[0]->desc }}</span>
                                        </a>
                                        <br />
                                    </span>
                                </div>
                            @endif
                            @if (count($features) > 1)
                                <div class="fci-row">
                                    <span class="fci-title"><i
                                            class="fa {{ $features[1]->name }} icons-phone"></i></span>
                                    <span class="fci-content">
                                        <a href="tel:{{ $features[1]->desc }}‬" title="Call Us">
                                            <span itemprop="telephone" class="phone">{{ $features[1]->desc }}‬</span>
                                        </a>
                                        <br />
                                        <a href="tel:" title="Call Us">
                                            <span itemprop="telephone" class="phone"></span>
                                        </a>
                                    </span>
                                </div>
                            @endif
                            @if (count($features) > 2)
                                <div class="fci-row">
                                    <span class="fci-title"><i
                                            class="fa {{ $features[2]->name }} icons-email"></i></span>
                                    <span class="fci-content">
                                        <a href="mailto:{{ $features[2]->desc }}" title="Mail Us">
                                            <span itemprop="email" class="email">{{ $features[2]->desc }}</span>
                                        </a>
                                        <br />
                                        <a href="mailto:" title="Mail Us">
                                            <span itemprop="email" class="email"></span>
                                        </a>
                                    </span>
                                </div>
                            @endif

                        </div>
                        <div class="heading">
                            <h2>Business Hours</h2>
                        </div>
                        <div>
                            <div class="foh-wrap">
                                @foreach ($extras as $extra)
                                    <div class="foh-row short" itemprop="openingHours" content="{{ $extra->desc }}">
                                        <span class="foh-date">{{ $extra->name }}</span>
                                        <span class="foh-time">{{ $extra->desc }}</span>
                                    </div>
                                @endforeach
                                <!-- Normal Day -->

                            </div>
                        </div>
                    </div>
                    <div class="form-column col-md-4 col-sm-12 col-xs-12">
                        <div class="heading">
                            <h2>Social</h2>
                        </div>
                        <div class="socials">
                            <ul class="list-line social">
                                <!-- facebook link -->
                                <li>
                                    <a itemprop="url" rel="nofollow" target="_blank" title="Facebook"
                                        href="https://www.facebook.com/FastboyMarketingAgency/">
                                        <img src="{{ asset('frontend') }}/public/library/social/circle/facebook.png"
                                            alt="Facebook" />
                                    </a>
                                </li>

                                <!-- google link -->
                                <li>
                                    <a itemprop="url" rel="nofollow" target="_blank" title="Google plus"
                                        href="https://www.google.com/maps/place/Fast+Boy+Marketing">
                                        <img src="{{ asset('frontend') }}/public/library/social/circle/google-plus.png"
                                            alt="Google plus" />
                                    </a>
                                </li>

                                <!-- twitter link -->

                                <!-- youtube link -->
                                <li>
                                    <a itemprop="url" rel="nofollow" target="_blank" title="Youtube"
                                        href="https://www.youtube.com">
                                        <img src="{{ asset('frontend') }}/public/library/social/circle/youtube.png"
                                            alt="Youtube" />
                                    </a>
                                </li>

                                <!-- instagram link -->
                                <li>
                                    <a itemprop="url" rel="nofollow" target="_blank" title="Instagram"
                                        href="https://www.instagram.com">
                                        <img src="{{ asset('frontend') }}/public/library/social/circle/instagram.png"
                                            alt="Instagram" />
                                    </a>
                                </li>

                                <!-- yelp link -->
                                <li>
                                    <a itemprop="url" rel="nofollow" target="_blank" title="Yelp"
                                        href="https://www.yelp.com/">
                                        <img src="{{ asset('frontend') }}/public/library/social/circle/yelp.png"
                                            alt="Yelp" />
                                    </a>
                                </li>
                                <li>
                                    <a itemprop="url" rel="nofollow" target="_blank" title="Foursquare"
                                        href="https://foursquare.com/">
                                        <img src="{{ asset('frontend') }}/public/library/social/circle/foursquare.png"
                                            alt="Foursquare" />
                                    </a>
                                </li>
                            </ul>
                        </div>
                        <div class="heading">
                            <h2>Follow Us</h2>
                        </div>
                        <div>
                            <!-- Start Single Footer Widget -->
                            <div class="social-fanpage clearfix">
                                <aside>
                                    <!-- facebook fanpage -->
                                    <div id="fanpage_fb_container"></div>
                                </aside>
                                <!-- use for calculator width -->
                                <div id="social_block_width" class="clearfix"
                                    style="width: 100% !important; height: 1px !important"></div>
                            </div>
                            <!-- End Single Footer Widget -->
                        </div>
                    </div>
                    <div class="map-column col-md-4 col-sm-12 col-xs-12">
                        <div class="heading">
                            <h2>Our Location</h2>
                        </div>
                        <div class="map-box">
                            <div class="google-maps" id="google-map">
                                <iframe
                                    src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3464.7391505606774!2d-95.57299108550652!3d29.727314340440024!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x8640ddc6ed681049%3A0x74ace03ed50728e8!2sFast+Boy+Marketing!5e0!3m2!1sen!2s!4v1510909605272"
                                    width="100%" height="450" frameborder="0" style="border: 0"
                                    allowfullscreen></iframe>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <div class="footer-bottom">
            <div class="container">
                <div class="copyright-text clearfix">
                    <div id="back-top" class="scroll-to-top scroll-to-target" data-target="html" style="display: block">
                        <span class="icon fa fa-level-up"></span>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <p class="copyright">
                                © 2021 Blooming Nails & Spa. All Rights Reserved.
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <div class="back-to-top">
        <span class="back-top"><i class="fa fa-chevron-up"></i></span>
    </div>
</footer>
<div class="freeze-footer">
    <ul>
        <li>
            <a href="tel:651-362-0535‬" class="btn btn-call" title="Call us">
                <i class="fa fa-phone" aria-hidden="true"></i>
                <span class="d-md-none hidden-md hidden-lg">Call Us</span>
            </a>
        </li>
        <li class="btn_make_appointment">
            <a href="{{ route('book') }}" class="btn btn-booking btn_make_appointment" title="Booking">
                <i class="fa fa-calendar" aria-hidden="true"></i>
                <span class="d-md-none hidden-md hidden-lg">Booking</span>
            </a>
        </li>
    </ul>
</div>
