@php
$menu = [
    [
        'title' => 'Home',
        'href' => route('home'),
        'name' => 'home',
    ],
    [
        'title' => 'About',
        'href' => route('about'),
        'name' => 'about',
    ],
    [
        'title' => 'Services',
        'href' => route('services'),
        'name' => 'services',
    ],
    [
        'title' => 'Booking',
        'href' => route('book'),
        'name' => 'book',
    ],
    [
        'title' => 'Gallery',
        'href' => route('gallery'),
        'name' => 'gallery',
    ],
    [
        'title' => 'Coupons',
        'href' => route('coupons'),
        'name' => 'coupons',
    ],
    [
        'title' => 'GiftCards',
        'href' => route('giftcards'),
        'name' => 'giftcards',
    ],
    [
        'title' => 'Contact',
        'href' => route('contact'),
        'name' => 'contact',
    ],
];
@endphp
<header class="header">
    <section class="main-header">
        <div class="header-top">
            <div class="container">
                <div class="inner-container clearfix">
                    <div class="top-left">
                        <ul class="clearfix">
                            @php
                                $temp = json_decode($header['features']);
                            @endphp
                            @if (count($temp) > 1)
                                <li>
                                    <a href="tel:651-362-0535‬" title="Call Us">
                                        <i class="fa {{ $temp[1]->name }}"></i>
                                        {{ $temp[1]->desc }}
                                    </a>
                                </li>
                            @endif
                            @if (count($temp) > 2)
                                <li>
                                    <a href="mailto:web@fastboy.net" title="Mail Us">
                                        <i class="fa {{ $temp[2]->name }}"></i>
                                        {{ $temp[2]->desc }}
                                    </a>
                                </li>
                            @endif

                        </ul>
                    </div>
                    <div class="top-right">
                        <div class="link-box">
                            <a class="btn_make_appointment" itemprop="url" href="{{ route('book') }}"
                                title="Book Now">Book
                                Now</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="
      wrap-freeze-header-mobile
      hidden-md hidden-lg
      d-block d-lg-none">
            <div class="flag-freeze-header-mobile">
                <div class="menu_mobile_v1">
                    <div class="mobile_logo_container">
                        <div class="mobile_logo">
                            @if (count(json_decode($header['images'])) > 0)
                                <a itemprop="url" href="{{ route('home') }}" title="Logo">
                                    <img class="imgrps"
                                        src="{{ asset('storage') }}/photos/{{ json_decode($header['images'])[0] }}"
                                        alt="Blooming Nails & Spa" itemprop="logo image" />
                                </a>
                            @endif
                        </div>
                    </div>
                    <div class="mobile_menu_container_v1">
                        <!-- Tpl menu mobile 3 layouts -->
                        <div class="mobile-menu-translate">
                            <nav id="mobile_translate">
                                <ul>
                                    <li>
                                        <a itemprop="url" href="{{ route('home') }}" title="Home">Home</a>
                                    </li>
                                    <li>
                                        <a itemprop="url" href="{{ route('about') }}" title="About Us">About Us</a>
                                    </li>
                                    <li>
                                        <a itemprop="url" href="{{ route('services') }}"
                                            title="Services">Services</a>
                                    </li>
                                    <li class="btn_make_appointment">
                                        <a itemprop="url" href="{{ route('book') }}" title="Booking">Booking</a>
                                    </li>
                                    <li>
                                        <a itemprop="url" href="{{ route('gallery') }}" title="Gallery">Gallery</a>
                                    </li>
                                    <li>
                                        <a itemprop="url" href="{{ route('coupons') }}" title="Coupons">Coupons</a>
                                    </li>
                                    <li>
                                        <a itemprop="url" href="{{ route('giftcards') }}"
                                            title="Giftcards">Giftcards</a>
                                    </li>
                                    <li>
                                        <a itemprop="url" href="{{ route('contact') }}" title="Contact Us">Contact
                                            Us</a>
                                    </li>
                                </ul>
                            </nav>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="wrap-freeze-header hidden-xs hidden-sm d-none d-lg-block">
            <div class="flag-freeze-header">
                <div class="header-lower">
                    <div class="container">
                        <div class="main-box clearfix">
                            <div class="logo-box">
                                @if (count(json_decode($header['images'])) > 0)
                                    <div class="logo">
                                        <a itemprop="url" href="{{ route('home') }}" title="Logo">
                                            <img class="imgrps"
                                                src="{{ asset('storage') }}/photos/{{ json_decode($header['images'])[0] }}"
                                                alt="Blooming Nails & Spa" itemprop="logo image" />
                                        </a>
                                    </div>
                                @endif
                            </div>
                            <div class="menu-box nav-outer clearfix">
                                <nav class="main-menu">
                                    <div class="
                                            navbar-collapse
                                            collapse
                                            menu_desktop_v1
                                            clearfix">
                                        <!-- Tpl menu main layouts -->
                                        <ul class="navigation clearfix">
                                            @foreach ($menu as $item)
                                                <li>
                                                    <a itemprop="url" href="{{ $item['href'] }}" class=" @if (Route::current()->getName() === $item['name']) active @endif">{{ $item['title'] }}</a>
                                                </li>
                                            @endforeach
                                        </ul>
                                    </div>
                                </nav>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</header>
