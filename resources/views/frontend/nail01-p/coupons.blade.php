<main class="main">
    <!-- Tpl main coupons -->
    <!--
# Support render.coupon_data.coupons
-->
    <section class="page-title">
        <div class="container">
            <div class="inner-container clearfix">
                <h2 itemprop="name">Coupons</h2>
                <ul class="bread-crumb clearfix">
                    <li><a href="{{ route('home') }}" itemprop="url" title="Home">Home</a></li>
                    <li>Coupons</li>
                </ul>
            </div>
        </div>
    </section>
    <section class="page-content section-padding our-coupons">
        <div class="left-image"><img itemprop="image"
                src="{{ asset('frontend') }}/themes/fnail01p/assets/images/side-img-left.png" alt="side-img-left.png">
        </div>
        <div class="auto-container">
            <h3 class="title">Blooming Nails & Spa</h3>
            <h2 itemprop="name">Our Coupons</h2>
            <div>
                <div class="row">
                    @foreach ($list as $item)
                        <div class="col-xs-12 col-sm-6 col-md-6">
                            <div class="pointer m-magnific-popup" data-group="coupon" title="Special Promotions"
                                href="{{ asset('storage') }}/photos/{{ $item['url'] }}">
                                <div class="m-coupon-box">
                                    <img itemprop="image" src="{{ asset('storage') }}/photos/{{ $item['url'] }}"
                                        alt="Special Promotions">
                                </div>
                            </div>
                        </div>
                    @endforeach
                </div>
                <div class="row">
                    <div class="col-md-12 text-center">
                        <nav aria-label="Page navigation">
                        </nav>
                    </div>
                </div>
            </div>
        </div>
    </section>
</main>
