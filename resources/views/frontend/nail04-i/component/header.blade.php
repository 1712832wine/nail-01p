<header class="header">
    <!-- Tpl header main -->
    <!--
# Support render.menu_mobile.layouts, render.menu_main.layouts, render.logo.layouts
-->
    <div class="
      wrap-freeze-header-mobile
      clearfix
      hidden-md hidden-lg
      menu-1024-hidden
    ">
        <div class="flag-freeze-header-mobile">
            <div class="menu_mobile_v1">
                <div class="mobile_logo">
                    <a itemprop="url" href="/" title="Logo">
                        <img class="imgrps"
                            src="http://fnail04i.fastboywebsite.info/uploads/fnail0zpqm2is/attach/1592191147_logo_fnail04i-logo.png"
                            alt="Luxury Nails & Spa" itemprop="logo image" />
                    </a>
                </div>
                <div class="mobile_menu_container_v1">
                    <div class="mobile-menu clearfix">
                        <nav id="mobile_dropdown">
                            <!-- Tpl menu mobile layouts -->
                            <ul>
                                <li><a itemprop="url" href="/" title="Home">Home</a></li>
                                <li>
                                    <a itemprop="url" href="/about" title="About Us">About Us</a>
                                </li>
                                <li>
                                    <a itemprop="url" href="/services" title="Services">Services</a>
                                </li>
                                <li>
                                    <a itemprop="url" href="/book" class="btn_make_appointment"
                                        title="Booking">Booking</a>
                                </li>
                                <li>
                                    <a itemprop="url" href="/gallery" title="Gallery">Gallery</a>
                                </li>
                                <li>
                                    <a itemprop="url" href="/coupons" title="Coupons">Coupons</a>
                                </li>
                                <li>
                                    <a itemprop="url" href="/giftcards" title="Gift Cards">Gift Cards</a>
                                </li>
                                <li>
                                    <a itemprop="url" href="/contact" title="Contact Us">Contact Us</a>
                                </li>
                            </ul>
                        </nav>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="wrap-freeze-header clearfix hidden-xs hidden-sm">
        <div class="flag-freeze-header">
            <div class="header-main">
                <div class="container">
                    <div class="header-nav dflex-sb">
                        <div class="logo">
                            <a itemprop="url" href="/" title="Logo">
                                <img class="imgrps"
                                    src="http://fnail04i.fastboywebsite.info/uploads/fnail0zpqm2is/attach/1592191147_logo_fnail04i-logo.png"
                                    alt="Luxury Nails & Spa" itemprop="logo image" />
                            </a>
                        </div>
                        <div class="head-nav-right dflex">
                            <nav class="navbar">
                                <div class="navbar-collapse collapse">
                                    <!-- Tpl menu main layouts -->
                                    <ul class="nav navbar-nav">
                                        <li>
                                            <a itemprop="url" href="/" title="Home">Home</a>
                                        </li>
                                        <li>
                                            <a itemprop="url" href="/about" title="About Us">About Us</a>
                                        </li>
                                        <li>
                                            <a itemprop="url" href="/services" title="Services">Services</a>
                                        </li>
                                        <li>
                                            <a itemprop="url" href="/book" class="btn_make_appointment"
                                                title="Booking">Booking</a>
                                        </li>
                                        <li>
                                            <a itemprop="url" href="/gallery" title="Gallery">Gallery</a>
                                        </li>
                                        <li>
                                            <a itemprop="url" href="/coupons" title="Coupons">Coupons</a>
                                        </li>
                                        <li>
                                            <a itemprop="url" href="/giftcards" title="Gift Cards">Gift
                                                Cards</a>
                                        </li>
                                        <li>
                                            <a itemprop="url" href="/contact" title="Contact Us">Contact Us</a>
                                        </li>
                                    </ul>
                                </div>
                            </nav>
                            <div class="header-search"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</header>
