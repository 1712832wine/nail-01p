<main class="main">

  <div class="pmain">
    <div class="page-heading">
      <div class="container">
        <div class="heading-inner">
          <h2>Coupons</h2>
          <ul class="breadcrumbs">
            <li><a href="/" title="Home">Home</a></li>
            <li>Coupons</li>
          </ul>
        </div>
      </div>
    </div>
    <div class="in-container">
      <div class="container">
        <div class="in-content">
          <div class="row">
            @foreach ($list as $item)
                    <div class="col-xs-12 col-sm-6 col-md-6">
                        <div class="pointer m-magnific-popup" data-group="coupon" title="Special Promotions"
                            href="{{ asset('storage') }}/photos/{{ $item['url'] }}">
                            <div class="m-coupon-box">
                                <img itemprop="image" src="{{ asset('storage') }}/photos/{{ $item['url'] }}"
                                    alt="Special Promotions" />
                            </div>
                        </div>
                    </div>
                @endforeach
          </div>
          <div class="row">
            <div class="col-md-12 text-center">
              <nav aria-label="Page navigation"></nav>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</main>
