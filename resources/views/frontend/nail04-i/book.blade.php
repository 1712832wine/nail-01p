<main class="main">

  <div class="pmain">
    <div class="page-heading">
      <div class="container">
        <div class="heading-inner">
          <h2>Booking</h2>
          <ul class="breadcrumbs">
            <li><a href="/" title="Home">Home</a></li>
            <li>Booking</li>
          </ul>
        </div>
      </div>
    </div>
    <div class="in-container">
      <div class="container">
        <div class="in-content">
          <div class="page-title"></div>
          <section id="boxBookingForm" class="box-booking-form">
            <form id="formBooking" class="form-booking" name="formBooking" action="/book/add" method="post"
              enctype="multipart/form-data">
              <div class="row">
                <div class="col-sm-6 col-md-4 col-lg-3 booking-date">
                  <div class="group-select">
                    <label>Date (required)</label>
                    <div class="relative w100 form-input-group">
                      <input type="text" class="form-control form-text booking_date" autocomplete="off"
                        name="booking_date" value="" data-validation="[NOTEMPTY]"
                        data-validation-message="Please choose date" title="Booking date" placeholder=""
                        maxlength="16" />
                      <span class="fa fa-calendar calendar form-icon"></span>
                    </div>
                  </div>
                </div>
              </div>
              <div class="row">
                <div class="col-sm-12 col-md-12 col-lg-9">
                  <div class="row booking-service-staff booking-item" id="bookingItem_0">
                    <div class="col-sm-12 col-md-7 col-lg-8 booking-service">
                      <div class="group-select">
                        <label>Service (required)</label>
                        <div class="relative w100">
                          <select class="form-control booking_service" name="product_id[]" data-validation="[NOTEMPTY]"
                            data-validation-message="Please choose a service" title="Booking service">
                            <option value="">Select service</option>
                          </select>
                        </div>
                      </div>
                    </div>
                    <div class="col-sm-12 col-md-5 col-lg-4 booking-staff">
                      <div class="group-select">
                        <label>Technician (optional)</label>
                        <div class="relative w100">
                          <select class="form-control booking_staff" name="staff_id[]" title="Booking staff">
                            <option value="">Select technician</option>
                          </select>
                        </div>
                      </div>
                    </div>
                  </div>

                  <!-- Button Add Item -->
                  <div class="clearfix">
                    <div class="add-services pointer booking_item_add">
                      <i class="fa fa-plus-circle"></i> Add another service
                    </div>
                  </div>
                </div>
                <div class="col-sm-12 col-md-12 col-lg-3">
                  <div class="group-select">
                    <label>&nbsp;</label>
                    <div class="relative w100">
                      <button class="btn btn-search search_booking" type="button">
                        Search
                      </button>
                    </div>

                    <!-- Hidden data -->
                    <input type="hidden" name="booking_hours" class="booking_hours" value="" />
                    <input type="hidden" name="booking_name" class="booking_name" value="" />
                    <input type="hidden" name="booking_phone" class="booking_phone" value="" />
                    <input type="hidden" name="booking_email" class="booking_email" value="" />
                    <input type="hidden" name="notelist" class="notelist" value="" />
                    <input type="hidden" name="store_id" class="store_id" value="" />

                    <input type="hidden" name="booking_area_code" class="booking_area_code" value="1" />
                    <input type="hidden" name="booking_form_email" class="booking_form_email" value="0" />
                    <input type="hidden" name="nocaptcha" class="nocaptcha" value="1" />
                    <input type="hidden" name="g-recaptcha-response" class="g-recaptcha-response" value="" />
                  </div>
                </div>
              </div>
            </form>

            <script type="text/javascript">
              $(document).ready(function () {
                /* Init Booking */
                webBookingForm.init(
                  '{"70":{"id":70,"name":"Mani Pedi - Pamper Me Spa","services":[197,198]},"71":{"id":71,"name":"Mani Pedi - Delight Spa","services":[199,200]},"72":{"id":72,"name":"Mani Pedi - Spa Mani Pedi","services":[201,202]},"73":{"id":73,"name":"Mani Pedi - Basic Mani Pedi","services":[203,204]},"74":{"id":74,"name":"Nails Enhancements","services":[205,206,207,208,209,210,211,212,213,214,215,216]},"75":{"id":75,"name":"Nails Enhancements - Shellac Gel Polish W\/ Powder","services":[217,218,219,220]},"76":{"id":76,"name":"Nails Enhancements - Silk Wraps","services":[221,222]},"77":{"id":77,"name":"Nails Enhancements - Solar Nails","services":[223,224,225,226,227,228]},"78":{"id":78,"name":"Nails Enhancements - Lume Gel ","services":[229,230,231,232,233,234]},"79":{"id":79,"name":"Nails Enhancements - Gel Dipping ","services":[235,236,237]},"80":{"id":80,"name":"Additional Services   ","services":[238,239,240,241,242,243,244,245,246,247,248,249,250,251,252,253,254,255,256,257,258,259,260,261,262,263,264,265]},"81":{"id":81,"name":"Eyelash Extensions","services":[266,267,268,269,270,271,272]},"82":{"id":82,"name":"Waxing","services":[273,274,275,276,277,278,279,280,281,282,283,284,285,286,287,288,289,290,291]},"83":{"id":83,"name":"Skin Care - Exfoliating Facial","services":[292,293,294,295]}}',
                  '{"197":{"id":197,"name":"Manicure","price":"$30 ","staffs":null,"schedule":false,"unit":"person","scheduleDay":null},"198":{"id":198,"name":"Pedicure","price":"$55 ","staffs":null,"schedule":false,"unit":"person","scheduleDay":null},"199":{"id":199,"name":"Manicure","price":"$25 ","staffs":null,"schedule":false,"unit":"person","scheduleDay":null},"200":{"id":200,"name":"Pedicure","price":"$45 ","staffs":null,"schedule":false,"unit":"person","scheduleDay":null},"201":{"id":201,"name":"Manicure","price":"$20 ","staffs":null,"schedule":false,"unit":"person","scheduleDay":null},"202":{"id":202,"name":"Pedicure","price":"$35 ","staffs":null,"schedule":false,"unit":"person","scheduleDay":null},"203":{"id":203,"name":"Manicure","price":"$15 ","staffs":null,"schedule":false,"unit":"person","scheduleDay":null},"204":{"id":204,"name":"Pedicure","price":"$25 ","staffs":null,"schedule":false,"unit":"person","scheduleDay":null},"205":{"id":205,"name":"Acrylic Full Set","price":"$25 ","staffs":null,"schedule":false,"unit":"person","scheduleDay":null},"206":{"id":206,"name":"Acrylic Refill","price":"$16 ","staffs":null,"schedule":false,"unit":"person","scheduleDay":null},"207":{"id":207,"name":"Acrylic Overlay Full Set","price":"$25 ","staffs":null,"schedule":false,"unit":"person","scheduleDay":null},"208":{"id":208,"name":"Acrylic Overlay Refill","price":"$16 ","staffs":null,"schedule":false,"unit":"person","scheduleDay":null},"209":{"id":209,"name":"White Tip Full Set","price":"$30 ","staffs":null,"schedule":false,"unit":"person","scheduleDay":null},"210":{"id":210,"name":"White Tip Refill","price":"$21 ","staffs":null,"schedule":false,"unit":"person","scheduleDay":null},"211":{"id":211,"name":"Crystal or Pink Powder Full Set","price":"$30 ","staffs":null,"schedule":false,"unit":"person","scheduleDay":null},"212":{"id":212,"name":"Crystal or Pink Powder Refill","price":"$20 ","staffs":null,"schedule":false,"unit":"person","scheduleDay":null},"213":{"id":213,"name":"Sculpture Nails Full Set","price":"$40 ","staffs":null,"schedule":false,"unit":"person","scheduleDay":null},"214":{"id":214,"name":"Sculpture Nails Refill","price":"$20 ","staffs":null,"schedule":false,"unit":"person","scheduleDay":null},"215":{"id":215,"name":"Gel Powder Full Set","price":"$40 ","staffs":null,"schedule":false,"unit":"person","scheduleDay":null},"216":{"id":216,"name":"Gel Powder Refill","price":"$25 ","staffs":null,"schedule":false,"unit":"person","scheduleDay":null},"217":{"id":217,"name":"Gel Powder Full Set","price":"$50 ","staffs":null,"schedule":false,"unit":"person","scheduleDay":null},"218":{"id":218,"name":"Gel Powder Refill","price":"$35 ","staffs":null,"schedule":false,"unit":"person","scheduleDay":null},"219":{"id":219,"name":"Acrylic Powder Full Set","price":"$35 ","staffs":null,"schedule":false,"unit":"person","scheduleDay":null},"220":{"id":220,"name":"Acrylic Powder Refill","price":"$26 ","staffs":null,"schedule":false,"unit":"person","scheduleDay":null},"221":{"id":221,"name":"Silk Wraps Full Set","price":"$40 ","staffs":null,"schedule":false,"unit":"person","scheduleDay":null},"222":{"id":222,"name":"Silk Wraps Refill","price":"$30 ","staffs":null,"schedule":false,"unit":"person","scheduleDay":null},"223":{"id":223,"name":"Pink & White Full Set","price":"$45 ","staffs":null,"schedule":false,"unit":"person","scheduleDay":null},"224":{"id":224,"name":"Pink & White Refill","price":"$30 ","staffs":null,"schedule":false,"unit":"person","scheduleDay":null},"225":{"id":225,"name":"Fading w\/ Glitter Powder Full Set","price":"$50 ","staffs":null,"schedule":false,"unit":"person","scheduleDay":null},"226":{"id":226,"name":"Fading w\/ Glitter Powder Refill","price":"$35 ","staffs":null,"schedule":false,"unit":"person","scheduleDay":null},"227":{"id":227,"name":"Seashell Full Set","price":"$55 ","staffs":null,"schedule":false,"unit":"person","scheduleDay":null},"228":{"id":228,"name":"Seashell Refill","price":"$40 ","staffs":null,"schedule":false,"unit":"person","scheduleDay":null},"229":{"id":229,"name":"Lume Gel Full Set","price":"$40 ","staffs":null,"schedule":false,"unit":"person","scheduleDay":null},"230":{"id":230,"name":"Lume Gel Refill","price":"$25 ","staffs":null,"schedule":false,"unit":"person","scheduleDay":null},"231":{"id":231,"name":"Gel Pink Full Set","price":"$45 ","staffs":null,"schedule":false,"unit":"person","scheduleDay":null},"232":{"id":232,"name":"Gel Pink Refill","price":"$30 ","staffs":null,"schedule":false,"unit":"person","scheduleDay":null},"233":{"id":233,"name":"Gel Pink & White Full Set","price":"$50 ","staffs":null,"schedule":false,"unit":"person","scheduleDay":null},"234":{"id":234,"name":"Gel Pink & White Refill","price":"$35 ","staffs":null,"schedule":false,"unit":"person","scheduleDay":null},"235":{"id":235,"name":"Gel Dipping w\/ Color","price":"$35 ","staffs":null,"schedule":false,"unit":"person","scheduleDay":null},"236":{"id":236,"name":"Gel Dipping Pink & White","price":"$45 ","staffs":null,"schedule":false,"unit":"person","scheduleDay":null},"237":{"id":237,"name":"Ombre Dipping","price":"$45 ","staffs":null,"schedule":false,"unit":"person","scheduleDay":null},"238":{"id":238,"name":"Gel Polish Hands","price":"$15 ","staffs":null,"schedule":false,"unit":"person","scheduleDay":null},"239":{"id":239,"name":"Gel Polish Feet","price":"$20 ","staffs":null,"schedule":false,"unit":"person","scheduleDay":null},"240":{"id":240,"name":"Gel French Tip Change Hands","price":"$20 ","staffs":null,"schedule":false,"unit":"person","scheduleDay":null},"241":{"id":241,"name":"Gel French Tip Change Feet","price":"$25 ","staffs":null,"schedule":false,"unit":"person","scheduleDay":null},"242":{"id":242,"name":"Gel Polish w\/ Services Hands","price":"$10 ","staffs":null,"schedule":false,"unit":"person","scheduleDay":null},"243":{"id":243,"name":"Gel Polish w\/ Services Feet","price":"$15 ","staffs":null,"schedule":false,"unit":"person","scheduleDay":null},"244":{"id":244,"name":"Polish Change Hands\/ Feet","price":"$8 ","staffs":null,"schedule":false,"unit":"person","scheduleDay":null},"245":{"id":245,"name":"French Tip w\/ Services","price":"$5 ","staffs":null,"schedule":false,"unit":"person","scheduleDay":null},"246":{"id":246,"name":"Gel Take Off","price":"$10 ","staffs":null,"schedule":false,"unit":"person","scheduleDay":null},"247":{"id":247,"name":"French Tip Polish Change","price":"$10 ","staffs":null,"schedule":false,"unit":"person","scheduleDay":null},"248":{"id":248,"name":"Acrylic Removal","price":"$10 ","staffs":null,"schedule":false,"unit":"person","scheduleDay":null},"249":{"id":249,"name":"Acrylic Removal with Polish Change","price":"$15 ","staffs":null,"schedule":false,"unit":"person","scheduleDay":null},"250":{"id":250,"name":"Acrylic Cut","price":"$7 ","staffs":null,"schedule":false,"unit":"person","scheduleDay":null},"251":{"id":251,"name":"Acrylic Cut & Polish","price":"$13 ","staffs":null,"schedule":false,"unit":"person","scheduleDay":null},"252":{"id":252,"name":"Callus Treatment","price":"$15 ","staffs":null,"schedule":false,"unit":"person","scheduleDay":null},"253":{"id":253,"name":"Callus Treatment w\/ Services","price":"$8 ","staffs":null,"schedule":false,"unit":"person","scheduleDay":null},"254":{"id":254,"name":"Paraffin Treatment for Hands","price":"$6 ","staffs":null,"schedule":false,"unit":"person","scheduleDay":null},"255":{"id":255,"name":"Paraffin Treatment for Feet","price":"$8 ","staffs":null,"schedule":false,"unit":"person","scheduleDay":null},"256":{"id":256,"name":"Buffing Cream","price":"$5 ","staffs":null,"schedule":false,"unit":"person","scheduleDay":null},"257":{"id":257,"name":"Clear Gel Coat Hands\/ Feet","price":"$5 ","staffs":null,"schedule":false,"unit":"person","scheduleDay":null},"258":{"id":258,"name":"Nail Designs","price":"$3 & Up","staffs":null,"schedule":false,"unit":"person","scheduleDay":null},"259":{"id":259,"name":"Broken Nail","price":"$3 & Up","staffs":null,"schedule":false,"unit":"person","scheduleDay":null},"260":{"id":260,"name":"Foot Massage (15&#39;)","price":"$15 ","staffs":null,"schedule":false,"unit":"person","scheduleDay":null},"261":{"id":261,"name":"Massage w\/ Services (1&#39;)","price":"$1 & Up","staffs":null,"schedule":false,"unit":"person","scheduleDay":null},"262":{"id":262,"name":"Natural Nails Cut","price":"$5 ","staffs":null,"schedule":false,"unit":"person","scheduleDay":null},"263":{"id":263,"name":"Natural Toe Cut","price":"$7 ","staffs":null,"schedule":false,"unit":"person","scheduleDay":null},"264":{"id":264,"name":"Nails Cut & Polish Hands","price":"$10 ","staffs":null,"schedule":false,"unit":"person","scheduleDay":null},"265":{"id":265,"name":"Nails Cut & Polish Feet","price":"$12 ","staffs":null,"schedule":false,"unit":"person","scheduleDay":null},"266":{"id":266,"name":"Full Set (one hair by one)","price":"$100 & Up","staffs":null,"schedule":false,"unit":"person","scheduleDay":null},"267":{"id":267,"name":"Fill (1 to 10 single hair refills)","price":"$55 & Up","staffs":null,"schedule":false,"unit":"person","scheduleDay":null},"268":{"id":268,"name":"Strip Eyelash Extension","price":"$35 ","staffs":null,"schedule":false,"unit":"person","scheduleDay":null},"269":{"id":269,"name":"Full Set Flare Eyelashes","price":"$50 ","staffs":null,"schedule":false,"unit":"person","scheduleDay":null},"270":{"id":270,"name":"Full Set Single","price":"$55 & Up","staffs":null,"schedule":false,"unit":"person","scheduleDay":null},"271":{"id":271,"name":"Fill Flare Flare","price":"$25 & Up","staffs":null,"schedule":false,"unit":"person","scheduleDay":null},"272":{"id":272,"name":"Remove Eyelashes","price":"$20 ","staffs":null,"schedule":false,"unit":"person","scheduleDay":null},"273":{"id":273,"name":"Eyebrows","price":"$10 ","staffs":null,"schedule":false,"unit":"person","scheduleDay":null},"274":{"id":274,"name":"Lip","price":"$7 ","staffs":null,"schedule":false,"unit":"person","scheduleDay":null},"275":{"id":275,"name":"Chin","price":"$8 ","staffs":null,"schedule":false,"unit":"person","scheduleDay":null},"276":{"id":276,"name":"Sideburns","price":"$18 ","staffs":null,"schedule":false,"unit":"person","scheduleDay":null},"277":{"id":277,"name":"Forehead","price":"$15 ","staffs":null,"schedule":false,"unit":"person","scheduleDay":null},"278":{"id":278,"name":"Full Face","price":"$40 ","staffs":null,"schedule":false,"unit":"person","scheduleDay":null},"279":{"id":279,"name":"Underarm","price":"$18 ","staffs":null,"schedule":false,"unit":"person","scheduleDay":null},"280":{"id":280,"name":"Half Arm","price":"$25 ","staffs":null,"schedule":false,"unit":"person","scheduleDay":null},"281":{"id":281,"name":"Full Arm","price":"$40 & Up","staffs":null,"schedule":false,"unit":"person","scheduleDay":null},"282":{"id":282,"name":"Half Leg","price":"$35 ","staffs":null,"schedule":false,"unit":"person","scheduleDay":null},"283":{"id":283,"name":"Full Leg","price":"$50 & Up","staffs":null,"schedule":false,"unit":"person","scheduleDay":null},"284":{"id":284,"name":"Stomach","price":"$18 ","staffs":null,"schedule":false,"unit":"person","scheduleDay":null},"285":{"id":285,"name":"Bikini","price":"$35 ","staffs":null,"schedule":false,"unit":"person","scheduleDay":null},"286":{"id":286,"name":"Chest","price":"$40 & Up","staffs":null,"schedule":false,"unit":"person","scheduleDay":null},"287":{"id":287,"name":"Back","price":"$40 & Up","staffs":null,"schedule":false,"unit":"person","scheduleDay":null},"288":{"id":288,"name":"Back & Shoulder","price":"$50 & Up","staffs":null,"schedule":false,"unit":"person","scheduleDay":null},"289":{"id":289,"name":"Brazilian","price":"$45 & Up","staffs":null,"schedule":false,"unit":"person","scheduleDay":null},"290":{"id":290,"name":"Toes","price":"$10 ","staffs":null,"schedule":false,"unit":"person","scheduleDay":null},"291":{"id":291,"name":"Neck","price":"$20 ","staffs":null,"schedule":false,"unit":"person","scheduleDay":null},"292":{"id":292,"name":"Cleanser","price":"$30 ","staffs":null,"schedule":false,"unit":"person","scheduleDay":null},"293":{"id":293,"name":"Peeling Gel","price":"$30 ","staffs":null,"schedule":false,"unit":"person","scheduleDay":null},"294":{"id":294,"name":"Toner","price":"$30 ","staffs":null,"schedule":false,"unit":"person","scheduleDay":null},"295":{"id":295,"name":"Moisturizer","price":"$30 ","staffs":null,"schedule":false,"unit":"person","scheduleDay":null}}',
                  "null",
                  '{"listperson":[],"notelist":[],"booking_date":"05\/24\/2021","booking_hours":""}'
                );
              });
            </script>
          </section>

          <section id="boxBookingInfo" class="box-booking-info relative" style="display: none">
            <h3 class="booking-info-title">Appointment Information</h3>

            <!-- Service, Staff -->
            <div id="boxServiceStaff" class="box-service-staff">
              <div class="service-staff">
                <div class="service-staff-avatar">
                  <img class="img-responsive" src="/public/library/global/no-photo.jpg" alt="Any person" />
                </div>
                <div class="service-staff-info">
                  <h5>Any Technician</h5>
                  <p>Any Service</p>
                  <p>Price: N/A</p>
                </div>
              </div>
              <div class="service-staff">
                <div class="service-staff-avatar no-photo">
                  <img class="img-responsive" src="/public/library/global/no-photo.jpg" alt="Any person" />
                </div>
                <div class="service-staff-info">
                  <h5>Any Technician</h5>
                  <p>Any Service</p>
                  <p>Price: N/A</p>
                </div>
              </div>
            </div>

            <!-- Date, Time List -->
            <div id="boxDateTime" class="box-date-time">
              <h4 class="date-info" id="dateInfo">Sunday, Jan-01-1970</h4>
              <div class="time-info">
                <h5>
                  Morning <span class="time-note" id="timeAMNote">N/A</span>
                </h5>
                <ul class="time-items" id="timeAMHtml"></ul>
              </div>
              <div class="time-info">
                <h5>
                  Afternoon
                  <span class="time-note" id="timePMNote">N/A</span>
                </h5>
                <ul class="time-items" id="timePMHtml"></ul>
              </div>
            </div>
          </section>

          <section id="popupBookingConfirm" class="popup-booking-confirm white-popup mfp-hide border-style">
            <section id="boxBookingConfirm" class="box-booking-confirm relative">
              <h3 class="booking-confirm-title">
                Confirm booking information ?
              </h3>
              <div class="booking-confirm-note">
                We will send a text message to you via the number below
                after we confirm the calendar for your booking.
              </div>
              <script src="https://www.google.com/recaptcha/api.js?hl=en" async defer></script>
              <!-- Google reCaptcha -->
              <script type="text/javascript">
                function ezyCaptcha_formBookingConfirm(token, is_submit) {
                  is_submit = 1;
                  if ($("#password").length) {
                    //$("input:password").val(md5(clean_input($("#password").val())));
                  }
                  return true;
                }
              </script>
              <form id="formBookingConfirm" class="form-booking-confirm" name="formBookingConfirm" method="post"
                action="/book" enctype="multipart/form-data">
                <div class="row">
                  <div class="col-md-6 booking-name">
                    <div class="group-select">
                      <label>Your name (required)</label>
                      <div class="relative w100">
                        <input type="text" class="form-control" name="booking_name" value=""
                          data-validation="[NOTEMPTY]" data-validation-message="Please enter your name" placeholder=""
                          maxlength="76" title="Booking name" />
                      </div>
                    </div>
                  </div>
                  <div class="col-md-6 booking-phone">
                    <div class="group-select">
                      <label>Your phone (required)</label>
                      <div class="relative w100">
                        <input type="text" class="form-control inputPhone" pattern="\d*" name="booking_phone"
                          data-validation="[NOTEMPTY]" data-validation-message="Please enter your phone" placeholder=""
                          maxlength="16" title="Booking phone" />
                      </div>
                    </div>
                  </div>
                  <div class="col-md-12 booking-email">
                    <div style="display: none" class="group-select">
                      <label>Your email (optional)</label>
                      <div class="relative w100">
                        <input type="text" class="form-control" name="booking_email" value="" placeholder=""
                          maxlength="76" title="Booking email" />
                      </div>
                    </div>
                  </div>
                  <div class="col-md-12 booking-notelist">
                    <div class="group-select">
                      <label>Note (optional)</label>
                      <div class="relative w100">
                        <textarea class="form-control" rows="5" name="notelist" placeholder="(Max length 200 character)"
                          maxlength="201" title="Booking note"></textarea>
                      </div>
                    </div>
                  </div>
                  <div class="col-md-12 booking-store">
                    <input type="hidden" name="choose_store" class="booking_store" value="0" />
                  </div>
                </div>
                <div class="row">
                  <div class="col-md-8 order-md-2 col-md-push-4">
                    <button class="btn btn-confirm btn_confirm" type="button">
                      Confirm
                    </button>
                  </div>
                  <div class="col-md-4 order-md-1 col-md-pull-8">
                    <button class="btn btn-cancel btn_cancel" type="button">
                      Cancel
                    </button>
                  </div>
                </div>
              </form>
            </section>
          </section>
        </div>
      </div>
    </div>
  </div>
</main>