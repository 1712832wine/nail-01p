<main class="main">

  <div class="pmain">
    <div class="page-heading">
      <div class="container">
        <div class="heading-inner">
          <h2>Gallery</h2>
          <ul class="breadcrumbs">
            <li><a href="/" title="Home">Home</a></li>
            <li>Gallery</li>
          </ul>
        </div>
      </div>
    </div>
    <div class="in-container">
      <div class="container">
        <div class="in-content">
          <ul class="clearfix m-category-tab" id="category_tab">
            @foreach ($albums as $index => $album)
                    <li class="tab @if ($index===0) active @endif"
                        data-id="{{ $album['id'] }}">
                        <span itemprop="name">{{ $album['name'] }}</span>
                    </li>
                @endforeach
          </ul>
          <div class="clearfix m-gallery-content" id="gallery_content">
            <div class="clearfix m-gallery-listing listing"></div>
            <div class="clearfix m-gallery-paging paging"></div>
          </div>
        </div>
      </div>
    </div>
  </div>
</main>
