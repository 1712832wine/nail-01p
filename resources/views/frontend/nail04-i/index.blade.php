<main class="main">

  <div class="pmain">
    <div class="section-slider-wrap">
      <section class="section-slider">
        <div class="slider-width-height" style="
                display: inline-block;
                width: 100%;
                height: 1px;
                overflow: hidden;
              ">
          <!-- SLIDER OPTION -->
          <!--
## autoplay: true/ false
# Indicates whether or not autoplay will be enabled.
# Default value: true
#
## autoplayDelay: number
# Sets the delay/interval (in milliseconds) at which the autoplay will run.
# Default value: 5000
-->
          <div id="slider-option" data-autoplay="true" data-autoplayDelay="5000"></div>
          <!-- END SLIDER OPTION -->

          <!-- USE FOR CALCULATOR START WIDTH HEIGHT -->
          <!--
            custom height with css:
            .slider-width-height .fixed {
                height: 0px; /* - set 0 if auto height, set numbers greater than zero if fixed height - */
            }
            -->

          <div class="fixed" style="width: 100%"></div>
          <img src="http://fnail04i.fastboywebsite.info/uploads/fnail0zpqm2is/logo/2020/06/14/fnail04i-slider-3.jpg"
            style="width: 100%; height: auto" alt="" />
        </div>
        <!-- END USE FOR CALCULATOR START WIDTH HEIGHT -->

        <div class="slider-pro" id="my-slider" style="display: none">
          <div class="sp-slides">
            <div class="sp-slide">
              <div class="sp-layer sp-static">
                <a href="/" target="_self" title="s44.jpg">
                  <img class="sp-image"
                    src="http://fnail04i.fastboywebsite.info/uploads/fnail0zpqm2is/logo/2020/06/14/fnail04i-slider-3.jpg"
                    alt="s44.jpg" />
                </a>
              </div>
            </div>
            <div class="sp-slide">
              <div class="sp-layer sp-static">
                <a href="/" target="_self" title="s23.jpg">
                  <img class="sp-image"
                    src="http://fnail04i.fastboywebsite.info/uploads/fnail0zpqm2is/logo/2020/06/14/fnail04i-slider-1.jpg"
                    alt="s23.jpg" />
                </a>
              </div>
            </div>
            <div class="sp-slide">
              <div class="sp-layer sp-static">
                <a href="/" target="_self" title="s14.jpg">
                  <img class="sp-image"
                    src="http://fnail04i.fastboywebsite.info/uploads/fnail0zpqm2is/logo/2020/06/14/fnail04i-slider-2.jpg"
                    alt="s14.jpg" />
                </a>
              </div>
            </div>
          </div>
        </div>
        <div class="slider-pro" id="my-slider-fixed-height" style="display: none">
          <div class="sp-slides">
            <div class="sp-slide">
              <a href="/" target="_self" title="s44.jpg">
                <div class="sp-layer sp-static" data-width="100%" data-height="100%" style="
                        width: 100%;
                        height: 100%;
                        background: url('http://fnail04i.fastboywebsite.info/uploads/fnail0zpqm2is/logo/2020/06/14/fnail04i-slider-3.jpg')
                          center center no-repeat;
                        background-size: cover;
                      "></div>
              </a>
            </div>
            <div class="sp-slide">
              <a href="/" target="_self" title="s23.jpg">
                <div class="sp-layer sp-static" data-width="100%" data-height="100%" style="
                        width: 100%;
                        height: 100%;
                        background: url('http://fnail04i.fastboywebsite.info/uploads/fnail0zpqm2is/logo/2020/06/14/fnail04i-slider-1.jpg')
                          center center no-repeat;
                        background-size: cover;
                      "></div>
              </a>
            </div>
            <div class="sp-slide">
              <a href="/" target="_self" title="s14.jpg">
                <div class="sp-layer sp-static" data-width="100%" data-height="100%" style="
                        width: 100%;
                        height: 100%;
                        background: url('http://fnail04i.fastboywebsite.info/uploads/fnail0zpqm2is/logo/2020/06/14/fnail04i-slider-2.jpg')
                          center center no-repeat;
                        background-size: cover;
                      "></div>
              </a>
            </div>
          </div>
        </div>
      </section>
    </div>
    <!-- Tpl introduce board -->
    <section class="section section-about">
      <div class="container">
        <img src="/uploads/fnail0zpqm2is/filemanager/about-png.png" alt="" /><br />
        <div class="section-heading text-center">
          <h3>WELCOME TO <br />LUXURY NAILS & SPA</h3>
        </div>
        <div class="section-content">
          <p>
            Located conveniently in Greenville, NC 27858, Our philosophy and
            mission are to ensure that customers are happy when they come
            and satisfied when they leave.
          </p>
          <p>
            Our goal at Luxury Nails & Spa is to make you feel good about
            choosing us. We offer exceptional customer service in a
            wonderfully warm and inviting atmosphere. Each treatment is
            tailored especially to the client’s individual needs. You will
            experience a unique atmosphere where you can relax and unwind
            with friends.
          </p>
          <a class="btn-short" href="/about.html" title="our story">About Us</a>
        </div>
      </div>
    </section>
    <!-- Tpl achievement board -->
    <section class="section-count">
      <div class="list-count">
        <div class="item">
          <div class="icon">
            <i class="fa fa-check" aria-hidden="true"></i>
          </div>
          <div class="number"><span>We have the best therapies</span></div>
          <h3>
            Nails Spa is a first-class Relaxation and Beauty Nails Spa that
            promotes comfort, beauty, well-being, and health.
          </h3>
        </div>
        <div class="item">
          <div class="icon">
            <i class="fa fa-users" aria-hidden="true"></i>
          </div>
          <div class="number"><span>Customer are happy with us</span></div>
          <h3>
            We use ONLY the most trusted brands in the beauty industry. Our
            world-class products, services, and top technologies.
          </h3>
        </div>
        <div class="item">
          <div class="icon">
            <i class="fa fa-calendar" aria-hidden="true"></i>
          </div>
          <div class="number"><span>Highly qualified staff</span></div>
          <h3>
            Our staff was trained to follow a proper sanitization protocol
            that puts our customer’s cleanliness as the number one priority.
          </h3>
        </div>
      </div>
    </section>
    <section class="section-scroll block-location">
      <div class="container-fluid">
        <div class="row">
            @foreach ($services as $item)
            <div class="col-md-3 col-sm-6">
                <div class="item-location">
                  <div class="bg-location" style="
                          background-image: url(&#39;images/nailservice-picture-1.jpg&#39;);
                        "></div>
                  <div class="content-location">
                    <h3>{{$item['name']}}</h3>
                  </div>
                </div>
              </div>
            @endforeach
        </div>
      </div>
    </section>
    <!-- Tpl gallery board -->
    <!--
# Support render.gallery_data.board
-->
    <section class="section section-gallery">
      <div class="container">
        <div class="section-heading text-center">
          <h3 class="m-heading">OUR PHOTOS</h3>
        </div>
        <div class="section-content none-border">
          <div class="m-gallery-box-wrap">
            <div class="row">
                @foreach ($gallery_list as $item)
                <div class="col-6 col-sm-4 col-md-3">
                    <div class="pointer m-magnific-popup"
                        href="{{ asset('storage') }}/photos/{{ $item['url'] }}');">
                        <div class="m-gallery-box">
                            <div class="m-image-bg"
                                style="background-image: url('{{ asset('storage') }}/photos/{{ $item['url'] }}');">
                                <img itemprop="image"
                                    src="{{ asset('storage') }}/photos/{{ $item['url'] }}" />
                            </div>
                        </div>
                    </div>
                </div>
            @endforeach
            </div>
          </div>
        </div>
      </div>
    </section>
    <!-- Tpl video board -->
    <section class="section section-video">
      <div class="container">
        <div class="section-heading text-center">
          <h3 class="m-heading">OUR VIDEOS</h3>
        </div>
        <div class="section-content">
          <div class="row">
            <div class="col-md-8 col-sm-10 col-md-push-2 col-sm-push-1">
              <div class="youtube-player">
                <iframe title="youtube" width="100%" height="100%" src="https://www.youtube.com/embed/1V1qj9IQwuI?rel=0"
                  frameborder="0" allowfullscreen="allowfullscreen"></iframe>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
    <!-- Tpl service board -->
    <!--# Support render.service_data.board, render.service_data2.board-->
    <section class="section section-gallery">
      <div class="container">
        <div class="section-heading text-center">
          <h3 class="m-heading">OUR SERVICES</h3>
        </div>
        <div class="section-content none-border">
          <div class="row">
            <div class="col-sm-6">
                @php
                    $i=1;
                @endphp
              <div class="faq-collapse show-one">
                  @foreach ($services as $service)
                  <div class="card">
                    <div class="card-header">
                      <h5 class="pointer icon-start-line collapsed" data-toggle="collapse" data-target="#collapse-{{$i}}"
                        aria-expanded="false">
                        <span>{{$service['name']}}</span>
                        <span class="icon"><i class="fa fa-angle-down icon-normal"></i><i
                            class="fa fa-angle-up icon-expanded"></i></span>
                      </h5>
                    </div>
                    <div id="collapse-{{$i}}" class="collapse" aria-expanded="false" style="height: 0px">
                      <div class="card-body">
                        <div class="clearfix service-list home">
                          <div class="service-desc"></div>

                          <!-- Service sub -->
                          @foreach (json_decode($service['features']) as $item)
                          <div class="clearfix service-list-sub" id="sci_7{{$i}}">
                            <h2 class="service-name" id=""></h2>
                            <div class="service-desc"></div>
                            <div class="detail-price-item">
                              <span class="detail-price-name">{{$item->name}}</span>
                              <span class="detail-price-dots"></span>
                              <span class="detail-price-number">
                                <span class="current">{{$item->desc}}</span>
                              </span>
                            </div>
                          </div>
                           @endforeach

                        </div>
                      </div>
                    </div>
                  </div>
                  @php
                      $i=$i+1;
                  @endphp
                  @endforeach

              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
    <!-- Tpl testimonial board -->
    <section class="section section-review">
      <div class="container">
        <div class="review-inner">
          <h3>What Our Clients Are Saying</h3>
          <div class="hr-short">
            <span class="hr-inner"><span class="hr-inner-style"></span></span>
          </div>
          <div class="review-list owl-carousel">
              @foreach ($carousel_customers as $item)
              <div class="item">
                <div class="content">
                  {!! $item['content'] !!}
                </div>
                <div class="review-name">
                  <h4></h4>
                  <span>Carmel Salon</span> –
                  <a href="/" target="_blank">{{$item['title']}} Review</a>
                </div>
              </div>
              @endforeach
          </div>
        </div>
      </div>
    </section>
  </div>
</main>
