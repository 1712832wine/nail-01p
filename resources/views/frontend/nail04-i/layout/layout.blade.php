<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" dir="ltr" lang="en" xmlns:og="http://ogp.me/ns#"
    xmlns:fb="http://www.facebook.com/2008/fbml" itemscope itemtype="https://schema.org/NailSalon">

<head>
    <!-- Language -->
    <meta http-equiv="content-language" content="en" />
    <link rel="alternate" href="http://fnail04i.fastboywebsite.info" hreflang="x-default" />
    <link rel="alternate" href="http://fnail04i.fastboywebsite.info" hreflang="en-US" />

    <!-- Meta limit -->

    <!-- Meta Tags -->
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width,initial-scale=1.0" />
    <meta http-equiv="content-type" content="text/html; charset=UTF-8" />
    <meta name="description" content="" />
    <meta name="keywords" content="" />
    <meta name="author" content="" />

    <!-- OG -->
    <meta property="og:title" content="" />
    <meta property="og:description" content="" />
    <meta property="og:image" content="" />
    <meta property="og:url" content="http://fnail04i.fastboywebsite.info/" />
    <meta property="og:type" content="" />
    <meta property="og:site_name" content="Fnail04I Luxury Nails & Spa" />

    <!-- Dublin Core -->
    <link rel="schema.DC" href="http://purl.org/dc/elements/1.1/" />
    <meta name="DC.title" content="" />
    <meta name="DC.identifier" content="http://fnail04i.fastboywebsite.info/" />
    <meta name="DC.description" content="" />
    <meta name="DC.subject" content="" />
    <meta name="DC.language" scheme="UTF-8" content="en-us" />
    <meta itemprop="priceRange" name="priceRange" content="" />

    <!-- GEO meta -->
    <meta name="geo.region" content="" />
    <meta name="geo.placename" content="" />
    <meta name="geo.position" content="" />
    <meta name="ICBM" content="" />

    <!-- Page Title -->
    <title>Fnail04I Luxury Nails & Spa</title>
    <base href="{{ asset('frontend') }}/themes/fnail04i/assets/" />

    <!-- canonical -->
    <link rel="canonical" href="http://fnail04i.fastboywebsite.info" />

    <!-- Favicons -->
    <link rel="icon" href="http://fnail04i.fastboywebsite.info/uploads/fnail0zpqm2is/attach/1575257757_fbm_fvc.png"
        type="image/png" />
    <link rel="shortcut icon"
        href="http://fnail04i.fastboywebsite.info/uploads/fnail0zpqm2is/attach/1575257757_fbm_fvc.png"
        type="image/png" />
    <link rel="apple-touch-icon"
        href="http://fnail04i.fastboywebsite.info/uploads/fnail0zpqm2is/attach/1575257757_fbm_fvc.png"
        type="image/png" />

    <!-- CSS -->
    <link rel="stylesheet" type="text/css" href="{{ asset('frontend') }}/public/library/jquery-ui/jquery-ui.min.css" />
    <link rel="stylesheet" type="text/css" href="{{ asset('frontend') }}/public/library/validation/validation.css" />
    <link rel="stylesheet" type="text/css" href="{{ asset('frontend') }}/public/library/animate/animate.css" />
    <link rel="stylesheet" type="text/css"
        href="{{ asset('frontend') }}/public/library/font-awesome/v4.7.0/css/font-awesome.min.css" />
    <link rel="stylesheet" type="text/css"
        href="{{ asset('frontend') }}/public/library/bootstrap/v3.3.7/bootstrap.min.css" />
    <link rel="stylesheet" type="text/css"
        href="{{ asset('frontend') }}/public/library/bootstrap-datetimepicker/bootstrap-datetimepicker.min.css" />
    <link rel="stylesheet" type="text/css" href="{{ asset('frontend') }}/public/library/meanmenu/meanmenu.css" />
    <link rel="stylesheet" type="text/css"
        href="{{ asset('frontend') }}/public/library/slider-pro/slider-pro.min.css" />
    <link rel="stylesheet" type="text/css"
        href="{{ asset('frontend') }}/public/library/owl-carousel/owl.carousel.min.css" />
    <link rel="stylesheet" type="text/css"
        href="{{ asset('frontend') }}/public/library/magnific-popup/magnific-popup.css" />
    <link rel="stylesheet" type="text/css" href="{{ asset('frontend') }}/public/library/instagram/instafeed.min.css" />
    <link rel="stylesheet" type="text/css" href="{{ asset('frontend') }}/public/library/signature/signature.css" />
    <link rel="stylesheet" type="text/css" href="{{ asset('frontend') }}/public/library/pnotify/pnotify.css" />
    <link rel="stylesheet" type="text/css" href="{{ asset('frontend') }}/public/library/popover/popover.css" />

    <link rel="stylesheet" type="text/css" href="{{ asset('frontend') }}/themes/fnail04i/assets/css/fonts.css" />
    <link rel="stylesheet" type="text/css" href="{{ asset('frontend') }}/themes/fnail04i/assets/css/style.css" />
    <link rel="stylesheet" type="text/css" href="{{ asset('frontend') }}/themes/fnail04i/assets/css/responsive.css" />
    <link rel="stylesheet" type="text/css" href="{{ asset('frontend') }}/public/library/global/css/global.css" />
    <link rel="stylesheet" type="text/css" href="{{ asset('frontend') }}/public/library/global/css/news.css" />
    <link rel="stylesheet" type="text/css" href="{{ asset('frontend') }}/public/library/global/css/giftcards.css" />
    <link rel="stylesheet" type="text/css" href="{{ asset('frontend') }}/public/library/global/css/cart-payment.css" />
    <link rel="stylesheet" type="text/css" href="{{ asset('frontend') }}/public/library/global/css/consent.css" />
    <link rel="stylesheet" type="text/css"
        href="{{ asset('frontend') }}/themes/fnail04i/assets/custom/css/custom.css?v=1.2" />
    <link rel="stylesheet" type="text/css"
        href="{{ asset('frontend') }}/themes/fnail04i/assets/custom/css/custom-2.css" />

    <!-- CSS EXTEND -->
    <style type="text/css">
        .header-main {
            padding: 0px 0;
        }

        .freeze-header.with-bg {
            background-color: #000000;
            -webkit-box-shadow: 0px 1px 1px #eee;
            -moz-box-shadow: 0px 1px 1px #eee;
            box-shadow: 0px 1px 1px #333;
        }

        .section-count .item {
            background-color: #00000075;
        }

        .section-count .item {
            border-right: 1px solid #060606;
            border-bottom: 1px solid #000;
        }

        .item-location .content-location {
            background-color: #000000ba;
            border-right: #000 1px solid;
            border-bottom: #000 1px solid;
        }

        .btn,
        .btn-main,
        .btn-short,
        .header-main.sticky,
        .header-page .header-main,
        .menu_mobile_v1 {
            background: #000000;
            font-size: 16px;
        }

        .section-count .item h3 {
            font-size: 18px;
            color: #fff;
            font-weight: 300;
        }

        .section-count .item .number span {
            border-bottom: 0;
            display: inline-block;
            font-size: 26px;
            font-family: "JosefinSlab";
        }

        .contact-hours,
        .page-heading,
        .footer-main {
            background: #1d2129;
        }

        .section-about .section-heading {
            border-bottom: 0;
        }

        .section-about .section-content {
            border-bottom: 0;
        }

        .section-about {
            padding: 70px 0 30px 0;
        }

        .section-video {
            background-color: #fafafa;
        }

        .section-service {
            text-align: center;
        }

        .section-service h5 {
            color: #efd02f;
            font-weight: 300;
        }

        .faq-collapse .card-header {
            background-color: #000000;
            padding: 15px 15px;
            border-radius: 0;
        }

        .card-header h5 {
            color: #fff;
        }

        .section-review:before {
            background-color: rgba(29, 33, 41, 0.86);
        }

        body:not(.site-idx) .header-main {
            background: #000000;
        }

        .sms-booking #btn_search_booking .btn-search {
            background-color: #000000;
        }

        .mean-container a.meanmenu-reveal {
            border: 1px solid #fff;
            border-radius: 0px;
        }

        .mean-container a.meanmenu-reveal>span {
            background: #fff;
        }

        .mean-container a.meanmenu-reveal>span:before,
        .mean-container a.meanmenu-reveal>span:after {
            background: #fff;
        }

        .section-count {
            background-image: none;
            background-color: #fff;
        }

        .section-review {
            background-image: url(/uploads/fnail0zpqm2is/filemanager/dreamstime_xl_9293092.jpg);
        }

    </style>

    <!-- JS -->
    <script type="text/javascript" src="{{ asset('frontend') }}/public/library/jquery/v3.3.1/jquery.min.js"></script>
    <script type="text/javascript" src="{{ asset('frontend') }}/public/library/jquery-ui/jquery-ui.min.js"></script>
    <script type="text/javascript" src="{{ asset('frontend') }}/public/library/jquery-ui/jquery-migrate.min.js">
    </script>
    <script type="text/javascript" src="{{ asset('frontend') }}/public/library/validation/validation.min.js">
    </script>
    <script type="text/javascript" src="{{ asset('frontend') }}/public/library/mask/mask.min.js"></script>
    <script type="text/javascript" src="{{ asset('frontend') }}/public/library/bootstrap/v3.3.7/bootstrap.min.js">
    </script>
    <script type="text/javascript"
        src="{{ asset('frontend') }}/public/library/waiting-modal-dialog/waiting-modal-dialog.js"></script>
    <script type="text/javascript" src="{{ asset('frontend') }}/public/library/moment/moment-with-locales.js">
    </script>
    <script type="text/javascript"
        src="{{ asset('frontend') }}/public/library/bootstrap-datetimepicker/bootstrap-datetimepicker.js"></script>
    <script type="text/javascript" src="{{ asset('frontend') }}/public/library/meanmenu/meanmenu.js"></script>
    <script type="text/javascript" src="{{ asset('frontend') }}/public/library/slider-pro/slider-pro.min.js">
    </script>
    <script type="text/javascript" src="{{ asset('frontend') }}/public/library/owl-carousel/owl.carousel.min.js">
    </script>
    <script type="text/javascript" src="{{ asset('frontend') }}/public/library/magnific-popup/magnific-popup.min.js">
    </script>
    <script type="text/javascript" src="{{ asset('frontend') }}/public/library/instagram/instafeed.min.js"></script>
    <script type="text/javascript" src="{{ asset('frontend') }}/public/library/signature/signature.min.js"></script>
    <script type="text/javascript" src="{{ asset('frontend') }}/public/library/pnotify/pnotify.js"></script>
    <script type="text/javascript" src="{{ asset('frontend') }}/public/library/popover/popover.js"></script>
    <script type="text/javascript" src="{{ asset('frontend') }}/public/library/global/js/js_php.js"></script>
    <script type="text/javascript" src="{{ asset('frontend') }}/public/library/global/js/global.js"></script>

    <script type="text/javascript">
        let webForm = {
            required: "(required)",
            optional: "(optional)",
            any_person: "Any person",
            price: "Price",
            morning: "Morning",
            afternoon: "Afternoon",
            sunday: "Sunday",
            monday: "Monday",
            tuesday: "Tuesday",
            wednesday: "Wednesday",
            thursday: "Thursday",
            friday: "Friday",
            saturday: "Saturday",
            jan: "Jan",
            feb: "Feb",
            mar: "Mar",
            apr: "Apr",
            may: "May",
            jun: "Jun",
            jul: "Jul",
            aug: "Aug",
            sep: "Sep",
            oct: "Oct",
            nov: "Nov",
            dec: "Dec",
            contact_name: "Your name",
            contact_name_placeholder: "",
            contact_name_maxlength: "76",
            contact_email: "Your email address",
            contact_email_placeholder: "",
            contact_email_maxlength: "76",
            contact_phone: "Your phone",
            contact_phone_placeholder: "",
            contact_phone_maxlength: "16",
            contact_subject: "Your subject",
            contact_subject_placeholder: "",
            contact_subject_maxlength: "251",
            contact_message: "Your message",
            contact_message_placeholder: "",
            contact_message_maxlength: "501",
            contact_btn_send: "Send Us",
            contact_name_err: "Please enter your name",
            contact_email_err: "Please enter your email",
            contact_phone_err: "Please enter your phone",
            contact_subject_err: "Please enter your subject",
            contact_message_err: "Please enter your message",
            booking_name: "Your name",
            booking_name_placeholder: "",
            booking_name_maxlength: "76",
            booking_phone: "Your phone",
            booking_phone_placeholder: "",
            booking_phone_maxlength: "16",
            booking_email: "Your email",
            booking_email_placeholder: "",
            booking_email_maxlength: "76",
            booking_service: "Service",
            booking_service_placeholder: "Select service",
            booking_menu: "Menu",
            booking_menu_placeholder: "Select menu",
            booking_technician: "Technician",
            booking_technician_placeholder: "Select technician",
            booking_person_number: "Number",
            booking_date: "Date",
            booking_date_placeholder: "",
            booking_date_maxlength: "16",
            booking_hours: "Hour",
            booking_hours_placeholder: "Select hour",
            booking_note: "Note",
            booking_note_maxlength: "201",
            booking_note_placeholder: "(Max length 200 character)",
            booking_store: "Storefront",
            booking_store_placeholder: "Select storefront",
            booking_add_another_service: "Add another service",
            booking_information: "Appointment Information",
            booking_order_information: "Order Information",
            booking_popup_message: "Message",
            booking_popup_confirm: "Confirm booking information ?",
            booking_popup_confirm_description: "We will send a text message to you via the number below after we confirm the calendar for your booking.",
            booking_order_popup_confirm: "Confirm order information ?",
            booking_order_popup_confirm_description: "We will send a text message to you via the number below after we confirm the calendar for your order.",
            booking_btn_send: "Send appointment now",
            booking_btn_search: "Search",
            booking_btn_booking: "Booking",
            booking_btn_confirm: "Confirm",
            booking_btn_cancel: "Cancel",
            booking_hours_expired: "Has expired",
            booking_name_err: "Please enter your name",
            booking_phone_err: "Please enter your phone",
            booking_email_err: "Please enter your email",
            booking_service_err: "Please choose a service",
            booking_menu_err: "Please choose a menu",
            booking_technician_err: "Please choose a technician",
            booking_date_err: "Please choose date",
            booking_hours_err: "Please choose hour",
            booking_get_hours_timeout: "Network timeout, Please click the button search to try again",
        };
        let webBooking = {
            enable: true,
            minDate: "05/24/2021",
            requiredTechnician: false,
            requiredEmail: false,
            requiredHour: true,
            isRestaurant: false,
        };
        let webFormat = {
            dateFormat: "MM/DD/YYYY",
            datePosition: "1,0,2",
            phoneFormat: "(000) 000-0000",
        };
        let webGlobal = {
            site: "idx",
            siteAct: "",
            siteSubAct: "",
            noPhoto: "/public/library/global/no-photo.jpg",
            isTablet: false,
            isMobile: false,
            enableRecaptcha: false,
        };
        let facebook_embed = {
            id_fanpage: "https://www.facebook.com/FastboyMarketingAgency",
            appId: "",
            width: 450,
            height: 300,
            tabs: "timeline",
            show_facepile: true,
            small_header: true,
            likebox_enable: true,
            likebox_show_faces: false,
            likebox_stream: true,
        };
        let google_id_fanpage = "";
        let twitter_id_fanpage = "";

    </script>

    <!-- Webmaster tools -->

    <!-- FB retargeting -->

    <!-- Schema Script -->

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    <meta name="csrf-token" content="{{ csrf_token() }}" />
</head>

<body class="site-idx site-act-">
    <!-- SOCIAL AND SEO -->
    <div id="fb-root"></div>
    <div style="height: 0; overflow: hidden">
        <h1 itemprop="name"></h1>
    </div>

    <!-- Tpl freeze header -->
    <!--# Active freeze header by insert this html into the website page# value = "1": Mobile and Desktop;# value = "2": Mobile;# value = "3": Desktop;-->
    <input type="hidden" name="activeFreezeHeader" value="1" />
    {{-- HEADER --}}
    @include('frontend.nail04-i.component.header',
    ['header'=>
    App\Models\Product::where('category_id',
    App\Models\Category::where('name','Header And Footer')->first()['id'])->first()])
    {{-- MAIN --}}
    {{ $slot }}
    {{-- FOOTER --}}
    @php
        $album = App\Models\Album::where([['type', 'Social'], ['status', 'PUBLISHED']])->first();
        $social = App\Models\Asset::where('albums_id', $album['id'])->orderBy('priority', 'desc')->get();
    @endphp
    @include('frontend.nail04-i.component.footer',['header'=>
    App\Models\Product::where('category_id',
    App\Models\Category::where('name','Header And Footer')->first()['id'])->first(),
    'social'=>$social]))

    <!-- AUTO REMOVE BOOKING BUTTON -->
    <script type="text/javascript">
        if (!webBooking.enable) {
            $(".btn_make_appointment").remove();
        }

    </script>

    <!-- JS -->
    <script type="text/javascript" src="{{ asset('frontend') }}/public/library/global/js/global-init.js"></script>
    <script type="text/javascript" src="{{ asset('frontend') }}/public/library/global/js/consent.js"></script>
    <script type="text/javascript" src="{{ asset('frontend') }}/themes/fnail04i/assets/js/script.js?v=1.0"></script>
    <script type="text/javascript" src="{{ asset('frontend') }}/themes/fnail04i/assets/custom/js/app.js?v=1.2">
    </script>

    <!-- JS EXTEND -->
    <script type="text/javascript"
    src="{{ asset('frontend') }}/public/assets/webfnail01n22247cd19562c7718b6357a3843319f8.js"></script>
<script>
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

</script>
    <script type="text/javascript"></script>
    @livewireScripts

    <!-- GOOGLE analytics -->

    <!-- GOOGLE ADWORDS remarketing -->

    <!-- POPUP -->
</body>

</html>
