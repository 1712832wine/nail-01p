<main class="main">
    <!-- Use for get current id -->
    <input type="hidden" name="group_id" value="{{ $service_id }}">
    <!-- Tpl main service -->
    <!--# Support:# render.service_data.service, render.service_data2.service-->
    <div class="pmain">
        <div class="page-heading">
            <div class="container">
                <div class="heading-inner">
                    <h2>Services</h2>
                    <ul class="breadcrumbs">
                        <li><a href="/" title="Home">Home</a></li>
                        <li>Services</li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="in-container">
            <div class="container">
                <div class="in-content">
                    <div class="service-container animation_sroll_button">
                        <div class="service-btn-group top-right"><a href="{{ route('book') }}"
                                class="btn btn-main btn_make_appointment" title="Make an appointment"> <img
                                    src="/themes/fnail04i/assets/assets/img/forma-11.png" alt="Forma-11.png" />Make
                                an appointment </a>&nbsp <a href="tel:832-968-6668" class="btn btn-main"
                                title="Call now"> <img src="/themes/fnail04i/assets/assets/img/shape-1.png"
                                    alt="Shape-1.png" />Call now </a></div>
                        <div class="row service-row">
                            <div class="col-sm-12">
                                <p><span>With the full of beauty care services and new colors for you to choose, you
                                        are ensured to enjoy the best services in our effort of doing a great job.
                                        Let’s take a look at our price list below!! Our nail salon is always
                                        committed to bringing you the reasonable price!</span></p>
                            </div>
                        </div>
                        <div class="clearfix animation_sroll_to_service service-list">
                            @foreach ($services as $index => $service)
                                @if ($index % 2 )
                                    <div class="row clearfix" id="sci_{{ $service['id'] }}">
                                        @if (count(json_decode($service['images'])) > 0)
                                            <div class="col-sm-4 col-md-4 right service-image-wrap">
                                                <div class="service-image circle">
                                                    <div class="service-image-bg"
                                                        style="background-image: url('{{ asset('storage') }}/photos/{{ json_decode($service['images'])[0] }}');">
                                                        <img class="img-responsive"
                                                            src="{{ asset('storage') }}/photos/{{ json_decode($service['images'])[0] }}"
                                                            alt="{{ $service['name'] }}" />
                                                    </div>
                                                </div>
                                            </div>
                                        @endif
                                        <div class="col-sm-8 col-md-8">
                                            <div class="clearfix service-list">
                                                <h2 class="service-name">{{ $service['name'] }}</h2>
                                                @foreach (json_decode($service['features']) as $item)
                                                    @switch($item->name)
                                                        @case('desc')
                                                            <p>{{ $item->desc }}</p>
                                                        @break
                                                        @case('center')
                                                        @break
                                                        @default
                                                            <div class="detail-item item-238">
                                                                <div class="detail-price-item">
                                                                    <span class="detail-price-name">{{ $item->name }}</span>
                                                                    <span class="detail-price-dots"></span>
                                                                    <span class="detail-price-number">
                                                                        <span class="current">{{ $item->desc }}</span>
                                                                    </span>
                                                                </div>
                                                            </div>
                                                    @endswitch
                                                @endforeach
                                            </div>
                                        </div>
                                    </div>
                                @else
                                    <div class="row clearfix" id="sci_{{ $service['id'] }}">
                                        <div class="service-line"></div>

                                        <div class="col-sm-8 col-md-8  order-md-1">
                                            <div class="clearfix service-list">
                                                <h2 class="service-name">{{ $service['name'] }}</h2>
                                                @foreach (json_decode($service['features']) as $item)
                                                    @switch($item->name)
                                                        @case('desc')
                                                            <p>{{ $item['desc'] }}</p>
                                                        @break
                                                        @case('center')
                                                        @break
                                                        @default
                                                            <div class="detail-item item-238">
                                                                <div class="detail-price-item">
                                                                    <span class="detail-price-name">{{ $item->name }}</span>
                                                                    <span class="detail-price-dots"></span>
                                                                    <span class="detail-price-number">
                                                                        <span class="current">{{ $item->desc }}</span>
                                                                    </span>
                                                                </div>
                                                            </div>
                                                    @endswitch
                                                @endforeach
                                            </div>
                                        </div>
                                        @if (count(json_decode($service['images'])) > 0)
                                            <div class="col-sm-4 col-md-4 order-md-2 left service-image-wrap">
                                                <div class="service-image circle">
                                                    <div class="service-image-bg"
                                                        style="background-image: url('{{ asset('storage') }}/photos/{{ json_decode($service['images'])[0] }}');">
                                                        <img class="img-responsive"
                                                            src="{{ asset('storage') }}/photos/{{ json_decode($service['images'])[0] }}"
                                                            alt="{{ $service['name'] }}" />
                                                    </div>
                                                </div>
                                            </div>
                                        @endif
                                    </div>
                                @endif
                            @endforeach
                        </div>
                        <div class="row">
                            <div class="col-sm-12">
                                <p style="font-style: oblique; font-size: 16px; margin-bottom: 40px;"></p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</main>
