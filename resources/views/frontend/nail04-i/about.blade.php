<main class="main">
  <!-- Tpl main about -->
  <div class="pmain">
    @foreach ($articles as $item)
    <div class="page-heading">
      <div class="container">
        <div class="heading-inner">
          <h2>About Us</h2>
          <ul class="breadcrumbs">
            <li><a href="/" title="Home">Home</a></li>
            <li>About Us</li>
          </ul>
        </div>
      </div>
    </div>
    <div class="in-container">
      <div class="container">
        <div class="in-content">
          <div class="text-center">
            <h3 class="m-heading">Luxury Nails & Spa</h3>
            {!! $item['content'] !!}
          </div>
            </div>
        </div>
      </div>
    </div>
    @endforeach
  </div>
</main>
