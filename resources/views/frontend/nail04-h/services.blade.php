@php
$features = json_decode($header['features']);
$extras = json_decode($header['extras']);
@endphp
<main>
    <div class="pmain">
        <!-- Use for get current id -->
        <input type="hidden" name="group_id" value="{{ $service_id }}" />
        <div class="page-heading">
            <div class="container">
                <div class="row">
                    <div class="col-md-5 col-sm-6">
                        <h2 class="title">Our Service</h2>
                    </div>
                    <div class="col-md-7 col-sm-6">
                        <div class="btn_service_book service-btn-group animation_sroll_button">
                            <a href="/book" class="btn btn-default"><i class="fa fa-calendar"></i> Make an
                                appointment</a>&nbsp;
                            <a href="tel:{{ explode(',', $features[1]->desc)[0] }}" class="btn btn-primary btn-call">
                                <i class="fa fa-phone"></i> Call now</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="in-container main-content animation_sroll_jumpto svedt">
            <!-- Animation scroll to block service: with only add class &#39;animation_sroll_jumpto&#39; in element contain entire html service data -->
            <div class="container page-container">
                @foreach ($services as $index => $service)
                    @if ($index % 2 === 0)
                        <div class="row service-row sroll_jumpto" id="sci_{{ $service['id'] }}">
                            <div class="col-sm-4 col-md-4 text-center hidden-lg hidden-md">
                                @if (count(json_decode($service['images'])) > 0)
                                    <div class="circle-service-image"
                                        style="background-image: url('{{ asset('storage') }}/photos/{{ json_decode($service['images'])[0] }}')">
                                    </div>
                                @endif
                            </div>
                            <div class="col-sm-8 col-md-8">
                                <h2 id="{{ $service['name'] }}" class="service-name">{{ $service['name'] }}</h2>
                                <p class="cat_title">{{ $service['description'] }}</p>
                                @foreach (json_decode($service['features']) as $item)
                                    @switch($item->name)
                                        @case('desc')
                                            <div class="detail-description">{{ $item->desc }}</div>
                                        @break
                                        @case('center')
                                        @break
                                        @default
                                            <div class="box_service">
                                                <div class="detail-price-item">
                                                    <span class="detail-price-name">{{ $item->name }}</span>
                                                    <span class="detail-price-dots"></span>
                                                    <span class="detail-price-number">
                                                        <span class="current">{{ $item->desc }}</span>
                                                    </span>
                                                </div>
                                                <div class="detail-description-item"></div>
                                            </div>
                                    @endswitch
                                @endforeach
                            </div>
                            <div class="col-sm-4 col-md-4 text-center hidden-sm hidden-xs">
                                <div class="circle-service-image"
                                    style="background-image: url('{{ asset('storage') }}/photos/{{ json_decode($service['images'])[0] }}')">
                                </div>
                            </div>
                        </div>
                    @else
                        <div class="row service-row sroll_jumpto" id="sci_{{ $service['id'] }}">
                            <div class="col-sm-4 col-md-4 text-center">
                                @if (count(json_decode($service['images'])) > 0)
                                    <div class="circle-service-image"
                                        style="background-image: url('{{ asset('storage') }}/photos/{{ json_decode($service['images'])[0] }}')">
                                    </div>
                                @endif
                            </div>
                            <div class="col-sm-8 col-md-8">
                                <h2 id="{{ $service['name'] }}" class="service-name">{{ $service['name'] }}</h2>
                                <p class="cat_title"></p>
                                @foreach (json_decode($service['features']) as $item)
                                    @switch($item->name)
                                        @case('desc')
                                            <div class="detail-description">{{ $item->desc }}</div>
                                        @break
                                        @case('center')
                                        @break
                                        @default
                                            <div class="box_service">
                                                <div class="detail-price-item">
                                                    <span class="detail-price-name">{{ $item->name }}</span>
                                                    <span class="detail-price-dots"></span>
                                                    <span class="detail-price-number">
                                                        <span class="current">{{ $item->desc }}</span>
                                                    </span>
                                                </div>
                                                <div class="detail-description-item"></div>
                                            </div>
                                    @endswitch
                                @endforeach
                            </div>
                        </div>
                    @endif
                @endforeach
            </div>
        </div>
    </div>
</main>
