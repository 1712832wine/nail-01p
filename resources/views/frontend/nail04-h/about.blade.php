<main>
    <div class="pmain">
        <div class="page-heading">
            <div class="container">
                <h2>About us</h2>
            </div>
        </div>
        <section class="section section-about wow fadeInUp">
            <div class="container">
                @foreach ($articles as $item)
                    <div class="row">
                        <div class="col-md-7">
                            <h2><span style="font-size: 28px;">{{ $item['title'] }}</span></h2>
                            {!! $item['content'] !!}
                        </div>
                        @if (count(json_decode($item['image'])) > 0)
                            <div class="col-md-5">
                                <img src="{{ asset('storage') }}/photos/{{ json_decode($item['image'])[0] }}"
                                    caption="false" />
                            </div>
                        @endif
                    </div>
                @endforeach
            </div>
        </section>
    </div>
</main>
