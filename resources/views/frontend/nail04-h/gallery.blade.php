<main>
    <div class="pmain">
        <div class="page-heading">
            <div class="container">
                <h2>Gallery</h2>
            </div>
        </div>
        <div class="in-container">
            <div class="container">
                <div class="in-content">
                    <div class="gallery-box-wrap">
                        <div class="row">
                            @foreach ($albums as $index => $album)
                                <div class="col-xs-6 col-sm-6 col-md-4">
                                    <a itemprop="url" title="Nails Design " href="/gallery-detail/{{ $album['id'] }}">
                                        <div class="gallery-box">
                                            <div class="image-bg"
                                                style="background-image: url('{{ asset('storage') }}/photos/{{ $lists[$index][0]['url'] }}');">
                                                <img itemprop="image"
                                                    src="{{ asset('storage') }}/photos/{{ $lists[$index][0]['url'] }}">
                                            </div>
                                            <div class="gallery-title">
                                                <h4 itemprop="name">{{ $album['name'] }}</h4>
                                            </div>
                                        </div>
                                    </a>
                                </div>
                            @endforeach
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</main>
