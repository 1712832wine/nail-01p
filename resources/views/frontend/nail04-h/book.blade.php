<main>
    <div class="pmain">
        <!-- Tpl Main Book -->
        <div class="page-heading">
            <div class="container">
                <h2>Booking</h2>
            </div>
        </div>
        <div class="in-container bg-width">
            <div class="container">
                <div class="in-content">
                    <div class="row">
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <script src='https://www.google.com/recaptcha/api.js?hl=en' async defer></script>
                            <!-- Google reCaptcha -->
                            <script type="text/javascript">
                                function ezyCaptcha_surveyForm(token, is_submit) {
                                    is_submit = 1;
                                    if ($("#password").length) {
                                        //$("input:password").val(md5(clean_input($("#password").val())));
                                    }
                                    return true;
                                }

                            </script>
                            <div class="content-shop bg-ctn-special">
                                <div class="content-shop-booking">
                                    <form enctype="multipart/form-data" id="surveyForm" method="post" action="/book/add"
                                        class="form-horizontal">
                                        <div class="item-booking">
                                            <div id="optionTemplate">
                                                <div class="row">
                                                    <div class="col-lg-6 col-md-6 col-xs-12">
                                                        <div class="group-select">
                                                            <label>Service(required)</label>
                                                            <div
                                                                style="display: inline-block; position: relative; width: 100%;">
                                                                <select name="product_id[]"
                                                                    class="list_service form-control"
                                                                    data-validation-message="Please choose service">
                                                                    <option value="" price="" staff="[]">Select
                                                                        Service</option>
                                                                    <!--List Categories-->
                                                                    <optgroup label="ARTIFICIAL NAILS">
                                                                        <!--List service-->
                                                                        <option value="305" price="$30" staff='[]'>
                                                                            Acrylic Full-Set ($30) </option>
                                                                        <option value="306" price="$20" staff='[]'>
                                                                            Acrylic Fill ($20) </option>
                                                                        <option value="307" price="$45" staff='[]'>
                                                                            Gel Full-Set ($45) </option>
                                                                        <option value="308" price="$35" staff='[]'>
                                                                            Gell Fill ($35) </option>
                                                                        <option value="309" price="$45" staff='[]'>
                                                                            Gell Back-Fill ($45) </option>
                                                                        <option value="310" price="$35" staff='[]'>
                                                                            Solar Regular Full-Set ($35) </option>
                                                                        <option value="311" price="$50" staff='[]'>
                                                                            Solar Full-Set Color ($50) </option>
                                                                        <option value="312" price="$25" staff='[]'>
                                                                            Solar Fill ($25) </option>
                                                                        <option value="313" price="$45" staff='[]'>
                                                                            Solar Full-Set P & W ($45) </option>
                                                                        <option value="314" price="$50" staff='[]'>
                                                                            Solar Full-Set Color Tip P & W ($50)
                                                                        </option>
                                                                        <option value="315" price="$40" staff='[]'>
                                                                            Solar Back-Fill P & W ($40) </option>
                                                                        <option value="316" price="$45" staff='[]'>
                                                                            Solar Back-Fill Color Tip P & W ($45)
                                                                        </option>
                                                                        <option value="317" price="$5" staff='[]'>
                                                                            Gel Coating ($5) </option>
                                                                        <option value="318" price="$10" staff='[]'>
                                                                            Acrylic Nails Take Off ($10) </option>
                                                                        <option value="319" price="$5" staff='[]'>
                                                                            Nails Repair ($5) </option>
                                                                        <option value="320" price="$15" staff='[]'>
                                                                            Gelish or Shellac ($15) </option>
                                                                        <option value="321" price="" staff='[]'>*Any
                                                                            service with French will cost $5 extra
                                                                        </option>
                                                                        <option value="322" price="" staff='[]'>
                                                                            *Long Nail will be charge extra</option>
                                                                    </optgroup>
                                                                    <optgroup label="NATURAL NAILS">
                                                                        <!--List service-->
                                                                        <option value="323" price="$15" staff='[]'>
                                                                            Express Manicure ($15) </option>
                                                                        <option value="324" price="$20" staff='[]'>
                                                                            Manicure ($20) </option>
                                                                        <option value="325" price="$25" staff='[]'>
                                                                            Deluxe Manicure ($25) </option>
                                                                        <option value="326" price="$25" staff='[]'>
                                                                            Spa Pedicure ($25) </option>
                                                                        <option value="327" price="$35" staff='[]'>
                                                                            Deluxe Pedicure (40 min) ($35) </option>
                                                                        <option value="328" price="$45" staff='[]'>
                                                                            Signature Pedicure (1 hour) ($45)
                                                                        </option>
                                                                        <option value="329" price="$45" staff='[]'>
                                                                            Mani & Spa Pedi ($45) </option>
                                                                        <option value="330" price="$8" staff='[]'>
                                                                            Nails Polish Change ($8) </option>
                                                                        <option value="331" price="$10" staff='[]'>
                                                                            Toes Polish Change ($10) </option>
                                                                        <option value="332" price="$10" staff='[]'>
                                                                            Nail Cut & File ($10) </option>
                                                                        <option value="333" price="$5" staff='[]'>
                                                                            Nails Arts (2 designs) ($5) </option>
                                                                        <option value="334" price="$25" staff='[]'>
                                                                            Take Off & Manicure ($25) </option>
                                                                        <option value="335" price="$30" staff='[]'>
                                                                            Shellac Color ($30) </option>
                                                                        <option value="336" price="$20" staff='[]'>
                                                                            Shellac Polish Change ($20) </option>
                                                                        <option value="337" price="$10" staff='[]'>
                                                                            Take Off Shellac ($10) </option>
                                                                        <option value="338" price="$8" staff='[]'>
                                                                            Color W/French Tip ($8) </option>
                                                                        <option value="339" price="$5" staff='[]'>
                                                                            Any service with paraffin wax will cost
                                                                            ($5) </option>
                                                                    </optgroup>
                                                                    <optgroup label="KIDS MENU">
                                                                        <!--List service-->
                                                                        <option value="340" price="" staff='[]'>10
                                                                            years old & under</option>
                                                                        <option value="341" price="$12" staff='[]'>
                                                                            Kid Mani ($12) </option>
                                                                        <option value="342" price="$28" staff='[]'>
                                                                            Mani & Pedi ($28) </option>
                                                                        <option value="343" price="$18" staff='[]'>
                                                                            Pedi ($18) </option>
                                                                        <option value="344" price="$7" staff='[]'>
                                                                            Polish Change Fingers ($7) </option>
                                                                        <option value="345" price="$8" staff='[]'>
                                                                            Polish Change Toes ($8) </option>
                                                                    </optgroup>
                                                                    <optgroup label="HAIR">
                                                                        <!--List service-->
                                                                        <option value="346" price="$30" staff='[]'>
                                                                            Women Cut ($30) </option>
                                                                        <option value="347" price="$20" staff='[]'>
                                                                            Men Cut ($20) </option>
                                                                        <option value="348" price="$18" staff='[]'>
                                                                            Girl/Boy under 10 Cut ($18) </option>
                                                                        <option value="349" price="$5" staff='[]'>
                                                                            Bang Trim ($5) </option>
                                                                        <option value="350" price="$65" staff='[]'>
                                                                            Women All-Over Color ($65) </option>
                                                                        <option value="351" price="$55" staff='[]'>
                                                                            Women Color Touch-Up ($55) </option>
                                                                        <option value="352" price="$75" staff='[]'>
                                                                            Women Highlights ($75) </option>
                                                                        <option value="353" price="$55" staff='[]'>
                                                                            Women Partial Highlights ($55) </option>
                                                                        <option value="354" price="$55" staff='[]'>
                                                                            Women Partial Lowlights ($55) </option>
                                                                        <option value="355" price="$80" staff='[]'>
                                                                            Perm ($80) </option>
                                                                        <option value="356" price="$50" staff='[]'>
                                                                            Men Highlights ($50) </option>
                                                                        <option value="357" price="$50" staff='[]'>
                                                                            Men Color ($50) </option>
                                                                        <option value="358" price="$5" staff='[]'>
                                                                            Shampoo ($5) </option>
                                                                        <option value="359" price="$35" staff='[]'>
                                                                            Shampoo & Style ($35) </option>
                                                                        <option value="360" price="$50" staff='[]'>
                                                                            Up-do ($50) </option>
                                                                    </optgroup>
                                                                    <optgroup label="WAXING">
                                                                        <!--List service-->
                                                                        <option value="361" price="$12" staff='[]'>
                                                                            Eyebrow ($12) </option>
                                                                        <option value="362" price="$10" staff='[]'>
                                                                            Upper Lip ($10) </option>
                                                                        <option value="363" price="$20" staff='[]'>
                                                                            Eyebrow & Upper Lip ($20) </option>
                                                                        <option value="364" price="$28" staff='[]'>
                                                                            Eyebrow, Upper Lip, Chin ($28) </option>
                                                                        <option value="365" price="$20" staff='[]'>
                                                                            Under Arms ($20) </option>
                                                                        <option value="366" price="$40" staff='[]'>
                                                                            Face Wax ($40) </option>
                                                                        <option value="367" price="$12" staff='[]'>
                                                                            Chin ($12) </option>
                                                                        <option value="368" price="$45" staff='[]'>
                                                                            Chest Or Back ($45) </option>
                                                                        <option value="369" price="$40" staff='[]'>
                                                                            Half Legs ($40) </option>
                                                                        <option value="370" price="$65" staff='[]'>
                                                                            Full Legs ($65) </option>
                                                                        <option value="371" price="$40" staff='[]'>
                                                                            Bikini ($40) </option>
                                                                        <option value="372" price="$55" staff='[]'>
                                                                            Brazillian ($55) </option>
                                                                        <option value="373" price="$45" staff='[]'>
                                                                            Arms ($45) </option>
                                                                        <option value="374" price="$35" staff='[]'>
                                                                            Half Arms ($35) </option>
                                                                        <option value="375" price="$150" staff='[]'>
                                                                            Eye Lashes Extension ($150) </option>
                                                                        <option value="376" price="$65" staff='[]'>
                                                                            Eye Lashes Extension Refill ($65)
                                                                        </option>
                                                                    </optgroup>
                                                                    <optgroup label="FACIALS">
                                                                        <!--List service-->
                                                                        <option value="377" price="$45" staff='[]'>
                                                                            Express Facial ($45) </option>
                                                                        <option value="378" price="$60" staff='[]'>
                                                                            Basic Facial ($60) </option>
                                                                        <option value="379" price="$60" staff='[]'>
                                                                            Reflexology ($60) </option>
                                                                        <option value="380" price="$55" staff='[]'>
                                                                            Make Up ($55 & up) </option>
                                                                    </optgroup>
                                                                </select>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-6 col-md-6 col-xs-12">
                                                        <div class="group-select">
                                                            <label>Technician (optional)</label>
                                                            <select name="staff_id[]" class="list_staff form-control">
                                                                <option value="">Select technician</option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-lg-6 col-md-6 col-xs-12">
                                                    <div class="group-select box-date">
                                                        <label>Date</label>
                                                        <input type="text" name="booking_date" id="datetimepicker_v1"
                                                            class="form-control choose_date booking_date"
                                                            placeholder="Choice a date" value="05/24/2021"
                                                            typehtml="html">
                                                        <span class="fa fa-calendar"></span>
                                                    </div>
                                                </div>
                                                <div class="col-lg-6 col-md-6 col-xs-12">
                                                    <label>&nbsp;</label>
                                                    <button class='btn btn-search btn_action title booking_search'
                                                        type='button'>Search</button>
                                                </div>
                                            </div>
                                            <input type="hidden" name="booking_hours" value="" />
                                            <input type="hidden" name="booking_area_code" value="" />
                                            <input type="hidden" name="booking_phone" value="" />
                                            <input type="hidden" name="booking_name" value="" />
                                            <input type="hidden" name="nocaptcha" value="1" />
                                            <input type="hidden" name="g-recaptcha-response" value="" />
                                            <input type="hidden" name="notelist" value="" />
                                        </div>
                                        <span class="add-services addButton">
                                            <img src="/public/library/global/add-service-icon-new.png"> Add Another
                                            service
                                        </span>
                                    </form>
                                    <div id="book-info" class="infor-booking box_detail_info">
                                        <div class="staff" id="box_person"></div>
                                        <div class="time-booking col-md-12 databooktime" style="display: none">
                                            <h3 class="time_show"></h3>
                                            <div class="am">
                                                <label>Morning <span class="note_am_time"
                                                        style="color: red; font-style: italic;"></span></label>
                                                <ul class="timemorning">

                                                </ul>
                                            </div>
                                            <div class="pm">
                                                <label>Afternoon <span class="note_pm_time"
                                                        style="color: red; font-style: italic;"></span></label>
                                                <ul class="timeafternoon">

                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                    <script type="text/javascript">
                                        $(document).ready(function() {
                                            var formbk =
                                                '{"listperson":[],"notelist":[],"booking_date":"05\/24\/2021","booking_hours":""}';
                                            $('.btn-search').addClass('btn-main');
                                            loadEvent();

                                        });

                                    </script>
                                </div>
                            </div>

                            <!-- module 4 -->
                            <section style="display: none;">
                                <!--POPUP LOGIN-->
                                <div id="popup_login" class="white-popup mfp-hide">
                                    <div class="box_account_v1">
                                        <div class="modal_form_header">
                                            <h4>Login</h4>
                                        </div>
                                        <div class="popup_main_area">
                                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 border_right">
                                                <form enctype="multipart/form-data" id="form-login" method="POST"
                                                    action="/login/login_do">
                                                    <fieldset class="form-group">
                                                        <div class="box_login">
                                                            <h2> Been here before?</h2>
                                                            <div class="form_input_1">
                                                                <div class="form-control-wrapper">
                                                                    <input type="email" name="cus_email"
                                                                        placeholder="Enter your E-mail"
                                                                        data-validation="[EMAIL]"
                                                                        data-validation-message="Email is not valid!">
                                                                </div>
                                                            </div>
                                                            <div class="form_input_2">
                                                                <div class="form-control-wrapper">
                                                                    <input type="password" placeholder="Password"
                                                                        name="cus_password" data-validation="[NOTEMPTY]"
                                                                        data-validation-message="Password must not be empty!">
                                                                </div>
                                                            </div>
                                                            <div class="col_psw_v1" style="padding-bottom: 3px; ">

                                                                <div class="form-control-wrapper"
                                                                    style="margin-top: -10px;padding-bottom: 3px;float:right">
                                                                    <a href="/login/forgot-password/">Forgot
                                                                        password</a>

                                                                </div>
                                                            </div>
                                                            <div class="btn_submit_login">
                                                                <button class="submit" type="submit">Login</button>
                                                            </div>
                                                        </div>
                                                    </fieldset>
                                                </form>
                                            </div>
                                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                                <div class="box_register">
                                                    <h2>New Account?</h2>
                                                    <div class="btn_submit_login">
                                                        <button class="submit" type="submit"
                                                            onclick="window.location.href = '/register/'">Create a
                                                            Username</button>
                                                    </div>
                                                    <div class="btn_login_social">

                                                        <a href="" class="btn btn_facebook_v1" title="facebook login">
                                                            <i class="fa fa-facebook"></i>
                                                            <span>Facebook</span>
                                                        </a>



                                                        <a class="btn btn_gplus_v1" href="" title="gplus login">
                                                            <i class="fa fa-google-plus"></i>
                                                            <span>Google</span>
                                                        </a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <!--POPUP BOOKING-->
                                <div id="open_booking" class="white-popup mfp-hide">
                                    <div class="box_account_v1">
                                        <div class="modal_form_header">
                                            <h4>Message</h4>
                                        </div>
                                        <div class="popup_main_area">
                                            <form enctype="multipart/form-data" id="booking_check" name="booking_check">
                                                <p style="font-weight: bold; font-size: 18px;">Confirm booking
                                                    information ?</p>
                                                <span
                                                    style="color: red; font-style: italic; margin: 10px 0; display: block;">We
                                                    will send a text message to you via the number below after we
                                                    confirm the calendar for your booking!</span>
                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label class="form-control-label">Your name</label>
                                                            <div class="form-control-wrapper">
                                                                <input name="input_name" type="text"
                                                                    class="form-control" type="text"
                                                                    data-validation="[NOTEMPTY]"
                                                                    data-validation-message="Please enter your name!" />
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-2" style="display: none;">
                                                        <div class="form-group">
                                                            <label class="form-control-label">Area code</label>
                                                            <select name="area_code" class="form-control"
                                                                defaultvalue="1">
                                                                <option value="1">US (+1)</option>
                                                                <option value="84">VN (+84)</option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label class="form-control-label">Phone number</label>
                                                            <div class="form-control-wrapper">
                                                                <input name="phone_number" type="text"
                                                                    class="form-control inputPhone" type="text"
                                                                    placeholder="Ex: (123) 123-1234"
                                                                    data-validation="[NOTEMPTY]"
                                                                    data-validation-message="Phone number invalid" />
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <div class="form-group">
                                                            <label class="form-control-label">Note
                                                                (Optional)</label>
                                                            <div class="form-control-wrapper">
                                                                <textarea name="notelist"
                                                                    placeholder="Max length 200 character" rows="5"
                                                                    class="form-control" maxlength="200"></textarea>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <button class="btn btn-success btn_confirmed "
                                                    type="button">Confirm</button>
                                                <button class="btn btn-danger btn-inline btn_cancel"
                                                    type="button">Cancel</button>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </section>
                            <script>
                                checktimebooking = 1;
                                $(document).ready(function() {
                                    $("#form-login").validate({
                                        submit: {
                                            settings: {
                                                button: "[type='submit']",
                                                inputContainer: '.form-group',
                                                errorListClass: 'form-tooltip-error',
                                            }
                                        }
                                    });
                                    // check Time
                                    $('form#booking_check').validate({
                                        submit: {
                                            settings: {
                                                clear: 'keypress',
                                                display: "inline",
                                                button: ".btn_confirmed",
                                                inputContainer: 'form-group',
                                                errorListClass: 'form-tooltip-error',
                                            },
                                            callback: {
                                                onSubmit: function(node, formdata) {
                                                    // console.log("toi day");
                                                    var areacode =
                                                        1; //$("select[name='area_code']").val();
                                                    var phone_number = $(
                                                        "input[name='phone_number']").val();
                                                    var cus_name = $("input[name='input_name']")
                                                        .val();

                                                    var notelist = $("textarea[name='notelist']")
                                                        .val();
                                                    $("input[name='notelist']").val(notelist);

                                                    if (phone_number != "" && phone_number.length >
                                                        6) {
                                                        $("input[name='booking_phone']").val(
                                                            phone_number);
                                                        $("input[name='booking_area_code']").val(
                                                            areacode);
                                                    }

                                                    if (cus_name) {
                                                        $("input[name='booking_name']").val(
                                                            cus_name);
                                                    }


                                                    if (enableRecaptcha) {
                                                        var check_google = $(
                                                            "#g-recaptcha-response").val();
                                                        if (typeof(check_google) != "undefined" &&
                                                            check_google != "") {
                                                            $("input[name='g-recaptcha-response']")
                                                                .val(check_google);
                                                            $("#surveyForm").submit();
                                                        }
                                                    } else {
                                                        $("#surveyForm").submit();
                                                    }

                                                    return false;
                                                }
                                            }
                                        }
                                    });
                                });

                            </script>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</main>
