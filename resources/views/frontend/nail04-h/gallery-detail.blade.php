<main>
    <div class="pmain">
        <section class="p-list-gallery">
            <!-- tpl list_gallery -->
            <!-- support render gallery_data -->

            <div class="page-heading">
                <div class="container">
                    <h2>Gallery</h2>
                </div>
            </div>
            <div class="in-container">
                <div class="container">
                    <div class="in-content">
                        <div class="gallery-box-wrap">
                            <div class="row">
                                @foreach ($list as $item)
                                    <div class="col-xs-6 col-sm-3">
                                        <a class="gallery-item" data-group="gallery-1" title="{{ $item['name'] }}"
                                            href="{{ asset('storage') }}/photos/{{ $item->url }}" itemprop="image">
                                            <span
                                                style="background-image:url('{{ asset('storage') }}/photos/{{ $item->url }}')"></span>
                                            <div class="gallery-info">
                                                <h3 class="project-title">Nails Design</h3>
                                                <div class="icon"><i class="fa fa-search-plus"></i></div>
                                            </div>
                                        </a>
                                    </div>
                                @endforeach
                            </div>
                        </div>
                        {{ $list->links('frontend.nail04-h.component.custom-pagination') }}
                    </div>
                </div>

            </div>
        </section>
    </div>
</main>
