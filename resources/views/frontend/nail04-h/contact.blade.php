@php
$features = json_decode($header['features']);
$extras = json_decode($header['extras']);
@endphp
<main>
    <div class="pmain">
        <div class="page-heading">
            <div class="container">
                <h2>Contact us</h2>
            </div>
        </div>
        <div class="in-container">
            <div class="container">
                <div class="contact-form">
                    <div class="row">
                        <div class="col-md-8">
                            <div class="contact-heading">
                                <h4>Contact Form</h4>
                            </div>
                            <div class="footer-3-box">
                                <div class="custom-form-2">
                                    <div class="form-contact-us-index">
                                        <div class="form-input-infor">
                                            <form enctype="multipart/form-data" method="post" name="send_contact"
                                                id="send_contact" action="{{ route('send_contact') }}">
                                                @csrf
                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <input title="your name" type="text"
                                                                data-validation="[NOTEMPTY]"
                                                                data-validation-message="Please enter your name!"
                                                                autocomplete="off" class="form-control style-input"
                                                                placeholder="Your Name" name="contactname">
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <input title="Your Email" type="text"
                                                                data-validation="[EMAIL]"
                                                                data-validation-message="Invalid email"
                                                                autocomplete="off" class="form-control style-input"
                                                                placeholder="Your Email" name="contactemail">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <div class="form-group">
                                                            <input title="Your Subject" type="text"
                                                                data-validation="[NOTEMPTY]"
                                                                data-validation-message="Please enter a subject!"
                                                                autocomplete="off" class="form-control style-input"
                                                                placeholder="Your Subject" name="contactsubject">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-xs-12">
                                                        <textarea style="margin-bottom: 15px;" title="Your Message"
                                                            rows="10" data-validation="[NOTEMPTY]"
                                                            data-validation-message="Please enter a content!"
                                                            autocomplete="off"
                                                            class="form-control style-input style-textarea"
                                                            placeholder="Your Message" name="contactcontent"></textarea>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-xs-12">
                                                        <div class="form-group">
                                                            @if (session('status'))
                                                                <div class="alert alert-success" role="alert">
                                                                    <button type="button" class="close"
                                                                        data-dismiss="alert">×</button>
                                                                    {{ session('status') }}
                                                                </div>
                                                            @elseif(session('failed'))
                                                                <div class="alert alert-danger" role="alert">
                                                                    <button type="button" class="close"
                                                                        data-dismiss="alert">×</button>
                                                                    {{ session('failed') }}
                                                                </div>
                                                            @endif
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-xs-12">
                                                        <div class="btn-sauna form-group">
                                                            <button type="submit"
                                                                class="btn btn-main btn-submit btn_contact  "> Send
                                                                message </button>
                                                        </div>
                                                    </div>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="contact-details">
                                <div class="cd-col">
                                    <i class="fa fa-map-signs"></i>
                                    <p>{{ $features[0]->desc }}</p>
                                </div>
                                <div class="cd-col">
                                    <i class="fa fa-phone"></i>

                                    @foreach (explode(',', $features[1]->desc) as $item)
                                        <p><a href="tel:{{ $item }}" itemprop="url">{{ $item }}</a></p>
                                    @endforeach

                                </div>
                                <div class="cd-col">
                                    <i class="fa fa-envelope"></i>
                                    @foreach (explode(',', $features[2]->desc) as $item)
                                        <p><a href="mailto:{{ $item }}" itemprop="url">{{ $item }}</a>
                                        </p>
                                    @endforeach
                                </div>
                            </div>
                            <div class="contact-hours">
                                @foreach ($extras as $item)
                                    <div class="cd-col" content="{{ $item->name }}:{{ $item->desc }}">
                                        <div class="opening-hour-day">{{ $item->name }}</div>
                                        <div class="opening-hour-time">{{ $item->desc }}</div>
                                    </div>
                                @endforeach
                            </div>
                        </div>
                    </div>
                </div>
                <!-- Google map area -->
                <div class="maps">
                    <div class="google-map" id="map" style="width:100%; height:450px; clear: both"><iframe
                            src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d6929.489650551249!2d-95.570685!3d29.727150000000005!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x74ace03ed50728e8!2sFast%20Boy%20Marketing!5e0!3m2!1sen!2sus!4v1569202339382!5m2!1sen!2sus"
                            width="100%" height="450" frameborder="0" style="border:0;" allowfullscreen=""></iframe>
                    </div>
                </div>
            </div>
        </div>
    </div>
</main>
