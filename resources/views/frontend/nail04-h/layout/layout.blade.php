<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" dir="ltr" lang="en" xmlns:og="http://ogp.me/ns#"
    xmlns:fb="http://www.facebook.com/2008/fbml" itemscope itemtype="https://schema.org/NailSalon">

<head>
    <!-- Language -->
    <meta http-equiv="content-language" content="en" />
    <link rel="alternate" href="https://fnail04h.fastboywebsites.com" hreflang="x-default">
    <link rel="alternate" href="https://fnail04h.fastboywebsites.com" hreflang="en-US">

    <!-- Meta limit -->

    <!-- Meta Tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width,initial-scale=1.0" />
    <meta http-equiv="content-type" content="text/html; charset=UTF-8" />
    <meta name="description" content="" />
    <meta name="keywords" content="" />
    <meta name="author" content="" />

    <!-- OG -->
    <meta property="og:title" content="" />
    <meta property="og:description" content="" />
    <meta property="og:image" content="" />
    <meta property="og:url" content="https://fnail04h.fastboywebsites.com/" />
    <meta property="og:type" content="website" />
    <meta property="og:site_name" content="Nails Spa" />

    <!-- Dublin Core -->
    <link rel="schema.DC" href="http://purl.org/dc/elements/1.1/">
    <meta name="DC.title" content="">
    <meta name="DC.identifier" content="https://fnail04h.fastboywebsites.com/">
    <meta name="DC.description" content="">
    <meta name="DC.subject" content="">
    <meta name="DC.language" scheme="UTF-8" content="en-us">
    <meta itemprop="priceRange" name="priceRange" content="">

    <!-- GEO meta -->
    <meta name="geo.region" content="">
    <meta name="geo.placename" content="">
    <meta name="geo.position" content="">
    <meta name="ICBM" content="">

    <!-- Page Title -->
    <title>Nails Spa</title>
    <base href="/themes/fnail04h/assets/">

    <!-- canonical -->
    <link rel="canonical" href="https://fnail04h.fastboywebsites.com">



    <!-- Favicons -->
    <link rel="icon" href="https://fnail04h.fastboywebsites.com/uploads/fnail07ri1nsb/attach/1569203408_fbm_fvc.png"
        type="image/x-icon">
    <link rel="shortcut icon"
        href="https://fnail04h.fastboywebsites.com/uploads/fnail07ri1nsb/attach/1569203408_fbm_fvc.png"
        type="image/x-icon">

    <!-- CSS -->
    <link href="https://fonts.googleapis.com/css?family=Oswald:400,700&display=swap&subset=vietnamese" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:400,700&display=swap&subset=vietnamese"
        rel="stylesheet">

    <link rel="stylesheet" type="text/css" href='{{ asset('frontend') }}/public/library/jquery-ui/jquery-ui.min.css'>
    <link rel="stylesheet" type="text/css" href='{{ asset('frontend') }}/public/library/animate/animate.css'>
    <link rel="stylesheet" type="text/css" href='{{ asset('frontend') }}/public/library/validation/validation.css'>
    <link rel="stylesheet" type="text/css"
        href='{{ asset('frontend') }}/public/library/bootstrap/v3.3.7/bootstrap.min.css'>
    <link rel="stylesheet" type="text/css"
        href='{{ asset('frontend') }}/public/library/font-awesome/v4.7.0/css/font-awesome.min.css'>
    <link rel="stylesheet" type="text/css"
        href='{{ asset('frontend') }}/public/library/bootstrap-datetimepicker/bootstrap-datetimepicker.min.css'>
    <link rel="stylesheet" type="text/css"
        href='{{ asset('frontend') }}/public/library/owl-carousel/v2.3.4/owl.carousel.min.css'>
    <link rel="stylesheet" type="text/css" href='{{ asset('frontend') }}/public/library/flex-label/flex-label.css'>
    <link rel="stylesheet" type="text/css" href='{{ asset('frontend') }}/public/library/pnotify/pnotify.css'>
    <link rel="stylesheet" type="text/css"
        href='{{ asset('frontend') }}/public/library/slider-pro/slider-pro.min.css'>
    <link rel="stylesheet" type="text/css"
        href='{{ asset('frontend') }}/public/library/magnific-popup/magnific-popup.css'>
    <link rel="stylesheet" type="text/css" href='{{ asset('frontend') }}/public/library/signature/signature.css'>
    <link rel="stylesheet" type="text/css" href='{{ asset('frontend') }}/public/library/instagram/instafeed.min.css'>

    <link rel="stylesheet" type="text/css" href='{{ asset('frontend') }}/themes/fnail04h/assets/css/meanmenu.css'>
    <link rel="stylesheet" type="text/css" href='{{ asset('frontend') }}/themes/fnail04h/assets/css/style.css'>
    <link rel="stylesheet" type="text/css" href='{{ asset('frontend') }}/themes/fnail04h/assets/css/responsive.css'>
    <link rel="stylesheet" type="text/css" href='{{ asset('frontend') }}/public/library/global/css/news.css'>
    <link rel="stylesheet" type="text/css" href='{{ asset('frontend') }}/public/library/global/css/consent.css'>
    <link rel="stylesheet" type="text/css"
        href='{{ asset('frontend') }}/themes/fnail04h/assets/css/custom.css?v=1.0'>

    <script type="text/javascript">
        var dateFormatBooking = "MM/DD/YYYY";
        var posFormat = "1,0,2";
        var timeMorning = '["9:00","9:30","10:00","10:30","11:00","11:30"]';
        var timeAfternoon =
            '["12:00","12:30","13:00","13:30","14:00","14:30","15:00","15:30","16:00","16:30","17:00","17:30","18:00","18:30","19:00"]';
        var bookLogin = "open_booking";
        var beforeTime = "1";
        var beforeDay = 0;
        var currDateT = "2021/05/24";
        var checktimebooking = 1;
        var enableRecaptcha = 0;
        var hoursTimeFormat = 12;
        var phoneFormat = "(000) 000-0000";
        var company_phone = "832-968-6668";
        var num_paging = "12";

        let facebook_embed = {
            id_fanpage: "",
            appId: "",
            width: 450,
            height: 300,
            tabs: "timeline",
            show_facepile: true,
            small_header: true,
            likebox_enable: true,
            likebox_show_faces: false,
            likebox_stream: true,
        };
        let google_id_fanpage = "";
        let twitter_id_fanpage = "";

    </script>

    <script type="text/javascript" src="{{ asset('frontend') }}/public/library/jquery/v3.3.1/jquery.min.js"></script>
    <script type="text/javascript" src="{{ asset('frontend') }}/public/library/jquery-ui/jquery-ui.min.js"></script>
    <script type="text/javascript" src="{{ asset('frontend') }}/public/library/jquery-ui/jquery-migrate.min.js">
    </script>
    <script type="text/javascript" src="{{ asset('frontend') }}/public/library/validation/validation.min.js">
    </script>
    <script type="text/javascript" src="{{ asset('frontend') }}/public/library/flex-label/flex-label.js"></script>
    <script type="text/javascript" src="{{ asset('frontend') }}/public/library/mask/mask.min.js"></script>
    <script type="text/javascript" src="{{ asset('frontend') }}/public/library/bootstrap/v3.3.7/bootstrap.min.js">
    </script>
    <script type="text/javascript" src="{{ asset('frontend') }}/public/library/moment/moment-with-locales.js">
    </script>
    <script type="text/javascript"
        src="{{ asset('frontend') }}/public/library/bootstrap-datetimepicker/bootstrap-datetimepicker.js"></script>
    <script type="text/javascript" src="{{ asset('frontend') }}/public/library/meanmenu/meanmenu.js"></script>
    <script type="text/javascript" src="{{ asset('frontend') }}/public/library/slider-pro/slider-pro.min.js">
    </script>
    <script type="text/javascript"
        src="{{ asset('frontend') }}/public/library/owl-carousel/v2.3.4/owl.carousel.min.js"></script>
    <script type="text/javascript" src="{{ asset('frontend') }}/public/library/pnotify/pnotify.js"></script>
    <script type="text/javascript" src="{{ asset('frontend') }}/public/library/wow/wow.min.js"></script>
    <script type="text/javascript" src="{{ asset('frontend') }}/public/library/magnific-popup/magnific-popup.min.js">
    </script>
    <script type="text/javascript" src="{{ asset('frontend') }}/public/library/instagram/instafeed.min.js"></script>
    <script type="text/javascript" src="{{ asset('frontend') }}/public/library/signature/signature.min.js"></script>
    <script type="text/javascript" src="{{ asset('frontend') }}/public/library/global/js/js_php.js"></script>

    <style type="text/css">
        .page-heading {
            padding-top: 40px;
            padding-bottom: 10px;
        }

        @media only screen and (max-width: 687px) {
            .page-heading {
                padding-top: 12px;
            }
        }

        .info_staff {
            display: none;
        }

        h4 {
            font-size: 22px;
        }

        .section p {
            font-size: 13px;
        }

        .parallax h3 {
            font-size: 28px;
        }

        .menu_mobile_v1 {
            background: #dcdcdc;
        }

        #our-services .info p {
            display: none;
        }

        .section-beautiful {
            background-image: url(/uploads/fnail07ri1nsb/filemanager/BG.jpg);
        }

        .gallery-box h4 {
            font-size: 16px;
            text-align: center;
        }

        section#our-services img {
            width: 100%;
        }

        .mobile_logo {
            margin: 5px;
        }

        .mobile_logo img {
            max-height: 100px;
        }

        .mean-container .mean-nav {
            margin-top: 110px;
        }

        .contact-details {
            padding: 30px 1px 20px 1px;
        }

    </style>

    <!-- Webmaster tools -->
    <!--Webmaster tools-->
    <!-- fb retargeting -->

    <!-- Schema Script -->
    <meta name="csrf-token" content="{{ csrf_token() }}" />
</head>

<body>
    <h1 itemprop="name" style="display: none">Nails Spa</h1>
    <div id="fb-root"></div>
    <!--
# Active freeze header by insert this html into the website page
# value = "1": Mobile and Desktop;
# value = "2": Mobile;
# value = "3": Desktop;
-->
    <!--<input type="hidden" name="activeFreezeHeader" value="1">-->
    {{-- HEADER --}}
    @include('frontend.nail04-h.component.header',
    ['header'=>
    App\Models\Product::where('category_id',
    App\Models\Category::where('name','Header And Footer')->first()['id'])->first()])
    {{-- MAIN --}}
    {{ $slot }}
    {{-- FOOTER --}}
    @php
        $album = App\Models\Album::where([['type', 'Social'], ['status', 'PUBLISHED']])->first();
        $social = App\Models\Asset::where('albums_id', $album['id'])->orderBy('priority', 'desc')->get();
    @endphp
    @include('frontend.nail04-h.component.footer',['header'=>
    App\Models\Product::where('category_id',
    App\Models\Category::where('name','Header And Footer')->first()['id'])->first(),
    'social'=>$social]))
    <script type="text/javascript">
        var enable_booking = "1";
        if (enable_booking == 0) {
            $(".btn_make_appointment").remove();
        }

    </script>

    <script type="text/javascript" src="{{ asset('frontend') }}/public/library/global/js/consent.js"></script>
    <script type="text/javascript" src="{{ asset('frontend') }}/themes/fnail04h/assets/js/script.js"></script>
    <script type="text/javascript" src="{{ asset('frontend') }}/themes/fnail04h/assets/js/app.js?v=1.0"></script>

    <!-- Google analytics -->
    <script>
        (function(i, s, o, g, r, a, m) {
            i['GoogleAnalyticsObject'] = r;
            i[r] = i[r] || function() {
                (i[r].q = i[r].q || []).push(arguments)
            }, i[r].l = 1 * new Date();
            a = s.createElement(o),
                m = s.getElementsByTagName(o)[0];
            a.async = 1;
            a.src = g;
            m.parentNode.insertBefore(a, m)
        })(window, document, 'script', 'https://www.google-analytics.com/analytics.js', 'ga');

        ga('create', '', 'auto');
        ga('send', 'pageview');

    </script>
    <script type="text/javascript">
        var enable_booking = "1";
        if (enable_booking == 0) {
            $(".btn_make_appointment").remove();
        }

    </script>
    <!-- End - Google analytics -->
    <!-- gg adwords remarketing -->

    <script type="text/javascript"></script>
    <script>
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

    </script>
    @livewireScripts
</body>

</html>
