@php
$features = json_decode($header['features']);
$extras = json_decode($header['extras']);
@endphp
<footer class="footer">
    <div class="footer-copyright">
        <div class="container">
            <div class="row">
                <div class="col-md-6 col-xs-12 pull-right">
                    <ul class="head-top-social">
                        @foreach ($social as $item)
                            <li>
                                @if ($item['url'])
                                    <a itemprop="url" rel="nofollow" target="_blank" title="{{ $item['title'] }}"
                                        href="{{ $item['name'] }}">
                                        <img src="{{ asset('storage') }}/photos/{{ $item['url'] }}"
                                            alt="{{ $item['title'] }}" />
                                    </a>
                                @endif
                            </li>
                        @endforeach
                    </ul>
                </div>
                <div class="col-md-6 col-xs-12 copyright pull-left">
                    <p>© Copyright by Nails Spa. All Rights Reserved.</p>
                </div>
            </div>
        </div>
    </div>
</footer>
<!-- Active freeze footer by delete style display: none -->
<div class="freeze-footer">
    <ul>
        <li>
            <a href="tel:{{ explode(',', $features[1]->desc)[0] }}" class="btn btn-main btn_call_now btn-call">
                {{ explode(',', $features[1]->desc)[0] }}
            </a>
        </li>
        <li><a href="/book" class="btn btn-main btn_make_appointment">Booking</a></li>
    </ul>
</div>
