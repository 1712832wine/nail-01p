<header class="header">
    <div class="wrap-freeze-header-mobile clearfix hidden-md hidden-lg menu-1024-hidden">
        <div class="flag-freeze-header-mobile">
            <div class="menu_mobile_v1">
                <div class="mobile_logo">
                    @if (count(json_decode($header['images'])) > 0)
                        <a href="/">
                            <img src="{{ asset('storage') }}/photos/{{ json_decode($header['images'])[0] }}"
                                class="imgrps" alt="Nails Spa">
                        </a>
                    @endif
                </div>
                <div class="mobile_menu_container_v1">
                    <div class="mobile-menu clearfix">
                        <nav id="mobile_dropdown">
                            <ul>
                                <li><a itemprop="url" href="/" title="Home">Home</a></li>
                                <li><a itemprop="url" class="scrol_action" data-href="about" href="/about"
                                        title="About Us">About Us</a></li>
                                <li><a itemprop="url" href="/services" class="scrol_action" data-href="service"
                                        title="Services">Services</a></li>
                                <li><a itemprop="url" href="/book" title="Booking">Booking</a></li>
                                <li><a itemprop="url" href="/coupons" title="Coupons">Coupons</a></li>
                                <li><a itemprop="url" href="/gallery" class="scrol_action" data-href="gallery"
                                        title="Gallery">Gallery</a></li>
                                <li><a itemprop="url" href="/contact" class="scrol_action" data-href="contact"
                                        title="Contact Us">Contact Us</a></li>
                            </ul>
                        </nav>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="wrap-freeze-header clearfix hidden-xs hidden-sm">
        <div class="flag-freeze-header">
            <div class="header-main">
                <div class="container">

                    <div class="header-nav dflex-sb">
                        @if (count(json_decode($header['images'])) > 0)
                            <a href="/" class="logo">
                                <img src="{{ asset('storage') }}/photos/{{ json_decode($header['images'])[0] }}"
                                    class="imgrps" alt="Nails Spa" itemprop="image">
                            </a>
                        @endif
                        <div class="head-nav-right dflex">
                            <nav class="navbar">
                                <div class="navbar-collapse collapse">
                                    <ul class="nav navbar-nav">
                                        <li><a itemprop="url" href="/" title="Home">Home</a></li>
                                        <li><a itemprop="url" class="scrol_action" data-href="about" href="/about"
                                                title="About Us">About Us</a></li>
                                        <li><a itemprop="url" href="/services" class="scrol_action" data-href="service"
                                                title="Services">Services</a></li>
                                        <li><a itemprop="url" href="/book" title="Booking">Booking</a></li>
                                        <li><a itemprop="url" href="/coupons" title="Coupons">Coupons</a></li>
                                        <li><a itemprop="url" href="/gallery" class="scrol_action" data-href="gallery"
                                                title="Gallery">Gallery</a></li>
                                        <li><a itemprop="url" href="/contact" class="scrol_action" data-href="contact"
                                                title="Contact Us">Contact Us</a></li>
                                    </ul>
                                </div>
                            </nav>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

</header>
