<main>
    <div class="pmain">
        <div class="page-heading">
            <div class="container">
                <h2>Coupon</h2>
            </div>
        </div>
        <div class="in-container">
            <div class="container">
                <div class="in-content">
                    <section class="coupon_code_v1">
                        <div class="row ">
                            @foreach ($list as $item)
                                <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6 col-coupont">
                                    <div title="Coupon" href="{{ asset('storage') }}/photos/{{ $item['url'] }}"
                                        class="coupon_img gallery-item">
                                        <img class="img-responsive img_size"
                                            src="{{ asset('storage') }}/photos/{{ $item['url'] }}" alt="Coupon">
                                    </div>
                                </div>
                            @endforeach
                        </div>
                    </section>
                </div>
            </div>
        </div>
    </div>
</main>
