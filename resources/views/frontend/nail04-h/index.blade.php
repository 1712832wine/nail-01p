@php
$features = json_decode($header['features']);
$extras = json_decode($header['extras']);
@endphp
<main>
    <div class="pmain">
        {{-- CAROUSEL --}}
        <section class="section-slider">
            <div class="slider-pro" id="my-slider">
                <div class="sp-slides">
                    @foreach ($imgs_carousel as $img)
                        <div class="sp-slide">
                            <a href="/">
                                <img class="sp-image" src="{{ asset('storage') }}/photos/{{ $img['url'] }}"
                                    alt="{{ $img['url'] }}" />
                            </a>
                        </div>
                    @endforeach
                </div>
            </div>
        </section>
        {{-- HOME ARTICLE --}}
        <section class="section section-about" id="about-us">
            <div class="container">
                <h2>{{ $home_article['title'] }}</h2>
                <p style="text-align: center;">{!! $home_article['content'] !!}</p>
            </div>
        </section>
        {{-- IMAGES 1 --}}
        <section class="section section-feel parallax">
            <div class="container">
                <h3>More Services & More Best Choice</h3>
            </div>
        </section>
        {{-- SERVICES --}}
        <div id="service">
            <section class="section section-stylist" id="our-services">
                <div class="container">
                    <div class="owl-cus-service owl-carousel owl-theme">
                        @foreach ($services as $service)
                            <div class="item">
                                <div class="thumb">
                                    <a
                                        href="{{ route('services-detail', ['slug' => strtolower(trim(preg_replace('/[^A-Za-z0-9-]+/', '-', $service['name']))), 'service_id' => $service['id']]) }}">
                                        <img src="{{ asset('storage') }}/photos/{{ json_decode($service['images'])[0] }}"
                                            alt="{{ $service['name'] }}" class="imgrps">
                                    </a>
                                </div>
                                <div class="info">
                                    <h5>
                                        <a
                                            href="{{ route('services-detail', ['slug' => strtolower(trim(preg_replace('/[^A-Za-z0-9-]+/', '-', $service['name']))), 'service_id' => $service['id']]) }}">{{ $service['name'] }}</a>
                                    </h5>
                                    <p></p>
                                </div>
                            </div>
                        @endforeach
                    </div>
                </div>
            </section>
        </div>
        {{-- IMAGES 2 --}}
        <section class="section section-love parallax">
            <div class="container">
                <h3>Come in and relax while the Professional Nail Care <br /> experts transform your nails</h3>
            </div>
        </section>
        {{-- ABOUT ARTICLES --}}
        <section class="section section-picture" id="our-products">
            <div class="container">
                @foreach ($home_asset as $item)
                    <div class="col-sm-4">
                        <h4 style="text-align: center;">
                            <img class="size-full wp-image-525 aligncenter"
                                src="{{ asset('storage') }}/photos/{{ $item['url'] }}"
                                alt="{{ $item['name'] }}" caption="false" width="84" height="84" />
                            <br /><br />{{ $item['name'] }}
                        </h4>
                        <p style="text-align: center;">
                            {{ $item['short_desc'] }}
                        </p>
                    </div>
                @endforeach
            </div>
        </section>
        {{-- IMAGES 3 --}}
        <section class="section section-beautiful parallax">
            <div class="container">
                <h3>Be beautiful</h3>
            </div>
        </section>
        <div id="contact">
            <section class="section section-contact" id="contact-us">
                <div class="container">
                    <h2 class="stitle">Contact us</h2>

                    <div class="row">
                        <div class="col-sm-6">
                            <h5>Nails Spa</h5>
                            <div class="contact-home">
                                <div class="cd-col">

                                    <p><strong class="icon">Address:</strong>{{ $features[0]->desc }}
                                    </p>
                                </div>
                                <div class="cd-col">

                                    <p> <strong class="icon">Phone:</strong>
                                        @foreach (explode(',', $features[1]->desc) as $item)
                                            <a href="tel:{{ $item }}" itemprop="url">{{ $item }}</a>
                                        @endforeach
                                    </p>
                                </div>
                                <div class="cd-col">

                                    <p><strong class="icon">Email:</strong>
                                        @foreach (explode(',', $features[2]->desc) as $item)
                                            <a href="mailto:{{ $item }}"
                                                itemprop="url">{{ $item }}</a>

                                        @endforeach
                                    </p>
                                </div>
                            </div>

                            <div class="footer-3-box">
                                <div class="custom-form-2">
                                    <div class="form-contact-us-index">
                                        <div class="form-input-infor">
                                            <form enctype="multipart/form-data" method="post" name="send_contact"
                                                id="send_contact" action="{{ route('send_contact') }}">
                                                @csrf
                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <input title="your name" type="text"
                                                                data-validation="[NOTEMPTY]"
                                                                data-validation-message="Please enter your name!"
                                                                autocomplete="off" class="form-control style-input"
                                                                placeholder="Your Name" name="contactname">
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <input title="Your Email" type="text"
                                                                data-validation="[EMAIL]"
                                                                data-validation-message="Invalid email"
                                                                autocomplete="off" class="form-control style-input"
                                                                placeholder="Your Email" name="contactemail">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <div class="form-group">
                                                            <input title="Your Subject" type="text"
                                                                data-validation="[NOTEMPTY]"
                                                                data-validation-message="Please enter a subject!"
                                                                autocomplete="off" class="form-control style-input"
                                                                placeholder="Your Subject" name="contactsubject">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-xs-12">
                                                        <textarea title="Your Message" rows="10"
                                                            data-validation="[NOTEMPTY]"
                                                            data-validation-message="Please enter a content!"
                                                            autocomplete="off"
                                                            class="form-control style-input style-textarea"
                                                            placeholder="Your Message" name="contactcontent"></textarea>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-xs-12">
                                                        <div class="form-group">
                                                            @if (session('status'))
                                                                <div class="alert alert-success" role="alert">
                                                                    <button type="button" class="close"
                                                                        data-dismiss="alert">×</button>
                                                                    {{ session('status') }}
                                                                </div>
                                                            @elseif(session('failed'))
                                                                <div class="alert alert-danger" role="alert">
                                                                    <button type="button" class="close"
                                                                        data-dismiss="alert">×</button>
                                                                    {{ session('failed') }}
                                                                </div>
                                                            @endif
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="row">
                                                    <div class="col-xs-12">
                                                        <div class="btn-sauna form-group">
                                                            <button type="submit"
                                                                class="btn btn-main btn-submit btn_contact"> Send
                                                                message </button>
                                                        </div>
                                                    </div>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>


                        </div>
                        <div class="col-sm-6">
                            <div class="map-address">
                                <section class="box_map">
                                    <!-- Google map area -->
                                    <div class="google-map-wrapper">
                                        <div class="google-map" id="map" style="width:100%; height:450px;"><iframe
                                                src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d6929.489650551249!2d-95.570685!3d29.727150000000005!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x74ace03ed50728e8!2sFast%20Boy%20Marketing!5e0!3m2!1sen!2sus!4v1569202339382!5m2!1sen!2sus"
                                                width="100%" height="450" frameborder="0" style="border:0;"
                                                allowfullscreen=""></iframe></div>
                                    </div>
                                </section>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </div>
    </div>
</main>
