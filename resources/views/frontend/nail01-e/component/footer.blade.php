@php
$features = json_decode($header['features']);
$extras = json_decode($header['extras']);
@endphp
<footer class="footer">
    <div class="container">
        <footer class="footer-container">
            <div class="row">
                <div class="col-md-4">
                    <h3>{{ $header['name'] }}</h3>
                    <div class="footer-info">
                        <p style="text-align: justify;">{{ $header['description'] }}</p>
                    </div>
                    <div class="footer-social">
                        @foreach ($social as $item)
                            @if ($item['url'])
                                <a itemprop="url" class="social-img-icons" rel="nofollow" target="_blank"
                                    title="{{ $item['title'] }}" href="{{ $item['content'] }}"><img
                                        src="{{ asset('storage') }}/photos/{{ $item['url'] }}"
                                        alt="{{ $item['title'] }}"></a>
                            @endif
                        @endforeach
                    </div>
                </div>
                <div class="col-sm-6 col-md-4">
                    <h3>Contact us</h3>
                    <div class="footer-address">
                        <div class="fadd"><i class="fa fa-home"></i>
                            <div class="fadd-info">
                                <h4>Address:</h4>
                                <p>{{ $features[0]->desc }}</p>
                            </div>
                        </div>
                        <div class="fadd"><i class="fa fa-phone"></i>
                            <div class="fadd-info">
                                <h4>Phone</h4>
                                <p><a href="tel:{{ $features[1]->desc }}">{{ $features[1]->desc }}</a></p>
                            </div>
                        </div>
                        <div class="fadd"><i class="fa fa-envelope"></i>
                            <div class="fadd-info">
                                <h4>Email:</h4>
                                <p><a href="mailto:{{ $features[2]->desc }}">{{ $features[2]->desc }}</a></p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-sm-6 col-md-4">
                    <h3>Location</h3>
                    <div class="footer-map">
                        <section class="box_map clearfix">
                            <div class="google-map-wrapper">
                                <div class="google-map" style="width:100%; height:auto;"><iframe
                                        src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3464.7391505606774!2d-95.57299108550652!3d29.727314340440024!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x8640ddc6ed681049%3A0x74ace03ed50728e8!2sFast+Boy+Marketing!5e0!3m2!1sen!2s!4v1510909605272"
                                        width="100%" height="450" frameborder="0" style="border:0"
                                        allowfullscreen></iframe></div>
                            </div>
                        </section>
                    </div>
                </div>
            </div>
            <div class="footer-copyright clearfix">
                <p class="copyright">© Copyright by FNAIL01E Nails & Spa. All Rights Reserved.</p>
                <div class="footer-bank">
                    <a href="/">
                        <img src="{{ asset('frontend') }}/themes/fnail01e/assets/images/icon-bank-1.png"
                            class="imgrps" alt="" itemprop="image" />
                    </a>
                    <a href="/">
                        <img src="{{ asset('frontend') }}/themes/fnail01e/assets/images/icon-bank-2.png"
                            class="imgrps" alt="" itemprop="image" />
                    </a>
                    <a href="/">
                        <img src="{{ asset('frontend') }}/themes/fnail01e/assets/images/icon-bank-3.png"
                            class="imgrps" alt="" itemprop="image" />
                    </a>
                    <a href="/">
                        <img src="{{ asset('frontend') }}/themes/fnail01e/assets/images/icon-bank-4.png"
                            class="imgrps" alt="" itemprop="image" />
                    </a>
                    <a href="/">
                        <img src="{{ asset('frontend') }}/themes/fnail01e/assets/images/icon-bank-5.png"
                            class="imgrps" alt="" itemprop="image" />
                    </a>
                </div>
            </div>
        </footer>
    </div>
</footer>
<div class="freeze-footer">
    <ul>
        <li><a href="tel:{{ $features[1]->desc }}" class="btn btn-default btn_call_now btn-call"
                title="Call us">{{ $features[1]->desc }}</a>
        </li>
        <li><a href="/book" class="btn btn-default btn_make_appointment" title="Booking">Booking</a></li>
    </ul>
</div>
