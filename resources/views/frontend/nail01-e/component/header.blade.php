@php
$features = json_decode($header['features']);
$extras = json_decode($header['extras']);
@endphp
<header class="header">
    <div class="container">
        <div class="header-top hidden-xs hidden-sm">
            <div class="header-top-left">{{ $features[0]->desc }}</div>
            <div class="header-top-right">Call for Appointment: <a
                    href="tel:{{ $features[1]->desc }}">{{ $features[1]->desc }}</a>
            </div>
        </div><!-- .header-top -->
    </div>

    <div class="wrap-freeze-header clearfix hidden-xs hidden-sm">
        <div class="flag-freeze-header">
            <div class="container">
                <div class="header-main clearfix hidden-xs hidden-sm">
                    <h1 class="logo">

                        @if (count(json_decode($header['images'])) > 0)
                            <a itemprop="url" href="/">
                                <img class="imgrps"
                                    src="{{ asset('storage') }}/photos/{{ json_decode($header['images'])[0] }}"
                                    alt="{{ $header['title'] }}" itemprop="logo image">
                            </a>
                        @endif
                    </h1>
                    <nav class="navbar main-nav hidden-xs">
                        <div id="desktop-nav" class="navbar-collapse collapse">

                            <ul class="nav navbar-nav nav-main">
                                <li class="left" itemprop="name"><a href="/" itemprop="url">Home</a></li>
                                <li class="left" itemprop="name"><a href="/about" itemprop="url">About
                                        Us</a></li>
                                <li class="left" itemprop="name"><a href="/services" itemprop="url">Services</a>
                                </li>
                                <li class="left" itemprop="name"><a href="/giftcards" itemprop="url">Gift
                                        Cards</a></li>
                            </ul>
                            <ul class="nav navbar-nav nav-main right">
                                <li class="right" itemprop="name"><a itemprop="url" href="/book">Booking</a></li>
                                <li class="right" itemprop="name"><a href="/gallery" itemprop="url">Gallery</a>
                                </li>
                                <li class="right" itemprop="name"><a href="/coupons" itemprop="url">Coupons</a>
                                </li>
                                <li class="right" itemprop="name"><a href="/contact" itemprop="url">Contact
                                        Us</a></li>
                            </ul>
                        </div>
                    </nav>
                </div><!-- .header-main-->
            </div>
        </div>
    </div>

    <div class="wrap-freeze-header-mobile clearfix hidden-md hidden-lg">
        <div class="flag-freeze-header-mobile">
            <div class="mobile-nav ">
                <div class="mobile_logo">
                    @if (count(json_decode($header['images'])) > 0)
                        <a itemprop="url" href="/">
                            <img class="imgrps"
                                src="{{ asset('storage') }}/photos/{{ json_decode($header['images'])[0] }}"
                                alt="{{ $header['title'] }}" itemprop="logo image">
                        </a>
                    @endif
                </div>
                <div class="menu_mobile_v1 hidden-md hidden-lg menu-1024-hidden">
                    <div class="mobile_menu_container_v1">
                        <div class="mobile-menu clearfix">
                            <nav id="mobile_dropdown">
                                <ul>
                                    <li class="left" itemprop="name"><a href="/" itemprop="url">Home</a></li>
                                    <li class="left" itemprop="name"><a href="/about" itemprop="url">About
                                            Us</a></li>
                                    <li class="left" itemprop="name"><a href="/services" itemprop="url">Services</a>
                                    </li>
                                    <li class="left" itemprop="name"><a href="/giftcards" itemprop="url">Giftcards</a>
                                    </li>
                                    <li class="right" itemprop="name"><a itemprop="url" href="/book">Booking</a>
                                    </li>
                                    <li class="right" itemprop="name"><a href="/gallery" itemprop="url">Gallery</a>
                                    </li>
                                    <li class="right" itemprop="name"><a href="/coupons" itemprop="url">Coupons</a>
                                    </li>
                                    <li class="right" itemprop="name"><a href="/contact" itemprop="url">Contact
                                            Us</a></li>
                                </ul>
                            </nav>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

</header>
