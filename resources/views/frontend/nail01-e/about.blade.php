<main>
    <div class="pmain">

        <section class="p-about">
            <!-- tpl main -->
            <div class="in-head">
                <div class="container">
                    <div class="in-headbg">
                        <h2 class="mTitle" itemprop="name">About us</h2>
                    </div>
                </div>
            </div>
            <div class="container">
                <div class="in-container">
                    <div class="in-content">
                        <div class="section-about about-page">
                            @foreach ($articles as $item)
                                <div class="row">
                                    <div class="col-md-7">
                                        <div class="about-content">
                                            <h2>Welcome to</h2>
                                            <h3>{{ $item['title'] }}</h3>
                                            {!! $item['content'] !!}
                                        </div>
                                    </div>
                                    <div class="col-md-5 about-img">
                                        @if (count(json_decode($item['image'])) > 0)
                                            <img class="imgrps"
                                                src="{{ asset('storage') }}/photos/{{ json_decode($item['image'])[0] }}"
                                                itemprop="image" caption="false" />
                                        @endif
                                    </div>
                                </div>
                            @endforeach
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
</main>
