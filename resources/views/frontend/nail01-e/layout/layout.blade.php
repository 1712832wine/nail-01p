<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" dir="ltr" lang="en" xmlns:og="http://ogp.me/ns#"
    xmlns:fb="http://www.facebook.com/2008/fbml" itemscope itemtype="https://schema.org/NailSalon">

<head>
    <!-- Language -->
    <meta http-equiv="content-language" content="en" />
    <link rel="alternate" href="" hreflang="x-default">
    <link rel="alternate" href="" hreflang="en-US">

    <!-- Meta limit -->

    <!-- Meta Tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width,initial-scale=1.0" />
    <meta http-equiv="content-type" content="text/html; charset=UTF-8" />
    <meta name="description" content="" />
    <meta name="keywords" content="" />
    <meta name="author" content="" />

    <!-- OG -->
    <meta property="og:title" content="" />
    <meta property="og:description" content="" />
    <meta property="og:image" content="" />
    <meta property="og:url" content="/" />
    <meta property="og:type" content="website" />
    <meta property="og:site_name" content="FNAIL01E Nails & Spa" />

    <!-- Dublin Core -->
    <link rel="schema.DC" href="http://purl.org/dc/elements/1.1/">
    <meta name="DC.title" content="">
    <meta name="DC.identifier" content="/">
    <meta name="DC.description" content="">
    <meta name="DC.subject" content="">
    <meta name="DC.language" scheme="UTF-8" content="en-us">
    <meta itemprop="priceRange" name="priceRange" content="">

    <!-- GEO meta -->
    <meta name="geo.region" content="">
    <meta name="geo.placename" content="">
    <meta name="geo.position" content="">
    <meta name="ICBM" content="">

    <!-- Page Title -->
    <title>FNAIL01E Nails & Spa</title>
    <base href="frontend/themes/fnail01e/assets/">

    <!-- canonical -->
    <link rel="canonical" href="">



    <!-- Favicons -->
    <link rel="icon" href="/uploads/demofnvti17i0/attach/1562751554_fbm_fvc.png" type="image/x-icon">
    <link rel="shortcut icon" href="/uploads/demofnvti17i0/attach/1562751554_fbm_fvc.png" type="image/x-icon">

    <!-- Css -->
    <link href="https://fonts.googleapis.com/css?family=Montserrat:100,300,400,600,700,800,900&amp;subset=vietnamese"
        rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Cookie" rel="stylesheet">

    <link rel="stylesheet" type="text/css" href='{{ asset('frontend') }}/public/library/jquery-ui/jquery-ui.min.css'>
    <link rel="stylesheet" type="text/css" href='{{ asset('frontend') }}/public/library/animate/animate.css'>
    <link rel="stylesheet" type="text/css" href='{{ asset('frontend') }}/public/library/validation/validation.css'>
    <link rel="stylesheet" type="text/css" href='{{ asset('frontend') }}/public/library/flex-label/flex-label.css'>
    <link rel="stylesheet" type="text/css"
        href='{{ asset('frontend') }}/public/library/bootstrap/v3.3.7/bootstrap.min.css'>
    <link rel="stylesheet" type="text/css"
        href='{{ asset('frontend') }}/public/library/bootstrap-datetimepicker/bootstrap-datetimepicker.min.css'>
    <link rel="stylesheet" type="text/css"
        href='{{ asset('frontend') }}/public/library/font-awesome/v4.7.0/css/font-awesome.min.css'>
    <link rel="stylesheet" type="text/css"
        href='{{ asset('frontend') }}/public/library/slider-pro/slider-pro.min.css'>
    <link rel="stylesheet" type="text/css"
        href='{{ asset('frontend') }}/public/library/owl-carousel/v2.2.1/owl.carousel.min.css'>
    <link rel="stylesheet" type="text/css"
        href='{{ asset('frontend') }}/public/library/magnific-popup/magnific-popup.css'>
    <link rel="stylesheet" type="text/css" href='{{ asset('frontend') }}/public/library/pnotify/pnotify.css'>
    <link rel="stylesheet" type="text/css" href='{{ asset('frontend') }}/public/library/instagram/instafeed.min.css'>
    <link rel="stylesheet" type="text/css" href='{{ asset('frontend') }}/public/library/signature/signature.css'>
    <style type="text/css">
        .style2 .header-top,
        .style2 .navbar-nav>li>a:hover,
        .style2 .navbar-nav>li>a:active,
        .style2 .navbar-nav>li>a:focus,
        .style2 .navbar-nav>li.active>a,
        .style2 .navbar-nav>li:hover>a,
        .style2 .navbar-nav .dropdown-menu,

        .style2 .btn-primary,
        .style2 a.btn-primary,
        .style2 .table-cart .thead .table-head,
        .style2 .cart .order {
            background-color: #e64077;
        }

        .style2 .header-main,
        .style2 .section-service h3,
        .style2 .btn-primary:focus,
        .style2 .btn-primary:hover,
        .style2 a.btn-primary:hover,

        .style2 .btn:hover,
        .style2 .footer-address i,
        .style2 .section-vt {
            background-color: #d46597;
        }


        .style2 .footer-container {
            background-color: #e64077;
        }

        .style2 .footer-container,
        .style2 .footer h3,
        .style2 .header-top,
        .style2 .header-top-right a,
        .style2 .navbar-nav>li>a:hover,
        .style2 .navbar-nav>li>a:active,
        .style2 .navbar-nav>li>a:focus,
        .style2 .navbar-nav>li.active>a,
        .style2 .navbar-nav>li:hover>a,
        .style2 .btn-primary,
        .style2 a.btn-primary,
        .style2 .cart .order,
        .style2 .cart .order .total ul li span,
        .style2 .footer p.copyright {
            color: ;

        }

        .title_gift,
        .service-row h2,
        .style2 .service-name,
        .style2 .mTitle,
        .style2 .info_inner_booking h2,
        .style2 .section_title,
        .style2 .section-about h2,
        .style2 .section-about h3,
        .style2 .section-about a,
        .style2 .contact-page h3,
        .style2 h1.service-name,
        .style2 .detail-price-number,
        .style2 .price-item-number {
            color: #e64077;
        }

        .style2 .footer-copyright {
            border-top: 1px solid #ffffff;
        }

        .style2 .footer-map iframe {
            border: 5px solid #e64077;
        }

        .style2 .footer-address i,
        .style2 .navbar-nav>li>a,
        .style2 .section-service h3,
        .style2 .section-vt h2,
        .style2 .vt-time,
        .style2 .section-service h3 a {
            color: #ffffff;
        }

        .pagination>li.active>a.hover-main-color,
        .pagination>li.active>span.hover-main-color,
        .pagination>li>a:hover.hover-main-color,
        .pagination>li>span:hover.hover-main-color {
            background-color: #e64077;
            color: ;
            border: 1px solid #e64077;
        }

        .box_img_giftcard.active,
        .box_img_giftcard:hover {
            border-color: #e64077;
        }

        .box_img_giftcard:hover .circle_check,
        .box_img_giftcard.active .circle_check {
            background: #e64077;
        }

    </style>
    <link rel="stylesheet" type="text/css"
        href='{{ asset('frontend') }}/themes/fnail01e/assets/css/ie10-viewport-bug-workaround.css'>
    <link rel="stylesheet" type="text/css" href='{{ asset('frontend') }}/themes/fnail01e/assets/css/meanmenu.css'>
    <link rel="stylesheet" type="text/css"
        href='{{ asset('frontend') }}/themes/fnail01e/assets/css/dropdown-submenu.css'>
    <link rel="stylesheet" type="text/css" href='{{ asset('frontend') }}/themes/fnail01e/assets/css/style.css'>
    <link rel="stylesheet" type="text/css" href='{{ asset('frontend') }}/themes/fnail01e/assets/css/responsive.css'>
    <link rel="stylesheet" type="text/css" href='{{ asset('frontend') }}/public/library/global/css/news.css'>
    <link rel="stylesheet" type="text/css" href='{{ asset('frontend') }}/public/library/global/css/consent.css'>
    <link rel="stylesheet" type="text/css"
        href='{{ asset('frontend') }}/themes/fnail01e/assets/css/custom.css?v=1.5'>
    <style type="text/css">
        .navbar-nav>li>a {
            padding: 12px 15px;
        }

        .navbar.main-nav {
            padding: 23px 5px;
        }

        p.copyright {
            font-size: 10px;
            color: #fff !important;
        }

        .footer-map iframe {
            border: 3px solid #fff !important;
        }

        .fadd a {
            color: #fff;
            font-weight: bold;
        }

        .price-item h4 {
            font-weight: bold;
        }

        .social-img-icons img {
            background-color: #fff;
        }

        .booking_staff_title {
            display: none;
        }

        .in-headbg {
            background: url(images/BG.png) 0px 95% no-repeat;
            background-size: cover;
        }

        .img-info-staff {
            display: none;
        }

        .service-name {
            font-family: "Montserrat", Sans-Serif !important;
            font-size: 20px !important;
            font-weight: 600;
        }

        .service-name {
            font-family: "Montserrat", Sans-Serif !important;
            font-size: 20px !important;
            font-weight: 600;
        }

        .section-gallery {
            background-color: #fff;
        }

        .section-gallery h2 {
            color: #e64077;
            font-family: &#39;
            Cookie&#39;
            ,
            cursive;
            font-size: 44px;
        }

    </style>

    <!-- Go to www.addthis.com/dashboard to customize your tools -->
    <script type="text/javascript"
        src="{{ asset('frontend') }}//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-59b8fec5dd14e0f2"></script>
    <script type="text/javascript">
        var addthis_share = {
            url: "/",
            title: "FNAIL01E Nails & Spa",
            description: "",
            media: ""
        }

    </script>
    <!-- Script -->


    <script type="text/javascript" src="{{ asset('frontend') }}/public/library/jquery/v1.10.2/jquery.min.js">
    </script>
    <script type="text/javascript" src="{{ asset('frontend') }}/public/library/jquery-ui/jquery-ui.min.js"></script>
    <script type="text/javascript" src="{{ asset('frontend') }}/public/library/jquery-ui/jquery-migrate.min.js">
    </script>
    <script type="text/javascript" src="{{ asset('frontend') }}/public/library/validation/validation.min.js">
    </script>
    <script type="text/javascript" src="{{ asset('frontend') }}/public/library/mask/mask.min.js"></script>
    <script type="text/javascript" src="{{ asset('frontend') }}/public/library/flex-label/flex-label.js"></script>
    <script type="text/javascript" src="{{ asset('frontend') }}/public/library/bootstrap/v3.3.7/bootstrap.min.js">
    </script>
    <script type="text/javascript"
        src="{{ asset('frontend') }}/public/library/waiting-modal-dialog/waiting-modal-dialog.js"></script>
    <script type="text/javascript" src="{{ asset('frontend') }}/public/library/moment/moment-with-locales.js">
    </script>
    <script type="text/javascript"
        src="{{ asset('frontend') }}/public/library/bootstrap-datetimepicker/bootstrap-datetimepicker.js"></script>
    <script type="text/javascript" src="{{ asset('frontend') }}/public/library/magnific-popup/magnific-popup.min.js">
    </script>
    <script type="text/javascript" src="{{ asset('frontend') }}/public/library/pnotify/pnotify.js"></script>
    <script type="text/javascript" src="{{ asset('frontend') }}/public/library/slider-pro/slider-pro.min.js">
    </script>
    <script type="text/javascript"
        src="{{ asset('frontend') }}/public/library/owl-carousel/v2.2.1/owl.carousel.min.js"></script>
    <script type="text/javascript" src="{{ asset('frontend') }}/public/library/wow/wow.min.js"></script>
    <script type="text/javascript" src="{{ asset('frontend') }}/public/library/meanmenu/meanmenu.js"></script>
    <script type="text/javascript" src="{{ asset('frontend') }}/public/library/instagram/instafeed.min.js"></script>
    <script type="text/javascript" src="{{ asset('frontend') }}/public/library/signature/signature.min.js"></script>
    <script type="text/javascript" src="{{ asset('frontend') }}/themes/fnail01e/assets/js/responsiveslides.min.js">
    </script>
    <script type="text/javascript" src="{{ asset('frontend') }}/public/library/global/js/js_php.js"></script>

    <script type="text/javascript">
        var dateFormatBooking = "MM/DD/YYYY";
        var posFormat = "1,0,2";
        var timeMorning = '["10:00","10:30","11:00","11:30"]';
        var timeAfternoon =
            '["12:00","12:30","13:00","13:30","14:00","14:30","15:00","15:30","16:00","16:30","17:00","17:30","18:00","18:30","19:00"]';
        var bookLogin = "open_booking";
        var beforeTime = "1";
        var beforeDay = 0;
        var currDateT = "2021/05/24";
        var checktimebooking = 1;
        var enableRecaptcha = 0;
        var hoursTimeFormat = 12;
        var phoneFormat = "(000) 000-0000";
        var company_phone = "832-968-6668";
        var num_paging = "12";
        var site = "idx";

        let facebook_embed = {
            id_fanpage: "https://www.facebook.com/FastboyMarketingAgency",
            appId: "",
            width: 450,
            height: 300,
            tabs: "timeline",
            show_facepile: true,
            small_header: true,
            likebox_enable: true,
            likebox_show_faces: false,
            likebox_stream: true,
        };
        let google_id_fanpage = "";
        let twitter_id_fanpage = "";

    </script>

    <!-- Webmaster tools -->
    <!--Webmaster tools-->
    <!-- fb retargeting -->

    <!-- Schema Script -->
    <meta name="csrf-token" content="{{ csrf_token() }}" />
</head>

<body class="style2">
    <div class="art-main">

        <!-- Facebook Root And H1 Seo -->
        <h1 style="display: none"></h1>
        <div id="fb-root" style="display: none"></div>
        <!-- Tpl freeze header -->
        <input type="hidden" name="activeFreezeHeader" value="1" />
        @include('frontend.nail01-e.component.header',
        ['header'=>
        App\Models\Product::where('category_id',
        App\Models\Category::where('name','Header And Footer')->first()['id'])->first()])
        {{-- MAIN --}}
        {{ $slot }}
        {{-- FOOTER --}}
        @php
            $album = App\Models\Album::where([['type', 'Social'], ['status', 'PUBLISHED']])->first();
            $social = App\Models\Asset::where('albums_id', $album['id'])->orderBy('priority', 'desc')->get();
        @endphp
        @include('frontend.nail01-e.component.footer',['header'=>
        App\Models\Product::where('category_id',
        App\Models\Category::where('name','Header And Footer')->first()['id'])->first(),
        'social'=>$social]))
        <!-- popup -->

        <!-- external javascripts -->
        <script type="text/javascript" src="{{ asset('frontend') }}/public/library/global/js/consent.js"></script>
        <script type="text/javascript" src="{{ asset('frontend') }}/themes/fnail01e/assets/js/app.js?version=1.6">
        </script>
        <script type="text/javascript" src="{{ asset('frontend') }}/themes/fnail01e/assets/js/script.js?version=1.3">
        </script>

        <!-- Google analytics -->
        <script>
            (function(i, s, o, g, r, a, m) {
                i['GoogleAnalyticsObject'] = r;
                i[r] = i[r] || function() {
                    (i[r].q = i[r].q || []).push(arguments)
                }, i[r].l = 1 * new Date();
                a = s.createElement(o),
                    m = s.getElementsByTagName(o)[0];
                a.async = 1;
                a.src = g;
                m.parentNode.insertBefore(a, m)
            })(window, document, 'script', 'https://www.google-analytics.com/analytics.js', 'ga');
            ga('create', '', 'auto');
            ga('send', 'pageview');

        </script>

        <!-- gg adwords remarketing -->

        <!-- JS extends -->
        <script>
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

        </script>
        <script type="text/javascript"></script>
        @livewireScripts
    </div>
</body>

</html>
