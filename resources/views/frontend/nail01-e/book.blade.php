<main>
    <div class="pmain">

        <section class="p-book">
            <!-- tpl main -->


            <div class="in-head">
                <div class="container">
                    <div class="in-headbg">
                        <h2 class="mTitle" itemprop="name">Booking</h2>
                    </div>
                </div>
            </div>
            <div class="container">
                <div class="in-container">
                    <div class="in-content">
                        <script>
                            checktimebooking = 1;

                        </script>

                        <div class="row">
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <div class="content-shop bg-ctn-special">
                                    <div class="content-shop-booking">
                                        <script src='https://www.google.com/recaptcha/api.js?hl=en' async defer>
                                        </script><!-- Google reCaptcha -->
                                        <script type="text/javascript">
                                            <!--
                                            function ezyCaptcha_surveyForm(token, is_submit) {
                                                is_submit = 1;
                                                if ($("#password").length) {
                                                    //$("input:password").val(md5(clean_input($("#password").val())));
                                                }
                                                return true;
                                            }
                                            //

                                            -->
                                        </script>
                                        <form enctype="multipart/form-data" id="surveyForm" method="post"
                                            action="/book/add" class="form-horizontal">
                                            <div class="item-booking">
                                                <div class="clearfix" id="optionTemplate">
                                                    <div class="row">
                                                        <div class="col-md-6 group-select">
                                                            <label>Service(required)</label>
                                                            <div
                                                                style="display: inline-block; position: relative; width: 100%;">
                                                                <select name="product_id[]"
                                                                    class="list_service form-control"
                                                                    data-validation-message="Please choose service">
                                                                    <option value="" price="" staff="[]">Select
                                                                        Service</option>
                                                                    <!--List Categories-->
                                                                    <optgroup label="Manicure & Pedicure">
                                                                        <!--List service-->
                                                                        <option value="273" price="" staff='[]'
                                                                            catId="73">Manicure</option>
                                                                        <option value="274" price="" staff='[]'
                                                                            catId="73">Pedicure</option>
                                                                        <option value="275" price="" staff='[]'
                                                                            catId="73">Manicure & Pedicure
                                                                        </option>
                                                                        <option value="276" price="" staff='[]'
                                                                            catId="73">French (Extra)</option>
                                                                        <option value="277" price="" staff='[]'
                                                                            catId="73">French Mani & Pedi
                                                                        </option>
                                                                        <option value="278" price="" staff='[]'
                                                                            catId="73">Gel Manicure</option>
                                                                        <option value="279" price="" staff='[]'
                                                                            catId="73">Gel French Manicure
                                                                        </option>
                                                                        <option value="280" price="" staff='[]'
                                                                            catId="73">Spa Pedicure</option>
                                                                    </optgroup>
                                                                    <optgroup label="Refill">
                                                                        <!--List service-->
                                                                        <option value="281" price="" staff='[]'
                                                                            catId="74">Full Set</option>
                                                                        <option value="282" price="" staff='[]'
                                                                            catId="74">Overlay</option>
                                                                        <option value="283" price="" staff='[]'
                                                                            catId="74">Pink & White</option>
                                                                        <option value="284" price="" staff='[]'
                                                                            catId="74">Gel Pink & White</option>
                                                                        <option value="285" price="" staff='[]'
                                                                            catId="74">Gel Powder</option>
                                                                    </optgroup>
                                                                    <optgroup label="Full Set">
                                                                        <!--List service-->
                                                                        <option value="286" price="" staff='[]'
                                                                            catId="75">Full Set</option>
                                                                        <option value="287" price="" staff='[]'
                                                                            catId="75">Overlay</option>
                                                                        <option value="288" price="" staff='[]'
                                                                            catId="75">Pink & White</option>
                                                                        <option value="289" price="" staff='[]'
                                                                            catId="75">Gel Pink & White</option>
                                                                        <option value="290" price="" staff='[]'
                                                                            catId="75">Gel Powder</option>
                                                                    </optgroup>
                                                                    <optgroup label="Facial">
                                                                        <!--List service-->
                                                                        <option value="291" price="" staff='[]'
                                                                            catId="76">Full Facial</option>
                                                                        <option value="292" price="" staff='[]'
                                                                            catId="76">Facial Mini</option>
                                                                    </optgroup>
                                                                    <optgroup label="Additional">
                                                                        <!--List service-->
                                                                        <option value="293" price="" staff='[]'
                                                                            catId="77">Change Color Hands
                                                                        </option>
                                                                        <option value="294" price="" staff='[]'
                                                                            catId="77">Change Color Feet
                                                                        </option>
                                                                        <option value="295" price="" staff='[]'
                                                                            catId="77">Nail Design</option>
                                                                        <option value="296" price="" staff='[]'
                                                                            catId="77">Repair</option>
                                                                        <option value="297" price="" staff='[]'
                                                                            catId="77">Gel Color Change</option>
                                                                        <option value="298" price="" staff='[]'
                                                                            catId="77">Nail Take Off</option>
                                                                        <option value="299" price="" staff='[]'
                                                                            catId="77">Nail Take Off & Full Set
                                                                        </option>
                                                                    </optgroup>
                                                                    <optgroup label="Waxing">
                                                                        <!--List service-->
                                                                        <option value="300" price="" staff='[]'
                                                                            catId="78">Eye Brow</option>
                                                                        <option value="301" price="" staff='[]'
                                                                            catId="78">Lip</option>
                                                                        <option value="302" price="" staff='[]'
                                                                            catId="78">Full Face</option>
                                                                        <option value="303" price="" staff='[]'
                                                                            catId="78">Chin</option>
                                                                        <option value="304" price="" staff='[]'
                                                                            catId="78">Back & Chest</option>
                                                                        <option value="305" price="" staff='[]'
                                                                            catId="78">Under Arm</option>
                                                                        <option value="306" price="" staff='[]'
                                                                            catId="78">Full Arm</option>
                                                                        <option value="307" price="" staff='[]'
                                                                            catId="78">Half Arm</option>
                                                                        <option value="308" price="" staff='[]'
                                                                            catId="78">Half Leg</option>
                                                                        <option value="309" price="" staff='[]'
                                                                            catId="78">Bikini</option>
                                                                        <option value="310" price="" staff='[]'
                                                                            catId="78">Brazilian</option>
                                                                    </optgroup>
                                                                </select>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6 group-select">
                                                            <label>Technician (optional)</label>
                                                            <select name="staff_id[]" class="list_staff form-control">
                                                                <option value="">Select technician</option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-6 group-select box-date">
                                                        <label>Date</label>
                                                        <input type="text" name="booking_date" id="datetimepicker_v1"
                                                            class="form-control choose_date booking_date"
                                                            placeholder="Choice a date" value="05/24/2021"
                                                            typehtml="html">
                                                        <span class="fa fa-calendar"></span>
                                                    </div>
                                                    <div class="col-md-6" id="btn_search_booking">
                                                        <label>&nbsp;</label>
                                                        <button class='btn btn-search btn_action title booking_search'
                                                            type='button'>Search</button>
                                                    </div>
                                                </div>
                                                <script>
                                                    var btn_search_booking = document.getElementById(
                                                        "btn_search_booking").getElementsByTagName(
                                                        "button")[0];
                                                    btn_search_booking.className +=
                                                        " btn-search btn-primary btn-book-search";

                                                </script>
                                                <input type="hidden" name="booking_hours" value="" />
                                                <input type="hidden" name="booking_area_code" value="" />
                                                <input type="hidden" name="booking_phone" value="" />
                                                <input type="hidden" name="booking_name" value="" />
                                                <input type="hidden" name="store_id" value="0" />
                                                <input type="hidden" name="nocaptcha" value="1" />
                                                <input type="hidden" name="g-recaptcha-response" value="" />
                                                <input type="hidden" name="notelist" value="" />
                                            </div>
                                            <div class="add-services addButton">
                                                <img src="/public/library/global/add-service-icon-new.png"> Add
                                                Another service
                                            </div>
                                        </form>
                                        <script type="text/javascript">
                                            $(document).ready(function() {
                                                var formbk =
                                                    '{"listperson":[],"notelist":[],"booking_date":"05\/24\/2021","booking_hours":""}';
                                                loadEvent();

                                                $('#btn_search_booking .btn-booking').addClass(
                                                    'btn-primary');
                                            });

                                        </script>
                                    </div>
                                </div>
                            </div>
                        </div>


                        <section class="box_staff_inner_v1 bg-gray box_detail_info" style="display: none;">
                            <div class="row">
                                <div class="col-md-12 col-xs-12">
                                    <div class="booking_staff_title" id="book-info">
                                        <h2 class="section_title" itemprop="name">Staff Us</h2>
                                        <p class="staff_us" itemprop="name">THE ASSETS OF OUR FAMILY</p>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12  col-xs-12">
                                    <div class="box_staff_bg">
                                        <div class="info_inner_booking">
                                            <div class="row" id="box_person"></div>
                                            <div class="row">
                                                <div class="time_work_staff_v1 col-md-12 databooktime">
                                                    <h3 class="time_show"></h3>
                                                    <div class="time_am_v1">
                                                        <label>Morning <span class="note_am_time"
                                                                style="color: red; font-style: italic;"></span></label>
                                                        <ul class="timemorning">
                                                            <!--List hours Morning-->
                                                            <li>
                                                                <a href="#open_booking" valhours="10:00"
                                                                    class="open_booking">10:00 am</a>
                                                            </li>
                                                            <li>
                                                                <a href="#open_booking" valhours="10:30"
                                                                    class="open_booking">10:30 am</a>
                                                            </li>
                                                            <li>
                                                                <a href="#open_booking" valhours="11:00"
                                                                    class="open_booking">11:00 am</a>
                                                            </li>
                                                            <li>
                                                                <a href="#open_booking" valhours="11:30"
                                                                    class="open_booking">11:30 am</a>
                                                            </li>
                                                            <!--End list hours Morning-->
                                                        </ul>
                                                    </div>
                                                    <div class="time_pm_v1">
                                                        <label>Afternoon <span class="note_pm_time"
                                                                style="color: red; font-style: italic;"></span></label>
                                                        <ul class="timeafternoon">
                                                            <!--List hours Afternoon-->
                                                            <li>
                                                                <a href="#open_booking" valhours="12:00"
                                                                    class="open_booking">12:00 pm</a>
                                                            </li>
                                                            <li>
                                                                <a href="#open_booking" valhours="12:30"
                                                                    class="open_booking">12:30 pm</a>
                                                            </li>
                                                            <li>
                                                                <a href="#open_booking" valhours="13:00"
                                                                    class="open_booking">13:00 pm</a>
                                                            </li>
                                                            <li>
                                                                <a href="#open_booking" valhours="13:30"
                                                                    class="open_booking">13:30 pm</a>
                                                            </li>
                                                            <li>
                                                                <a href="#open_booking" valhours="14:00"
                                                                    class="open_booking">14:00 pm</a>
                                                            </li>
                                                            <li>
                                                                <a href="#open_booking" valhours="14:30"
                                                                    class="open_booking">14:30 pm</a>
                                                            </li>
                                                            <li>
                                                                <a href="#open_booking" valhours="15:00"
                                                                    class="open_booking">15:00 pm</a>
                                                            </li>
                                                            <li>
                                                                <a href="#open_booking" valhours="15:30"
                                                                    class="open_booking">15:30 pm</a>
                                                            </li>
                                                            <li>
                                                                <a href="#open_booking" valhours="16:00"
                                                                    class="open_booking">16:00 pm</a>
                                                            </li>
                                                            <li>
                                                                <a href="#open_booking" valhours="16:30"
                                                                    class="open_booking">16:30 pm</a>
                                                            </li>
                                                            <li>
                                                                <a href="#open_booking" valhours="17:00"
                                                                    class="open_booking">17:00 pm</a>
                                                            </li>
                                                            <li>
                                                                <a href="#open_booking" valhours="17:30"
                                                                    class="open_booking">17:30 pm</a>
                                                            </li>
                                                            <li>
                                                                <a href="#open_booking" valhours="18:00"
                                                                    class="open_booking">18:00 pm</a>
                                                            </li>
                                                            <li>
                                                                <a href="#open_booking" valhours="18:30"
                                                                    class="open_booking">18:30 pm</a>
                                                            </li>
                                                            <li>
                                                                <a href="#open_booking" valhours="19:00"
                                                                    class="open_booking">19:00 pm</a>
                                                            </li>
                                                            <!--End list hours Afternoon-->
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>
                                            <!--POPUP LOGIN-->
                                            <div id="popup_login" class="white-popup mfp-hide">
                                                <div class="box_account_v1">
                                                    <div class="modal_form_header">
                                                        <h4>Login</h4>
                                                    </div>
                                                    <div class="popup_main_area">
                                                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 border_right">
                                                            <form enctype="multipart/form-data" id="form-login"
                                                                method="POST" action="/login/login_do">
                                                                <fieldset class="form-group">
                                                                    <div class="box_login">
                                                                        <h2> Been here before?</h2>
                                                                        <div class="form_input_1">
                                                                            <div class="form-control-wrapper">
                                                                                <input type="email" name="cus_email"
                                                                                    placeholder="Enter your E-mail"
                                                                                    data-validation="[EMAIL]"
                                                                                    data-validation-message="Email is not valid!">
                                                                            </div>
                                                                        </div>
                                                                        <div class="form_input_2">
                                                                            <div class="form-control-wrapper">
                                                                                <input type="password"
                                                                                    placeholder="Password"
                                                                                    name="cus_password"
                                                                                    data-validation="[NOTEMPTY]"
                                                                                    data-validation-message="Password must not be empty!">
                                                                            </div>
                                                                        </div>
                                                                        <div class="col_psw_v1"
                                                                            style="padding-bottom: 3px; ">

                                                                            <div class="form-control-wrapper"
                                                                                style="margin-top: -10px;padding-bottom: 3px;float:right">
                                                                                <a href="/login/forgot-password/">Forgot
                                                                                    password</a>

                                                                            </div>
                                                                        </div>
                                                                        <div class="btn_submit_login">
                                                                            <button class="submit"
                                                                                type="submit">Login</button>
                                                                        </div>
                                                                    </div>
                                                                </fieldset>
                                                            </form>
                                                        </div>
                                                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                                            <div class="box_register">
                                                                <h2>New Account?</h2>
                                                                <div class="btn_submit_login">
                                                                    <button class="submit" type="submit"
                                                                        onclick="window.location.href='/register/'">Create
                                                                        a Username
                                                                    </button>
                                                                </div>
                                                                <div class="btn_login_social">

                                                                    <a href="" class="btn btn_facebook_v1" href="#"
                                                                        title="facebook login">
                                                                        <i class="fa fa-facebook"></i>
                                                                        <span>Facebook</span>
                                                                    </a>

                                                                    <a class="btn btn_gplus_v1" href=""
                                                                        title="gplus login">
                                                                        <i class="fa fa-google-plus"></i>
                                                                        <span>Google</span>
                                                                    </a>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                            <!--POPUP BOOKING-->
                                            <div id="open_booking" class="white-popup mfp-hide">
                                                <div class="box_account_v1">
                                                    <div class="modal_form_header">
                                                        <h4>Message</h4>
                                                    </div>
                                                    <div class="popup_main_area">
                                                        <form enctype="multipart/form-data" id="booking_check"
                                                            name="booking_check">
                                                            <p style="font-weight: bold; font-size: 18px;">
                                                                Confirm booking information ?</p>
                                                            <span
                                                                style="color: red; font-style: italic; margin: 0 0 10px; display: block;">We
                                                                will send a text message to you via the number
                                                                below after we confirm the calendar for your
                                                                booking!</span>
                                                            <div class="row">
                                                                <div class="col-md-6">
                                                                    <div class="form-group">
                                                                        <label class="form-control-label">Your
                                                                            name</label>
                                                                        <div class="form-control-wrapper">
                                                                            <input name="input_name" type="text"
                                                                                class="form-control" type="text"
                                                                                data-validation="[NOTEMPTY]"
                                                                                data-validation-message="Please enter your name!" />
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="col-md-2" style="display: none;">
                                                                    <div class="form-group">
                                                                        <label class="form-control-label">Area
                                                                            code</label>
                                                                        <select name="area_code" class="form-control"
                                                                            defaultvalue="1">
                                                                            <option value="1">US (+1)</option>
                                                                            <option value="84">VN (+84)</option>
                                                                        </select>
                                                                    </div>
                                                                </div>
                                                                <div class="col-md-6">
                                                                    <div class="form-group">
                                                                        <label class="form-control-label">Phone
                                                                            number</label>
                                                                        <div class="form-control-wrapper">
                                                                            <input name="phone_number" type="text"
                                                                                class="form-control inputPhone"
                                                                                placeholder="Ex: (123) 123-1234"
                                                                                data-validation="[NOTEMPTY]"
                                                                                data-validation-message="Phone number invalid" />
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="col-md-12">
                                                                    <div class="form-group">
                                                                        <label class="form-control-label">Note
                                                                            (Optional)</label>
                                                                        <div class="form-control-wrapper">
                                                                            <textarea name="notelist"
                                                                                placeholder="Max length 200 character"
                                                                                rows="5" class="form-control"
                                                                                maxlength="200"></textarea>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <!-- Store options -->
                                                                <input type="hidden" name="choose_store" value="">
                                                                <!-- End - Store options -->
                                                            </div>
                                                            <button class="btn btn-success btn_confirmed "
                                                                type="button">Confirm
                                                            </button>
                                                            <button class="btn btn-danger btn-inline btn_cancel"
                                                                type="button">Cancel
                                                            </button>
                                                        </form>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </section>

                        <script>
                            $(document).ready(function() {
                                $("#form-login").validate({
                                    submit: {
                                        settings: {
                                            button: "[type='submit']",
                                            inputContainer: '.form-group',
                                            errorListClass: 'form-tooltip-error',
                                        }
                                    }
                                });

                                // check Time
                                $('form#booking_check').validate({
                                    submit: {
                                        settings: {
                                            clear: 'keypress',
                                            display: "inline",
                                            button: ".btn_confirmed",
                                            inputContainer: 'form-group',
                                            errorListClass: 'form-tooltip-error',
                                        },
                                        callback: {
                                            onSubmit: function(node, formdata) {
                                                var areacode =
                                                    1; //$("select[name='area_code']").val();
                                                var phone_number = $(
                                                    "input[name='phone_number']").val();
                                                var cus_name = $("input[name='input_name']")
                                                    .val();
                                                var store_id = $("[name='choose_store']")
                                                    .length > 0 ? $("[name='choose_store']")
                                                    .val() : 0;
                                                $("[name='store_id']").val(store_id);

                                                var notelist = $(
                                                    "textarea[name='notelist']").val();
                                                $("input[name='notelist']").val(notelist);

                                                if (phone_number != "" && phone_number
                                                    .length > 6) {
                                                    $("input[name='booking_phone']").val(
                                                        phone_number);
                                                    $("input[name='booking_area_code']")
                                                        .val(areacode);
                                                }

                                                if (cus_name) {
                                                    $("input[name='booking_name']").val(
                                                        cus_name);
                                                }

                                                if (enableRecaptcha) {
                                                    var check_google = $(
                                                        "#g-recaptcha-response").val();
                                                    if (typeof(check_google) !=
                                                        "undefined" && check_google != "") {
                                                        $("input[name='g-recaptcha-response']")
                                                            .val(check_google);
                                                        $("#surveyForm").submit();
                                                    }
                                                } else {
                                                    $("#surveyForm").submit();
                                                }
                                                return false;
                                            }
                                        }
                                    }
                                });
                            });

                        </script>
                    </div>
                </div>
            </div>
        </section>
    </div>
</main>
