<main>
    <div class="pmain">

        <div class="in-head">
            <div class="container">
                <div class="in-headbg">
                    <h2 class="mTitle" itemprop="name">Gallery</h2>
                </div>
            </div>
        </div>
        <div class="container">
            <div class="in-container">
                <div class="in-content">
                    <div class="main-content">
                        <div class="gallery-style-1 bg-fa gallery-inpage clearfix ">
                            <div class="clearfix">
                                <div class="cate-gallery hidden-xs">
                                    <ul id="filter">
                                        @foreach ($albums as $index => $album)
                                            <li itemprop="name">
                                                <a itemprop="{{ $album['id'] }}">{{ $album['name'] }}</a>
                                            </li>
                                        @endforeach
                                    </ul>
                                </div>
                                <div class="hidden-sm hidden-md hidden-lg hidden-xl">
                                    <div class="please-choose">Please choose categories</div>
                                    <div class="form-group col-md-12 col-xs-12">
                                        <select class="form-control select_tab" autocomplete="off" name="filter_select" id="filter_select">
                                            @foreach ($albums as $index => $album)
                                                <option value="{{ $album['id'] }}">{{ $album['name'] }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="clearfix relative" id="gallery_content">
                                <div class="list-gallery">
                                    <div class="box_list_gallery listing"></div>
                                </div>
                                <div class="clearfix">
                                    <nav class="text-center box_paging paging text-center"></nav>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</main>
