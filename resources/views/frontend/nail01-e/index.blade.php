@php
$features = json_decode($header['features']);
$extras = json_decode($header['extras']);
@endphp
<main>
    <div class="pmain">
        {{-- CAROUSEL --}}
        <div class="section-slider-wrap">
            <div class="container">
                <section class="section-slider">
                    <div class="slider-pro" id="my-slider" style="display: none;">
                        <div class="sp-slides">
                            @foreach ($imgs_carousel as $img)
                                <div class="sp-slide">
                                    <div class="sp-layer sp-static">
                                        <a href="/" target="_self" title="{{ $img['title'] }}">
                                            <img class="sp-image"
                                                src="{{ asset('storage') }}/photos/{{ $img['url'] }}"
                                                alt="{{ $img['title'] }}">
                                        </a>
                                    </div>
                                </div>
                            @endforeach
                        </div>
                    </div>
                </section>
            </div>
        </div>
        <div class="container">
            {{-- ABOUT ARTICLE --}}
            <div class="section-about section">
                <div class="row">
                    <div class="col-md-7">
                        <div class="about-content">
                            <h2>Welcome to</h2>
                            <h3>{{ $home_article['title'] }}</h3>
                            {!! $home_article['content'] !!}
                        </div>
                    </div>
                    <div class="col-md-5 about-img">
                        @if (count(json_decode($home_article['image'])) > 0)
                            <img class="imgrps"
                                src="{{ asset('storage') }}/photos/{{ json_decode($home_article['image'])[0] }}"
                                itemprop="image" caption="false" />
                        @endif
                    </div>
                </div>
            </div>
            {{-- SERVICES --}}
            <div class="section-service section">
                <div class="row">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <div class="owl-carousel owl-theme owl_service_board">
                            @foreach ($services as $service)
                                <div class="item">
                                    <h3>
                                        <a itemprop="url"
                                            href="{{ route('services-detail', ['slug' => strtolower(trim(preg_replace('/[^A-Za-z0-9-]+/', '-', $service['name']))), 'service_id' => $service['id']]) }}">{{ $service['name'] }}</a>
                                    </h3>
                                    <a href="{{ route('services-detail', ['slug' => strtolower(trim(preg_replace('/[^A-Za-z0-9-]+/', '-', $service['name']))), 'service_id' => $service['id']]) }}"
                                        class="cover">
                                        @if (count(json_decode($service['images'])) > 0)
                                            <img class="imgrps"
                                                src="{{ asset('storage') }}/photos/{{ json_decode($service['images'])[0] }}"
                                                alt="{{ $service['name'] }}" itemprop="image">
                                        @endif
                                    </a>
                                </div>
                            @endforeach
                        </div>
                    </div>
                </div>
            </div>
            {{-- GALLERY --}}
            <section class="section-gallery gallery-section">
                <div class="container">
                    <div class="mixitup-gallery">
                        <div class="inner-container clearfix">
                            <div class="sec-title text-center">
                                <h2 itemprop="name">Our Photos</h2>
                                <div class="separator"><span class="flaticon-flower"></span></div>
                                <p itemprop="description">Lorem ipsum dolor sit amet, consectetur adipisicing
                                    elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut
                                    enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut
                                    aliquip</p>
                            </div>
                            <div>
                                <div class="m-gallery-box-wrap">
                                    <div class="row">
                                        @foreach ($gallery_list as $item)
                                            <div class="col-xs-6 col-sm-4 col-md-3">
                                                <a class="gallery-item" data-group="gallery-1"
                                                    title="{{ $item['name'] }}"
                                                    href="{{ asset('storage') }}/photos/{{ $item['url'] }}"
                                                    itemprop="image">
                                                    <span
                                                        style="background-image:url('{{ asset('storage') }}/photos/{{ $item['url'] }}')"></span>
                                                    <div class="gallery-info">
                                                        <h3 class="project-title">{{ $item['name'] }}</h3>
                                                        <div class="icon"><i class="fa fa-search-plus"></i></div>
                                                    </div>
                                                </a>
                                            </div>
                                        @endforeach
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            {{-- VIDEO --}}
            <div class="section-vt section">
                <div class="row">
                    <div class="col-md-6">
                        <h2>Video</h2>
                        <div class="embed-responsive embed-responsive-16by9 mar_15"><iframe width="300" height="150"
                                class="embed-responsive-item" src="{{ $home_article['description'] }}"
                                frameborder="0" allowfullscreen="allowfullscreen"></iframe></div>
                    </div>
                    <div class="col-md-6">
                        <h2>Business hours</h2>
                        <div class="vt-time">
                            @foreach ($extras as $item)
                                <div class="col-2" itemprop="openingHours"
                                    content="{{ $item->name }}:{{ $item->desc }}">
                                    <div class="col-lef">{{ $item->name }}</div>
                                    <div class="col-right">{{ $item->desc }}</div>
                                </div>
                            @endforeach
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</main>
