@php
$features = json_decode($header['features']);
$extras = json_decode($header['extras']);
@endphp
<main>
    <div class="pmain">
        <section class="p-service">
            <div class="in-head">
                <div class="container">
                    <div class="in-headbg">
                        <h2 class="mTitle" itemprop="name">Services</h2>
                    </div>
                </div>
            </div>
            <div class="container">
                <div class="in-container">
                    <div class="service-container">
                        <div class="row">
                            <div class="col-sm-6"></div>
                            <div class="col-sm-6 top-right btn_service_defale" style="padding-bottom: 20px;">
                                <div class="btn_service_book">
                                    <a href="/book" class="btn btn-primary btn_make_appointment"> Make an
                                        appointment</a>&nbsp <a href="tel:{{ $features[1]->desc }}"
                                        class="btn btn-primary"><i class="fa fa-phone"></i>Call now</a>
                                </div>
                            </div>
                        </div>
                        <div class="animation_sroll_jumpto">
                            <div class="sroll_jumpto">
                                <input type="hidden" name="group_id" value="{{ $service_id }}" />
                                <!-- Service -->
                                @foreach ($services as $index => $service)
                                    @if ($index % 2 === 0)
                                        <div class="row service-row" id="sci_{{ $service['id'] }}">
                                            <div class="col-sm-4 col-md-4 text-center">
                                                @if (count(json_decode($service['images'])) > 0)
                                                    <div class="circle-service-image"
                                                        style="background-image: url('{{ asset('storage') }}/photos/{{ json_decode($service['images'])[0] }}')">
                                                    </div>
                                                @endif
                                            </div>
                                            <div class="col-sm-8 col-md-8">
                                                <h2 class="service-name" id="manicure_pedicure">{{ $service['name'] }}
                                                </h2>
                                                <div class="service-desc">{{ $service['description'] }}</div>
                                                @foreach (json_decode($service['features']) as $item)
                                                    @switch($item->name)
                                                        @case('desc')
                                                            <p>{{ $item->desc }}</p>
                                                        @break
                                                        @case('center')
                                                        @break
                                                        @default
                                                            <div class="detail-price-item">
                                                                <span class="detail-price-name">{{ $item->name }}</span>
                                                                <span class="detail-price-dots"></span>
                                                                <span class="detail-price-number">{{ $item->desc }}</span>
                                                            </div>
                                                    @endswitch
                                                @endforeach
                                            </div>
                                        </div>
                                    @else
                                        <div class="row service-row" id="sci_{{ $service['id'] }}">
                                            <div class="col-md-1 hidden-sm hidden-xs"></div>
                                            <div class="col-md-3 col-xs-12 text-center hidden-lg hidden-md hidden-sm">
                                                @if (count(json_decode($service['images'])) > 0)
                                                    <div class="circle-service-image"
                                                        style="background-image: url('{{ asset('storage') }}/photos/{{ json_decode($service['images'])[0] }}')">
                                                    </div>
                                                @endif
                                            </div>
                                            <div class="col-sm-8 col-md-7 ">
                                                <h2 class="service-name" id="facial">{{ $service['name'] }}</h2>
                                                <div class="service-desc">{{ $service['description'] }}</div>
                                                @foreach (json_decode($service['features']) as $item)
                                                    @switch($item->name)
                                                        @case('desc')
                                                            <p>{{ $item['desc'] }}</p>
                                                        @break
                                                        @case('center')
                                                        @break
                                                        @default
                                                            <div class="detail-price-item">
                                                                <span class="detail-price-name">{{ $item->name }}</span>
                                                                <span class="detail-price-dots"></span>
                                                                <span class="detail-price-number">
                                                                    <span class="current">{{ $item->desc }}
                                                                    </span>
                                                                </span>
                                                            </div>
                                                    @endswitch
                                                @endforeach
                                            </div>
                                            <div class="col-sm-4 col-md-3 text-center hidden-xs">
                                                @if (count(json_decode($service['images'])) > 0)
                                                    <div class="circle-service-image"
                                                        style="background-image: url('{{ asset('storage') }}/photos/{{ json_decode($service['images'])[0] }}')">
                                                    </div>
                                                @endif
                                            </div>
                                            <div class="col-md-1 hidden-sm hidden-xs"></div>
                                        </div>
                                    @endif
                                @endforeach
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
</main>
