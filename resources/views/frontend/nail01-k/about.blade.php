<main>
    <div class="pmain">
        <div class="container">
            <section class="in-container">
                <div class="title-page">
                    <h2><span>About us</span></h2>
                </div>
                <div class="section-about about-page">
                    @foreach ($articles as $item)
                        <div class="row">
                            <div class="col-md-6 col-xs-12">
                                <div class="about-content">
                                    <h2>{{ $item['title'] }}</h2>
                                    {!! $item['content'] !!}
                                </div>
                            </div>
                            <div class="col-md-6 col-xs-12">
                                <div class="about-video" style="margin-top: 20px;">
                                    @if (count(json_decode($item['image'])) > 0)
                                        <img src="{{ asset('storage') }}/photos/{{ json_decode($item['image'])[0] }}"
                                            caption="false" />
                                    @endif
                                </div>
                            </div>
                        </div>
                    @endforeach
                </div>
            </section>
        </div>
    </div>
</main>
