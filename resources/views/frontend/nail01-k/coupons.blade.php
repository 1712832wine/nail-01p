<main>
    <div class="pmain">

        <section class="p-coupons">
            <div class="container">
                <div class="in-container">
                    <div class="title-page">
                        <h2><span>Coupons</span></h2>
                    </div>
                    <div class="row">
                        @foreach ($list as $item)
                            <div class="col-sm-6 col-md-6 cards-item ">
                                <a class="coupon_img_v1 pointer  image-magnific-popup" data-group=""
                                    href="{{ asset('storage') }}/photos/{{ $item['url'] }}" title="Coupon">
                                    <img class="cards-item-image img-responsive"
                                        src="{{ asset('storage') }}/photos/{{ $item['url'] }}" alt="Coupon">
                                </a>
                            </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </section>
    </div>
</main>
