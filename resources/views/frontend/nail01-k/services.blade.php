<main>
    <div class="pmain">
        <div class="container">
            <section class="in-container">
                <!-- Use for get current id -->
                <input type="hidden" name="group_id" value="{{ $service_id }}" />
                <input type="hidden" name="lid" value="">
                <div class="title-page">
                    <h2><span>Service</span></h2>
                </div>
                <div class="service-container">
                    <div class="row">
                        <div class="col-sm-6"></div>
                        <div class="col-sm-6 top-right btn_service_defale" style="padding-bottom: 20px;"></div>
                    </div>
                    <div class="animation_sroll_jumpto">
                        <div class="sroll_jumpto">
                            <!-- Service -->
                            @foreach ($services as $index => $service)
                                @if ($index % 2 === 0)
                                    <div class="row service-row" id="sci_{{ $service['id'] }}">
                                        <div class="col-sm-4 col-md-4 col-xs-12 text-center ">
                                            @if (count(json_decode($service['images'])) > 0)
                                                <div class="circle-service-image"
                                                    style="background-image: url('{{ asset('storage') }}/photos/{{ json_decode($service['images'])[0] }}')">
                                                </div>
                                            @endif
                                        </div>
                                        <div class="col-sm-8 col-md-8 col-xs-12">
                                            <h2 class="service-name" id="manicure_pedicure">{{ $service['name'] }}
                                            </h2>
                                            <div class="c_product_description">{{ $service['description'] }}</div>
                                            @foreach (json_decode($service['features']) as $item)
                                                @switch($item->name)
                                                    @case('desc')
                                                        <p>{{ $item->desc }}</p>
                                                    @break
                                                    @case('center')
                                                    @break
                                                    @default
                                                        <div class="detail-price-item">
                                                            <span class="detail-price-name">{{ $item->name }}</span>
                                                            <span class="detail-price-dots"></span>
                                                            <span class="detail-price-number">{{ $item->desc }}</span>
                                                        </div>
                                                @endswitch
                                            @endforeach
                                        </div>
                                    </div>
                                @else
                                    <div class="row service-row" id="sci_{{ $service['id'] }}">
                                        <div class="col-xs-12 text-center hidden-md hidden-lg hidden-sm">
                                            <div class="circle-service-image"
                                                style="background-image: url('{{ asset('storage') }}/photos/{{ json_decode($service['images'])[0] }}')">
                                            </div>
                                        </div>
                                        <div class="col-sm-8 col-md-8 col-xs-12">
                                            <h2 class="service-name" id="facial">{{ $service['name'] }}</h2>
                                            <div class="c_product_description">{{ $service['description'] }}</div>
                                            @foreach (json_decode($service['features']) as $item)
                                                @switch($item->name)
                                                    @case('desc')
                                                        <p>{{ $item['desc'] }}</p>
                                                    @break
                                                    @case('center')
                                                    @break
                                                    @default
                                                        <div class="detail-price-item">
                                                            <span class="detail-price-name">{{ $item->name }}</span>
                                                            <span class="detail-price-dots"></span>
                                                            <span class="detail-price-number">
                                                                <span class="current">{{ $item->desc }}
                                                                </span>
                                                            </span>
                                                        </div>
                                                @endswitch
                                            @endforeach
                                        </div>
                                        <div class="col-sm-4 col-md-4 col-xs-12 hidden-xs text-center ">
                                            @if (count(json_decode($service['images'])) > 0)
                                                <div class="circle-service-image"
                                                    style="background-image: url('{{ asset('storage') }}/photos/{{ json_decode($service['images'])[0] }}')">
                                                </div>
                                            @endif
                                        </div>
                                    </div>
                                @endif
                            @endforeach
                        </div>
                    </div>
                </div>
            </section>
        </div>
    </div>
</main>
