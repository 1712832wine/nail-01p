@php
$features = json_decode($header['features']);
$extras = json_decode($header['extras']);
@endphp
<footer class="footer">
    <div class="container">
        <div class="footer-main">
            <div class="row">
                <div class="col-md-4"></div>
                <div class="col-md-4">
                    <div class="footer-logo">
                        @if (count(json_decode($header['images'])) > 0)
                            <a itemprop="url" href="/">
                                <img class="imgrps"
                                    src="{{ asset('storage') }}/photos/{{ json_decode($header['images'])[0] }}"
                                    alt="{{ $header['title'] }}" itemprop="logo image">
                            </a>
                        @endif
                    </div>
                    <div class="footer-social">
                        @foreach ($social as $item)
                            @if ($item['url'])
                                <a itemprop="url" class="social-img-icons" rel="nofollow" target="_blank"
                                    title="{{ $item['title'] }}" href="{{ $item['content'] }}"><img
                                        src="{{ asset('storage') }}/photos/{{ $item['url'] }}"
                                        alt="{{ $item['title'] }}"></a>
                            @endif
                        @endforeach
                    </div>
                </div>
                <div class="col-md-4"></div>
            </div>
        </div>
    </div>
    <div class="footer-copyright">
        <div class="container"><span>© Copyright by Fnail01k - Nails Spa. All Rights Reserved.</span></div>
    </div>
</footer>
<div class="freeze-footer">
    <ul>
        <li><a href="tel:{{ $features[1]->desc }}"
                class="btn btn-default btn_call_now btn-call">{{ $features[1]->desc }}</a></li>
        <li><a href="/book" class="btn btn-default btn_make_appointment">Booking</a></li>
    </ul>
</div>
