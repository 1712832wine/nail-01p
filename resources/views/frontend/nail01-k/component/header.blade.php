<header class="header">
    <div class="wrap-freeze-header-mobile clearfix">
        <div class="flag-freeze-header-mobile hidden-md hidden-lg">
            <div class="menu_mobile_v1 menu-1024-hidden">
                <div class="mobile_logo">
                    @if (count(json_decode($header['images'])) > 0)
                        <a itemprop="url" href="/">
                            <img class="imgrps"
                                src="{{ asset('storage') }}/photos/{{ json_decode($header['images'])[0] }}"
                                alt="{{ $header['title'] }}" itemprop="logo image">
                        </a>
                    @endif
                </div>
                <div class="mobile_menu_container_v1">
                    <div class="mobile-menu clearfix">
                        <nav id="mobile_dropdown">
                            <ul>
                                <li itemprop="name"><a href="/" itemprop="url">Home</a></li>
                                <li itemprop="name"><a href="/about" itemprop="url">About Us</a></li>
                                <li itemprop="name"><a href="/services" itemprop="url">Services</a></li>
                                <li itemprop="name"><a href="/coupons" itemprop="url">Coupon</a></li>
                                <li itemprop="name"><a itemprop="url" href="/giftcards">Giftcards</a></li>
                                <li itemprop="name"><a itemprop="url" href="/book">Booking</a></li>
                                <li itemprop="name"><a href="/gallery" itemprop="url">Gallery</a></li>
                                <li itemprop="name"><a href="/contact" itemprop="url">Contact Us</a></li>
                            </ul>
                        </nav>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="wrap-freeze-header clearfix">
        <div class="flag-freeze-header hidden-sm hidden-xs">
            <div class="container">
                <div class="header-main">
                    <div class="header-left-block">
                        <nav class="navbar main-nav">
                            <div id="desktop-nav" class="navbar-collapse collapse">
                                <ul class="nav navbar-nav nav-main">
                                    <li itemprop="name"><a href="/" itemprop="url">Home</a></li>
                                    <li itemprop="name"><a href="/about" itemprop="url">About Us</a></li>
                                    <li itemprop="name"><a href="/services" itemprop="url">Services</a></li>
                                    <li itemprop="name"><a href="/coupons" itemprop="url">Coupon</a></li>
                                </ul>
                            </div>
                            <!--/.nav-collapse -->
                        </nav>
                    </div>

                    <div class="header-center-block">
                        <div class="site-logo">

                            @if (count(json_decode($header['images'])) > 0)
                                <a itemprop="url" href="/">
                                    <img class="imgrps"
                                        src="{{ asset('storage') }}/photos/{{ json_decode($header['images'])[0] }}"
                                        alt="{{ $header['title'] }}" itemprop="logo image">
                                </a>
                            @endif
                        </div>
                    </div>
                    <div class="header-right-block">
                        <nav class="navbar main-nav hidden-sm hidden-xs">
                            <div id="desktop-nav" class="navbar-collapse collapse">
                                <ul class="nav navbar-nav nav-main">
                                    <li itemprop="name"><a itemprop="url" href="/giftcards">Giftcards</a>
                                    </li>
                                    <li itemprop="name"><a itemprop="url" href="/book">Booking</a></li>
                                    <li itemprop="name"><a href="/gallery" itemprop="url">Gallery</a></li>
                                    <li itemprop="name"><a href="/contact" itemprop="url">Contact Us</a>
                                    </li>
                                </ul>
                            </div>
                        </nav>
                    </div>
                </div>
            </div>
        </div>
    </div>
</header>
<!--
# meanmenu oprions
# meanRevealPosition: left, right (default), center
# meanMenuOpen: <span></span> (default)
# meanScreenWidth: 991 (default)
-->
<div id="menuMobileOption" data-meanRevealPosition="right" data-meanMenuOpen="<span></span>" data-meanScreenWidth="991">
</div>
