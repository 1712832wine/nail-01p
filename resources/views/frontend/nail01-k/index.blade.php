@php
$features = json_decode($header['features']);
$extras = json_decode($header['extras']);
@endphp
<main>
    <div class="pmain">
        {{-- CAROUSEL --}}
        <div class="container">
            <div class="section section-slider">
                <div class="slider-pro" id="my-slider">
                    <div class="sp-slides">
                        @foreach ($imgs_carousel as $img)
                            <div class="sp-slide">
                                <img class="sp-image" src="{{ asset('storage') }}/photos/{{ $img['url'] }}"
                                    itemprop="image">
                            </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
        {{-- SERVICES --}}
        <section class="section section-product products-list">
            <div class="container">
                <div class="title-center">
                    <div class="icon"><i class="fa fa-pagelines"></i></div>
                    <h2><span>Welcome to Fnail01k - Nails Spa</span></h2>
                    <h4 class="sub-title">Come to our salon, you can enjoy the ambiance of our comforting decor. Our
                        warm atmosphere is designed to make you feel relaxed and refreshed.</h4>
                </div>
                <div class="section-service section sale-box">
                    <div class="row">
                        @foreach ($services as $service)
                            <div class="break-row break-row-1"></div>
                            <div class="col-md-4 col-sm-6">
                                <div class="item">
                                    <a href="{{ route('services-detail', ['slug' => strtolower(trim(preg_replace('/[^A-Za-z0-9-]+/', '-', $service['name']))), 'service_id' => $service['id']]) }}"
                                        class="cover">
                                        @if (count(json_decode($service['images'])) > 0)
                                            <img class="imgrps"
                                                src="{{ asset('storage') }}/photos/{{ json_decode($service['images'])[0] }}"
                                                alt="Manicure & Pedicure" itemprop="image">
                                        @endif
                                    </a>
                                    <h3><a itemprop="url"
                                            href="{{ route('services-detail', ['slug' => strtolower(trim(preg_replace('/[^A-Za-z0-9-]+/', '-', $service['name']))), 'service_id' => $service['id']]) }}">{{ $service['name'] }}</a>
                                    </h3>
                                    <div class="sumarry">{{ $service['description'] }}</div>
                                    <div><a class="btn btn-primary"
                                            href="{{ route('services-detail', ['slug' => strtolower(trim(preg_replace('/[^A-Za-z0-9-]+/', '-', $service['name']))), 'service_id' => $service['id']]) }}">View
                                            more</a></div>
                                </div>
                            </div>
                        @endforeach
                    </div>
                </div>



            </div>
        </section>
        {{-- GALLERY --}}
        <section class="section section-sale">
            <div class="container">
                <div class="title-left">
                    <h3><span>Our Gallery</span></h3>
                </div>
                <div class="row">
                    <div class="col-md-8">
                        <div class="row">
                            <div class="list-gallery">
                                <div class="tab-content">
                                    <!-- Custom height with class: .portfolio.gallery-bg-cover {padding-bottom: 75%;} -->
                                    <div class="tab-pane fade active in">
                                        <ul id="portfolio-grid" class="row four-column hover-four portfolio-grid">
                                            @foreach ($gallery_list as $item)
                                                <li
                                                    class="col-md-3 col-xs-6 portfolio-item gallery-bg-cover text-center">
                                                    <div class="image-magnific-popup pointer" data-group="gallery-home"
                                                        href="{{ asset('storage') }}/photos/{{ $item['url'] }}"
                                                        title="Nails Design">
                                                        <div class="portfolio gallery-bg-cover">
                                                            <div class="tt-overlay"></div>
                                                            <meta itemprop="image"
                                                                content="{{ asset('storage') }}/photos/{{ $item['url'] }}">
                                                            </meta>
                                                            <div class="portfolio-image"
                                                                style="background: url('{{ asset('storage') }}/photos/{{ $item['url'] }}') center center no-repeat; background-size: cover;">
                                                            </div>
                                                            <div class="portfolio-info">
                                                                <h3 itemprop="name" class="project-title">
                                                                    Nails Design
                                                                </h3>
                                                                <span class="links" itemprop="description">
                                                                    Nails Design</span>
                                                            </div>
                                                            <ul class="portfolio-details">
                                                                <li><span><i class="fa fa-search-plus"></i></span></li>
                                                            </ul>
                                                        </div>
                                                    </div>
                                                </li>
                                            @endforeach
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4 sale-col">
                        <div class="sale-box">
                            <h3>Fnail01k - Nails Spa</h3>
                            <div class="box-sub-text">
                                <ul class="custom_address">
                                    <li><i>Address:</i>
                                        <spam>{{ $features[0]->desc }}</spam>
                                    </li>
                                    <li><i>Phone:</i>
                                        <spam>{{ $features[1]->desc }}</spam>
                                    </li>
                                    <li><i>Email:</i>
                                        <spam>{{ $features[2]->desc }}</spam>
                                    </li>
                                </ul>
                            </div>
                            <h3>BUSINESS HOURS</h3>
                            <div class="foh-wrap">
                                @foreach ($extras as $item)
                                    <div class="foh-row" itemprop="openingHours" content="Mon 10:00 am - 7:00 pm">
                                        <span class="foh-date">{{ $item->name }}</span>
                                        <span class="foh-time">{{ $item->desc }}</span>
                                    </div>
                                @endforeach
                            </div>
                            <a class="sale-link" href="/book">Booking</a>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
</main>
