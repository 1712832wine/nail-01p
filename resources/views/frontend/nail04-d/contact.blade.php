@php
$features = json_decode($header['features']);
$extras = json_decode($header['extras']);
$name = explode(',', $header->name);
$desc = explode(',', $header->desc);
@endphp
<main>
    <div class="pmain">
        <!-- tpl main contact -->
        <div class="page-heading">
            <div class="container">
                <h2>Contact Us</h2>
            </div>
        </div>
        <div class="in-container">
            <div class="container">
                <div class="contact-form">
                    <div class="row">
                        <div class="col-md-8">
                            <div class="contact-heading">
                                <h4>Contact Form</h4>
                            </div>
                            <div class="footer-3-box">
                                <div class="custom-form-2">
                                    <div class="form-contact-us-index">
                                        <div class="form-input-infor">
                                            <form enctype="multipart/form-data" method="post" name="send_contact"
                                                id="send_contact" action="/contact/send">
                                                <div class="row">


                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <input title="your name" type="text"
                                                                data-validation="[NOTEMPTY]"
                                                                data-validation-message="Please enter your name!"
                                                                autocomplete="off" class="form-control style-input"
                                                                placeholder="Your Name" name="contactname">
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <input title="Your Email" type="text"
                                                                data-validation="[EMAIL]"
                                                                data-validation-message="Invalid email"
                                                                autocomplete="off" class="form-control style-input"
                                                                placeholder="Your Email" name="contactemail">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <div class="form-group">
                                                            <input title="Your Subject" type="text"
                                                                data-validation="[NOTEMPTY]"
                                                                data-validation-message="Please enter a subject!"
                                                                autocomplete="off" class="form-control style-input"
                                                                placeholder="Your Subject" name="contactsubject">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-xs-12">
                                                        <textarea title="Your Message" rows="10"
                                                            data-validation="[NOTEMPTY]"
                                                            data-validation-message="Please enter a content!"
                                                            autocomplete="off"
                                                            class="form-control style-input style-textarea"
                                                            placeholder="Your Message" name="contactcontent"></textarea>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-xs-12">
                                                        <div class="btn-sauna form-group">
                                                            <button type="submit"
                                                                class="btn btn-main btn-submit btn_contact  "> Send
                                                                message </button>
                                                        </div>
                                                    </div>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>


                        </div>
                        <div class="col-md-4">
                            <div class="contact-right">
                                <div class="ctn-about-shop">
                                    <div class="pad-ctn-5">


                                        <div class="contact-details">
                                            <div class="cd-col">
                                                <i class="fa fa-map-signs"></i>
                                                <p>{{ $features[0]->desc }}</p>
                                            </div>
                                            <div class="cd-col">
                                                <i class="fa fa-phone"></i>
                                                <p><a href="tel:832-968-6668" itemprop="url">{{ $features[2]->desc }}</a></p>
                                            </div>
                                            <div class="cd-col">
                                                <i class="fa fa-envelope"></i>
                                                <p><a href="mailto:web@fastboy.net">{{ $features[1]->desc }}</a></p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="contact-hours">
                                    @foreach ($extras as $extra)
                                    <div class="cd-col" content="Monday 10:00 am7:00 pm">
                                        <span class="opening-hour-day">{{ $extra->name }}</span>
                                        <span class="opening-hour-time">{{ $extra->desc }}</span>
                                    </div>
                                        @endforeach

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="section">
                    <!-- Google map area -->
                    <div class="maps">
                        <div class="google-map" id="map" style="width:100%; height:450px; clear: both"><iframe
                                src="url({{asset('frontend')}}/fron)"
                                width="100%" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
</main>
