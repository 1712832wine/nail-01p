<main>
    <div class="pmain">
        <!-- TPL main book -->
        <div class="page-heading">
            <div class="container">
                <h2>Booking</h2>
            </div>
        </div>
        <div class="in-container bg-width">
            <div class="container">
                <div class="in-content">
                    <div class="row">
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <script src='https://www.google.com/recaptcha/api.js?hl=en' async defer></script>
                            <!-- Google reCaptcha -->
                            <script type="text/javascript">
                                function ezyCaptcha_surveyForm(token, is_submit) {
                                    is_submit = 1;
                                    if ($("#password").length) {
                                        //$("input:password").val(md5(clean_input($("#password").val())));
                                    }
                                    return true;
                                }

                            </script>
                            <div class="content-shop bg-ctn-special">
                                <div class="content-shop-booking">
                                    <form enctype="multipart/form-data" id="surveyForm" method="post" action="/book/add"
                                        class="form-horizontal">
                                        <div class="item-booking">
                                            <div id="optionTemplate">
                                                <div class="row">
                                                    <div class="col-lg-6 col-md-6 col-xs-12">
                                                        <div class="group-select">
                                                            <label>Service(required)</label>
                                                            <div
                                                                style="display: inline-block; position: relative; width: 100%;">
                                                                <select name="product_id[]"
                                                                    class="list_service form-control"
                                                                    data-validation-message="Please choose service">
                                                                    <option value="" price="" staff="[]">Select
                                                                        Service</option>
                                                                    <!--List Categories-->
                                                                    <optgroup label="Gel Powder Nails">
                                                                        <!--List service-->
                                                                        <option value="202" price="$40"
                                                                            staff='[{"id":"5","name":"Dung","note":"","image":"\/public\/library\/global\/no-photo.jpg","imageThumb":"\/public\/library\/global\/no-photo.jpg","imageClass":"no-photo"}]'>
                                                                            Full Set ($40 &amp; up) </option>
                                                                        <option value="203" price="$50" staff='[]'>
                                                                            Full Set with No-Chip Polish ($50)
                                                                        </option>
                                                                        <option value="204" price="$28" staff='[]'>
                                                                            Fill-in ($28 &amp; up) </option>
                                                                        <option value="205" price="$40" staff='[]'>
                                                                            Fill in with No-Chip Polish ($40)
                                                                        </option>
                                                                    </optgroup>
                                                                    <optgroup label="Natural Nail Services">
                                                                        <!--List service-->
                                                                        <option value="215" price="$18" staff='[]'>
                                                                            Spa Manicure ($18) </option>
                                                                        <option value="216" price="$28" staff='[]'>
                                                                            Collagen Manicure ($28) </option>
                                                                        <option value="217" price="$35" staff='[]'>
                                                                            No-Chip - Spa Manicure ($35) </option>
                                                                        <option value="264" price="$42" staff='[]'>
                                                                            Dip Powder ($42) </option>
                                                                    </optgroup>
                                                                    <optgroup label="Acrylic Powder Nails">
                                                                        <!--List service-->
                                                                        <option value="206" price="$32" staff='[]'>
                                                                            Full Set ($32 & up) </option>
                                                                        <option value="207" price="$45" staff='[]'>
                                                                            Full Set with No-Chip Polish ($45)
                                                                        </option>
                                                                        <option value="208" price="$23" staff='[]'>
                                                                            Fill-in ($23 & up) </option>
                                                                        <option value="209" price="$35" staff='[]'>
                                                                            Fill in with No-Chip Polish ($35)
                                                                        </option>
                                                                    </optgroup>
                                                                    <optgroup label="Pink & White Powder Nails">
                                                                        <!--List service-->
                                                                        <option value="197" price="$50" staff='[]'>
                                                                            Full Set ($50 & up) </option>
                                                                        <option value="198" price="$45" staff='[]'>
                                                                            Fill-in, Pink & White ($45 & up)
                                                                        </option>
                                                                        <option value="199" price="$30" staff='[]'>
                                                                            Fill-in, Pink only ($30 & up) </option>
                                                                        <option value="290" price="$65" staff='[]'>
                                                                            Full Set - Pink & White with Ombre ($65)
                                                                        </option>
                                                                        <option value="291" price="$50" staff='[]'>
                                                                            Fill-in Pink & White with Ombre ($50)
                                                                        </option>
                                                                    </optgroup>
                                                                    <optgroup label="Pedicures">
                                                                        <!--List service-->
                                                                        <option value="258" price="$30" staff='[]'>
                                                                            Spa Pedicure ($30) </option>
                                                                        <option value="259" price="$47" staff='[]'>
                                                                            No-chip, Spa Pedicure ($47) </option>
                                                                        <option value="260" price="$40" staff='[]'>
                                                                            Deluxe Pedicure ($40) </option>
                                                                        <option value="261" price="$50" staff='[]'>
                                                                            Pedi-in-a-box ($50) </option>
                                                                        <option value="262" price="$45" staff='[]'>
                                                                            Lavender & Jojoba ($45) </option>
                                                                        <option value="263" price="$65" staff='[]'>
                                                                            Volcano Pedicure ($65) </option>
                                                                        <option value="269" price="$55" staff='[]'>
                                                                            Milk & Honey Pedicure ($55) </option>
                                                                        <option value="270" price="$70" staff='[]'>
                                                                            Marine Pedicure ($70) </option>
                                                                    </optgroup>
                                                                    <optgroup label="Mani & Pedi Combo ">
                                                                        <!--List service-->
                                                                        <option value="224" price="$42" staff='[]'>
                                                                            Ladies Combo ($42) </option>
                                                                        <option value="225" price="$45" staff='[]'>
                                                                            Gentlemens Combo ($45) </option>
                                                                        <option value="226" price="$37" staff='[]'>
                                                                            Teenager Combo ($37) </option>
                                                                        <option value="227" price="$32" staff='[]'>
                                                                            Little Princess Combo ($32) </option>
                                                                    </optgroup>
                                                                    <optgroup label="Little Princess">
                                                                        <!--List service-->
                                                                        <option value="220" price="$12" staff='[]'>
                                                                            Manicure ($12) </option>
                                                                        <option value="221" price="$20" staff='[]'>
                                                                            Pedicure ($20) </option>
                                                                        <option value="222" price="$5" staff='[]'>
                                                                            Polish Change-Hands ($5) </option>
                                                                        <option value="223" price="$8" staff='[]'>
                                                                            Polish Change-Feet ($8) </option>
                                                                    </optgroup>
                                                                    <optgroup label="Waxing">
                                                                        <!--List service-->
                                                                        <option value="228" price="$10" staff='[]'>
                                                                            Brow Clean up ($10 & up) </option>
                                                                        <option value="229" price="$15" staff='[]'>
                                                                            Eyebrow Shape ($15 & up) </option>
                                                                        <option value="230" price="$8" staff='[]'>
                                                                            Upper Lip ($8) </option>
                                                                        <option value="231" price="$16" staff='[]'>
                                                                            Brow Clean up + Lip ($16 & up) </option>
                                                                        <option value="232" price="$12" staff='[]'>
                                                                            Chin ($12) </option>
                                                                        <option value="233" price="$15" staff='[]'>
                                                                            Cheek ($15) </option>
                                                                        <option value="234" price="$25" staff='[]'>
                                                                            Brow Clean Up + Lip + Chin ($25)
                                                                        </option>
                                                                        <option value="235" price="$40" staff='[]'>
                                                                            Full Face ($40) </option>
                                                                        <option value="236" price="$10" staff='[]'>
                                                                            Hairline ($10) </option>
                                                                        <option value="237" price="$8" staff='[]'>
                                                                            Sideburn Clean up ($8) </option>
                                                                        <option value="238" price="$22" staff='[]'>
                                                                            Chin & Cheek ($22) </option>
                                                                        <option value="239" price="$30" staff='[]'>
                                                                            Half Arm ($30 & up) </option>
                                                                        <option value="240" price="$40" staff='[]'>
                                                                            Full Arm ($40 & up) </option>
                                                                        <option value="241" price="$20" staff='[]'>
                                                                            Under Arm ($20) </option>
                                                                        <option value="242" price="$35" staff='[]'>
                                                                            Half Leg ($35 & up) </option>
                                                                        <option value="243" price="$60" staff='[]'>
                                                                            Full Leg ($60 & up) </option>
                                                                        <option value="244" price="$30" staff='[]'>
                                                                            Bikini ($30 & up) </option>
                                                                        <option value="245" price="$75" staff='[]'>
                                                                            Full Leg & Bikini ($75 & up) </option>
                                                                        <option value="246" price="$55" staff='[]'>
                                                                            Brazilian ($55 & up) </option>
                                                                        <option value="247" price="$15" staff='[]'>
                                                                            Neck ($15) </option>
                                                                        <option value="248" price="$20" staff='[]'>
                                                                            Stomach ($20 & up) </option>
                                                                        <option value="249" price="$50" staff='[]'>
                                                                            Back ($50 & up) </option>
                                                                        <option value="250" price="$40" staff='[]'>
                                                                            Chest ($40 & up) </option>
                                                                    </optgroup>
                                                                </select>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-6 col-md-6 col-xs-12">
                                                        <div class="group-select">
                                                            <label>Technician (optional)</label>
                                                            <select name="staff_id[]" class="list_staff form-control">
                                                                <option value="">Select technician</option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-lg-6 col-md-6 col-xs-12">
                                                    <div class="group-select box-date">
                                                        <label>Date</label>
                                                        <input type="text" name="booking_date" id="datetimepicker_v1"
                                                            class="form-control choose_date booking_date"
                                                            placeholder="Choice a date" value="05/24/2021"
                                                            typehtml="html">
                                                        <span class="fa fa-calendar"></span>
                                                    </div>
                                                </div>
                                                <div class="col-lg-6 col-md-6 col-xs-12">
                                                    <label>&nbsp;</label>
                                                    <button class='btn btn-search btn_action title booking_search'
                                                        type='button'>Search</button>
                                                </div>
                                            </div>
                                            <input type="hidden" name="booking_hours" value="" />
                                            <input type="hidden" name="booking_area_code" value="" />
                                            <input type="hidden" name="booking_phone" value="" />
                                            <input type="hidden" name="booking_name" value="" />
                                            <input type="hidden" name="nocaptcha" value="1" />
                                            <input type="hidden" name="g-recaptcha-response" value="" />
                                            <input type="hidden" name="notelist" value="" />
                                        </div>
                                        <div class="add-services addButton">
                                            <img src="/public/library/global/add-service-icon-new.png"> Add Another
                                            service
                                        </div>
                                    </form>
                                    <div id="book-info" class="infor-booking box_detail_info">
                                        <div class="staff" id="box_person"></div>
                                        <div class="time-booking col-md-12 databooktime" style="display: none">
                                            <h3 class="time_show"></h3>
                                            <div class="am">
                                                <label>Morning <span class="note_am_time"
                                                        style="color: red; font-style: italic;"></span></label>
                                                <ul class="timemorning">

                                                </ul>
                                            </div>
                                            <div class="pm">
                                                <label>Afternoon <span class="note_pm_time"
                                                        style="color: red; font-style: italic;"></span></label>
                                                <ul class="timeafternoon">

                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                    <script type="text/javascript">
                                        $(document).ready(function() {
                                            var formbk =
                                                '{"listperson":[],"notelist":[],"booking_date":"05\/24\/2021","booking_hours":""}';
                                            $('.btn-search').addClass('btn-main');
                                            loadEvent();

                                        });

                                    </script>
                                </div>
                            </div>

                            <!-- module 4 -->
                            <section style="display: none;">
                                <!--POPUP LOGIN-->
                                <div id="popup_login" class="white-popup mfp-hide">
                                    <div class="box_account_v1">
                                        <div class="modal_form_header">
                                            <h4>Login</h4>
                                        </div>
                                        <div class="popup_main_area">
                                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 border_right">
                                                <form enctype="multipart/form-data" id="form-login" method="POST"
                                                    action="/login/login_do">
                                                    <fieldset class="form-group">
                                                        <div class="box_login">
                                                            <h2> Been here before?</h2>
                                                            <div class="form_input_1">
                                                                <div class="form-control-wrapper">
                                                                    <input type="email" name="cus_email"
                                                                        placeholder="Enter your E-mail"
                                                                        data-validation="[EMAIL]"
                                                                        data-validation-message="Email is not valid!">
                                                                </div>
                                                            </div>
                                                            <div class="form_input_2">
                                                                <div class="form-control-wrapper">
                                                                    <input type="password" placeholder="Password"
                                                                        name="cus_password" data-validation="[NOTEMPTY]"
                                                                        data-validation-message="Password must not be empty!">
                                                                </div>
                                                            </div>
                                                            <div class="col_psw_v1" style="padding-bottom: 3px; ">

                                                                <div class="form-control-wrapper"
                                                                    style="margin-top: -10px;padding-bottom: 3px;float:right">
                                                                    <a href="/login/forgot-password/">Forgot
                                                                        password</a>

                                                                </div>
                                                            </div>
                                                            <div class="btn_submit_login">
                                                                <button class="submit" type="submit">Login</button>
                                                            </div>
                                                        </div>
                                                    </fieldset>
                                                </form>
                                            </div>
                                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                                <div class="box_register">
                                                    <h2>New Account?</h2>
                                                    <div class="btn_submit_login">
                                                        <button class="submit" type="submit"
                                                            onclick="window.location.href = '/register/'">Create a
                                                            Username</button>
                                                    </div>
                                                    <div class="btn_login_social">

                                                        <a href="" class="btn btn_facebook_v1" title="facebook login">
                                                            <i class="fa fa-facebook"></i>
                                                            <span>Facebook</span>
                                                        </a>



                                                        <a class="btn btn_gplus_v1" href="" title="gplus login">
                                                            <i class="fa fa-google-plus"></i>
                                                            <span>Google</span>
                                                        </a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <!--POPUP BOOKING-->
                                <div id="open_booking" class="white-popup mfp-hide">
                                    <div class="box_account_v1">
                                        <div class="modal_form_header">
                                            <h4>Message</h4>
                                        </div>
                                        <div class="popup_main_area">
                                            <form enctype="multipart/form-data" id="booking_check" name="booking_check">
                                                <p style="font-weight: bold; font-size: 18px;">Confirm booking
                                                    information ?</p>
                                                <span
                                                    style="color: red; font-style: italic; margin: 10px 0; display: block;">We
                                                    will send a text message to you via the number below after we
                                                    confirm the calendar for your booking!</span>
                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label class="form-control-label">Your name</label>
                                                            <div class="form-control-wrapper">
                                                                <input name="input_name" type="text"
                                                                    class="form-control" type="text"
                                                                    data-validation="[NOTEMPTY]"
                                                                    data-validation-message="Please enter your name!" />
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-2" style="display: none;">
                                                        <div class="form-group">
                                                            <label class="form-control-label">Area code</label>
                                                            <select name="area_code" class="form-control"
                                                                defaultvalue="1">
                                                                <option value="1">US (+1)</option>
                                                                <option value="84">VN (+84)</option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label class="form-control-label">Phone number</label>
                                                            <div class="form-control-wrapper">
                                                                <input name="phone_number" type="text"
                                                                    class="form-control inputPhone" type="text"
                                                                    placeholder="Ex: (123) 123-1234"
                                                                    data-validation="[NOTEMPTY]"
                                                                    data-validation-message="Phone number invalid" />
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row booking-note">
                                                    <div class="col-md-12">
                                                        <div class="form-group">
                                                            <label class="form-control-label">Note
                                                                (Optional)</label>
                                                            <div class="form-control-wrapper">
                                                                <textarea name="notelist"
                                                                    placeholder="Max length 200 character" rows="5"
                                                                    class="form-control" maxlength="200"></textarea>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <button class="btn btn-success btn_confirmed "
                                                    type="button">Confirm</button>
                                                <button class="btn btn-danger btn-inline btn_cancel"
                                                    type="button">Cancel</button>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </section>
                            <script>
                                checktimebooking = 1;
                                $(document).ready(function() {
                                    $("#form-login").validate({
                                        submit: {
                                            settings: {
                                                button: "[type='submit']",
                                                inputContainer: '.form-group',
                                                errorListClass: 'form-tooltip-error',
                                            }
                                        }
                                    });
                                    // check Time
                                    $('form#booking_check').validate({
                                        submit: {
                                            settings: {
                                                clear: 'keypress',
                                                display: "inline",
                                                button: ".btn_confirmed",
                                                inputContainer: 'form-group',
                                                errorListClass: 'form-tooltip-error',
                                            },
                                            callback: {
                                                onSubmit: function(node, formdata) {
                                                    // console.log("toi day");
                                                    var areacode =
                                                    1; //$("select[name='area_code']").val();
                                                    var phone_number = $(
                                                        "input[name='phone_number']").val();
                                                    var cus_name = $("input[name='input_name']")
                                                        .val();

                                                    var notelist = $("textarea[name='notelist']")
                                                        .val();
                                                    $("input[name='notelist']").val(notelist);

                                                    if (phone_number != "" && phone_number.length >
                                                        6) {
                                                        $("input[name='booking_phone']").val(
                                                            phone_number);
                                                        $("input[name='booking_area_code']").val(
                                                            areacode);
                                                    }

                                                    if (cus_name) {
                                                        $("input[name='booking_name']").val(
                                                            cus_name);
                                                    }


                                                    if (enableRecaptcha) {
                                                        var check_google = $(
                                                            "#g-recaptcha-response").val();
                                                        if (typeof(check_google) != "undefined" &&
                                                            check_google != "") {
                                                            $("input[name='g-recaptcha-response']")
                                                                .val(check_google);
                                                            $("#surveyForm").submit();
                                                        }
                                                    } else {
                                                        $("#surveyForm").submit();
                                                    }

                                                    return false;
                                                }
                                            }
                                        }
                                    });
                                });

                            </script>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</main>
