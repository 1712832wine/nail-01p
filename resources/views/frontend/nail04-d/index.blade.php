<main>
    <div class="pmain">
        <div class="main-content">

            <div class="section-slider-wrap">
                <section class="section-slider">
                    <div class="slider-width-height"
                        style="display: inline-block; width: 100%; height: 1px; overflow: hidden;">

                        <div id="slider-option" data-autoplay="true" data-autoplayDelay="5000"></div>

                        <div class="fixed" style="width: 100%;"></div>
                        <img src="{{ asset('storage') }}/photos/{{ $imgs_carousel[0]['url'] }}"
                            style="width: 100%; height: auto;" alt="">
                        <!-- END USE FOR CALCULATOR START WIDTH HEIGHT -->
                    </div>

                    <div class="slider-pro" id="my-slider" style="display: none;">
                        <div class="sp-slides">
                            @foreach ($imgs_carousel as $img)
                        <div class="sp-slide">
                            <div class="sp-layer sp-static">
                                <a href="{{ route('home') }}" target="_self" title="slide7.png">
                                    <img class="sp-image" src="{{ asset('storage') }}/photos/{{ $img['url'] }}"
                                        alt="slide7.png" />
                                </a>
                            </div>
                        </div>
                    @endforeach
                        </div>
                    </div>
                </section>
            </div>
            <section class="section section-about wow fadeInUp">
                <div class="container">
                    <div class="row">
                        <div class="col-md-6 col-xs-12">
                            <div class="clearfix"><img
                                    src="{{ asset('storage') }}/photos/{{ $imgs_carousel[1]['url'] }}"
                                    caption="false" /></div>
                        </div>
                        <div class="col-md-6 col-xs-12">
                            <h2>Welcome to Greenery Nails & Spa</h2>
                            <p style="text-align: justify;">Located at a beautiful place in Houston, Texas 77042,
                                Nails & Spa offers you the ultimate in pampering and boosting your natural beauty
                                with our whole-hearted, creative & professional staff.</p>
                            <p style="text-align: justify;">Our salon takes pride in providing our valued customers
                                all good services and top-high quality products as well as materials. You can find
                                all nail-related services, from Manicure, Pedicure to Artificial Nails. At Nails &
                                Spa, a wide choice is provided to customers, so you can use any services that you
                                like. All of our services are at decent prices, so there is no hesitation when you
                                use one or even more services at our salon.</p>
                        </div>
                    </div>
                </div>
            </section>
            <section class="section section-special bg-main">
                <div class="container">
                    <div class="center-title wow fadeInDown">
                        <h2 class="mtitle">Special Services</h2>
                    </div>
                    <div class="special-list">
                        @foreach ($services as $service)
                        <div class="item wow fadeInUp">
                            <div class="thumb"><a href="#"><img
                                        src="{{ asset('storage') }}/photos/{{ json_decode($service['images'])[0]  }}"
                                        class="imgrps" caption="false" /></a>
                                <div class="discount">{{json_decode($service['features'])[0]->desc}}</div>
                            </div>
                            <div class="info">
                                <h3><a href="/services-69-acrylic-powder-nails">{{$service['name']}}</a></h3>
                            </div>
                        </div>
                        @endforeach
                    </div>
                    <div class="view-all wow fadeInUp" data-wow-delay="1.2s"><a href="/book" class="btn btn-main">MAKE
                            AN APPOINTMENT</a></div>
                </div>
            </section>
            <section class="section section-services">
                <div class="container">
                    <div class="center-title wow fadeInDown">
                        <h2 class="mtitle">Our Services</h2>
                    </div>
                    <div class="services-list wow fadeInUp">
                        <div class="row">
                            @foreach ($gallery_list as $item)
                                <div class="col-xs-6 col-sm-6 col-md-4">
                                    <div class="pointer m-magnific-popup" data-group="gallery-14" title="Nails Design"
                                        href="{{ asset('storage') }}/photos/{{ $item['url'] }}">
                                        <div class="m-gallery-box">
                                            <div class="m-image-bg"
                                                style="background-image: url('{{ asset('storage') }}/photos/{{ $item['url'] }}');">
                                                <img itemprop="image"
                                                    src="{{ asset('storage') }}/photos/{{ $item['url'] }}"
                                                    alt="Nails Design">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            @endforeach
                        </div>
                    </div>
                </div>
            </section>
            <section class="section section-testimonials bg-main">
                <div class="container">
                    <div class="center-title wow fadeInDown">
                        <h2 class="mtitle">Testimonials</h2>
                    </div>
                    <div class="testimonials-list owl-carousel">
                        @foreach ($carousel_customers as $item)

                        <div class="item">
                            <div class="testimonial-quote">“</div>
                            {!! $item['content'] !!}
                            <div class="name">{{$item['title']}}</div>
                        </div>
                        @endforeach
                    </div>
                </div>
            </section>
        </div>
    </div>
</main>
