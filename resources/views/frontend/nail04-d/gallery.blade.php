<main>
    <div class="pmain">
        <!-- Tpl main gallery -->
        <!-- Support render gallery, gallery_category, gallery_data-->

        <div class="page-heading">
            <div class="container">
                <h2>Gallery</h2>
            </div>
        </div>
        <div class="in-container">
            <div class="container">
                <div class="in-content">
                    <div class="gallery-box-wrap">
                        <div class="row">
                            <!-- Custome Height with .gallery-box .image-bg{padding-bottom: 75%;} -->
                            <!-- List category -->
                            @foreach ($albums as $index => $album)
                                <div class="col-xs-6 col-sm-6 col-md-4">
                                    <a itemprop="url" title="Nails Design 77042"
                                        href="/gallery-detail/{{ $album['id'] }}">
                                        <div class="gallery-box">
                                            <div class="image-bg"
                                                style="background-image: url('{{ asset('storage') }}/photos/{{ $lists[$index][0]['url'] }}');">
                                                <img itemprop="image"
                                                    src="{{ asset('storage') }}/photos/{{ $lists[$index][0]['url'] }}">
                                            </div>
                                            <div class="gallery-title">
                                                <h4 itemprop="name">{{ $album['name'] }}</h4>
                                            </div>
                                        </div>
                                    </a>
                                </div>
                            @endforeach
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</main>
