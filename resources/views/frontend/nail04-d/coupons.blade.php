<main>
    <div class="pmain">
        <div class="page-heading">
            <div class="container">
                <h2>Coupon</h2>
            </div>
        </div>
        <div class="in-container">
            <div class="container">
                <div class="in-content">
                    <section class="coupon_code_v1">
                        <div class="row">
                            @foreach ($list as $item)
                                <div class="col-xs-12 col-sm-6 col-md-6">
                                    <div class="pointer m-magnific-popup" data-group="coupon" title="Special Promotions"
                                        href="{{ asset('storage') }}/photos/{{ $item['url'] }}">
                                        <div class="m-coupon-box">
                                            <img itemprop="image" src="{{ asset('storage') }}/photos/{{ $item['url'] }}"
                                                alt="Special Promotions" />
                                        </div>
                                    </div>
                                </div>
                            @endforeach
                        </div>
                    </section>
                </div>
            </div>
        </div>
    </div>
</main>
