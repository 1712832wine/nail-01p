<header class="header">
    <div class="flag-freeze-header-mobile menu_mobile_v1 hidden-md hidden-lg menu-1024-hidden">
        <div class="mobile_logo">
            <a href="/"><img
                    src="http://fnail04d.fastboywebsites.com/uploads/fnail0lsagxak/attach/1561086700_logofnail04d.png"
                    class="imgrps" alt="Greenery Nails & Spa"></a>
        </div>
        <div class="mobile_menu_container_v1">
            <div class="mobile-menu clearfix">
                <nav id="mobile_dropdown">
                    <ul>
                        <li><a itemprop="url" href="/" title="Home">Home</a></li>
                        <li><a itemprop="url" href="/about" title="About Us">About Us</a></li>
                        <li><a itemprop="url" href="/services" title="Services">Services</a></li>
                        <li><a itemprop="url" href="/book" title="Booking">Booking</a></li>
                        <li><a itemprop="url" href="/coupons" title="Coupons">Coupons</a></li>
                        <li><a itemprop="url" href="/giftcards" title="Coupons">Gift Cards</a></li>
                        <li><a itemprop="url" href="/gallery" title="Gallery">Gallery</a></li>
                        <li><a itemprop="url" href="/contact" title="Contact Us">Contact Us</a></li>
                    </ul>
                </nav>
            </div>
        </div><a href="/book" class="btn-main btn_make_appointment">Booking</a>
    </div>
    <div class="head-top hidden-xs hidden-sm">
        <div class="container">
            <div class="head-top-inner dflex-sb">
                <ul class="head-top-social">

                    <li><a target="_blank" href="https://www.facebook.com/FastboyMarketingAgency/">
                            <img src="{{asset('frontend')}}/public/library/social/square/facebook.png" alt="facebook">
                        </a></li>

                    <!-- google link -->
                    <li><a target="_blank" href="https://www.google.com/maps/place/Fast+Boy+Marketing">
                            <img src="{{asset('frontend')}}/public/library/social/square/google-plus.png" alt="google-plus">
                        </a></li>

                    <!-- twitter link -->

                    <!-- youtube link -->
                    <li><a target="_blank" href="https://www.youtube.com">
                            <img src="{{asset('frontend')}}/public/library/social/square/youtube.png" alt="youtube">
                        </a></li>

                    <!-- instagram link -->
                    <li><a target="_blank" href="https://www.instagram.com/">
                            <img src="{{asset('frontend')}}/public/library/social/square/instagram.png" alt="instagram">
                        </a></li>

                    <!-- yelp link -->
                    <li><a target="_blank" href="https://www.yelp.com/">
                            <img src="{{asset('frontend')}}/public/library/social/square/yelp.png" alt="yelp">
                        </a></li>

                    <!-- vimeo link -->

                    <!-- blog link -->

                    <!-- pinterest link -->

                    <!-- yellowpage link -->
                    <li><a target="_blank" href="http://www.yellowpages.vn/" itemprop="url">
                            <img src="{{asset('frontend')}}/public/library/social/square/yellowpages.png" alt="yellowpages">
                        </a></li>

                    <!-- foursquare link -->
                    <li><a target="_blank" href="https://foursquare.com/" itemprop="url">
                            <img src="{{asset('frontend')}}/public/library/social/square/foursquare.png" alt="foursquare">
                        </a></li>
                </ul>
                <div class="head-top-right">
                    <div class="ht-item"><i class="fa fa-phone"></i>
                        <div class="info">
                            <div class="info-name">Phone number</div>
                            <div class="info-content">832-968-6668</div>
                        </div>
                    </div>
                    <div class="ht-item"><i class="fa fa-envelope"></i>
                        <div class="info">
                            <div class="info-name">Send message</div>
                            <div class="info-content"><a href="mailto:web@fastboy.net?Subject=Hello%20again"
                                    title="web@fastboy.net" target="_top" itemprop="email">web@fastboy.net</a></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="flag-freeze-header header-main hidden-xs hidden-sm">
        <div class="container">
            <div class="header-nav dflex-sb"><a href="/" class="logo">
                    <img src="http://fnail04d.fastboywebsites.com/uploads/fnail0lsagxak/attach/1561086700_logofnail04d.png"
                        class="imgrps" alt="Greenery Nails & Spa" itemprop="image">
                </a>
                <div class="head-nav-right dflex">
                    <nav class="navbar">
                        <div class="navbar-collapse collapse">
                            <ul class="nav navbar-nav">
                                <li><a itemprop="url" href="/" title="Home">Home</a></li>
                                <li><a itemprop="url" href="/about" title="About Us">About Us</a></li>
                                <li><a itemprop="url" href="/services" title="Services">Services</a></li>
                                <li><a itemprop="url" href="/coupons" title="Coupons">Coupons</a></li>
                                <li><a itemprop="url" href="/giftcards" title="Coupons">Gift Cards</a></li>
                                <li><a itemprop="url" href="/gallery" title="Gallery">Gallery</a></li>
                                <li><a itemprop="url" href="/contact" title="Contact Us">Contact Us</a></li>
                            </ul>
                        </div>
                    </nav><a href="/book" class="btn-main">Booking</a>
                </div>
            </div>
        </div>
    </div>
</header>
