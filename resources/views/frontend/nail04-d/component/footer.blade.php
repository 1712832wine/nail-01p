<footer class="footer">
    <div class="footer-main">
        <div class="container">
            <div class="footer-info">
                <h3>Make an Appointment?</h3>
                <p><a href="http://www.signaturenailspaca.com/our-services/professional-nail-care-for-ladie-and-gentleman/"
                        ping="/url?sa=t&source=web&rct=j&url=http://www.signaturenailspaca.com/our-services/professional-nail-care-for-ladie-and-gentleman/&ved=2ahUKEwip0_aioPrkAhVNQN4KHeeFDhsQFjAAegQIARAB"></a>
                </p>
                <div class="ellip"><a href="tel:832-968-6668">832-968-6668</a></div>
            </div>
        </div>
    </div>
    <div class="footer-copyright">
        <div class="container">
            <div class="footer-inner dflex-sb">
                <div class="footer-social hidden-md hidden-lg">
                    <ul class="head-top-social">

                        <li><a target="_blank" href="https://www.facebook.com/FastboyMarketingAgency/">
                                <img src="{{asset('frontend')}}/public/library/social/square/facebook.png" alt="facebook">
                            </a></li>

                        <!-- google link -->
                        <li><a target="_blank" href="https://www.google.com/maps/place/Fast+Boy+Marketing">
                                <img src="{{asset('frontend')}}/public/library/social/square/google-plus.png" alt="google-plus">
                            </a></li>

                        <!-- twitter link -->

                        <!-- youtube link -->
                        <li><a target="_blank" href="https://www.youtube.com">
                                <img src="{{asset('frontend')}}/public/library/social/square/youtube.png" alt="youtube">
                            </a></li>

                        <!-- instagram link -->
                        <li><a target="_blank" href="https://www.instagram.com/">
                                <img src="{{asset('frontend')}}/public/library/social/square/instagram.png" alt="instagram">
                            </a></li>

                        <!-- yelp link -->
                        <li><a target="_blank" href="https://www.yelp.com/">
                                <img src="{{asset('frontend')}}/public/library/social/square/yelp.png" alt="yelp">
                            </a></li>

                        <!-- vimeo link -->

                        <!-- blog link -->

                        <!-- pinterest link -->

                        <!-- yellowpage link -->
                        <li><a target="_blank" href="http://www.yellowpages.vn/" itemprop="url">
                                <img src="{{asset('frontend')}}/public/library/social/square/yellowpages.png" alt="yellowpages">
                            </a></li>

                        <!-- foursquare link -->
                        <li><a target="_blank" href="https://foursquare.com/" itemprop="url">
                                <img src="{{asset('frontend')}}/public/library/social/square/foursquare.png" alt="foursquare">
                            </a></li>
                    </ul>
                </div>
                <div class="copyright">
                    <p>© Copyright by Greenery Nails & Spa. All Rights Reserved.</p>
                </div>
            </div>
        </div>
    </div>
</footer>
<!-- Active freeze footer by delete style display: none -->
<div class="freeze-footer">
    <ul>
        <li><a href="tel:832-968-6668" class="btn btn-main btn_call_now btn-call">832-968-6668</a></li>
    </ul>
</div>
