<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" dir="ltr" lang="en" xmlns:og="http://ogp.me/ns#"
    xmlns:fb="http://www.facebook.com/2008/fbml" itemscope itemtype="https://schema.org/NailSalon">

<head>
    <!-- Language -->
    <meta http-equiv="content-language" content="en" />
    <link rel="alternate" href="http://fnail04d.fastboywebsites.com" hreflang="x-default">
    <link rel="alternate" href="http://fnail04d.fastboywebsites.com" hreflang="en-US">

    <!-- Meta limit -->

    <!-- Meta Tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width,initial-scale=1.0" />
    <meta http-equiv="content-type" content="text/html; charset=UTF-8" />
    <meta name="description" content="" />
    <meta name="keywords" content="" />
    <meta name="author" content="" />

    <!-- OG -->
    <meta property="og:title" content="" />
    <meta property="og:description" content="" />
    <meta property="og:image" content="" />
    <meta property="og:url" content="http://fnail04d.fastboywebsites.com/" />
    <meta property="og:type" content="website" />
    <meta property="og:site_name" content="Greenery Nails & Spa" />

    <!-- Dublin Core -->
    <link rel="schema.DC" href="http://purl.org/dc/elements/1.1/">
    <meta name="DC.title" content="">
    <meta name="DC.identifier" content="http://fnail04d.fastboywebsites.com/">
    <meta name="DC.description" content="">
    <meta name="DC.subject" content="">
    <meta name="DC.language" scheme="UTF-8" content="">
    <meta itemprop="priceRange" name="priceRange" content="Price range">

    <!-- GEO meta -->
    <meta name="geo.region" content="">
    <meta name="geo.placename" content="">
    <meta name="geo.position" content="">
    <meta name="ICBM" content="">

    <!-- Page Title -->
    <title>Greenery Nails & Spa</title>
    <base href="/themes/fnail04d/assets/">

    <!-- canonical -->
    <link rel="canonical" href="http://fnail04d.fastboywebsites.com">



    <!-- Favicons -->
    <link rel="icon" href="http://fnail04d.fastboywebsites.com/uploads/fnail0lsagxak/attach/1560926724_fbm_fvc.png"
        type="image/x-icon">
    <link rel="shortcut icon"
        href="http://fnail04d.fastboywebsites.com/uploads/fnail0lsagxak/attach/1560926724_fbm_fvc.png"
        type="image/x-icon">

    <!-- CSS -->
    <link href="https://fonts.googleapis.com/css?family=Baumans" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Montserrat:400,500,700" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Dancing+Script" rel="stylesheet">

    <link rel="stylesheet" type="text/css" href='{{ asset('frontend') }}/public/library/jquery-ui/jquery-ui.min.css'>
    <link rel="stylesheet" type="text/css" href='{{ asset('frontend') }}/public/library/animate/animate.css'>
    <link rel="stylesheet" type="text/css" href='{{ asset('frontend') }}/public/library/validation/validation.css'>
    <link rel="stylesheet" type="text/css"
        href='{{ asset('frontend') }}/public/library/bootstrap/v3.3.7/bootstrap.min.css'>
    <link rel="stylesheet" type="text/css"
        href='{{ asset('frontend') }}/public/library/font-awesome/v4.7.0/css/font-awesome.min.css'>
    <link rel="stylesheet" type="text/css"
        href='{{ asset('frontend') }}/public/library/owl-carousel/v2.2.1/owl.carousel.min.css'>
    <link rel="stylesheet" type="text/css" href='{{ asset('frontend') }}/public/library/flex-label/flex-label.css'>
    <link rel="stylesheet" type="text/css" href='{{ asset('frontend') }}/public/library/pnotify/pnotify.css'>
    <link rel="stylesheet" type="text/css" href='{{ asset('frontend') }}/public/library/slider-pro/slider-pro.min.css'>
    <link rel="stylesheet" type="text/css"
        href='{{ asset('frontend') }}/public/library/magnific-popup/magnific-popup.css'>
    <link rel="stylesheet" type="text/css"
        href='{{ asset('frontend') }}/public/library/bootstrap-datetimepicker/bootstrap-datetimepicker.min.css'>
    <link rel="stylesheet" type="text/css" href='{{ asset('frontend') }}/public/library/signature/signature.css'>
    <link rel="stylesheet" type="text/css" href='{{ asset('frontend') }}/public/library/instagram/instafeed.min.css'>

    <link rel="stylesheet" type="text/css" href='{{ asset('frontend') }}/themes/fnail04d/assets/css/meanmenu.css'>
    <link rel="stylesheet" type="text/css" href='{{ asset('frontend') }}/themes/fnail04d/assets/css/style.css'>
    <link rel="stylesheet" type="text/css" href='{{ asset('frontend') }}/themes/fnail04d/assets/css/responsive.css'>
    <link rel="stylesheet" type="text/css" href='{{ asset('frontend') }}/public/library/global/css/news.css?v=1.0'>
    <link rel="stylesheet" type="text/css" href='{{ asset('frontend') }}/public/library/global/css/consent.css'>
    <link rel="stylesheet" type="text/css" href='{{ asset('frontend') }}/themes/fnail04d/assets/css/custom.css?v=1.3'>
    <link rel="stylesheet" type="text/css" href='{{ asset('frontend') }}/themes/fnail04d/assets/css/custom-2.css'>

    <script type="text/javascript">
        var dateFormatBooking = "MM/DD/YYYY";
        var minDateBooking = '05/24/2021';
        var posFormat = "1,0,2";
        var timeMorning = '["10:00","10:30","11:00","11:30"]';
        var timeAfternoon =
            '["12:00","12:30","13:00","13:30","14:00","14:30","15:00","15:30","16:00","16:30","17:00","17:30","18:00","18:30","19:00"]';
        var bookLogin = "open_booking";
        var beforeTime = "1";
        var beforeDay = 0;
        var currDateT = "2021/05/24";
        var checktimebooking = 1;
        var enableRecaptcha = 0;
        var hoursTimeFormat = 12;
        var phoneFormat = "(000) 000-0000";
        var company_phone = "832-968-6668";
        var num_paging = "12";
        var site = "idx";
        var site_act = "";

        let facebook_embed = {
            id_fanpage: "https://www.facebook.com/FastboyMarketingAgency",
            appId: "",
            width: 450,
            height: 300,
            tabs: "timeline",
            show_facepile: true,
            small_header: true,
            likebox_enable: true,
            likebox_show_faces: false,
            likebox_stream: true,
        };
        let google_id_fanpage = "";
        let twitter_id_fanpage = "";

    </script>

    <script type="text/javascript" src="{{ asset('frontend') }}/public/library/jquery/v3.3.1/jquery.min.js"></script>
    <script type="text/javascript" src="{{ asset('frontend') }}/public/library/jquery-ui/jquery-ui.min.js"></script>
    <script type="text/javascript" src="{{ asset('frontend') }}/public/library/jquery-ui/jquery-migrate.min.js">
    </script>
    <script type="text/javascript" src="{{ asset('frontend') }}/public/library/validation/validation.min.js">
    </script>
    <script type="text/javascript" src="{{ asset('frontend') }}/public/library/flex-label/flex-label.js"></script>
    <script type="text/javascript" src="{{ asset('frontend') }}/public/library/mask/mask.min.js"></script>
    <script type="text/javascript" src="{{ asset('frontend') }}/public/library/bootstrap/v3.3.7/bootstrap.min.js">
    </script>
    <script type="text/javascript" src="{{ asset('frontend') }}/public/library/moment/moment-with-locales.js">
    </script>
    <script type="text/javascript"
        src="{{ asset('frontend') }}/public/library/bootstrap-datetimepicker/bootstrap-datetimepicker.js"></script>
    <script type="text/javascript" src="{{ asset('frontend') }}/public/library/meanmenu/meanmenu.js"></script>
    <script type="text/javascript" src="{{ asset('frontend') }}/public/library/slider-pro/slider-pro.min.js">
    </script>
    <script type="text/javascript"
        src="{{ asset('frontend') }}/public/library/owl-carousel/v2.2.1/owl.carousel.min.js"></script>
    <script type="text/javascript" src="{{ asset('frontend') }}/public/library/pnotify/pnotify.js"></script>
    <script type="text/javascript" src="{{ asset('frontend') }}/public/library/wow/wow.min.js"></script>
    <script type="text/javascript" src="{{ asset('frontend') }}/public/library/magnific-popup/magnific-popup.min.js">
    </script>
    <script type="text/javascript" src="{{ asset('frontend') }}/public/library/lightbox/lightbox.js"></script>
    <script type="text/javascript" src="{{ asset('frontend') }}/public/library/instagram/instafeed.min.js"></script>
    <script type="text/javascript" src="{{ asset('frontend') }}/public/library/signature/signature.min.js"></script>
    <script type="text/javascript" src="{{ asset('frontend') }}/public/library/global/js/js_php.js"></script>

    <style type="text/css">
        .footer-copyright {
            border-width: 8px;
        }

        #back-top {
            bottom: 0;
        }

        .freeze-footer {
            bottom: 75px;
        }

        @media (max-width: 575px) {
            .footer-main {
                padding: 75px 0;
            }
        }

        .contact-details p {
            margin-left: 30px;
        }

        .section-services h3 a {
            font-size: 21px;
        }

        @media(max-width: 667px) {
            .gallery-title {
                font-size: 10px;
            }
        }

        h3.stitle {
            font-size: 28px;
            font-family: inherit;
        }

        .footer-main p {
            padding: 0;
        }

        .section-about h2 {
            font-size: 30px;
            font-family: inherit;
        }

        .section-services h2,
        h2.mtitle,
        .footer-main h3 {
            font-family: inherit;
        }

        @media (min-width: 768px) {
            .services-list .col-sm-4 {
                width: 24.99%;
            }
        }

        .section-about p {
            color: #000;
            font-size: 20px;
        }

        .section-about h2 {
            font-size: 40px;
            font-family: inherit;
        }

        @media only screen and (max-width: 687px) {
            .section-about h2 {
                font-size: 30px;
                font-family: inherit;
            }
        }

        .navbar-nav>li>a {
            font-size: 20px;
        }

    </style>

    <!-- Webmaster tools -->
    <!--Webmaster tools-->
    <!-- fb retargeting -->

    <!-- Schema Script -->
</head>

<body>
    <h1 itemprop="name" style="display: none">Luxury Nails & Spa</h1>
    <div id="fb-root"></div>
    <!-- Active freeze header by insert this html into the website page -->
    <div class="wrap-freeze-header"><input type="hidden" name="activeFreezeHeader" value="1" /></div>
    {{-- HEADER --}}
    @include('frontend.nail04-d.component.header',
    ['header'=>
    App\Models\Product::where('category_id',
    App\Models\Category::where('name','Header And Footer')->first()['id'])->first()])
    {{-- MAIN --}}
    {{ $slot }}
    {{-- FOOTER --}}
    @php
        $album = App\Models\Album::where([['type', 'Social'], ['status', 'PUBLISHED']])->first();
        $social = App\Models\Asset::where('albums_id', $album['id'])->orderBy('priority', 'desc')->get();
    @endphp
    @include('frontend.nail04-d.component.footer',['header'=>
    App\Models\Product::where('category_id',
    App\Models\Category::where('name','Header And Footer')->first()['id'])->first(),
    'social'=>$social]))
    <script type="text/javascript">
        var enable_booking = "1";
        if (enable_booking == 0) {
            $(".btn_make_appointment").remove();
        }

    </script>

    <script type="text/javascript" src="{{ asset('frontend') }}/public/library/global/js/consent.js"></script>
    <script type="text/javascript" src="{{ asset('frontend') }}/themes/fnail04d/assets/js/script.js"></script>
    <script type="text/javascript" src="{{ asset('frontend') }}/themes/fnail04d/assets/js/app.js?v=1.4"></script>

    <!-- Google analytics -->
    <script>
        (function(i, s, o, g, r, a, m) {
            i['GoogleAnalyticsObject'] = r;
            i[r] = i[r] || function() {
                (i[r].q = i[r].q || []).push(arguments)
            }, i[r].l = 1 * new Date();
            a = s.createElement(o),
                m = s.getElementsByTagName(o)[0];
            a.async = 1;
            a.src = g;
            m.parentNode.insertBefore(a, m)
        })(window, document, 'script', 'https://www.google-analytics.com/analytics.js', 'ga');

        ga('create', '', 'auto');
        ga('send', 'pageview');

    </script>
    <script type="text/javascript">
        var enable_booking = "1";
        if (enable_booking == 0) {
            $(".btn_make_appointment").remove();
        }

    </script>
    <!-- End - Google analytics -->
    <!-- gg adwords remarketing -->

    <script type="text/javascript">
    </script>
</body>

</html>
