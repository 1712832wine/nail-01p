<main>
    <div class="pmain">
        <!-- Use for get current id -->
        <input type="hidden" name="group_id" value="">


        <div class="page-heading">
            <div class="container">
                <h2>Our Service</h2>
            </div>
        </div>
        @foreach ($services as $index => $service)
        @if ($index % 2)
        <div class="in-container main-content animation_sroll_jumpto">
            <!-- Animation scroll to block service: with only add class 'animation_sroll_jumpto' in element contain entire html service data -->
            <div class="container page-container">
                <div class="row service-row sroll_jumpto" id="sci_68">
                    <div class="col-sm-4 col-md-4 text-center">
                        <div class="circle-service-image"
                            style="background-image: url('{{ asset('storage') }}/photos/{{ json_decode($service['images'])[0] }}')">
                        </div>
                    </div>
                    <div class="col-sm-8 col-md-8">
                        <h2 id="gel_powder_nails" class="service-name">{{ $service['name'] }}</h2>
                        @foreach (json_decode($service['features']) as $item)
                        <div class="box_service item-202">
                            <div class="detail-price-item">
                                <span class="detail-price-name">{{ $item->name }}</span>
                                <span class="detail-price-dots"></span>
                                <span class="detail-price-number">{{ $item->desc }}</span>
                            </div>
                        </div>
                        @endforeach
                    </div>
                </div>

        @else
                <div class="row service-row sroll_jumpto" id="sci_70">
                    <div class="col-sm-4 col-md-4 text-center hidden-lg hidden-md">
                        <div class="circle-service-image"
                            style="background-image: url({{ asset('storage') }}/photos/{{ json_decode($service['images'])[0] }})">
                        </div>
                    </div>
                    <div class="col-sm-8 col-md-8">
                        <h2 id="natural_nail_services" class="service-name">{{ $service['name'] }}</h2>
                        @foreach (json_decode($service['features']) as $item)
                        <div class="box_service item-215">
                            <div class="detail-price-item">
                                <span class="detail-price-name">{{ $item->name }}</span>
                                <span class="detail-price-dots"></span>
                                <span class="detail-price-number">{{ $item->desc }}</span>
                            </div>
                        </div>
                        @endforeach

                    </div>
                    <div class="col-sm-4 col-md-4 text-center hidden-sm hidden-xs">
                        <div class="circle-service-image"
                            style="background-image: url('http://fnail04d.fastboywebsites.com/uploads/fnail0lsagxak/product/sv-04c-4.jpg')">
                        </div>
                    </div>
                </div>
            </div>
        </div>
        @endif
        @endforeach
    </div>
</main>
