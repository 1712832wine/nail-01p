<main>
    <div class="pmain">
        @foreach ($articles as $item)
        <div class="page-heading">
            <div class="container">
                <h2>About us</h2>
            </div>
        </div>
        <section class="section section-about wow fadeInUp">
            <div class="container">
                <div class="row">
                    <div class="col-md-7 about-left">
                        {!! $item['content'] !!}
                    </div>
                    <div class="col-md-5">
                        <div class="clearfix"><img src="{{ asset('storage') }}/photos/{{ json_decode($item['image'])[0] }}"
                                caption="false" /></div>
                    </div>
                </div>
            </div>
        </section>
        <section class="section section-about-text">
            <div class="container">
                <h3>"Professional Nail Care for Ladies and Gentleman"</h3>
            </div>
        </section>
        @endforeach
    </div>
</main>
