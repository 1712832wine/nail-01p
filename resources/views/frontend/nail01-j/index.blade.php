{{-- CAROUSEL --}}
<section class="clearfix">
    <div class="container-druid">
        <div class="wpbSlider-wrapper">
            <div class="slider-pro" id="my-slider">
                <div class="sp-slides">
                    @foreach ($imgs_carousel as $img)
                        <div class="sp-slide">
                            <div class="sp-layer">
                                <img class="sp-image" src="{{ asset('storage') }}/photos/{{ $img['url'] }}"
                                    itemprop="image">
                            </div>
                        </div>
                    @endforeach
                </div>
            </div>
        </div>
    </div>
</section>
{{-- SERVICES --}}
<section class="home clearfix" id="cms-content-wrapper">
    <div class="container">
        <h3>Our Services</h3>
        <div class="section-service">
            <script type="text/javascript">
                $(document).ready(function() {
                    $(".owl_service_board").owlCarousel({
                        // loop:true,
                        margin: 30,
                        nav: true,
                        navContainerClass: 'carousel-nav-btn-gc',
                        navText: ['<i class="fa fa-angle-left"></i>',
                            '<i class="fa fa-angle-right"></i>'
                        ],
                        navClass: ['carousel-nav-left', 'carousel-nav-right'],
                        dots: false,
                        responsive: {
                            0: {
                                items: 1
                            },
                            760: {
                                items: 2
                            },
                            1000: {
                                items: 3
                            }
                        }
                    });
                });

            </script>
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="owl-carousel owl-theme owl_service_board sale-box ">
                        @foreach ($services as $service)
                            <div class="item">
                                <a href="{{ route('services-detail', ['slug' => strtolower(trim(preg_replace('/[^A-Za-z0-9-]+/', '-', $service['name']))), 'service_id' => $service['id']]) }}"
                                    class="cover"
                                    style="background-image: url('{{ asset('storage') }}/photos/{{ json_decode($service['images'])[0] }}');">
                                    <img style="display: none" class="imgrps"
                                        src="{{ asset('storage') }}/photos/{{ json_decode($service['images'])[0] }}"
                                        alt="{{ $service['name'] }}" itemprop="image">
                                </a>
                                <h3><a itemprop="url"
                                        href="{{ route('services-detail', ['slug' => strtolower(trim(preg_replace('/[^A-Za-z0-9-]+/', '-', $service['name']))), 'service_id' => $service['id']]) }}">{{ $service['name'] }}</a>
                                </h3>
                                <div class="sumarry">{{ $service['description'] }}</div>
                                <div><a class="btn btn-primary"
                                        href="{{ route('services-detail', ['slug' => strtolower(trim(preg_replace('/[^A-Za-z0-9-]+/', '-', $service['name']))), 'service_id' => $service['id']]) }}">View
                                        more</a></div>
                            </div>
                        @endforeach
                    </div>
                </div>
                <style>
                    .section-service .cover {
                        width: 100%;
                        height: auto;
                        margin: 0;
                        padding: 0;
                        padding-bottom: 100%;
                        background-repeat: no-repeat;
                        background-attachment: scroll;
                        background-clip: border-box;
                        background-origin: padding-box;
                        background-size: cover;
                        -webkit-transition: all .5s;
                        -moz-transition: all .5s;
                        -o-transition: all .5s;
                        transition: all .5s;
                        display: block;
                        background-position: center;
                    }

                    .owl-carousel {
                        position: relative;
                    }

                    .owl-carousel .carousel-nav-btn-gc {
                        top: 30%;
                        width: 100%;
                        font-size: 32px;
                    }

                    .owl-carousel .carousel-nav-btn-gc .carousel-nav-left {
                        padding: 5px 10px;
                        cursor: pointer;
                        position: absolute;
                        top: 30%;
                        left: 10px;
                    }

                    .owl-carousel .carousel-nav-btn-gc .carousel-nav-right {
                        padding: 5px 10px;
                        cursor: pointer;
                        position: absolute;
                        top: 30%;
                        right: 10px;
                    }

                    @media (max-width: 768px) {

                        .owl-carousel .carousel-nav-btn-gc .carousel-nav-left {
                            top: 25%;
                            left: 0px;
                        }

                        .owl-carousel .carousel-nav-btn-gc .carousel-nav-right {
                            top: 25%;
                            right: 0px;
                        }


                    }

                </style>
            </div>

        </div>
    </div>
</section>
{{-- GALLERY --}}
<section class="clearfix" id="cms-content-wrapper-2">
    <div class="container-druid">
        <div class="vc_row wpb_row vc_row-fluid vc_custom_1532483482042 vc_row-no-padding" data-vc-full-width="true"
            data-vc-full-width-init="false" data-vc-stretch-content="true">
            <div class="wpb_column vc_column_container vc_col-sm-12">
                <div class="vc_column-inner">
                    <div class="wpb_wrapper">
                        <div class="cms-grid-wraper cms-grid-portfolio template-cms_grid--portfolio" id="cms-grid-2">
                            <div class="cms-grid">
                                <div class="container-style">
                                    <h3>Our Gallery</h3>
                                    <!-- Custom height with class: .image-bg {padding-bottom: 75%;} -->
                                    @foreach ($lists[0] as $item)
                                        <div
                                            class="cms-grid-item col-lg-3 col-md-3 col-sm-6 col-xs-6 nopaddingall overlay-wrap">
                                            <div class="cms-grid-media has-thumbnail image-bg"
                                                style="background-image: url('{{ asset('storage') }}/photos/{{ $item['url'] }}');">

                                                <img itemprop="image" alt="Nails Design"
                                                    class="attachment-large size-large wp-post-image"
                                                    src="{{ asset('storage') }}/photos/{{ $item['url'] }}">
                                            </div>
                                            <div class="overlay text-center">
                                                <div class="overlay-content">
                                                    <a class="gallery-item fancybox" style="display: block"
                                                        data-group="gallery-1" title="Nails Design"
                                                        href="{{ asset('storage') }}/photos/{{ $item['url'] }}">
                                                        <h4 class="cms-grid-title">Nails Design</h4>
                                                        <div class="cms-grid-categories playfairdisplay">
                                                            Nails Design </div>
                                                    </a>
                                                </div>
                                            </div>
                                        </div>
                                    @endforeach
                                    <style>
                                        .has-thumbnail.image-bg img {
                                            display: none;
                                        }

                                        .has-thumbnail.image-bg {
                                            padding-bottom: 75%;
                                            background-position: center;
                                            background-repeat: no-repeat;
                                            background-size: cover;
                                        }

                                    </style>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="vc_row-full-width vc_clearfix"></div>
    </div>
</section>
