<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" dir="ltr" lang="en" xmlns:og="http://ogp.me/ns#"
    xmlns:fb="http://www.facebook.com/2008/fbml" itemscope itemtype="https://schema.org/NailSalon">

<head>
    <!-- Language -->
    <meta http-equiv="content-language" content="en" />
    <link rel="alternate" href="" hreflang="x-default">
    <link rel="alternate" href="" hreflang="en-US">

    <!-- Meta limit -->

    <!-- Meta Tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width,initial-scale=1.0" />
    <meta http-equiv="content-type" content="text/html; charset=UTF-8" />
    <meta name="description" content="" />
    <meta name="keywords" content="" />
    <meta name="author" content="" />

    <!-- OG -->
    <meta property="og:title" content="" />
    <meta property="og:description" content="" />
    <meta property="og:image" content="" />
    <meta property="og:url" content="/" />
    <meta property="og:type" content="website" />
    <meta property="og:site_name" content="Desbe Nails & Spa Fnail01j" />

    <!-- Dublin Core -->
    <link rel="schema.DC" href="http://purl.org/dc/elements/1.1/">
    <meta name="DC.title" content="">
    <meta name="DC.identifier" content="/">
    <meta name="DC.description" content="">
    <meta name="DC.subject" content="">
    <meta name="DC.language" scheme="UTF-8" content="en-us">
    <meta itemprop="priceRange" name="priceRange" content="">

    <!-- GEO meta -->
    <meta name="geo.region" content="">
    <meta name="geo.placename" content="">
    <meta name="geo.position" content="">
    <meta name="ICBM" content="">

    <!-- Page Title -->
    <title>Desbe Nails & Spa Fnail01j</title>
    <base href="/themes/fnail01j/assets/">

    <!-- canonical -->
    <link rel="canonical" href="">



    <!-- Favicons -->
    <link rel="icon" href="/uploads/fnail0iwhfupd/attach/1562752283_fbm_fvc.png" type="image/x-icon">
    <link rel="shortcut icon" href="/uploads/fnail0iwhfupd/attach/1562752283_fbm_fvc.png" type="image/x-icon">

    <!-- Css -->


    <link rel="stylesheet" type="text/css" href='{{ asset('frontend') }}/public/library/jquery-ui/jquery-ui.min.css'>
    <link rel="stylesheet" type="text/css" href='{{ asset('frontend') }}/public/library/animate/animate.css'>
    <link rel="stylesheet" type="text/css" href='{{ asset('frontend') }}/public/library/validation/validation.css'>
    <link rel="stylesheet" type="text/css" href='{{ asset('frontend') }}/public/library/flex-label/flex-label.css'>
    <link rel="stylesheet" type="text/css"
        href='{{ asset('frontend') }}/public/library/bootstrap/v3.3.7/bootstrap.min.css'>
    <link rel="stylesheet" type="text/css"
        href='{{ asset('frontend') }}/public/library/bootstrap-datetimepicker/bootstrap-datetimepicker.min.css'>
    <link rel="stylesheet" type="text/css"
        href='{{ asset('frontend') }}/public/library/font-awesome/v4.7.0/css/font-awesome.min.css'>
    <link rel="stylesheet" type="text/css"
        href='{{ asset('frontend') }}/public/library/slider-pro/slider-pro.min.css'>
    <link rel="stylesheet" type="text/css"
        href='{{ asset('frontend') }}/public/library/owl-carousel/v2.2.1/owl.carousel.min.css'>
    <link rel="stylesheet" type="text/css"
        href='{{ asset('frontend') }}/public/library/magnific-popup/magnific-popup.css'>
    <link rel="stylesheet" type="text/css" href='{{ asset('frontend') }}/public/library/pnotify/pnotify.css'>
    <link rel="stylesheet" type="text/css" href='{{ asset('frontend') }}/public/library/instagram/instafeed.min.css'>
    <link rel="stylesheet" type="text/css" href='{{ asset('frontend') }}/public/library/signature/signature.css'>
    <link rel="stylesheet" type="text/css" href='{{ asset('frontend') }}/themes/fnail01j/assets/css/meanmenu.css'>
    <link rel="stylesheet" type="text/css"
        href='{{ asset('frontend') }}/themes/fnail01j/assets/css/pe-icon-7-stroke.css'>
    <link rel="stylesheet" type="text/css"
        href='{{ asset('frontend') }}/themes/fnail01j/assets/css/ie10-viewport-bug-workaround.css'>
    <link rel="stylesheet" type="text/css" href='{{ asset('frontend') }}/themes/fnail01j/assets/css/static.css'>
    <link rel="stylesheet" type="text/css"
        href='{{ asset('frontend') }}/themes/fnail01j/assets/css/js_composer.min.css'>
    <link rel="stylesheet" type="text/css"
        href='{{ asset('frontend') }}/themes/fnail01j/assets/css/dropdown-submenu.css'>
    <link rel="stylesheet" type="text/css" href='{{ asset('frontend') }}/themes/fnail01j/assets/css/style.css'>
    <style type="text/css">
        /*===================================================================================*/
        /*  CSS COLORS
    /*===================================================================================*/
        #masthead .main-navigation>div ul:first-child>li>a:after {
            border-bottom: 2px solid free_theme_color_1;
        }

        .circle-service-image {
            border: 3px solid free_theme_color_1;
        }

        .btn-primary,
        .tnp-widget input.tnp-submit,
        .pagination>li.active>a.hover-main-color,
        .pagination>li.active>span.hover-main-color,
        .pagination>li>a:hover.hover-main-color,
        .pagination>li>span:hover.hover-main-color,
        .cms-fancy-box-single:hover .fancy-box-icon-inner {
            background: free_theme_color_1;
            color: #fff;
            border: 0px solid free_theme_color_1;
        }

        h2,
        .h2,
        h2 a,
        .h2 a,
        h3,
        .h3,
        h3 a,
        .h3 a,
        h4,
        .h4,
        h4 a,
        .h4 a,
        .detail-price-number,
        h2.service-name,
        .cms-carousel.cms-carousel-testimonial .cms-carousel-title {
            color: free_theme_color_1;
        }

        .btn-primary:hover,
        .tnp-widget input.tnp-submit:hover {
            border: 0px;
            background: free_theme_color_2;
            border-color: free_theme_color_3;
            color: free_theme_color_3;
        }

        footer#footer-wrapper #cms-footer-top>.container aside.widget ul li a:hover,
        form input:hover,
        .form input:hover,
        form input:active,
        .form input:active,
        form input:focus,
        .form input:focus,
        select:hover,
        .form-control:focus,
        .select2.select2-container:hover,
        select:active,
        .select2.select2-container:active,
        select:focus,
        .select2.select2-container:focus {
            border-color: free_theme_color_1;
            color: free_theme_color_1;
        }

        .mean-container a.meanmenu-reveal>span,
        .mean-container a.meanmenu-reveal>span:before,
        .mean-container a.meanmenu-reveal>span:after,
        input[type='submit'].btn-primary.btn {
            background: free_theme_color_1;
        }

        #wpbSlider .sp-button:hover,
        #wpbSlider .sp-button.sp-selected-button,
        .bootstrap-datetimepicker-widget table td.active,
        .bootstrap-datetimepicker-widget table td.active:hover {
            background-color: free_theme_color_1;
        }

        footer #cms-footer-bottom,
        footer #cms-footer-top,
        .btn-primary:hover {
            background-color: free_theme_color_2;
            border: 0px;
            border-color: free_theme_color_3;
            color: free_theme_color_3
        }

        html body .overlay {
            background: free_theme_color_150;
        }

        #cms-footer-top,
        #cms-footer-top a,
        #cms-footer-top aside.widget .wg-title {
            color: free_theme_color_3
        }



        .box_img_giftcard.active,
        .box_img_giftcard:hover {
            border-color: free_theme_color_1;
        }

    </style>

    <link rel="stylesheet" type="text/css" href='{{ asset('frontend') }}/public/library/global/css/news.css'>
    <link rel="stylesheet" type="text/css" href='{{ asset('frontend') }}/public/library/global/css/giftcards.css'>
    <link rel="stylesheet" type="text/css" href='{{ asset('frontend') }}/public/library/global/css/cart-payment.css'>
    <link rel="stylesheet" type="text/css" href='{{ asset('frontend') }}/public/library/global/css/consent.css'>
    <link rel="stylesheet" type="text/css"
        href='{{ asset('frontend') }}/themes/fnail01j/assets/css/custom.css?v=2.1'>

    <style type="text/css">
        .section-service.section .item h3 {
            line-height: 75px;
            height: 75px;
            overflow: hidden;
        }

        .section-service.section .item h3 a {
            font-size: 18px;
            line-height: 1;
        }

        footer #cms-footer-bottom {
            padding-top: 10px;
            padding-bottom: 10px;
        }

        div#cms-footer-bottom p {
            text-align: center;
            margin-bottom: 0px;
        }

        footer #cms-footer-bottom {
            background-color: #4b4b4b;
        }

        h3.wg-title {
            font-size: 20px;
        }

        @media only screen and (max-width: 687px) {
            .about-content h3 {
                font-size: 18px;
            }
        }

        .fancy-box-content {
            color: #000;
            font-weight: 400;
        }

        footer #cms-footer-top {
            padding-top: 20px;
        }

        .booking_staff_title {
            display: none;
        }

        .section-service.section .item h3 {
            border: solid 0.5px;
            margin-bottom: 10px;
        }

        .img-info-staff {
            display: none;
        }

        .vt-time {
            display: inline-block;
        }

        .description {
            display: none;
        }

        section#cms-content-wrapper h3 {
            text-align: center;
        }

        .cms-grid h3 {
            text-align: center;
        }

        #cms-content-wrapper {
            margin: 30px 0px;
        }

    </style>

    <!-- Go to www.addthis.com/dashboard to customize your tools -->
    <script type="text/javascript"
        src="{{ asset('frontend') }}//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-59b8fec5dd14e0f2"></script>
    <script type="text/javascript">
        var addthis_share = {
            url: "/",
            title: "Desbe Nails & Spa Fnail01j",
            description: "",
            media: ""
        }

    </script>
    <!-- Script -->

    <script type="text/javascript" src="{{ asset('frontend') }}/public/library/jquery/v1.10.2/jquery.min.js">
    </script>
    <script type="text/javascript" src="{{ asset('frontend') }}/public/library/jquery-ui/jquery-ui.min.js"></script>
    <script type="text/javascript" src="{{ asset('frontend') }}/public/library/jquery-ui/jquery-migrate.min.js">
    </script>
    <script type="text/javascript" src="{{ asset('frontend') }}/public/library/validation/validation.min.js">
    </script>
    <script type="text/javascript" src="{{ asset('frontend') }}/public/library/mask/mask.min.js"></script>
    <script type="text/javascript" src="{{ asset('frontend') }}/public/library/flex-label/flex-label.js"></script>
    <script type="text/javascript" src="{{ asset('frontend') }}/public/library/bootstrap/v3.3.7/bootstrap.min.js">
    </script>
    <script type="text/javascript"
        src="{{ asset('frontend') }}/public/library/waiting-modal-dialog/waiting-modal-dialog.js"></script>
    <script type="text/javascript" src="{{ asset('frontend') }}/public/library/moment/moment-with-locales.js">
    </script>
    <script type="text/javascript"
        src="{{ asset('frontend') }}/public/library/bootstrap-datetimepicker/bootstrap-datetimepicker.js"></script>
    <script type="text/javascript" src="{{ asset('frontend') }}/public/library/magnific-popup/magnific-popup.min.js">
    </script>
    <script type="text/javascript" src="{{ asset('frontend') }}/public/library/pnotify/pnotify.js"></script>
    <script type="text/javascript" src="{{ asset('frontend') }}/public/library/slider-pro/slider-pro.min.js">
    </script>
    <script type="text/javascript"
        src="{{ asset('frontend') }}/public/library/owl-carousel/v2.2.1/owl.carousel.min.js"></script>
    <script type="text/javascript" src="{{ asset('frontend') }}/public/library/wow/wow.min.js"></script>
    <script type="text/javascript" src="{{ asset('frontend') }}/public/library/meanmenu/meanmenu.js"></script>
    <script type="text/javascript" src="{{ asset('frontend') }}/public/library/instagram/instafeed.min.js"></script>
    <script type="text/javascript" src="{{ asset('frontend') }}/public/library/signature/signature.min.js"></script>
    <script type="text/javascript" src="{{ asset('frontend') }}/public/library/global/js/js_php.js"></script>

    <script type="text/javascript">
        let webForm = {
            "required": "(required)",
            "optional": "(optional)",
            "any_person": "Any person",
            "price": "Price",
            "morning": "Morning",
            "afternoon": "Afternoon",
            "sunday": "Sunday",
            "monday": "Monday",
            "tuesday": "Tuesday",
            "wednesday": "Wednesday",
            "thursday": "Thursday",
            "friday": "Friday",
            "saturday": "Saturday",
            "jan": "Jan",
            "feb": "Feb",
            "mar": "Mar",
            "apr": "Apr",
            "may": "May",
            "jun": "Jun",
            "jul": "Jul",
            "aug": "Aug",
            "sep": "Sep",
            "oct": "Oct",
            "nov": "Nov",
            "dec": "Dec",
            "contact_name": "Your name",
            "contact_name_placeholder": "",
            "contact_name_maxlength": "76",
            "contact_email": "Your email address",
            "contact_email_placeholder": "",
            "contact_email_maxlength": "76",
            "contact_phone": "Your phone",
            "contact_phone_placeholder": "",
            "contact_phone_maxlength": "16",
            "contact_subject": "Your subject",
            "contact_subject_placeholder": "",
            "contact_subject_maxlength": "251",
            "contact_message": "Your message",
            "contact_message_placeholder": "",
            "contact_message_maxlength": "501",
            "contact_btn_send": "Send Us",
            "contact_name_err": "Please enter your name",
            "contact_email_err": "Please enter your email",
            "contact_phone_err": "Please enter your phone",
            "contact_subject_err": "Please enter your subject",
            "contact_message_err": "Please enter your message",
            "booking_name": "Your name",
            "booking_name_placeholder": "",
            "booking_name_maxlength": "76",
            "booking_phone": "Your phone",
            "booking_phone_placeholder": "",
            "booking_phone_maxlength": "16",
            "booking_email": "Your email",
            "booking_email_placeholder": "",
            "booking_email_maxlength": "76",
            "booking_service": "Service",
            "booking_service_placeholder": "Select service",
            "booking_menu": "Menu",
            "booking_menu_placeholder": "Select menu",
            "booking_technician": "Technician",
            "booking_technician_placeholder": "Select technician",
            "booking_person_number": "Number",
            "booking_date": "Date",
            "booking_date_placeholder": "",
            "booking_date_maxlength": "16",
            "booking_hours": "Hour",
            "booking_hours_placeholder": "Select hour",
            "booking_note": "Note",
            "booking_note_maxlength": "201",
            "booking_note_placeholder": "(Max length 200 character)",
            "booking_store": "Storefront",
            "booking_store_placeholder": "Select storefront",
            "booking_add_another_service": "Add another service",
            "booking_information": "Appointment Information",
            "booking_order_information": "Order Information",
            "booking_popup_message": "Message",
            "booking_popup_confirm": "Confirm booking information ?",
            "booking_popup_confirm_description": "We will send a text message to you via the number below after we confirm the calendar for your booking.",
            "booking_order_popup_confirm": "Confirm order information ?",
            "booking_order_popup_confirm_description": "We will send a text message to you via the number below after we confirm the calendar for your order.",
            "booking_btn_send": "Send appointment now",
            "booking_btn_search": "Search",
            "booking_btn_booking": "Booking",
            "booking_btn_confirm": "Confirm",
            "booking_btn_cancel": "Cancel",
            "booking_hours_expired": "Has expired",
            "booking_name_err": "Please enter your name",
            "booking_phone_err": "Please enter your phone",
            "booking_email_err": "Please enter your email",
            "booking_service_err": "Please choose a service",
            "booking_menu_err": "Please choose a menu",
            "booking_technician_err": "Please choose a technician",
            "booking_date_err": "Please choose date",
            "booking_hours_err": "Please choose hour",
            "booking_get_hours_timeout": "Network timeout, Please click the button search to try again"
        };
        let webBooking = {
            enable: true,
            minDate: "05/24/2021",
            requiredTechnician: false,
            requiredEmail: false,
            requiredHour: true,
        };
        let webFormat = {
            dateFormat: "MM/DD/YYYY",
            datePosition: "1,0,2",
            phoneFormat: "(000) 000-0000",
        };
        let webGlobal = {
            site: "idx",
            siteAct: "",
            siteSubAct: "",
            noPhoto: "/public/library/global/no-photo.jpg",
            isTablet: false,
            isMobile: false,
            enableRecaptcha: false,
        };
        var dateFormatBooking = "MM/DD/YYYY";
        var posFormat = "1,0,2";
        var minDateBooking = '05/24/2021';
        var timeMorning = '["10:00","10:30","11:00","11:30"]';
        var timeAfternoon =
            '["12:00","12:30","13:00","13:30","14:00","14:30","15:00","15:30","16:00","16:30","17:00","17:30","18:00","18:30","19:00"]';
        var bookLogin = "open_booking";
        var beforeTime = "1";
        var beforeDay = 0;
        var currDateT = "2021/05/24";
        var checktimebooking = 1;
        var enableRecaptcha = 0;
        var hoursTimeFormat = 12;
        var phoneFormat = "(000) 000-0000";
        var company_phone = "832-968-6668";
        var num_paging = "12";
        var site = "idx";

        let facebook_embed = {
            id_fanpage: "https://www.facebook.com/FastboyMarketingAgency",
            appId: "",
            width: 450,
            height: 300,
            tabs: "timeline",
            show_facepile: true,
            small_header: true,
            likebox_enable: true,
            likebox_show_faces: false,
            likebox_stream: true,
        };
        let google_id_fanpage = "";
        let twitter_id_fanpage = "";

    </script>

    <!-- Webmaster tools -->
    <!--Webmaster tools-->
    <!-- fb retargeting -->

    <!-- Schema Script -->
    <meta name="csrf-token" content="{{ csrf_token() }}" />
</head>

<body
    class="home page-template-default page page-id-366 woocommerce-no-js ltr monaco-home columns-3 wpb-js-composer js-comp-ver-5.5.2 vc_responsive">
    <div class="cs-wide header-default no-border-home clearfix" id="cms-page">

        <!-- Facebook Root And H1 Seo -->
        <h1 style="display: none"></h1>
        <div id="fb-root" style="display: none"></div>

        <!-- Tpl freeze header -->
        <input type="hidden" name="activeFreezeHeader" value="1" />
        {{-- HEADER --}}
        @include('frontend.nail01-j.component.header',
        ['header'=>
        App\Models\Product::where('category_id',
        App\Models\Category::where('name','Header And Footer')->first()['id'])->first()])
        {{-- MAIN --}}
        {{ $slot }}
        {{-- FOOTER --}}
        @php
            $album = App\Models\Album::where([['type', 'Social'], ['status', 'PUBLISHED']])->first();
            $social = App\Models\Asset::where('albums_id', $album['id'])->orderBy('priority', 'desc')->get();
        @endphp
        @include('frontend.nail01-j.component.footer',['header'=>
        App\Models\Product::where('category_id',
        App\Models\Category::where('name','Header And Footer')->first()['id'])->first(),
        'social'=>$social]))

        <!-- external javascripts -->
        <script type="text/javascript" src="{{ asset('frontend') }}/public/library/global/js/consent.js"></script>
        <script type="text/javascript" src="{{ asset('frontend') }}/themes/fnail01j/assets/js/app.js?version=1.9">
        </script>
        <script type="text/javascript" src="{{ asset('frontend') }}/themes/fnail01j/assets/js/main.js?version=1.6">
        </script>

        <!-- Google analytics -->
        <script>
            (function(i, s, o, g, r, a, m) {
                i['GoogleAnalyticsObject'] = r;
                i[r] = i[r] || function() {
                    (i[r].q = i[r].q || []).push(arguments)
                }, i[r].l = 1 * new Date();
                a = s.createElement(o),
                    m = s.getElementsByTagName(o)[0];
                a.async = 1;
                a.src = g;
                m.parentNode.insertBefore(a, m)
            })(window, document, 'script', 'https://www.google-analytics.com/analytics.js', 'ga');
            ga('create', '', 'auto');
            ga('send', 'pageview');

        </script>

        <!-- gg adwords remarketing -->

        <!-- JS extends -->
        <script>
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

        </script>
        <script type="text/javascript"></script>
        @livewireScripts
    </div>

</body>

</html>
