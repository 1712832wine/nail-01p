@php
$features = json_decode($header['features']);
$extras = json_decode($header['extras']);
@endphp
<footer class="footer-bottom-layout-3" id="footer-wrapper">
    <div class="footer-wrapper-inner">
        <div id="cms-footer-top">
            <div class="container">
                <div class="row">
                    <div class="col-md-3">
                        <aside class="widget widget_text" id="text-2">
                            <h3 class="wg-title">Fast Boy Marketing</h3>
                            <p>{{ $features[0]->desc }}</p>
                            <p><a href="tel:{{ $features[1]->desc }}">{{ $features[1]->desc }}</a></p>
                            <p><a href="mailto:{{ $features[2]->desc }}">{{ $features[2]->desc }}</a></p>
                        </aside>
                    </div>
                    <div class="col-md-3">
                        <aside class="widget widget_text" id="text-3">
                            <h3 class="wg-title">MENU</h3>
                            <div class="textwidget">
                                <ul>
                                    <li itemprop="name"><a href="/about." itemprop="url">About Us</a></li>
                                    <li itemprop="name"><a href="/services." itemprop="url">Services</a></li>
                                    <li itemprop="name"><a itemprop="url" href="/book.">Booking</a></li>
                                    <li itemprop="name"><a href="/coupons." itemprop="url">Coupons</a></li>
                                    <li itemprop="name"><a href="/giftcards." itemprop="url">Gift Cards</a>
                                    </li>
                                    <li itemprop="name"><a href="/gallery." itemprop="url">Gallery</a></li>
                                    <li itemprop="name"><a href="/contact." itemprop="url">Contact Us</a>
                                    </li>
                                </ul>
                            </div>
                        </aside>
                    </div>
                    <div class="col-md-3">
                        <aside class="widget widget_text">
                            <h3 class="wg-title">BUSINESS HOURS</h3>
                            <div class="textwidget">
                                <div class="vt-time">
                                    @foreach ($extras as $extra)
                                        <div class="col-2" itemprop="openingHours" content="Mon 10:00 am - 7:00 pm">
                                            <div class="col-lef">{{ $extra->name }}</div>
                                            <div class="col-right">{{ $extra->desc }}</div>
                                        </div>
                                    @endforeach
                                </div>
                            </div>
                        </aside>
                    </div>
                    <div class="col-md-3">
                        <aside class="widget widget_text">
                            <h3 class="wg-title">SOCIAL</h3>
                            <div class="cms-social">
                                @foreach ($social as $item)
                                    @if ($item['url'])
                                        <a itemprop="url" class="social-img-icons" rel="nofollow" target="_blank"
                                            title="{{ $item['title'] }}" href="{{ $item['content'] }}"><img
                                                src="{{ asset('storage') }}/photos/{{ $item['url'] }}"
                                                alt="{{ $item['title'] }}"></a>
                                    @endif
                                @endforeach
                            </div>
                        </aside>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="layout-3" id="cms-footer-bottom">
        <div class="container">
            <div class="row">
                <div class="footer-copyright footer-bottom-2">
                    <div class="cms-copyright">
                        <p>© Copyright by Fast Boy Marketing. All Rights Reserved.</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</footer>
<!-- Tpl freeze footer -->
<!-- Active freeze footer by delete style display: none -->
<div class="freeze-footer">
    <ul>
        <li><a href="tel:{{ $features[1]->desc }}" class="btn btn-default btn_call_now btn-call"
                title="Call us">{{ $features[1]->desc }}</a>
        </li>
        <li><a href="/book." class="btn btn-default btn_make_appointment" title="Booking">Booking</a></li>
    </ul>
</div>
<!-- popup -->
