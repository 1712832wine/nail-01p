<section class="clearfix" id="cms-header-wrapper">
    <header class="site-header header-default no-border-home clearfix" id="masthead" role="banner">
        <div class="cms-header header-default has-sticky clearfix" id="cms-header">
            <div class="wrap-freeze-header clearfix hidden-xs hidden-sm">
                <div class="flag-freeze-header">
                    <div class="container">
                        <div class="main-navigation pull-left hidden-sm hidden-xs" id="cms-header-logo">
                            @if (count(json_decode($header['images'])) > 0)
                                <a itemprop="url" href="/">
                                    <img class="imgrps"
                                        src="{{ asset('storage') }}/photos/{{ json_decode($header['images'])[0] }}"
                                        alt="{{ $header['title'] }}" itemprop="logo image">
                                </a>
                            @endif
                        </div>
                        <div class="cms-header-navigation" id="cms-header-navigation">
                            <nav class="main-navigation clearfix" id="site-navigation" role="navigation">
                                <div class="cms-menu pull-right">
                                    <div class="menu-main-menu-container">
                                        <ul class="nav-menu" id="menu-main-menu">
                                            <li itemprop="name"><a href="/" itemprop="url">Home</a></li>
                                            <li itemprop="name"><a href="/about" itemprop="url">About
                                                    Us</a></li>
                                            <li itemprop="name"><a href="/services" itemprop="url">Services</a></li>
                                            <li itemprop="name"><a itemprop="url" href="/book">Booking</a>
                                            </li>
                                            <li itemprop="name"><a href="/coupons" itemprop="url">Coupons</a></li>
                                            <li itemprop="name"><a href="/giftcards" itemprop="url">Gift
                                                    Cards</a></li>
                                            <li itemprop="name"><a href="/gallery" itemprop="url">Gallery</a></li>
                                            <li itemprop="name"><a href="/contact" itemprop="url">Contact
                                                    Us</a></li>
                                        </ul>
                                    </div>
                                </div>
                            </nav>
                        </div>
                    </div>
                </div>
            </div>

            <div class="wrap-freeze-header-mobile clearfix hidden-md hidden-lg menu-1024-hidden">
                <div class="flag-freeze-header-mobile">
                    <div class="container">
                        <div class="row mobile-menu-wrap hidden-md hidden-lg">
                            <div class="col-md-12">
                                <div class="menu_mobile_v1 hidden-md hidden-lg menu-1024-hidden">
                                    <div class="mobile_menu_container_v1">
                                        <div class="mobile_logo logo">

                                            @if (count(json_decode($header['images'])) > 0)
                                                <a itemprop="url" href="/">
                                                    <img class="imgrps"
                                                        src="{{ asset('storage') }}/photos/{{ json_decode($header['images'])[0] }}"
                                                        alt="{{ $header['title'] }}" itemprop="logo image">
                                                </a>
                                            @endif
                                        </div>
                                        <div class="mobile-menu clearfix">
                                            <nav id="mobile_dropdown">
                                                <ul>
                                                    <li itemprop="name"><a href="/" itemprop="url">Home</a></li>
                                                    <li itemprop="name"><a href="/about" itemprop="url">About
                                                            Us</a></li>
                                                    <li itemprop="name"><a href="/services" itemprop="url">Services</a>
                                                    </li>
                                                    <li itemprop="name"><a itemprop="url" href="/book">Booking</a>
                                                    </li>
                                                    <li itemprop="name"><a href="/coupons" itemprop="url">Coupons</a>
                                                    </li>
                                                    <li itemprop="name"><a href="/giftcards" itemprop="url">Gift
                                                            Cards</a></li>
                                                    <li itemprop="name"><a href="/gallery" itemprop="url">Gallery</a>
                                                    </li>
                                                    <li itemprop="name"><a href="/contact" itemprop="url">Contact
                                                            Us</a></li>
                                                </ul>
                                            </nav>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </header>
</section>
