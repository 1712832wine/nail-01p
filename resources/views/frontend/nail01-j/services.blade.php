<div class="no-container">
    <div class="page-title" id="page-title">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 text-center" id="page-title-text">
                    <h1>Service</h1>
                </div>
            </div>
        </div>
    </div>
</div>
<input type="hidden" name="group_id" value="{{ $service_id }}" />
<div class="container">
    <section class="in-container">
        <div class="service-container about-page">
            <div class="row">
                <div class="col-sm-6"></div>
                <div class="col-sm-6 top-right btn_service_defale" style="padding-bottom: 20px;"></div>
            </div>
            <div class="animation_sroll_jumpto">
                <div class="sroll_jumpto">
                    @foreach ($services as $index => $service)
                        @if ($index % 2 === 0)
                            <div class="row clearfix" id="sci_{{ $service['id'] }}">
                                <div class="col-sm-4 col-md-4  right service-image-wrap">
                                    <div class="service-image circle">
                                        @if (count(json_decode($service['images'])) > 0)
                                            <div class="service-image-bg"
                                                style="background-image: url('{{ asset('storage') }}/photos/{{ json_decode($service['images'])[0] }}')">
                                                <img class="img-responsive"
                                                    src="{{ asset('storage') }}/photos/{{ json_decode($service['images'])[0] }}"
                                                    alt="{{ $service['name'] }}">
                                            </div>
                                        @endif
                                    </div>
                                </div>
                                <div class="col-sm-8 col-md-8 ">
                                    <div class="clearfix service-list">
                                        <h2 class="service-name">{{ $service['name'] }}</h2>
                                        <div class="service-desc">{{ $service['description'] }}</div>
                                        @foreach (json_decode($service['features']) as $item)
                                            @switch($item->name)
                                                @case('desc')
                                                    <p>{{ $item->desc }}</p>
                                                @break
                                                @case('center')
                                                @break
                                                @default
                                                    <div class="detail-price-item">
                                                        <span class="detail-price-name">{{ $item->name }}</span>
                                                        <span class="detail-price-dots"></span>
                                                        <span class="detail-price-number">
                                                            <span class="current">{{ $item->desc }}</span>
                                                        </span>
                                                    </div>
                                            @endswitch
                                        @endforeach
                                    </div>
                                </div>
                            </div>
                        @else
                            <div class="row clearfix" id="sci_{{ $service['id'] }}">
                                <div class="service-line"></div>
                                <div class="col-sm-4 col-md-4 col-md-push-8 col-sm-push-8 left service-image-wrap">
                                    <div class="service-image circle">
                                        @if (count(json_decode($service['images'])) > 0)
                                            <div class="service-image-bg"
                                                style="background-image: url('{{ asset('storage') }}/photos/{{ json_decode($service['images'])[0] }}')">
                                                <img class="img-responsive"
                                                    src="{{ asset('storage') }}/photos/{{ json_decode($service['images'])[0] }}"
                                                    alt="{{ $service['name'] }}">
                                            </div>
                                        @endif
                                    </div>
                                </div>
                                <div class="col-sm-8 col-md-8 col-md-pull-4 col-sm-pull-4">
                                    <div class="clearfix service-list">
                                        <h2 class="service-name">{{ $service['name'] }}</h2>
                                        <div class="service-desc">{{ $service['description'] }}
                                        </div>
                                        @foreach (json_decode($service['features']) as $item)
                                            @switch($item->name)
                                                @case('desc')
                                                    <p>{{ $item['desc'] }}</p>
                                                @break
                                                @case('center')
                                                @break
                                                @default
                                                    <div class="detail-price-item">
                                                        <span class="detail-price-name">{{ $item->name }}</span>
                                                        <span class="detail-price-dots"></span>
                                                        <span class="detail-price-number">
                                                            <span class="current">{{ $item->desc }}</span>
                                                        </span>
                                                    </div>
                                            @endswitch
                                        @endforeach
                                    </div>
                                </div>
                            </div>
                        @endif
                    @endforeach
                </div>
            </div>
        </div>
    </section>
</div>
