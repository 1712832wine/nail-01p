<div class="no-container">
    <div class="page-title" id="page-title">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 text-center" id="page-title-text">
                    <h1>Gallery</h1>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="container">
    <div class="in-container">
        <div class="gallery-style-1 bg-fa gallery-inpage clearfix ">
            <ul class="clearfix m-category-tab" id="category_tab">
                @foreach ($albums as $index => $album)
                    <li class="tab @if ($index===0) active @endif"
                        data-id="{{ $album['id'] }}">
                        <span itemprop="name">{{ $album['name'] }}</span>
                    </li>
                @endforeach
            </ul>
            <div class="clearfix m-gallery-content" id="gallery_content">
                <div class="clearfix m-gallery-listing listing"></div>
                <div class="clearfix m-gallery-paging paging"></div>
            </div>
        </div>
    </div>
</div>
