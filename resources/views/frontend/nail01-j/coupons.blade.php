<div class="no-container">
    <div class="page-title" id="page-title">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 text-center" id="page-title-text">
                    <h1>Coupons</h1>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="container">
    <div class="in-container about-page">
        @foreach ($list as $item)
            <div class="col-sm-6 col-md-6 cards-item ">
                <a class="coupon_img_v1 pointer  image-magnific-popup" data-group=""
                    href="{{ asset('storage') }}/photos/{{ $item['url'] }}" title="Coupon">
                    <img class="cards-item-image img-responsive"
                        src="{{ asset('storage') }}/photos/{{ $item['url'] }}" alt="Coupon">
                </a>
            </div>
        @endforeach
    </div>
</div>
