<div class="no-container">
    <div class="page-title" id="page-title">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 text-center" id="page-title-text">
                    <h1>About Us</h1>
                </div>
            </div>
        </div>
    </div><!-- #page-title -->
</div>
<div class="container">
    <section class="in-container">
        <div class="section-about about-page">
            @foreach ($articles as $item)
                <div class="row">
                    <div class="col-md-6 col-xs-12">
                        <div class="about-content">
                            <h3>{{ $item['title'] }}</h3>
                            {!! $item['content'] !!}
                        </div>
                    </div>
                    <div class="col-md-6 col-xs-12">
                        <div class="row">
                            <div class="youtube-player">
                                @if (count(json_decode($item['image'])) > 0)
                                    <img src="{{ asset('storage') }}/photos/{{ json_decode($item['image'])[0] }}"
                                        alt="" />
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
            @endforeach
        </div>
    </section>
</div>
