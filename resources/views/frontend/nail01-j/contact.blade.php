@php
$features = json_decode($header['features']);
$extras = json_decode($header['extras']);
@endphp
<div class="no-container">
    <div class="page-title" id="page-title">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 text-center" id="page-title-text">
                    <h1>Contact Us</h1>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="container">
    <div class="in-container">
        <div class="row">
            <div class="col-sm-6">
                <div class="contact-form">
                    <h3 itemprop="name">Send Message</h3>
                    <section class="contact_us_v1">
                        <div class="box-msg box-msg-" style=""></div>
                        <script src='https://www.google.com/recaptcha/api.js?hl=en' async defer></script>
                        <!-- Google reCaptcha -->
                        <script type="text/javascript">
                            <!--
                            function ezyCaptcha_send_contact(token, is_submit) {
                                is_submit = 0;
                                if ($("#password").length) {
                                    //$("input:password").val(md5(clean_input($("#password").val())));
                                }
                                return true;
                            }
                            //

                            -->
                        </script>
                        <div class="row">
                            <form class="wpcf7-form" enctype="multipart/form-data" method="post" name="send_contact"
                                id="send_contact" action="{{ route('send_contact') }}">
                                @csrf
                                <div class="col-md-6 col-xs-12">
                                    <label>Your Name</label>
                                    <div class="form-group">
                                        <input type="text" data-validation="[NOTEMPTY]"
                                            data-validation-message="Please enter your name!" autocomplete="off"
                                            class="form-control" placeholder="Your Name" name="contactname">
                                    </div>
                                </div>
                                <div class="col-md-6 col-xs-12">
                                    <label>Your Email</label>
                                    <div class="form-group">
                                        <input type="text" data-validation="[EMAIL]"
                                            data-validation-message="Invalid email" autocomplete="off"
                                            class="form-control" placeholder="Your Email" name="contactemail">
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <label>Your Subject</label>
                                    <div class="form-group">
                                        <input type="text" data-validation="[NOTEMPTY]"
                                            data-validation-message="Please enter a subject!" autocomplete="off"
                                            class="form-control" placeholder="Your Subject" name="contactsubject">
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <label>Your Message</label>
                                    <div class="form-group">
                                        <textarea rows="5" data-validation="[NOTEMPTY]"
                                            data-validation-message="Please enter a content!" autocomplete="off"
                                            class="form-control" placeholder="Your Message"
                                            name="contactcontent"></textarea>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group">
                                        @if (session('status'))
                                            <div class="alert alert-success" role="alert">
                                                <button type="button" class="close" data-dismiss="alert">×</button>
                                                {{ session('status') }}
                                            </div>
                                        @elseif(session('failed'))
                                            <div class="alert alert-danger" role="alert">
                                                <button type="button" class="close" data-dismiss="alert">×</button>
                                                {{ session('failed') }}
                                            </div>
                                        @endif
                                    </div>
                                </div>

                                <div class="col-md-12">
                                    <div class="form-group">
                                        <button type="submit" class="btn btn-primary btn_contact "> Send message
                                        </button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </section>
                </div><!-- .contact-form -->
            </div>
            <div class="col-sm-6">
                <div class="contact-map">
                    <h3 itemprop="name">Map:</h3>
                    <section class="box_map clearfix">
                        <div class="google-map-wrapper">
                            <div class="google-map" style="width:100%; height:auto;"><iframe
                                    src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3464.7391505606774!2d-95.57299108550652!3d29.727314340440024!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x8640ddc6ed681049%3A0x74ace03ed50728e8!2sFast+Boy+Marketing!5e0!3m2!1sen!2s!4v1510909605272"
                                    width="100%" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
                            </div>
                        </div>
                    </section>
                </div><!-- .contact-form -->
            </div>
        </div>
    </div><!-- .in-container -->
    <div class="vc_row wpb_row vc_row-fluid vc_custom_1434618310212">
        <div class="wpb_column vc_column_container vc_col-sm-4">
            <div class="vc_column-inner">
                <div class="wpb_wrapper">
                    <div class="cms-fancyboxes-wraper cms-fancy-box-single template-cms_fancybox_single content-align-default clearfix"
                        id="cms-fancy-box-single">
                        <div class="cms-fancyboxes-body">
                            <div class="cms-fancybox-item">
                                <div class="fancy-box-icon contact-fancy-box-icon pull-left">
                                    <div class="fancy-box-icon-inner"><i class="pe-7s-map-marker"></i></div>
                                </div>
                                <div class="fancy-box-content-wrap has-icon-image">
                                    <h4>OUR LOCATION</h4>
                                    <div class="fancy-box-content">
                                        <p><a href="/">{{ $features[0]->desc }}</a></p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="wpb_column vc_column_container vc_col-sm-4">
            <div class="vc_column-inner">
                <div class="wpb_wrapper">
                    <div class="cms-fancyboxes-wraper cms-fancy-box-single template-cms_fancybox_single content-align-default clearfix"
                        id="cms-fancy-box-single-2">
                        <div class="cms-fancyboxes-body">
                            <div class="cms-fancybox-item">
                                <div class="fancy-box-icon contact-fancy-box-icon pull-left">
                                    <div class="fancy-box-icon-inner"><i class="pe-7s-phone"></i></div>
                                </div>
                                <div class="fancy-box-content-wrap has-icon-image">
                                    <h4>CALL US</h4>
                                    <div class="fancy-box-content">
                                        <p><a href="tel:{{ $features[1]->desc }}">{{ $features[1]->desc }}</a></p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="wpb_column vc_column_container vc_col-sm-4">
            <div class="vc_column-inner">
                <div class="wpb_wrapper">
                    <div class="cms-fancyboxes-wraper cms-fancy-box-single template-cms_fancybox_single content-align-default clearfix"
                        id="cms-fancy-box-single-3">
                        <div class="cms-fancyboxes-body">
                            <div class="cms-fancybox-item">
                                <div class="fancy-box-icon contact-fancy-box-icon pull-left">
                                    <div class="fancy-box-icon-inner"><i class="pe-7s-mail-open-file"></i></div>
                                </div>
                                <div class="fancy-box-content-wrap has-icon-image">
                                    <h4>E-MAIL US</h4>
                                    <div class="fancy-box-content">
                                        <p><a href="mailto:{{ $features[2]->desc }}">{{ $features[2]->desc }}</a>
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
