<main class="main">

    <div class="main-wrap">
        {{-- CAROUSEL --}}
        <div class="section-slider-wrap">
            <section class="section-slider">
                @if (count($imgs_carousel) > 0)
                    <div class="slider-width-height"
                        style="display: inline-block;width: 100%;height: 1px;overflow: hidden;">
                        <div class="fixed" style="width: 100%"></div>
                        <img src="{{ asset('storage') }}/photos/{{ $imgs_carousel[0]['url'] }}"
                            style="width: 100%; height: auto" alt="" />
                    </div>
                @endif
                <div class="slider-pro" id="my-slider" style="display: none">
                    <div class="sp-slides">
                        @foreach ($imgs_carousel as $img)
                            <div class="sp-slide">
                                <div class="sp-layer sp-static">
                                    <a href="{{ route('home') }}" target="_self" title="slide7.png">
                                        <img class="sp-image" src="{{ asset('storage') }}/photos/{{ $img['url'] }}"
                                            alt="slide7.png" />
                                    </a>
                                </div>
                            </div>
                        @endforeach
                    </div>
                </div>
                <div class="slider-pro" id="my-slider-fixed-height" style="display: none">
                    <div class="sp-slides">
                        @foreach ($imgs_carousel as $img)
                            <div class="sp-slide">
                                <a href="{{ route('home') }}" target="_self" title="slide7.png">
                                    <div class="sp-layer sp-static" data-width="100%" data-height="100%" style="
                                        width: 100%;
                                        height: 100%;
                                        background: url('{{ asset('storage') }}/photos/{{ $img['url'] }}')
                                        center center no-repeat;
                                        background-size: cover;"></div>
                                </a>
                            </div>
                        @endforeach
                    </div>
                </div>
            </section>
        </div>
        {{-- OUR SERVICES --}}
        <section class="section section-service section-padding">
            <div class="container">
                <div class="row">
                    <div class="col-lg-8 offset-lg-2 col-12">
                        <div class="section-header text-center">
                            <h3 class="section-title">Our Service</h3>
                            <p class="section-subtext">
                                Professional Nail Care for Ladies and Gentleman
                            </p>
                        </div>
                    </div>
                </div>
                <div class="service-list">
                    <div class="service-items artists">
                        <div class="row">
                            @foreach ($services as $service)
                                <div class="break-row break-row-1"></div>
                                <div class="col-lg-4 col-md-6 col-sm-6">
                                    <a itemprop="url"
                                        href="{{ route('services-detail', ['slug' => strtolower(trim(preg_replace('/[^A-Za-z0-9-]+/', '-', $service['name']))), 'service_id' => $service['id']]) }}"
                                        title="{{ $service['name'] }}">
                                        <div class="service-item artist">
                                            <div class="service-i-img artist-avatar">
                                                <div class="service-i-container">
                                                    <img itemprop="image"
                                                        src="{{ asset('storage') }}/photos/{{ json_decode($service['images'])[0] }}"
                                                        class="imgrps" alt="{{ $service['name'] }}" />
                                                    <h4 class="service-i-name artist-name" itemprop="text">
                                                        {{ $service['name'] }}
                                                    </h4>
                                                </div>
                                            </div>
                                        </div>
                                    </a>
                                </div>
                            @endforeach
                        </div>
                    </div>
                </div>
            </div>
        </section>
        {{-- HOME ARTICLE --}}
        <section class="
            section section-about section-about-home
            about-singer-section
            text-white">
            <div class="section-about-home-bg"></div>
            <div class="section-about-home-bg2"
                style="background-image: url('{{ asset('frontend') }}/public/library/global/nail-images/bg-about.jpg');">
            </div>
            <div class="container">
                <div class="row">
                    <div class="col-md-6"></div>
                    <div class="col-md-6">
                        <div class="about-singer-content">
                            <h3 class="about-singer-title">
                                {{ $home_article['title'] }}
                            </h3>
                            {!! $home_article['content'] !!}
                        </div>
                    </div>
                </div>
            </div>
        </section>
        {{-- GALLERY --}}
        <section class="section section-gallery section-padding">
            <div class="container">
                <div class="row">
                    <div class="col-lg-8 offset-lg-2 col-12">
                        <div class="section-header text-center">
                            <h3 class="section-title">Our Gallery</h3>
                            <p class="section-subtext">
                                {{ $gallery['short_desc'] }}
                            </p>
                        </div>
                    </div>
                </div>
                <div class="gallery-list">
                    <div class="m-gallery-box-wrap">
                        <div class="row">
                            @foreach ($gallery_list as $item)
                                <div class="col-6 col-sm-4 col-md-3">
                                    <div class="pointer m-magnific-popup" data-group="gallery-4" title="Nails Design"
                                        href="{{ asset('storage') }}/photos/{{ $item['url'] }}">
                                        <div class="m-gallery-box">
                                            <div class="m-image-bg"
                                                style="background-image: url('{{ asset('storage') }}/photos/{{ $item['url'] }}');">
                                                <img itemprop="image"
                                                    src="{{ asset('storage') }}/photos/{{ $item['url'] }}"
                                                    alt="Nails Design" />
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            @endforeach
                        </div>
                    </div>
                </div>
            </div>
        </section>
        {{-- LOCATION & WORKING HOUR --}}
        <section class="section section-location section-padding latest-album-section text-white">
            <div class="container">
                @php
                    $features = json_decode($header['features']);
                    $extras = json_decode($header['extras']);
                @endphp
                <div class="row">
                    <div class="col-md-4">
                        <div class="section-header left-style">
                            <h3 class="section-title">Location</h3>
                            <div>
                                <div class="fci-wrap">
                                    @if (count($features) > 0)
                                        <div class="fci-row">
                                            <span class="fci-title"><i
                                                    class="fa {{ $features[0]->name }} icons-address"></i></span>
                                            <span class="fci-content" itemprop="address" itemscope=""
                                                itemtype="http://schema.org/PostalAddress">
                                                <span itemprop="streetAddress"
                                                    class="address">{{ $features[0]->desc }}</span>
                                            </span>
                                        </div>
                                    @endif
                                    @if (count($features) > 1)
                                        <div class="fci-row">
                                            <span class="fci-title"><i
                                                    class="fa {{ $features[1]->name }} icons-email"></i></span>
                                            <span class="fci-content">
                                                <a href="tel:{{ $features[1]->desc }}" title="Call Us">
                                                    <span itemprop="email"
                                                        class="email">{{ $features[1]->desc }}</span>
                                                </a>
                                            </span>
                                        </div>
                                    @endif
                                    @if (count($features) > 2)
                                        <div class="fci-row">
                                            <span class="fci-title"><i
                                                    class="fa {{ $features[2]->name }} icons-email"></i></span>
                                            <span class="fci-content">
                                                <a href="mailto:{{ $features[2]->desc }}" title="Mail Us">
                                                    <span itemprop="email"
                                                        class="email">{{ $features[2]->desc }}</span>
                                                </a>
                                                <br />
                                                <a href="mailto:" title="Mail Us">
                                                    <span itemprop="email" class="email"></span>
                                                </a>
                                            </span>
                                        </div>
                                    @endif


                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="section-header left-style">
                            <h3 class="section-title">Open Hours</h3>
                            <div>
                                <div class="foh-wrap">
                                    @foreach ($extras as $extra)
                                        <div class="foh-row short" itemprop="openingHours"
                                            content="{{ $extra->desc }}">
                                            <span class="foh-date">{{ $extra->name }}</span>
                                            <span class="foh-time">{{ $extra->desc }}</span>
                                        </div>
                                    @endforeach
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="section-header left-style">
                            <h3 class="section-title">Follow Us</h3>
                            <div>
                                <!-- Start Single Footer Widget -->
                                <div class="social-fanpage clearfix">
                                    <aside>
                                        <!-- facebook fanpage -->
                                        <div id="fanpage_fb_container"><iframe style="overflow:hidden;max-height:233px"
                                                title="Social fanpage"
                                                src="https://www.facebook.com/plugins/likebox.php?&amp;width=350&amp;height=350&amp;show_faces=false&amp;stream=true&amp;header=false&amp;href=https%3A%2F%2Fwww.facebook.com%2FFastboyMarketingAgency"
                                                width="350" height="233" scrolling="no" frameborder="0"
                                                allowtransparency="true"></iframe></div>
                                    </aside>
                                    <!-- use for calculator width -->
                                    <div id="social_block_width" class="clearfix"
                                        style="width:100% !important; height: 1px !important"></div>
                                </div><!-- End Single Footer Widget -->
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- Tpl testimonial board -->
        <!--# Support render.google_maps.layouts-->
        {{-- background-image: url(/public/library/global/nail-images/bg-nail.jpg); --}}
        <div class="section section-testimonial section-padding" style="">
            <div class="container">
                <div class="row">
                    <div class="col-md-8">
                        <div class="testimonial-content">
                            <div class="testimonial-heading section-header text-center">
                                <h3 class="section-title">Our Testimonial</h3>
                            </div>
                            <div class="items-testimonial testimonial-carousel owl-carousel">
                                @foreach ($carousel_customers as $item)
                                    <div class="item">
                                        <div class="single-testimonial-item">
                                            <div class="testimonial-content">
                                                {!! $item['content'] !!}
                                                <h4 class="name" itemprop="name">{{ $item['title'] }}</h4>
                                            </div>
                                            @if (count(json_decode($item['image'])) > 0)
                                                <div class="testimonial-img">
                                                    <img itemprop="image"
                                                        src="{{ asset('storage') }}/photos/{{ json_decode($item['image'])[0] }}"
                                                        alt="testimonial-1.jpg" />
                                                </div>
                                            @endif
                                        </div>
                                    </div>
                                @endforeach
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="home-map">
                            <div class="google-maps" id="google-map">
                                <iframe
                                    src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3464.7391505606774!2d-95.57299108550652!3d29.727314340440024!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x8640ddc6ed681049%3A0x74ace03ed50728e8!2sFast+Boy+Marketing!5e0!3m2!1sen!2s!4v1510909605272"
                                    width="100%" height="450" frameborder="0" style="border: 0"
                                    allowfullscreen></iframe>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</main>
