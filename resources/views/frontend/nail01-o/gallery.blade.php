<main class="main">
    <div class="page-header">
        <div class="container text-center">
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb text-center">
                    <li class="breadcrumb-item"><a href="{{ route('home') }}" title="Home">Home</a></li>
                    <li class="breadcrumb-item active">Gallery</li>
                </ol>
            </nav>
            <h2 class="page-title">Special Nails & Spa</h2>
        </div>
    </div>
    <section class="section section-gallery section-padding">
        <div class="container">
            <ul class="clearfix m-category-tab" id="category_tab">
                @foreach ($albums as $index => $album)
                    <li class="tab @if ($index===0) active @endif"
                        data-id="{{ $album['id'] }}">
                        <span itemprop="name">{{ $album['name'] }}</span>
                    </li>
                @endforeach
            </ul>
            <div class="clearfix m-gallery-content" id="gallery_content" data-isNewTemplate="true">
                <div class="clearfix m-gallery-listing listing"></div>
                <div class="clearfix m-gallery-paging paging"></div>
            </div>
        </div>
    </section>
</main>
