<main class="main">

    <div class="page-header">
        <div class="container text-center">
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb text-center">
                    <li class="breadcrumb-item"><a href="/" title="Home">Home</a></li>
                    <li class="breadcrumb-item active">Coupons</li>
                </ol>
            </nav>
            <h2 class="page-title">Special Nails & Spa</h2>
        </div>
    </div>
    <section class="section section-coupon section-padding">
        <div class="container">
            <div class="row">
                @foreach ($list as $item)
                    <div class="col-xs-12 col-sm-6 col-md-6">
                        <div class="pointer m-magnific-popup" data-group="coupon" title="Special Promotions"
                            href="{{ asset('storage') }}/photos/{{ $item['url'] }}">
                            <div class="m-coupon-box">
                                <img itemprop="image" src="{{ asset('storage') }}/photos/{{ $item['url'] }}"
                                    alt="Special Promotions" />
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>
            <div class="row">
                <div class="col-md-12 text-center">
                    <nav aria-label="Page navigation"></nav>
                </div>
            </div>
        </div>
    </section>
</main>
