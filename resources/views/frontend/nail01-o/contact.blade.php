@php
$features = json_decode($header['features']);
$extras = json_decode($header['extras']);
$name = explode(',', $item->name);
$desc = explode(',', $item->desc);
@endphp
<main class="main">
    <div class="page-header">
        <div class="container text-center">
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb text-center">
                    <li class="breadcrumb-item"><a href="{{ route('home') }}" title="Home">Home</a></li>
                    <li class="breadcrumb-item active">Contact Us</li>
                </ol>
            </nav>
            <h2 class="page-title">Special Nails & Spa</h2>
        </div>
    </div>
    <section class="section section-contact">
        <div class="section section-contact-info">
            <div class="fluid-container">
                @if (count($features) > 0)
                    <div class="fluid-col-1 contact-info-item">
                        <h3 class=<i class="fa {{ $features[0]->name }}"></i> Location
                        </h3>

                        <p class="contact-info">
                            <span itemprop="address" itemscope="" itemtype="http://schema.org/PostalAddress">
                                <span itemprop="streetAddress" class="address">{{ $features[0]->desc }}</span>
                            </span>
                        </p>
                    </div>"contact-title">

                @endif

                <div class="fluid-col-1 contact-info-item">
                    @if (count($features) > 2)
                        <h3 class="contact-title"><i class="fa {{ $features[2]->name }}"></i> Phone</h3>
                        <h5 class="contact-person">
                            <a href="tel:832-968-6668" title="Call Us">
                                <span itemprop="telephone" class="phone">{{ $features[2]->desc }}</span>
                            </a>
                        </h5>
                        <h5 class="contact-person">
                            <a href="tel:" title="Call Us">
                                <span itemprop="telephone" class="phone"></span>
                            </a>
                        </h5>
                        <div><br /><br /></div>
                    @endif
                    @if (count($features) > 1)
                        <h3 class="contact-title">
                            <i class="fa {{ $features[1]->name }}"></i> Email
                        </h3>
                        <p class="contact-info">
                            <a href="mailto:{{ $features[1]->desc }}" title="Mail Us">
                                <span itemprop="email" class="email">{{ $features[1]->desc }}</span>
                            </a>
                        </p>
                    @endif
                </div>
                <div class="fluid-col-1 contact-info-item">

                    <h3 class="contact-title">
                        <i class="fa fa-calendar"></i> Business Hours
                    </h3>
                    <div>
                        <div class="foh-wrap">
                            @foreach ($extras as $extra)
                                <div class="foh-row short" itemprop="openingHours" content="{{ $extra->desc }}">
                                    <span class="foh-date">{{ $extra->name }}</span>
                                    <span class="foh-time">{{ $extra->desc }}</span>
                                </div>
                            @endforeach
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="section section-maps">
            <div class="google-maps" id="google-map">
                <iframe
                    src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3464.7391505606774!2d-95.57299108550652!3d29.727314340440024!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x8640ddc6ed681049%3A0x74ace03ed50728e8!2sFast+Boy+Marketing!5e0!3m2!1sen!2s!4v1510909605272"
                    width="100%" height="450" frameborder="0" style="border: 0" allowfullscreen></iframe>
            </div>
        </div>
        <div class="section section-contact-form section-padding">
            <div class="container">
                <div class="section-header text-center">
                    <h3 class="section-title">Contact Us</h3>
                    <p class="section-subtext"></p>
                </div>
                <div class="">
                    <form method="post" name="send_contact" id="send_contact" action="{{ route('send_contact') }}"
                        class="form-horizontal">
                        @csrf
                        <div class="row">
                            <div class="col-md-6">
                                <div class="group-select">
                                    <label>Your name (required)</label>
                                    <div class="relative w100">
                                        <input title="Your name" type="text" name="contactname"
                                            data-validation="[NOTEMPTY]"
                                            data-validation-message="Please enter your name" class="form-control"
                                            autocomplete="off" placeholder="" maxlength="76" />
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="group-select">
                                    <label>Your email address (required)</label>
                                    <div class="relative w100">
                                        <input title="Your email address" type="text" name="contactemail"
                                            data-validation="[EMAIL]" data-validation-message="Please enter your email"
                                            class="form-control" autocomplete="off" placeholder="" maxlength="76" />
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="group-select">
                            <label>Your subject (required)</label>
                            <div class="relative w100">
                                <input title="Your subject" type="text" name="contactsubject"
                                    data-validation="[NOTEMPTY]" data-validation-message="Please enter your subject"
                                    class="form-control" autocomplete="off" placeholder="" maxlength="251" />
                            </div>
                        </div>
                        <div class="group-select">
                            <label>Your message (required)</label>
                            <div class="relative w100">
                                <textarea title="Your message" name="contactcontent" data-validation="[NOTEMPTY]"
                                    data-validation-message="Please enter your message" class="form-control"
                                    autocomplete="off" rows="10" placeholder="" maxlength="501"></textarea>
                            </div>
                        </div>
                        @if (session('status'))
                            <div class="alert alert-success" role="alert">
                                <button type="button" class="close" data-dismiss="alert">×</button>
                                {{ session('status') }}
                            </div>
                        @elseif(session('failed'))
                            <div class="alert alert-danger" role="alert">
                                <button type="button" class="close" data-dismiss="alert">×</button>
                                {{ session('failed') }}
                            </div>
                        @endif
                        <div class="form-group text-center">
                            <button type="submit" class="btn btn-main btn-submit btn_contact">
                                Send Us
                            </button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </section>
</main>
