<main class="main">

    <div class="page-header">
        <div class="container text-center">
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb text-center">
                    <li class="breadcrumb-item"><a href="/" title="Home">Home</a></li>
                    <li class="breadcrumb-item active">Giftcards</li>
                </ol>
            </nav>
            <h2 class="page-title">Special Nails & Spa</h2>
        </div>
    </div>
    <section class="section section-giftcards section-padding">
        <div class="container">
            <div class="row">
                <div class="col-md-8">

                    <!-- Giftcards -->
                    <div class="block-giftcards clearfix" id="boxPaymentItems">
                        <div class="title_gift"><i class="fa fa-location-arrow" aria-hidden="true"></i> Please
                            choose a design
                            and
                            add input e-gift information:
                        </div>
                        <div class="row">
                            <div class="col-6 col-xs-6 col-md-3 pd-fix">
                                <div data-id="252" data-name="Happy Mother's Day"
                                    data-image="https://fnail01o.opencartfb.com/uploads/fnail0bg5wepy/product/1599447282_img_product1599447282.png"
                                    data-price="25" data-price_custom="1" data-price_min="25" data-price_max="250"
                                    class="box_img_giftcard pointer paymentItem">
                                    <img itemprop="image" class="img-responsive"
                                        src="https://fnail01o.opencartfb.com/uploads/fnail0bg5wepy/product/1599447282_img_product1599447282.png"
                                        alt="">
                                    <div class="circle_check"></div>
                                    <div class="img_popup">
                                        <img src="https://fnail01o.opencartfb.com/uploads/fnail0bg5wepy/product/1599447282_img_product1599447282.png"
                                            alt="" />
                                    </div>
                                </div>
                            </div>
                            <div class="col-6 col-xs-6 col-md-3 pd-fix">
                                <div data-id="251" data-name="Happy Father's Day"
                                    data-image="https://fnail01o.opencartfb.com/uploads/fnail0bg5wepy/product/1599447270_img_product1599447270.png"
                                    data-price="25" data-price_custom="1" data-price_min="25" data-price_max="250"
                                    class="box_img_giftcard pointer paymentItem">
                                    <img itemprop="image" class="img-responsive"
                                        src="https://fnail01o.opencartfb.com/uploads/fnail0bg5wepy/product/1599447270_img_product1599447270.png"
                                        alt="">
                                    <div class="circle_check"></div>
                                    <div class="img_popup">
                                        <img src="https://fnail01o.opencartfb.com/uploads/fnail0bg5wepy/product/1599447270_img_product1599447270.png"
                                            alt="" />
                                    </div>
                                </div>
                            </div>
                            <div class="col-6 col-xs-6 col-md-3 pd-fix">
                                <div data-id="250" data-name="Happy Birthday"
                                    data-image="https://fnail01o.opencartfb.com/uploads/fnail0bg5wepy/product/1599447256_img_product1599447256.png"
                                    data-price="25" data-price_custom="1" data-price_min="25" data-price_max="250"
                                    class="box_img_giftcard pointer paymentItem">
                                    <img itemprop="image" class="img-responsive"
                                        src="https://fnail01o.opencartfb.com/uploads/fnail0bg5wepy/product/1599447256_img_product1599447256.png"
                                        alt="">
                                    <div class="circle_check"></div>
                                    <div class="img_popup">
                                        <img src="https://fnail01o.opencartfb.com/uploads/fnail0bg5wepy/product/1599447256_img_product1599447256.png"
                                            alt="" />
                                    </div>
                                </div>
                            </div>
                            <div class="col-6 col-xs-6 col-md-3 pd-fix">
                                <div data-id="249" data-name="Happy Valentine Day"
                                    data-image="https://fnail01o.opencartfb.com/uploads/fnail0bg5wepy/product/1599447244_img_product1599447244.png"
                                    data-price="25" data-price_custom="1" data-price_min="25" data-price_max="250"
                                    class="box_img_giftcard pointer paymentItem">
                                    <img itemprop="image" class="img-responsive"
                                        src="https://fnail01o.opencartfb.com/uploads/fnail0bg5wepy/product/1599447244_img_product1599447244.png"
                                        alt="">
                                    <div class="circle_check"></div>
                                    <div class="img_popup">
                                        <img src="https://fnail01o.opencartfb.com/uploads/fnail0bg5wepy/product/1599447244_img_product1599447244.png"
                                            alt="" />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-6">
                            <!-- Payment -->
                            <div class="block-Payment">
                                <form enctype="multipart/form-data" id="formPayment" name="formPayment"
                                    action="/payment/checkout" method="POST" class="form-horizontal">

                                    <div class="box-payer clearfix">
                                        <div class="title_gift">
                                            <i class="fa fa-id-card" aria-hidden="true"></i> Payer Information
                                        </div>
                                        <div class="row gift-price-quantity">
                                            <div class="col-6 col-xs-6 gift-price">
                                                <div class="group-select">
                                                    <label>Amount (required)</label>
                                                    <div class="relative w100">
                                                        <input class="form-control" type="text" title="Amount"
                                                            id="custom_price" name="custom_price" data-id="0"
                                                            data-price_custom="0" data-price_min="0" data-price_max="0"
                                                            value="25"
                                                            onkeypress="return check_enter_number(event, this);"
                                                            data-validation="[NOTEMPTY]"
                                                            data-validation-message="Please enter the amount"
                                                            autocomplete="off" placeholder="" readonly />
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-6 col-xs-6 gift-quantity">
                                                <div class="group-select">
                                                    <label>Quantity (required)</label>
                                                    <div class="relative w100">
                                                        <input class="form-control" type="number"
                                                            title="custom quantity" id="custom_quantity"
                                                            name="custom_quantity" data-id="0" value="1"
                                                            onkeypress="return check_enter_number(event,this);"
                                                            data-validation="[NOTEMPTY]"
                                                            data-validation-message="Please enter the quantity"
                                                            autocomplete="off" placeholder="" />
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-12 col-xs-12 gift-price-note" id="custom_price_note"
                                                style="display: none;"></div>
                                        </div>
                                        <div class="group-select gift-ship-name">
                                            <label>Full name (required)</label>
                                            <div class="relative w100">
                                                <input type="text" class="form-control" maxlength="70" title="Full name"
                                                    name="ship_full_name" id="ship_full_name" placeholder="Full name"
                                                    value="" data-validation="[NOTEMPTY]"
                                                    data-validation-message="Please enter your full name!" required
                                                    autocomplete="off">
                                            </div>
                                        </div>
                                        <div class="group-select gift-ship-email">
                                            <label>Email (required)</label>
                                            <div class="relative w100">
                                                <input type="text" class="form-control" name="ship_email" title="Email"
                                                    id="ship_email" placeholder="Email" value=""
                                                    data-validation="[EMAIL]"
                                                    data-validation-message="Please enter your email!" required
                                                    autocomplete="off">
                                            </div>
                                        </div>
                                        <div class="group-select gift-ship-phone">
                                            <label>Phone (required)</label>
                                            <div class="relative w100">
                                                <input type="text" class="form-control inputPhone" name="ship_phone"
                                                    pattern="\d*" id="ship_phone" title="Phone" placeholder="Phone"
                                                    value="" data-validation="[NOTEMPTY]"
                                                    data-validation-message="Please enter your phone!" required
                                                    autocomplete="off">
                                            </div>
                                        </div>
                                    </div>

                                    <div class="group-select group-checkbox clearfix gift-send-to-friend">
                                        <input title="send to friend" type="checkbox" name="send_to_friend"
                                            id="send_to_friend" value="1" class="pointer" style="margin-left: 0;">
                                        <label class="pointer" for="send_to_friend">Send gift card to
                                            relatives</label>
                                    </div>

                                    <div class="box_recipient box-recipient clearfix" id="boxRecipient"
                                        style="display: none;">
                                        <div class="title_gift"><i class="fa fa-users" aria-hidden="true"></i>
                                            Recipient information</div>
                                        <div class="group-select gift-recipient-name">
                                            <label>Recipient's Name</label>
                                            <div class="relative w100">
                                                <input type="text" class="form-control" maxlength="70"
                                                    title="Recipient's Name" name="recipient_name" id="recipient_name"
                                                    placeholder="Recipient name" value="" autocomplete="off">
                                            </div>
                                        </div>
                                        <div class="group-select gift-recipient-email">
                                            <label>Recipient's Email</label>
                                            <div class="relative w100">
                                                <input type="text" class="form-control" name="recipient_email"
                                                    id="recipient_email" title="Recipient" placeholder="Email" value=""
                                                    autocomplete="off">
                                            </div>
                                        </div>
                                        <div class="group-select gift-recipient-message">
                                            <label>Message</label>
                                            <div class="relative w100">
                                                <textarea class="form-control" name="recipient_message" title="Message"
                                                    id="recipient_message" placeholder="Message send to Recipient"
                                                    onkeyup="change_content(this, '#preview_message');"
                                                    maxlength="300"></textarea>
                                            </div>
                                        </div>
                                    </div>

                                    <button type="submit" title="Checkout with Paypal"
                                        class="btn btn-main btn-style-four btn_payment">
                                        <span>Checkout with Paypal</span>
                                    </button>

                                    <!-- Tpl note_payment -->
                                    <p>By clicking 'Checkout with Paypal' you agree to our privacy policy and terms
                                        of service. You also agree to receive periodic email updates, discounts, and
                                        special offers.</p>
                                    <!-- Hidden data -->
                                    <input type="hidden" name="type_page" value="1">
                                </form>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <!-- Cart -->
                            <div class="box-cart clearfix" id="boxCart">
                                <div class="title_gift"><i class="fa fa-cart-plus" aria-hidden="true"></i>
                                    Your cart </div>
                                <figure class="mybox-order">
                                    <ul>
                                        <li class="cart-item">
                                            <div class="item-info">
                                                <div class="item-image" id="cart_image"><img
                                                        src="/public/library/global/no-photo.jpg" alt="No photo" />
                                                </div>
                                                <div>
                                                    <span class="item-quantity" id="cart_quantity">1</span>
                                                    <span> x </span>
                                                    <span class="item-name" id="cart_name">N/A</span>
                                                </div>
                                            </div>
                                        </li>
                                        <li class="cart-subtotal">
                                            <label>Subtotal</label>
                                            <span class="money pull-right" id="cart_subtotal">N/A</span>
                                        </li>
                                        <li class="cart-discount">
                                            <label>Discount/Bonus</label>
                                            <span class="money pull-right" id="cart_discount">N/A</span>
                                        </li>
                                        <li class="cart-vat">
                                            <label>Tax/Fee</label>
                                            <span class="money pull-right" id="cart_tax">N/A</span>
                                        </li>
                                        <li class="cart-vat">
                                            <label>Total</label>
                                            <span class="money pull-right" id="cart_total">N/A</span>
                                        </li>
                                    </ul>
                                </figure>

                                <!-- Tpl note_shipping -->
                                <figure class="note">
                                    <p itemprop="name" class="sanb" style="font-size: 16px; font-weight: bold;">When
                                        will my order be shipped?</p>
                                    <p itemprop="description">Your e-Gift card will be sent immediately after we
                                        receive your order</p>
                                </figure>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <!-- Preview -->
                    <div class="block-preview" id="boxPreview">
                        <div class="title_gift"><i class="fa fa-television" aria-hidden="true"></i> Preview</div>
                        <div class="box_preview">
                            <ul class="information">
                                <li>
                                    <span class="center">Special Nails & Spa</span>
                                </li>
                                <li>
                                    <span class="left">Address:</span>
                                    <span class="right">11011 Richmond Ave,<br> Ste 250 <br /> Houston, TX
                                        77042</span>
                                </li>
                                <li>
                                    <span class="left">Phone:</span>
                                    <span class="right">832-968-6668</span>
                                </li>
                            </ul>
                            <div class="preview_image" id="preview_image">
                                <img src="/public/library/global/no-photo.jpg" alt="giftcard" />
                            </div>
                            <div class="info_send">
                                <ul class="information">
                                    <li>
                                        <div class="row">
                                            <div class="col-6 col-xs-6">
                                                <span class="left">Amount:</span>
                                                <span class="right preview_amount" id="preview_amount">$0</span>
                                            </div>
                                            <div class="col-6 col-xs-6">
                                                <span class="left">Quantity:</span>
                                                <span class="right preview_quantity" id="preview_quantity">1</span>
                                            </div>
                                        </div>
                                    </li>
                                    <li>
                                        <span class="left">From:</span>
                                        <span class="right preview_from" id="preview_from"></span>
                                    </li>
                                    <li>
                                        <span class="left">To:</span>
                                        <span class="right preview_to" id="preview_to"></span>
                                    </li>
                                    <li>
                                        <span class="left">Message:</span>
                                        <span class="right preview_message" id="preview_message"></span>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <script type="text/javascript">
                $(document).ready(function() {
                    webPaymentForm.init(true, '{"id":252,"cus_price":25,"cus_quantity":1}');
                });

            </script>
        </div>
    </section>
</main>
