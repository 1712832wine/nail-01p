<footer class="footer">
    @php
        $features = json_decode($header['features']);
        $extras = json_decode($header['extras']);
    @endphp
    <div class="section-footer section-padding text-white">
        <div class="container text-center">
            @if (count(json_decode($header['images'])) > 0)
                <div class="logo logo-footer">
                    <a itemprop="url" href="{{ route('home') }}" title="Logo">
                        <img class="imgrps"
                            src="{{ asset('storage') }}/photos/{{ json_decode($header['images'])[0] }}"
                            alt="Special Nails & Spa" itemprop="logo image" />
                    </a>
                </div>
            @endif
            <div class="footer-contact">
                @if (count($features) > 2)
                    <p class="phone">
                        <i class="fa {{ $features[2]->name }}"></i>
                        <a href="tel:{{ $features[2]->desc }}" title="Call Us">
                            <span itemprop="telephone">{{ $features[2]->desc }}</span>
                        </a>
                    </p>
                @endif
                @if (count($features) > 1)
                    <p class="email">
                        <i class="fa fa-envelope-o"></i>
                        <a href="mailto:{{ $features[1]->desc }}" title="Mail Us">
                            <span itemprop="email">{{ $features[1]->desc }}</span>
                        </a>
                    </p>
                @endif
            </div>
        </div>
    </div>
    <div class="copyright-footer">
        <div class="container">
            <div class="row">
                <div class="col-md-6 col-12 order-md-2">
                    <div class="socials">
                        <ul class="list-line social">
                            @foreach ($social as $item)
                                <li>
                                    @if ($item['url'])
                                        <a itemprop="url" rel="nofollow" target="_blank" title="Facebook"
                                            href="{{ $item['content'] }}">
                                            <img src="{{ asset('storage') }}/photos/{{ $item['url'] }}"
                                                alt="Facebook" />
                                        </a>
                                    @endif

                                </li>
                            @endforeach
                        </ul>
                    </div>
                </div>
                <div class="col-md-6 col-12 order-md-1">
                    <p class="copyright">
                        © 2021 Special Nails & Spa. All Rights Reserved.
                    </p>
                </div>
            </div>
        </div>
    </div>
    <div class="back-to-top">
        <span class="back-top"><i class="fa fa-chevron-up"></i></span>
    </div>
</footer>
<div class="freeze-footer">
    <ul>
        <li>
            <a href="tel:832-968-6668" class="btn btn-call" title="Call us">
                <i class="fa fa-phone" aria-hidden="true"></i>
                <span class="d-md-none">Call Us</span>
            </a>
        </li>
        <li class="btn_make_appointment">
            <a href="{{ route('book') }}" class="btn btn-booking btn_make_appointment" title="Booking">
                <i class="fa fa-calendar" aria-hidden="true"></i>
                <span class="d-md-none">Booking</span>
            </a>
        </li>
    </ul>
</div>
