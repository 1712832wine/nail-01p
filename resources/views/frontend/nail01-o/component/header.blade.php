@php
$menu = [
    [
        'title' => 'Home',
        'href' => route('home'),
        'name' => 'home',
    ],
    [
        'title' => 'About',
        'href' => route('about'),
        'name' => 'about',
    ],
    [
        'title' => 'Services',
        'href' => route('services'),
        'name' => 'services',
    ],
    [
        'title' => 'Booking',
        'href' => route('book'),
        'name' => 'book',
    ],
    [
        'title' => 'Coupons',
        'href' => route('coupons'),
        'name' => 'coupons',
    ],
    [
        'title' => 'GiftCards',
        'href' => route('giftcards'),
        'name' => 'giftcards',
    ],
    [
        'title' => 'Gallery',
        'href' => route('gallery'),
        'name' => 'gallery',
    ],
    [
        'title' => 'Contact',
        'href' => route('contact'),
        'name' => 'contact',
    ],
];
@endphp
<header class="header">
    <section class="header-section clearfix top-header">
        <div class="
        wrap-freeze-header-mobile
        hidden-md hidden-lg
        d-block d-lg-none">
            <div class="flag-freeze-header-mobile">
                <div class="menu_mobile_v1">
                    <div class="mobile_logo_container">
                        <div class="mobile_logo">
                            <a itemprop="url" href="/" title="Logo">
                                <img class="imgrps"
                                    src="https://fnail01o.opencartfb.com/uploads/fnail0bg5wepy/attach/1599534589_logo_logo-fnail01o.png"
                                    alt="Special Nails & Spa" itemprop="logo image" />
                            </a>
                        </div>
                    </div>
                    <div class="mobile_menu_container_v1">
                        <!-- Tpl menu mobile 2 layouts -->
                        <div class="mobile-menu-scroll">
                            <nav id="mobile_scroll">
                                <ul>
                                    @foreach ($menu as $item)
                                        <li class="@if (Route::current()->getName() ===
                                            $item['name']) active @endif">
                                            <a itemprop="url" href="{{ $item['href'] }}" class="@if (Route::current()->getName() === $item['name']) active @endif">{{ $item['title'] }}</a>
                                        </li>
                                    @endforeach
                                </ul>
                            </nav>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="wrap-freeze-header hidden-xs hidden-sm d-none d-lg-block">
            <div class="flag-freeze-header">
                <nav class="navbar navbar-default navbar-expand-lg">
                    <div class="container navbar-container">
                        @if (count(json_decode($header['images'])) > 0)
                            <div class="navbar-brand">
                                <a itemprop="url" href="{{ route('home') }}" title="Logo">
                                    <img class="imgrps"
                                        src="{{ asset('storage') }}/photos/{{ json_decode($header['images'])[0] }}"
                                        alt="Special Nails & Spa" itemprop="logo image" />
                                </a>
                            </div>
                        @endif

                        <div class="navbar-collapse header-nav-desktop">
                            <!-- Tpl menu main layouts -->
                            <ul class="nav navbar-nav">
                                @foreach ($menu as $item)
                                    <li class="@if (Route::current()->getName() ===
                                        $item['name']) active @endif">
                                        <a itemprop="url" href="{{ $item['href'] }}" class="@if (Route::current()->getName() === $item['name']) active @endif">{{ $item['title'] }}</a>
                                    </li>
                                @endforeach
                            </ul>
                        </div>
                    </div>
                </nav>
            </div>
        </div>
    </section>
</header>
