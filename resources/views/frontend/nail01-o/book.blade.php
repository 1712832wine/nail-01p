<main class="main">

    <div class="page-header">
        <div class="container text-center">
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb text-center">
                    <li class="breadcrumb-item"><a href="/" title="Home">Home</a></li>
                    <li class="breadcrumb-item active">Booking</li>
                </ol>
            </nav>
            <h2 class="page-title">Special Nails & Spa</h2>
        </div>
    </div>
    <section class="section section-booking section-padding">
        <div class="container">
            <section id="boxBookingForm" class="box-booking-form">
                <form id="formBooking" class="form-booking" name="formBooking" action="/book/add" method="post"
                    enctype="multipart/form-data">
                    <div class="row">
                        <div class="col-sm-6 col-md-4 col-lg-3 booking-date">
                            <div class="group-select">
                                <label>Date (required)</label>
                                <div class="relative w100 form-input-group">
                                    <input type="text" class="form-control form-text booking_date" autocomplete="off"
                                        name="booking_date" value="" data-validation="[NOTEMPTY]"
                                        data-validation-message="Please choose date" title="Booking date" placeholder=""
                                        maxlength="16" />
                                    <span class="fa fa-calendar calendar form-icon"></span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-12 col-md-12 col-lg-9">
                            <div class="row booking-service-staff booking-item" id="bookingItem_0">
                                <div class="col-sm-12 col-md-7 col-lg-8 booking-service">
                                    <div class="group-select">
                                        <label>Service (required)</label>
                                        <div class="relative w100">
                                            <select class="form-control booking_service" name="product_id[]"
                                                data-validation="[NOTEMPTY]"
                                                data-validation-message="Please choose a service"
                                                title="Booking service">
                                                <option value="">Select service</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-12 col-md-5 col-lg-4 booking-staff">
                                    <div class="group-select">
                                        <label>Technician (optional)</label>
                                        <div class="relative w100">
                                            <select class="form-control booking_staff" name="staff_id[]"
                                                title="Booking staff">
                                                <option value="">Select technician</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <!-- Button Add Item -->
                            <div class="clearfix">
                                <div class="add-services pointer booking_item_add">
                                    <i class="fa fa-plus-circle"></i> Add another service
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-12 col-md-12 col-lg-3">
                            <div class="group-select">
                                <label>&nbsp;</label>
                                <div class="relative w100">
                                    <button class="btn btn-search search_booking" type="button">
                                        Search
                                    </button>
                                </div>

                                <!-- Hidden data -->
                                <input type="hidden" name="booking_hours" class="booking_hours" value="" />
                                <input type="hidden" name="booking_name" class="booking_name" value="" />
                                <input type="hidden" name="booking_phone" class="booking_phone" value="" />
                                <input type="hidden" name="booking_email" class="booking_email" value="" />
                                <input type="hidden" name="notelist" class="notelist" value="" />
                                <input type="hidden" name="store_id" class="store_id" value="" />

                                <input type="hidden" name="booking_area_code" class="booking_area_code" value="1" />
                                <input type="hidden" name="booking_form_email" class="booking_form_email" value="0" />
                                <input type="hidden" name="nocaptcha" class="nocaptcha" value="1" />
                                <input type="hidden" name="g-recaptcha-response" class="g-recaptcha-response"
                                    value="" />
                            </div>
                        </div>
                    </div>
                </form>

                <script type="text/javascript">
                    $(document).ready(function() {
                        /* Init Booking */
                        webBookingForm.init(
                            '{"66":{"id":66,"name":"Manicure","services":[210,211,212,213,214,215]},"70":{"id":70,"name":"Solar Nails - Full Set","services":[190,191,193,195,197,199,202,203,204,205]},"71":{"id":71,"name":"Solar Nails - Refill","services":[192,194,196,198,200,201]},"67":{"id":67,"name":"Pedicure","services":[216,217,218,219,220,221,222,223]},"65":{"id":65,"name":"SNS (Dipping Powder)","services":[246,247,248]},"69":{"id":69,"name":"Kid Menu","services":[240,241,242,243,244,245]},"68":{"id":68,"name":"Waxing","services":[224,225,226,227,228,229,230,231,232,233,234,235,236,237,238,239]}}',
                            '{"210":{"id":210,"name":"Manicure Classic","price":"$16 &UP","staffs":null,"schedule":false,"unit":"person","scheduleDay":null},"211":{"id":211,"name":"Shellac Manicure","price":"$35 ","staffs":null,"schedule":false,"unit":"person","scheduleDay":null},"212":{"id":212,"name":"Milk and Honey Manicure","price":"$35 ","staffs":null,"schedule":false,"unit":"person","scheduleDay":null},"213":{"id":213,"name":"Cucumber Manicure","price":"$35 ","staffs":null,"schedule":false,"unit":"person","scheduleDay":null},"214":{"id":214,"name":"Nu Skin Manicure","price":"$35 ","staffs":null,"schedule":false,"unit":"person","scheduleDay":null},"215":{"id":215,"name":"Lavender Manicure","price":"$35 ","staffs":null,"schedule":false,"unit":"person","scheduleDay":null},"190":{"id":190,"name":"Full Set Color","price":"$35 ","staffs":null,"schedule":false,"unit":"person","scheduleDay":null},"191":{"id":191,"name":"Full Set Pearl \/ White Tip","price":"$35 ","staffs":null,"schedule":false,"unit":"person","scheduleDay":null},"193":{"id":193,"name":"Shellac Full Set","price":"$50 ","staffs":null,"schedule":false,"unit":"person","scheduleDay":null},"195":{"id":195,"name":"Ombre Set","price":"$60 ","staffs":null,"schedule":false,"unit":"person","scheduleDay":null},"197":{"id":197,"name":"Pink and White Full Set","price":"$55 ","staffs":null,"schedule":false,"unit":"person","scheduleDay":null},"199":{"id":199,"name":"Color Powder Full Set","price":"$53 ","staffs":null,"schedule":false,"unit":"person","scheduleDay":null},"202":{"id":202,"name":"Shape","price":"$5 ","staffs":null,"schedule":false,"unit":"person","scheduleDay":null},"203":{"id":203,"name":"Long","price":"$5 ","staffs":null,"schedule":false,"unit":"person","scheduleDay":null},"204":{"id":204,"name":"Take off only","price":"$10 ","staffs":null,"schedule":false,"unit":"person","scheduleDay":null},"205":{"id":205,"name":"Nail Care Upgrade","price":"$0 ","staffs":null,"schedule":false,"unit":"person","scheduleDay":null},"192":{"id":192,"name":"Pink Fill In","price":"$25 ","staffs":null,"schedule":false,"unit":"person","scheduleDay":null},"194":{"id":194,"name":"Shellac Fill","price":"$42 ","staffs":null,"schedule":false,"unit":"person","scheduleDay":null},"196":{"id":196,"name":"Ombre Fills","price":"$50 ","staffs":null,"schedule":false,"unit":"person","scheduleDay":null},"198":{"id":198,"name":"Pink and White Fill In","price":"$45 ","staffs":null,"schedule":false,"unit":"person","scheduleDay":null},"200":{"id":200,"name":"Change Color Fill","price":"$43 ","staffs":null,"schedule":false,"unit":"person","scheduleDay":null},"201":{"id":201,"name":"Fill same Color Powder","price":"$33 ","staffs":null,"schedule":false,"unit":"person","scheduleDay":null},"216":{"id":216,"name":"Cucumber Cucumber","price":"$51 ","staffs":null,"schedule":false,"unit":"person","scheduleDay":null},"217":{"id":217,"name":"Heavenly","price":"$50 ","staffs":null,"schedule":false,"unit":"person","scheduleDay":null},"218":{"id":218,"name":"Sport Pedicure","price":"$45 ","staffs":null,"schedule":false,"unit":"person","scheduleDay":null},"219":{"id":219,"name":"Lavender","price":"$40 ","staffs":null,"schedule":false,"unit":"person","scheduleDay":null},"220":{"id":220,"name":"Classic","price":"$30 ","staffs":null,"schedule":false,"unit":"person","scheduleDay":null},"221":{"id":221,"name":"Milk and Honey","price":"$60 ","staffs":null,"schedule":false,"unit":"person","scheduleDay":null},"222":{"id":222,"name":"NUSKIN Natural Herbal","price":"$60 ","staffs":null,"schedule":false,"unit":"person","scheduleDay":null},"223":{"id":223,"name":"Fruity Tutti","price":"$55 ","staffs":null,"schedule":false,"unit":"person","scheduleDay":null},"246":{"id":246,"name":"Pink\/White","price":"$40 ","staffs":null,"schedule":false,"unit":"person","scheduleDay":null},"247":{"id":247,"name":"Color","price":"$40 ","staffs":null,"schedule":false,"unit":"person","scheduleDay":null},"248":{"id":248,"name":"With Tip","price":"$45 ","staffs":null,"schedule":false,"unit":"person","scheduleDay":null},"240":{"id":240,"name":"Manicure Princess","price":"$12 ","staffs":null,"schedule":false,"unit":"person","scheduleDay":null},"241":{"id":241,"name":"Pedicure Princess","price":"$22 ","staffs":null,"schedule":false,"unit":"person","scheduleDay":null},"242":{"id":242,"name":"Princess Polish Hand","price":"$7 ","staffs":null,"schedule":false,"unit":"person","scheduleDay":null},"243":{"id":243,"name":"Princess Polish Toes","price":"$10 ","staffs":null,"schedule":false,"unit":"person","scheduleDay":null},"244":{"id":244,"name":"Shellac Manicure < 10","price":"$27 ","staffs":null,"schedule":false,"unit":"person","scheduleDay":null},"245":{"id":245,"name":"Princess Shellac Pedicure","price":"$37 ","staffs":null,"schedule":false,"unit":"person","scheduleDay":null},"224":{"id":224,"name":"Eyebrows","price":"$12 ","staffs":null,"schedule":false,"unit":"person","scheduleDay":null},"225":{"id":225,"name":"Hand\/Toes\/Nose\/Neck\/Sideburns","price":"$10 ","staffs":null,"schedule":false,"unit":"person","scheduleDay":null},"226":{"id":226,"name":"Arms (Full)","price":"$35 ","staffs":null,"schedule":false,"unit":"person","scheduleDay":null},"227":{"id":227,"name":"Arms (Half)","price":"$30 ","staffs":null,"schedule":false,"unit":"person","scheduleDay":null},"228":{"id":228,"name":"Under Arm","price":"$30 ","staffs":null,"schedule":false,"unit":"person","scheduleDay":null},"229":{"id":229,"name":"Shoulders","price":"$20 ","staffs":null,"schedule":false,"unit":"person","scheduleDay":null},"230":{"id":230,"name":"Lip","price":"$8 ","staffs":null,"schedule":false,"unit":"person","scheduleDay":null},"231":{"id":231,"name":"Back (Full)","price":"$50 ","staffs":null,"schedule":false,"unit":"person","scheduleDay":null},"232":{"id":232,"name":"Legs (Full)","price":"$60 ","staffs":null,"schedule":false,"unit":"person","scheduleDay":null},"233":{"id":233,"name":"Legs (Half)","price":"$30 ","staffs":null,"schedule":false,"unit":"person","scheduleDay":null},"234":{"id":234,"name":"Mini Facial","price":"$35 ","staffs":null,"schedule":false,"unit":"person","scheduleDay":null},"235":{"id":235,"name":"Chin","price":"$8 ","staffs":null,"schedule":false,"unit":"person","scheduleDay":null},"236":{"id":236,"name":"Chest","price":"$25 ","staffs":null,"schedule":false,"unit":"person","scheduleDay":null},"237":{"id":237,"name":"Brazilian","price":"$50 ","staffs":null,"schedule":false,"unit":"person","scheduleDay":null},"238":{"id":238,"name":"Bikini","price":"$35 ","staffs":null,"schedule":false,"unit":"person","scheduleDay":null},"239":{"id":239,"name":"Extra Waxing Service","price":"$0 ","staffs":null,"schedule":false,"unit":"person","scheduleDay":null}}',
                            "null",
                            '{"listperson":[],"notelist":[],"booking_date":"05\/24\/2021","booking_hours":""}'
                        );
                    });

                </script>
            </section>

            <section id="boxBookingInfo" class="box-booking-info relative" style="display: none">
                <h3 class="booking-info-title">Appointment Information</h3>

                <!-- Service, Staff -->
                <div id="boxServiceStaff" class="box-service-staff">
                    <div class="service-staff">
                        <div class="service-staff-avatar">
                            <img class="img-responsive" src="/public/library/global/no-photo.jpg" alt="Any person" />
                        </div>
                        <div class="service-staff-info">
                            <h5>Any Technician</h5>
                            <p>Any Service</p>
                            <p>Price: N/A</p>
                        </div>
                    </div>
                    <div class="service-staff">
                        <div class="service-staff-avatar no-photo">
                            <img class="img-responsive" src="/public/library/global/no-photo.jpg" alt="Any person" />
                        </div>
                        <div class="service-staff-info">
                            <h5>Any Technician</h5>
                            <p>Any Service</p>
                            <p>Price: N/A</p>
                        </div>
                    </div>
                </div>

                <!-- Date, Time List -->
                <div id="boxDateTime" class="box-date-time">
                    <h4 class="date-info" id="dateInfo">Sunday, Jan-01-1970</h4>
                    <div class="time-info">
                        <h5>
                            Morning <span class="time-note" id="timeAMNote">N/A</span>
                        </h5>
                        <ul class="time-items" id="timeAMHtml"></ul>
                    </div>
                    <div class="time-info">
                        <h5>
                            Afternoon <span class="time-note" id="timePMNote">N/A</span>
                        </h5>
                        <ul class="time-items" id="timePMHtml"></ul>
                    </div>
                </div>
            </section>

            <section id="popupBookingConfirm" class="popup-booking-confirm white-popup mfp-hide border-style">
                <section id="boxBookingConfirm" class="box-booking-confirm relative">
                    <h3 class="booking-confirm-title">
                        Confirm booking information ?
                    </h3>
                    <div class="booking-confirm-note">
                        We will send a text message to you via the number below after we
                        confirm the calendar for your booking.
                    </div>
                    <script src="https://www.google.com/recaptcha/api.js?hl=en" async defer></script>
                    <!-- Google reCaptcha -->
                    <script type="text/javascript">
                        function ezyCaptcha_formBookingConfirm(token, is_submit) {
                            is_submit = 1;
                            if ($("#password").length) {
                                //$("input:password").val(md5(clean_input($("#password").val())));
                            }
                            return true;
                        }

                    </script>
                    <form id="formBookingConfirm" class="form-booking-confirm" name="formBookingConfirm" method="post"
                        action="/book" enctype="multipart/form-data">
                        <div class="row">
                            <div class="col-md-6 booking-name">
                                <div class="group-select">
                                    <label>Your name (required)</label>
                                    <div class="relative w100">
                                        <input type="text" class="form-control" name="booking_name" value=""
                                            data-validation="[NOTEMPTY]"
                                            data-validation-message="Please enter your name" placeholder=""
                                            maxlength="76" title="Booking name" />
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6 booking-phone">
                                <div class="group-select">
                                    <label>Your phone (required)</label>
                                    <div class="relative w100">
                                        <input type="text" class="form-control inputPhone" pattern="\d*"
                                            name="booking_phone" data-validation="[NOTEMPTY]"
                                            data-validation-message="Please enter your phone" placeholder=""
                                            maxlength="16" title="Booking phone" />
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-12 booking-email">
                                <div style="display: none" class="group-select">
                                    <label>Your email (optional)</label>
                                    <div class="relative w100">
                                        <input type="text" class="form-control" name="booking_email" value=""
                                            placeholder="" maxlength="76" title="Booking email" />
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-12 booking-notelist">
                                <div class="group-select">
                                    <label>Note (optional)</label>
                                    <div class="relative w100">
                                        <textarea class="form-control" rows="5" name="notelist"
                                            placeholder="(Max length 200 character)" maxlength="201"
                                            title="Booking note"></textarea>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-12 booking-store">
                                <input type="hidden" name="choose_store" class="booking_store" value="0" />
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-8 order-md-2 col-md-push-4">
                                <button class="btn btn-confirm btn_confirm" type="button">
                                    Confirm
                                </button>
                            </div>
                            <div class="col-md-4 order-md-1 col-md-pull-8">
                                <button class="btn btn-cancel btn_cancel" type="button">
                                    Cancel
                                </button>
                            </div>
                        </div>
                    </form>
                </section>
            </section>
        </div>
    </section>
</main>
