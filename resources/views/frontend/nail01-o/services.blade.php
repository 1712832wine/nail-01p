<main class="main">
    <!-- Use for get current id -->
    <input type="hidden" name="group_id" value="{{ $service_id }}" />
    <!-- Tpl main service -->
    <!--# Support:# render.service_data.service, render.service_data2.service-->
    <div class="page-header">
        <div class="container text-center">
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb text-center">
                    <li class="breadcrumb-item"><a href="/" title="Home">Home</a></li>
                    <li class="breadcrumb-item active">Services</li>
                </ol>
            </nav>
            <h2 class="page-title">Special Nails & Spa</h2>
        </div>
    </div>
    <section class="section section-service section-padding">
        <div class="container">
            <div class="service-container animation_sroll_button">
                <div class="row">
                    <div class="col-md-7 order-md-2">
                        <div class="service-btn-group text-right btn-wrapper"><a href="{{ route('book') }}"
                                class="btn btn-main btn_make_appointment btn-blank" title="Make an appointment"> <i
                                    class="far fa-calendar-check"></i> Make an appointment </a>&nbsp <a
                                href="tel:832-968-6668" class="btn btn-main btn-blank" title="Call now"> <i
                                    class="fa fa-phone"></i> Call now </a></div>
                    </div>
                    <div class="col-md-5">
                        <h2>Nail Services</h2>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <p><em>With the full of beauty care services and new colors for you to choose from, you are
                                ensured to enjoy the best services in our effort of doing a great
                                job.</em><br /><em> Let’s take a look at our price list below!! Our nail salon is
                                always committed to bringing you a reasonable price!</em></p>
                    </div>
                </div>
                <div class="clearfix animation_sroll_to_service service-list">
                    @foreach ($services as $index => $service)
                        @if ($index % 2)
                            <div class="row clearfix" id="sci_{{ $service['id'] }}">
                                @if (count(json_decode($service['images'])) > 0)
                                    <div class="col-sm-4 col-md-4  right service-image-wrap">
                                        <div class="service-image circle">
                                            <div class="service-image-bg"
                                                style="background-image: url('{{ asset('storage') }}/photos/{{ json_decode($service['images'])[0] }}')">
                                                <img class="img-responsive"
                                                    src="{{ asset('storage') }}/photos/{{ json_decode($service['images'])[0] }}"
                                                    alt="Manicure">
                                            </div>
                                        </div>
                                    </div>
                                @endif
                                <div class="col-sm-8 col-md-8 ">
                                    <div class="clearfix service-list">
                                        <h2 class="service-name">{{ $service['name'] }}</h2>
                                        @foreach (json_decode($service['features']) as $item)
                                            <div class="detail-item item-210">
                                                <div class="detail-price-item">
                                                    <span class="detail-price-name">{{ $item->name }}</span>
                                                    <span class="detail-price-dots"></span>
                                                    <span class="detail-price-number">
                                                        <span class="current">{{ $item->desc }}</span>
                                                    </span>
                                                </div>
                                            </div>
                                        @endforeach
                                    </div>
                                </div>
                            </div>
                        @else
                            <div class="row clearfix" id="sci_{{ $service['id'] }}">
                                <div class="service-line"></div>
                                <div class="col-sm-4 col-md-4 order-md-2 left service-image-wrap">
                                    <div class="service-image circle">
                                        <div class="service-image-bg"
                                            style="background-image: url('{{ asset('storage') }}/photos/{{ json_decode($service['images'])[0] }}')">
                                            <img class="img-responsive"
                                                src="{{ asset('storage') }}/photos/{{ json_decode($service['images'])[0] }}"
                                                alt="Solar Nails">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-8 col-md-8 order-md-1">
                                    <div class="clearfix service-list">
                                        <div class="clearfix service-list-sub">
                                            <h2 class="service-name">{{ $service['name'] }}</h2>
                                            @foreach (json_decode($service['features']) as $item)
                                                <div class="detail-item item-190">
                                                    <div class="detail-price-item">
                                                        <span class="detail-price-name">{{ $item->name }}</span>
                                                        <span class="detail-price-dots"></span>
                                                        <span class="detail-price-number">
                                                            <span class="current">{{ $item->desc }}</span>
                                                        </span>
                                                    </div>
                                                </div>
                                            @endforeach
                                        </div>

                                    </div>
                                </div>
                            </div>
                        @endif
                    @endforeach
                </div>
            </div>
        </div>
    </section>
</main>
