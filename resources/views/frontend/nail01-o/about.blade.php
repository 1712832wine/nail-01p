<main class="main">
    <!-- Tpl main about -->
    <div class="page-header">
        <div class="container text-center">
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb text-center">
                    <li class="breadcrumb-item"><a href="{{ route('home') }}" title="Home">Home</a></li>
                    <li class="breadcrumb-item active">About Us</li>
                </ol>
            </nav>
            <h2 class="page-title">Special Nails & Spa</h2>
        </div>
    </div>
    <section class="section section-about section-padding">
        <div class="container">
            @foreach ($articles as $item)
                <div class="row">
                    @if (count(json_decode($item['image'])) > 0)
                        <div class="col-lg-5 col-md-6">
                            <p class="text-center"><img itemprop="image"
                                    src="{{ asset('storage') }}/photos/{{ json_decode($item['image'])[0] }}"
                                    alt="About Us" caption="false" /></p>
                        </div>
                    @endif
                    <div class="col-lg-6 col-md-7">
                        <div class="about-content">
                            {!! $item['content'] !!}
                        </div>
                    </div>
                </div>
            @endforeach
        </div>
    </section>
</main>
