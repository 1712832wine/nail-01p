<main>

    <section class="p-service">
        <!-- tpl main -->
        <!-- support render service_data1 or service_data2 -->
        <!-- Animation scroll to block service: with only add class 'animation_sroll_jumpto' in element contain entire html service data -->
        <div class="pmain">
            <div class="page-heading">
                <h2><span>Services</span></h2>
                <img src="{{ asset('frontend') }}/themes/fnail04a/assets/images/bodysugarytop-1.jpg" alt=""
                    class="imgrps">
            </div>
            <div class="in-container bg-width">
                <div class="container">
                    <div class="in-container-service">
                        <div class="row">
                            <div class="col-sm-12 top-right btn_service_defale"></div>
                        </div>
                        <div class="service-container">
                            <div class="animation_sroll_jumpto">
                                <div class="sroll_jumpto">
                                    <input type="hidden" name="group_id" value="{{ $service_id }}" />
                                    @foreach ($services as $index => $service)
                                        @if ($index % 2 !== 1)
                                            <div class="row service-row" id="sci_{{ $service['id'] }}">
                                                <div class="col-sm-4 col-md-4 text-center">
                                                    @if (count(json_decode($service['images'])) > 0)
                                                        <div class="circle-service-image"
                                                            style="background-image: url('{{ asset('storage') }}/photos/{{ json_decode($service['images'])[0] }}');')">
                                                        </div>
                                                    @endif
                                                </div>
                                                <div class="col-sm-8 col-md-8">
                                                    <h2 class="service-name" id="pink_white_powder_nails">
                                                        {{ $service['name'] }}</h2>
                                                    <p></p>
                                                    @foreach (json_decode($service['features']) as $item)
                                                        @switch($item->name)
                                                            @case('desc')
                                                                <p>{{ $item->desc }}</p>
                                                            @break
                                                            @case('center')
                                                            @break
                                                            @default
                                                                <div class="detail-price-item">
                                                                    <span class="detail-price-name">{{ $item->name }}</span>
                                                                    <span class="detail-price-dots"></span>
                                                                    <span class="detail-price-number">{{ $item->desc }}</span>
                                                                    </span>
                                                                </div>
                                                        @endswitch
                                                    @endforeach
                                                </div>
                                            </div>
                                        @else
                                            <div class="row service-row" id="sci_{{ $service['id'] }}">
                                                <div
                                                    class="col-sm-4 col-md-4 text-center hidden-sm hidden-md hidden-lg">
                                                    @if (count(json_decode($service['images'])) > 0)
                                                        <div class="circle-service-image"
                                                            style="background-image: url('{{ asset('storage') }}/photos/{{ json_decode($service['images'])[0] }}')">
                                                        </div>
                                                    @endif
                                                </div>
                                                <div class="col-sm-8 col-md-8 ">
                                                    <h2 class="service-name" id="gel_powder_nails">
                                                        {{ $service['name'] }}</h2>
                                                    <p></p>
                                                    @foreach (json_decode($service['features']) as $item)
                                                        @switch($item->name)
                                                            @case('desc')
                                                                <p>{{ $item->desc }}</p>
                                                            @break
                                                            @case('center')
                                                            @break
                                                            @default
                                                                <div class="detail-price-item">
                                                                    <span class="detail-price-name">{{ $item->name }}</span>
                                                                    <span class="detail-price-dots"></span>
                                                                    <span
                                                                        class="detail-price-number">{{ $item->desc }}</span>
                                                                    </span>
                                                                </div>
                                                        @endswitch
                                                    @endforeach
                                                </div>
                                                <div class="col-sm-4 col-md-4 text-center hidden-xs">
                                                    @if (count(json_decode($service['images'])) > 0)
                                                        <div class="circle-service-image"
                                                            style="background-image: url('{{ asset('storage') }}/photos/{{ json_decode($service['images'])[0] }}')">
                                                        </div>
                                                    @endif
                                                </div>
                                            </div>
                                        @endif
                                    @endforeach
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</main>
