<main>

    <div class="pmain">
        <div class="page-heading">
            <h2><span>Contact us</span></h2>
            <img src="{{ asset('frontend') }}/themes/fnail04a/assets/images/bodysugarytop-1.jpg" alt=""
                class="imgrps">
        </div>
        <div class="in-container bg-width clearfix">
            <div class="container">
                <div class="in-content">
                    <div class="row flex">
                        <div class="col-md-6 col-xs-12">
                            <h4 class="color-main">Contact Us</h4>
                            <div class="contact-form">
                                <div class="box-msg box-msg-"></div>
                                <script src='https://www.google.com/recaptcha/api.js?hl=en' async defer></script>
                                <!-- Google reCaptcha -->
                                <script type="text/javascript">
                                    function ezyCaptcha_send_contact(token, is_submit) {
                                        is_submit = 0;
                                        if ($("#password").length) {
                                            //$("input:password").val(md5(clean_input($("#password").val())));
                                        }
                                        return true;
                                    }

                                </script>
                                <div class="row">
                                    <form class="wpcf7-form" enctype="multipart/form-data" method="post"
                                        name="send_contact" id="send_contact" action="{{ route('send_contact') }}">
                                        @csrf

                                        <div class="col-md-12">
                                            <label>Your Name</label>
                                            <div class="form-group">
                                                <input type="text" data-validation="[NOTEMPTY]"
                                                    data-validation-message="Please enter your name!" autocomplete="off"
                                                    class="form-control" placeholder="Your Name" name="contactname">
                                            </div>
                                        </div>
                                        <div class="col-md-12">
                                            <label>Your Email</label>
                                            <div class="form-group">
                                                <input type="text" data-validation="[EMAIL]"
                                                    data-validation-message="Invalid email" autocomplete="off"
                                                    class="form-control" placeholder="Your Email" name="contactemail">
                                            </div>
                                        </div>
                                        <div class="col-md-12">
                                            <label>Your Subject</label>
                                            <div class="form-group">
                                                <input type="text" data-validation="[NOTEMPTY]"
                                                    data-validation-message="Please enter a subject!" autocomplete="off"
                                                    class="form-control" placeholder="Your Subject"
                                                    name="contactsubject">
                                            </div>
                                        </div>
                                        <div class="col-md-12">
                                            <label>Your Message</label>
                                            <div class="form-group">
                                                <textarea rows="5" data-validation="[NOTEMPTY]"
                                                    data-validation-message="Please enter a content!" autocomplete="off"
                                                    class="form-control" placeholder="Your Message"
                                                    name="contactcontent"></textarea>
                                            </div>
                                        </div>
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                @if (session('status'))
                                                    <div class="alert alert-success" role="alert">
                                                        <button type="button" class="close"
                                                            data-dismiss="alert">×</button>
                                                        {{ session('status') }}
                                                    </div>
                                                @elseif(session('failed'))
                                                    <div class="alert alert-danger" role="alert">
                                                        <button type="button" class="close"
                                                            data-dismiss="alert">×</button>
                                                        {{ session('failed') }}
                                                    </div>
                                                @endif
                                            </div>
                                        </div>

                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <button type="submit" class="btn btn-primary btn_contact "> Send
                                                    message </button>
                                            </div>
                                        </div>
                                    </form>
                                </div>

                            </div>
                        </div>
                        <div class="col-md-6 col-xs-12">
                            <div class="art-sidebar">
                                <section class="box_map clearfix">
                                    <div class="google-map-wrapper">
                                        <div class="google-map" style="width:100%; height:auto;"><iframe
                                                src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3464.7391505606774!2d-95.57299108550652!3d29.727314340440024!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x8640ddc6ed681049%3A0x74ace03ed50728e8!2sFast+Boy+Marketing!5e0!3m2!1sen!2s!4v1510909605272"
                                                width="100%" height="450" frameborder="0" style="border:0"
                                                allowfullscreen></iframe></div>
                                    </div>
                                </section>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</main>
