<main>

    <div class="pmain">
        <div class="page-heading">
            <h2><span>Gallery</span></h2>
            <img src="{{ asset('frontend') }}/themes/fnail04a/assets/images/bodysugarytop-1.jpg" alt=""
                class="imgrps">
        </div>
        <div class="in-container bg-width">
            <div class="container">
                <div class="in-content">
                    <!-- CONTAINER -->
                    <!-- Gallery page 1 -->
                    <div class="gallery-style-1 bg-fa gallery-inpage ">
                        <div class="cate-gallery hidden-xs">
                            <ul id="filter">
                                @foreach ($albums as $index => $album)
                                    <li itemprop="name">
                                        <a itemprop="{{ $album['id'] }}">{{ $album['name'] }}</a>
                                    </li>
                                @endforeach
                            </ul>
                        </div>
                        <div class="hidden-sm hidden-md hidden-lg hidden-xl">
                            <div class="please-choose">Please choose categories</div>
                            <div class="form-group col-md-12 col-xs-12">
                                <select class="form-control select_tab" autocomplete="off" name="filter_select">
                                    <!-- Category data -->
                                    @foreach ($albums as $index => $album)
                                        <option value="{{ $album['id'] }}">{{ $album['name'] }}</option>
                                    @endforeach

                                    <!-- Category data -->
                                </select>
                            </div>
                        </div>
                        <div class="list-gallery clearfix">
                            <div class="box_list_gallery" id="content">
                            </div>
                        </div>
                    </div>
                    <!-- End Gallery page 1 -->
                    <!--Start paging-->
                    <nav class="text-center box_paging text-center"></nav>
                    <!--End paging-->
                    <script>
                        $(document).on('change', '.select_tab', function() {
                            var tab = $(this).val();
                            $('.tab-pane').removeClass('active');
                            $('#' + tab).addClass('active');
                        });

                    </script> <!-- ban co the render gallery_items hoạc gallery_category -->
                </div>
            </div>
        </div>
    </div>
</main>
