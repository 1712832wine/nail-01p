<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" dir="ltr" lang="en" xmlns:og="http://ogp.me/ns#"
    xmlns:fb="http://www.facebook.com/2008/fbml" itemscope itemtype="https://schema.org/NailSalon">

<head>
    <!-- Language -->
    <meta http-equiv="content-language" content="en" />
    <link rel="alternate" href="http://fnail04a.fastboymedia.com" hreflang="x-default">
    <link rel="alternate" href="http://fnail04a.fastboymedia.com" hreflang="en-US">

    <!-- Meta limit -->

    <!-- Meta Tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width,initial-scale=1.0" />
    <meta http-equiv="content-type" content="text/html; charset=UTF-8" />
    <meta name="description" content="" />
    <meta name="keywords" content="" />
    <meta name="author" content="" />

    <!-- OG -->
    <meta property="og:title" content="" />
    <meta property="og:description" content="" />
    <meta property="og:image" content="" />
    <meta property="og:url" content="http://fnail04a.fastboymedia.com/" />
    <meta property="og:type" content="website" />
    <meta property="og:site_name" content="Fnail04a Nails & Spa " />

    <!-- Dublin Core -->
    <link rel="schema.DC" href="http://purl.org/dc/elements/1.1/">
    <meta name="DC.title" content="">
    <meta name="DC.identifier" content="http://fnail04a.fastboymedia.com/">
    <meta name="DC.description" content="">
    <meta name="DC.subject" content="">
    <meta name="DC.language" scheme="UTF-8" content="en-us">
    <meta itemprop="priceRange" name="priceRange" content="">

    <!-- GEO meta -->
    <meta name="geo.region" content="">
    <meta name="geo.placename" content="">
    <meta name="geo.position" content="">
    <meta name="ICBM" content="">

    <!-- Page Title -->
    <title>Fnail04a Nails & Spa </title>
    <base href="/themes/fnail04a/assets/">

    <!-- canonical -->
    <link rel="canonical" href="http://fnail04a.fastboymedia.com">



    <!-- Favicons -->
    <link rel="icon" href="http://fnail04a.fastboymedia.com/uploads/fnail0qzrr63v/attach/1562752861_fbm_fvc.png"
        type="image/x-icon">
    <link rel="shortcut icon"
        href="http://fnail04a.fastboymedia.com/uploads/fnail0qzrr63v/attach/1562752861_fbm_fvc.png" type="image/x-icon">

    <link href="https://fonts.googleapis.com/css?family=Raleway:300,400,600,700" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Roboto:300,400" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Montserrat:300,400,600" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Parisienne" rel="stylesheet">


    <link rel="stylesheet" type="text/css" href='{{ asset('frontend') }}/public/library/jquery-ui/jquery-ui.min.css'>
    <link rel="stylesheet" type="text/css" href='{{ asset('frontend') }}/public/library/animate/animate.css'>
    <link rel="stylesheet" type="text/css" href='{{ asset('frontend') }}/public/library/validation/validation.css'>
    <link rel="stylesheet" type="text/css" href='{{ asset('frontend') }}/public/library/flex-label/flex-label.css'>
    <link rel="stylesheet" type="text/css"
        href='{{ asset('frontend') }}/public/library/bootstrap/v3.3.7/bootstrap.min.css'>
    <link rel="stylesheet" type="text/css"
        href='{{ asset('frontend') }}/public/library/bootstrap-datetimepicker/bootstrap-datetimepicker.min.css'>
    <link rel="stylesheet" type="text/css"
        href='{{ asset('frontend') }}/public/library/font-awesome/v4.7.0/css/font-awesome.min.css'>
    <link rel="stylesheet" type="text/css"
        href='{{ asset('frontend') }}/public/library/slider-pro/slider-pro.min.css'>
    <link rel="stylesheet" type="text/css" href='{{ asset('frontend') }}/public/library/pnotify/pnotify.css'>
    <link rel="stylesheet" type="text/css"
        href='{{ asset('frontend') }}/public/library/owl-carousel/v2.3.4/owl.carousel.min.css'>
    <link rel="stylesheet" type="text/css"
        href='{{ asset('frontend') }}/public/library/magnific-popup/magnific-popup.css'>
    <link rel="stylesheet" type="text/css" href='{{ asset('frontend') }}/public/library/signature/signature.css'>
    <link rel="stylesheet" type="text/css" href='{{ asset('frontend') }}/public/library/instagram/instafeed.min.css'>
    <link rel="stylesheet" type="text/css" href='{{ asset('frontend') }}/themes/fnail04a/assets/css/meanmenu.css'>

    <link rel="stylesheet" type="text/css"
        href='{{ asset('frontend') }}/themes/fnail04a/assets/css/mediaelementplayer-legacy.min.css'>
    <link rel="stylesheet" type="text/css"
        href='{{ asset('frontend') }}/themes/fnail04a/assets/css/wp-mediaelement.min.css'>
    <link rel="stylesheet" type="text/css"
        href='{{ asset('frontend') }}/themes/fnail04a/assets/css/js_composer.min.css'>
    <link rel="stylesheet" type="text/css"
        href='{{ asset('frontend') }}/themes/fnail04a/assets/css/dropdown-submenu.css'>
    <link rel="stylesheet" type="text/css" href='{{ asset('frontend') }}/themes/fnail04a/assets/css/style.css'>
    <link rel="stylesheet" type="text/css" href='{{ asset('frontend') }}/themes/fnail04a/assets/css/responsive.css'>
    <link rel="stylesheet" type="text/css" href='{{ asset('frontend') }}/public/library/global/css/news.css'>
    <link rel="stylesheet" type="text/css" href='{{ asset('frontend') }}/public/library/global/css/consent.css'>

    <style type="text/css">
        .box_img_giftcard.active,
        .box_img_giftcard:hover {
            border-color: #ff0048;
        }

        .bootstrap-datetimepicker-widget table td.active,
        .bootstrap-datetimepicker-widget table td.active:hover,
        .active .circle_check,
        .info_inner_booking .open_booking:hover,
        a.btn,
        .btn,
        .btn-default,
        a.btn-default,
        .btn-primary,
        a.btn-primary,
        .btn,
        .call-to,
        .call-to .h3,
        .btn-primary {
            border-color: #ff0048;
            background-color: #ff0048;
            color: #ffffff;

        }

        h2,
        h1,
        h3,
        h4,
        h5,
        .title_appointment,
        .color-main,
        .title_gift,
        .list_service optgroup,
        .in-container-service h4,
        .price-item-number,
        .nav-left.stickUp .navbar-nav>li>a:hover,
        .nav-left.nav-white .navbar-nav>li>a:hover,
        .service-name {
            color: #ff0048;
        }

        .modal_form_header h4 {
            color: #ffffff;
        }

        .list_service optgroup option {
            color: #040404;
        }

        .circle-service-image {
            border: 3px solid #ff0048;
        }

        .box_account_v1 .modal_form_header {
            border-bottom: 1px solid #ff0048;
            background-color: #ff0048;
            color: #ffffff;
        }

        .pagination>li>a:focus,
        .pagination>li>a:hover,
        .pagination>li>span:focus,
        .pagination>li>span:hover,
        .pagination>.active>a,
        .pagination>.active>a:focus,
        .pagination>.active>a:hover,
        .pagination>.active>span,
        .pagination>.active>span:focus,
        .pagination>.active>span:hover {
            color: #ffffff;
            background-color: #ff0048;
            border-color: #ff0048;
        }

        .block_cart .form-control {
            border: 1px solid #ff0048;
        }

        /*
    mau sác phụ
     */

        .footer {
            border-color: #1f1f1f;
            background-color: #1f1f1f;
            color: #ffffff;
        }

        .footer-main h5,
        .footer-nav li a,
        .footer-social a {
            color: #ffffff;
        }

        .btn-primary:hover,
        .btn:hover,

        .btn-primary.active.focus,
        .btn-primary.active:focus,
        .btn-primary.active:hover,
        .btn-primary:active.focus,
        .btn-primary:active:focus,
        .btn-primary:active:hover,
        .open>.dropdown-toggle.btn-primary.focus,
        .open>.dropdown-toggle.btn-primary:focus,
        .open>.dropdown-toggle.btn-primary:hover {
            border-color: #1f1f1f;
            background-color: #1f1f1f;
            color: #ffffff;
        }

        ul#filter li.active a {
            background-color: #ff0048;
            color: #ffffff;
        }

    </style>
    <link rel="stylesheet" type="text/css"
        href='{{ asset('frontend') }}/themes/fnail04a/assets/css/custom.css?v=1.3'>
    <style type="text/css">
        .about_img img {
            float: left;
            padding-right: 15px;
            max-width: 50%;
        }

        @media only screen and (max-width: 687px) {
            .about_img img {
                max-width: 100%;
                width: 100%;
                text-align: center;
                float: none;
                padding-right: 0px;
            }
        }

        .about_img h2 {
            font-size: 20px;
        }

        .footer-copyright p {
            margin-bottom: 0px;
        }

        .fa-phone:before {
            margin-right: 10px;
        }

        .section-about h2 {
            font-size: 22px;
        }

        @media only screen and (max-width: 687px) {
            .section-about h2 {
                font-size: 17px;
                margin-top: 0px;
            }
        }

        .call-to {
            padding: 10px 10px;
        }

        hr {
            border-top: 1px solid #ff0048;
        }

        .nav-left {
            box-shadow: 0 -1px 5px rgba(0, 0, 0, .06);
        }

        .footer-main {
            padding: 70px 0;
        }

        .circle-service-image {
            background-size: cover;
        }

        .section-about h2 {
            padding-top: 20px;
            margin-bottom: 20px;
        }

        @media (max-width: 687px) {
            .call-to h3 {
                margin-bottom: 0px;
            }
        }

        .art-main {
            overflow: hidden;
        }

        .mean-container a.meanmenu-reveal {
            z-index: 9999;
        }

        .mobile_logo {
            margin-left: 50px;
        }

        .booking_staff_title {
            display: none;
        }

        .mobile_logo a {
            display: initial;
        }

        .img-info-staff {
            display: none !important;
        }

        .title-staff {
            width: 100%;
        }

        .mobile_logo,
        .mobile_logo a {
            display: initial;
        }

        /*Css-Menu*/
        .mean-container .mean-bar {
            top: 0;
        }

        .mean-container a.meanmenu-reveal {
            margin: 0;
            position: absolute;
            right: 0;
            left: auto;
        }

        .mean-container .mean-nav {
            margin-top: 80px;
        }

        .mobile_logo {
            margin: 0 50px 0 0;
        }

        /*END-Css-Menu*/
        .detail-price-name {
            color: #000;
            font-weight: 600;
        }

        .bg-width {
            background-color: #fff;
        }

    </style>

    <!-- Go to www.addthis.com/dashboard to customize your tools -->
    <script type="text/javascript" src="/s7.addthis.com/js/300/addthis_widget.js#pubid=ra-59b8fec5dd14e0f2"></script>
    <script type="text/javascript">
        var addthis_share = {
            url: "http://fnail04a.fastboymedia.com/",
            title: "Fnail04a Nails & Spa ",
            description: "",
            media: ""
        }

    </script>
    <!-- Script -->

    <script type="text/javascript" src="{{ asset('frontend') }}/public/library/jquery/v1.10.2/jquery.min.js">
    </script>
    <script type="text/javascript" src="{{ asset('frontend') }}/public/library/jquery-ui/jquery-ui.min.js"></script>
    <script type="text/javascript" src="{{ asset('frontend') }}/public/library/jquery-ui/jquery-migrate.min.js">
    </script>
    <script type="text/javascript" src="{{ asset('frontend') }}/public/library/validation/validation.min.js">
    </script>
    <script type="text/javascript" src="{{ asset('frontend') }}/public/library/flex-label/flex-label.js"></script>
    <script type="text/javascript" src="{{ asset('frontend') }}/public/library/mask/mask.min.js"></script>
    <script type="text/javascript" src="{{ asset('frontend') }}/public/library/bootstrap/v3.3.7/bootstrap.min.js">
    </script>
    <script type="text/javascript" src="{{ asset('frontend') }}/public/library/moment/moment-with-locales.js">
    </script>
    <script type="text/javascript"
        src="{{ asset('frontend') }}/public/library/bootstrap-datetimepicker/bootstrap-datetimepicker.js"></script>
    <script type="text/javascript" src="{{ asset('frontend') }}/public/library/magnific-popup/magnific-popup.min.js">
    </script>
    <script type="text/javascript"
        src="{{ asset('frontend') }}/public/library/owl-carousel/v2.3.4/owl.carousel.min.js"></script>
    <script type="text/javascript" src="{{ asset('frontend') }}/public/library/slider-pro/slider-pro.min.js">
    </script>
    <script type="text/javascript" src="{{ asset('frontend') }}/public/library/pnotify/pnotify.js"></script>
    <script type="text/javascript" src="{{ asset('frontend') }}/public/library/meanmenu/meanmenu.js"></script>
    <script type="text/javascript" src="{{ asset('frontend') }}/public/library/instagram/instafeed.min.js"></script>
    <script type="text/javascript" src="{{ asset('frontend') }}/public/library/signature/signature.min.js"></script>
    <script type="text/javascript" src="{{ asset('frontend') }}/public/library/global/js/js_php.js"></script>

    <script type="text/javascript">
        var dateFormatBooking = "MM/DD/YYYY";
        var posFormat = "1,0,2";
        var timeMorning = '["10:00","10:30","11:00","11:30"]';
        var timeAfternoon =
            '["12:00","12:30","13:00","13:30","14:00","14:30","15:00","15:30","16:00","16:30","17:00","17:30","18:00","18:30","19:00"]';
        var bookLogin = "open_booking";
        var beforeTime = "1";
        var beforeDay = 0;
        var currDateT = "2021/05/24";
        var checktimebooking = 1;
        var enableRecaptcha = 0;
        var hoursTimeFormat = 12;
        var phoneFormat = "(000) 000-0000";
        var company_phone = "832-968-6668";
        var num_paging = "12";
        var site = "idx";

        let facebook_embed = {
            id_fanpage: "https://www.facebook.com/FastboyMarketingAgency",
            appId: "",
            width: 450,
            height: 300,
            tabs: "timeline",
            show_facepile: true,
            small_header: true,
            likebox_enable: true,
            likebox_show_faces: false,
            likebox_stream: true,
        };
        let google_id_fanpage = "";
        let twitter_id_fanpage = "";

    </script>

    <!-- Webmaster tools -->
    <!--Webmaster tools-->
    <!-- fb retargeting -->

    <!-- Schema Script -->
    <meta name="csrf-token" content="{{ csrf_token() }}" />
</head>

<body class="style2">
    <div class="art-main">
        <!-- Facebook Root And H1 Seo -->
        <h1 style="display: none"></h1>
        @php
            $album = App\Models\Album::where([['type', 'Social'], ['status', 'PUBLISHED']])->first();
            $header = App\Models\Product::where('category_id', App\Models\Category::where('name', 'Header And Footer')->first()['id'])->first();
            $social = App\Models\Asset::where('albums_id', $album['id'])->orderBy('priority', 'desc')->get();
        @endphp
        @include('frontend.nail04-a.component.header',['header'=>$header,'social'=>$social])
        {{ $slot }}

        @include('frontend.nail04-a.component.footer',['header'=>$header])

        <!-- external javascripts -->
        <script type="text/javascript" src="{{ asset('frontend') }}/public/library/global/js/consent.js"></script>
        <script type="text/javascript" src="{{ asset('frontend') }}/themes/fnail04a/assets/js/script.js?v=1.2">
        </script>
        <script type="text/javascript" src="{{ asset('frontend') }}/themes/fnail04a/assets/js/app.js?v=1.2"></script>

        <!-- Google analytics -->
        <script>
            (function(i, s, o, g, r, a, m) {
                i['GoogleAnalyticsObject'] = r;
                i[r] = i[r] || function() {
                    (i[r].q = i[r].q || []).push(arguments)
                }, i[r].l = 1 * new Date();
                a = s.createElement(o),
                    m = s.getElementsByTagName(o)[0];
                a.async = 1;
                a.src = g;
                m.parentNode.insertBefore(a, m)
            })(window, document, 'script', 'https://www.google-analytics.com/analytics.js', 'ga');
            ga('create', '', 'auto');
            ga('send', 'pageview');

        </script>

        <!-- gg adwords remarketing -->

        <!-- JS extends -->

        <script type="text/javascript"></script>
        <script>
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

        </script>
        @livewireScripts
    </div>
    <!-- End content -->
</body>

</html>
