<main>


    <div class="pmain">
        {{-- CAROUSEL --}}
        <section class="section section-slider">
            <div class="slider-pro" id="my-slider">
                <div class="sp-slides">
                    @foreach ($imgs_carousel as $img)
                        <div class="sp-slide">
                            <div class="sp-layer sp-static">
                                <a href="/">
                                    <img class="sp-image" src="{{ asset('storage') }}/photos/{{ $img['url'] }}"
                                        alt="" />
                                </a>
                            </div>
                        </div>
                    @endforeach
                </div>
            </div>
        </section>
        {{-- HOME ARTICLE --}}
        <section class="section section-about bg-width">
            <div class="container">
                <div class="row">
                    <div class="col-sm-6">
                        <h2>
                            @if (count(json_decode($home_article['image'])) > 0)
                                <img src="{{ asset('storage') }}/photos/{{ json_decode($home_article['image'])[0] }}"
                                    alt="" width="260" height="100" />
                            @endif<br />
                            {{ $home_article['title'] }}
                        </h2>
                        {!! $home_article['content'] !!}
                        <a itemprop="url" href="/services" class="btn">Our Services</a>
                    </div>
                    <div class="col-sm-6">
                        @if (count(json_decode($home_article['image'])) > 1)
                            <img src="{{ asset('storage') }}/photos/{{ json_decode($home_article['image'])[1] }}"
                                class="imgrps" caption="false" />
                        @endif
                    </div>
                </div>
            </div>
        </section>
        {{-- SERVICES --}}
        <section class="section section-gallery">
            <div class="section-service" style="padding: 20px 0px 20px;">
                <h2 class="section">Our Services</h2>
            </div>
            <div class="service_home">
                <div class="owl-cus-service owl-carousel owl-theme">
                    @foreach ($services as $service)
                        <div class="item">
                            <a class="thumb"
                                href="{{ route('services-detail', ['slug' => strtolower(trim(preg_replace('/[^A-Za-z0-9-]+/', '-', $service['name']))), 'service_id' => $service['id']]) }}"
                                itemprop="url">
                                <img src="{{ asset('storage') }}/photos/{{ json_decode($service['images'])[0] }}"
                                    alt="" class="imgrps" itemprop="image">
                            </a>
                            <div class="sv-info">
                                <div class="sv-conten">
                                    <h3 class="sv-name"> <a class="thumb"
                                            href="{{ route('services-detail', ['slug' => strtolower(trim(preg_replace('/[^A-Za-z0-9-]+/', '-', $service['name']))), 'service_id' => $service['id']]) }}"
                                            itemprop="url">{{ $service['name'] }}</a></h3>
                                </div>
                            </div>
                        </div>
                    @endforeach
                </div>
            </div>
            <div class="call-to">
                <h3>Professional Nails Spa</h3>
                <div class="btn-right"><a href="/service.html" class="btn btn-white">View Our Services</a></div>
            </div>
        </section>
        <section class="section section-client">
            <div class="container">
                <h2>WHAT OUR CLIENTS SAY</h2>
                <div class="list-client owl-carousel">
                    @foreach ($carousel_customers as $item)
                        <div class="item">
                            {!! $item['content'] !!}
                            <p class="name">{{ $item['title'] }}</p>
                        </div>
                    @endforeach
                </div>
            </div>
        </section>
    </div>
</main>
