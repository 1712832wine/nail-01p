<main>

    <section class="p-coupons">
        <!-- tpl main -->
        <div class="pmain">
            <div class="page-heading">
                <h2><span>Coupons</span></h2>
                <img src="{{ asset('frontend') }}/themes/fnail04a/assets/images/bodysugarytop-1.jpg" alt=""
                    class="imgrps">
            </div>
            <div class="in-container bg-width clearfix">
                <div class="container">
                    <div class="in-content">
                        @foreach ($list as $item)
                            <div class="col-sm-6 col-md-6 cards-item ">
                                <a class="coupon_img_v1 pointer  image-magnific-popup" data-group=""
                                    href="{{ asset('storage') }}/photos/{{ $item['url'] }}" title="Coupon">
                                    <img class="cards-item-image img-responsive"
                                        src="{{ asset('storage') }}/photos/{{ $item['url'] }}" alt="Coupon">
                                </a>
                            </div>
                        @endforeach
                        {{-- <div class="row">


                            <div class="col-sm-6 col-md-6 cards-item ">
                                <a class="coupon_img_v1 pointer  image-magnific-popup" data-group=""
                                    href="http://fnail04a.fastboymedia.com/uploads/fnail0qzrr63v/coupon/1553158679_img_coupon1553158679.jpeg"
                                    title="Coupon">
                                    <img class="cards-item-image img-responsive"
                                        src="http://fnail04a.fastboymedia.com/uploads/fnail0qzrr63v/coupon/thumbnail/1553158679_img_coupon1553158679-w550.jpeg"
                                        alt="Coupon">
                                </a>
                            </div>

                        </div>
                        <div class="row">
                            <div class="col-sm-6 col-md-6 cards-item ">
                                <a class="coupon_img_v1 pointer  image-magnific-popup" data-group=""
                                    href="http://fnail04a.fastboymedia.com/uploads/fnail0qzrr63v/coupon/1553158674_img_coupon1553158674.jpeg"
                                    title="Coupon">
                                    <img class="cards-item-image img-responsive"
                                        src="http://fnail04a.fastboymedia.com/uploads/fnail0qzrr63v/coupon/thumbnail/1553158674_img_coupon1553158674-w550.jpeg"
                                        alt="Coupon">
                                </a>
                            </div>

                            <div class="col-sm-6 col-md-6 cards-item ">
                                <a class="coupon_img_v1 pointer  image-magnific-popup" data-group=""
                                    href="http://fnail04a.fastboymedia.com/uploads/fnail0qzrr63v/coupon/1553158669_img_coupon1553158669.jpeg"
                                    title="Coupon">
                                    <img class="cards-item-image img-responsive"
                                        src="http://fnail04a.fastboymedia.com/uploads/fnail0qzrr63v/coupon/thumbnail/1553158669_img_coupon1553158669-w550.jpeg"
                                        alt="Coupon">
                                </a>
                            </div>

                        </div> --}}
                    </div>
                </div>
            </div>
        </div>
    </section>
</main>
