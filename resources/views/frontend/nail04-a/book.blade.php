<main>

    <section class="p-book">
        <!-- tpl main -->
        <div class="pmain">
            <div class="page-heading">
                <h2><span>Booking</span></h2>
                <img src="{{ asset('frontend') }}/themes/fnail04a/assets/images/bodysugarytop-1.jpg" alt=""
                    class="imgrps">
            </div>
            <div class="in-container bg-width">
                <div class="container">
                    <div class="in-content">
                        <script>
                            checktimebooking = 0;

                        </script>

                        <div class="row">
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <div class="content-shop bg-ctn-special">
                                    <div class="content-shop-booking">
                                        <script src='https://www.google.com/recaptcha/api.js?hl=en' async defer>
                                        </script><!-- Google reCaptcha -->
                                        <script type="text/javascript">
                                            function ezyCaptcha_surveyForm(token, is_submit) {
                                                is_submit = 1;
                                                if ($("#password").length) {
                                                    //$("input:password").val(md5(clean_input($("#password").val())));
                                                }
                                                return true;
                                            }

                                        </script>
                                        <form enctype="multipart/form-data" id="surveyForm" method="post"
                                            action="/book/add" class="form-horizontal">
                                            <div class="item-booking">
                                                <div class="clearfix" id="optionTemplate">
                                                    <div class="row">
                                                        <div class="col-lg-6 col-md-6">
                                                            <div class="group-select">
                                                                <label>Service(required)</label>
                                                                <div
                                                                    style="display: inline-block; position: relative; width: 100%;">
                                                                    <select name="product_id[]"
                                                                        class="list_service form-control"
                                                                        data-validation-message="Please choose service">
                                                                        <option value="" price="" staff="[]">
                                                                            Select Service</option>
                                                                        <!--List Categories-->
                                                                        <optgroup label="Pink & White Powder Nails">
                                                                            <!--List service-->
                                                                            <option value="193" price="$50" staff='[]'>
                                                                                Full Set ($50 &amp;
                                                                                up) </option>
                                                                            <option value="286" price="$65" staff='[]'>
                                                                                Full Set - Pink &amp;
                                                                                White with Ombre ($65) </option>
                                                                            <option value="194" price="$45" staff='[]'>
                                                                                Fill-in, Pink &amp;
                                                                                White ($45 &amp; up) </option>
                                                                            <option value="195" price="$30" staff='[]'>
                                                                                Fill-in, Pink only
                                                                                ($30 & up) </option>
                                                                            <option value="287" price="$50" staff='[]'>
                                                                                Fill-in Pink &amp;
                                                                                White with Ombre ($50) </option>
                                                                        </optgroup>
                                                                        <optgroup label="Gel Powder Nails">
                                                                            <!--List service-->
                                                                            <option value="198" price="$40" staff='[]'>
                                                                                Full Set ($40 & up)
                                                                            </option>
                                                                            <option value="199" price="$50" staff='[]'>
                                                                                Full Set with No-Chip
                                                                                Polish ($50) </option>
                                                                            <option value="200" price="$28" staff='[]'>
                                                                                Fill-in ($28 & up)
                                                                            </option>
                                                                            <option value="201" price="$40" staff='[]'>
                                                                                Fill in with No-Chip
                                                                                Polish ($40) </option>
                                                                        </optgroup>
                                                                        <optgroup label="Acrylic Powder Nails">
                                                                            <!--List service-->
                                                                            <option value="202" price="$32" staff='[]'>
                                                                                Full Set ($32 & up)
                                                                            </option>
                                                                            <option value="203" price="$45" staff='[]'>
                                                                                Full Set with No-Chip
                                                                                Polish ($45) </option>
                                                                            <option value="204" price="$23" staff='[]'>
                                                                                Fill-in ($23 & up)
                                                                            </option>
                                                                            <option value="205" price="$35" staff='[]'>
                                                                                Fill in with No-Chip
                                                                                Polish ($35) </option>
                                                                        </optgroup>
                                                                        <optgroup label="Extra Services">
                                                                            <!--List service-->
                                                                            <option value="206" price="$3" staff='[]'>
                                                                                Nail Repair ($3 & up)
                                                                            </option>
                                                                            <option value="207" price="$5" staff='[]'>
                                                                                White Tips ($5)
                                                                            </option>
                                                                            <option value="208" price="$5" staff='[]'>
                                                                                Cuticles, Soak & Cut
                                                                                ($5) </option>
                                                                            <option value="209" price="$10" staff='[]'>
                                                                                Artificial Nail
                                                                                Take-off ($10) </option>
                                                                            <option value="210" price="$5" staff='[]'>
                                                                                Additional Length ($5
                                                                                & up) </option>
                                                                            <option value="267" price="$5" staff='[]'>
                                                                                Advanced Shaping ($5
                                                                                &amp; up) </option>
                                                                        </optgroup>
                                                                        <optgroup label="Natural Nail Services">
                                                                            <!--List service-->
                                                                            <option value="211" price="$18" staff='[]'>
                                                                                Spa Manicure ($18)
                                                                            </option>
                                                                            <option value="212" price="$28" staff='[]'>
                                                                                Collagen Manicure
                                                                                ($28) </option>
                                                                            <option value="213" price="$35" staff='[]'>
                                                                                No-Chip - Spa
                                                                                Manicure ($35) </option>
                                                                            <option value="260" price="$42" staff='[]'>
                                                                                Dip Powder ($42)
                                                                            </option>
                                                                        </optgroup>
                                                                        <optgroup label="Teenager ">
                                                                            <!--List service-->
                                                                            <option value="214" price="$15" staff='[]'>
                                                                                Manicure ($15)
                                                                            </option>
                                                                            <option value="215" price="$27" staff='[]'>
                                                                                Pedicure ($27)
                                                                            </option>
                                                                        </optgroup>
                                                                        <optgroup label="Little Princess">
                                                                            <!--List service-->
                                                                            <option value="216" price="$12" staff='[]'>
                                                                                Manicure ($12)
                                                                            </option>
                                                                            <option value="217" price="$20" staff='[]'>
                                                                                Pedicure ($20)
                                                                            </option>
                                                                            <option value="218" price="$5" staff='[]'>
                                                                                Polish Change-Hands
                                                                                ($5) </option>
                                                                            <option value="219" price="$8" staff='[]'>
                                                                                Polish Change-Feet
                                                                                ($8) </option>
                                                                        </optgroup>
                                                                        <optgroup label="Manicure & Pedicure Combo ">
                                                                            <!--List service-->
                                                                            <option value="220" price="$42" staff='[]'>
                                                                                Ladies Combo ($42)
                                                                            </option>
                                                                            <option value="221" price="$45" staff='[]'>
                                                                                Gentlemens Combo
                                                                                ($45) </option>
                                                                            <option value="222" price="$37" staff='[]'>
                                                                                Teenager Combo ($37)
                                                                            </option>
                                                                            <option value="223" price="$32" staff='[]'>
                                                                                Little Princess Combo
                                                                                ($32) </option>
                                                                        </optgroup>
                                                                        <optgroup label="Waxing">
                                                                            <!--List service-->
                                                                            <option value="224" price="$10" staff='[]'>
                                                                                Brow Clean up ($10 &
                                                                                up) </option>
                                                                            <option value="225" price="$15" staff='[]'>
                                                                                Eyebrow Shape ($15 &
                                                                                up) </option>
                                                                            <option value="226" price="$8" staff='[]'>
                                                                                Upper Lip ($8)
                                                                            </option>
                                                                            <option value="227" price="$16" staff='[]'>
                                                                                Brow Clean up + Lip
                                                                                ($16 & up) </option>
                                                                            <option value="228" price="$12" staff='[]'>
                                                                                Chin ($12) </option>
                                                                            <option value="229" price="$15" staff='[]'>
                                                                                Cheek ($15) </option>
                                                                            <option value="230" price="$25" staff='[]'>
                                                                                Brow Clean Up + Lip +
                                                                                Chin ($25) </option>
                                                                            <option value="231" price="$40" staff='[]'>
                                                                                Full Face ($40)
                                                                            </option>
                                                                            <option value="232" price="$10" staff='[]'>
                                                                                Hairline ($10)
                                                                            </option>
                                                                            <option value="233" price="$8" staff='[]'>
                                                                                Sideburn Clean up
                                                                                ($8) </option>
                                                                            <option value="234" price="$22" staff='[]'>
                                                                                Chin & Cheek ($22)
                                                                            </option>
                                                                            <option value="235" price="$30" staff='[]'>
                                                                                Half Arm ($30 & up)
                                                                            </option>
                                                                            <option value="236" price="$40" staff='[]'>
                                                                                Full Arm ($40 & up)
                                                                            </option>
                                                                            <option value="237" price="$20" staff='[]'>
                                                                                Under Arm ($20)
                                                                            </option>
                                                                            <option value="238" price="$35" staff='[]'>
                                                                                Half Leg ($35 & up)
                                                                            </option>
                                                                            <option value="239" price="$60" staff='[]'>
                                                                                Full Leg ($60 & up)
                                                                            </option>
                                                                            <option value="240" price="$30" staff='[]'>
                                                                                Bikini ($30 & up)
                                                                            </option>
                                                                            <option value="241" price="$75" staff='[]'>
                                                                                Full Leg & Bikini
                                                                                ($75 & up) </option>
                                                                            <option value="242" price="$55" staff='[]'>
                                                                                Brazilian ($55 & up)
                                                                            </option>
                                                                            <option value="243" price="$15" staff='[]'>
                                                                                Neck ($15) </option>
                                                                            <option value="244" price="$20" staff='[]'>
                                                                                Stomach ($20 & up)
                                                                            </option>
                                                                            <option value="245" price="$50" staff='[]'>
                                                                                Back ($50 & up)
                                                                            </option>
                                                                            <option value="246" price="$40" staff='[]'>
                                                                                Chest ($40 & up)
                                                                            </option>
                                                                        </optgroup>
                                                                        <optgroup label="Miscellaneous Services">
                                                                            <!--List service-->
                                                                            <option value="247" price="$4" staff='[]'>
                                                                                Chrome Powder ($4 per
                                                                                nail) </option>
                                                                            <option value="248" price="$5" staff='[]'>
                                                                                Shiny Buffer ($5 per
                                                                                service) </option>
                                                                            <option value="249" price="$5" staff='[]'>
                                                                                French Polish ($5 per
                                                                                service) </option>
                                                                            <option value="250" price="$10" staff='[]'>
                                                                                Polish Change Hands
                                                                                ($10) </option>
                                                                            <option value="251" price="$14" staff='[]'>
                                                                                Polish Change Hands,
                                                                                French ($14) </option>
                                                                            <option value="252" price="$12" staff='[]'>
                                                                                Polish Change Feet
                                                                                ($12) </option>
                                                                            <option value="253" price="$16" staff='[]'>
                                                                                Polish Change Feet,
                                                                                French ($16) </option>
                                                                        </optgroup>
                                                                        <optgroup label="Pedicures">
                                                                            <!--List service-->
                                                                            <option value="254" price="$30" staff='[]'>
                                                                                Spa Pedicure ($30)
                                                                            </option>
                                                                            <option value="255" price="$47" staff='[]'>
                                                                                No-chip, Spa Pedicure
                                                                                ($47) </option>
                                                                            <option value="256" price="$40" staff='[]'>
                                                                                Deluxe Pedicure ($40)
                                                                            </option>
                                                                            <option value="257" price="$50" staff='[]'>
                                                                                Pedi-in-a-box ($50)
                                                                            </option>
                                                                            <option value="258" price="$45" staff='[]'>
                                                                                Lavender & Jojoba
                                                                                ($45) </option>
                                                                            <option value="259" price="$65" staff='[]'>
                                                                                Volcano Pedicure
                                                                                ($65) </option>
                                                                            <option value="265" price="$55" staff='[]'>
                                                                                Milk & Honey Pedicure
                                                                                ($55) </option>
                                                                            <option value="266" price="$70" staff='[]'>
                                                                                Marine Pedicure ($70)
                                                                            </option>
                                                                        </optgroup>
                                                                        <optgroup label="Gentlemen’s Rates">
                                                                            <!--List service-->
                                                                            <option value="261" price="$18" staff='[]'>
                                                                                Spa Manicure ($18)
                                                                            </option>
                                                                            <option value="262" price="$30" staff='[]'>
                                                                                Spa Pedicure ($30)
                                                                            </option>
                                                                            <option value="263" price="$15" staff='[]'>
                                                                                Eyebrow Wax ($15)
                                                                            </option>
                                                                            <option value="264" price="$5" staff='[]'>
                                                                                Shiny Buffer ($5 per
                                                                                service) </option>
                                                                            <option value="268" price="$45" staff='[]'>
                                                                                Spa Mani & Pedi ($45)
                                                                            </option>
                                                                        </optgroup>
                                                                        <optgroup label="Extra Service">
                                                                            <!--List service-->
                                                                            <option value="270" price="$5" staff='[]'>
                                                                                Freehand Design or
                                                                                Decals on Two Nails ($5 & up)
                                                                            </option>
                                                                            <option value="271" price="$10" staff='[]'>
                                                                                Freehand Design or
                                                                                Decals on Ten Nails ($10 & up)
                                                                            </option>
                                                                            <option value="272" price="$4" staff='[]'>
                                                                                Chrome Powder ($4 per
                                                                                nail) </option>
                                                                            <option value="273" price="$5" staff='[]'>
                                                                                Shiny Buffer ($5 per
                                                                                service) </option>
                                                                            <option value="274" price="$5" staff='[]'>
                                                                                French Polish ($5 per
                                                                                service) </option>
                                                                            <option value="275" price="$10" staff='[]'>
                                                                                Polish Change Hands
                                                                                ($10) </option>
                                                                            <option value="276" price="$14" staff='[]'>
                                                                                Polish Change Hands,
                                                                                French ($14) </option>
                                                                            <option value="277" price="$14" staff='[]'>
                                                                                Polish Change Feet
                                                                                ($14) </option>
                                                                            <option value="278" price="$18" staff='[]'>
                                                                                Polish Change Feet,
                                                                                French ($18) </option>
                                                                            <option value="279" price="$10" staff='[]'>
                                                                                Paraffin Wax, Hands
                                                                                ($10) </option>
                                                                            <option value="280" price="$10" staff='[]'>
                                                                                No-chip Take Off,
                                                                                Hands ($10) </option>
                                                                        </optgroup>
                                                                        <optgroup label="Extra Services For The Feet">
                                                                            <!--List service-->
                                                                            <option value="281" price="$5" staff='[]'>
                                                                                Heavy Callus Removal
                                                                                ($5) </option>
                                                                            <option value="282" price="$5" staff='[]'>
                                                                                Extra Leg Massage (5
                                                                                mins) ($5) </option>
                                                                            <option value="283" price="$15" staff='[]'>
                                                                                No-chip Take-off,
                                                                                Feet ($15) </option>
                                                                            <option value="284" price="$12" staff='[]'>
                                                                                Paraffin Wax, Feet
                                                                                ($12) </option>
                                                                            <option value="285" price="$8" staff='[]'>
                                                                                Acrylic on Toes ($8
                                                                                per nail) </option>
                                                                        </optgroup>
                                                                    </select>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-lg-6 col-md-6">
                                                            <div class="group-select">
                                                                <label>Technician (optional)</label>
                                                                <select name="staff_id[]"
                                                                    class="list_staff form-control">
                                                                    <option value="">Select technician</option>
                                                                </select>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-lg-6 col-md-6 col-xs-12 ">
                                                        <div class="group-select box-date">
                                                            <label>Date</label>
                                                            <input type="text" name="booking_date"
                                                                id="datetimepicker_v1"
                                                                class="form-control choose_date booking_date"
                                                                placeholder="Choice a date" value="05/24/2021"
                                                                typehtml="html">
                                                            <span class="fa fa-calendar"></span>
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-6 col-md-6 col-xs-12" id="btn_search_booking">
                                                        <label>&nbsp;</label>
                                                        <button class='btn btn-search btn_action title booking_search'
                                                            type='button'>Search</button>
                                                    </div>
                                                </div>
                                                <script>
                                                    var btn_search_booking = document.getElementById(
                                                        "btn_search_booking").getElementsByTagName("button")[0];
                                                    btn_search_booking.className +=
                                                        " btn-search btn-primary btn-book-search";

                                                </script>
                                                <input type="hidden" name="booking_hours" value="" />
                                                <input type="hidden" name="booking_area_code" value="" />
                                                <input type="hidden" name="booking_phone" value="" />
                                                <input type="hidden" name="booking_name" value="" />
                                                <input type="hidden" name="store_id" value="0" />
                                                <input type="hidden" name="nocaptcha" value="1" />
                                                <input type="hidden" name="g-recaptcha-response" value="" />
                                                <input type="hidden" name="notelist" value="" />
                                            </div>
                                            <div class="add-services addButton">
                                                <img src="/public/library/global/add-service-icon-new.png"> Add
                                                Another service
                                            </div>
                                        </form>
                                        <script type="text/javascript">
                                            $(document).ready(function() {
                                                var formbk =
                                                    '{"listperson":[],"notelist":[],"booking_date":"05\/24\/2021","booking_hours":""}';
                                                loadEvent();
                                                $('#btn_search_booking .btn-booking').addClass('btn-primary');
                                            });

                                        </script>
                                    </div>
                                </div>
                            </div>
                        </div>


                        <section class="box_staff_inner_v1 bg-gray box_detail_info" style="display: none;">
                            <div class="row">
                                <div class="col-md-12 col-xs-12">
                                    <div class="booking_staff_title" id="book-info">
                                        <h2 class="section_title" itemprop="name">Staff Us</h2>
                                        <p class="staff_us" itemprop="name">THE ASSETS OF OUR FAMILY</p>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12  col-xs-12">
                                    <div class="box_staff_bg">
                                        <div class="info_inner_booking">
                                            <div class="row" id="box_person"></div>
                                            <div class="row">
                                                <div class="time_work_staff_v1 col-md-12 databooktime">
                                                    <h3 class="time_show"></h3>
                                                    <div class="time_am_v1">
                                                        <label>Morning <span class="note_am_time"
                                                                style="color: red; font-style: italic;"></span></label>
                                                        <ul class="timemorning">

                                                        </ul>
                                                    </div>
                                                    <div class="time_pm_v1">
                                                        <label>Afternoon <span class="note_pm_time"
                                                                style="color: red; font-style: italic;"></span></label>
                                                        <ul class="timeafternoon">

                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>
                                            <!--POPUP LOGIN-->
                                            <div id="popup_login" class="white-popup mfp-hide">
                                                <div class="box_account_v1">
                                                    <div class="modal_form_header">
                                                        <h4>Login</h4>
                                                    </div>
                                                    <div class="popup_main_area">
                                                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 border_right">
                                                            <form enctype="multipart/form-data" id="form-login"
                                                                method="POST" action="/login/login_do">
                                                                <fieldset class="form-group">
                                                                    <div class="box_login">
                                                                        <h2> Been here before?</h2>
                                                                        <div class="form_input_1">
                                                                            <div class="form-control-wrapper">
                                                                                <input type="email" name="cus_email"
                                                                                    placeholder="Enter your E-mail"
                                                                                    data-validation="[EMAIL]"
                                                                                    data-validation-message="Email is not valid!">
                                                                            </div>
                                                                        </div>
                                                                        <div class="form_input_2">
                                                                            <div class="form-control-wrapper">
                                                                                <input type="password"
                                                                                    placeholder="Password"
                                                                                    name="cus_password"
                                                                                    data-validation="[NOTEMPTY]"
                                                                                    data-validation-message="Password must not be empty!">
                                                                            </div>
                                                                        </div>
                                                                        <div class="col_psw_v1"
                                                                            style="padding-bottom: 3px; ">

                                                                            <div class="form-control-wrapper"
                                                                                style="margin-top: -10px;padding-bottom: 3px;float:right">
                                                                                <a href="/login/forgot-password/">Forgot
                                                                                    password</a>

                                                                            </div>
                                                                        </div>
                                                                        <div class="btn_submit_login">
                                                                            <button class="submit"
                                                                                type="submit">Login</button>
                                                                        </div>
                                                                    </div>
                                                                </fieldset>
                                                            </form>
                                                        </div>
                                                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                                            <div class="box_register">
                                                                <h2>New Account?</h2>
                                                                <div class="btn_submit_login">
                                                                    <button class="submit" type="submit"
                                                                        onclick="window.location.href='/register/'">Create
                                                                        a Username</button>
                                                                </div>
                                                                <div class="btn_login_social">

                                                                    <a href="" class="btn btn_facebook_v1" href="#"
                                                                        title="facebook login">
                                                                        <i class="fa fa-facebook"></i>
                                                                        <span>Facebook</span>
                                                                    </a>

                                                                    <a class="btn btn_gplus_v1" href=""
                                                                        title="gplus login">
                                                                        <i class="fa fa-google-plus"></i>
                                                                        <span>Google</span>
                                                                    </a>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                            <!--POPUP BOOKING-->
                                            <div id="open_booking" class="white-popup mfp-hide">
                                                <div class="box_account_v1">
                                                    <div class="modal_form_header">
                                                        <h4>Message</h4>
                                                    </div>
                                                    <div class="popup_main_area">
                                                        <form enctype="multipart/form-data" id="booking_check"
                                                            name="booking_check">
                                                            <p style="font-weight: bold; font-size: 18px;">
                                                                Confirm booking information ?</p>
                                                            <span
                                                                style="color: red; font-style: italic; margin: 0 0 10px; display: block;">We
                                                                will send a text message to you via the number
                                                                below after we confirm the calendar for your
                                                                booking!</span>
                                                            <div class="row">
                                                                <div class="col-md-6">
                                                                    <div class="form-group">
                                                                        <label class="form-control-label">Your
                                                                            name</label>
                                                                        <div class="form-control-wrapper">
                                                                            <input name="input_name" type="text"
                                                                                class="form-control" type="text"
                                                                                data-validation="[NOTEMPTY]"
                                                                                data-validation-message="Please enter your name!" />
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="col-md-2" style="display: none;">
                                                                    <div class="form-group">
                                                                        <label class="form-control-label">Area
                                                                            code</label>
                                                                        <select name="area_code" class="form-control"
                                                                            defaultvalue="1">
                                                                            <option value="1">US (+1)</option>
                                                                            <option value="84">VN (+84)</option>
                                                                        </select>
                                                                    </div>
                                                                </div>
                                                                <div class="col-md-6">
                                                                    <div class="form-group">
                                                                        <label class="form-control-label">Phone
                                                                            number</label>
                                                                        <div class="form-control-wrapper">
                                                                            <input name="phone_number" type="text"
                                                                                class="form-control inputPhone"
                                                                                placeholder="Ex: (123) 123-1234"
                                                                                data-validation="[NOTEMPTY]"
                                                                                data-validation-message="Phone number invalid" />
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="col-md-12">
                                                                    <div class="form-group">
                                                                        <label class="form-control-label">Note
                                                                            (Optional)</label>
                                                                        <div class="form-control-wrapper">
                                                                            <textarea name="notelist"
                                                                                placeholder="Max length 200 character"
                                                                                rows="5" class="form-control"
                                                                                maxlength="200"></textarea>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <!-- Store options -->
                                                                <input type="hidden" name="choose_store" value="">
                                                                <!-- End - Store options -->
                                                            </div>
                                                            <button class="btn btn-success btn_confirmed "
                                                                type="button">Confirm</button>
                                                            <button class="btn btn-danger btn-inline btn_cancel"
                                                                type="button">Cancel</button>
                                                        </form>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </section>

                        <script>
                            $(document).ready(function() {
                                $("#form-login").validate({
                                    submit: {
                                        settings: {
                                            button: "[type='submit']",
                                            inputContainer: '.form-group',
                                            errorListClass: 'form-tooltip-error',
                                        }
                                    }
                                });

                                // check Time
                                $('form#booking_check').validate({
                                    submit: {
                                        settings: {
                                            clear: 'keypress',
                                            display: "inline",
                                            button: ".btn_confirmed",
                                            inputContainer: 'form-group',
                                            errorListClass: 'form-tooltip-error',
                                        },
                                        callback: {
                                            onSubmit: function(node, formdata) {
                                                var areacode =
                                                    1; //$("select[name='area_code']").val();
                                                var phone_number = $("input[name='phone_number']")
                                                    .val();
                                                var cus_name = $("input[name='input_name']").val();
                                                var store_id = $("[name='choose_store']").length >
                                                    0 ? $("[name='choose_store']").val() : 0;
                                                $("[name='store_id']").val(store_id);

                                                var notelist = $("textarea[name='notelist']").val();
                                                $("input[name='notelist']").val(notelist);

                                                if (phone_number != "" && phone_number.length > 6) {
                                                    $("input[name='booking_phone']").val(
                                                        phone_number);
                                                    $("input[name='booking_area_code']").val(
                                                        areacode);
                                                }

                                                if (cus_name) {
                                                    $("input[name='booking_name']").val(cus_name);
                                                }

                                                if (enableRecaptcha) {
                                                    var check_google = $("#g-recaptcha-response")
                                                        .val();
                                                    if (typeof(check_google) != "undefined" &&
                                                        check_google != "") {
                                                        $("input[name='g-recaptcha-response']").val(
                                                            check_google);
                                                        $("#surveyForm").submit();
                                                    }
                                                } else {
                                                    $("#surveyForm").submit();
                                                }
                                                return false;
                                            }
                                        }
                                    }
                                });
                            });

                        </script>
                    </div>
                </div>
            </div>
        </div>
    </section>
</main>
