<main>

    <section class="p-about">
        <!-- tpl main -->
        <div class="pmain">
            <div class="page-heading">
                <h2><span>About us</span></h2>
                <img src="{{ asset('frontend') }}/themes/fnail04a/assets/images/bodysugarytop-1.jpg" alt=""
                    class="imgrps">
            </div>
            <div class="in-container bg-width">
                <div class="container">
                    @foreach ($articles as $item)
                        <div class="in-content">
                            <div class="about_img">
                                @if (count(json_decode($item['image'])) > 0)
                                    <img src="{{ asset('storage') }}/photos/{{ json_decode($item['image'])[0] }}"
                                        caption="false" />
                                @endif
                                <h2>{{ $item['title'] }}</h2>
                                {!! $item['content'] !!}
                            </div>
                        </div>
                    @endforeach
                </div>
            </div>



            {{-- BOTTOM --}}
            <div class="call-to">
                <h3>Professional Nails Care for Ladies & Gentlemen</h3>
                <div class="btn-right"><a href="/service.html" class="btn btn-white">View Our Services</a></div>
            </div>
            <section class="section section-client">
                <div class="container">
                    <h2>WHAT OUR CLIENTS SAY</h2>
                    <div class="list-client owl-carousel">
                        @foreach ($carousel_customers as $item)
                            <div class="item">
                                {!! $item['content'] !!}
                                <p class="name">{{ $item['title'] }}</p>
                            </div>
                        @endforeach
                    </div>
                </div>
            </section>
        </div>
    </section>
</main>
