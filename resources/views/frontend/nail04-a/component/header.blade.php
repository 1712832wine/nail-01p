<header class="header">
    <div class="menu_mobile_v1 hidden-md hidden-lg menu-1024-hidden">
        <div class="mobile_logo">

            <a itemprop="url" href="/">
                @if (count(json_decode($header['images'])) > 0)
                    <img class="imgrps" src="{{ asset('storage') }}/photos/{{ json_decode($header['images'])[0] }}"
                        alt="{{ $header['title'] }}" itemprop="logo image">
                @endif
            </a>
        </div>
        <div class="mobile_menu_container_v1">
            <div class="mobile-menu clearfix">
                <nav id="mobile_dropdown">
                    <ul>
                        <li class="left" itemprop="name"><a href="/" itemprop="url">Home</a></li>
                        <li class="left" itemprop="name"><a href="/about" itemprop="url">About Us</a></li>
                        <li class="left" itemprop="name"><a href="/services" itemprop="url">Services</a>
                        </li>
                        <li class="right" itemprop="name"><a itemprop="url" href="/book">Booking</a></li>
                        <li class="right" itemprop="name"><a href="/gallery" itemprop="url">Gallery</a>
                        </li>
                        <li class="right" itemprop="name"><a href="/giftcards" itemprop="url">Gift
                                Cards</a>
                        </li>
                        <li class="right" itemprop="name"><a href="/coupons" itemprop="url">Coupons</a>
                        </li>
                        <li class="right" itemprop="name"><a href="/contact" itemprop="url">Contact Us</a>
                        </li>
                    </ul>
                </nav>
            </div>
        </div>
    </div>
    <div class="nav-left stickUp">
        <h2 class="logo">
            <a itemprop="url" href="/">
                @if (count(json_decode($header['images'])) > 0)
                    <img class="imgrps logo_white"
                        src="{{ asset('storage') }}/photos/{{ json_decode($header['images'])[0] }}"
                        alt="{{ $header['title'] }}" itemprop="logo image">
                    <img class="imgrps logo_black"
                        src="{{ asset('storage') }}/photos/{{ json_decode($header['images'])[0] }}"
                        alt="{{ $header['title'] }}" itemprop="logo image">
                @endif

            </a>
        </h2>
        <nav class="navbar main-nav">
            <div id="desktop-nav" class="navbar-collapse collapse">
                <ul class="nav navbar-nav nav-main">
                    <li class="left" itemprop="name"><a href="/" itemprop="url">Home</a></li>
                    <li class="left" itemprop="name"><a href="/about" itemprop="url">About Us</a></li>
                    <li class="left" itemprop="name"><a href="/services" itemprop="url">Services</a></li>
                    <li class="right" itemprop="name"><a itemprop="url" href="/book">Booking</a></li>
                    <li class="right" itemprop="name"><a href="/gallery" itemprop="url">Gallery</a></li>
                    <li class="right" itemprop="name"><a href="/giftcards" itemprop="url">Gift Cards</a>
                    </li>
                    <li class="right" itemprop="name"><a href="/coupons" itemprop="url">Coupons</a></li>
                    <li class="right" itemprop="name"><a href="/contact" itemprop="url">Contact Us</a></li>
                </ul>
            </div>
        </nav>
        <div class="nav-social">
            @foreach ($social as $item)
                @if ($item['url'])
                    <a itemprop="url" class="social-img-icons" rel="nofollow" target="_blank"
                        href="{{ $item['content'] }}">
                        <img src="{{ asset('storage') }}/photos/{{ $item['url'] }}">
                    </a>
                @endif
            @endforeach
        </div>
    </div>
</header>
