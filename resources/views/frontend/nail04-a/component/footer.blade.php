@php
$features = json_decode($header['features']);
$extras = json_decode($header['extras']);
@endphp
<footer class="footer">
    <div class="footer-main">
        <div class="container">
            <div class="row">
                <div class="col-md-4 col-xs-12">
                    <h5>Fnail04a Nails & Spa </h5>
                    <p style="text-align: justify;"><span>Our salon takes pride in providing our valued
                            customers all good services and top-high quality products as well as
                            materials.</span></p>
                    <ul class="footer_infomaytion">

                        <li>
                            <spam>Address:</spam> {{ $features[0]->desc }}
                        </li>
                        <li>
                            <spam>Email:</spam>{{ $features[1]->desc }}
                        </li>
                        <li>
                            <spam>Phone:</spam>{{ $features[2]->desc }}
                        </li>
                    </ul>
                </div>
                <div class="col-md-4 col-xs-12 ">
                    <h5>Business Hours</h5><!-- support render openhours_data -->
                    <div class="vt-time">
                        @foreach ($extras as $extra)
                            <div class="col-2" itemprop="openingHours" content="{{ $extra->desc }}">
                                <div class="col-lef">{{ $extra->name }}:</div>
                                <div class="col-right">{{ $extra->desc }}</div>
                            </div>
                        @endforeach
                    </div>

                </div>
                <div class="col-md-4 col-xs-12 ">
                    <div class="hidden-md hidden-sm hidden-lg">
                        <div class="nav-social">
                            @foreach ($social as $item)
                                @if ($item['url'])
                                    <a itemprop="url" class="social-img-icons" rel="nofollow" target="_blank"
                                        href="{{ $item['content'] }}">
                                        <img src="{{ asset('storage') }}/photos/{{ $item['url'] }}">
                                    </a>
                                @endif
                            @endforeach
                        </div>
                    </div>
                    <aside>
                        <div id="fanpage_fb_container"></div>
                        <div id="social_block_width" style="width:100% !important; height: 1px !important">
                        </div>
                    </aside>
                </div>
            </div>
        </div>
    </div>
    <div class="footer-copyright">
        <div class="container">
            <p> © Copyright by Fnail04a Nails & Spa . All Rights Reserved.</p>
        </div>
    </div>
</footer>
<ul class="btn_book_home">
    <li>
        <a href="/book" class="btn btn-primary btn_make_appointment">Appointment</a>
    </li>
    <li>
        <a href="tel:{{ explode(',', $features[0]->desc)[0] }}" class="btn btn-call btn-primary">
            <i class="fa fa-phone"></i>Call now
        </a>
    </li>
</ul>
