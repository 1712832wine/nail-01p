<section class="main-wrap clearfix">
    <section class="p-coupons">
        <!-- tpl main -->
        <section class="small-section bg-dark-lighter breadcrumbs coupons-breadcrumb">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <h2 class="hs-line-11 font-alt" itemprop="name">Coupons</h2>
                    </div>
                </div>
            </div>
        </section>
        <section class="page-section pc-coupons">
            <div class="container">
                <div class="row ">
                    @foreach ($list as $item)
                        <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                            <div class="coupon_img_v1 pointer image-magnific-popup" data-group=""
                                href="{{ asset('storage') }}/photos/{{ $item['url'] }}" title="Coupon">
                                <img itemprop="image" class="img-responsive"
                                    src="{{ asset('storage') }}/photos/{{ $item['url'] }}" alt="coupon v1">
                            </div>
                        </div>
                    @endforeach
                </div>
            </div>
        </section>
    </section>
</section>
