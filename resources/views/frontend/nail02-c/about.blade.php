<section class="main-wrap clearfix">
    <section class="p-about">
        <section class="small-section bg-dark-lighter breadcrumbs about-breadcrumb">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <h2 class="hs-line-11 font-alt" itemprop="name">About us</h2>
                    </div>
                </div>
            </div>
        </section>
        <section class="page-section pa-about-us">
            @foreach ($articles as $item)
                <div class="container relative">
                    <div class="section-text">
                        <div itemscope="" itemtype="http://schema.org/AboutPage" class="row">
                            <div class="col-md-4">
                                <blockquote>
                                    <h2 itemprop="name">{{ $header['name'] }}</h2>
                                    <p iemprop="description">{{ $item['title'] }}</p>
                                    <footer iemprop="description">Our Story.</footer>
                                </blockquote>
                                @if (count(json_decode($item['image'])) > 0)
                                    <img src="{{ asset('storage') }}/photos/{{ json_decode($item['image'])[0] }}"
                                        alt="" />
                                @endif
                            </div>
                            <div itemprop="text" class="col-md-8 col-sm-6">
                                <div class="text">
                                    {!! $item['content'] !!}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            @endforeach
        </section>
    </section>
</section>
