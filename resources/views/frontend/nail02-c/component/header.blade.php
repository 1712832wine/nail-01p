<header class="header-wrap clearfix">
    <div class="wrap-freeze-header">
        <div class="flag-freeze-header">
            <!-- tpl header_main -->
            <!-- Active freeze header by insert this html into the website page -->
            <!-- <input type="hidden" name="activeFreezeHeader" value="1">--><input type="hidden"
                name="activeFreezeHeader" value="1" />
            <nav class="main-nav">
                <div class="header-bottom full-wrapper relative clearfix">
                    <div class="nav-logo-wrap local-scroll">
                        @if (count(json_decode($header['images'])) > 0)
                            <a class="logo" href="/">
                                <img src="{{ asset('storage') }}/photos/{{ json_decode($header['images'])[0] }}"
                                    alt="{{ $header['title'] }}" itemprop="logo">
                            </a>
                        @endif

                    </div>
                    <div class="mobile-nav"><i class="fa fa-bars"></i></div>
                    <div class="inner-nav desktop-nav">
                        <ul class="clearlist">
                            <li><a href="/" itemprop="url"><span itemprop="name">Home</span></a></li>
                            <li><a href="/about" itemprop="url"><span itemprop="name">About Us</span></a>
                            </li>
                            <li><a href="/services" itemprop="url"><span itemprop="name">Services</span></a>
                            </li>
                            <li><a href="/coupons" itemprop="url"><span itemprop="name">Coupons</span></a>
                            </li>
                            <li class="btn_make_appointment"><a href="/book" itemprop="url"><span
                                        itemprop="name">Booking</span></a></li>
                            <li><a href="/gallery" itemprop="url"><span itemprop="name">Gallery</span></a>
                            </li>
                            <li><a href="/contact" itemprop="url"><span itemprop="name">Contact
                                        Us</span></a></li>
                        </ul>
                    </div>
                </div>
            </nav>
        </div>
    </div>
</header>
