@php
$features = json_decode($header['features']);
$extras = json_decode($header['extras']);
@endphp
<footer class="foot-wrap footer clearfix">
    <div class="container">
        <div class="row">
            <div class="col-md-3 col-sm-12">
                <div class="footer-logo">
                    @if (count(json_decode($header['images'])) > 0)
                        <a class="logo" href="/">
                            <img src="{{ asset('storage') }}/photos/{{ json_decode($header['images'])[0] }}"
                                alt="{{ $header['title'] }}" itemprop="logo">
                        </a>
                    @endif
                </div>
                <div class="footer-about"></div>
            </div>
            <div class="col-md-3 col-sm-6">
                <div class="footer_contact">
                    <h4 itemprop="name">Contact Us:</h4>
                    <div class="footer-contact-info">
                        <div class="fci-wrap">
                            <div class="fci-row" itemprop="address" itemscope
                                itemtype="http://schema.org/PostalAddress">
                                <span class="fci-title"><i class="fa fa-map-marker icon-address"></i></span>
                                <span class="fci-content address-wrap">
                                    <span itemprop="streetAddress" class="address">{{ $features[0]->desc }}</span>
                                </span>
                            </div>
                            <div class="fci-row">
                                <span class="fci-title"><i class="fa fa-phone icon-phone"></i></span>
                                <span class="fci-content phone-wrap">
                                    @foreach (explode(',', $features[1]->desc) as $item)
                                        <a href="tel:{{ $item }}" title="Call Us">
                                            <span itemprop="telephone" class="phone">{{ $item }}</span>
                                        </a>
                                        <br>
                                    @endforeach
                                </span>
                            </div>
                            <div class="fci-row">
                                <span class="fci-title"><i class="fa fa-envelope icon-email"></i></span>
                                <span class="fci-content email-wrap">
                                    @foreach (explode(',', $features[2]->desc) as $item)
                                        <a href="mailto:{{ $item }}">
                                            <span itemprop="email" class="email">{{ $item }}</span>
                                        </a>
                                        <br />
                                    @endforeach
                                </span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-3 col-sm-6">
                <div class="openhours">
                    <h4 itemprop="name">Business Hours:</h4>
                    <div class="footer-openhours">
                        <div class="foh-wrap">
                            <!-- Normal Day -->
                            @foreach ($extras as $extra)
                                <div class="foh-row" itemprop="openingHours"
                                    content="{{ $extra->name }}{{ $extra->desc }}">
                                    <span class="foh-date">{{ $extra->name }}</span>
                                    <span class="foh-time">{{ $extra->desc }}</span>
                                </div>
                            @endforeach
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-3 col-sm-12">
                <div class="fotter-follow-us">
                    <h4 itemprop="name">Follow Us:</h4>
                    <div class="footer-social-links mysocial">
                        <div class="social-img-icons-wrap">
                            @foreach ($social as $item)
                                @if ($item['url'])
                                    <a itemprop="url" class="social-img-icons" target="_blank"
                                        href="{{ $item['name'] }}">
                                        <img src="{{ asset('storage') }}/photos/{{ $item['url'] }}">
                                    </a>
                                @endif
                            @endforeach
                        </div>
                    </div>
                    <div class="footer-social-fanpage">
                        <aside>
                            <!-- Start Single Footer Widget -->
                            <!-- facebook fanpage -->
                            <div id="fanpage_fb_container"></div>
                            <!-- use for calculator width -->
                            <div id="social_block_width" class="clearfix"
                                style="width:100% !important; height: 1px !important"></div>
                            <!-- End Single Footer Widget -->
                        </aside>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="footer-copyright clearfix"></div> <!-- Active freeze footer by delete style display: none -->
    <div class="freeze-footer">
        <ul>
            <li><a href="tel:{{ explode(',', $features[1]->desc)[0] }}"
                    class="btn btn-default btn_call_now btn-call">{{ explode(',', $features[1]->desc)[0] }}</a></li>
            <li><a href="/book" class="btn btn-default btn_make_appointment">Booking</a></li>
        </ul>
    </div>
</footer>
