<section class="main-wrap clearfix">
    <section class="p-list-gallery">
        <!-- tpl list_gallery -->
        <!-- support render gallery_data -->
        <section class="small-section bg-dark-lighter breadcrumbs gallery-breadcrumb">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <h2 class="hs-line-11 font-alt" itemprop="name">Gallery</h2>
                    </div>
                </div>
            </div>
        </section>
        <section class="page-section plg-gallery">
            <div class="container">
                <div class="gallery-box-wrap">
                    <div class="row">
                        @foreach ($list as $item)
                            <div class="col-xs-6 col-sm-6 col-md-4">
                                <a itemprop="url" class="gallery-item gallery-item-popup" data-group="gallery-1"
                                    title="Nails Design" href="{{ asset('storage') }}/photos/{{ $item->url }}">
                                    <div class="gallery-box">
                                        <div class="image-bg"
                                            style="background-image: url('{{ asset('storage') }}/photos/{{ $item->url }}');">
                                            <img itemprop="image"
                                                src="{{ asset('storage') }}/photos/{{ $item->url }}">
                                        </div>
                                    </div>
                                </a>
                            </div>
                        @endforeach
                    </div>
                </div>
                {{ $list->links('frontend.nail02-c.component.custom-pagination') }}
            </div>
        </section>
    </section>
</section>
