@php
$album = App\Models\Album::where([['type', 'Social'], ['status', 'PUBLISHED']])->first();
$social = App\Models\Asset::where('albums_id', $album['id'])->get();
@endphp
<section class="main-wrap clearfix">
    <section class="p-home">
        {{-- CAROUSEL --}}
        <div id="slider-option" data-speed="3"></div>
        <div class="section-slider-wrap">
            <section class="section section-slider">
                @if (count($imgs_carousel) > 0)
                    <div class="slider-width-height"
                        style="display: inline-block; width: 100%; height: 1px; overflow: hidden;">
                        <div class="fixed" style="width: 100%;"></div>
                        <img src="{{ asset('storage') }}/photos/{{ $imgs_carousel[0]['url'] }}"
                            style="width: 100%; height: auto;">
                    </div>
                @endif
                {{-- END USE FOR CALCULATOR START WIDTH HEIGHT --}}
                <div class="slider-pro bg-dark-alfa-30" id="my-slider" style="display: none;">
                    <div class="sp-slides">
                        @foreach ($imgs_carousel as $img)
                            <div class="sp-slide">
                                <div class="sp-layer sp-static">
                                    <img itemprop="image" class="sp-image"
                                        src="{{ asset('storage') }}/photos/{{ $img['url'] }}"
                                        alt="dreamstime_l_98741223.png">
                                </div>
                                <div class="sp-layer sp-static" data-width="100%" data-height="100%">
                                    <div class="home-content">
                                        <div class="home-text">
                                            <div class="local-scroll">
                                                <a itemprop="url" href="/book"
                                                    class="btn btn-mod btn-w btn-medium btn-round btn-slider btn_make_appointment"
                                                    id="mytime">
                                                    <font itemprop="name">Make an appointment</font>
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        @endforeach
                    </div>
                </div>

                <div class="slider-pro bg-dark-alfa-30" id="my-slider-fixed-height" style="display: none;">
                    <div class="sp-slides">
                        @foreach ($imgs_carousel as $img)
                            <div class="sp-slide">
                                <div class="sp-layer sp-static" data-width="100%" data-height="100%"
                                    style="width: 100%;height: 100%;background: url('{{ asset('storage') }}/photos/{{ $img['url'] }}') center center no-repeat;background-size: cover;">
                                    <img style="display:none;" itemprop="image" class="sp-image"
                                        src="{{ asset('storage') }}/photos/{{ $img['url'] }}"
                                        alt="{{ $img['url'] }}">
                                </div>
                                <div class="sp-layer sp-static" data-width="100%" data-height="100%">
                                    <div class="home-content">
                                        <div class="home-text">
                                            <div class="local-scroll">
                                                <a itemprop="url" href="/book"
                                                    class="btn btn-mod btn-w btn-medium btn-round btn-slider btn_make_appointment"
                                                    id="mytime">
                                                    <font itemprop="name">Make an appointment</font>
                                                </a>

                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        @endforeach

                    </div>
                </div>
            </section>
        </div>
        {{-- SOCIAL --}}
        <section class="small-section bg-dark home-social">
            <div class="container relative">
                <div class="align-center home-social-inner">
                    <h3 itemprop="name" class="banner-heading font-alt">BE SOCIAL</h3>
                    <div class="footer-social-links-w">
                        <div class="social-img-icons-wrap">
                            @foreach ($social as $item)
                                @if ($item['url'])
                                    <a itemprop="url" class="social-img-icons" target="_blank"
                                        href="{{ $item['name'] }}">
                                        <img src="{{ asset('storage') }}/photos/{{ $item['url'] }}">
                                    </a>
                                @endif
                            @endforeach
                        </div>
                    </div>
                </div>
            </div>
        </section>
        {{-- HOME ARTICLE --}}
        <section class="page-section home-about">
            <div class="container relative">
                <div class="row">
                    <div class="col-md-6">
                        <div class="home-abox">
                            <blockquote>
                                <p iemprop="description">{{ $home_article['title'] }}</p>
                                <footer iemprop="description">Our Story.</footer>
                            </blockquote>
                            {!! $home_article['content'] !!}
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="home-abox">
                            <div class="youtube-player"><iframe width="100%" height="100%"
                                    src="{{ $home_article['description'] }}" frameborder="0"
                                    allowfullscreen="allowfullscreen" data-autoplay="true"
                                    data-automute="true"></iframe></div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        {{-- SERVICES --}}
        <section class="page-section home-service">
            <div class="container-fluid">
                <h2 style="text-align: center;">Our Services</h2>
                <div class="row">
                    <div class="relative">
                        <ul class="works-grid work-grid-6 clearfix font-alt hover-white home-service-data"
                            id="work-grid-1">
                            <!-- Work Item (Lightbox) -->
                            @foreach ($services as $service)
                                <li itemscope itemtype="http://schema.org/Service"
                                    class="work-item mix branding design">
                                    <meta itemprop="serviceType" content="NailSalon" />
                                    <a itemprop="url"
                                        href="{{ route('services-detail', ['slug' => strtolower(trim(preg_replace('/[^A-Za-z0-9-]+/', '-', $service['name']))), 'service_id' => $service['id']]) }}"
                                        style="text-decoration: none">
                                        <div class="work-img">
                                            <div class="image-bg"
                                                style="background-image: url('{{ asset('storage') }}/photos/{{ json_decode($service['images'])[0] }}');">
                                                <img itemprop="image" class="work-img"
                                                    src="{{ asset('storage') }}/photos/{{ json_decode($service['images'])[0] }}"
                                                    alt=" {{ $service['name'] }}">
                                            </div>
                                        </div>
                                        <div class="work-intro">
                                            <h3 itemprop="name" class="work-title"> {{ $service['name'] }}</h3>
                                            <div itemprop="description" class="work-descr"></div>
                                        </div>
                                    </a>
                                </li>
                            @endforeach
                        </ul>
                    </div>
                </div>
            </div>
        </section>
        <div class="section section-gallery">
            <h2>Our Gallery</h2>
            <div class="line-title"></div>
            <div class="gallery-box-wrap">
                <div class="row">
                    @foreach ($gallery_list as $item)
                        <div class="col-xs-6 col-sm-6 col-md-3">
                            <a itemprop="url" class="gallery-item gallery-item-popup" data-group="gallery-1"
                                title="Nails Design" href="{{ asset('storage') }}/photos/{{ $item['url'] }}">
                                <div class="gallery-box">
                                    <div class="image-bg"
                                        style="background-image: url('{{ asset('storage') }}/photos/{{ $item['url'] }}');">
                                        <img itemprop="image"
                                            src="{{ asset('storage') }}/photos/{{ $item['url'] }}">
                                    </div>
                                </div>
                            </a>
                        </div>
                    @endforeach

                </div>
            </div>
        </div>
    </section>
</section>
