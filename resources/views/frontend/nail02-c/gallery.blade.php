<section class="main-wrap clearfix">
    <section class="p-gallery">
        <!-- tpl main -->
        <!-- support render gallery_category, gallery_data -->
        <section class="small-section bg-dark-lighter breadcrumbs gallery-breadcrumb">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <h2 class="hs-line-11 font-alt" itemprop="name">Gallery</h2>
                    </div>
                </div>
            </div>
        </section>
        <section class="page-section pg-gallery">
            <div class="container">
                <div class="gallery-box-wrap">
                    <div class="row">
                        <!-- Custome Height with .gallery-box .image-bg{padding-bottom: 75%;} -->
                        <!-- List category -->
                        @foreach ($albums as $index => $album)
                            <div class="col-xs-6 col-sm-6 col-md-4">
                                <a itemprop="url" class="gallery-item" data-group="gallery-1" title="Nails Design 77042"
                                    href="/gallery-detail/{{ $album['id'] }}">
                                    <div class="gallery-box">
                                        <div class="image-bg"
                                            style="background-image: url('{{ asset('storage') }}/photos/{{ $lists[$index][0]['url'] }}');">
                                            <img itemprop="image"
                                                src="{{ asset('storage') }}/photos/{{ $lists[$index][0]['url'] }}">
                                        </div>
                                        <div class="gallery-title">
                                            <span itemprop="name">{{ $album['name'] }}</span>
                                        </div>
                                    </div>
                                </a>
                            </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </section>
    </section>
</section>
