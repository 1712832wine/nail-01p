@php
$features = json_decode($header['features']);
@endphp
<section class="main-wrap clearfix">
    <section class="p-contact">
        <!-- tpl main -->
        <section class="small-section bg-dark-lighter breadcrumbs contact-breadcrumb">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <h2 class="hs-line-11 font-alt" itemprop="name">Contact us</h2>
                    </div>
                </div>
            </div>
        </section>
        <section class="page-section pc-contact">
            <div class="container relative">
                <div class="row">
                    <div itemscope="" itemtype="http://schema.org/ContactPage" class="section-text">
                        <div class="col-md-4 col-sm-12">
                            <blockquote>
                                <p itemprop="name">Better Nails, Better Life.</p>
                                <footer itemprop="description">Our Story.</footer>
                            </blockquote>
                        </div>
                        <div itemprop="text" class="col-md-4 col-sm-6">
                            <p>{{ $header['description'] }}</p>
                        </div>
                        <div itemprop="text" class="col-md-4 col-sm-6"></div>
                    </div>
                </div>
                <div class="row">
                    <div class="section-contact">
                        <div class="col-md-4">
                            <div class="contact-item">
                                <div class="ci-icon"><i class="fa fa-home"></i></div>
                                <div itemprop="name" class="ci-title font-alt">Store contact:</div>
                                <div iemprop="desc" class="ci-text">
                                    <div class="contact-info">
                                        <div class="fci-wrap">
                                            <div class="fci-row" itemprop="address" itemscope
                                                itemtype="http://schema.org/PostalAddress">
                                                <span class="fci-title"><i
                                                        class="fa fa-map-marker icon-address"></i></span>
                                                <span class="fci-content address-wrap">
                                                    <span itemprop="streetAddress"
                                                        class="address">{{ $features[0]->desc }}</span>

                                                </span>
                                            </div>

                                            <div class="fci-row">
                                                <span class="fci-title"><i class="fa fa-phone icon-phone"></i></span>
                                                <span class="fci-content phone-wrap">
                                                    @foreach (explode(',', $features[1]->desc) as $item)
                                                        <a href="tel:{{ $item }}" title="Call Us">
                                                            <span itemprop="telephone"
                                                                class="phone">{{ $item }}</span>
                                                        </a>
                                                        <br>
                                                    @endforeach


                                                </span>
                                            </div>

                                            <div class="fci-row">
                                                <span class="fci-title"><i class="fa fa-envelope icon-email"></i></span>
                                                <span class="fci-content email-wrap">
                                                    @foreach (explode(',', $features[2]->desc) as $item)
                                                        <a href="mailto:{{ $item }}">
                                                            <span itemprop="email"
                                                                class="email">{{ $item }}</span>
                                                        </a>
                                                        <br>
                                                    @endforeach
                                                </span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-8">
                            <section class="clearfix contact-form-wrap">
                                <div class="box-msg box-msg-" style=""></div>
                                <script src='https://www.google.com/recaptcha/api.js?hl=en' async defer>
                                </script><!-- Google reCaptcha -->
                                <script type="text/javascript">
                                    <!--
                                    function ezyCaptcha_send_contact(token, is_submit) {
                                        is_submit = 0;
                                        if ($("#password").length) {
                                            //$("input:password").val(md5(clean_input($("#password").val())));
                                        }
                                        return true;
                                    }
                                    //

                                    -->
                                </script>
                                <form enctype="multipart/form-data" class="contact-form" method="post"
                                    name="send_contact" id="send_contact" action="{{ route('send_contact') }}"
                                    accept-charset="UTF-8">
                                    @csrf
                                    <!-- <input name="form_type" type="hidden" value="contact"><input name="utf8" type="hidden" value="✓"> -->
                                    <div class="clearfix">
                                        <div class="cf-left-col">
                                            <div class="form-group">
                                                <input class="input-md round form-control" name="contactname"
                                                    type="text" data-validation="[NOTEMPTY]"
                                                    data-validation-message="Please enter your name!" autocomplete="off"
                                                    placeholder="Your Name">
                                            </div>
                                            <div class="form-group">
                                                <input class="input-md round form-control" name="contactemail"
                                                    type="text" data-validation="[EMAIL]"
                                                    data-validation-message="Invalid email" autocomplete="off"
                                                    placeholder="Your Email">
                                            </div>
                                            <div class="form-group">
                                                <input class="input-md round form-control" name="contactsubject"
                                                    type="text" data-validation="[NOTEMPTY]"
                                                    data-validation-message="Please enter a subject!" autocomplete="off"
                                                    placeholder="Your Subject">
                                            </div>
                                        </div>
                                        <div class="cf-right-col">
                                            <div class="form-group contact-textarea">
                                                <textarea class="input-md round form-control" name="contactcontent"
                                                    rows="6" data-validation="[NOTEMPTY]"
                                                    data-validation-message="Please enter a content!" autocomplete="off"
                                                    placeholder="Your message" maxlength="501"></textarea>
                                            </div>
                                        </div>
                                        <div class="cf-col">
                                            <div class="form-group">
                                                @if (session('status'))
                                                    <div class="alert alert-success" role="alert">
                                                        <button type="button" class="close"
                                                            data-dismiss="alert">×</button>
                                                        {{ session('status') }}
                                                    </div>
                                                @elseif(session('failed'))
                                                    <div class="alert alert-danger" role="alert">
                                                        <button type="button" class="close"
                                                            data-dismiss="alert">×</button>
                                                        {{ session('failed') }}
                                                    </div>
                                                @endif
                                            </div>
                                        </div>
                                    </div>
                                    <div class="clearfix">
                                        <div class="cf-left-col">
                                            <div class="form-tip pt-20"><i class="fa fa-info-circle"></i> All
                                                the fields are required</div>
                                        </div>
                                        <div class="cf-right-col">
                                            <div class="align-right pt-10">
                                                <button class="submit_btn btn btn-mod btn-medium btn-round btn_contact "
                                                    id="contactFormSubmit" type="submit">Submit Message</button>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </section>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <div itemscope itemprop="hasMap" itemtype="http://schema.org/Map" id="map-container" class="map-container">
            <iframe
                src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3464.7391505606774!2d-95.57299108550652!3d29.727314340440024!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x8640ddc6ed681049%3A0x74ace03ed50728e8!2sFast+Boy+Marketing!5e0!3m2!1sen!2s!4v1510909605272"
                width="100%" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
        </div>
    </section>
</section>
