@php
$features = json_decode($header['features']);
$extras = json_decode($header['extras']);
@endphp
<div class="content-main">
    {{-- CAROUSEL --}}
    <div class="slider_area bg_color_1">
        <div class="slider-width-height" style="display: block; width: 100%; height: 0.1px; overflow: hidden;">
            <div class="fixed" style="width: 100%;"></div>
            @if (count($imgs_carousel) > 0)
                <img src="{{ asset('storage') }}/photos/{{ $imgs_carousel[0]['url'] }}"
                    style="width: 100%; height: auto;">
            @endif
        </div>
        {{-- END USE FOR CALCULATOR START WIDTH HEIGHT --}}
        <div class="slide-active slider-shape-1 slider-activation slider-nav-style-1">
            @foreach ($imgs_carousel as $img)
                <div class="single-slider-wrap">
                    <div class="slick-img"
                        style="background: url('{{ asset('storage') }}/photos/{{ $img['url'] }}') no-repeat center center;background-size: cover;">
                        <img src="{{ asset('storage') }}/photos/{{ $img['url'] }}">
                    </div>
                    <div class="slider-content">
                        <div class="slider-inner text-center">
                            <div class="slider-button">
                                <a class="hs-btn btn-light btn-large btn_make_appointment" href="/book"
                                    itemprop="url">Make an appointment online</a>
                                <a class="hs-btn btn-light btn-large mb-15 btn_tel" style="margin-left:15px;"
                                    href="tel:{{ explode(',', $features[1]->desc)[0] }}"><span class=" icon"><i
                                            class="icon-phone"></i></span><span class="title">Call now</span></a>
                            </div>
                        </div>
                    </div>
                </div>
            @endforeach
        </div>
    </div>
    {{-- HOME ARTICLE --}}
    <section class="about_home_v1 home-about">
        <div class="container">
            <div class="row">
                <div class="col-md-4 hidden-sm hidden-xs">
                    <div class="about_thumb_v1">
                        <div class="thumb_img"><img itemprop="image"
                                src="{{ asset('storage') }}/photos/{{ json_decode($home_article['image'])[0] }}"
                                alt="about home page" caption="false" /></div>
                    </div>
                </div>
                <div class="col-md-8">
                    <div class="about_box_right">
                        <h2 itemprop="name">About Us</h2>
                        <div itemprop="description" class="sub-title">{{ $home_article['title'] }}</div>
                        <div itemprop="description">
                            {!! $home_article['content'] !!}
                        </div>
                        <div class="text-right"></div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    {{-- SERVICES --}}
    <section class="box_our_service home-service">
        <div class="box_our_service_inner">
            <div class="container">
                <div class="our_service_title">
                    <h2 class="section_title" itemprop="name">Our Services</h2>
                    <p class="section_details" itemprop="name">VRS - Nails & Spa Center</p>
                </div>
                <div class="row">
                    <div class="col-md-12 clearfix">
                        <div class="row">
                            @foreach ($services as $service)
                                <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
                                    <div class="single_service_area first">
                                        <div class="thumb_img">
                                            <div class="image-bg"
                                                style="background-image: url('{{ asset('storage') }}/photos/{{ json_decode($service['images'])[0] }}');">
                                                <img itemprop="image" class="img-circle img_fsize"
                                                    src="{{ asset('storage') }}/photos/{{ json_decode($service['images'])[0] }}"
                                                    alt="{{ $service['name'] }}">
                                            </div>
                                        </div>
                                        <div class="service-content">
                                            <h4 itemprop="name" class="service_tit">{{ $service['name'] }}</h4>
                                            <p itemprop="description" class="ser_pra">{{ $service['description'] }}
                                            </p>
                                            <div class="btn_book_service">
                                                <a itemprop="url" class="mb-15"
                                                    href="{{ route('services-detail', ['slug' => strtolower(trim(preg_replace('/[^A-Za-z0-9-]+/', '-', $service['name']))), 'service_id' => $service['id']]) }}"
                                                    title="Book now">view more</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            @endforeach
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    {{-- GALLERY --}}
    <section class="gallery_album_v1 home-gallery home-video">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="gallery_title">
                        <h2 class="section_title" itemprop="name">Gallery Album</h2>
                        <p class="section_details">The best for you</p>
                    </div><!-- Custom height with class: .image-bg {padding-bottom: 75%;} -->
                    <div class="portfolio-grid-wrap">
                        <ul id="portfolio-grid" class="two-column hover-two portfolio-grid">
                            @foreach ($gallery_list as $item)
                                <li class="portfolio-item text-center">
                                    <a class="tt-lightbox" href="{{ asset('storage') }}/photos/{{ $item['url'] }}"
                                        title="{{ $item['name'] }}">
                                        <div class="portfolio">
                                            <div class="tt-overlay"></div>
                                            <div class="image-bg"
                                                style="background-image: url('{{ asset('storage') }}/photos/{{ $item['url'] }}');">
                                                <img itemprop="image"
                                                    src="{{ asset('storage') }}/photos/{{ $item['url'] }}"
                                                    alt="{{ $item['name'] }}">
                                            </div>
                                            <div class="portfolio-info">
                                                <h3 class="project-title">NAIL DESIGN</h3>
                                                <span class="links">{{ $item['name'] }}</span>
                                            </div>
                                            <ul class="portfolio-details">
                                                <li><span><i class="icon icon-zoom-in"></i></span></li>
                                            </ul>
                                        </div>
                                    </a>
                                </li>
                            @endforeach
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>
