@php
$features = json_decode($header['features']);
@endphp
<div class="content-main">
    <section class="p-service">
        <section class="breadcrumbs_area breadcrumb-image section-padding parallex">
            <div class="container">
                <div class="breadcrumbs">
                    <div class="col-full">
                        <h2 class="page_title" itemprop="name">Services</h2>
                    </div>
                </div>
            </div>
        </section>
        <div class="container animation_sroll_jumpto">
            <div class="page-container">
                <input type="hidden" name="group_id" value="{{ $service_id }}" />
                <div class="row">
                    <div class="col-md-10 col-md-push-1">
                        <div class="pc-service-data1">
                            <div class="service-btn sb-service-data1">
                                <a class="btn btn-default btn-book btn_make_appointment" href="/book">
                                    <img
                                        src="{{ asset('frontend') }}/themes/fnail01a/assets/images/btn-book-bg.png">Make
                                    an appointment
                                </a>
                                <a class="btn btn-default btn-call" href="tel:{{ $features[1]->desc }}">
                                    <img
                                        src="{{ asset('frontend') }}/themes/fnail01a/assets/images/btn-call-bg.png">Call
                                    now
                                </a>
                            </div>
                            <!-- Service -->
                            @foreach ($services as $index => $service)
                                @if ($index % 2 === 0)
                                    <div class="service-row sroll_jumpto" id="sci_{{ $service['id'] }}">
                                        <div class="row">
                                            <div class="col-md-3 col-sm-4 ">
                                                <div class="service-image-wrap circle-wrap">
                                                    @if (count(json_decode($service['images'])) > 0)
                                                        <div class="circle-service-image"
                                                            style="background-image: url('{{ asset('storage') }}/photos/{{ json_decode($service['images'])[0] }}')">
                                                        </div>
                                                    @endif
                                                </div>
                                            </div>
                                            <div class="col-md-9 col-sm-8 ">
                                                <div class="service-content-wrap">
                                                    <h3 id="artificial_nails" class="service-catname">
                                                        {{ $service['name'] }}
                                                    </h3>
                                                    <div class="cat_title">{{ $service['description'] }}</div>
                                                    @foreach (json_decode($service['features']) as $item)
                                                        @switch($item->name)
                                                            @case('desc')
                                                                <p>{{ $item->desc }}</p>
                                                            @break
                                                            @case('center')
                                                            @break
                                                            @default
                                                                <div class="service-item">
                                                                    <div class="si-info">
                                                                        <div class="si-info-wrap">
                                                                            <div class="si-info-col">
                                                                                <h5 class="si-name">{{ $item->name }}</h5>
                                                                            </div>
                                                                            <div class="si-info-col si-dots-wrap"></div>
                                                                            <div class="si-info-col">
                                                                                <div class="si-price">
                                                                                    <span class="si-p-wrap">
                                                                                        <span
                                                                                            class="current">{{ $item->desc }}</span>
                                                                                    </span>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                        @endswitch
                                                    @endforeach
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                @else
                                    <div class="service-row sroll_jumpto" id="sci_{{ $service['id'] }}">
                                        <div class="row">
                                            <div class="col-md-3 col-sm-4 col-md-push-9 col-sm-push-8">
                                                <div class="service-image-wrap circle-wrap">
                                                    @if (count(json_decode($service['images'])) > 0)
                                                        <div class="circle-service-image"
                                                            style="background-image: url('{{ asset('storage') }}/photos/{{ json_decode($service['images'])[0] }}')">
                                                        </div>
                                                    @endif
                                                </div>
                                            </div>
                                            <div class="col-md-9 col-sm-8 col-md-pull-3 col-sm-pull-4">
                                                <div class="service-content-wrap">
                                                    <h3 id="natural_nails" class="service-catname">
                                                        {{ $service['name'] }}
                                                    </h3>
                                                    <div class="cat_title">{{ $service['description'] }}</div>
                                                    @foreach (json_decode($service['features']) as $item)
                                                        @switch($item->name)
                                                            @case('desc')
                                                                <p>{{ $item->desc }}</p>
                                                            @break
                                                            @case('center')
                                                            @break
                                                            @default
                                                                <div class="service-item">
                                                                    <div class="si-info">
                                                                        <div class="si-info-wrap">
                                                                            <div class="si-info-col">
                                                                                <h5 class="si-name">{{ $item->name }}</h5>
                                                                            </div>
                                                                            <div class="si-info-col si-dots-wrap"></div>
                                                                            <div class="si-info-col">
                                                                                <div class="si-price">
                                                                                    <span class="si-p-wrap">
                                                                                        <span
                                                                                            class="current">{{ $item->desc }}</span>
                                                                                    </span>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                        @endswitch
                                                    @endforeach
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                @endif
                            @endforeach
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>
