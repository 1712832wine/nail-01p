<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" dir="ltr" lang="en" xmlns:og="http://ogp.me/ns#"
    xmlns:fb="http://www.facebook.com/2008/fbml" itemscope itemtype="https://schema.org/NailSalon">

<head>
    <!-- Language -->
    <meta http-equiv="content-language" content="en" />
    <link rel="alternate" href="" hreflang="x-default">
    <link rel="alternate" href="" hreflang="en-US">

    <!-- Meta limit -->

    <!-- Meta Tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width,initial-scale=1.0" />
    <meta http-equiv="content-type" content="text/html; charset=UTF-8" />
    <meta name="description" content="Fast Boy Marketing" />
    <meta name="keywords" content="Fast Boy Marketing" />
    <meta name="author" content="Fast Boy Marketing" />

    <!-- OG -->
    <meta property="og:title" content="Fast Boy Marketing" />
    <meta property="og:description" content="Fast Boy Marketing" />
    <meta property="og:image" content="" />
    <meta property="og:url" content="/" />
    <meta property="og:type" content="website" />
    <meta property="og:site_name" content="Demo FNAIL01A" />

    <!-- Dublin Core -->
    <link rel="schema.DC" href="http://purl.org/dc/elements/1.1/">
    <meta name="DC.title" content="Fast Boy Marketing">
    <meta name="DC.identifier" content="/">
    <meta name="DC.description" content="Fast Boy Marketing">
    <meta name="DC.subject" content="Fast Boy Marketing">
    <meta name="DC.language" scheme="UTF-8" content="en-us">
    <meta itemprop="priceRange" name="priceRange" content="">

    <!-- GEO meta -->
    <meta name="geo.region" content="">
    <meta name="geo.placename" content="">
    <meta name="geo.position" content="">
    <meta name="ICBM" content="">

    <!-- Page Title -->
    <title>Demo FNAIL01A</title>
    <base href="/themes/fnail01a/assets/">

    <!-- canonical -->
    <link rel="canonical" href="">



    <!-- Favicons -->
    <link rel="icon" href="/uploads/fnail0ycqa6y8/attach/1562750965_fbm_fvc.png" type="image/x-icon">
    <link rel="shortcut icon" href="/uploads/fnail0ycqa6y8/attach/1562750965_fbm_fvc.png" type="image/x-icon">

    <!-- Css -->
    <link rel="stylesheet" type="text/css"
        href='https://fonts.googleapis.com/css?family=Lato:300,300i,400,400i,600,700|Open+Sans:300,400,600'>
    <link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Cookie">
    <link rel="stylesheet" type="text/css" href='{{ asset('frontend') }}/public/library/jquery-ui/jquery-ui.min.css'>
    <link rel="stylesheet" type="text/css" href='{{ asset('frontend') }}/public/library/animate/animate.css'>
    <link rel="stylesheet" type="text/css" href='{{ asset('frontend') }}/public/library/validation/validation.css'>
    <link rel="stylesheet" type="text/css" href='{{ asset('frontend') }}/public/library/flex-label/flex-label.css'>
    <link rel="stylesheet" type="text/css"
        href='{{ asset('frontend') }}/public/library/bootstrap/v3.3.7/bootstrap.min.css'>
    <link rel="stylesheet" type="text/css"
        href='{{ asset('frontend') }}/public/library/bootstrap-datetimepicker/bootstrap-datetimepicker.min.css'>
    <link rel="stylesheet" type="text/css"
        href='{{ asset('frontend') }}/public/library/font-awesome/v4.7.0/css/font-awesome.min.css'>
    <link rel="stylesheet" type="text/css"
        href='{{ asset('frontend') }}/public/library/font-flaticon/css/font-flaticon.css'>
    <link rel="stylesheet" type="text/css"
        href='{{ asset('frontend') }}/public/library/slider-pro/slider-pro.min.css'>
    <link rel="stylesheet" type="text/css"
        href='{{ asset('frontend') }}/public/library/slider-slick/slider-slick.css'>
    <link rel="stylesheet" type="text/css"
        href='{{ asset('frontend') }}/public/library/slider-slick/slider-theme.css'>
    <link rel="stylesheet" type="text/css"
        href='{{ asset('frontend') }}/public/library/slider-slick/slider-style.css'>
    <link rel="stylesheet" type="text/css"
        href='{{ asset('frontend') }}/public/library/owl-carousel/v2.3.4/owl.carousel.min.css'>
    <link rel="stylesheet" type="text/css"
        href='{{ asset('frontend') }}/public/library/owl-carousel/v2.3.4/owl.theme.default.min.css'>
    <link rel="stylesheet" type="text/css"
        href='{{ asset('frontend') }}/public/library/magnific-popup/magnific-popup.css'>
    <link rel="stylesheet" type="text/css" href='{{ asset('frontend') }}/public/library/lightbox/lightbox.css'>
    <link rel="stylesheet" type="text/css" href='{{ asset('frontend') }}/public/library/pnotify/pnotify.css'>
    <link rel="stylesheet" type="text/css" href='{{ asset('frontend') }}/public/library/instagram/instafeed.min.css'>
    <link rel="stylesheet" type="text/css" href='{{ asset('frontend') }}/public/library/signature/signature.css'>
    <link rel="stylesheet" type="text/css"
        href='{{ asset('frontend') }}/themes/fnail01a/assets/assets/fontello/css/fontello.css'>
    <link rel="stylesheet" type="text/css"
        href='{{ asset('frontend') }}/themes/fnail01a/assets/assets/css/pe-icon-7-stroke.css'>
    <link rel="stylesheet" type="text/css" href='{{ asset('frontend') }}/themes/fnail01a/assets/css/meanmenu.css'>
    <link rel="stylesheet" type="text/css" href='{{ asset('frontend') }}/themes/fnail01a/assets/css/reset.css'>
    <link rel="stylesheet" type="text/css" href='{{ asset('frontend') }}/themes/fnail01a/assets/css/app.css?v=1.0'>
    <link rel="stylesheet" type="text/css" href='{{ asset('frontend') }}/themes/fnail01a/assets/css/responsive.css'>
    <link rel="stylesheet" type="text/css"
        href='{{ asset('frontend') }}/themes/fnail01a/assets/assets/css/helper.css'>
    <link rel="stylesheet" type="text/css"
        href='{{ asset('frontend') }}/themes/fnail01a/assets/css/custom/custom_general.css'>
    <link rel="stylesheet" type="text/css" href='{{ asset('frontend') }}/themes/fnail01a/assets/css/custom/main.css'>
    <link rel="stylesheet" type="text/css" href='{{ asset('frontend') }}/public/library/global/css/news.css'>
    <link rel="stylesheet" type="text/css" href='{{ asset('frontend') }}/public/library/global/css/consent.css'>
    <link rel="stylesheet" type="text/css"
        href='{{ asset('frontend') }}/themes/fnail01a/assets/css/custom.css?v=1.2'>

    <style type="text/css">
        .single_service_area {
            padding: 30px 15px;
        }

        .about-us {
            text-align: justify;
        }

        .single_service_area .service-content {
            height: 54px;
        }

        .about_box_right {
            margin-top: 17px;
        }

        .slider-width-height .fixed {
            height: 0px;
        }

        .breadcrumb-image {
            background: rgba(255, 25, 25, 0);
        }

        h2.page_title {
            text-align: center;
        }

        @media (min-width: 1200px) {
            .nails_logo {
                height: 190px;
            }
        }

        .mean-container ement.style {}

        .mean-container .mean-bar {
            background: #c2c2c2;
        }

        .wrap-freeze-header.clearfix.hidden-xs.hidden-sm {
            background: #c2c2c2;
        }

        @media only screen and (max-width: 687px) {
            .footer_logo_v1 {
                width: 44%;
                margin: auto;
            }
        }

        .info_staff {
            display: none;
        }

        .booking_staff_title {
            display: none;
        }

        @media (min-width: 1200px) {
            .col-lg-3 {
                width: 33.33% !important;
            }
        }

        ul#portfolio-grid.two-column>li {
            width: 25%;
            padding: 5px;
        }

    </style>

    <!-- Go to www.addthis.com/dashboard to customize your tools -->
    <script type="text/javascript"
        src="{{ asset('frontend') }}//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-59b8fec5dd14e0f2"></script>
    <script type="text/javascript">
        var addthis_share = {
            url: "/",
            title: "Demo FNAIL01A",
            description: "Fast Boy Marketing",
            media: ""
        }

    </script>

    <!-- Script -->
    <script type="text/javascript" src="{{ asset('frontend') }}/public/library/jquery/v1.10.2/jquery.min.js">
    </script>
    <script type="text/javascript" src="{{ asset('frontend') }}/public/library/jquery-ui/jquery-ui.min.js"></script>
    <script type="text/javascript" src="{{ asset('frontend') }}/public/library/jquery-ui/jquery-migrate.min.js">
    </script>
    <script type="text/javascript" src="{{ asset('frontend') }}/public/library/validation/validation.min.js">
    </script>
    <script type="text/javascript" src="{{ asset('frontend') }}/public/library/mask/mask.min.js"></script>
    <script type="text/javascript" src="{{ asset('frontend') }}/public/library/flex-label/flex-label.js"></script>
    <script type="text/javascript" src="{{ asset('frontend') }}/public/library/bootstrap/v3.3.7/bootstrap.min.js">
    </script>
    <script type="text/javascript"
        src="{{ asset('frontend') }}/public/library/waiting-modal-dialog/waiting-modal-dialog.js"></script>
    <script type="text/javascript" src="{{ asset('frontend') }}/public/library/moment/moment-with-locales.js">
    </script>
    <script type="text/javascript"
        src="{{ asset('frontend') }}/public/library/bootstrap-datetimepicker/bootstrap-datetimepicker.js"></script>
    <script type="text/javascript" src="{{ asset('frontend') }}/public/library/magnific-popup/magnific-popup.min.js">
    </script>
    <script type="text/javascript" src="{{ asset('frontend') }}/public/library/pnotify/pnotify.js"></script>
    <script type="text/javascript" src="{{ asset('frontend') }}/public/library/slider-pro/slider-pro.min.js">
    </script>
    <script type="text/javascript" src="{{ asset('frontend') }}/public/library/slider-slick/slider-slick.js">
    </script>
    <script type="text/javascript" src="{{ asset('frontend') }}/public/library/shuffle/shuffle.min.js"></script>
    <script type="text/javascript"
        src="{{ asset('frontend') }}/public/library/owl-carousel/v2.3.4/owl.carousel.min.js"></script>
    <script type="text/javascript" src="{{ asset('frontend') }}/public/library/wow/wow.min.js"></script>
    <script type="text/javascript" src="{{ asset('frontend') }}/public/library/meanmenu/meanmenu.js"></script>
    <script type="text/javascript" src="{{ asset('frontend') }}/public/library/instagram/instafeed.min.js"></script>
    <script type="text/javascript" src="{{ asset('frontend') }}/public/library/signature/signature.min.js"></script>
    <script type="text/javascript" src="{{ asset('frontend') }}/themes/fnail01a/assets/assets/js/stellar.js">
    </script>
    <script type="text/javascript"
        src="{{ asset('frontend') }}/themes/fnail01a/assets/assets/js/jquery.prettyPhoto.js"></script>
    <script type="text/javascript" src="{{ asset('frontend') }}/themes/fnail01a/assets/assets/js/jquery.mixitup.js">
    </script>
    <script type="text/javascript"
        src="{{ asset('frontend') }}/themes/fnail01a/assets/assets/js/jquery.simpleLens.min.js"></script>
    <script type="text/javascript" src="{{ asset('frontend') }}/themes/fnail01a/assets/assets/js/plugins.js">
    </script>
    <script type="text/javascript" src="{{ asset('frontend') }}/public/library/global/js/js_php.js"></script>
    <script type="text/javascript" src="{{ asset('frontend') }}/public/library/global/js/global.js"></script>

    <script type="text/javascript">
        var dateFormatBooking = "MM/DD/YYYY";
        var posFormat = "1,0,2";
        var timeMorning = '["10:00","10:30","11:00","11:30"]';
        var timeAfternoon =
            '["12:00","12:30","13:00","13:30","14:00","14:30","15:00","15:30","16:00","16:30","17:00","17:30","18:00","18:30","19:00"]';
        var bookLogin = "open_booking";
        var beforeTime = "1";
        var beforeDay = 0;
        var currDateT = "2021/05/24";
        var checktimebooking = 1;
        var enableRecaptcha = 0;
        var hoursTimeFormat = 12;
        var phoneFormat = "(000) 000-0000";
        var company_phone = "832-968-6668";
        var num_paging = "12";
        var site = "idx";

        let facebook_embed = {
            id_fanpage: "https://www.facebook.com/FastboyMarketingAgency",
            appId: "",
            width: 450,
            height: 300,
            tabs: "timeline",
            show_facepile: true,
            small_header: true,
            likebox_enable: true,
            likebox_show_faces: false,
            likebox_stream: true,
        };
        let google_id_fanpage = "";
        let twitter_id_fanpage = "";

    </script>

    <!-- Webmaster tools -->
    <!--Webmaster tools-->
    <!-- fb retargeting -->

    <!-- Schema Script -->
    <meta name="csrf-token" content="{{ csrf_token() }}" />
</head>

<body>

    <!-- Facebook Root And H1 Seo -->
    <h1 style="display: none"></h1>
    <div id="fb-root" style="display: none"></div>

    <!-- Tpl freeze header -->
    <input type="hidden" name="activeFreezeHeader" value="1" />
    @php
        $header = App\Models\Product::where('category_id', App\Models\Category::where('name', 'Header And Footer')->first()['id'])->first();
        $album = App\Models\Album::where([['type', 'Social'], ['status', 'PUBLISHED']])->first();
        $social = App\Models\Asset::where('albums_id', $album['id'])->orderBy('priority', 'desc')->get();
        $features = json_decode($header['features']);
    @endphp
    <div class="wrapper">
        @include('frontend.nail01-a.component.header',
        ['header'=>$header,
        'social'=>$social])
        {{-- MAIN --}}
        {{ $slot }}
        {{-- FOOTER --}}

        @include('frontend.nail01-a.component.footer',
        ['header' => $header,
        'social' => $social]))
    </div>

    <div class="freeze-footer">
        <ul>
            <li><a href="tel:{{ $features[1]->desc }}" class="btn btn-default btn_call_now btn-call"
                    title="Call us">{{ $features[1]->desc }}</a></li>
            <li><a href="/book" class="btn btn-default btn_make_appointment" title="Booking">Booking</a></li>
        </ul>
    </div>
    <script type="text/javascript">
        var enable_booking = "1";
        if (enable_booking == 0) {
            $(".btn_make_appointment").remove();
        }

    </script>

    <!-- popup -->

    <!-- external javascripts -->
    <script type="text/javascript" src="{{ asset('frontend') }}/public/library/global/js/consent.js"></script>
    <script type="text/javascript" src="{{ asset('frontend') }}/themes/fnail01a/assets/js/app.js?v=1.3"></script>

    <!-- Google analytics -->
    <script>
        (function(i, s, o, g, r, a, m) {
            i['GoogleAnalyticsObject'] = r;
            i[r] = i[r] || function() {
                (i[r].q = i[r].q || []).push(arguments)
            }, i[r].l = 1 * new Date();
            a = s.createElement(o),
                m = s.getElementsByTagName(o)[0];
            a.async = 1;
            a.src = g;
            m.parentNode.insertBefore(a, m)
        })(window, document, 'script', 'https://www.google-analytics.com/analytics.js', 'ga');
        ga('create', '', 'auto');
        ga('send', 'pageview');

    </script>

    <!-- gg adwords remarketing -->

    <!-- JS extends -->
    <script>
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

    </script>
    <script type="text/javascript"></script>
    @livewireScripts
</body>

</html>
