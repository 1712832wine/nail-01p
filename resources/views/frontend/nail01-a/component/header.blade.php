@php
$features = json_decode($header['features']);
$extras = json_decode($header['extras']);
@endphp
<div class="header_light bg_grey header-main">
    <div id="sticky_header_topbar" class="header_wrap hidden-sm hidden-xs">
        <div class="container">
            <div class="row">
                <div class="col-md-12 col-lg-12 col-sm-12">
                    <div class="heaader_top_v1">
                        <div class="hd_top_v1_left">
                            <div class="fci-wrap">
                                <div class="fci-row" itemprop="address" itemscope
                                    itemtype="http://schema.org/PostalAddress">
                                    <span class="fci-title"><i class="fa fa-map-marker icons-address"></i></span>
                                    <span class="fci-content">
                                        <span itemprop="streetAddress" class="address">{{ $features[0]->desc }}</span>
                                    </span>
                                </div>
                            </div>
                        </div>
                        <div class="vs_social_icon_top">
                            @foreach ($social as $item)
                                @if ($item['url'])
                                    <a itemprop="url" class="social-img-icons" rel="nofollow" target="_blank"
                                        href="{{ $item['content'] }}"><img
                                            src="{{ asset('storage') }}/photos/{{ $item['url'] }}">
                                    </a>
                                @endif
                            @endforeach
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="wrap-freeze-header clearfix hidden-xs hidden-sm">
            <div class="flag-freeze-header">
                <div class="container">
                    <div class="row fles">
                        <div class="col-sm-5 col-md-4">
                            <div class="nails_logo">
                                @if (count(json_decode($header['images'])) > 0)
                                    <a itemprop="url" href="/">
                                        <img src="{{ asset('storage') }}/photos/{{ json_decode($header['images'])[0] }}"
                                            alt="{{ $header['title'] }}" itemprop="logo image">
                                    </a>
                                @endif
                            </div>
                        </div>
                        <div class="col-sm-7 col-md-8">
                            <div class="mainmenu_wrap">
                                <nav class="main_nav_v1">
                                    <nav class="custom_menu">
                                        <!-- tpl menu_main -->
                                        <ul>
                                            <li itemprop="name"><a href="/" itemprop="url">Home</a></li>
                                            <li itemprop="name"><a href="/about" itemprop="url">About
                                                    us</a></li>
                                            <li itemprop="name"><a href="/services" itemprop="url">Services</a></li>
                                            <li itemprop="name"><a href="/coupons" itemprop="url">COUPONS</a></li>
                                            <li itemprop="name"><a href="/giftcards" itemprop="url">E-gift</a></li>
                                            <li itemprop="name"><a href="/gallery" itemprop="url">Gallery</a></li>
                                            <li itemprop="name"><a href="/book" itemprop="url">Booking</a>
                                            </li>
                                            <li itemprop="name"><a href="/contact" itemprop="url">Contact
                                                    us</a></li>
                                        </ul>
                                    </nav>
                                </nav>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="wrap-freeze-header-mobile clearfix hidden-md hidden-lg">
        <div class="flag-freeze-header-mobile">
            <div class="menu_mobile_v1">
                <div class="mobile_menu_container_v1">
                    <div class="mobile_logo">
                        <div class="logo_left">
                            @if (count(json_decode($header['images'])) > 0)
                                <a itemprop="url" href="/">
                                    <img src="{{ asset('storage') }}/photos/{{ json_decode($header['images'])[0] }}"
                                        alt="{{ $header['title'] }}" itemprop="logo image">
                                </a>
                            @endif
                        </div>
                        <div class="logo_right"></div>
                    </div>
                    <div class="mobile-menu clearfix">
                        <nav id="mobile_dropdown">
                            <!-- tpl menu_mobile -->
                            <ul>
                                <li itemprop="name"><a href="/" itemprop="url">Home</a></li>
                                <li itemprop="name"><a href="/about" itemprop="url">About us</a></li>
                                <li itemprop="name"><a href="/services" itemprop="url">Services</a></li>
                                <li itemprop="name"><a href="/coupons" itemprop="url">Coupons</a></li>
                                <li itemprop="name"><a href="/giftcards" itemprop="url">E-gift</a></li>
                                <li itemprop="name"><a href="/gallery" itemprop="url">Gallery</a></li>
                                <li itemprop="name"><a href="/book" itemprop="url">Booking</a></li>
                                <li itemprop="name"><a href="/contact" itemprop="url">Contact us</a></li>
                            </ul>
                        </nav>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
