@php
$features = json_decode($header['features']);
$extras = json_decode($header['extras']);
@endphp
<footer class="footer_area_v1 footer-main">
    <div class="footer_top_v1">
        <div class="container">
            <div class="row">
                <div class="col-lg-3 col-md-4 col-sm-6 col-xs-12">
                    <div class="single_footer_widget">
                        <div class="footer_logo_v1">
                            @if (count(json_decode($header['images'])) > 0)
                                <a itemprop="url" href="/">
                                    <img src="{{ asset('storage') }}/photos/{{ json_decode($header['images'])[0] }}"
                                        alt="{{ $header['title'] }}" itemprop="logo image">
                                </a>
                            @endif
                        </div>
                        <div itemprop="description" class="footer_spa"></div>
                        <div class="social_icon">
                            @foreach ($social as $item)
                                @if ($item['url'])
                                    <a itemprop="url" class="social-img-icons" rel="nofollow" target="_blank"
                                        href="{{ $item['content'] }}"><img
                                            src="{{ asset('storage') }}/photos/{{ $item['url'] }}">
                                    </a>
                                @endif
                            @endforeach
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 col-md-4 col-sm-6 col-xs-12">
                    <div class="single_footer_widget">
                        <h5 itemprop="name">BUSINESS HOURS</h5>
                        <div class="footer-openhours">
                            <!-- support render openhours_data -->
                            <div class="foh-wrap">
                                @foreach ($extras as $item)
                                    <div class="foh-row" itemprop="openingHours"
                                        content="{{ $item->name }}:{{ $item->desc }}">
                                        <span class="foh-date">{{ $item->name }}</span>
                                        <span class="foh-time">{{ $item->desc }}</span>
                                    </div>
                                @endforeach
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 col-md-4 col-sm-6 col-xs-12">
                    <div class="single_footer_widget">
                        <h5 class="footer_title_v1">Contact us</h5>
                        <div class="footer_address_area_v1">
                            <div class="fci-wrap">
                                <div class="fci-row" itemprop="address" itemscope
                                    itemtype="http://schema.org/PostalAddress">
                                    <span class="fci-title"><i class="fa fa-map-marker icons-address"></i></span>
                                    <span class="fci-content">
                                        <span itemprop="streetAddress"
                                            class="address">{{ $features[0]->desc }}</span>

                                    </span>
                                </div>

                                <div class="fci-row">
                                    <span class="fci-title"><i class="fa fa-phone icons-phone"></i></span>
                                    <span class="fci-content">
                                        <a href="tel:{{ $features[1]->desc }}" title="Call Us">
                                            <span itemprop="telephone" class="phone">{{ $features[1]->desc }}</span>
                                        </a>

                                    </span>
                                </div>
                                <div class="fci-row">
                                    <span class="fci-title"><i class="fa fa-envelope icons-email"></i></span>
                                    <span class="fci-content">
                                        <a href="mailto:{{ $features[2]->desc }}">
                                            <span itemprop="email" class="email">{{ $features[2]->desc }}</span>
                                        </a>
                                    </span>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="single_footer_widget">
                        <h5 class="footer_title_v1">Follow Us</h5>
                        <div class="social_fanpage">
                            <aside>
                                <div id="fanpage_fb_container"></div>
                                <div id="social_block_width" style="width:100% !important; height: 1px !important">
                                </div>
                            </aside>
                        </div>
                        <span class="fanpage_fb_container_rate" data-rate-division="3" data-rate-multiplication="2"
                            style="display: none;"></span>
                    </div>

                </div>

            </div>
        </div>
    </div>
    <div class="footer-copyright">
        <p>© Copyright by Demo FNAIL01A. All Right Reserved.</p>
    </div>
</footer>
