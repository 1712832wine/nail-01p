<div class="content-main">
    <section class="p-about">
        <section class="breadcrumbs_area breadcrumb-image section-padding parallex">
            <div class="container">
                <div class="breadcrumbs">
                    <div class="col-full">
                        <h2 class="page_title" itemprop="name">About us</h2>
                    </div>
                </div>
            </div>
        </section>
        <section class="about_us_v1">
            <div class="container">
                @foreach ($articles as $item)
                    <div class="row">
                        <div class="col-md-7 ">
                            <div class="about-us">
                                <h2 itemprop="name">{{ $item['title'] }}</h2>
                                {!! $item['content'] !!}
                            </div>
                        </div>
                        <div class="col-md-5">
                            @if (count(json_decode($item['image'])) > 0)
                                <img itemprop="image" class="img-responsive"
                                    src="{{ asset('storage') }}/photos/{{ json_decode($item['image'])[0] }}"
                                    caption="false" style="display: block; margin-left: auto; margin-right: auto;" />
                            @endif
                        </div>
                    </div>
                @endforeach
            </div>
        </section>
    </section>
</div>
