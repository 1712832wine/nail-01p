<div class="content-main">
    <section class="p-gallery">
        <!-- tpl main -->
        <section class="breadcrumbs_area breadcrumb-image section-padding parallex">
            <div class="container">
                <div class="breadcrumbs">
                    <div class="col-full">
                        <h2 class="page_title" itemprop="name">Gallery</h2>
                    </div>
                </div>
            </div>
        </section>
        <div class="container">
            <div style="margin: 20px 0px;" class="portfolio-container text-center">


                <div class="portfolio-grid-wrap">
                    <ul id="portfolio-grid" class="four-column hover-four box_list_gallery portfolio-grid">
                        @foreach ($lists[0] as $item)
                            <li class="portfolio-item">
                                <div class="tt-lightbox" href="{{ asset('storage') }}/photos/{{ $item['url'] }}"
                                    title="Nails Design">
                                    <div class="portfolio">
                                        <div class="tt-overlay">
                                        </div>
                                        <div class="image-bg"
                                            style="background-image: url('{{ asset('storage') }}/photos/{{ $item['url'] }}')">
                                            <img itemprop="image"
                                                src="{{ asset('storage') }}/photos/{{ $item['url'] }}">
                                        </div>
                                        <div class="portfolio-info">
                                            <h3 itemprop="name" class="project-title">Nails Design</h3>
                                            <span class="links"></span>
                                        </div>
                                        <ul class="portfolio-details">
                                            <li><span><i class="fa fa-search-plus" aria-hidden="true"></i></span>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </li>
                        @endforeach
                    </ul>
                    <div class="row">
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>
