<div class="content-main">
    <section class="p-coupons">
        <!-- tpl main -->
        <section class="breadcrumbs_area breadcrumb-image section-padding parallex">
            <div class="container">
                <div class="breadcrumbs">
                    <div class="col-full">
                        <h2 class="page_title" itemprop="name">Coupons</h2>
                    </div>
                </div>
            </div>
        </section>
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="coupon_code_v1">
                        <div class="row">
                            @foreach ($list as $item)
                                <div class="col-xs-12 col-md-6">
                                    <div class="coupon_img_v1 pointer image-magnific-popup" data-group=""
                                        href="{{ asset('storage') }}/photos/{{ $item['url'] }}" title="Coupon 2">
                                        <img itemprop="image" class="img-responsive"
                                            src="{{ asset('storage') }}/photos/{{ $item['url'] }}" alt="coupon v1">
                                    </div>
                                </div>
                            @endforeach
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>
