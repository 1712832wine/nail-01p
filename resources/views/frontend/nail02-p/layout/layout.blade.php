<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" dir="ltr" lang="en" xmlns:og="http://ogp.me/ns#"
    xmlns:fb="http://www.facebook.com/2008/fbml" itemscope itemtype="https://schema.org/NailSalon">

<head>
    <!-- Language -->
    <meta http-equiv="content-language" content="en" />
    <link rel="alternate" href="http://fnail02p.fastboywebhosts.com" hreflang="x-default">
    <link rel="alternate" href="http://fnail02p.fastboywebhosts.com" hreflang="en-US">

    <!-- Meta limit -->

    <!-- Meta Tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width,initial-scale=1.0" />
    <meta http-equiv="content-type" content="text/html; charset=UTF-8" />
    <meta name="description" content="" />
    <meta name="keywords" content="" />
    <meta name="author" content="" />

    <!-- OG -->
    <meta property="og:title" content="" />
    <meta property="og:description" content="" />
    <meta property="og:image" content="" />
    <meta property="og:url" content="http://fnail02p.fastboywebhosts.com/" />
    <meta property="og:type" content="website" />
    <meta property="og:site_name" content="Jewelry Nail & Spa" />

    <!-- Dublin Core -->
    <link rel="schema.DC" href="http://purl.org/dc/elements/1.1/">
    <meta name="DC.title" content="">
    <meta name="DC.identifier" content="http://fnail02p.fastboywebhosts.com/">
    <meta name="DC.description" content="">
    <meta name="DC.subject" content="">
    <meta name="DC.language" scheme="UTF-8" content="">
    <meta itemprop="priceRange" name="priceRange" content="">

    <!-- GEO meta -->
    <meta name="geo.region" content="">
    <meta name="geo.placename" content="">
    <meta name="geo.position" content="">
    <meta name="ICBM" content="">

    <!-- Page Title -->
    <title>Jewelry Nail & Spa</title>
    <base href="/themes/fnail02p/assets/">

    <!-- canonical -->
    <link rel="canonical" href="http://fnail02p.fastboywebhosts.com">



    <!-- Favicons -->
    <link rel="icon" href="http://fnail02p.fastboywebhosts.com/uploads/fnail0utxt26q/attach/1562750546_fbm_fvc.png"
        type="image/x-icon">
    <link rel="shortcut icon"
        href="http://fnail02p.fastboywebhosts.com/uploads/fnail0utxt26q/attach/1562750546_fbm_fvc.png"
        type="image/x-icon">

    <!-- Css -->
    <link href="https://fonts.googleapis.com/css?family=Raleway:400,700,500&amp;subset=latin-ext" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href='{{ asset('frontend') }}/public/library/jquery-ui/jquery-ui.min.css'>
    <link rel="stylesheet" type="text/css" href='{{ asset('frontend') }}/public/library/animate/animate.css'>
    <link rel="stylesheet" type="text/css" href='{{ asset('frontend') }}/public/library/validation/validation.css'>
    <link rel="stylesheet" type="text/css"
        href='{{ asset('frontend') }}/public/library/bootstrap/v3.3.7/bootstrap.min.css'>
    <link rel="stylesheet" type="text/css"
        href='{{ asset('frontend') }}/public/library/bootstrap-datetimepicker/bootstrap-datetimepicker.min.css'>
    <link rel="stylesheet" type="text/css"
        href='{{ asset('frontend') }}/public/library/font-awesome/v4.7.0/css/font-awesome.min.css'>
    <link rel="stylesheet" type="text/css"
        href='{{ asset('frontend') }}/public/library/slider-pro/slider-pro.min.css'>
    <link rel="stylesheet" type="text/css"
        href='{{ asset('frontend') }}/public/library/owl-carousel/v1.3.3/owl.carousel.min.css'>
    <link rel="stylesheet" type="text/css"
        href='{{ asset('frontend') }}/public/library/magnific-popup/magnific-popup.css'>
    <link rel="stylesheet" type="text/css" href='{{ asset('frontend') }}/public/library/pnotify/pnotify.css'>
    <link rel="stylesheet" type="text/css" href='{{ asset('frontend') }}/public/library/instagram/instafeed.min.css'>
    <link rel="stylesheet" type="text/css" href='{{ asset('frontend') }}/public/library/signature/signature.css'>

    <link rel="stylesheet" type="text/css"
        href='{{ asset('frontend') }}/themes/fnail02p/assets/css/ie10-viewport-bug-workaround.css'>
    <link rel="stylesheet" type="text/css" href='{{ asset('frontend') }}/themes/fnail02p/assets/css/meanmenu.css'>
    <link rel="stylesheet" type="text/css"
        href='{{ asset('frontend') }}/themes/fnail02p/assets/css/dropdown-submenu.css'>
    <link rel="stylesheet" type="text/css" href='{{ asset('frontend') }}/themes/fnail02p/assets/css/style.css'>
    <link rel="stylesheet" type="text/css" href='{{ asset('frontend') }}/themes/fnail02p/assets/css/responsive.css'>
    <link rel="stylesheet" type="text/css" href='{{ asset('frontend') }}/public/library/global/css/news.css'>
    <link rel="stylesheet" type="text/css" href='{{ asset('frontend') }}/public/library/global/css/giftcards.css'>
    <link rel="stylesheet" type="text/css" href='{{ asset('frontend') }}/public/library/global/css/cart-payment.css'>
    <link rel="stylesheet" type="text/css" href='{{ asset('frontend') }}/public/library/global/css/consent.css'>
    <link rel="stylesheet" type="text/css"
        href='{{ asset('frontend') }}/themes/fnail02p/assets/custom/css/custom.css?v=1.0'>

    <!-- CSS custom -->
    <style type="text/css">
        .sp-image-container {
            background-color: rgb(217, 199, 199);
        }

        .row.box-about-up {
            line-height: 23px;
        }

        #footer {
            padding: 16px 0px 10px;
        }

        .slider-pro img.sp-image {
            opacity: inherit;
        }

        ul.art-hmenu>li>a {
            color: #EF577A;
        }

        .page-child .art-header:after {
            background: rgba(255, 255, 255, 0.7);
        }

        .mfp-figure:after {
            box-shadow: none;
            background: none;
        }

        .mfp-title {
            display: none;
        }

        .staff-avatar {
            display: none;
        }

        #box-testimonial {
            background-image: url(/files/bg-testimonial.jpg);
            background-position: top right;
        }

        @media (min-width: 768px) {
            #box-testimonial {
                background-position: center right;
                background-attachment: fixed;
            }
        }

        @media only screen and (max-width: 687px) {
            .service-item .si-name {
                font-size: 12px;
            }
        }

        h5.si-name {
            font-weight: bold !important;
            color: #000 !important;
        }

        span.current {
            font-weight: 600 !important;
        }

    </style>

    <!-- Go to www.addthis.com/dashboard to customize your tools -->
    <script type="text/javascript"
        src="{{ asset('frontend') }}//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-59b8fec5dd14e0f2"></script>

    <script type="text/javascript">
        var addthis_share = {
            url: "http://fnail02p.fastboywebhosts.com/",
            title: "Jewelry Nail & Spa",
            description: "",
            media: ""
        }
    </script>

    <!-- JS Twitter -->

    <script type="text/javascript">
        var enable_giftcard_buy_custom = 1;
        var dateFormatBooking = "MM/DD/YYYY";
        var posFormat = "1,0,2";
        var timeMorning = '["10:00","10:30","11:00","11:30"]';
        var timeAfternoon =
            '["12:00","12:30","13:00","13:30","14:00","14:30","15:00","15:30","16:00","16:30","17:00","17:30","18:00","18:30","19:00"]';
        var bookLogin = "open_booking";
        var beforeTime = "1";
        var beforeDay = 0;
        var currDateT = "2021/05/24";
        var checktimebooking = 1;
        var enableRecaptcha = 0;
        var hoursTimeFormat = 12;
        var phoneFormat = "(000) 000-0000";
        var company_phone = "832-968-6668";
        var num_paging = "12";
        var site = "idx";
        var site_act = "";

        let facebook_embed = {
            id_fanpage: "https://www.facebook.com/FastboyMarketingAgency",
            appId: "",
            width: 450,
            height: 300,
            tabs: "timeline",
            show_facepile: true,
            small_header: true,
            likebox_enable: true,
            likebox_show_faces: false,
            likebox_stream: true,
        };
        let google_id_fanpage = "";
        let twitter_id_fanpage = "";
    </script>

    <!-- JS -->
    <script type="text/javascript" src="{{ asset('frontend') }}/public/library/jquery/v1.10.2/jquery.min.js">
    </script>
    <script type="text/javascript" src="{{ asset('frontend') }}/public/library/jquery-ui/jquery-ui.min.js"></script>
    <script type="text/javascript" src="{{ asset('frontend') }}/public/library/jquery-ui/jquery-migrate.min.js">
    </script>
    <script type="text/javascript" src="{{ asset('frontend') }}/public/library/validation/validation.min.js">
    </script>
    <script type="text/javascript" src="{{ asset('frontend') }}/public/library/mask/mask.min.js"></script>
    <script type="text/javascript" src="{{ asset('frontend') }}/public/library/bootstrap/v3.3.7/bootstrap.min.js">
    </script>
    <script type="text/javascript"
        src="{{ asset('frontend') }}/public/library/waiting-modal-dialog/waiting-modal-dialog.js"></script>
    <script type="text/javascript" src="{{ asset('frontend') }}/public/library/moment/moment-with-locales.js">
    </script>
    <script type="text/javascript"
        src="{{ asset('frontend') }}/public/library/bootstrap-datetimepicker/bootstrap-datetimepicker.js"></script>
    <script type="text/javascript" src="{{ asset('frontend') }}/public/library/magnific-popup/magnific-popup.min.js">
    </script>
    <script type="text/javascript" src="{{ asset('frontend') }}/public/library/pnotify/pnotify.js"></script>
    <script type="text/javascript" src="{{ asset('frontend') }}/public/library/slider-pro/slider-pro.min.js">
    </script>
    <script type="text/javascript" src="{{ asset('frontend') }}/public/library/owl-carousel/v1.3.3/owl.carousel.min.js">
    </script>
    <script type="text/javascript" src="{{ asset('frontend') }}/public/library/wow/wow.min.js"></script>
    <script type="text/javascript" src="{{ asset('frontend') }}/public/library/meanmenu/meanmenu.js"></script>
    <script type="text/javascript" src="{{ asset('frontend') }}/public/library/signature/signature.min.js"></script>
    <script type="text/javascript" src="{{ asset('frontend') }}/public/library/instagram/instafeed.min.js"></script>
    <script type="text/javascript" src="{{ asset('frontend') }}/themes/fnail02p/assets/custom/js/modernizr.custom.js">
    </script>
    <script type="text/javascript"
        src="{{ asset('frontend') }}/themes/fnail02p/assets/js/jquery.directional-hover.min.js"></script>
    <script type="text/javascript" src="{{ asset('frontend') }}/themes/fnail02p/assets/js/jquery.gridder.min.js">
    </script>
    <script type="text/javascript" src="{{ asset('frontend') }}/public/library/global/js/js_php.js"></script>

    <!-- Webmaster tools -->
    <!--Webmaster tools-->
    <!-- fb retargeting -->

    <!-- Schema Script -->

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    <meta name="csrf-token" content="{{ csrf_token() }}" />
</head>

<body class="custom-background">
    <!-- H1 and Seo -->
    <div id="fb-root" style="display: none"></div>
    <h1 itemprop="name" style="display: none"></h1>

    <div class="art-main page-home">

        <!--<input type="hidden" name="activeFreezeHeader" value="1">-->
        @php
            use App\Models\Album;
            use App\Models\Asset;
            $home_carousel = Album::where([['type', 'Home Carousel'], ['status', 'PUBLISHED']])->value('id');
            $imgs_carousel = Asset::where('albums_id', $home_carousel)
                ->orderBy('priority', 'desc')
                ->get();
            $header = App\Models\Product::where('category_id', App\Models\Category::where('name', 'Header And Footer')->value('id'))->first();
            if ($header) {
                $features = json_decode($header['features']);
            } else {
                $features = [];
            }
        @endphp


        @include('frontend.nail02-p.component.header',
        [
        'header'=>$header,
        'imgs_carousel'=>$imgs_carousel
        ])
        <main class="main-wrap">
            {{ $slot }}
            @include('frontend.nail02-p.component.footer',
            [
            'header'=>$header,
            ])
        </main>
        <footer class="footer-wrap">
            <!-- tpl footer_main layouts -->
            <section id="footer" class="footer">
                <div class="container">
                    <p itemprop="description">© Copyrights by Jewelry Nail & Spa. All Rights Reserved.</p>
                </div>
            </section>
        </footer>

        <!-- Tpl freeze footer -->
        <!-- Active freeze footer by delete style display: none -->
        @if ($features)
            <div class="freeze-footer">
                <ul>
                    <li><a href="tel:{{ explode(',', $features[1]->desc)[0] }}" title="Call us"
                            class="btn btn-default btn_call_now btn-call">{{ explode(',', $features[1]->desc)[0] }}</a>
                    </li>
                    <li><a href="/book" class="btn btn-default btn_make_appointment" title="Booking">Booking</a></li>
                </ul>
            </div>
        @endif

    </div>

    <!-- js check remove booking -->
    <script type="text/javascript">
        var enable_booking = "1";
        if (enable_booking == 0) {
            $(".btn_make_appointment").remove();
        }
    </script>

    <!-- JS -->
    <script type="text/javascript" src="{{ asset('frontend') }}/public/library/global/js/consent.js"></script>
    <script type="text/javascript" src="{{ asset('frontend') }}/themes/fnail02p/assets/js/script.js"></script>
    <script type="text/javascript" src="{{ asset('frontend') }}/themes/fnail02p/assets/custom/js/app_script.js?v=1.0">
    </script>
    <script type="text/javascript" src="{{ asset('frontend') }}/themes/fnail02p/assets/custom/js/app.js?v=1.0">
    </script>

    <!-- Google analytics -->
    <script>
        (function(i, s, o, g, r, a, m) {
            i['GoogleAnalyticsObject'] = r;
            i[r] = i[r] || function() {
                (i[r].q = i[r].q || []).push(arguments)
            }, i[r].l = 1 * new Date();
            a = s.createElement(o),
                m = s.getElementsByTagName(o)[0];
            a.async = 1;
            a.src = g;
            m.parentNode.insertBefore(a, m)
        })(window, document, 'script', 'https://www.google-analytics.com/analytics.js', 'ga');
        ga('create', '', 'auto');
        ga('send', 'pageview');
    </script>
    <script type="text/javascript"></script>
    <script>
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
    </script>
    @livewireScripts
</body>

</html>
