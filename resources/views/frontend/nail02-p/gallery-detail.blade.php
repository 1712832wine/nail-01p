<main class="main-wrap">
    <!-- tpl list_gallery -->
    <!-- support render gallery_data, gallery_fourcol_data, gallery_overlay_data -->
    <div class="page-header">
        <div class="container">
            <h2 itemprop="name" class="title">{{ $album['name'] }}</h2>
        </div>
    </div>
    <div class="box pGallery" id="box-about">
        <div class="container">
            <div class="text-center">
                <div class="gallery-box-wrap">
                    <div class="row">
                        @foreach ($list as $item)
                            <div class="col-xs-6 col-sm-4 col-md-4">
                                <div class="gallery-box pointer image-magnific-popup" data-group="gallery"
                                    title="Nails Design" href="{{ asset('storage') }}/photos/{{ $item['url'] }}">
                                    <div class="image-bg-wrap">
                                        <div class="image-bg"
                                            style="background-image: url('{{ asset('storage') }}/photos/{{ $item['url'] }}');">
                                            <img itemprop="image"
                                                src="{{ asset('storage') }}/photos/{{ $item['url'] }}">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        @endforeach
                    </div>
                </div>
                {{ $list->links('frontend.nail02-p.component.custom-pagination') }}
            </div>
        </div>
    </div>
</main>
