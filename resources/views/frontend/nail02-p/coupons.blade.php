<main class="main-wrap">
    <!-- tpl main -->
    <!-- support coupons_data, coupons_twocol_data -->
    <div class="page-header">
        <div class="container">
            <h2 itemprop="name" class="title">Coupons</h2>
        </div>
    </div>
    <div class="box pCoupons" id="box-about">
        <div class="container">
            <div class="row ">
                @foreach ($list as $item)
                    <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                        <div class="coupon_img_v1">
                            <div class="pointer image-magnific-popup" data-group="giftcards"
                                href="{{ asset('storage') }}/photos/{{ $item['url'] }}" title="Coupon">
                                <img itemprop="image" class="img-responsive"
                                    src="{{ asset('storage') }}/photos/{{ $item['url'] }}" alt="Coupon">
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>

            <div class="row">
                <div class="col-md-12">
                </div>
            </div>
        </div>
    </div>
