@php
if ($header) {
    $features = json_decode($header['features']);
    $extras = json_decode($header['extras']);
} else {
    $features = [];
    $extras = [];
}

@endphp
<div class="page-header">
    <div class="container">
        <h2 itemprop="name" class="title">Contact</h2>
    </div>
</div>
<div id="page-contact" class="container pContact">
    <div class="row">
        <div class="col-sm-6">
            <div itemprop="name" class="text-bold">Business Hours:</div>
            <div class="openhours-contact">
                <!-- support render openhours_data or openhoursshort_data -->
                <div class="foh-wrap">
                    @foreach ($extras as $item)
                        <div class="foh-row short" itemprop="openingHours"
                            content="{{ $item->name }}{{ $item->desc }}">
                            <span class="foh-date">{{ $item->name }}</span>
                            <span class="foh-time">{{ $item->desc }}</span>
                        </div>
                    @endforeach
                </div>
            </div>
            <div class="contact-contact">
                <div class="fci-wrap">
                    <div class="fci-row phone-wrap">
                        <span class="fci-title">Tel:</span>
                        <span class="fci-content">
                            @if ($features)
                                @foreach (explode(',', $features[1]->desc) as $item)
                                    <a href="tel:{{ $item }}" title="Call Us">
                                        <span itemprop="telephone" class="phone">{{ $item }}</span>
                                    </a>
                                    <br>
                                @endforeach
                            @endif

                        </span>
                    </div>
                    <div class="fci-row email-wrap">
                        <span class="fci-title">Email:</span>
                        <span class="fci-content">
                            @if ($features)
                                @foreach (explode(',', $features[2]->desc) as $item)
                                    <a href="mailto:{{ $item }}">
                                        <span itemprop="email" class="email">{{ $item }}</span>
                                    </a>
                                    <br />
                                @endforeach
                            @endif

                        </span>
                    </div>
                    <div class="fci-row  address-wrap" itemprop="address" itemscope
                        itemtype="http://schema.org/PostalAddress">
                        <span class="fci-title">Address:</span>
                        @if ($features)
                            <span class="fci-content">
                                <span itemprop="streetAddress" class="address">{{ $features[0]->desc }}</span>
                            </span>
                        @endif

                    </div>
                </div>
            </div>
            <p></p>
            <h3 itemprop="name">Get in touch with us</h3>
            <div class="wpcf7" dir="ltr" id="wpcf7-f143-o1" lang="en-US" role="form">
                <div class="screen-reader-response"></div>
                <section class="clearfix contact-form-wrap">
                    <div class="box-msg box-msg-" style=""></div>

                    <script src='https://www.google.com/recaptcha/api.js?hl=en' async defer></script>
                    <!-- Google reCaptcha -->
                    <script type="text/javascript">
                        <!--
                        function ezyCaptcha_send_contact(token, is_submit) {
                            is_submit = 0;
                            if ($("#password").length) {
                                //$("input:password").val(md5(clean_input($("#password").val())));
                            }
                            return true;
                        }
                        //
                        -->
                    </script>
                    <form enctype="multipart/form-data" class="mycontact-form wpcf7-form form-contact" method="post"
                        name="send_contact" id="send_contact" action="{{ route('send_contact') }}"
                        accept-charset="UTF-8">
                        @csrf
                        <div class="clearfix row">
                            <div class="col-xs-12 col-sm-6">
                                <div class="form-group">
                                    <div class="form-input">
                                        <input class="form-control" name="contactname" type="text"
                                            data-validation="[NOTEMPTY]"
                                            data-validation-message="Please enter your name!" autocomplete="off"
                                            placeholder="Your Name (required)">
                                    </div>
                                </div>
                            </div>
                            <div class="col-xs-12 col-sm-6">
                                <div class="form-group">
                                    <div class="form-input">
                                        <input class="form-control" name="contactemail" type="text"
                                            data-validation="[EMAIL]" data-validation-message="Invalid email"
                                            autocomplete="off" placeholder="Your Email (required)">
                                    </div>
                                </div>
                            </div>
                            <div class="col-xs-12 col-sm-12">
                                <div class="form-group">
                                    <div class="form-input">
                                        <input class="form-control" name="contactsubject" type="text"
                                            data-validation="[NOTEMPTY]"
                                            data-validation-message="Please enter a subject!" autocomplete="off"
                                            placeholder="Your Subject (required)">
                                    </div>
                                </div>
                            </div>
                            <div class="col-xs-12 col-sm-12">
                                <div class="form-group">
                                    <div class="form-input">
                                        <textarea class="form-control" name="contactcontent" rows="6"
                                            data-validation="[NOTEMPTY]"
                                            data-validation-message="Please enter a content!" autocomplete="off"
                                            placeholder="Your Message (required)"></textarea>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xs-12 col-sm-12">
                                <div class="form-group">
                                    @if (session('status'))
                                        <div class="alert alert-success" role="alert">
                                            <button type="button" class="close" data-dismiss="alert">×</button>
                                            {{ session('status') }}
                                        </div>
                                    @elseif(session('failed'))
                                        <div class="alert alert-danger" role="alert">
                                            <button type="button" class="close" data-dismiss="alert">×</button>
                                            {{ session('failed') }}
                                        </div>
                                    @endif
                                </div>
                            </div>
                            <div class="col-xs-12 col-sm-12">
                                <div class="form-group form-group-btn text-center">
                                    <button
                                        class="wpcf7-form-control wpcf7-submit btn btn-default btn-send btn_contact "
                                        type="submit">Send</button>
                                </div>
                            </div>
                        </div>
                    </form>
                </section>
            </div>
        </div>
        <div class="col-sm-6">
            <div id="map">
                <div itemscope itemprop="hasMap" itemtype="http://schema.org/Map" id="map-container"
                    class="map-container"><iframe
                        src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3464.7391505606774!2d-95.57299108550652!3d29.727314340440024!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x8640ddc6ed681049%3A0x74ace03ed50728e8!2sFast+Boy+Marketing!5e0!3m2!1sen!2s!4v1510909605272"
                        width="100%" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
                </div>
            </div>
        </div>
    </div>
</div>


<!-- js check remove booking -->
<script type="text/javascript">
    var enable_booking = "1";
    if (enable_booking == 0) {
        $(".btn_make_appointment").remove();
    }
</script>


<!-- Google analytics -->
<script>
    (function(i, s, o, g, r, a, m) {
        i['GoogleAnalyticsObject'] = r;
        i[r] = i[r] || function() {
            (i[r].q = i[r].q || []).push(arguments)
        }, i[r].l = 1 * new Date();
        a = s.createElement(o),
            m = s.getElementsByTagName(o)[0];
        a.async = 1;
        a.src = g;
        m.parentNode.insertBefore(a, m)
    })(window, document, 'script', 'https://www.google-analytics.com/analytics.js', 'ga');
    ga('create', '', 'auto');
    ga('send', 'pageview');
</script>

<!-- gg adwords remarketing -->

<!-- Custom js -->
<script type="text/javascript"></script>

<!-- js Popup -->

<!-- Js pnotify -->
</body>

</html>
