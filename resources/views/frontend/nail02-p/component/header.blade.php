<header class="header-wrap">
    <!-- tpl header_main layouts -->
    <section class="art-header">
        <div class="top-header">
            <div class="wrap-freeze-header-mobile clearfix hidden-md hidden-lg">
                <div class="flag-freeze-header-mobile">
                    <div class="mobile-menu-wrap">
                        <div class="menu_mobile_v1">
                            <div class="mobile_menu_container_v1">
                                <div class="mobile_logo stable">
                                    <div class="stable-row">
                                        <div class="stable-cell">
                                            @if ($header && count(json_decode($header['images'])) > 0)
                                                <a class="logo" href="/">
                                                    <img src="{{ asset('storage') }}/photos/{{ json_decode($header['images'])[0] }}"
                                                        alt="{{ $header['title'] }}" itemprop="logo">
                                                </a>
                                            @endif
                                        </div>
                                        <div class="stable-cell account-mobile-here"></div>
                                    </div>
                                </div>
                                <div class="mobile_menu_container_v1">
                                    <div class="mobile-menu clearfix">
                                        <nav id="mobile_dropdown" class="menu-mobile-wrap">
                                            <!-- tpl menu_mobile layouts -->
                                            <!-- Show cart then render account_mobile layout in element contain class account-mobile-here -->
                                            <ul class="clearlist">
                                                <li><a href="/" itemprop="url"><span itemprop="name">Home</span></a>
                                                </li>
                                                <li><a href="/about" itemprop="url"><span itemprop="name">About
                                                            Us</span></a></li>
                                                <li><a href="/services" itemprop="url"><span
                                                            itemprop="name">Services</span></a></li>
                                                <li><a href="/coupons" itemprop="url"><span
                                                            itemprop="name">Coupons</span></a></li>
                                                <li><a href="/gallery" itemprop="url"><span
                                                            itemprop="name">Gallery</span></a></li>
                                                <li class="btn_make_appointment"><a href="/book" itemprop="url"><span
                                                            itemprop="name">Booking</span></a>
                                                </li>
                                                <li><a href="/contact" itemprop="url"><span itemprop="name">Contact
                                                            Us</span></a></li>
                                            </ul>
                                        </nav>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="container hidden-xs hidden-sm">
                <div class="row">
                    <div class="col-md-12">
                        <div class="desktop-logo hidden-sm hidden-xs">
                            @if ($header && count(json_decode($header['images'])) > 0)
                                <a class="logo" href="/">
                                    <img src="{{ asset('storage') }}/photos/{{ json_decode($header['images'])[0] }}"
                                        alt="{{ $header['title'] }}" itemprop="logo">
                                </a>
                            @endif
                        </div>
                    </div>
                </div>
            </div>

            <div class="wrap-freeze-header clearfix hidden-xs hidden-sm">
                <div class="flag-freeze-header">
                    <div class="container">
                        <div class="row">
                            <div class="col-md-12">
                                <nav class="navbar art-nav hidden-sm hidden-xs account-main-here">
                                    <ul class="nav navbar-nav art-hmenu menu_main">
                                        <li><a href="/" itemprop="url"><span itemprop="name">Home</span></a>
                                        </li>
                                        <li><a href="/about" itemprop="url"><span itemprop="name">About
                                                    Us</span></a></li>
                                        <li><a href="/services" itemprop="url"><span itemprop="name">Services</span></a>
                                        </li>
                                        <li><a href="/coupons" itemprop="url"><span itemprop="name">Coupons</span></a>
                                        </li>
                                        <li><a href="/gallery" itemprop="url"><span itemprop="name">Gallery</span></a>
                                        </li>
                                        <li class="btn_make_appointment"><a href="/book" itemprop="url"><span
                                                    itemprop="name">Booking</span></a></li>
                                        <li><a href="/contact" itemprop="url"><span itemprop="name">Contact
                                                    Us</span></a></li>
                                    </ul>
                                </nav>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="slider-pro-wrap home-bottom-item-inner">
            <div class="section-slider-wrap">
                <section class="section section-slider">
                    {{-- USE FOR CALCULATOR START WIDTH HEIGHT
                    custom height with css:
                    .slider-width-height .fixed {
                        height: 0px; /* - set 0 if auto height, set numbers greater than zero if fixed height - */
                    } --}}
                    @if (count($imgs_carousel) > 0)
                        <div class="slider-width-height"
                            style="display: inline-block; width: 100%; height: 1px; overflow: hidden;">
                            <div class="fixed" style="width: 100%;"></div>
                            <img src="{{ asset('storage') }}/photos/{{ $imgs_carousel[0]['url'] }}"
                                style="width: 100%; height: auto;">
                        </div><!-- END USE FOR CALCULATOR START WIDTH HEIGHT -->
                    @endif


                    <div class="slider-pro" id="my-slider" style="display: none;">
                        <div class="sp-slides">
                            @foreach ($imgs_carousel as $img)
                                <div class="sp-slide">
                                    <div class="sp-layer sp-static">
                                        <img class="sp-image" src="{{ asset('storage') }}/photos/{{ $img['url'] }}"
                                            alt="s222.jpg">
                                    </div>
                                </div>
                            @endforeach
                        </div>
                    </div>
                    <div class="slider-pro" id="my-slider-fixed-height" style="display: none;">
                        <div class="sp-slides">
                            @foreach ($imgs_carousel as $img)
                                <div class="sp-slide">
                                    <div class="sp-layer sp-static" data-width="100%" data-height="100%"
                                        style="width: 100%;height: 100%;background: url('{{ asset('storage') }}/photos/{{ $img['url'] }}') center center no-repeat;background-size: cover;">
                                    </div>
                                </div>
                            @endforeach
                        </div>
                    </div>
                </section>
            </div>
        </div>
    </section>
</header>
