@php
if ($header) {
    $features = json_decode($header['features']);
    $extras = json_decode($header['extras']);
} else {
    $features = [];
    $extras = [];
}
@endphp
<section class="box" id="box-contact">
    <div class="container">
        <header class="heading heading-center">
            <h2 itemprop="name" class="title">Contact us</h2>
            <address class="desc">
                <div class="row">
                    <div class="col-md-6 openhours-board-wrap-master">
                        <div class="openhours-board-wrap">
                            <div itemprop="name">Business Hours:</div>
                            <div class="openhours-board">
                                <div class="openhours-board-inner">
                                    <!-- support render openhours_data or openhoursshort_data -->
                                    <div class="foh-wrap">
                                        @foreach ($extras as $extra)
                                            <div class="foh-row short" itemprop="openingHours"
                                                content="{{ $extra->desc }}">
                                                <span class="foh-date">{{ $extra->name }}</span>
                                                <span class="foh-time">{{ $extra->desc }}</span>
                                            </div>
                                        @endforeach
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6 contact-board-master">
                        <div class="contact-board">
                            <div class="contact-board-inner">
                                <div class="fci-wrap">
                                    <div class="fci-row phone-wrap"><span class="fci-title">Tel:</span>
                                        <span class="fci-content">
                                            @if ($features)
                                                @foreach (explode(',', $features[1]->desc) as $item)
                                                    <a href="tel:{{ $item }}" title="Call Us">
                                                        <span itemprop="telephone"
                                                            class="phone">{{ $item }}</span>
                                                    </a>
                                                    <br>
                                                @endforeach
                                            @endif

                                        </span>
                                    </div>
                                    <div class="fci-row email-wrap"><span class="fci-title">Email:</span>
                                        <span class="fci-content">
                                            @if ($features)
                                                @foreach (explode(',', $features[2]->desc) as $item)
                                                    <a href="mailto:{{ $item }}">
                                                        <span itemprop="email" class="email">{{ $item }}</span>
                                                    </a>
                                                    <br>
                                                @endforeach
                                            @endif

                                        </span>
                                    </div>
                                    @if ($features)
                                        <div class="fci-row  address-wrap" itemprop="address" itemscope=""
                                            itemtype="http://schema.org/PostalAddress">
                                            <span class="fci-title">Address:</span>
                                            <span class="fci-content">
                                                <span itemprop="streetAddress"
                                                    class="address">{{ $features[0]->desc }}
                                                </span>
                                            </span>
                                        </div>
                                    @endif

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </address>
        </header>
    </div>
</section>
