@if ($paginator->hasPages())
    <div class="row text-center">
        <div class="col-md-12">
            <nav aria-label="Page navigation">
                <div class="row">
                    <div class="pagination text-center">
                        <ul>
                            @foreach ($elements as $element)
                                @if (is_array($element))
                                    @foreach ($element as $page => $url)
                                        @if ($page == $paginator->currentPage())
                                            <li class="active"><a href="#" class="hover-main-color">
                                                    {{ $page }}</a></li>
                                        @else
                                            <li><a href="{{ $url }}">{{ $page }}</a></li>
                                        @endif
                                    @endforeach
                                @endif
                            @endforeach
                        </ul>
                    </div>
                </div>
            </nav>
        </div>
    </div>
    <style>

    </style>
@endif
