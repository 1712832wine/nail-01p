    <div class="page-header">
        <div class="container">
            <h2 itemprop="name" class="title">Gallery</h2>
        </div>
    </div>
    <div class="box pGallery" id="box-about">
        <div class="container">
            <div class="text-center">
                <div class="gallery-box-wrap">
                    <div class="row">
                        @foreach ($albums as $index => $album)
                            <div class="col-xs-6 col-sm-4 col-md-4">
                                <a href="/gallery-detail/{{ $album['id'] }}">
                                    <div class="gallery-box">
                                        <div class="image-bg"
                                            style="background-image: url('{{ asset('storage') }}/photos/{{ $lists[$index][0]['url'] }}');">
                                            <img itemprop="image"
                                                src="{{ asset('storage') }}/photos/{{ $lists[$index][0]['url'] }}">
                                            <div class="gallery-title">
                                                <span itemprop="name">{{ $album['name'] }}</span>
                                            </div>
                                        </div>
                                    </div>
                                </a>
                            </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </div>
