@php
if ($header) {
    $features = json_decode($header['features']);
    $extras = json_decode($header['extras']);
} else {
    $features = [];
    $extras = [];
}

@endphp
<div class="page-header">
    <div class="container">
        <h2 itemprop="name" class="title">About Us</h2>
    </div>
</div>
<div id="box-about" class="box" itemscope="" itemtype="http://schema.org/AboutPage">
    <div class="container">
        @foreach ($articles as $item)
            <div class="row box-about-up">
                <div class="col-md-6">
                    @if (count(json_decode($item['image'])) > 0)
                        <img class="img-full wp-post-image"
                            src="{{ asset('storage') }}/photos/{{ json_decode($item['image'])[0] }}"
                            itemprop="image" caption="false" />
                    @endif
                </div>
                <div class="col-md-6">
                    <header class="heading">
                        <h2 itemprop="name" class="title">{{ $header['name'] }}</h2>
                    </header>
                    <div itemprop="text">
                        {!! $item['content'] !!}
                        <p class="MsoNormal-text">Call us for make an appointment <a
                                href="tel:{{ explode(',', $features[1]->desc)[0] }}"><b>{{ explode(',', $features[1]->desc)[0] }}</b></a>.
                        </p>
                        <a href="/services" class="btn btn-primary btn-special" itemprop="url">Our Services<i
                                class="fa fa-chevron-circle-right"></i></a>
                    </div>
                </div>
            </div>

        @endforeach

    </div>
</div>
