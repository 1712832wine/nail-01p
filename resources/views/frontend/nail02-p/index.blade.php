{{-- HOME ARTICLES --}}
<div class="section">
    <div class="container main-container box">
        <div class="row">
            <div class="col-md-6">
                @if ($home_article && count(json_decode($home_article['image'])) > 0)
                    <img src="{{ asset('storage') }}/photos/{{ json_decode($home_article['image'])[0] }}" alt="" />
                @endif

            </div>
            <div class="col-md-6" itemscope="" itemtype="http://schema.org/AboutPage">
                <header class="heading">
                    <h2 class="title"><span itemprop="name" class="about-span">About Us</span>
                        <div class="social-img-icons-wrap">
                            @foreach ($social as $item)
                                <a itemprop="url" class="social-img-icons facebook" target="_blank"
                                    href="{{ $item['name'] }}">
                                    <img src="{{ asset('storage') }}/photos/{{ $item['url'] }}">
                                    <img src="{{ asset('storage') }}/photos/{{ $item['url'] }}">
                                </a>
                            @endforeach
                        </div>
                    </h2>
                </header>
                <div itemprop="text">
                    @if ($home_article)
                        <p class="MsoNormal"> {{ $home_article['title'] }}</p>
                        {!! $home_article['content'] !!}
                        <a href="/about" class="btn btn-primary btn-special" itemprop="url">
                            View more <i class="fa fa-chevron-circle-right"></i>
                        </a>
                    @endif

                </div>
            </div>
        </div>
    </div>
</div>
{{-- SERVICES --}}
<div class="section " id="box-service">
    <div class="container main-container">
        <div class="row">
            @foreach ($services as $service)
                <div class="col-sm-4">
                    <div class="service-item">
                        <div class="thumb">
                            <img src="{{ asset('storage') }}/photos/{{ json_decode($service['images'])[0] }}"
                                alt="{{ $service['name'] }}" itemprop="image">
                        </div>
                        <a itemprop="url"
                            href="{{ route('services-detail', ['slug' => strtolower(trim(preg_replace('/[^A-Za-z0-9-]+/', '-', $service['name']))), 'service_id' => $service['id']]) }}"
                            title="{{ $service['name'] }}">
                            <h2 class="name" itemprop="name">{{ $service['name'] }}</h2>
                        </a>
                        <div class="description" itemprop="description"></div>
                    </div>
                </div>
            @endforeach
        </div>
    </div>
</div>
{{-- CUSTOMERS --}}
<section class="parallax-window box" id="box-testimonial">
    <div class="container">
        <header class="heading heading-center">
            <h2 itemprop="name" class="title">Testimonials</h2>
        </header>
        <div class="row">
            <div class="col-md-10 center-block">
                <div id="carousel-testimonial" class="owl-carousel">
                    @foreach ($carousel_customers as $item)
                        <div class="slide">
                            <p class="slide-excerpt">{!! $item['content'] !!}</p>
                            <h4 class="slide-name">{{ $item['title'] }}</h4>
                        </div>
                    @endforeach
                </div>
            </div>
        </div>
    </div>
</section>
