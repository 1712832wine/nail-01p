<main>
    <div class="pmain">
        <style type="text/css">
            .img-item-list-gallery img {
                width: 100%;
            }

            .title-standard h2 {
                text-align: center !important;
            }

            .read-more {
                text-align: center !important;
            }

            .item-services-2 ul li p {
                padding-left: 10px;
                padding-right: 10px;
                color: #828282;
                font-size: 13px;
            }

        </style>
        <div class="page-heading">
            <div class="container">
                <div class="page-heading-inner">
                    <h2><span>About Us</span></h2>
                    <ul class="breadcrumb">
                        <li><a href="/">Home</a></li>
                        <li><span>About Us</span></li>
                    </ul>
                    <a class="go-button">
                        <svg width="20" height="20" viewBox="0 0 20 20" xmlns="http://www.w3.org/2000/svg" ratio="1">
                            <polyline fill="none" stroke="#000" points="10 14 5 9.5 10 5"></polyline>
                            <line fill="none" stroke="#000" x1="16" y1="9.5" x2="5" y2="9.52"></line>
                        </svg>
                        Back
                    </a>
                </div>
            </div>
        </div>
        <div class="in-container">
            <div class="container">
                <div class="instroduce">
                    @foreach ($articles as $item)
                        <div class="item-about-us row">
                            <div class="img-item-about-us col-md-4">
                                @if (count(json_decode($item['image'])) > 0)
                                    <a itemprop="url" href="/">
                                        <img itemprop="image"
                                            src="{{ asset('storage') }}/photos/{{ json_decode($item['image'])[0] }}"
                                            caption="false" />
                                    </a>
                                @endif
                            </div>
                            <div itemprop="description" class="content-item-about-us col-md-8">
                                <h2 class="title-about-us" itemprop="name" style="text-align: center;">
                                    {{ $item['title'] }}
                                </h2>
                                {!! $item['content'] !!}
                            </div>
                        </div>
                    @endforeach
                </div>
            </div>
        </div>
    </div>
</main>
