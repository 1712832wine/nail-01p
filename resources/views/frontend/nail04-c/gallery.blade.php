<main>
    <div class="pmain">

        <div class="page-heading">
            <div class="container">
                <div class="page-heading-inner">
                    <h2><span>Gallery</span></h2>
                    <ul class="breadcrumb">
                        <li><a href="/">Home</a></li>
                        <li><span>Gallery</span></li>
                    </ul>
                    <a class="go-button">
                        <svg width="20" height="20" viewBox="0 0 20 20" xmlns="http://www.w3.org/2000/svg" ratio="1">
                            <polyline fill="none" stroke="#000" points="10 14 5 9.5 10 5"></polyline>
                            <line fill="none" stroke="#000" x1="16" y1="9.5" x2="5" y2="9.52"></line>
                        </svg>
                        Back
                    </a>
                </div>

            </div>
        </div>
        <div class="in-container">
            <!-- CONTAINER -->
            <div class="main-content">
                <!-- Gallery page 1 -->
                <div class="gallery-style-1 bg-fa gallery-inpage ">
                    <div class="cate-gallery hidden-xs">
                        <div class="container">
                            <ul id="filter">
                                @foreach ($albums as $index => $album)
                                    <li itemprop="name">
                                        <a itemprop="{{ $album['id'] }}">{{ $album['name'] }}</a>
                                    </li>
                                @endforeach
                            </ul>
                        </div>
                    </div>
                    <div class="hidden-sm hidden-md hidden-lg hidden-xl">
                        <div class="please-choose">Please choose categories</div>
                        <div class="form-group col-md-12 col-xs-12">
                            <select id="filter_select" class="form-control select_tab" autocomplete="off"
                                name="filter_select">
                                <!-- Category data -->
                                @foreach ($albums as $index => $album)
                                    <option value="{{ $album['id'] }}">{{ $album['name'] }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="list-gallery">
                        <div class="container">
                            <div class="box_list_gallery">
                            </div>
                        </div>
                    </div>
                </div>
                <nav class="text-center box_paging container"></nav>
            </div>
        </div>
    </div>
</main>
