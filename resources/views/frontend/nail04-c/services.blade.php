<main>
    <div class="pmain">
        <div class="page-heading">
            <div class="container">
                <div class="page-heading-inner">
                    <h2><span>Services</span></h2>
                    <ul class="breadcrumb">
                        <li><a href="/">Home</a></li>
                        <li><span>Services</span></li>
                    </ul>
                    <a class="go-button">
                        <svg width="20" height="20" viewBox="0 0 20 20" xmlns="http://www.w3.org/2000/svg" ratio="1">
                            <polyline fill="none" stroke="#000" points="10 14 5 9.5 10 5"></polyline>
                            <line fill="none" stroke="#000" x1="16" y1="9.5" x2="5" y2="9.52"></line>
                        </svg>
                        Back
                    </a>
                </div>

            </div>
        </div>


        <div class="in-container">
            <div class="container">
                <div class="service-container">
                    <div class="row">
                        <div class="col-sm-6"></div>
                        <div class="col-sm-6 top-right">
                            <div class="btn_service_defale"></div>
                        </div>
                    </div>
                    <div class="main-content animation_sroll_jumpto">
                        <div class="container page-container">
                            <!-- Use for get current id -->
                            <input type="hidden" name="group_id" value="{{ $service_id }}" />
                            @foreach ($services as $index => $service)
                                <div class="row sroll_jumpto" id="sci_{{ $service['id'] }}">
                                    <div id="pink_white_powder_nails"></div>
                                    <div class="col-md-1 hidden-xs hidden-sm"></div>
                                    <div class="col-sm-4 col-md-3 text-center">
                                        <img class="img-responsive" style="padding-top: 20px"
                                            src="{{ asset('storage') }}/photos/{{ json_decode($service['images'])[0] }}"
                                            alt="{{ $service['name'] }}">
                                    </div>
                                    <div class="col-sm-8 col-md-7">
                                        <h1 class="service-name">{{ $service['name'] }}</h1>
                                        <p class="cat_title"></p>
                                        @foreach (json_decode($service['features']) as $item)
                                            @switch($item->name)
                                                @case('desc')
                                                    <p>{{ $item->desc }}</p>
                                                @break
                                                @case('center')
                                                @break
                                                @default
                                                    <div class="box_service">
                                                        <div class="detail-price-item">
                                                            <span class="detail-price-name">{{ $item->name }}</span>
                                                            <span class="detail-price-dots"></span>
                                                            <span class="detail-price-number">{{ $item->desc }}</span>
                                                        </div>
                                                        <p></p>
                                                    </div>
                                            @endswitch
                                        @endforeach
                                    </div>
                                    <div class="col-md-1 hidden-xs hidden-sm"></div>
                                </div>
                            @endforeach
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</main>
