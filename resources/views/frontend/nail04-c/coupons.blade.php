<main>
    <div class="pmain">
        <div class="page-heading">
            <div class="container">
                <div class="page-heading-inner">
                    <h2><span>Coupons</span></h2>
                    <ul class="breadcrumb">
                        <li><a href="/">Home</a></li>
                        <li><span>Coupons</span></li>
                    </ul>
                    <a class="go-button">
                        <svg width="20" height="20" viewBox="0 0 20 20" xmlns="http://www.w3.org/2000/svg" ratio="1">
                            <polyline fill="none" stroke="#000" points="10 14 5 9.5 10 5"></polyline>
                            <line fill="none" stroke="#000" x1="16" y1="9.5" x2="5" y2="9.52"></line>
                        </svg>
                        Back
                    </a>
                </div>
            </div>
        </div>
        <!-- module 3 -->
        <section class="in-container">
            <div class="container">
                <section class="coupon_code_v1">
                    <div class="row ">
                        <!-- Start list coupons -->
                        @foreach ($list as $item)
                            <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                                <div class="coupon_img_v1">
                                    <a rel="group2" href="{{ asset('storage') }}/photos/{{ $item['url'] }}"
                                        class="coupon_gift">
                                        <img itemprop="image" class="img_size"
                                            src="{{ asset('storage') }}/photos/{{ $item['url'] }}" alt="Coupon">
                                    </a>
                                </div>
                            </div>
                        @endforeach
                    </div>
                </section>
            </div>
        </section>
    </div>
</main>
