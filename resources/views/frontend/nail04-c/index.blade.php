<main>
    <div class="pmain">
        {{-- CAROUSEL --}}
        <section class="section-slider">
            <div class="slider-pro" id="my-slider">
                <div class="sp-slides">
                    @foreach ($imgs_carousel as $img)
                        <div class="sp-slide">
                            <img itemprop="image" class="sp-image"
                                src="{{ asset('storage') }}/photos/{{ $img['url'] }}"
                                alt="dreamstime_l_39359034.png" />
                        </div>
                    @endforeach
                </div>
            </div>
        </section>
        {{-- ABOUT ARTICLE --}}
        <section class="section section-about">
            <div class="container">
                <div class="center-title wow fadeInDown">
                    <div class="sub-title">WELCOME TO</div>
                    <h2>Fnail04c Nails Spa</h2>
                </div>
                <div class="about-list wow fadeInUp">
                    <div class="row">
                        @foreach ($home_asset as $item)
                            <div class="col-sm-4">
                                <div class="item">
                                    <div class="iconhome"><img
                                            src="{{ asset('storage') }}/photos/{{ $item['url'] }}" alt="" />
                                    </div>
                                    <h3>{{ $item['name'] }}</h3>
                                    <p>{{ $item['content'] }}</p>
                                    <a href="/about" class="icon-link"><i class="fa fa-long-arrow-right"></i></a>
                                </div>
                            </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </section>
        {{-- SERVICES --}}
        <section class="section section-services">
            <div class="container">
                <div class="center-title wow fadeInDown">
                    <div class="sub-title">OUR MOST</div>
                    <h2>POPULAR SERVICES</h2>
                </div>
                <div class="services-list wow fadeInUp">
                    <link rel="stylesheet"
                        href="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.2.1/assets/owl.carousel.min.css">
                    <script src="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.2.1/owl.carousel.min.js">
                    </script>
                    <script type="text/javascript">
                        $(document).ready(function() {
                            $(".owl_service_board").owlCarousel({
                                // loop:true,
                                margin: 30,
                                nav: false,
                                dots: false,
                                navContainerClass: 'carousel-nav-btn-gc',
                                navText: ['<i class="fa fa-angle-left"></i>',
                                    '<i class="fa fa-angle-right"></i>'
                                ],
                                navClass: ['carousel-nav-left', 'carousel-nav-right'],
                                responsive: {
                                    0: {
                                        items: 1
                                    },
                                    760: {
                                        items: 2
                                    },
                                    1000: {
                                        items: 3
                                    }
                                }
                            });
                        });

                    </script>
                    <div class="row">
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <div class="display_flex owl-carousel owl-theme owl_service_board">
                                @foreach ($services as $service)
                                    <div class="item">
                                        <div class="thumb">
                                            <a
                                                href="{{ route('services-detail', ['slug' => strtolower(trim(preg_replace('/[^A-Za-z0-9-]+/', '-', $service['name']))), 'service_id' => $service['id']]) }}"><img
                                                    itemprop="image"
                                                    src="{{ asset('storage') }}/photos/{{ json_decode($service['images'])[0] }}"
                                                    data-src="{{ asset('storage') }}/photos/{{ json_decode($service['images'])[0] }}"
                                                    alt="{{ $service['name'] }}" class="imgrps"></a>
                                        </div>
                                        <div class="info">
                                            <h3><a
                                                    href="{{ route('services-detail', ['slug' => strtolower(trim(preg_replace('/[^A-Za-z0-9-]+/', '-', $service['name']))), 'service_id' => $service['id']]) }}">{{ $service['name'] }}</a>
                                            </h3>
                                            <a href="{{ route('services-detail', ['slug' => strtolower(trim(preg_replace('/[^A-Za-z0-9-]+/', '-', $service['name']))), 'service_id' => $service['id']]) }}"
                                                class="icon-link"><i class="fa fa-long-arrow-right"></i></a>
                                        </div>
                                    </div>

                                @endforeach
                            </div>
                        </div>
                        <style>
                            .section-service .cover {
                                width: 100%;
                                height: auto;
                                margin: 0;
                                padding: 0;
                                padding-bottom: 75%;
                                background-repeat: no-repeat;
                                background-attachment: scroll;
                                background-clip: border-box;
                                background-origin: padding-box;
                                background-position-x: left;
                                background-position-y: top;
                                background-size: cover;
                                -webkit-transition: all .5s;
                                -moz-transition: all .5s;
                                -o-transition: all .5s;
                                transition: all .5s;
                                display: block;
                            }

                            .owl-carousel {
                                position: relative;
                            }

                            .owl-carousel .carousel-nav-btn-gc {
                                top: 30%;
                                width: 100%;
                                font-size: 32px;
                            }

                            .owl-carousel .carousel-nav-btn-gc .carousel-nav-left {
                                padding: 5px 10px;
                                cursor: pointer;
                                position: absolute;
                                top: 30%;
                                left: 10px;
                            }

                            .owl-carousel .carousel-nav-btn-gc .carousel-nav-right {
                                padding: 5px 10px;
                                cursor: pointer;
                                position: absolute;
                                top: 30%;
                                right: 10px;
                            }

                            @media (max-width: 768px) {

                                .owl-carousel .carousel-nav-btn-gc .carousel-nav-left {
                                    top: 25%;
                                    left: 0px;
                                }

                                .owl-carousel .carousel-nav-btn-gc .carousel-nav-right {
                                    top: 25%;
                                    right: 0px;
                                }


                            }

                        </style>
                    </div>
                </div>
            </div>
        </section>
        {{-- OUR CHANNEL --}}
        @if ($home_video)
            <section class="section section-video">
                <div class="container">
                    <div class="video-inner">
                        <div class="text-right wow fadeInLeft">
                            <p>Follow us</p>
                        </div>
                        <div class="text-center"><a class="popup-youtube" href="{{ $home_video['content'] }}"><i
                                    class="fa fa-play"></i></a></div>
                        <div class="text-left wow fadeInRight">
                            <p>Our Channel</p>
                        </div>
                    </div>
                </div>
            </section>
        @endif
        {{-- HOME ARTICLE --}}
        <section class="section-tips">
            <div class="tips-inner">
                @if (count(json_decode($home_article['image'])) > 0)
                    <div class="tips-left"
                        style="background-image: url('{{ asset('storage') }}/photos/{{ json_decode($home_article['image'])[0] }}')">
                    </div>
                @endif
                <div class="tips-right  wow fadeInRight">
                    <div class="m-title">
                        <div class="sub-title">ABOUT US</div>
                        <h3> {{ $home_article['title'] }}</h3>
                    </div>
                    {!! $home_article['content'] !!}
                </div>
            </div>
        </section>
        {{-- CUSTOMER --}}
        <section class="section section-client">
            <div class="container-fluid">
                <div class="list-client owl-carousel">
                    @foreach ($carousel_customers as $item)
                        <div class="item">
                            <div class="item-header dflex">
                                <div class="thumb">
                                    @if (count(json_decode($item['image'])) > 0)
                                        <img src="{{ asset('storage') }}/photos/{{ json_decode($item['image'])[0] }}"
                                            class="imgrps" alt="" />
                                    @endif
                                </div>
                                <div class="item-header-info">
                                    <h4>{{ $item['title'] }}</h4>
                                    {{-- <p>Tupelo, MS 38801</p> --}}
                                </div>
                            </div>
                            <div class="item-text">
                                <div class="content">
                                    {!! $item['content'] !!}
                                </div>
                                <div class="item-rate">
                                    <i class="fa fa-star" aria-hidden="true"></i> <i class="fa fa-star"
                                        aria-hidden="true"></i> <i class="fa fa-star" aria-hidden="true"></i> <i
                                        class="fa fa-star" aria-hidden="true"></i> <i class="fa fa-star"
                                        aria-hidden="true"></i>
                                </div>
                            </div>
                        </div>
                    @endforeach
                </div>
            </div>
        </section>
        <script type="text/javascript">
            $(document).ready(function() {
                $("#owl-demo").owlCarousel({
                    autoPlay: 5000, //Set AutoPlay to 3 seconds
                    dots: true,
                    items: 1,
                    itemsDesktop: [1199, 3],
                    itemsDesktopSmall: [979, 3]
                });

            })

        </script>

</main>
