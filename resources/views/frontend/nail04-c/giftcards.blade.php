<main>
    <div class="pmain">
        <div class="page-heading">
            <div class="container">
                <div class="page-heading-inner">
                    <h2><span>Giftcards</span></h2>
                    <ul class="breadcrumb">
                        <li><a href="/">Home</a></li>
                        <li><span>Giftcards</span></li>
                    </ul>
                    <a class="go-button">
                        <svg width="20" height="20" viewBox="0 0 20 20" xmlns="http://www.w3.org/2000/svg" ratio="1">
                            <polyline fill="none" stroke="#000" points="10 14 5 9.5 10 5"></polyline>
                            <line fill="none" stroke="#000" x1="16" y1="9.5" x2="5" y2="9.52"></line>
                        </svg>
                        Back
                    </a>
                </div>
            </div>
        </div>
        <div class="in-container">
            <div class="container">
                <div class="in-content">
                    <div class="page-title">
                        <h2>Nails Spa e-Gift Cards</h2>
                        <p>Let your sweetheart know how much you love and care for him/her by sending our love
                            cards! Buy our gift card for your loved one.</p>
                    </div>
                    <div style="font-size: 20px" class="alert alert-warning text-center">
                        Our system is being upgraded.<br>
                        Your order has not been processed this time.<br>
                        We apologize for this inconvenience.<br>
                        Please contact: <a href="tel:832-968-6668" title="Call us">832-968-6668</a>
                    </div>
                    <div class="row">
                        <div class="col-xs-12 col-sm-6 col-md-6">
                            <div class="pointer m-magnific-popup" data-group="coupon" title="Merry Christmas"
                                href="http://fnail04c.fastboywebsites.com/uploads/fnail0xdqeu75/product/1555401131_img_product1555401131.png">
                                <div class="m-coupon-box">
                                    <img itemprop="image"
                                        src="http://fnail04c.fastboywebsites.com/uploads/fnail0xdqeu75/product/1555401131_img_product1555401131.png"
                                        alt="Merry Christmas">
                                    <div class="m-coupon-price">10% Off</div>
                                </div>
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-6 col-md-6">
                            <div class="pointer m-magnific-popup" data-group="coupon" title="Father&#39;s Day"
                                href="http://fnail04c.fastboywebsites.com/uploads/fnail0xdqeu75/product/1555401114_img_product1555401114.png">
                                <div class="m-coupon-box">
                                    <img itemprop="image"
                                        src="http://fnail04c.fastboywebsites.com/uploads/fnail0xdqeu75/product/1555401114_img_product1555401114.png"
                                        alt="Father&#39;s Day">
                                </div>
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-6 col-md-6">
                            <div class="pointer m-magnific-popup" data-group="coupon" title="Mother&#39;s Day"
                                href="http://fnail04c.fastboywebsites.com/uploads/fnail0xdqeu75/product/1555401113_img_product1555401113.png">
                                <div class="m-coupon-box">
                                    <img itemprop="image"
                                        src="http://fnail04c.fastboywebsites.com/uploads/fnail0xdqeu75/product/1555401113_img_product1555401113.png"
                                        alt="Mother&#39;s Day">
                                </div>
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-6 col-md-6">
                            <div class="pointer m-magnific-popup" data-group="coupon" title="Happy Birthday"
                                href="http://fnail04c.fastboywebsites.com/uploads/fnail0xdqeu75/product/1555401093_img_product1555401093.png">
                                <div class="m-coupon-box">
                                    <img itemprop="image"
                                        src="http://fnail04c.fastboywebsites.com/uploads/fnail0xdqeu75/product/1555401093_img_product1555401093.png"
                                        alt="Happy Birthday">
                                </div>
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-6 col-md-6">
                            <div class="pointer m-magnific-popup" data-group="coupon" title="Valentine Day"
                                href="http://fnail04c.fastboywebsites.com/uploads/fnail0xdqeu75/product/1555401077_img_product1555401077.png">
                                <div class="m-coupon-box">
                                    <img itemprop="image"
                                        src="http://fnail04c.fastboywebsites.com/uploads/fnail0xdqeu75/product/1555401077_img_product1555401077.png"
                                        alt="Valentine Day">
                                </div>
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-6 col-md-6">
                            <div class="pointer m-magnific-popup" data-group="coupon" title="New Year"
                                href="http://fnail04c.fastboywebsites.com/uploads/fnail0xdqeu75/product/1555401071_img_product1555401071.png">
                                <div class="m-coupon-box">
                                    <img itemprop="image"
                                        src="http://fnail04c.fastboywebsites.com/uploads/fnail0xdqeu75/product/1555401071_img_product1555401071.png"
                                        alt="New Year">
                                </div>
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-6 col-md-6">
                            <div class="pointer m-magnific-popup" data-group="coupon" title="Graduation Day"
                                href="http://fnail04c.fastboywebsites.com/uploads/fnail0xdqeu75/product/1555401050_img_product1555401050.png">
                                <div class="m-coupon-box">
                                    <img itemprop="image"
                                        src="http://fnail04c.fastboywebsites.com/uploads/fnail0xdqeu75/product/1555401050_img_product1555401050.png"
                                        alt="Graduation Day">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12 text-center">
                            <div class="row">
                                <div class=" text-center">
                                    <ul class="pagination">
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</main>
