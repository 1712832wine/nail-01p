<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" dir="ltr" lang="en" xmlns:og="http://ogp.me/ns#"
    xmlns:fb="http://www.facebook.com/2008/fbml" itemscope itemtype="https://schema.org/NailSalon">

<head>
    <!-- Language -->
    <meta http-equiv="content-language" content="en" />
    <link rel="alternate" href="http://fnail04c.fastboywebsites.com" hreflang="x-default">
    <link rel="alternate" href="http://fnail04c.fastboywebsites.com" hreflang="en-US">

    <!-- Meta limit -->

    <!-- Meta Tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width,initial-scale=1.0" />
    <meta http-equiv="content-type" content="text/html; charset=UTF-8" />
    <meta name="description" content="" />
    <meta name="keywords" content="" />
    <meta name="author" content="" />

    <!-- OG -->
    <meta property="og:title" content="" />
    <meta property="og:description" content="" />
    <meta property="og:image" content="" />
    <meta property="og:url" content="http://fnail04c.fastboywebsites.com/" />
    <meta property="og:type" content="website" />
    <meta property="og:site_name" content="Fnail04c Nails Spa" />

    <!-- Dublin Core -->
    <link rel="schema.DC" href="http://purl.org/dc/elements/1.1/">
    <meta name="DC.title" content="">
    <meta name="DC.identifier" content="http://fnail04c.fastboywebsites.com/">
    <meta name="DC.description" content="">
    <meta name="DC.subject" content="">
    <meta name="DC.language" scheme="UTF-8" content="en-us">
    <meta itemprop="priceRange" name="priceRange" content="">

    <!-- GEO meta -->
    <meta name="geo.region" content="">
    <meta name="geo.placename" content="">
    <meta name="geo.position" content="">
    <meta name="ICBM" content="">

    <!-- Page Title -->
    <title>Fnail04c Nails Spa</title>
    <base href="/themes/fnail04c/assets/">

    <!-- canonical -->
    <link rel="canonical" href="http://fnail04c.fastboywebsites.com">



    <!-- Favicons -->
    <link rel="icon" href="http://fnail04c.fastboywebsites.com/uploads/fnail0xdqeu75/attach/1555397714_fbm_fvc.png"
        type="image/x-icon">
    <link rel="shortcut icon"
        href="http://fnail04c.fastboywebsites.com/uploads/fnail0xdqeu75/attach/1555397714_fbm_fvc.png"
        type="image/x-icon">

    <link href="https://fonts.googleapis.com/css?family=Quicksand:400,500,700" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,700" rel="stylesheet">



    <link rel="stylesheet" type="text/css" href='{{ asset('frontend') }}/public/library/jquery-ui/jquery-ui.min.css'>
    <link rel="stylesheet" type="text/css" href='{{ asset('frontend') }}/public/library/animate/animate.css'>
    <link rel="stylesheet" type="text/css" href='{{ asset('frontend') }}/public/library/validation/validation.css'>
    <link rel="stylesheet" type="text/css"
        href='{{ asset('frontend') }}/public/library/bootstrap/v3.3.7/bootstrap.min.css'>
    <link rel="stylesheet" type="text/css"
        href='{{ asset('frontend') }}/public/library/font-awesome/v4.7.0/css/font-awesome.min.css'>
    <link rel="stylesheet" type="text/css"
        href='{{ asset('frontend') }}/public/library/owl-carousel/v2.3.4/owl.carousel.min.css'>
    <link rel="stylesheet" type="text/css"
        href='{{ asset('frontend') }}/public/library/slider-pro/slider-pro.min.css'>
    <link rel="stylesheet" type="text/css"
        href='{{ asset('frontend') }}/public/library/bootstrap-datetimepicker/bootstrap-datetimepicker.min.css'>
    <link rel="stylesheet" type="text/css"
        href='{{ asset('frontend') }}/public/library/magnific-popup/magnific-popup.css'>
    <link rel="stylesheet" type="text/css" href='{{ asset('frontend') }}/public/library/flex-label/flex-label.css'>
    <link rel="stylesheet" type="text/css" href='{{ asset('frontend') }}/public/library/pnotify/pnotify.css'>
    <link rel="stylesheet" type="text/css" href='{{ asset('frontend') }}/public/library/signature/signature.css'>
    <link rel="stylesheet" type="text/css" href='{{ asset('frontend') }}/public/library/instagram/instafeed.min.css'>

    <link rel="stylesheet" type="text/css" href='{{ asset('frontend') }}/themes/fnail04c/assets/css/meanmenu.css'>
    <link rel="stylesheet" type="text/css"
        href='{{ asset('frontend') }}/themes/fnail04c/assets/css/dropdown-submenu.css'>
    <link rel="stylesheet" type="text/css" href='{{ asset('frontend') }}/themes/fnail04c/assets/css/style.css'>
    <link rel="stylesheet" type="text/css" href='{{ asset('frontend') }}/themes/fnail04c/assets/css/app.css'>
    <link rel="stylesheet" type="text/css" href='{{ asset('frontend') }}/themes/fnail04c/assets/css/responsive.css'>
    <link rel="stylesheet" type="text/css" href='{{ asset('frontend') }}/public/library/global/css/news.css'>
    <link rel="stylesheet" type="text/css" href='{{ asset('frontend') }}/public/library/global/css/giftcards.css'>
    <link rel="stylesheet" type="text/css" href='{{ asset('frontend') }}/public/library/global/css/cart-payment.css'>
    <link rel="stylesheet" type="text/css" href='{{ asset('frontend') }}/public/library/global/css/consent.css'>
    <link rel="stylesheet" type="text/css"
        href='{{ asset('frontend') }}/themes/fnail04c/assets/css/custom.css?v=1.0'>

    <style type="text/css">
        .box_img_giftcard.active,
        .box_img_giftcard:hover {
            border-color: #eb3b5b;
        }

        .bootstrap-datetimepicker-widget table td.active,
        .bootstrap-datetimepicker-widget table td.active:hover,
        .active .circle_check,
        .info_inner_booking .open_booking:hover,
        a.btn,
        .btn,
        .btn-default,
        a.btn-default,
        .btn-primary,
        a.btn-primary,
        .btn,
        .call-to,
        .call-to .h3,
        .btn_send_newsletter,
        .about-list .item .icon,
        .contact-heading span,
        .btn-primary {
            border-color: #eb3b5b;
            background-color: #eb3b5b;
            color: #ffffff;

        }

        .about-list .item .icon {
            box-shadow: 0 0 0 5px #ffffff, 0 0 0 6px #eb3b5b;
        }

        h2,
        h1,
        h3,
        h4,
        h5,
        h1.service-name,
        h3 a,
        .title_appointment,
        .color-main,
        .title_gift,
        .list_service optgroup,
        .in-container-service h4,
        .price-item-number,
        .nav-left.stickUp .navbar-nav>li>a:hover,
        .nav-left.nav-white .navbar-nav>li>a:hover,
        .service-name {
            color: #eb3b5b;
        }

        .modal_form_header h4 {
            color: #ffffff;
        }

        .list_service optgroup option {
            color: #040404;
        }

        .circle-service-image {
            border: 3px solid #eb3b5b;
        }

        .box_account_v1 .modal_form_header {
            border-bottom: 1px solid #eb3b5b;
            background-color: #eb3b5b;
            color: #ffffff;
        }

        .footer-navcolor,
        .icon-link,
        .navbar-nav>li>a:before,
        .about-list .item h3:after,
        .pagination>li>a:focus,
        .pagination>li>a:hover,
        .pagination>li>span:focus,
        .pagination>li>span:hover,
        .pagination>.active>a,
        .pagination>.active>a:focus,
        .pagination>.active>a:hover,
        .pagination>.active>span,
        .pagination>.active>span:focus,
        .pagination>.active>span:hover {
            color: #ffffff;
            background-color: #eb3b5b;
            border-color: #eb3b5b;
        }

        .block_cart .form-control,
        .ab out-list .item {
            border: 1px solid #eb3b5b;
        }

        .about-list .item:hover {
            box-shadow: 0px 5px 15px 0px #eb3b5b30;
        }


        /*
    mau sác phụ
     */



        .footer {
            border-color: #17171c;
            background-color: #17171c;
            color: free_theme_color_3;
        }

        .footer-nav li span,
        .footer-main h5,
        .footer-nav li a,
        .footer-social a {
            color: free_theme_color_3;
        }

        .btn-primary:hover,
        .btn:hover,
        .about-list .item:hover .icon,
        .btn-primary.active.focus,
        .btn-primary.active:focus,
        .btn-primary.active:hover,
        .btn-primary:active.focus,
        .btn-primary:active:focus,
        .btn-primary:active:hover,
        .open>.dropdown-toggle.btn-primary.focus,
        .open>.dropdown-toggle.btn-primary:focus,
        .open>.dropdown-toggle.btn-primary:hover {
            border-color: #17171c;
            background-color: #17171c;
            color: free_theme_color_3;
        }

    </style> <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

    <script type="text/javascript">
        var enable_giftcard_buy_custom = 1;
        var dateFormatBooking = "MM/DD/YYYY";
        var posFormat = "1,0,2";
        var timeMorning = '["10:00","10:30","11:00","11:30"]';
        var timeAfternoon =
            '["12:00","12:30","13:00","13:30","14:00","14:30","15:00","15:30","16:00","16:30","17:00","17:30","18:00","18:30","19:00"]';
        var bookLogin = "open_booking";
        var beforeTime = "1";
        var beforeDay = 0;
        var currDateT = "2021/05/24";
        var checktimebooking = 1;
        var enableRecaptcha = 0;
        var hoursTimeFormat = 12;
        var phoneFormat = "(000) 000-0000";
        var company_phone = "832-968-6668";
        var num_paging = "12";
        var menu = "idx";

        let facebook_embed = {
            id_fanpage: "https://www.facebook.com/FastboyMarketingAgency",
            appId: "",
            width: 450,
            height: 300,
            tabs: "timeline",
            show_facepile: true,
            small_header: true,
            likebox_enable: true,
            likebox_show_faces: false,
            likebox_stream: true,
        };
        let google_id_fanpage = "";
        let twitter_id_fanpage = "";

    </script>



    <script type="text/javascript" src="{{ asset('frontend') }}/public/library/jquery/v1.10.2/jquery.min.js">
    </script>
    <script type="text/javascript" src="{{ asset('frontend') }}/public/library/jquery-ui/jquery-ui.min.js"></script>
    <script type="text/javascript" src="{{ asset('frontend') }}/public/library/jquery-ui/jquery-migrate.min.js">
    </script>
    <script type="text/javascript" src="{{ asset('frontend') }}/public/library/validation/validation.min.js">
    </script>
    <script type="text/javascript" src="{{ asset('frontend') }}/public/library/flex-label/flex-label.js"></script>
    <script type="text/javascript" src="{{ asset('frontend') }}/public/library/mask/mask.min.js"></script>
    <script type="text/javascript" src="{{ asset('frontend') }}/public/library/bootstrap/v3.3.7/bootstrap.min.js">
    </script>
    <script type="text/javascript"
        src="{{ asset('frontend') }}/public/library/waiting-modal-dialog/waiting-modal-dialog.js"></script>
    <script type="text/javascript" src="{{ asset('frontend') }}/public/library/moment/moment-with-locales.js">
    </script>
    <script type="text/javascript"
        src="{{ asset('frontend') }}/public/library/bootstrap-datetimepicker/bootstrap-datetimepicker.js"></script>
    <script type="text/javascript"
        src="{{ asset('frontend') }}/public/library/owl-carousel/v2.3.4/owl.carousel.min.js"></script>
    <script type="text/javascript" src="{{ asset('frontend') }}/public/library/wow/wow.min.js"></script>
    <script type="text/javascript" src="{{ asset('frontend') }}/public/library/slider-pro/slider-pro.min.js">
    </script>
    <script type="text/javascript" src="{{ asset('frontend') }}/public/library/magnific-popup/magnific-popup.min.js">
    </script>
    <script type="text/javascript" src="{{ asset('frontend') }}/public/library/meanmenu/meanmenu.js"></script>
    <script type="text/javascript" src="{{ asset('frontend') }}/public/library/parallax/parallax.min.js"></script>
    <script type="text/javascript" src="{{ asset('frontend') }}/public/library/pnotify/pnotify.js"></script>
    <script type="text/javascript" src="{{ asset('frontend') }}/public/library/instagram/instafeed.min.js"></script>
    <script type="text/javascript" src="{{ asset('frontend') }}/public/library/signature/signature.min.js"></script>
    <script type="text/javascript" src="{{ asset('frontend') }}/public/library/global/js/js_php.js"></script>


    <style type="text/css">
        .img-info-staff {
            display: none;
        }

        .navbar-nav>li>a {
            font-size: 14px;
        }

        .in-container {
            padding: 10px 0;
        }

        h2.title-about-us {
            text-align: center;
            font-size: 30px;
        }

        .detail-price-number {
            padding-left: 5px;
            font-size: 15px;
            color: #000000;
            font-weight: 500;
        }

        span.detail-price-name {
            color: #000000;
        }

        .detail-price-item {
            font-weight: normal !important;
        }

        .cat_title b {
            font-weight: normal !important;
            color: #000;
        }

        .cat_title {
            font-weight: normal;
            color: #000;
        }

        .footer-nav ul {
            text-align: left;
        }


        .tips-right {
            padding: 10px 20px;
        }

        .content {
            min-height: 140px;
        }

    </style>
    <!-- Go to www.addthis.com/dashboard to customize your tools -->
    <script type="text/javascript"
        src="{{ asset('frontend') }}//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-59b8fec5dd14e0f2"></script>

    <script type="text/javascript">
        var addthis_share = {
            url: "http://fnail04c.fastboywebsites.com/",
            title: "Fnail04c Nails Spa",
            description: "",
            media: ""

        }

    </script>

    <!-- Webmaster tools -->
    <!--Webmaster tools-->
    <!-- fb retargeting -->

    <!-- Schema Script -->
    <meta name="csrf-token" content="{{ csrf_token() }}" />
</head>

<body class="style2">
    <div class="art-main">
        <h1 style="display: none">Fnail04c Nails Spa</h1>
        {{-- HEADER --}}
        @include('frontend.nail04-c.component.header',
        ['header'=>
        App\Models\Product::where('category_id',
        App\Models\Category::where('name','Header And Footer')->first()['id'])->first()])
        {{-- MAIN --}}
        {{ $slot }}
        {{-- FOOTER --}}
        @php
            $album = App\Models\Album::where([['type', 'Social'], ['status', 'PUBLISHED']])->first();
            $social = App\Models\Asset::where('albums_id', $album['id'])->orderBy('priority', 'desc')->get();
        @endphp
        @include('frontend.nail04-c.component.footer',['header'=>
        App\Models\Product::where('category_id',
        App\Models\Category::where('name','Header And Footer')->first()['id'])->first(),
        'social'=>$social]))

    </div>

    <script type="text/javascript">
        var enable_booking = "1";
        if (enable_booking == 0) {
            $(".btn_make_appointment").remove();
        }

    </script>
    <script type="text/javascript" src="{{ asset('frontend') }}/public/library/global/js/consent.js"></script>
    <script type="text/javascript" src="{{ asset('frontend') }}/themes/fnail04c/assets/js/script.js"></script>
    <script type="text/javascript" src="{{ asset('frontend') }}/themes/fnail04c/assets/js/app.js?v=1.0"></script>
    <!-- Google analytics -->
    <script>
        (function(i, s, o, g, r, a, m) {
            i['GoogleAnalyticsObject'] = r;
            i[r] = i[r] || function() {
                (i[r].q = i[r].q || []).push(arguments)
            }, i[r].l = 1 * new Date();
            a = s.createElement(o),
                m = s.getElementsByTagName(o)[0];
            a.async = 1;
            a.src = g;
            m.parentNode.insertBefore(a, m)
        })(window, document, 'script', 'https://www.google-analytics.com/analytics.js', 'ga');

        ga('create', '', 'auto');
        ga('send', 'pageview');

    </script>
    <!-- gg adwords remarketing -->

    <script type="text/javascript"></script>
    <script>
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

    </script>
    @livewireScripts
</body>

</html>
