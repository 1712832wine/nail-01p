@php
$features = json_decode($header['features']);
$extras = json_decode($header['extras']);
@endphp
<footer class="footer">
    <div class="footer-main">
        <div class="container">
            <div class="row">
                <div class="col-md-4">
                    <div class="footer-nav footer-navcolor">
                        <h3>About Us</h3>
                        <ul>
                            <li>
                                <div class="icon"><i class="fa {{ $features[0]->name }}"></i></div>
                                <p class="contact_address">{{ $features[0]->desc }}
                                </p>
                            </li>
                            <li>
                                <div class="icon"><i class="fa {{ $features[1]->name }}"></i></div>
                                <p>{{ $features[1]->desc }}</p>
                            </li>
                            <li>
                                <div class="icon"><i class="fa {{ $features[2]->name }}"></i></div>
                                <p>{{ $features[2]->desc }}</p>
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="col-sm-4 col-md-4">
                    <div class="footer-nav footer-nav-padd">
                        <h3>Business Hours</h3>
                        <ul class="openingHours">
                            @foreach ($extras as $extra)
                                <li itemprop="openingHours" data="{{ $extra->desc }}">
                                    <spam><strong>{{ $extra->name }}</strong></spam>
                                    <span>{{ $extra->desc }}</span>
                                </li>
                            @endforeach
                        </ul>
                    </div>
                </div>
                <div class="col-sm-4 col-md-4">
                    <div class="footer-nav">
                        <h3>Connect Us</h3>
                        <ul class="footer-social">
                            @foreach ($social as $item)
                                <li>
                                    @if ($item['url'])
                                        <a itemprop="url" rel="nofollow" target="_blank" title="{{ $item['title'] }}"
                                            href="{{ $item['content'] }}">
                                            <img src="{{ asset('storage') }}/photos/{{ $item['url'] }}"
                                                alt="{{ $item['title'] }}" />
                                        </a>
                                    @endif
                                </li>
                            @endforeach
                        </ul>
                        <div class="header-social-page">
                            <!-- facebook fanpage -->
                            <div class="single_footer_widget">
                                <div class="box_fanpage_fb">
                                    <div id="fanpage_fb_container"></div>
                                </div>
                            </div>

                            <!-- use for calculator width -->
                            <div class="single_footer_widget">
                                <div id="social_block_width" style="width:100% !important; height: 1px !important">
                                </div>
                            </div>
                            <!-- End Single Footer Widget -->
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="footer-copyright">
        <div class="container">
            <div class="row">
                <div class="col-md-6">
                    <div class="copyright">
                        © Copyright by Fnail04c Nails Spa. All Rights Reserved.
                    </div>
                </div>
                <div class="col-md-6"></div>
            </div>
        </div>
    </div>
</footer>
<div class="freeze-footer">
    <ul>
        <li>
            <a href="tel:{{ explode(',', $features[2]->desc)[0] }}" class="btn btn-default btn_call_now btn-call">
                {{ explode(',', $features[2]->desc)[0] }}
            </a>
        </li>
        <li><a href="/book" class="btn btn-default btn_make_appointment">Booking</a></li>
    </ul>
</div>
