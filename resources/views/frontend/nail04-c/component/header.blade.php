<!-- TPL header_main -->
<header class="header">
    <div class="wrap-freeze-header">
        <div class="flag-freeze-header">
            <!-- Active freeze header by insert this html into the website page -->
            <!-- <input type="hidden" name="activeFreezeHeader" value="1">-->
            <div class="menu_mobile_v1 hidden-md hidden-lg menu-1024-hidden">
                <div class="mobile_logo">
                    <a class="logo" itemprop="url" href="/">
                        <img itemprop="logo" class="imgrps"
                            src="http://fnail04c.fastboywebsites.com/uploads/fnail0xdqeu75/attach/1555916752_logo04c.png"
                            alt="">
                    </a>
                </div>

                <div class="mobile_menu_container_v1">
                    <div class="mobile-menu clearfix">
                        <nav id="mobile_dropdown">
                            <ul>
                                <li itemprop="name"><a itemprop="url" href="/">Home</a></li>
                                <li><a itemprop="url" href="/about">About Us</a></li>
                                <li itemprop="name"><a itemprop="url" href="/services">Services</a></li>
                                <li itemprop="name"><a itemprop="url" class="btn_make_appointment"
                                        href="/book">Booking</a></li>
                                <li itemprop="name"><a itemprop="url" href="/coupons" title="">Coupons</a>
                                </li>
                                <li itemprop="name"><a itemprop="url" href="/gallery">Gallery</a></li>
                                <li itemprop="name"><a itemprop="url" href="/giftcards" title="">Gift
                                        Cards</a>
                                </li>
                                <li itemprop="name"><a itemprop="url" href="/contact">Contact Us</a></li>
                            </ul>
                        </nav>
                    </div>
                </div>
            </div>
            <div class="header-main hidden-xs hidden-sm">
                <div class="container">
                    <div class="header-nav"><a class="logo" itemprop="url" href="/">
                            <img itemprop="logo" class="imgrps"
                                src="http://fnail04c.fastboywebsites.com/uploads/fnail0xdqeu75/attach/1555916752_logo04c.png"
                                alt="">
                        </a>
                        <nav class="navbar main-nav">
                            <div id="desktop-nav" class="navbar-collapse collapse">
                                <ul class="nav navbar-nav nav-main">
                                    <li itemprop="name"><a itemprop="url" href="/">Home</a></li>
                                    <li><a itemprop="url" href="/about">About Us</a></li>
                                    <li itemprop="name"><a itemprop="url" href="/services">Services</a></li>
                                    <li itemprop="name"><a itemprop="url" href="/coupons" title="">Coupons</a>
                                    </li>
                                    <li itemprop="name"><a itemprop="url" href="/gallery">Gallery</a></li>
                                    <li itemprop="name"><a itemprop="url" href="/giftcards" title="">Gift
                                            Cards</a></li>
                                    <li itemprop="name"><a itemprop="url" href="/contact">Contact Us</a>
                                    </li>
                                </ul>
                            </div>
                        </nav>
                        <div class="navbar-right"><a href="/book" class="btn btn_make_appointment">Book
                                Now</a>
                        </div>
                    </div>
                </div>
                <div class="line">
                    <div></div>
                </div>
            </div>
        </div>
    </div>
</header>
