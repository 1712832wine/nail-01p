<main class="main">

    <div class="breadcrumb-area">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="breadcrumb-inner">
                        <h2 class="page-title">Gallery</h2>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="gallery-section padding-bottom-100 padding-top-100">
        <div class="container">
            <ul class="clearfix m-category-tab" id="category_tab">
                @foreach ($albums as $index => $album)
                    <li class="tab @if ($index===0) active @endif"
                        data-id="{{ $album['id'] }}">
                        <span itemprop="name">{{ $album['name'] }}</span>
                    </li>
                @endforeach
            </ul>
            <div class="clearfix m-gallery-content" id="gallery_content">
                <div class="clearfix m-gallery-listing listing"></div>
                <div class="clearfix m-gallery-paging paging"></div>
            </div>
        </div>
    </div>
</main>
