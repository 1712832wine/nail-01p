<main class="main">

    <div class="breadcrumb-area">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="breadcrumb-inner">
                        <h2 class="page-title">Coupons</h2>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="gallery-section padding-bottom-100 padding-top-100">
        <div class="container">
            <div class="row">
                @foreach ($list as $item)
                    <div class="col-xs-12 col-sm-6 col-md-6">
                        <div class="pointer m-magnific-popup" data-group="coupon" title="Coupon"
                            href="{{ asset('storage') }}/photos/{{ $item['url'] }}">
                            <div class="m-coupon-box">
                                <img itemprop="image" src="{{ asset('storage') }}/photos/{{ $item['url'] }}"
                                    alt="Coupon" />
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>
            <div class="row">
                <div class="col-md-12 text-center">
                    <nav aria-label="Page navigation"></nav>
                </div>
            </div>
        </div>
    </div>
</main>
