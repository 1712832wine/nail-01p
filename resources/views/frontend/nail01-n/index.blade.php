<main class="main">

    <div class="pmain">
        {{-- CAROUSEL --}}
        <div class="section-slider-wrap">
            <section class="section-slider">
                @if (count($imgs_carousel) > 0)
                    <div class="slider-width-height"
                        style="display: inline-block;width: 100%;height: 1px;overflow: hidden;">
                        <div class="fixed" style="width: 100%"></div>
                        <img src="{{ asset('storage') }}/photos/{{ $imgs_carousel[0]['url'] }}"
                            style="width: 100%; height: auto" alt="" />
                    </div>
                @endif
                <div class="slider-pro" id="my-slider" style="display: none">
                    <div class="sp-slides">
                        @foreach ($imgs_carousel as $img)
                            <div class="sp-slide">
                                <div class="sp-layer sp-static">
                                    <a href="{{ route('home') }}" target="_self" title="slide7.png">
                                        <img class="sp-image" src="{{ asset('storage') }}/photos/{{ $img['url'] }}"
                                            alt="slide7.png" />
                                    </a>
                                </div>
                            </div>
                        @endforeach
                    </div>
                </div>
                <div class="slider-pro" id="my-slider-fixed-height" style="display: none">
                    <div class="sp-slides">
                        @foreach ($imgs_carousel as $img)
                            <div class="sp-slide">
                                <a href="{{ route('home') }}" target="_self" title="slide7.png">
                                    <div class="sp-layer sp-static" data-width="100%" data-height="100%" style="
                                        width: 100%;
                                        height: 100%;
                                        background: url('{{ asset('storage') }}/photos/{{ $img['url'] }}')
                                        center center no-repeat;
                                        background-size: cover;"></div>
                                </a>
                            </div>
                        @endforeach
                    </div>
                </div>
            </section>
        </div>
        {{-- HOME ARTICLE --}}
        <div class="work-section-four bg-white padding-top-90 padding-bottom-100">
            <div class="nav-container">
                <div class="row">
                    <div class="col-lg-5 col-md-6">
                        @if (count(json_decode($home_article['image'])) > 0)
                            <div class="text-center">
                                <img itemprop="image"
                                    src="{{ asset('storage') }}/photos/{{ json_decode($home_article['image'])[0] }}"
                                    caption="false" />
                            </div>
                        @endif
                    </div>
                    <div class="col-lg-7 col-md-6">
                        <div class="work-text-wrapper">
                            <div class="work-text">
                                <span class="title" itemprop="title">Welcome To</span>
                                <div class="section-title work-title">
                                    <h2 class="title" itemprop="title">
                                        {{ $home_article['title'] }}
                                    </h2>
                                </div>
                                {!! $home_article['content'] !!}
                                <div class="counterup-area-work">
                                    <div class="single-counter-item">
                                        <div class="content">
                                            <h2 class="count-num plus">1080</h2>
                                            <h4 class="title">Project Complete</h4>
                                        </div>
                                    </div>
                                    <div class="single-counter-item">
                                        <div class="content">
                                            <h2 class="count-num plus">345</h2>
                                            <h4 class="title">Happy Clients</h4>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        {{-- SERVICES --}}
        <section class="service-four bg-white padding-bottom-100">
            <div class="nav-container">
                <div class="row">
                    <div class="col-md-12">
                        <div class="
                    testi-title
                    section-title
                    padding-bottom-55 padding-top-95
                  ">
                            <h2 class="title" itemprop="title">Our Service</h2>
                            <p>Professional Nail Care for Ladies and Gentleman</p>
                        </div>
                    </div>
                </div>
                <div class="service-list">
                    <div class="service-items">
                        <div class="row">
                            @foreach ($services as $service)
                                <div class="break-row break-row-2"></div>
                                <div class="col-md-3 col-sm-6">
                                    <a itemprop="url"
                                        href="{{ route('services-detail', ['slug' => strtolower(trim(preg_replace('/[^A-Za-z0-9-]+/', '-', $service['name']))), 'service_id' => $service['id']]) }}"
                                        title="{{ $service['name'] }}">
                                        <div class="service-content service-item">
                                            <div class="service-img">
                                                <img itemprop="image"
                                                    src="{{ asset('storage') }}/photos/{{ json_decode($service['images'])[0] }}"
                                                    class="imgrps" alt="{{ $service['name'] }}" />
                                            </div>
                                            <div class="service-text">
                                                <div class="name" itemprop="text">
                                                    {{ $service['name'] }}
                                                </div>
                                            </div>
                                        </div>
                                    </a>
                                </div>
                            @endforeach
                        </div>
                    </div>
                </div>
            </div>
        </section>
        {{-- GALLERY --}}
        <section class="creative-team-two padding-top-90 padding-bottom-30">
            <div class="nav-container">
                <div class="row">
                    <div class="col-lg-12 col-md-12">
                        <div class="testi-title section-title padding-bottom-55">
                            <h2 class="title" itemprop="title">OUR PHOTOS</h2>
                            <p>
                                {{ $gallery['short_desc'] }}
                            </p>
                        </div>
                    </div>
                </div>
                <div class="gallery-list">
                    <div class="m-gallery-box-wrap">
                        <div class="row">
                            @foreach ($gallery_list as $item)
                                <div class="col-6 col-sm-4 col-md-3">
                                    <div class="pointer m-magnific-popup"
                                        href="{{ asset('storage') }}/photos/{{ $item['url'] }}');">
                                        <div class="m-gallery-box">
                                            <div class="m-image-bg"
                                                style="background-image: url('{{ asset('storage') }}/photos/{{ $item['url'] }}');">
                                                <img itemprop="image"
                                                    src="{{ asset('storage') }}/photos/{{ $item['url'] }}" />
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            @endforeach
                        </div>
                    </div>
                </div>
            </div>
        </section>
        {{-- CUSTOMERS --}}
        <section class="
            testimonial-section-four
            bg-lite-hover
            padding-bottom-160 padding-top-100
          ">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="testimonial-carousel-04 testimonial-inner-section">
                            @foreach ($carousel_customers as $item)
                                <div class="single-testimonial-item">
                                    <div class="hover">
                                        <div class="hover-inner">
                                            <div class="author-meta">
                                                <div class="icon">
                                                    <i class="fas fa-quote-left"></i>
                                                </div>
                                                {!! $item['content'] !!}
                                                <h4 class="name" itemprop="name">{{ $item['title'] }}</h4>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="testimonial-img">
                                        @if (count(json_decode($item['image'])) > 0)
                                            <div class="testimonial-img-item">
                                                <img itemprop="image"
                                                    src="{{ asset('storage') }}/photos/{{ json_decode($item['image'])[0] }}"
                                                    alt="" />
                                                <div class="d-nation">Customer</div>
                                            </div>
                                        @endif
                                    </div>
                                </div>
                            @endforeach
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
</main>
