<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" dir="ltr" lang="en" xmlns:og="http://ogp.me/ns#"
    xmlns:fb="http://www.facebook.com/2008/fbml" itemscope itemtype="https://schema.org/BeautySalon">

<head>
    <!-- Language -->
    <meta http-equiv="content-language" content="en" />
    <link rel="alternate" href="https://fnail01n.fbm.company" hreflang="x-default" />
    <link rel="alternate" href="https://fnail01n.fbm.company" hreflang="en-US" />

    <!-- Meta limit -->

    <!-- Meta Tags -->
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width,initial-scale=1.0" />
    <meta http-equiv="content-type" content="text/html; charset=UTF-8" />
    <meta name="description" content="" />
    <meta name="keywords" content="" />
    <meta name="author" content="" />

    <!-- OG -->
    <meta property="og:title" content="" />
    <meta property="og:description" content="" />
    <meta property="og:image" content="" />
    <meta property="og:url" content="https://fnail01n.fbm.company/" />
    <meta property="og:type" content="" />
    <meta property="og:site_name" content="Flowers Nails & Spa" />

    <!-- Dublin Core -->
    <link rel="schema.DC" href="http://purl.org/dc/elements/1.1/" />
    <meta name="DC.title" content="" />
    <meta name="DC.identifier" content="https://fnail01n.fbm.company/" />
    <meta name="DC.description" content="" />
    <meta name="DC.subject" content="" />
    <meta name="DC.language" scheme="UTF-8" content="en-us" />
    <meta itemprop="priceRange" name="priceRange" content="" />

    <!-- GEO meta -->
    <meta name="geo.region" content="" />
    <meta name="geo.placename" content="" />
    <meta name="geo.position" content="" />
    <meta name="ICBM" content="" />

    <!-- Page Title -->
    <title>Flowers Nails & Spa</title>
    <base href="{{ asset('frontend') }}/themes/fnail01n/assets/" />

    <!-- canonical -->
    <link rel="canonical" href="https://fnail01n.fbm.company" />

    <!-- Favicons -->
    <link rel="icon" href="https://fnail01n.fbm.company/uploads/fnail0arlsjpl/attach/1571367676_fbm_fvc.png"
        type="image/png" />
    <link rel="shortcut icon" href="https://fnail01n.fbm.company/uploads/fnail0arlsjpl/attach/1571367676_fbm_fvc.png"
        type="image/png" />
    <link rel="apple-touch-icon" href="https://fnail01n.fbm.company/uploads/fnail0arlsjpl/attach/1571367676_fbm_fvc.png"
        type="image/png" />

    <!-- CSS -->

    <link rel="stylesheet" type="text/css"
        href="{{ asset('frontend') }}/public/assets/webfnail01n965eb49803b2f9734ba44fa4f154f9e1.css" />

    <link rel="stylesheet" type="text/css"
        href="{{ asset('frontend') }}/public/assets/webfnail01nfbd473779c76a13babe48f0d9f71016c.css" />

    <!-- CSS EXTEND -->
    <style type="text/css">
        @media (min-width: 992px) {
            .service-items .col-md-3.col-sm-6 {
                max-width: 33.33%;
                flex: 0 0 33.33%;
            }
        }

    </style>

    <!-- JS -->
    <script type="text/javascript"
        src="{{ asset('frontend') }}/public/assets/webfnail01na63d488c31077a418ea1f564d6628704.js"></script>

    <script type="text/javascript">
        let webForm = {
            required: "(required)",
            optional: "(optional)",
            any_person: "Any person",
            price: "Price",
            morning: "Morning",
            afternoon: "Afternoon",
            sunday: "Sunday",
            monday: "Monday",
            tuesday: "Tuesday",
            wednesday: "Wednesday",
            thursday: "Thursday",
            friday: "Friday",
            saturday: "Saturday",
            jan: "Jan",
            feb: "Feb",
            mar: "Mar",
            apr: "Apr",
            may: "May",
            jun: "Jun",
            jul: "Jul",
            aug: "Aug",
            sep: "Sep",
            oct: "Oct",
            nov: "Nov",
            dec: "Dec",
            contact_name: "Your name",
            contact_name_placeholder: "",
            contact_name_maxlength: "76",
            contact_email: "Your email address",
            contact_email_placeholder: "",
            contact_email_maxlength: "76",
            contact_phone: "Your phone",
            contact_phone_placeholder: "",
            contact_phone_maxlength: "16",
            contact_subject: "Your subject",
            contact_subject_placeholder: "",
            contact_subject_maxlength: "251",
            contact_message: "Your message",
            contact_message_placeholder: "",
            contact_message_maxlength: "501",
            contact_btn_send: "Send Us",
            contact_name_err: "Please enter your name",
            contact_email_err: "Please enter your email",
            contact_phone_err: "Please enter your phone",
            contact_subject_err: "Please enter your subject",
            contact_message_err: "Please enter your message",
            booking_name: "Your name",
            booking_name_placeholder: "",
            booking_name_maxlength: "76",
            booking_phone: "Your phone",
            booking_phone_placeholder: "",
            booking_phone_maxlength: "16",
            booking_email: "Your email",
            booking_email_placeholder: "",
            booking_email_maxlength: "76",
            booking_service: "Service",
            booking_service_placeholder: "Select service",
            booking_menu: "Menu",
            booking_menu_placeholder: "Select menu",
            booking_technician: "Technician",
            booking_technician_placeholder: "Select technician",
            booking_person_number: "Number",
            booking_date: "Date",
            booking_date_placeholder: "",
            booking_date_maxlength: "16",
            booking_hours: "Hour",
            booking_hours_placeholder: "Select hour",
            booking_note: "Note",
            booking_note_maxlength: "201",
            booking_note_placeholder: "(Max length 200 character)",
            booking_store: "Storefront",
            booking_store_placeholder: "Select storefront",
            booking_add_another_service: "Add another service",
            booking_information: "Appointment Information",
            booking_order_information: "Order Information",
            booking_popup_message: "Message",
            booking_popup_confirm: "Confirm booking information ?",
            booking_popup_confirm_description: "We will send a text message to you via the number below after we confirm the calendar for your booking.",
            booking_order_popup_confirm: "Confirm order information ?",
            booking_order_popup_confirm_description: "We will send a text message to you via the number below after we confirm the calendar for your order.",
            booking_btn_send: "Send appointment now",
            booking_btn_search: "Search",
            booking_btn_booking: "Booking",
            booking_btn_confirm: "Confirm",
            booking_btn_cancel: "Cancel",
            booking_hours_expired: "Has expired",
            booking_name_err: "Please enter your name",
            booking_phone_err: "Please enter your phone",
            booking_email_err: "Please enter your email",
            booking_service_err: "Please choose a service",
            booking_menu_err: "Please choose a menu",
            booking_technician_err: "Please choose a technician",
            booking_date_err: "Please choose date",
            booking_hours_err: "Please choose hour",
            booking_get_hours_timeout: "Network timeout, Please click the button search to try again",
        };
        let webBooking = {
            enable: true,
            minDate: "05/24/2021",
            requiredTechnician: false,
            requiredEmail: false,
            requiredHour: true,
            isRestaurant: false,
        };
        let webFormat = {
            dateFormat: "MM/DD/YYYY",
            datePosition: "1,0,2",
            phoneFormat: "(000) 000-0000",
        };
        let webGlobal = {
            site: "idx",
            siteAct: "",
            siteSubAct: "",
            noPhoto: "/public/library/global/no-photo.jpg",
            isTablet: false,
            isMobile: false,
            enableRecaptcha: false,
        };
        let facebook_embed = {
            id_fanpage: "https://www.facebook.com/FastboyMarketingAgency",
            appId: "",
            width: 450,
            height: 300,
            tabs: "timeline",
            show_facepile: true,
            small_header: true,
            likebox_enable: true,
            likebox_show_faces: false,
            likebox_stream: true,
        };
        let google_id_fanpage = "";
        let twitter_id_fanpage = "";

    </script>
    <meta name="csrf-token" content="{{ csrf_token() }}" />
</head>

<body class="site-idx site-act-">
    <div id="fb-root"></div>
    <div style="height: 0; overflow: hidden">
        <h1 itemprop="name">Super Star Nails &amp; Spa</h1>
    </div>
    <input type="hidden" name="activeFreezeHeader" value="1" />
    {{-- HEADER --}}
    @php
        $album = App\Models\Album::where([['type', 'Social'], ['status', 'PUBLISHED']])->first();
        $social = App\Models\Asset::where('albums_id', $album['id'])->orderBy('priority', 'desc')->get();
        $header = App\Models\Product::where('category_id', App\Models\Category::where('name', 'Header And Footer')->first()['id'])->first();
    @endphp
    @include('frontend.nail01-n.component.header',
    ['header'=>$header])
    {{-- MAIN --}}
    {{ $slot }}
    {{-- FOOTER --}}
    @include('frontend.nail01-n.component.footer',[
    'header'=>$header,
    'social'=>$social])

    <script type="text/javascript">
        if (!webBooking.enable) {
            $(".btn_make_appointment").remove();
        }

    </script>
    <script type="text/javascript"
        src="{{ asset('frontend') }}/public/assets/webfnail01n22247cd19562c7718b6357a3843319f8.js"></script>
    <script>
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

    </script>
    <script type="text/javascript"></script>
    @livewireScripts
</body>

</html>
