<main class="main">

    <div class="breadcrumb-area">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="breadcrumb-inner">
                        <h2 class="page-title">Booking</h2>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="booking-section padding-bottom-100 padding-top-100">
        <div class="container">
            <section id="boxBookingForm" class="box-booking-form">
                <form id="formBooking" class="form-booking" name="formBooking" action="/book/add" method="post"
                    enctype="multipart/form-data">
                    <div class="row">
                        <div class="col-sm-6 col-md-4 col-lg-3 booking-date">
                            <div class="group-select">
                                <label>Date (required)</label>
                                <div class="relative w100 form-input-group">
                                    <input type="text" class="form-control form-text booking_date" autocomplete="off"
                                        name="booking_date" value="" data-validation="[NOTEMPTY]"
                                        data-validation-message="Please choose date" title="Booking date" placeholder=""
                                        maxlength="16" />
                                    <span class="fa fa-calendar calendar form-icon"></span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-12 col-md-12 col-lg-9">
                            <div class="row booking-service-staff booking-item" id="bookingItem_0">
                                <div class="col-sm-12 col-md-7 col-lg-8 booking-service">
                                    <div class="group-select">
                                        <label>Service (required)</label>
                                        <div class="relative w100">
                                            <select class="form-control booking_service" name="product_id[]"
                                                data-validation="[NOTEMPTY]"
                                                data-validation-message="Please choose a service"
                                                title="Booking service">
                                                <option value="">Select service</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-12 col-md-5 col-lg-4 booking-staff">
                                    <div class="group-select">
                                        <label>Technician (optional)</label>
                                        <div class="relative w100">
                                            <select class="form-control booking_staff" name="staff_id[]"
                                                title="Booking staff">
                                                <option value="">Select technician</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <!-- Button Add Item -->
                            <div class="clearfix">
                                <div class="add-services pointer booking_item_add">
                                    <i class="fa fa-plus-circle"></i> Add another service
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-12 col-md-12 col-lg-3">
                            <div class="group-select">
                                <label>&nbsp;</label>
                                <div class="relative w100">
                                    <button class="btn btn-search search_booking" type="button">
                                        Search
                                    </button>
                                </div>

                                <!-- Hidden data -->
                                <input type="hidden" name="booking_hours" class="booking_hours" value="" />
                                <input type="hidden" name="booking_name" class="booking_name" value="" />
                                <input type="hidden" name="booking_phone" class="booking_phone" value="" />
                                <input type="hidden" name="booking_email" class="booking_email" value="" />
                                <input type="hidden" name="notelist" class="notelist" value="" />
                                <input type="hidden" name="store_id" class="store_id" value="" />

                                <input type="hidden" name="booking_area_code" class="booking_area_code" value="1" />
                                <input type="hidden" name="booking_form_email" class="booking_form_email" value="0" />
                                <input type="hidden" name="nocaptcha" class="nocaptcha" value="1" />
                                <input type="hidden" name="g-recaptcha-response" class="g-recaptcha-response"
                                    value="" />
                            </div>
                        </div>
                    </div>
                </form>

                <script type="text/javascript">
                    $(document).ready(function() {
                        /* Init Booking */
                        webBookingForm.init(
                            '{"76":{"id":76,"name":"MANICURE & PEDICURE","services":[222,223,224,225,226,227,228,229,230,231,232,233,234,235,236]},"77":{"id":77,"name":"KIDS SERVICES 10 & UNDER","services":[237,238,239,240]},"78":{"id":78,"name":"NAIL ENHANCEMENTS","services":[241,242,243,244,245,246,247,248]},"79":{"id":79,"name":"SNS HEALTHY NAIL SYSTEM","services":[249,250,251]},"80":{"id":80,"name":"ADDITIONAL SERVICE","services":[252,253,254,255,256,257,258,259,260,261,262,263,264,265,266,267,268,269]},"81":{"id":81,"name":"WAXING SERVICE","services":[270,271,272,273,274,275,276,277,278]}}',
                            '{"222":{"id":222,"name":"Paradise Manicure","price":"$36 ","staffs":null,"schedule":false,"unit":"person","scheduleDay":null},"223":{"id":223,"name":"Paradise Pedicure","price":"$52 ","staffs":null,"schedule":false,"unit":"person","scheduleDay":null},"224":{"id":224,"name":"Pure Organic JELLY","price":"$0 ","staffs":null,"schedule":false,"unit":"person","scheduleDay":null},"225":{"id":225,"name":"Organic Jasmine Coconut","price":"$0 ","staffs":null,"schedule":false,"unit":"person","scheduleDay":null},"226":{"id":226,"name":"Organic Mandarin + Mango","price":"$0 ","staffs":null,"schedule":false,"unit":"person","scheduleDay":null},"227":{"id":227,"name":"Organic Milk & Honey","price":"$0 ","staffs":null,"schedule":false,"unit":"person","scheduleDay":null},"228":{"id":228,"name":"Organic Pitaya Dragon Fruit","price":"$0 ","staffs":null,"schedule":false,"unit":"person","scheduleDay":null},"229":{"id":229,"name":"Organic Tingling Mint + CBD","price":"$0 ","staffs":null,"schedule":false,"unit":"person","scheduleDay":null},"230":{"id":230,"name":"Organic Lemongrass + Green Tea","price":"$0 ","staffs":null,"schedule":false,"unit":"person","scheduleDay":null},"231":{"id":231,"name":"NEW Oxygen Caffe Macchiato","price":"$52 ","staffs":null,"schedule":false,"unit":"person","scheduleDay":null},"232":{"id":232,"name":"NEW Oxygen Mint Mimosa","price":"$52 ","staffs":null,"schedule":false,"unit":"person","scheduleDay":null},"233":{"id":233,"name":"Deluxe Manicure","price":"$26 ","staffs":null,"schedule":false,"unit":"person","scheduleDay":null},"234":{"id":234,"name":"Deluxe Pedicure","price":"$41 ","staffs":null,"schedule":false,"unit":"person","scheduleDay":null},"235":{"id":235,"name":"Classic Manicure","price":"$16 ","staffs":null,"schedule":false,"unit":"person","scheduleDay":null},"236":{"id":236,"name":"Classic Pedicure","price":"$23 ","staffs":null,"schedule":false,"unit":"person","scheduleDay":null},"237":{"id":237,"name":"Kids Spa","price":"$30 +","staffs":null,"schedule":false,"unit":"person","scheduleDay":null},"238":{"id":238,"name":"Kids Manicure","price":"$14 +","staffs":null,"schedule":false,"unit":"person","scheduleDay":null},"239":{"id":239,"name":"Kids Spa Pedicure","price":"$20 +","staffs":null,"schedule":false,"unit":"person","scheduleDay":null},"240":{"id":240,"name":"Polish Change","price":"$5 +","staffs":null,"schedule":false,"unit":"person","scheduleDay":null},"241":{"id":241,"name":"Full Set","price":"$32 +","staffs":null,"schedule":false,"unit":"person","scheduleDay":null},"242":{"id":242,"name":"Fill","price":"$25 +","staffs":null,"schedule":false,"unit":"person","scheduleDay":null},"243":{"id":243,"name":"Full Set P&W","price":"$45 +","staffs":null,"schedule":false,"unit":"person","scheduleDay":null},"244":{"id":244,"name":"Fill Pink","price":"$30 +","staffs":null,"schedule":false,"unit":"person","scheduleDay":null},"245":{"id":245,"name":"Fill Pink & White","price":"$40 +","staffs":null,"schedule":false,"unit":"person","scheduleDay":null},"246":{"id":246,"name":"Silk Wrap F\/S","price":"$40 +","staffs":null,"schedule":false,"unit":"person","scheduleDay":null},"247":{"id":247,"name":"Silk Wrap Fill","price":"$35 +","staffs":null,"schedule":false,"unit":"person","scheduleDay":null},"248":{"id":248,"name":"White Tip F\/S","price":"$35 +","staffs":null,"schedule":false,"unit":"person","scheduleDay":null},"249":{"id":249,"name":"SNS Full Colors","price":"$40 +","staffs":null,"schedule":false,"unit":"person","scheduleDay":null},"250":{"id":250,"name":"SNS Pink & White","price":"$50 +","staffs":null,"schedule":false,"unit":"person","scheduleDay":null},"251":{"id":251,"name":"SNS Ombré","price":"$50 +","staffs":null,"schedule":false,"unit":"person","scheduleDay":null},"252":{"id":252,"name":"Buff Shine","price":"$5 ","staffs":null,"schedule":false,"unit":"person","scheduleDay":null},"253":{"id":253,"name":"Callus Treatment","price":"$10 ","staffs":null,"schedule":false,"unit":"person","scheduleDay":null},"254":{"id":254,"name":"Paraffin Treatment","price":"$10 ","staffs":null,"schedule":false,"unit":"person","scheduleDay":null},"255":{"id":255,"name":"Sugar Scrub","price":"$10 ","staffs":null,"schedule":false,"unit":"person","scheduleDay":null},"256":{"id":256,"name":"Extra Foot Massage","price":"$10 ","staffs":null,"schedule":false,"unit":"person","scheduleDay":null},"257":{"id":257,"name":"10 mins Hot Stone Massage","price":"$15 ","staffs":null,"schedule":false,"unit":"person","scheduleDay":null},"258":{"id":258,"name":"Gel Polish W\/ Service","price":"$15 ","staffs":null,"schedule":false,"unit":"person","scheduleDay":null},"259":{"id":259,"name":"Gel French W\/ Service","price":"$23 ","staffs":null,"schedule":false,"unit":"person","scheduleDay":null},"260":{"id":260,"name":"Gel Polish Without Service","price":"$25 +","staffs":null,"schedule":false,"unit":"person","scheduleDay":null},"261":{"id":261,"name":"Gel French Without Service","price":"$33 +","staffs":null,"schedule":false,"unit":"person","scheduleDay":null},"262":{"id":262,"name":"Gel Topcoat","price":"$5 ","staffs":null,"schedule":false,"unit":"person","scheduleDay":null},"263":{"id":263,"name":"Shape Design","price":"$5 ","staffs":null,"schedule":false,"unit":"person","scheduleDay":null},"264":{"id":264,"name":"Nail Design","price":"$6 +","staffs":null,"schedule":false,"unit":"person","scheduleDay":null},"265":{"id":265,"name":"Nail Repair","price":"$5 +","staffs":null,"schedule":false,"unit":"person","scheduleDay":null},"266":{"id":266,"name":"Acrylic Removal","price":"$10 ","staffs":null,"schedule":false,"unit":"person","scheduleDay":null},"267":{"id":267,"name":"Gel Removal","price":"$8 ","staffs":null,"schedule":false,"unit":"person","scheduleDay":null},"268":{"id":268,"name":"Polish Change","price":"$10 +","staffs":null,"schedule":false,"unit":"person","scheduleDay":null},"269":{"id":269,"name":"Matt Topcoat","price":"$5 +","staffs":null,"schedule":false,"unit":"person","scheduleDay":null},"270":{"id":270,"name":"Eyebrow","price":"$13 ","staffs":null,"schedule":false,"unit":"person","scheduleDay":null},"271":{"id":271,"name":"Chin","price":"$10 ","staffs":null,"schedule":false,"unit":"person","scheduleDay":null},"272":{"id":272,"name":"Lip","price":"$6 ","staffs":null,"schedule":false,"unit":"person","scheduleDay":null},"273":{"id":273,"name":"Face","price":"$30 +","staffs":null,"schedule":false,"unit":"person","scheduleDay":null},"274":{"id":274,"name":"Half Arms","price":"$25 +","staffs":null,"schedule":false,"unit":"person","scheduleDay":null},"275":{"id":275,"name":"Full Arms","price":"$35 +","staffs":null,"schedule":false,"unit":"person","scheduleDay":null},"276":{"id":276,"name":"Half Legs","price":"$35 +","staffs":null,"schedule":false,"unit":"person","scheduleDay":null},"277":{"id":277,"name":"Full Legs","price":"$45 +","staffs":null,"schedule":false,"unit":"person","scheduleDay":null},"278":{"id":278,"name":"Under Arms","price":"$18 ","staffs":null,"schedule":false,"unit":"person","scheduleDay":null}}',
                            "null",
                            '{"listperson":[],"notelist":[],"booking_date":"05\/24\/2021","booking_hours":""}'
                        );
                    });

                </script>
            </section>

            <section id="boxBookingInfo" class="box-booking-info relative" style="display: none">
                <h3 class="booking-info-title">Appointment Information</h3>

                <!-- Service, Staff -->
                <div id="boxServiceStaff" class="box-service-staff">
                    <div class="service-staff">
                        <div class="service-staff-avatar">
                            <img class="img-responsive" src="/public/library/global/no-photo.jpg" alt="Any person" />
                        </div>
                        <div class="service-staff-info">
                            <h5>Any Technician</h5>
                            <p>Any Service</p>
                            <p>Price: N/A</p>
                        </div>
                    </div>
                    <div class="service-staff">
                        <div class="service-staff-avatar no-photo">
                            <img class="img-responsive" src="/public/library/global/no-photo.jpg" alt="Any person" />
                        </div>
                        <div class="service-staff-info">
                            <h5>Any Technician</h5>
                            <p>Any Service</p>
                            <p>Price: N/A</p>
                        </div>
                    </div>
                </div>

                <!-- Date, Time List -->
                <div id="boxDateTime" class="box-date-time">
                    <h4 class="date-info" id="dateInfo">Sunday, Jan-01-1970</h4>
                    <div class="time-info">
                        <h5>
                            Morning <span class="time-note" id="timeAMNote">N/A</span>
                        </h5>
                        <ul class="time-items" id="timeAMHtml"></ul>
                    </div>
                    <div class="time-info">
                        <h5>
                            Afternoon <span class="time-note" id="timePMNote">N/A</span>
                        </h5>
                        <ul class="time-items" id="timePMHtml"></ul>
                    </div>
                </div>
            </section>

            <section id="popupBookingConfirm" class="popup-booking-confirm white-popup mfp-hide border-style">
                <section id="boxBookingConfirm" class="box-booking-confirm relative">
                    <h3 class="booking-confirm-title">
                        Confirm booking information ?
                    </h3>
                    <div class="booking-confirm-note">
                        We will send a text message to you via the number below after we
                        confirm the calendar for your booking.
                    </div>
                    <script src="https://www.google.com/recaptcha/api.js?hl=en" async defer></script>
                    <!-- Google reCaptcha -->
                    <script type="text/javascript">
                        function ezyCaptcha_formBookingConfirm(token, is_submit) {
                            is_submit = 1;
                            if ($("#password").length) {
                                //$("input:password").val(md5(clean_input($("#password").val())));
                            }
                            return true;
                        }

                    </script>
                    <form id="formBookingConfirm" class="form-booking-confirm" name="formBookingConfirm" method="post"
                        action="/book" enctype="multipart/form-data">
                        <div class="row">
                            <div class="col-md-6 booking-name">
                                <div class="group-select">
                                    <label>Your name (required)</label>
                                    <div class="relative w100">
                                        <input type="text" class="form-control" name="booking_name" value=""
                                            data-validation="[NOTEMPTY]"
                                            data-validation-message="Please enter your name" placeholder=""
                                            maxlength="76" title="Booking name" />
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6 booking-phone">
                                <div class="group-select">
                                    <label>Your phone (required)</label>
                                    <div class="relative w100">
                                        <input type="text" class="form-control inputPhone" pattern="\d*"
                                            name="booking_phone" data-validation="[NOTEMPTY]"
                                            data-validation-message="Please enter your phone" placeholder=""
                                            maxlength="16" title="Booking phone" />
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-12 booking-email">
                                <div style="display: none" class="group-select">
                                    <label>Your email (optional)</label>
                                    <div class="relative w100">
                                        <input type="text" class="form-control" name="booking_email" value=""
                                            placeholder="" maxlength="76" title="Booking email" />
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-12 booking-notelist">
                                <div class="group-select">
                                    <label>Note (optional)</label>
                                    <div class="relative w100">
                                        <textarea class="form-control" rows="5" name="notelist"
                                            placeholder="(Max length 200 character)" maxlength="201"
                                            title="Booking note"></textarea>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-12 booking-store">
                                <input type="hidden" name="choose_store" class="booking_store" value="0" />
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-8 order-md-2 col-md-push-4">
                                <button class="btn btn-confirm btn_confirm" type="button">
                                    Confirm
                                </button>
                            </div>
                            <div class="col-md-4 order-md-1 col-md-pull-8">
                                <button class="btn btn-cancel btn_cancel" type="button">
                                    Cancel
                                </button>
                            </div>
                        </div>
                    </form>
                </section>
            </section>
        </div>
    </div>
</main>
