<main class="main">
    <!-- Tpl main about -->
    <div class="breadcrumb-area">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="breadcrumb-inner">
                        <h2 class="page-title">About Us</h2>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="work-section padding-bottom-100 padding-top-100">
        <div class="container">
            @foreach ($articles as $item)
                <div class="row no-gutters work-section-bottom">
                    @if (count(json_decode($item['image'])) > 0)
                        <div class="col-md-6">
                            <div class="work-img">
                                <img src="{{ asset('storage') }}/photos/{{ json_decode($item['image'])[0] }}"
                                    alt="About Us" caption="false" />
                            </div>
                        </div>
                    @endif
                    <div class="col-md-6">
                        <div class="work-text-wrapper">
                            <div class="work-text">
                                <div class="section-title padding-bottom-15 work-title desktop-left">
                                    <h2 class="title">{{ $item['title'] }}</h2>
                                </div>
                                {!! $item['content'] !!}
                            </div>
                        </div>
                    </div>
                </div>
            @endforeach
        </div>
    </div>
</main>
