<main class="main">

    <div class="breadcrumb-area">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="breadcrumb-inner">
                        <h2 class="page-title">Contact Us</h2>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @php
        $features = json_decode($header['features']);
    @endphp
    <div class="get-on-us bg-white padding-top-100 padding-bottom-100">
        <div class="container">
            <div class="row">
                @foreach ($features as $index => $item)
                    @php
                        $name = explode(',', $item->name);
                        $desc = explode(',', $item->desc);
                    @endphp
                    <div class="col-md-4">
                        <div class="inner-touch">
                            <div class="icon">
                                @if (count($name) > 0)
                                    <i class="{{ $name[0] }}"></i>
                                @endif
                            </div>
                            @if (count($name) > 1)
                                <h4 class="title">{{ $name[1] }}</h4>
                            @endif
                            <p>
                                @if ($index === 1)
                                    <p itemprop="address" itemscope itemtype="http://schema.org/PostalAddress">
                                        <span itemprop="streetAddress" class="address">{{ $item->desc }}</span>
                                    </p>
                                    <br />
                                @else
                                    @foreach ($desc as $el)
                                        @if ($index === 0)
                                            <a href="tel:{{ $el }}" title="Call Us">
                                                <span itemprop="telephone" class="phone">{{ $el }}</span>
                                            </a>
                                            <br />
                                        @endif
                                        @if ($index === 2)
                                            <a href="mailto:{{ $el }}" title="Mail Us">
                                                <span itemprop="email" class="email">{{ $el }}</span>
                                            </a>
                                            <br />
                                        @endif
                                    @endforeach
                                @endif
                            </p>
                        </div>
                    </div>
                @endforeach
            </div>
            <div class="maps">
                <div class="google-maps" id="google-map">
                    <iframe
                        src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d2710.54909919198!2d-95.57399343505905!3d29.726958506843314!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x74ace03ed50728e8!2sFast%20Boy%20Marketing!5e0!3m2!1sen!2s!4v1582538295697!5m2!1sen!2s"
                        width="100%" height="450" frameborder="0" style="border: 0" allowfullscreen=""></iframe>
                </div>
            </div>
        </div>
    </div>
</main>
