@php
$features = json_decode($header['features']);
$extras = json_decode($header['extras']);
@endphp
<footer class="footer">
    <footer class="footer-area">
        <div class="map-section-four bg-blue-2 padding-top-90 padding-bottom-45">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <div class="section-title padding-bottom-55">
                            <h2 class="title" itemprop="title">Contact Us</h2>
                        </div>
                    </div>
                </div>
                <div class="row no-gutters map-section-info">
                    <div class="col-md-12 col-lg-12">
                        <div class="contact-info padding-bottom-40">
                            <div class="contact-form">
                                <form enctype="multipart/form-data" method="post" name="send_contact" id="send_contact"
                                    action="{{ route('send_contact') }}" class="form-horizontal">
                                    @csrf
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="group-select">
                                                <label>Your name (required)</label>
                                                <div class="relative w100">
                                                    <input title="Your name" type="text" name="contactname"
                                                        data-validation="[NOTEMPTY]"
                                                        data-validation-message="Please enter your name"
                                                        class="form-control" autocomplete="off" placeholder=""
                                                        maxlength="76" />
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="group-select">
                                                <label>Your email address (required)</label>
                                                <div class="relative w100">
                                                    <input title="Your email address" type="text" name="contactemail"
                                                        data-validation="[EMAIL]"
                                                        data-validation-message="Please enter your email"
                                                        class="form-control" autocomplete="off" placeholder=""
                                                        maxlength="76" />
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="group-select">
                                        <label>Your subject (required)</label>
                                        <div class="relative w100">
                                            <input title="Your subject" type="text" name="contactsubject"
                                                data-validation="[NOTEMPTY]"
                                                data-validation-message="Please enter your subject" class="form-control"
                                                autocomplete="off" placeholder="" maxlength="251" />
                                        </div>
                                    </div>
                                    <div class="group-select">
                                        <label>Your message (required)</label>
                                        <div class="relative w100">
                                            <textarea title="Your message" name="contactcontent"
                                                data-validation="[NOTEMPTY]"
                                                data-validation-message="Please enter your message" class="form-control"
                                                autocomplete="off" rows="10" placeholder="" maxlength="501"></textarea>
                                        </div>
                                    </div>
                                    @if (session('status'))
                                        <div class="alert alert-success" role="alert">
                                            <button type="button" class="close" data-dismiss="alert">×</button>
                                            {{ session('status') }}
                                        </div>
                                    @elseif(session('failed'))
                                        <div class="alert alert-danger" role="alert">
                                            <button type="button" class="close" data-dismiss="alert">×</button>
                                            {{ session('failed') }}
                                        </div>
                                    @endif
                                    <div class="form-group text-center">
                                        <button type="submit" class="btn btn-main btn-submit btn_contact">
                                            Send Us
                                        </button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6 col-lg-4">
                        <div class="footer-content">
                            <h4 class="title" itemprop="title">LOCATION</h4>
                            <div class="fci-wrap">
                                @foreach ($features as $index => $item)
                                    @php
                                        $name = explode(',', $item->name);
                                        $desc = explode(',', $item->desc);
                                    @endphp
                                    <div class="fci-row">
                                        <span class="fci-title">
                                            @if (count($name) > 0)
                                                <i class=" icons-address {{ $name[0] }}"></i>
                                            @endif
                                        </span>
                                        @if ($index === 1)
                                            <span itemprop="streetAddress"
                                                class="fci-content">{{ $item->desc }}</span>
                                            <br />
                                        @else
                                            @foreach ($desc as $el)
                                                @if ($index === 0)
                                                    <a href="tel:{{ $el }}" title="Call Us">
                                                        <span itemprop="telephone"
                                                            class="phone">{{ $el }}</span>
                                                    </a>
                                                    <br />
                                                @endif
                                                @if ($index === 2)
                                                    <a href="mailto:{{ $el }}" title="Mail Us">
                                                        <span itemprop="email" class="email">{{ $el }}</span>
                                                    </a>
                                                    <br />
                                                @endif
                                            @endforeach
                                        @endif
                                    </div>
                                @endforeach
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6 col-lg-4">
                        <div class="footer-content">
                            <h4 class="title" itemprop="title">BUSINESS HOURS</h4>
                            <div class="foh-wrap">
                                <!-- Normal Day -->
                                @foreach ($extras as $extra)
                                    <div class="foh-row short" itemprop="openingHours" content="{{ $extra->desc }}">
                                        <span class="foh-date">{{ $extra->name }}</span>
                                        <span class="foh-time">{{ $extra->desc }}</span>
                                    </div>
                                @endforeach
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6 col-lg-4">
                        <div class="footer-content">
                            <h4 class="title" itemprop="title">FOLLOW US</h4>
                            <ul class="list-line social">
                                @foreach ($social as $item)
                                    <li>
                                        @if ($item['url'])
                                            <a itemprop="url" rel="nofollow" target="_blank"
                                                title="{{ $item['title'] }}" href="{{ $item['name'] }}">
                                                <img src="{{ asset('storage') }}/photos/{{ $item['url'] }}"
                                                    alt="{{ $item['title'] }}" />
                                            </a>
                                        @endif
                                    </li>
                                @endforeach
                            </ul>
                            <!-- Start Single Footer Widget -->
                            <div class="social-fanpage clearfix">
                                <aside>
                                    <!-- facebook fanpage -->
                                    <div id="fanpage_fb_container"></div>
                                </aside>
                                <!-- use for calculator width -->
                                <div id="social_block_width" class="clearfix"
                                    style="width: 100% !important; height: 1px !important">
                                </div>
                            </div>
                            <!-- End Single Footer Widget -->
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="copyright-area">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="copyright-area-inner">
                            <div class="qry-copy">
                                © 2021 Flowers Nails & Spa. All Rights Reserved.
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </footer>
    <div class="back-to-top">
        <span class="back-top"><span class="arrow_carrot-up"></span></span>
    </div>
</footer>
<div class="freeze-footer">
    <ul>
        <li>
            <a href="tel:{{ explode(',', $features[1]->desc)[0] }}" class="btn btn-default btn_call_now btn-call"
                title="Call us">
                <div class="icon">
                    <i class="fa fa-phone" aria-hidden="true"></i>
                    <span class="d-md-none">Call Us</span>
                </div>
            </a>
        </li>
        <li class="btn_make_appointment">
            <a href="{{ route('book') }}" class="btn btn-default btn_make_appointment" title="Booking">
                <div class="icon">
                    <i class="fa fa-calendar" aria-hidden="true"></i>
                    <span class="d-md-none">Booking</span>
                </div>
            </a>
        </li>
    </ul>
</div>
