@php
$menu = [
    [
        'title' => 'Home',
        'href' => route('home'),
        'name' => 'home',
    ],
    [
        'title' => 'About',
        'href' => route('about'),
        'name' => 'about',
    ],
    [
        'title' => 'Services',
        'href' => route('services'),
        'name' => 'services',
    ],
    [
        'title' => 'Gallery',
        'href' => route('gallery'),
        'name' => 'gallery',
    ],
    [
        'title' => 'Coupons',
        'href' => route('coupons'),
        'name' => 'coupons',
    ],
    [
        'title' => 'GiftCards',
        'href' => route('giftcards'),
        'name' => 'giftcards',
    ],
    [
        'title' => 'Contact',
        'href' => route('contact'),
        'name' => 'contact',
    ],
];
@endphp
<header class="header">
    <div class="header-section">
        <div class="wrap-freeze-header-mobile clearfix hidden-md hidden-lg d-block d-lg-none        fixed-freeze mobile"
            style="">
            <div class="instead-flag-freeze-header mobile" style="height: 0px;"></div>
            <div class="flag-freeze-header-mobile initializedFreezeHeader">
                <div class="menu_mobile_v1">
                    <div class="mobile_logo">
                        @if (count(json_decode($header['images'])) > 0)
                            <div class="logo">
                                <a itemprop="url" href="{{ route('home') }}" title="Logo">
                                    <img class="imgrps"
                                        src="{{ asset('storage') }}/photos/{{ json_decode($header['images'])[0] }}"
                                        alt="{{ $header['title'] }}" itemprop="logo image" />
                                </a>
                            </div>
                        @endif
                    </div>
                    <div class="mobile_menu_container_v1">
                        <div class="mobile-menu clearfix">
                            <nav id="mobile_dropdown" style="display: block;">
                                <!-- Tpl menu mobile layouts -->
                                <ul>
                                    @foreach ($menu as $item)
                                        <li class=" @if (Route::current()->getName() ===
                                            $item['name']) active @endif">
                                            <a itemprop="url" href="{{ $item['href'] }}" class=" @if (Route::current()->getName() === $item['name']) active @endif">{{ $item['title'] }}</a>
                                        </li>
                                    @endforeach
                                </ul>
                            </nav>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="wrap-freeze-header clearfix hidden-xs hidden-sm d-none d-lg-block        fixed-freeze desktop">
            <div class="instead-flag-freeze-header desktop" style="height: 0px;"></div>
            <div class="flag-freeze-header initializedFreezeHeader">
                <nav class="navbar navbar-area navbar-expand-lg nav-style-01">
                    <div class="container nav-container">
                        <div class="responsive-mobile-menu">
                            @if (count(json_decode($header['images'])) > 0)
                                <div class="logo-wrapper">
                                    <a itemprop="url" href="{{ route('home') }}" title="Logo">
                                        <img class="imgrps"
                                            src="{{ asset('storage') }}/photos/{{ json_decode($header['images'])[0] }}"
                                            alt="{{ $header['title'] }}" itemprop="logo image">
                                    </a>
                                </div>
                            @endif
                        </div>
                        <div class="collapse navbar-collapse header-nav-desktop" id="bdesign_main_menu">
                            <!-- Tpl menu main layouts -->
                            <ul class="nav navbar-nav">
                                @foreach ($menu as $item)
                                    <li class=" @if (Route::current()->getName() ===
                                        $item['name']) active @endif">
                                        <a itemprop="url" href="{{ $item['href'] }}" class=" @if (Route::current()->getName() === $item['name']) active @endif">{{ $item['title'] }}</a>
                                    </li>
                                @endforeach
                                <li>
                                    <div class="btn-wrapper change-version btn_make_appointment">
                                        <a itemprop="url" href="/book" class="btn-showcase hover-border"
                                            title="Booking">Booking</a>
                                    </div>
                                </li>
                            </ul>
                        </div>
                        <div class="nav-right-content"></div>
                    </div>
                </nav>
            </div>
        </div>
    </div>
</header>
