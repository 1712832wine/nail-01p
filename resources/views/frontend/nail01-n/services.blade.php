@php
$features = json_decode($header['features']);
$extras = json_decode($header['extras']);
@endphp
<main class="main">
    <input type="hidden" name="group_id" value="{{ $service_id }}" />
    <div class="breadcrumb-area">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="breadcrumb-inner">
                        <h2 class="page-title">Services</h2>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="services-section padding-bottom-100 padding-top-100">
        <div class="container">
            <div class="service-container animation_sroll_button">
                <div class="row">
                    <div class="col-md-7 order-md-2">
                        <div class="service-btn-group text-right btn-wrapper">
                            <a href="/book" class="btn btn-main btn_make_appointment btn-blank"
                                title="Make an appointment">
                                <i class="far fa-calendar-check"></i> Make an appointment </a>&nbsp
                            <a href="tel:{{ explode(',', $features[1]->desc)[0] }}" class="btn btn-main btn-blank"
                                title="Call now">
                                <i class="fa fa-phone"></i> Call now
                            </a>
                        </div>
                    </div>
                    <div class="col-md-5"></div>
                </div>
                <div class="clearfix animation_sroll_to_service service-list">
                    @foreach ($services as $index => $service)
                        @if ($index % 2 === 0)
                            <div class="row clearfix" id="sci_{{ $service['id'] }}">
                                @if (count(json_decode($service['images'])) > 0)
                                    <div class="col-sm-4 col-md-4 right service-image-wrap">
                                        <div class="service-image circle">
                                            <div class="service-image-bg"
                                                style="background-image: url('{{ asset('storage') }}/photos/{{ json_decode($service['images'])[0] }}');">
                                                <img class="img-responsive"
                                                    src="{{ asset('storage') }}/photos/{{ json_decode($service['images'])[0] }}"
                                                    alt="{{ $service['name'] }}" />
                                            </div>
                                        </div>
                                    </div>
                                @endif
                                <div class="col-sm-8 col-md-8">
                                    <div class="clearfix service-list">
                                        <h2 class="service-name">{{ $service['name'] }}</h2>
                                        @foreach (json_decode($service['features']) as $item)
                                            @switch($item->name)
                                                @case('desc')
                                                    <p>{{ $item->desc }}</p>
                                                @break
                                                @case('center')
                                                @break
                                                @default
                                                    <div class="detail-item item-238">
                                                        <div class="detail-price-item">
                                                            <span class="detail-price-name">{{ $item->name }}</span>
                                                            <span class="detail-price-dots"></span>
                                                            <span class="detail-price-number">
                                                                <span class="current">{{ $item->desc }}</span>
                                                            </span>
                                                        </div>
                                                    </div>
                                            @endswitch
                                        @endforeach
                                    </div>
                                </div>
                            </div>
                        @else
                            <div class="row clearfix" id="sci_{{ $service['id'] }}">
                                <div class="service-line"></div>
                                @if (count(json_decode($service['images'])) > 0)
                                    <div class="col-sm-4 col-md-4 order-md-2 left service-image-wrap">
                                        <div class="service-image circle">
                                            <div class="service-image-bg"
                                                style="background-image: url('{{ asset('storage') }}/photos/{{ json_decode($service['images'])[0] }}');">
                                                <img class="img-responsive"
                                                    src="{{ asset('storage') }}/photos/{{ json_decode($service['images'])[0] }}"
                                                    alt="{{ $service['name'] }}" />
                                            </div>
                                        </div>
                                    </div>
                                @endif
                                <div class="col-sm-8 col-md-8 order-md-1">
                                    <div class="clearfix service-list">
                                        <h2 class="service-name">{{ $service['name'] }}</h2>
                                        @foreach (json_decode($service['features']) as $item)
                                            @switch($item->name)
                                                @case('desc')
                                                    <p>{{ $item['desc'] }}</p>
                                                @break
                                                @case('center')
                                                @break
                                                @default
                                                    <div class="detail-item item-238">
                                                        <div class="detail-price-item">
                                                            <span class="detail-price-name">{{ $item->name }}</span>
                                                            <span class="detail-price-dots"></span>
                                                            <span class="detail-price-number">
                                                                <span class="current">{{ $item->desc }}</span>
                                                            </span>
                                                        </div>
                                                    </div>
                                            @endswitch
                                        @endforeach
                                    </div>
                                </div>
                            </div>
                        @endif
                    @endforeach
                </div>
            </div>
        </div>
    </div>
</main>
