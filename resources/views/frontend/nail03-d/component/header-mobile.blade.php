<div class="menu-bar-mobile">
    <div class="logo-menu">
        @if (count(json_decode($header['images'])) > 0)
            <a class="logo" href="/">
                <img src="{{ asset('storage') }}/photos/{{ json_decode($header['images'])[0] }}"
                    alt="{{ $header['title'] }}" itemprop="logo">
            </a>
        @endif
    </div>
    <ul>
        <li itemprop="name"><a itemprop="url" href="/">Home</a></li>
        <li><a itemprop="url" href="/about">About Us</a></li>
        <li itemprop="name"><a itemprop="url" href="/services">Services</a></li>
        <li itemprop="name"><a itemprop="url" href="/book">Booking</a></li>
        <li itemprop="name"><a itemprop="url" href="/coupons" title="">Coupons</a></li>
        <li itemprop="name"><a itemprop="url" href="/gallery">Gallery</a></li>
        <li itemprop="name"><a itemprop="url" href="/giftcards">Gift Cards</a></li>
        <li itemprop="name"><a itemprop="url" href="/contact">Contact Us</a></li>
    </ul>
</div>
