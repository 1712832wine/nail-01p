@php
$features = json_decode($header['features']);
$extras = json_decode($header['extras']);
@endphp
<div class="header-container">
    <div class="layout-header-1 pad-vertical-5 row background-42 hidden-sm hidden-xs">
        <div class="container">
            <div class="row">
                <div class="col-lg-8 col-md-8 col-sm-12 col-xs-12 hidden-sm hidden-xs">
                    <div class="contact-link">
                        <p class="inline-block mar-horizontal-5 company_address"><i
                                class="fa fa-map-marker mar-right-10"></i>
                            <span itemprop="address">{{ $features[0]->desc }}
                            </span>
                        </p>
                        <p class="inline-block mar-horizontal-5">
                            <i class="fa fa-phone mar-right-10"></i>
                            <span>
                                @foreach (explode(',', $features[1]->desc) as $item)
                                    <a itemprop="telephone" style="color: #ffffff;"
                                        href="tel:{{ $item }}">{{ $item }}</a>
                                @endforeach
                            </span>
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="wrap-freeze-header-mobile clearfix hidden-md hidden-lg">
        <div class="flag-freeze-header-mobile">
            <div class="container">
                <div class="box-color clearfix">
                    <div class="layout-header-2 pad-vertical-5">
                        <div class="logo">
                            @if (count(json_decode($header['images'])) > 0)
                                <a class="" href="/">
                                    <img src="{{ asset('storage') }}/photos/{{ json_decode($header['images'])[0] }}"
                                        alt="{{ $header['title'] }}" itemprop="logo">
                                </a>
                            @endif
                        </div>
                        <div class="menu-btn-show pos-left">
                            <span class="border-style"></span>
                            <span class="border-style"></span>
                            <span class="border-style"></span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="container hidden-xs hidden-sm">
        <div class="box-color clearfix">
            <div class="layout-header-2 pad-vertical-5">
                <div class="logo">
                    @if (count(json_decode($header['images'])) > 0)
                        <a class="" href="/">
                            <img src="{{ asset('storage') }}/photos/{{ json_decode($header['images'])[0] }}"
                                alt="{{ $header['title'] }}" itemprop="logo">
                        </a>
                    @endif
                </div>
            </div>
        </div>
    </div>
    <div class="wrap-freeze-header clearfix hidden-xs hidden-sm">
        <div class="flag-freeze-header">
            <div class="container">
                <div class="box-color clearfix">
                    <div class="layout-header-3 pad-vertical-5">
                        <div class="main-menu center">
                            <ul>
                                <li itemprop="name"><a itemprop="url" href="/">Home</a></li>
                                <li><a itemprop="url" href="/about">About Us</a></li>
                                <li itemprop="name"><a itemprop="url" href="/services">Services</a></li>
                                <li itemprop="name"><a itemprop="url" href="/book">Booking</a></li>
                                <li itemprop="name"><a itemprop="url" href="/coupons" title="">Coupons</a></li>
                                <li itemprop="name"><a itemprop="url" href="/gallery">Gallery</a></li>
                                <li itemprop="name"><a itemprop="url" href="/giftcards">Gift Cards</a></li>
                                <li itemprop="name"><a itemprop="url" href="/contact">Contact Us</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
