@php
$features = json_decode($header['features']);
$extras = json_decode($header['extras']);
@endphp
<div class="footer-page center">
    <div class="contact-footer center">
        <div class="container">
            <div class="row">
                <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                    <div class="item-footer">
                        <h2 itemprop="name">Address</h2>
                        <ul class="address_info">
                            <li style="color: #FFFFFF; font-size: 15px;">
                                <i class="fa fa-map-marker"></i>
                                <span itemprop="address">{{ $features[0]->desc }}</span>
                            </li>
                            <li>
                                <i class="fa fa-phone" style="color: #FFFFFF; font-size: 15px;"></i>
                                @foreach (explode(',', $features[1]->desc) as $item)
                                    <a style="color: #FFFFFF; font-size: 15px;" itemprop="url"
                                        href="tel:{{ $item }}">
                                        <span itemprop="telephone">{{ $item }}</span>
                                    </a>
                                    <br>
                                @endforeach
                            </li>
                            <li><i class="fa fa-envelope" style="color: #FFFFFF; font-size: 15px;"></i>
                                @foreach (explode(',', $features[2]->desc) as $item)
                                    <a style="color: #FFFFFF; font-size: 15px;" itemprop="url"
                                        href="mail:{{ $item }}">
                                        <span itemprop="telephone">{{ $item }}</span>
                                    </a>
                                    <br>
                                @endforeach
                            </li>
                        </ul>

                    </div>
                </div>

                <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                    <div class="item-footer business-hour">
                        <h2 itemprop="name"><strong>Business Hours</strong></h2>
                        <ul>
                            @foreach ($extras as $extra)
                                <li itemprop="openingHours" data="{{ $extra->desc }}">
                                    <p>
                                        <spam><strong>{{ $extra->name }}</strong></spam>
                                        <span>{{ $extra->desc }}</span>
                                    </p>
                                </li>
                            @endforeach
                        </ul>
                    </div>
                </div>
                <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                    <div class="item-footer">
                        <h2>FOLLOW US</h2>
                        <div class="list-icon center">
                            <ul>
                                @foreach ($social as $item)
                                    <li>
                                        <a target="_blank" href="{{ $item['name'] }}">
                                            <img src="{{ asset('storage') }}/photos/{{ $item['url'] }}">
                                        </a>
                                    </li>
                                @endforeach
                            </ul>
                            <div class="header-social-page">
                                <!-- facebook fanpage -->
                                <div class="single_footer_widget">
                                    <div class="box_fanpage_fb">
                                        <div id="fanpage_fb_container"></div>
                                    </div>
                                </div>

                                <!-- use for calculator width -->
                                <div class="single_footer_widget">
                                    <div id="social_block_width" style="width:100% !important; height: 1px !important">
                                    </div>
                                </div>
                                <!-- End Single Footer Widget -->
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="arrow-footer zindex-1" data-aos="fade-up" data-aos-easing="linear" data-aos-duration="700">
            <span class="fa fa-angle-up" data-aos="fade-up" data-aos-easing="linear" data-aos-duration="1100"></span>
            <span class="fa fa-angle-up" data-aos="fade-up" data-aos-easing="linear" data-aos-duration="1000"></span>
        </div>
    </div>
    <div class="copyright zindex-9 center">
        <p>© Copyright by Beauty Nails. All Rights Reserved.</p>
    </div>
</div>
