<div class="banner-page-list">
    <div class="title-page">
        <div class="ac">
            <h2>Contact Us</h2>
            <div class="breakcrumb">
                <a itemprop="url" href="/">Home</a>
                /
                <p itemprop="name">Contact</p>
            </div>
        </div>
    </div>
</div>
<script src='https://www.google.com/recaptcha/api.js?hl=en' async defer></script><!-- Google reCaptcha -->
<script type="text/javascript">
    <!--
    function ezyCaptcha_send_contact_main(token, is_submit) {
        is_submit = 0;
        if ($("#password").length) {
            //$("input:password").val(md5(clean_input($("#password").val())));
        }
        return true;
    }
    //

    -->
</script>
<div class="main-content">
    <!-- About us page 1 -->
    <div class="contact-page-1">
        <div class="container">
            <div class="row multi-columns-row">
                <div class="col-md-6">
                    <div class="form-input-infor">
                        <h2>Contact us</h2>
                        <form enctype="multipart/form-data" method="post" name="send_contact" id="send_contact_main"
                            action="{{ route('send_contact') }}">
                            @csrf
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <input type="text" data-validation="[NOTEMPTY]"
                                            data-validation-message="Please enter your name!" autocomplete="off"
                                            class="form-control style-input" placeholder="Your Name" name="contactname">
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <input type="text" data-validation="[EMAIL]"
                                            data-validation-message="Invalid email" autocomplete="off"
                                            class="form-control style-input" placeholder="Your Email"
                                            name="contactemail">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <input type="text" data-validation="[NOTEMPTY]"
                                            data-validation-message="Please enter a subject!" autocomplete="off"
                                            class="form-control style-input" placeholder="Your Subject"
                                            name="contactsubject">
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <textarea rows="3" data-validation="[NOTEMPTY]"
                                            data-validation-message="Please enter a content!" autocomplete="off"
                                            class="form-control style-input style-textarea" placeholder="Your Message"
                                            name="contactcontent"></textarea>
                                        @if (session('status'))
                                            <div class="alert alert-success" role="alert">
                                                <button type="button" class="close" data-dismiss="alert">×</button>
                                                {{ session('status') }}
                                            </div>
                                        @elseif(session('failed'))
                                            <div class="alert alert-danger" role="alert">
                                                <button type="button" class="close" data-dismiss="alert">×</button>
                                                {{ session('failed') }}
                                            </div>
                                        @endif
                                        <div class="btn-sauna">
                                            <input class="btn_contact style-button " type="submit" value="Send Email"
                                                name="submit_ok">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="clearfix">
                        <!-- Google map area -->
                        <div class="google-map-wrapper">
                            <div class="google-map" id="map" style="float: left;width: 100%;"><iframe
                                    src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3464.7391505606774!2d-95.57299108550652!3d29.727314340440024!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x8640ddc6ed681049%3A0x74ace03ed50728e8!2sFast+Boy+Marketing!5e0!3m2!1sen!2s!4v1510909605272"
                                    width="100%" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div><!-- End About us page 1 -->
</div>
<style type="text/css">
    .img-item-list-gallery img {
        width: 100%;
    }

    .title-standard h2 {
        text-align: center !important;
    }

    .read-more {
        text-align: center !important;
    }

    .item-services-2 ul li p {
        padding-left: 10px;
        padding-right: 10px;
        color: #828282;
        font-size: 13px;
    }

</style>
