<div class="banner-slider-index">
    <div class="ac">
        <div class="box-color">
            <div class="tp-banner-container">
                <div class="slider-width-height tp-banner-img"
                    style="display: inline-block; width: 100%; height: 1px; overflow: hidden;">
                    <div id="slider-option" data-delay="10000" data-autoplay="true" data-autoplayDelay="5000"
                        data-autoplayDirection="normal" data-fade="true"></div> <!-- END SLIDER OPTION -->
                    <div class="fixed" style="width: 100%;"></div>
                    @if (count($imgs_carousel) > 0))
                        <img src="{{ asset('storage') }}/photos/{{ $imgs_carousel[0]['url'] }}"
                            style="width: 100%; height: auto;" alt="">
                        <!-- END USE FOR CALCULATOR START WIDTH HEIGHT -->
                    @endif
                </div>
                <div class="tp-banner">
                    <ul>
                        @foreach ($imgs_carousel as $img)
                            <li data-transition="fade" data-slotamount="7" data-masterspeed="1500">
                                <!-- MAIN IMAGE -->
                                <img src="{{ asset('storage') }}/photos/{{ $img['url'] }}"
                                    alt="dreamstime_l_172721886.jpg" data-bgfit="cover" data-bgposition="left top"
                                    data-bgrepeat="no-repeat">
                                <div class="tp-caption large_bold_grey skewfromrightshort customout" data-x="right"
                                    data-y="center" data-hoffset="-110" data-voffset="0"
                                    data-customout="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0.75;scaleY:0.75;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"
                                    data-speed="500" data-start="800" data-easing="Back.easeOut" data-endspeed="500"
                                    data-endeasing="Power4.easeIn" data-captionhidden="on" style="z-index: 4">
                                </div>
                            </li>
                        @endforeach
                    </ul>
                    <div class="tp-bannertimer"></div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="main-content">
    <div class="about-us">
        <div class="container">
            <div class="bg-about-us clearfix">
                <div class="title-standard">
                    <h2 itemprop="name">About us</h2>
                </div>
                <div class="row ctn-about-us ">
                    <div class="col-md-6 col-xs-12">
                        {!! $home_article['content'] !!} <a itemprop="url" href="/about" class="read-more">Read more</a>
                    </div>
                    <div class="col-md-6 col-xs-12">
                        <div class="video_intro">
                            <iframe width="300" height="150" src="{{ $home_article['description'] }}" frameborder="0"
                                allowfullscreen="allowfullscreen"></iframe>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    {{-- SERVICES --}}
    <div class="our-services" style="background-image:url({{ asset('files') }}/bg-home.jpg)">
        <div class="container">
            <div class="ctn-our-services">
                <div class="title-standard">
                    <h2 itemprop="name">Our Services</h2>
                </div>
                <div class="owl-carousel owl-theme" id="owl-our-services">
                    @foreach ($services as $service)
                        <div class="item-our-services">
                            <div class="img-our-services">
                                <a itemprop="url"
                                    href="{{ route('services-detail', ['slug' => strtolower(trim(preg_replace('/[^A-Za-z0-9-]+/', '-', $service['name']))), 'service_id' => $service['id']]) }}">
                                    <img itemprop="image" class="lazyOwl"
                                        src="{{ asset('storage') }}/photos/{{ json_decode($service['images'])[0] }}"
                                        data-src="{{ asset('storage') }}/photos/{{ json_decode($service['images'])[0] }}"
                                        alt="{{ $service['name'] }}">
                                </a>
                            </div>
                            <div class="title-our-services">
                                <h2 itemprop="name"><a itemprop="url"
                                        href="{{ route('services-detail', ['slug' => strtolower(trim(preg_replace('/[^A-Za-z0-9-]+/', '-', $service['name']))), 'service_id' => $service['id']]) }}">
                                        {{ $service['name'] }}</a>
                                </h2>
                            </div>
                        </div>
                    @endforeach
                </div>
            </div>
        </div>
    </div>
    {{-- GALLERY --}}
    <div class="gallery-index">
        <div class="container">
            <div class="title-standard">
                <h2 itemprop="name">Our Gallery</h2>
            </div>
        </div>
        <div class="main-content">
            <div class="gallery-style-1 bg-fa gallery-inpage ">
                <div class="list-gallery">
                    <div class="container">
                        <div class="tab-content">
                            <div class="tab-pane fade active in">
                                @foreach ($gallery_list as $item)
                                    <div class="col-lg-3 col-md-3 col-sm-6 col-xs-6">
                                        <div class="item-gallery">
                                            <div class="img-item-gallery">
                                                <div class="img-item">
                                                    <a itemprop="url" rel="group1" class="gallery-item2"
                                                        href="{{ asset('storage') }}/photos/{{ $item['url'] }}">
                                                        <span
                                                            style="background-image:url('{{ asset('storage') }}/photos/{{ $item['url'] }}')"></span>
                                                        <img style="display: none" itemprop="image"
                                                            src="{{ asset('storage') }}/photos/{{ $item['url'] }}"
                                                            alt="Nails Design">
                                                    </a>
                                                </div>
                                            </div>
                                            <a itemprop="url" rel="group1" class="clearfix gallery-item1"
                                                href="{{ asset('storage') }}/photos/{{ $item['url'] }}">
                                                <div class="social-item-gallery">
                                                    <div class="vertical">
                                                        <h2 itemprop="name">
                                                            <span class="a">
                                                                Nails Design </span>
                                                        </h2>
                                                        <div class="img-vector-services">
                                                            <img itemprop="image" src="images/icon-vector-services.png">
                                                        </div>
                                                        <span itemprop="name">Nails Design</span>
                                                    </div>
                                                </div>
                                            </a>
                                        </div>

                                    </div>
                                @endforeach

                                <script>
                                    $(document).on('change', '.select_tab', function() {
                                        var tab = $(this).val();
                                        $('.tab-pane').removeClass('active');
                                        $('#' + tab).addClass('active');
                                    });

                                </script>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    {{-- CUSTOMER --}}
    <div class="our-services abc" style="background-image: url(&#39;images/nail-art-wallpapers.jpeg&#39;);">
        <div class="container">
            <div class="ctn-our-services">
                <div class="title-standard">
                    <h2>Reviews</h2>
                </div>
                <div id="owl-demo">
                    @foreach ($carousel_customers as $index => $item)
                        <div class="item">
                            @if (count(json_decode($item['image'])) > 0)
                                <img alt="review1"
                                    src="{{ asset('storage') }}/photos/{{ json_decode($item['image'])[0] }}"
                                    caption="false" />
                            @endif
                            <p class="review"><span>{{ $item['title'] }}</span></p>
                            {!! $item['content'] !!}
                            <p> </p>
                        </div>
                    @endforeach
                </div>
            </div>
        </div>
    </div>
    <script type="text/javascript">
        $(document).ready(function() {
            $("#owl-demo").owlCarousel({
                autoPlay: 5000, //Set AutoPlay to 3 seconds
                dots: true,
                items: 1,
                itemsDesktop: [1199, 3],
                itemsDesktopSmall: [979, 3]
            });

        })

    </script>
