<div class="banner-page-list">
    <div class="title-page">
        <div class="ac">
            <h2>Gallery</h2>
            <div class="breakcrumb">
                <a itemprop="url" href="/">Home</a>
                /
                <p itemprop="name">Gallery</p>
            </div>
        </div>
    </div>
</div> <!-- CONTAINER -->
<div class="main-content">
    <!-- Gallery page 1 -->
    <div class="gallery-style-1 bg-fa gallery-inpage ">
        <div class="cate-gallery hidden-xs">
            <div class="container">
                <ul id="filter">
                    @foreach ($albums as $index => $album)
                        <li itemprop="name" class="">
                            <a data-id="{{ $album['id'] }}">{{ $album['name'] }}</a>
                        </li>
                    @endforeach
                </ul>
            </div>
        </div>
        <div class="hidden-sm hidden-md hidden-lg hidden-xl">
            <!--<div class="please-choose">Please choose categories</div>-->
            <div class="form-group col-md-12 col-xs-12">
                <select id="filter_select" class="form-control select_tab" autocomplete="off" name="filter_select">
                    <!-- Category data -->
                    @foreach ($albums as $index => $album)
                        <option value="{{ $album['id'] }}">{{ $album['name'] }}</option>
                    @endforeach
                    <!-- Category data -->
                </select>
            </div>
        </div>
        <div class="list-gallery">
            <div class="container">
                <div class="box_list_gallery">

                </div>
            </div>
        </div>
    </div>
    <nav class="text-center box_paging text-center"></nav>
</div>
