<div class="banner-page-list">
    <div class="title-page">
        <div class="ac">
            <h2>About Us</h2>
            <div class="breakcrumb">
                <a itemprop="url" href="/">Home</a>
                /
                <p itemprop="name">About Us</p>
            </div>
        </div>
    </div>
</div>


<div class="main-content">
    <!-- About us page 1 -->
    <div class="about-us-page-1">
        <div class="container">
            <style type="text/css">
                .img-item-list-gallery img {
                    width: 100%;
                }

                .title-standard h2 {
                    text-align: center !important;
                }

                .read-more {
                    text-align: center !important;
                }

                .item-services-2 ul li p {
                    padding-left: 10px;
                    padding-right: 10px;
                    color: #828282;
                    font-size: 13px;
                }

            </style>
            <div class="instroduce">
                @foreach ($articles as $item)
                    <div class="item-about-us">
                        <h2 class="title-about-us" itemprop="name">{{ $item['title'] }}</h2>
                        <div class="row">
                            <div class="col-md-6 clearfix">
                                <div itemprop="description" class="content-item-about-us">
                                    {!! $item['content'] !!}
                                </div>
                            </div>
                            <div class="col-md-6 clearfix">
                                <div class="img-item-about-us ">
                                    @if (count(json_decode($item['image'])) > 0)
                                        <a itemprop="url" href="/">
                                            <img itemprop="image"
                                                src="{{ asset('storage') }}/photos/{{ json_decode($item['image'])[0] }}"
                                                caption="false" />
                                        </a>
                                    @endif
                                </div>
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>
        </div>
    </div>
</div>
