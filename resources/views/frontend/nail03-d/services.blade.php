<link rel="stylesheet" type="text/css" href="{{ asset('frontend') }}/themes/fnail03d/assets/css/service.css">
<div class="clearfix">
    <!-- module 2 -->
    <div class="banner-page-list">
        <div class="container">
            <div class="title-page">
                <div class="row">
                    <div class="col-md-6">
                        <h2>Our Services</h2>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="main-content animation_sroll_jumpto">
        <div class="container">
            {!! $intro['content'] !!}
        </div>
        <div class="container page-container service-container">
            <!-- Use for get current id -->
            <input type="hidden" name="group_id" value="{{ $service_id }}" />

            @foreach ($services as $index => $service)
                @if ($index % 2 === 0)
                    <div class="row service-row">
                        <div class="col-md-1 hidden-sm hidden-xs"></div>
                        <div class="col-sm-4 col-md-3 text-center">
                            @if (count(json_decode($service['images'])) > 0)
                                <div class="circle-service-image"
                                    style="background-image: url('{{ asset('storage') }}/photos/{{ json_decode($service['images'])[0] }}')">
                                </div>
                            @endif
                        </div>
                        <div class="col-sm-8 col-md-7">
                            <h1 id="sci_{{ $service['id'] }}" class="service-name">{{ $service['name'] }}
                            </h1>
                            <p class="cat_title">{{ $service['description'] }}</p>
                            @foreach (json_decode($service['features']) as $item)
                                @switch($item->name)
                                    @case('desc')
                                        <p>{{ $item->desc }}</p>
                                    @break
                                    @case('center')
                                    @break
                                    @default
                                        <div class="box_service">
                                            <div class="detail-price-item">
                                                <span class="detail-price-name">{{ $item->name }}</span>
                                                <span class="detail-price-dots"></span>
                                                <span class="detail-price-number">
                                                    <span class="current">{{ $item->desc }}</span>
                                                </span>
                                            </div>
                                            <div></div>
                                        </div>
                                @endswitch
                            @endforeach
                        </div>
                        <div class="col-md-1 hidden-sm hidden-xs"></div>
                    </div>
                @else
                    <div class="row service-row">
                        <div class="col-md-1 hidden-sm hidden-xs"></div>
                        <div class="col-xs-12 text-center visible-xs">
                            @if (count(json_decode($service['images'])) > 0)
                                <div class="circle-service-image"
                                    style="background-image: url('{{ asset('storage') }}/photos/{{ json_decode($service['images'])[0] }}')">
                                </div>
                            @endif
                        </div>
                        <div class="col-sm-8 col-md-7">
                            <h1 id="sci_{{ $service['id'] }}" class="service-name"> {{ $service['name'] }}
                            </h1>
                            <p class="cat_title"> {{ $service['description'] }}</p>
                            @foreach (json_decode($service['features']) as $item)
                                @switch($item->name)
                                    @case('desc')
                                        <p>{{ $item['desc'] }}</p>
                                    @break
                                    @case('center')
                                    @break
                                    @default
                                        <div class="box_service">
                                            <div class="detail-price-item">
                                                <span class="detail-price-name">{{ $item->name }}</span>
                                                <span class="detail-price-dots"></span>
                                                <span class="detail-price-number">
                                                    <span class="current">{{ $item->desc }}</span>
                                                </span>
                                            </div>
                                            <div></div>
                                        </div>
                                @endswitch
                            @endforeach
                        </div>
                        <div class="col-sm-4 col-md-3 text-center hidden-xs">
                            @if (count(json_decode($service['images'])) > 0)
                                <div class="circle-service-image"
                                    style="background-image: url('{{ asset('storage') }}/photos/{{ json_decode($service['images'])[0] }}')">
                                </div>
                            @endif
                        </div>
                        <div class="col-md-1 hidden-sm hidden-xs"></div>
                    </div>
                @endif
            @endforeach
        </div>
    </div>
</div>
