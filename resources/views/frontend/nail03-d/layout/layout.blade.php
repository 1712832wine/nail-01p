<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" dir="ltr" lang="en" xmlns:og="http://ogp.me/ns#"
    xmlns:fb="http://www.facebook.com/2008/fbml" itemscope itemtype="https://schema.org/NailSalon">

<head>
    <!-- Language -->
    <meta http-equiv="content-language" content="en" />
    <link rel="alternate" href="http://fnail03d.fastboywebhosts.com" hreflang="x-default">
    <link rel="alternate" href="http://fnail03d.fastboywebhosts.com" hreflang="en-US">

    <!-- Meta limit -->

    <!-- Meta Tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width,initial-scale=1.0" />
    <meta http-equiv="content-type" content="text/html; charset=UTF-8" />
    <meta name="description" content="" />
    <meta name="keywords" content="" />
    <meta name="author" content="" />

    <!-- OG -->
    <meta property="og:title" content="" />
    <meta property="og:description" content="" />
    <meta property="og:image" content="" />
    <meta property="og:url" content="http://fnail03d.fastboywebhosts.com/" />
    <meta property="og:type" content="website" />
    <meta property="og:site_name" content="Beauty Nails" />

    <!-- Dublin Core -->
    <link rel="schema.DC" href="http://purl.org/dc/elements/1.1/">
    <meta name="DC.title" content="">
    <meta name="DC.identifier" content="http://fnail03d.fastboywebhosts.com/">
    <meta name="DC.description" content="">
    <meta name="DC.subject" content="">
    <meta name="DC.language" scheme="UTF-8" content="">
    <meta itemprop="priceRange" name="priceRange" content="">

    <!-- GEO meta -->
    <meta name="geo.region" content="">
    <meta name="geo.placename" content="">
    <meta name="geo.position" content="">
    <meta name="ICBM" content="">

    <!-- Page Title -->
    <title>Beauty Nails</title>
    <base href="frontend/themes/fnail03d/assets/">

    <!-- canonical -->
    <link rel="canonical" href="http://fnail03d.fastboywebhosts.com">



    <!-- Favicons -->
    <link rel="icon" href="http://fnail03d.fastboywebhosts.com/uploads/fnail0dlojwt6/attach/1562750359_fbm_fvc.png"
        type="image/x-icon">
    <link rel="shortcut icon"
        href="http://fnail03d.fastboywebhosts.com/uploads/fnail0dlojwt6/attach/1562750359_fbm_fvc.png"
        type="image/x-icon">

    <link rel="stylesheet" type="text/css"
        href="https://fonts.googleapis.com/css?family=Montserrat:400,700|Roboto:100,100i,300,300i,400,400i,500,500i,700,700i,900,900i&subset=greek,greek-ext,latin-ext,vietnamese">
    <link rel="stylesheet" type="text/css"
        href="https://fonts.googleapis.com/css?family=Dosis:200,300,400,500,600,700,800&subset=latin-ext">
    <link rel="stylesheet" type="text/css"
        href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i,800,800i&subset=greek-ext,latin-ext,vietnamese">
    <link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Stardos+Stencil">
    <link rel="stylesheet" type="text/css"
        href="https://fonts.googleapis.com/css?family=Great+Vibes|Prata&subset=cyrillic,cyrillic-ext,latin-ext,vietnamese">

    <link rel="stylesheet" type="text/css" href='{{ asset('frontend') }}/public/library/jquery-ui/jquery-ui.min.css'>
    <link rel="stylesheet" type="text/css" href='{{ asset('frontend') }}/public/library/validation/validation.css'>
    <link rel="stylesheet" type="text/css" href='{{ asset('frontend') }}/public/library/flex-label/flex-label.css'>
    <link rel="stylesheet" type="text/css" href='{{ asset('frontend') }}/public/library/animate/animate.css'>
    <link rel="stylesheet" type="text/css"
        href='{{ asset('frontend') }}/public/library/bootstrap/v3.3.7/bootstrap.min.css'>
    <link rel="stylesheet" type="text/css"
        href='{{ asset('frontend') }}/public/library/bootstrap-datetimepicker/bootstrap-datetimepicker.min.css'>
    <link rel="stylesheet" type="text/css"
        href='{{ asset('frontend') }}/public/library/font-awesome/v4.7.0/css/font-awesome.min.css'>
    <link rel="stylesheet" type="text/css"
        href='{{ asset('frontend') }}/public/library/owl-carousel/v1.3.3/owl.carousel.min.css'>
    <link rel="stylesheet" type="text/css"
        href='{{ asset('frontend') }}/public/library/magnific-popup/magnific-popup.css'>
    <link rel="stylesheet" type="text/css"
        href='{{ asset('frontend') }}/public/library/slider-pro/slider-pro.min.css'>
    <link rel="stylesheet" type="text/css"
        href='{{ asset('frontend') }}/public/library/revolution/themepunch.revolution.settings.css'>
    <link rel="stylesheet" type="text/css" href='{{ asset('frontend') }}/public/library/instagram/instafeed.min.css'>
    <link rel="stylesheet" type="text/css" href='{{ asset('frontend') }}/public/library/signature/signature.css'>

    <link rel="stylesheet" type="text/css" href='{{ asset('frontend') }}/themes/fnail03d/assets/css/settings.css'>
    <link rel="stylesheet" type="text/css" href='{{ asset('frontend') }}/themes/fnail03d/assets/css/selectric.css'>
    <link rel="stylesheet" type="text/css" href='{{ asset('frontend') }}/themes/fnail03d/assets/css/menu_bar.css'>
    <link rel="stylesheet" type="text/css" href='{{ asset('frontend') }}/themes/fnail03d/assets/css/main-styles.css'>
    <link rel="stylesheet" type="text/css" href='{{ asset('frontend') }}/themes/fnail03d/assets/css/style.css?v=1.0'>
    <link rel="stylesheet" type="text/css" href='{{ asset('frontend') }}/themes/fnail03d/assets/css/main.css'>
    <link rel="stylesheet" type="text/css" href='{{ asset('frontend') }}/themes/fnail03d/assets/css/app.css'>
    <link rel="stylesheet" type="text/css" href='{{ asset('frontend') }}/themes/fnail03d/assets/css/responsive.css'>
    <link rel="stylesheet" type="text/css" href='{{ asset('frontend') }}/public/library/global/css/news.css'>
    <link rel="stylesheet" type="text/css" href='{{ asset('frontend') }}/public/library/global/css/giftcards.css'>
    <link rel="stylesheet" type="text/css" href='{{ asset('frontend') }}/public/library/global/css/cart-payment.css'>
    <link rel="stylesheet" type="text/css" href='{{ asset('frontend') }}/public/library/global/css/consent.css'>
    <link rel="stylesheet" type="text/css"
        href='{{ asset('frontend') }}/themes/fnail03d/assets/css/custom.css?v=1.1'>

    <script type="text/javascript">
        let webForm = {
            "required": "(required)",
            "optional": "(optional)",
            "any_person": "Any person",
            "price": "Price",
            "morning": "Morning",
            "afternoon": "Afternoon",
            "sunday": "Sunday",
            "monday": "Monday",
            "tuesday": "Tuesday",
            "wednesday": "Wednesday",
            "thursday": "Thursday",
            "friday": "Friday",
            "saturday": "Saturday",
            "jan": "Jan",
            "feb": "Feb",
            "mar": "Mar",
            "apr": "Apr",
            "may": "May",
            "jun": "Jun",
            "jul": "Jul",
            "aug": "Aug",
            "sep": "Sep",
            "oct": "Oct",
            "nov": "Nov",
            "dec": "Dec",
            "contact_name": "Your name",
            "contact_name_placeholder": "",
            "contact_name_maxlength": "76",
            "contact_email": "Your email address",
            "contact_email_placeholder": "",
            "contact_email_maxlength": "76",
            "contact_phone": "Your phone",
            "contact_phone_placeholder": "",
            "contact_phone_maxlength": "16",
            "contact_subject": "Your subject",
            "contact_subject_placeholder": "",
            "contact_subject_maxlength": "251",
            "contact_message": "Your message",
            "contact_message_placeholder": "",
            "contact_message_maxlength": "501",
            "contact_btn_send": "Send Us",
            "contact_name_err": "Please enter your name",
            "contact_email_err": "Please enter your email",
            "contact_phone_err": "Please enter your phone",
            "contact_subject_err": "Please enter your subject",
            "contact_message_err": "Please enter your message",
            "booking_name": "Your name",
            "booking_name_placeholder": "",
            "booking_name_maxlength": "76",
            "booking_phone": "Your phone",
            "booking_phone_placeholder": "",
            "booking_phone_maxlength": "16",
            "booking_email": "Your email",
            "booking_email_placeholder": "",
            "booking_email_maxlength": "76",
            "booking_service": "Service",
            "booking_service_placeholder": "Select service",
            "booking_menu": "Menu",
            "booking_menu_placeholder": "Select menu",
            "booking_technician": "Technician",
            "booking_technician_placeholder": "Select technician",
            "booking_person_number": "Number",
            "booking_date": "Date",
            "booking_date_placeholder": "",
            "booking_date_maxlength": "16",
            "booking_hours": "Hour",
            "booking_hours_placeholder": "Select hour",
            "booking_note": "Note",
            "booking_note_maxlength": "201",
            "booking_note_placeholder": "(Max length 200 character)",
            "booking_store": "Storefront",
            "booking_store_placeholder": "Select storefront",
            "booking_add_another_service": "Add another service",
            "booking_information": "Appointment Information",
            "booking_order_information": "Order Information",
            "booking_popup_message": "Message",
            "booking_popup_confirm": "Confirm booking information ?",
            "booking_popup_confirm_description": "We will send a text message to you via the number below after we confirm the calendar for your booking.",
            "booking_order_popup_confirm": "Confirm order information ?",
            "booking_order_popup_confirm_description": "We will send a text message to you via the number below after we confirm the calendar for your order.",
            "booking_btn_send": "Send appointment now",
            "booking_btn_search": "Search",
            "booking_btn_booking": "Booking",
            "booking_btn_confirm": "Confirm",
            "booking_btn_cancel": "Cancel",
            "booking_hours_expired": "Has expired",
            "booking_name_err": "Please enter your name",
            "booking_phone_err": "Please enter your phone",
            "booking_email_err": "Please enter your email",
            "booking_service_err": "Please choose a service",
            "booking_menu_err": "Please choose a menu",
            "booking_technician_err": "Please choose a technician",
            "booking_date_err": "Please choose date",
            "booking_hours_err": "Please choose hour",
            "booking_get_hours_timeout": "Network timeout, Please click the button search to try again"
        };
        let webBooking = {
            enable: true,
            minDate: "05/23/2021",
            requiredTechnician: false,
            requiredEmail: false,
            requiredHour: true,
        };
        let webFormat = {
            dateFormat: "MM/DD/YYYY",
            datePosition: "1,0,2",
            phoneFormat: "(000) 000-0000",
        };
        let webGlobal = {
            site: "idx",
            siteAct: "",
            siteSubAct: "",
            noPhoto: "/public/library/global/no-photo.jpg",
            isTablet: false,
            isMobile: false,
            enableRecaptcha: false,
        };

        var enable_booking = "1";
        var dateFormatBooking = "MM/DD/YYYY";
        var minDateBooking = '05/23/2021';
        var bookingRequiredTechnician = '';
        var bookingRequiredEmail = '';
        var posFormat = "1,0,2";
        var timeMorning = '[]';
        var timeAfternoon = '[]';
        var bookLogin = "open_booking";
        var beforeTime = "1";
        var beforeDay = 0;
        var currDateT = "2021/05/23";
        var checktimebooking = 1;
        var enableRecaptcha = 0;
        var hoursTimeFormat = 12;
        var phoneFormat = "(000) 000-0000";
        var company_phone = "832-968-6668";
        var num_paging = "12";
        var menu = "idx";

        let facebook_embed = {
            id_fanpage: "https://www.facebook.com/FastboyMarketingAgency",
            appId: "",
            width: 450,
            height: 300,
            tabs: "timeline",
            show_facepile: true,
            small_header: true,
            likebox_enable: true,
            likebox_show_faces: false,
            likebox_stream: true,
        };
        let google_id_fanpage = "";
        let twitter_id_fanpage = "";

    </script>

    <script type="text/javascript" src="{{ asset('frontend') }}/public/library/jquery/v1.10.2/jquery.min.js">
    </script>
    <script type="text/javascript" src="{{ asset('frontend') }}/public/library/jquery-ui/jquery-ui.min.js"></script>
    <script type="text/javascript" src="{{ asset('frontend') }}/public/library/jquery-ui/jquery-migrate.min.js">
    </script>
    <script type="text/javascript" src="{{ asset('frontend') }}/public/library/validation/validation.min.js">
    </script>
    <script type="text/javascript" src="{{ asset('frontend') }}/public/library/flex-label/flex-label.js"></script>
    <script type="text/javascript" src="{{ asset('frontend') }}/public/library/mask/mask.min.js"></script>
    <script type="text/javascript" src="{{ asset('frontend') }}/public/library/bootstrap/v3.3.7/bootstrap.min.js">
    </script>
    <script type="text/javascript"
        src="{{ asset('frontend') }}/public/library/waiting-modal-dialog/waiting-modal-dialog.js"></script>
    <script type="text/javascript" src="{{ asset('frontend') }}/public/library/moment/moment-with-locales.js">
    </script>
    <script type="text/javascript"
        src="{{ asset('frontend') }}/public/library/bootstrap-datetimepicker/bootstrap-datetimepicker.js"></script>
    <script type="text/javascript" src="{{ asset('frontend') }}/public/library/magnific-popup/magnific-popup.min.js">
    </script>
    <script type="text/javascript"
        src="{{ asset('frontend') }}/public/library/owl-carousel/v1.3.3/owl.carousel.min.js"></script>
    <script type="text/javascript" src="{{ asset('frontend') }}/public/library/slider-pro/slider-pro.min.js">
    </script>
    <script type="text/javascript"
        src="{{ asset('frontend') }}/public/library/revolution/jquery.themepunch.plugins.min.js"></script>
    <script type="text/javascript"
        src="{{ asset('frontend') }}/public/library/revolution/jquery.themepunch.revolution.min.js"></script>
    <script type="text/javascript" src="{{ asset('frontend') }}/public/library/pnotify/pnotify.js"></script>
    <script type="text/javascript" src="{{ asset('frontend') }}/public/library/wow/wow.min.js"></script>
    <script type="text/javascript" src="{{ asset('frontend') }}/public/library/instagram/instafeed.min.js"></script>
    <script type="text/javascript" src="{{ asset('frontend') }}/public/library/signature/signature.min.js"></script>
    <script type="text/javascript" src="{{ asset('frontend') }}/public/library/global/js/js_php.js"></script>
    <script type="text/javascript" src="{{ asset('frontend') }}/themes/fnail03d/assets/js/jcarousel.js"></script>
    <script type="text/javascript" src="{{ asset('frontend') }}/themes/fnail03d/assets/js/web3nhat.min.js"></script>

    <style type="text/css">
        .service-staff-info h5,
        .date-info,
        .time-info {
            font-size: 17px;
        }

        .content-side-menu,
        body,
        .header-container {
            z-index: 2;
            position: static;
        }

        .layout-header-1.pad-vertical-5.row.background-42.hidden-sm.hidden-xs {
            background: #868686;
        }

        p.title_giftcard {
            font-size: 24px;
        }

        @media screen and (max-width: 992px) {
            .content-item-about-us {
                margin-bottom: 0px;
                margin: 10px;
            }
        }

        .content-item-about-us p {
            margin-top: 10px;
        }

        .content-item-about-us,
        .img-item-about-us,
        .content-item-about-us p {
            width: 100%;
            padding: 0 !important;
            margin: 0 !important;
        }

        .about-us-page-1 .title-about-us {
            font-size: 30px;
            text-align: left;
            margin-bottom: 20px;
        }

        #owl-demo .item img {
            width: 18%;
        }

        .banner-page-list h2 {
            color: #000;
        }

        .banner-page-list {
            background-image: url(/files/bg-banner.jpg);
            background-size: cover;
        }

        .img-info-staff {
            display: none;
        }

        p.review {
            color: #850050 !IMPORTANT;
            font-weight: 600;
        }

        #owl-demo .item img {
            border: inherit !important;
        }

        .title-standard h2 {
            color: #000;
        }

        .abc .owl-item p {
            color: #fff;
            font-style: italic;
        }

        .our-services.abc {
            background-image: none !important;
            background: linear-gradient(to right, #e9e9ff, #945790) !important;
        }

        .layout-header-1.pad-vertical-5.row.background-42.hidden-sm.hidden-xs {
            background: linear-gradient(to right, #afafca, #945790) !important;
        }

        @media (max-width: 767px) {

            .our-services,
            .banner-page-list {
                background-position: center;
                background-size: 100%;
                background-attachment: scroll;
            }

            .our-services {
                background-size: cover;
            }
        }

        .footer-page {
            background-position: center;
        }

        .footer-page {
            background: linear-gradient(to right, #945790, #AFAFCB) !important;
        }

        #owl-demo .item img {
            width: 13%;
            text-align: center;
            margin: auto;
            display: block;
            width: 150px !important;
        }

        .review {
            padding-top: 10px;
        }

        @media (max-width: 687px) {
            .banner-page-list {
                height: 100px !important;
            }
        }

        @media (max-width: 575px) {
            .info-staff {
                float: none;
                width: 100%;
            }
        }

        .detail-price-name {
            font-weight: normal !important;
        }

        .detail-price-number {
            font-weight: 600;
        }

        .col-xs-4.staff-avatar {
            display: none;
        }

        .arrow-footer.zindex-1 {
            display: none;
        }

        .img-our-services {
            width: 100%;
            height: 327px;
        }

        @media only screen and (max-width: 687px) {
            .about-us-page-1 .title-about-us {
                font-size: 24px;
                text-align: center;
            }
        }

        .copyright p {
            color: #fff;
        }

        .contact-footer p spam {
            color: #fff;
        }

        .footer-page:after {
            background: none;
        }

    </style>

    <!-- Webmaster tools -->
    <!--Webmaster tools-->
    <!-- fb retargeting -->

    <!-- Schema Script -->
    <meta name="csrf-token" content="{{ csrf_token() }}" />
</head>

<body class="home site-idx">
    <h1 style="display: none"></h1>

    <!-- Tpl freeze header -->
    <!-- Active freeze header by insert this html into the website page -->
    <!-- value = "1": Mobile and Desktop;  -->
    <!-- value = "2": Mobile;  -->
    <!-- value = "3": Desktop;  -->
    <!--<input type="hidden" name="activeFreezeHeader" value="1">-->
    @php
        use App\Models\Product;
        use App\Models\Category;
        use App\Models\Album;
        use App\Models\Asset;
        $header = Product::where('category_id', Category::where('name', 'Header And Footer')->first()['id'])->first();
        $album = Album::where([['type', 'Social'], ['status', 'PUBLISHED']])->first();
        $social = Asset::where('albums_id', $album['id'])->orderBy('priority', 'desc')->get();
        $features = json_decode($header['features']);
    @endphp
    @include('frontend.nail03-d.component.header-mobile',
    ['header'=>$header])
    <div class="content-side-menu">
        <div class="shadow-mobile"></div>
        @include('frontend.nail03-d.component.header',
        ['header'=>$header])

        {{ $slot }}

        @include('frontend.nail03-d.component.footer',
        [
        'header'=>$header,
        'social'=>$social]))
    </div>
    <div class="freeze-footer">
        <ul>
            <li><a href="tel:{{ explode(',', $features[1]->desc)[0] }}" class="btn btn-default btn_call_now btn-call"
                    title="Call us">{{ explode(',', $features[1]->desc)[0] }}</a></li>
            <li><a href="/book" class="btn btn-default btn_make_appointment" title="Booking">Booking</a></li>
        </ul>
    </div>
    <!-- Main -->
    <script type="text/javascript">
        /*<![CDATA[*/

        var id = (jQuery('#bg13843').length) ? '#bg13843 ' : '';
        var jcarousel = $('.jcarousel').jcarousel({
            wrap: 'circular'
        });
        $(id + '.jcarousel').jcarouselAutoscroll({
            autostart: true,
            interval: 4000,
            target: '+=1'
        });
        $(id + '.jcarousel-control-prev').on('jcarouselcontrol:active', function() {
                $(this).removeClass('inactive');
            })
            .on('jcarouselcontrol:inactive', function() {
                $(this).addClass('inactive');
            })
            .jcarouselControl({
                target: '-=1'
            });

        $(id + '.jcarousel-control-next')
            .on('jcarouselcontrol:active', function() {
                $(this).removeClass('inactive');
            })
            .on('jcarouselcontrol:inactive', function() {
                $(this).addClass('inactive');
            })
            .jcarouselControl({
                target: '+=1'
            });
        $(id + '.jcarousel-pagination')
            .on('jcarouselpagination:active', 'a', function() {
                $(this).addClass('active');
            })
            .on('jcarouselpagination:inactive', 'a', function() {
                $(this).removeClass('active');
            })
            .on('click', function(e) {
                e.preventDefault();
            })
            .jcarouselPagination({
                perPage: 1,
                item: function(page) {
                    return '<a href="#' + page + '">' + page + '</a>';
                }
            });

    </script>
    <script type="text/javascript">
        (function($) {
            $.fn.clickToggle = function(func1, func2) {
                var funcs = [func1, func2];
                this.data('toggleclicked', 0);
                this.click(function() {
                    var data = $(this).data();
                    var tc = data.toggleclicked;
                    $.proxy(funcs[tc], this)();
                    data.toggleclicked = (tc + 1) % 2;
                });
                return this;
            };
        }(jQuery));
        $(document).ready(function() {
            $('.dropdown-toggle').clickToggle(function() {
                $(this).next().css('display', 'block');
            }, function() {
                $(this).next().css('display', 'none');
            });
        });

    </script>
    <script type="text/javascript">
        if (enable_booking == 0) {
            $(".btn_make_appointment").remove();
        }

    </script>
    <script type="text/javascript" src="{{ asset('frontend') }}/public/library/global/js/consent.js"></script>
    <script type="text/javascript" src="{{ asset('frontend') }}/themes/fnail03d/assets/js/main.js"></script>
    <script type="text/javascript" src="{{ asset('frontend') }}/themes/fnail03d/assets/js/app.js?v=1.2"></script>
    <!-- Google analytics -->
    <script>
        (function(i, s, o, g, r, a, m) {
            i['GoogleAnalyticsObject'] = r;
            i[r] = i[r] || function() {
                (i[r].q = i[r].q || []).push(arguments)
            }, i[r].l = 1 * new Date();
            a = s.createElement(o),
                m = s.getElementsByTagName(o)[0];
            a.async = 1;
            a.src = g;
            m.parentNode.insertBefore(a, m)
        })(window, document, 'script', 'https://www.google-analytics.com/analytics.js', 'ga');

        ga('create', '', 'auto');
        ga('send', 'pageview');

    </script>
    <!-- gg adwords remarketing -->

    <!-- End - Google analytics -->
    <script type="text/javascript"></script>
    <script>
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

    </script>
    @livewireScripts
</body>

</html>
