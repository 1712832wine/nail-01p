<div class="banner-page-list">
    <div class="title-page">
        <div class="ac">
            <h2>
                Coupons
            </h2>
            <div class="breakcrumb">
                <a href="/">Home</a>
                /
                <p>Coupons</p>
            </div>
        </div>
    </div>
</div>

<section class="coupon_page_v1">
    <div class="container">
        <section class="coupon_code_v1">
            <div class="row ">
                @foreach ($list as $item)
                    <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                        <div class="coupon_img_v1">
                            <a rel="group2" href="{{ asset('storage') }}/photos/{{ $item['url'] }}"
                                class="coupon_gift">
                                <img itemprop="image" class="img_size"
                                    src="{{ asset('storage') }}/photos/{{ $item['url'] }}" alt="coupon v1">
                            </a>
                        </div>
                    </div>
                @endforeach
            </div>
        </section>
    </div>
</section>
