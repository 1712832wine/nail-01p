<header class="header">
    <div class="wrap-freeze-header-mobile clearfix hidden-md hidden-lg menu-1024-hidden">
        <div class="flag-freeze-header-mobile">
            <div class="menu_mobile_v1">
                <div class="mobile_logo">
                    <a itemprop="url" href="/" title="Luxury Nails & Spa">
                        <img class="imgrps"
                            src="https://fnail04k.fastboywebsites.com/uploads/fnail0naok6sw/attach/1566356513_fnail04klogo.jpg"
                            alt="Luxury Nails & Spa" itemprop="logo image">
                    </a>
                </div>
                <div class="mobile_menu_container_v1">
                    <div class="mobile-menu clearfix">
                        <nav id="mobile_dropdown">
                            <!-- Tpl menu mobile layouts -->
                            <ul>
                                <li><a itemprop="url" href="/" title="Home">Home</a></li>
                                <li><a itemprop="url" href="/about" title="About Us">About Us</a></li>
                                <li><a itemprop="url" href="/services" title="Services">Services</a></li>
                                <li><a itemprop="url" href="/book" class="btn_make_appointment"
                                        title="Booking">Booking</a></li>
                                <li><a itemprop="url" href="/gallery" title="Gallery">Gallery</a></li>
                                <li><a itemprop="url" href="/coupons" title="Coupons">Coupons</a></li>
                                <li><a itemprop="url" href="/giftcards" title="Giftcards">Giftcards</a></li>
                                <li><a itemprop="url" href="/contact" title="Contact Us">Contact Us</a></li>
                            </ul>
                        </nav>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="wrap-freeze-header clearfix hidden-xs hidden-sm">
        <div class="flag-freeze-header">
            <nav class="header-main nav-desktop">
                <div class="clearfix container">
                    <!-- Tpl menu main layouts -->
                    <div class="header-nav-desktop">
                        <div class="header-nav left">
                            <ul class="list-line header-left">
                                <li><a itemprop="url" href="/" title="Home">Home</a></li>
                                <li><a itemprop="url" href="/about" title="About Us">About Us</a></li>
                                <li><a itemprop="url" href="/services" title="Services">Services</a></li>
                                <li><a itemprop="url" href="/gallery" title="Gallery">Gallery</a></li>
                            </ul>
                        </div>
                        <div class="site-logo center">
                            <div class="logo"><a itemprop="url" href="/" title="Luxury Nails & Spa">
                                    <img class="imgrps"
                                        src="https://fnail04k.fastboywebsites.com/uploads/fnail0naok6sw/attach/1566356513_fnail04klogo.jpg"
                                        alt="Luxury Nails & Spa" itemprop="logo image">
                                </a></div>
                        </div>
                        <div class="header-nav right">
                            <ul class="list-line header-right">
                                <li><a itemprop="url" href="/book" class="btn_make_appointment"
                                        title="Booking">Booking</a></li>
                                <li><a itemprop="url" href="/coupons" title="Coupons">Coupons</a></li>
                                <li><a itemprop="url" href="/giftcards" title="Giftcards">Giftcards</a></li>
                                <li><a itemprop="url" href="/contact" title="Contact Us">Contact Us</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </nav>
        </div>
    </div>
</header>
