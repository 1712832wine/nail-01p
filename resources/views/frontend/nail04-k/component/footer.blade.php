<footer class="footer">
    <!-- Tpl footer main -->
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-3">
                <div class="footer-openhours">
                    <div class="foh-wrap">
                        <!-- Normal Day -->
                        <div class="foh-row short" itemprop="openingHours" content="Mon - Fri 10:00 am - 7:00 pm">
                            <span class="foh-date">Mon - Fri:</span>
                            <span class="foh-time">10:00 am - 7:00 pm</span>
                        </div>

                        <!-- Normal Day -->
                        <div class="foh-row short" itemprop="openingHours" content="Saturday 10:00 am - 6:00 pm">
                            <span class="foh-date">Saturday:</span>
                            <span class="foh-time">10:00 am - 6:00 pm</span>
                        </div>

                        <!-- Close Day -->
                        <div class="foh-row short">
                            <span class="foh-date">Sunday:</span>
                            <span class="foh-time">Closed</span>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="footer-logo"><a itemprop="url" href="/" title="Luxury Nails & Spa">
                        <img class="imgrps"
                            src="https://fnail04k.fastboywebsites.com/uploads/fnail0naok6sw/attach/1566356513_fnail04klogo.jpg"
                            alt="Luxury Nails & Spa" itemprop="logo image">
                    </a></div>
                <div class="footer-copyright">
                    <p class="footer-address" itemprop="address" itemscope=""
                        itemtype="http://schema.org/PostalAddress"><span itemprop="streetAddress">11011 Richmond Ave
                            <br.Ste 250 <br /> Houston, TX 77042
                        </span></p>
                    <p class="footer-address" itemprop="address" itemscope=""
                        itemtype="http://schema.org/PostalAddress"><a href="tel:832-968-6668"><span
                                itemprop="streetAddress">832-968-6668</span></a></p>
                    <p class="footer-address" itemprop="address" itemscope=""
                        itemtype="http://schema.org/PostalAddress"><a href="mailto:web@fastboy.net"><span
                                itemprop="streetAddress">web@fastboy.net</span></a></p>
                    <p class="footer-address" itemprop="address" itemscope=""
                        itemtype="http://schema.org/PostalAddress"><a href="/"><span itemprop="streetAddress">Copyright
                                2021 © Luxury Nails & Spa. All Rights
                                Reserved.</span></a></p>
                </div>
            </div>
            <div class="col-md-3">
                <ul class="list-line social">
                    <!-- facebook link -->
                    <li>
                        <a itemprop="url" rel="nofollow" target="_blank" title="Facebook"
                            href="https://www.facebook.com/FastboyMarketingAgency/">
                            <img src="{{asset('frontend')}}/public/library/social/square/facebook.png" alt="Facebook">
                        </a>
                    </li>

                    <!-- google link -->
                    <li>
                        <a itemprop="url" rel="nofollow" target="_blank" title="Google plus"
                            href="https://www.google.com/maps/place/Fast+Boy+Marketing">
                            <img src="{{asset('frontend')}}/public/library/social/square/google-plus.png" alt="Google plus">
                        </a>
                    </li>

                    <!-- twitter link -->

                    <!-- youtube link -->
                    <li>
                        <a itemprop="url" rel="nofollow" target="_blank" title="Youtube"
                            href="https://www.youtube.com/">
                            <img src="{{asset('frontend')}}/public/library/social/square/youtube.png" alt="Youtube">
                        </a>
                    </li>

                    <!-- instagram link -->
                    <li>
                        <a itemprop="url" rel="nofollow" target="_blank" title="Instagram"
                            href="https://www.instagram.com">
                            <img src="{{asset('frontend')}}/public/library/social/square/instagram.png" alt="Instagram">
                        </a>
                    </li>

                    <!-- yelp link -->
                    <li>
                        <a itemprop="url" rel="nofollow" target="_blank" title="Yelp"
                            href="https://www.yelp.com/biz/isse-salon-hillsboro">
                            <img src="{{asset('frontend')}}/public/library/social/square/yelp.png" alt="Yelp">
                        </a>
                    </li>

                    <!-- vimeo link -->

                    <!-- blog link -->

                    <!-- pinterest link -->
                    <li>
                        <a itemprop="url" rel="nofollow" target="_blank" title="Pinterest"
                            href="https://www.pinterest.com/">
                            <img src="{{asset('frontend')}}/public/library/social/square/pinterest.png" alt="Pinterest">
                        </a>
                    </li>

                    <!-- yellowpage link -->
                    <li>
                        <a itemprop="url" rel="nofollow" target="_blank" title="Yellowpages"
                            href="https://www.yellowpages.com/">
                            <img src="{{asset('frontend')}}/public/library/social/square/yellowpages.png" alt="Yellowpages">
                        </a>
                    </li>

                    <!-- foursquare link -->
                    <li>
                        <a itemprop="url" rel="nofollow" target="_blank" title="Foursquare"
                            href="https://foursquare.com/">
                            <img src="{{asset('frontend')}}/public/library/social/square/foursquare.png" alt="Foursquare">
                        </a>
                    </li>

                    <!-- flickr link -->
                </ul>
                <div class="clearfix" style="height: 15px;"></div><!-- Start Single Footer Widget -->
                <div class="social-fanpage clearfix">
                    <aside>
                        <!-- facebook fanpage -->
                        <div id="fanpage_fb_container"></div>
                    </aside>
                    <!-- use for calculator width -->
                    <div id="social_block_width" class="clearfix" style="width:100% !important; height: 1px !important">
                    </div>
                </div><!-- End Single Footer Widget -->
            </div>
        </div>
    </div>
</footer>

<div class="freeze-footer">
    <ul>
        <li><a href="tel:832-968-6668" class="btn btn-default btn_call_now btn-call" title="Call us">832-968-6668</a>
        </li>
        <li><a href="/book.html" class="btn btn-default btn_make_appointment" title="Booking">Booking</a></li>
    </ul>
</div>
