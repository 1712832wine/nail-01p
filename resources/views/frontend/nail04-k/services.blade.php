<main class="main">
    <!-- Tpl main service -->
    <div class="pmain">
        <div class="page-heading"><img src="{{ asset('frontend') }}/themes/fnail04k/assets/images/heading-1.jpg"
                class="imgrps" alt="heading-1.jpg" /></div>
        <div class="in-container">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <div class="in-content">
                            <div class="page-title">
                                <h2 itemprop="name">Our Services</h2>
                            </div>
                            <div class="service-container animation_sroll_button">
                                <div class="row service-row">
                                    <div class="col-sm-12">
                                        <h2>Nail Services</h2>
                                        <p style="text-align: justify;">Solar nails are a good option for women with
                                            naturally weak nails that may easily break. They are still the strongest
                                            product in the industry. Solar nails are artificial nails that are
                                            applied by mixing Solar powder with liquid, proportionately to make a
                                            hardening mixture. This mixture is then applied to your nails to enhance
                                            the look of your natural nails and make them feel durable and
                                            long-lasting. If your natural nails are short from wear and tear or if
                                            you just want a longer nail for special occasions, a Solar nail
                                            treatment at our salon will be a perfect choice of you to get your nails
                                            to be lengthened and shaped artificially. The most popular Solar style
                                            is pink and white, but you may choose other options and colors that fit
                                            your personality.</p>
                                    </div>
                                </div>
                                <div class="clearfix animation_sroll_to_service service-list">
                                    <!-- Use for get current id -->
                                    <input type="hidden" name="group_id" value="">
                                    @foreach ($services as $index => $service)
                                        @if ($index % 2)
                                            <div class="row clearfix" id="sci_89">
                                                <div class="col-sm-4 col-md-4  text-center">
                                                    <div class="service-image circle">
                                                        <div class="service-image-bg"
                                                            style="background-image: url('{{ asset('storage') }}/photos/{{ json_decode($service['images'])[0] }}')">
                                                            <img class="img-responsive"
                                                                src="{{ asset('storage') }}/photos/{{ json_decode($service['images'])[0] }}"
                                                                alt="P & W Powder Nails">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-sm-8 col-md-8 ">
                                                    <div class="clearfix service-list">
                                                        <h2 class="service-name">{{ $service['name'] }}</h2>
                                                        <div class="service-desc"></div>
                                                        @foreach (json_decode($service['features']) as $item)
                                                            <div class="service-item item-352">
                                                                <div class="detail-price-item">
                                                                    <span
                                                                        class="detail-price-name">{{ $item->name }}</span>
                                                                    <span class="detail-price-dots"></span>
                                                                    <span class="detail-price-number">
                                                                        <span class="current">{{ $item->desc }}</span>
                                                                    </span>
                                                                </div>
                                                            </div>
                                                        @endforeach
                                                        <!-- Service sub -->
                                                    </div>
                                                </div>
                                            </div>
                                        @else
                                            <div class="row clearfix" id="sci_{{ $service['id'] }}">
                                                <div class="service-line"></div>
                                                <div class="col-sm-4 col-md-4 col-md-push-8 col-sm-push-8 text-center">
                                                    <div class="service-image circle">
                                                        <div class="service-image-bg"
                                                            style="background-image: url('{{ asset('storage') }}/photos/{{ json_decode($service['images'])[0] }}')">
                                                            <img class="img-responsive"
                                                                src="{{ asset('storage') }}/photos/{{ json_decode($service['images'])[0] }}"
                                                                alt="Gel Powder Nails">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-sm-8 col-md-8 col-md-pull-4 col-sm-pull-4">
                                                    <div class="clearfix service-list">
                                                        <h2 class="service-name">{{ $service['name'] }}</h2>
                                                        <div class="service-desc"></div>
                                                        @foreach (json_decode($service['features']) as $item)
                                                            <div class="service-item item-357">
                                                                <div class="detail-price-item">
                                                                    <span
                                                                        class="detail-price-name">{{ $item->name }}</span>
                                                                    <span class="detail-price-dots"></span>
                                                                    <span class="detail-price-number">
                                                                        <span
                                                                            class="current">{{ $item->desc }}</span>
                                                                    </span>
                                                                </div>
                                                            </div>
                                                        @endforeach
                                                        <!-- Service sub -->
                                                    </div>
                                                </div>
                                            </div>
                                        @endif
                                    @endforeach
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</main>
