<main class="main">

    <div class="pmain">
        @foreach ($articles as $item)
            <div class="page-heading">
                <img src="{{ asset('storage') }}/photos/{{ json_decode($item['image'])[0] }}" class="imgrps"
                    alt="heading-1.jpg">
            </div>
            <div class="in-container">
                <div class="container">
                    <div class="in-content">
                        <!-- Tpl introduction about -->
                        <div class="in-content">
                            <div class="page-title">
                                <h2>About us</h2>
                                <h3>Welcome to Luxury Nails & Spa</h3>
                            </div>
                            {!! $item['content'] !!}
                        </div>
                    </div>
                </div>
            </div>
        @endforeach

    </div>
</main>
