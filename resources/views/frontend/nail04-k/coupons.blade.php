<main class="main">
    <!-- Tpl main coupons -->
    <div class="pmain">
        <div class="page-heading"><img src="/themes/fnail04k/assets/images/heading-1.jpg" class="imgrps"
                alt="heading-1.jpg" caption="false" /></div>
        <div class="in-container">
            <div class="container">
                <div class="row">
                    @foreach ($list as $item)
                        <div class="col-xs-12 col-sm-6 col-md-6">
                            <div class="pointer m-magnific-popup" data-group="coupon" title="Special Promotions"
                                href="{{ asset('storage') }}/photos/{{ $item['url'] }}">
                                <div class="m-coupon-box">
                                    <img itemprop="image" src="{{ asset('storage') }}/photos/{{ $item['url'] }}"
                                        alt="Special Promotions" />
                                </div>
                            </div>
                        </div>
                    @endforeach
                </div>
            </div>
        </div>
    </div>
</main>
