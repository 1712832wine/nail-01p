<main class="main">

    <div class="pmain">
        <div class="page-heading">
            <img src="images/heading-1.jpg" class="imgrps" alt="heading-1.jpg">
        </div>
        <div class="in-container">
            <div class="container">
                <div class="in-content">
                    <div class="page-title">
                        <h2>Gallery</h2>
                    </div>
                    <ul class="clearfix m-category-tab" id="category_tab">
                        @foreach ($albums as $index => $album)
                            <li class="tab @if ($index===0) active @endif" data-id="{{ $album['id'] }}">
                                <span itemprop="name">{{ $album['name'] }}</span>
                            </li>
                        @endforeach
                    </ul>
                    <div class="clearfix m-gallery-content" id="gallery_content">
                        <div class="clearfix m-gallery-listing listing">

                        </div>
                        <div class="clearfix m-gallery-paging paging">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</main>
