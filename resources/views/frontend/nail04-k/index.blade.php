<main class="main">
    <!-- Tpl main board -->
    <!--# Support render.slider.board, render.booking.board, render.service.board,# render.gallery.board, render.testimonial.board-->
    <div class="section-slider-wrap">
        <section class="section-slider">
            @if (count($imgs_carousel) > 0)
                <div class="slider-width-height"
                    style="display: inline-block;width: 100%;height: 1px;overflow: hidden;">
                    <div class="fixed" style="width: 100%"></div>
                    <img src="{{ asset('storage') }}/photos/{{ $imgs_carousel[0]['url'] }}"
                        style="width: 100%; height: auto" alt="" />
                </div>
            @endif
            <div class="slider-pro" id="my-slider" style="display: none">
                <div class="sp-slides">
                    @foreach ($imgs_carousel as $img)
                        <div class="sp-slide">
                            <div class="sp-layer sp-static">
                                <a href="{{ route('home') }}" target="_self" title="slide7.png">
                                    <img class="sp-image" src="{{ asset('storage') }}/photos/{{ $img['url'] }}"
                                        alt="slide7.png" />
                                </a>
                            </div>
                        </div>
                    @endforeach
                </div>
            </div>
            <div class="slider-pro" id="my-slider-fixed-height" style="display: none">
                <div class="sp-slides">
                    @foreach ($imgs_carousel as $img)
                        <div class="sp-slide">
                            <a href="{{ route('home') }}" target="_self" title="slide7.png">
                                <div class="sp-layer sp-static" data-width="100%" data-height="100%" style="
                                        width: 100%;
                                        height: 100%;
                                        background: url('{{ asset('storage') }}/photos/{{ $img['url'] }}')
                                        center center no-repeat;
                                        background-size: cover;"></div>
                            </a>
                        </div>
                    @endforeach
                </div>
            </div>
        </section>
    </div>
    <section class="section section-booking">
        <div class="booking-container">
            <!-- Tpl booking board -->
            <div class="container">
                <div class="booking-inner">
                    <h2 itemprop="name">Welcome to <br />Luxury Nails & Spa</h2>
                    <p itemprop="description">Our mission is to provide the highest quality service, in union with
                        the highest quality products for the well being of our guests and the environment.</p><a
                        itemprop="url" href="/book" class="btn btn_make_appointment" title="Book Online">Book
                        Online</a>
                </div>
            </div>
        </div>
    </section> <!-- Tpl service board -->
    <!--# Support render.service_data.board-->
    <section class="section section-gallery">
        <div class="container">
            <h2 class="mTitle"><span style="color: #000000;">Our Services</span></h2>
            <div>
                <div class="services-listall row">
                    @foreach ($services as $service)
                        <div class="col-xs-6 col-sm-4 col-md-4">
                            <a itemprop="url"
                                href="{{ route('services-detail', ['slug' => strtolower(trim(preg_replace('/[^A-Za-z0-9-]+/', '-', $service['name']))), 'service_id' => $service['id']]) }}"
                                title="{{ $service['name'] }}">
                                <div class="item service">
                                    <div class="thumb">
                                        <img itemprop="image"
                                            src="{{ asset('storage') }}/photos/{{ json_decode($service['images'])[0] }}"
                                            class="imgrps" alt="{{ $service['name'] }}">
                                    </div>
                                    <div class="info">
                                        <h3 itemprop="name">{{ $service['name'] }}</h3>
                                    </div>
                                </div>
                            </a>
                        </div>
                    @endforeach
                </div>
            </div>
        </div>
    </section> <!-- Tpl gallery board -->
    <!--# Support render.gallery_data.board, render.gallery_slider_data.board-->
    <section class="section section-gallery">
        <div class="container">
            <h2 class="mTitle">Our gallery</h2>
            <div>
                <div class="m-gallery-box-wrap">
                    <div class="row">
                        @foreach ($gallery_list as $item)
                            <div class="col-xs-6 col-sm-6 col-md-4">
                                <div class="pointer m-magnific-popup" data-group="gallery-14" title="Nails Design"
                                    href="{{ asset('storage') }}/photos/{{ $item['url'] }}">
                                    <div class="m-gallery-box">
                                        <div class="m-image-bg"
                                            style="background-image: url('{{ asset('storage') }}/photos/{{ $item['url'] }}');">
                                            <img itemprop="image"
                                                src="{{ asset('storage') }}/photos/{{ $item['url'] }}"
                                                alt="Nails Design">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </section>
</main>
