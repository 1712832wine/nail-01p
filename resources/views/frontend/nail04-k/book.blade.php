<main class="main">

    <div class="pmain">
        <div class="page-heading">
            <img src="images/heading-1.jpg" class="imgrps" alt="heading-1.jpg">
        </div>
        <div class="in-container">
            <div class="container">
                <div class="in-content">
                    <div class="page-title">
                        <h2>Booking</h2>
                    </div>
                    <section id="boxBookingForm" class="box-booking-form">
                        <form id="formBooking" class="form-booking" name="formBooking" action="/book/add" method="post"
                            enctype="multipart/form-data">
                            <div class="row">
                                <div class="col-sm-6 col-md-4 col-lg-3 booking-date">
                                    <div class="group-select">
                                        <label>Date (required)</label>
                                        <div class="relative w100 form-input-group">
                                            <input type="text" class="form-control form-text booking_date"
                                                autocomplete="off" name="booking_date" value=""
                                                data-validation="[NOTEMPTY]"
                                                data-validation-message="Please choose date" title="Booking date"
                                                placeholder="" maxlength="16">
                                            <span class="fa fa-calendar calendar form-icon"></span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-12 col-md-12 col-lg-9">
                                    <div class="row booking-service-staff booking-item" id="bookingItem_0">
                                        <div class="col-sm-12 col-md-7 col-lg-8 booking-service">
                                            <div class="group-select">
                                                <label>Service (required)</label>
                                                <div class="relative w100">
                                                    <select class="form-control booking_service" name="product_id[]"
                                                        data-validation="[NOTEMPTY]"
                                                        data-validation-message="Please choose a service"
                                                        title="Booking service">
                                                        <option value="">Select service</option>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-sm-12 col-md-5 col-lg-4 booking-staff">
                                            <div class="group-select">
                                                <label>Technician (optional)</label>
                                                <div class="relative w100">
                                                    <select class="form-control booking_staff" name="staff_id[]"
                                                        title="Booking staff">
                                                        <option value="">Select technician</option>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <!-- Button Add Item -->
                                    <div class="clearfix">
                                        <div class="add-services pointer booking_item_add">
                                            <i class="fa fa-plus-circle"></i> Add another service
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-12 col-md-12 col-lg-3">
                                    <div class="group-select">
                                        <label>&nbsp;</label>
                                        <div class="relative w100"><button class='btn btn-search search_booking'
                                                type='button'>Search</button></div>

                                        <!-- Hidden data -->
                                        <input type="hidden" name="booking_hours" class="booking_hours" value="">
                                        <input type="hidden" name="booking_name" class="booking_name" value="">
                                        <input type="hidden" name="booking_phone" class="booking_phone" value="">
                                        <input type="hidden" name="booking_email" class="booking_email" value="">
                                        <input type="hidden" name="notelist" class="notelist" value="">
                                        <input type="hidden" name="store_id" class="store_id" value="">

                                        <input type="hidden" name="booking_area_code" class="booking_area_code"
                                            value="1">
                                        <input type="hidden" name="booking_form_email" class="booking_form_email"
                                            value="0" />
                                        <input type="hidden" name="nocaptcha" class="nocaptcha" value="1">
                                        <input type="hidden" name="g-recaptcha-response" class="g-recaptcha-response"
                                            value="">
                                    </div>
                                </div>
                            </div>
                        </form>

                        <script type="text/javascript">
                            $(document).ready(function() {
                                /* Init Booking */
                                webBookingForm.init(
                                    '{"89":{"id":89,"name":"P & W Powder Nails","services":[352,353,354,445,446]},"91":{"id":91,"name":"Gel Powder Nails","services":[357,358,359,360]},"92":{"id":92,"name":"Acrylic Nails","services":[361,362,363,364]},"93":{"id":93,"name":"Extra Services","services":[365,366,367,368,369,426]},"94":{"id":94,"name":"Natural Nail","services":[370,371,372,419]},"96":{"id":96,"name":"Little Princess","services":[375,376,377,378]},"97":{"id":97,"name":"Mani & Pedi Combo ","services":[379,380,381,382]},"98":{"id":98,"name":"Waxing","services":[383,384,385,386,387,388,389,390,391,392,393,394,395,396,397,398,399,400,401,402,403,404,405]},"99":{"id":99,"name":"Miscellaneous","services":[406,407,408,409,410,411,412]},"100":{"id":100,"name":"Pedicures","services":[413,414,415,416,417,418,424,425]},"102":{"id":102,"name":"Extra Service","services":[429,430,431,432,433,434,435,436,437,438,439]},"103":{"id":103,"name":"For The Feet","services":[440,441,442,443,444]}}',
                                    '{"352":{"id":352,"name":"Full Set","price":"$50 & up","staffs":[1],"schedule":false,"unit":"person","scheduleDay":null},"353":{"id":353,"name":"Fill-in, Pink & White","price":"$45 & up","staffs":[1],"schedule":false,"unit":"person","scheduleDay":null},"354":{"id":354,"name":"Fill-in, Pink only","price":"$30 & up","staffs":[1],"schedule":false,"unit":"person","scheduleDay":null},"445":{"id":445,"name":"Full Set - Pink & White with Ombre","price":"$65 ","staffs":[1],"schedule":false,"unit":"person","scheduleDay":null},"446":{"id":446,"name":"Fill-in Pink &amp; White with Ombre","price":"$50 ","staffs":[1],"schedule":false,"unit":"person","scheduleDay":null},"357":{"id":357,"name":"Full Set","price":"$40 & up","staffs":[1],"schedule":false,"unit":"person","scheduleDay":null},"358":{"id":358,"name":"Full Set with No-Chip Polish","price":"$50 ","staffs":[1],"schedule":false,"unit":"person","scheduleDay":null},"359":{"id":359,"name":"Fill-in","price":"$28 & up","staffs":[1],"schedule":false,"unit":"person","scheduleDay":null},"360":{"id":360,"name":"Fill in with No-Chip Polish","price":"$40 ","staffs":[1],"schedule":false,"unit":"person","scheduleDay":null},"361":{"id":361,"name":"Full Set","price":"$32 & up","staffs":[1],"schedule":false,"unit":"person","scheduleDay":null},"362":{"id":362,"name":"Full Set with No-Chip Polish","price":"$45 ","staffs":[1],"schedule":false,"unit":"person","scheduleDay":null},"363":{"id":363,"name":"Fill-in","price":"$23 & up","staffs":[1],"schedule":false,"unit":"person","scheduleDay":null},"364":{"id":364,"name":"Fill in with No-Chip Polish","price":"$35 ","staffs":[1],"schedule":false,"unit":"person","scheduleDay":null},"365":{"id":365,"name":"Nail Repair","price":"$3 & up","staffs":[1],"schedule":false,"unit":"person","scheduleDay":null},"366":{"id":366,"name":"White Tips","price":"$5 ","staffs":[1],"schedule":false,"unit":"person","scheduleDay":null},"367":{"id":367,"name":"Cuticles, Soak & Cut","price":"$5 ","staffs":[1],"schedule":false,"unit":"person","scheduleDay":null},"368":{"id":368,"name":"Artificial Nail Take-off","price":"$10 ","staffs":[1],"schedule":false,"unit":"person","scheduleDay":null},"369":{"id":369,"name":"Additional Length","price":"$5 & up","staffs":[1],"schedule":false,"unit":"person","scheduleDay":null},"426":{"id":426,"name":"Advanced Shaping","price":"$5 & up","staffs":[1],"schedule":false,"unit":"person","scheduleDay":null},"370":{"id":370,"name":"Spa Manicure","price":"$18 ","staffs":[1],"schedule":false,"unit":"person","scheduleDay":null},"371":{"id":371,"name":"Collagen Manicure","price":"$28 ","staffs":[1],"schedule":false,"unit":"person","scheduleDay":null},"372":{"id":372,"name":"No-Chip - Spa Manicure","price":"$35 ","staffs":[1],"schedule":false,"unit":"person","scheduleDay":null},"419":{"id":419,"name":"Dip Powder","price":"$42 ","staffs":[1],"schedule":false,"unit":"person","scheduleDay":null},"375":{"id":375,"name":"Manicure","price":"$12 ","staffs":[1],"schedule":false,"unit":"person","scheduleDay":null},"376":{"id":376,"name":"Pedicure","price":"$20 ","staffs":[1],"schedule":false,"unit":"person","scheduleDay":null},"377":{"id":377,"name":"Polish Change-Hands","price":"$5 ","staffs":[1],"schedule":false,"unit":"person","scheduleDay":null},"378":{"id":378,"name":"Polish Change-Feet","price":"$8 ","staffs":[1],"schedule":false,"unit":"person","scheduleDay":null},"379":{"id":379,"name":"Ladies Combo","price":"$42 ","staffs":[1],"schedule":false,"unit":"person","scheduleDay":null},"380":{"id":380,"name":"Gentlemens Combo","price":"$45 ","staffs":[1],"schedule":false,"unit":"person","scheduleDay":null},"381":{"id":381,"name":"Teenager Combo","price":"$37 ","staffs":[1],"schedule":false,"unit":"person","scheduleDay":null},"382":{"id":382,"name":"Little Princess Combo","price":"$32 ","staffs":[1],"schedule":false,"unit":"person","scheduleDay":null},"383":{"id":383,"name":"Brow Clean up","price":"$10 & up","staffs":[1],"schedule":false,"unit":"person","scheduleDay":null},"384":{"id":384,"name":"Eyebrow Shape","price":"$15 & up","staffs":[1],"schedule":false,"unit":"person","scheduleDay":null},"385":{"id":385,"name":"Upper Lip","price":"$8 ","staffs":[1],"schedule":false,"unit":"person","scheduleDay":null},"386":{"id":386,"name":"Brow Clean up + Lip","price":"$16 & up","staffs":[1],"schedule":false,"unit":"person","scheduleDay":null},"387":{"id":387,"name":"Chin","price":"$12 ","staffs":[1],"schedule":false,"unit":"person","scheduleDay":null},"388":{"id":388,"name":"Cheek","price":"$15 ","staffs":[1],"schedule":false,"unit":"person","scheduleDay":null},"389":{"id":389,"name":"Brow Clean Up + Lip + Chin","price":"$25 ","staffs":[1],"schedule":false,"unit":"person","scheduleDay":null},"390":{"id":390,"name":"Full Face","price":"$40 ","staffs":[1],"schedule":false,"unit":"person","scheduleDay":null},"391":{"id":391,"name":"Hairline","price":"$10 ","staffs":[1],"schedule":false,"unit":"person","scheduleDay":null},"392":{"id":392,"name":"Sideburn Clean up","price":"$8 ","staffs":[1],"schedule":false,"unit":"person","scheduleDay":null},"393":{"id":393,"name":"Chin & Cheek","price":"$22 ","staffs":[1],"schedule":false,"unit":"person","scheduleDay":null},"394":{"id":394,"name":"Half Arm","price":"$30 & up","staffs":[1],"schedule":false,"unit":"person","scheduleDay":null},"395":{"id":395,"name":"Full Arm","price":"$40 & up","staffs":[1],"schedule":false,"unit":"person","scheduleDay":null},"396":{"id":396,"name":"Under Arm","price":"$20 ","staffs":[1],"schedule":false,"unit":"person","scheduleDay":null},"397":{"id":397,"name":"Half Leg","price":"$35 & up","staffs":[1],"schedule":false,"unit":"person","scheduleDay":null},"398":{"id":398,"name":"Full Leg","price":"$60 & up","staffs":[1],"schedule":false,"unit":"person","scheduleDay":null},"399":{"id":399,"name":"Bikini","price":"$30 & up","staffs":[1],"schedule":false,"unit":"person","scheduleDay":null},"400":{"id":400,"name":"Full Leg & Bikini","price":"$75 & up","staffs":[1],"schedule":false,"unit":"person","scheduleDay":null},"401":{"id":401,"name":"Brazilian","price":"$55 & up","staffs":[1],"schedule":false,"unit":"person","scheduleDay":null},"402":{"id":402,"name":"Neck","price":"$15 ","staffs":[1],"schedule":false,"unit":"person","scheduleDay":null},"403":{"id":403,"name":"Stomach","price":"$20 & up","staffs":[1],"schedule":false,"unit":"person","scheduleDay":null},"404":{"id":404,"name":"Back","price":"$50 & up","staffs":[1],"schedule":false,"unit":"person","scheduleDay":null},"405":{"id":405,"name":"Chest","price":"$40 & up","staffs":[1],"schedule":false,"unit":"person","scheduleDay":null},"406":{"id":406,"name":"Chrome Powder","price":"$4 per nail","staffs":[1],"schedule":false,"unit":"person","scheduleDay":null},"407":{"id":407,"name":"Shiny Buffer","price":"$5 per service","staffs":[1],"schedule":false,"unit":"person","scheduleDay":null},"408":{"id":408,"name":"French Polish","price":"$5 per service","staffs":[1],"schedule":false,"unit":"person","scheduleDay":null},"409":{"id":409,"name":"Polish Change Hands","price":"$10 ","staffs":[1],"schedule":false,"unit":"person","scheduleDay":null},"410":{"id":410,"name":"Polish Change Hands, French","price":"$14 ","staffs":[1],"schedule":false,"unit":"person","scheduleDay":null},"411":{"id":411,"name":"Polish Change Feet","price":"$12 ","staffs":[1],"schedule":false,"unit":"person","scheduleDay":null},"412":{"id":412,"name":"Polish Change Feet, French","price":"$16 ","staffs":[1],"schedule":false,"unit":"person","scheduleDay":null},"413":{"id":413,"name":"Spa Pedicure","price":"$30 ","staffs":[1],"schedule":false,"unit":"person","scheduleDay":null},"414":{"id":414,"name":"No-chip, Spa Pedicure","price":"$47 ","staffs":[1],"schedule":false,"unit":"person","scheduleDay":null},"415":{"id":415,"name":"Deluxe Pedicure","price":"$40 ","staffs":[1],"schedule":false,"unit":"person","scheduleDay":null},"416":{"id":416,"name":"Pedi-in-a-box","price":"$50 ","staffs":[1],"schedule":false,"unit":"person","scheduleDay":null},"417":{"id":417,"name":"Lavender & Jojoba","price":"$45 ","staffs":[1],"schedule":false,"unit":"person","scheduleDay":null},"418":{"id":418,"name":"Volcano Pedicure","price":"$65 ","staffs":[1],"schedule":false,"unit":"person","scheduleDay":null},"424":{"id":424,"name":"Milk & Honey Pedicure","price":"$55 ","staffs":[1],"schedule":false,"unit":"person","scheduleDay":null},"425":{"id":425,"name":"Marine Pedicure","price":"$70 ","staffs":[1],"schedule":false,"unit":"person","scheduleDay":null},"429":{"id":429,"name":"Freehand Design or Decals on Two Nails","price":"$5 & up","staffs":[1],"schedule":false,"unit":"person","scheduleDay":null},"430":{"id":430,"name":"Freehand Design or Decals on Ten Nails","price":"$10 & up","staffs":[1],"schedule":false,"unit":"person","scheduleDay":null},"431":{"id":431,"name":"Chrome Powder","price":"$4 per nail","staffs":[1],"schedule":false,"unit":"person","scheduleDay":null},"432":{"id":432,"name":"Shiny Buffer","price":"$5 per service","staffs":[1],"schedule":false,"unit":"person","scheduleDay":null},"433":{"id":433,"name":"French Polish","price":"$5 per service","staffs":[1],"schedule":false,"unit":"person","scheduleDay":null},"434":{"id":434,"name":"Polish Change Hands","price":"$10 ","staffs":[1],"schedule":false,"unit":"person","scheduleDay":null},"435":{"id":435,"name":"Polish Change Hands, French","price":"$14 ","staffs":[1],"schedule":false,"unit":"person","scheduleDay":null},"436":{"id":436,"name":"Polish Change Feet","price":"$14 ","staffs":[1],"schedule":false,"unit":"person","scheduleDay":null},"437":{"id":437,"name":"Polish Change Feet, French","price":"$18 ","staffs":[1],"schedule":false,"unit":"person","scheduleDay":null},"438":{"id":438,"name":"Paraffin Wax, Hands","price":"$10 ","staffs":[1],"schedule":false,"unit":"person","scheduleDay":null},"439":{"id":439,"name":"No-chip Take Off, Hands","price":"$10 ","staffs":[1],"schedule":false,"unit":"person","scheduleDay":null},"440":{"id":440,"name":"Heavy Callus Removal","price":"$5 ","staffs":[1],"schedule":false,"unit":"person","scheduleDay":null},"441":{"id":441,"name":"Extra Leg Massage (5 mins)","price":"$5 ","staffs":[1],"schedule":false,"unit":"person","scheduleDay":null},"442":{"id":442,"name":"No-chip Take-off, Feet","price":"$15 ","staffs":[1],"schedule":false,"unit":"person","scheduleDay":null},"443":{"id":443,"name":"Paraffin Wax, Feet","price":"$12 ","staffs":[1],"schedule":false,"unit":"person","scheduleDay":null},"444":{"id":444,"name":"Acrylic on Toes","price":"$8 per nail","staffs":[1],"schedule":false,"unit":"person","scheduleDay":null}}',
                                    'null',
                                    '{"listperson":[],"notelist":[],"booking_date":"05\/24\/2021","booking_hours":""}'
                                );
                            });

                        </script>
                    </section>

                    <section id="boxBookingInfo" class="box-booking-info relative" style="display: none;">
                        <h3 class="booking-info-title">Appointment Information</h3>

                        <!-- Service, Staff -->
                        <div id="boxServiceStaff" class="box-service-staff">
                            <div class="service-staff">
                                <div class="service-staff-avatar">
                                    <img class="img-responsive" src="/public/library/global/no-photo.jpg"
                                        alt="Any person">
                                </div>
                                <div class="service-staff-info">
                                    <h5>Any Technician</h5>
                                    <p>Any Service</p>
                                    <p>Price: N/A</p>
                                </div>
                            </div>
                            <div class="service-staff">
                                <div class="service-staff-avatar no-photo">
                                    <img class="img-responsive" src="/public/library/global/no-photo.jpg"
                                        alt="Any person">
                                </div>
                                <div class="service-staff-info">
                                    <h5>Any Technician</h5>
                                    <p>Any Service</p>
                                    <p>Price: N/A</p>
                                </div>
                            </div>
                        </div>

                        <!-- Date, Time List -->
                        <div id="boxDateTime" class="box-date-time">
                            <h4 class="date-info" id="dateInfo">Sunday, Jan-01-1970</h4>
                            <div class="time-info">
                                <h5>Morning <span class="time-note" id="timeAMNote">N/A</span></h5>
                                <ul class="time-items" id="timeAMHtml"></ul>
                            </div>
                            <div class="time-info">
                                <h5>Afternoon <span class="time-note" id="timePMNote">N/A</span></h5>
                                <ul class="time-items" id="timePMHtml"></ul>
                            </div>
                        </div>
                    </section>

                    <section id="popupBookingConfirm" class="popup-booking-confirm white-popup mfp-hide border-style">
                        <section id="boxBookingConfirm" class="box-booking-confirm relative">
                            <h3 class="booking-confirm-title">Confirm booking information ?</h3>
                            <div class="booking-confirm-note">We will send a text message to you via the number
                                below after we confirm the calendar for your booking.</div>
                            <script src='https://www.google.com/recaptcha/api.js?hl=en' async defer></script>
                            <!-- Google reCaptcha -->
                            <script type="text/javascript">
                                function ezyCaptcha_formBookingConfirm(token, is_submit) {
                                    is_submit = 1;
                                    if ($("#password").length) {
                                        //$("input:password").val(md5(clean_input($("#password").val())));
                                    }
                                    return true;
                                }

                            </script>
                            <form id="formBookingConfirm" class="form-booking-confirm" name="formBookingConfirm"
                                method="post" action="/book" enctype="multipart/form-data">
                                <div class="row">
                                    <div class="col-md-6 booking-name">
                                        <div class="group-select">
                                            <label>Your name (required)</label>
                                            <div class="relative w100">
                                                <input type="text" class="form-control" name="booking_name" value=""
                                                    data-validation="[NOTEMPTY]"
                                                    data-validation-message="Please enter your name" placeholder=""
                                                    maxlength="76" title="Booking name" />
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6 booking-phone">
                                        <div class="group-select">
                                            <label>Your phone (required)</label>
                                            <div class="relative w100">
                                                <input type="text" class="form-control inputPhone" pattern="\d*"
                                                    name="booking_phone" data-validation="[NOTEMPTY]"
                                                    data-validation-message="Please enter your phone" placeholder=""
                                                    maxlength="16" title="Booking phone" />
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-12 booking-email">
                                        <div style="display: none;" class="group-select">
                                            <label>Your email (optional)</label>
                                            <div class="relative w100">
                                                <input type="text" class="form-control" name="booking_email" value=""
                                                    placeholder="" maxlength="76" title="Booking email" />
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-12 booking-notelist">
                                        <div class="group-select">
                                            <label>Note (optional)</label>
                                            <div class="relative w100">
                                                <textarea class="form-control" rows="5" name="notelist"
                                                    placeholder="(Max length 200 character)" maxlength="201"
                                                    title="Booking note"></textarea>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-12 booking-store">
                                        <input type="hidden" name="choose_store" class="booking_store" value="0">
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-8 order-md-2 col-md-push-4">
                                        <button class="btn btn-confirm btn_confirm " type="button">Confirm</button>
                                    </div>
                                    <div class="col-md-4 order-md-1 col-md-pull-8">
                                        <button class="btn btn-cancel btn_cancel" type="button">Cancel</button>
                                    </div>
                                </div>
                            </form>
                        </section>
                    </section>
                </div>
            </div>
        </div>
    </div>
</main>
