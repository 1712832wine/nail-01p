@php
$features = json_decode($header['features']);
$extras = json_decode($header['extras']);
$name = explode(',', $header->name);
$desc = explode(',', $header->desc);
@endphp
<main class="main">
    <div class="pmain">
        <div class="page-heading"><img src="/themes/fnail04k/assets/images/heading-1.jpg" class="imgrps"
                alt="heading-1.jpg" /></div>
        <div class="in-container">
            <div class="container">
                <div class="in-content">
                    <!-- Tpl information contact -->
                    <div class="clearfix contact-info">
                        <div class="page-title">
                            <h2>CONTACT US</h2>
                        </div>
                        <div class="contact-info">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="fci-wrap">
                                        <div class="fci-row address-row" itemprop="address" itemscope=""
                                            itemtype="http://schema.org/PostalAddress"><span
                                                class="fci-title">Address:</span> <span
                                                class="fci-content address-wrap"> <span itemprop="streetAddress"
                                                    class="address">{{ $features[0]->desc }}
                                                </span> </span></div>
                                        <div class="fci-row phone-row"><span class="fci-title">Phone:</span> <span
                                                class="fci-content phone-wrap"> <a href="tel:832-968-6668"
                                                    title="Call Us"> <span itemprop="telephone"
                                                        class="phone">{{ $features[2]->desc }}</span> </a> </span>
                                        </div>
                                        <div class="fci-row email-row"><span class="fci-title">Email:</span> <span
                                                class="fci-content email-wrap"> <a href="mailto:web@fastboy.net"
                                                    title="Mail Us"> <span itemprop="email"
                                                        class="email">{{ $features[1]->desc }}</span> </a> <br /> <a
                                                    href="mailto:design@fastboy.net" title="Mail Us"> <span
                                                        itemprop="email" class="email">{{ $features[1]->desc }}</span>
                                                </a> </span></div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="foh-wrap">
                                        <!-- Normal Day -->
                                        @foreach ($extras as $extra)
                                            <div class="foh-row" itemprop="openingHours" content="{{ $extra->desc }}">
                                                <span class="foh-date">{{ $extra->name }}</span>
                                                <span class="foh-time">{{ $extra->desc }}</span>
                                            </div>
                                        @endforeach

                                    </div>
                                </div>
                            </div>
                            <p>We enjoy getting feedback from our clients and potential clients. If you’d like to
                                send us a message, please complete this short form and we’ll be in touch soon!</p>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-7">
                            <div class="google-maps" id="google-map"><iframe
                                    src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3464.7391505606774!2d-95.57299108550652!3d29.727314340440024!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x8640ddc6ed681049%3A0x74ace03ed50728e8!2sFast+Boy+Marketing!5e0!3m2!1sen!2s!4v1510909605272"
                                    width="100%" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
                            </div>
                        </div>
                        <div class="col-md-5">

                            <div class="clearfix">
                                <form enctype="multipart/form-data" method="post" name="send_contact" id="send_contact"
                                    action="/contact/send" class="form-horizontal">
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="group-select">
                                                <label>Your name (required)</label>
                                                <div class="relative w100">
                                                    <input title="Your name" type="text" name="contactname"
                                                        data-validation="[NOTEMPTY]"
                                                        data-validation-message="Please enter your name"
                                                        class="form-control" autocomplete="off" placeholder=""
                                                        maxlength="76">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="group-select">
                                                <label>Your email address (required)</label>
                                                <div class="relative w100">
                                                    <input title="Your email address" type="text" name="contactemail"
                                                        data-validation="[EMAIL]"
                                                        data-validation-message="Please enter your email"
                                                        class="form-control" autocomplete="off" placeholder=""
                                                        maxlength="76">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="group-select">
                                        <label>Your subject (required)</label>
                                        <div class="relative w100">
                                            <input title="Your subject" type="text" name="contactsubject"
                                                data-validation="[NOTEMPTY]"
                                                data-validation-message="Please enter your subject" class="form-control"
                                                autocomplete="off" placeholder="" maxlength="251">
                                        </div>
                                    </div>
                                    <div class="group-select">
                                        <label>Your message (required)</label>
                                        <div class="relative w100">
                                            <textarea title="Your message" name="contactcontent"
                                                data-validation="[NOTEMPTY]"
                                                data-validation-message="Please enter your message" class="form-control"
                                                autocomplete="off" rows="10" placeholder="" maxlength="501"></textarea>
                                        </div>
                                    </div>
                                    <div class="form-group text-center">
                                        <button type="submit" class="btn btn-main btn-submit btn_contact ">
                                            Send Us </button>
                                    </div>
                                </form> <br>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</main>
