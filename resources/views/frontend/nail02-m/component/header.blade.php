@php
$features = json_decode($header['features']);
$extras = json_decode($header['extras']);
@endphp
<header>
    <div class="top-bar">
        <div class="container">
            <div class="row">
                <div class="col-sm-6 col-md-7 col-lg-6 hidden-xs hidden-ms">
                    <div class="widget-info">
                        <ul class="list-inline">
                            <li><i class="fa fa-envelope"></i>
                                @foreach (explode(',', $features[2]->desc) as $item)
                                    <a href="mailto:{{ $item }}" itemprop="email">{{ $item }}</a>
                                @endforeach
                            </li>
                            <li><i class="fa fa-phone"></i>
                                @foreach (explode(',', $features[1]->desc) as $item)
                                    <a href="tel:{{ $item }}" itemprop="telephone">{{ $item }}</a>
                                @endforeach
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="col-sm-6 col-md-5 col-lg-6 col-xs-12 text-right">
                    <div class="widget-social"></div>
                </div>
            </div>
        </div>
    </div> <!-- TPL Header Menu Layouts -->
    <div class="wrap-freeze-header-mobile clearfix hidden-md hidden-lg menu-1024-hidden fixed-freeze mobile" style="">
        <div class="instead-flag-freeze-header mobile" style="height: 0px;"></div>
        <div class="flag-freeze-header-mobile initializedFreezeHeader">
            <div class="clearfix">
                <div class="menu_mobile_v1">
                    <div class="mobile_menu_container_v1">
                        <div class="mobile_logo">
                            @if (count(json_decode($header['images'])) > 0)
                                <a class="logo" href="/">
                                    <img src="{{ asset('storage') }}/photos/{{ json_decode($header['images'])[0] }}"
                                        alt="{{ $header['title'] }}" itemprop="logo">
                                </a>
                            @endif
                        </div>
                        <div class="mobile_menu_container_v1">
                            <div class="mobile-menu clearfix">
                                <nav id="mobile_dropdown" style="display: block;">
                                    <ul>
                                        <li><a itemprop="url" href="/">Home</a></li>
                                        <li><a itemprop="url" href="/about">About Us</a></li>
                                        <li><a itemprop="url" href="/services">Services</a></li>
                                        <li><a itemprop="url" href="/book">Booking</a></li>
                                        <li><a itemprop="url" href="/coupons">Coupons</a></li>
                                        <li><a itemprop="url" href="/giftcards">Gift Cards</a></li>
                                        <li><a itemprop="url" href="/gallery">Gallery</a></li>
                                        <li><a itemprop="url" href="/contact">Contact Us</a></li>
                                    </ul>
                                </nav>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div> <!-- TPL Header Menu Layouts -->
    <div class="header clearfix hidden-sm hidden-xs">
        <div class="wrap-freeze-header clearfix hidden-xs hidden-sm fixed-freeze desktop">
            <div class="instead-flag-freeze-header desktop" style="height: 0px;"></div>
            <div class="flag-freeze-header initializedFreezeHeader">
                <div class="container">
                    <div class="logo">
                        @if (count(json_decode($header['images'])) > 0)
                            <a class="logo" href="/">
                                <img src="{{ asset('storage') }}/photos/{{ json_decode($header['images'])[0] }}"
                                    alt="{{ $header['title'] }}" itemprop="logo">
                            </a>
                        @endif
                    </div>
                    <nav class="navbar main-nav hidden-xs hidden-sm">
                        <div id="desktop-nav" class="navbar-collapse collapse">
                            <ul class="nav navbar-nav nav-main">
                                <li><a itemprop="url" href="/">Home</a></li>
                                <li><a itemprop="url" href="/about">About Us</a></li>
                                <li><a itemprop="url" href="/services">Services</a></li>
                                <li><a itemprop="url" href="/book">Booking</a></li>
                                <li><a itemprop="url" href="/coupons">Coupons</a></li>
                                <li><a itemprop="url" href="/giftcards">Gift Cards</a></li>
                                <li><a itemprop="url" href="/gallery">Gallery</a></li>
                                <li><a itemprop="url" href="/contact">Contact Us</a></li>
                            </ul>
                        </div>
                    </nav>
                </div>
            </div>
        </div>
    </div>
</header>
