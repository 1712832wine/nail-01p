@php
$features = json_decode($header['features']);
$extras = json_decode($header['extras']);
@endphp
<footer class="footer">
    <div class="footer-widget">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-sm-3">
                    <div class="logo-footer">
                        @if (count(json_decode($header['images'])) > 0)
                            <a class="logo" href="/">
                                <img src="{{ asset('storage') }}/photos/{{ json_decode($header['images'])[0] }}"
                                    alt="{{ $header['title'] }}" itemprop="logo">
                            </a>
                        @endif

                    </div>
                </div>
                <div class="col-xs-12 col-sm-3">
                    <h3>CONTACT US</h3>
                    <ul class="address_info">
                        <li>
                            <i class="fa fa-home fa-lg"></i><span itemprop="address">{{ $features[0]->desc }}</span>
                        </li>
                        <li>
                            <i class="fa fa-phone fa-lg"></i>
                            @foreach (explode(',', $features[1]->desc) as $item)
                                <a href="tel:{{ $item }}" itemprop="telephone">{{ $item }}</a>
                                <br>
                            @endforeach
                        </li>
                        <li>
                            <i class="fa fa-envelope fa-lg"></i>
                            @foreach (explode(',', $features[2]->desc) as $item)
                                <a href="mailto:{{ $item }}" itemprop="email">{{ $item }}</a>
                                <br>
                            @endforeach
                        </li>
                    </ul>
                </div>
                <div class="col-xs-12 col-sm-3">
                    <div class="footer-info">
                        <h3>BUSINESS HOURS</h3>
                        <div class="foh-wrap">
                            @foreach ($extras as $extra)
                                <div class="foh-row short" itemprop="openingHours" content="{{ $extra->desc }}">
                                    <span class="foh-date">{{ $extra->name }}</span>
                                    <span class="foh-time">{{ $extra->desc }}</span>
                                </div>
                            @endforeach
                        </div>
                    </div>
                </div>
                <div class="col-xs-12 col-sm-3">
                    <div class="footer-social">
                        <h3>FOLLOW US</h3>
                        <ul class="icon-footer">
                            @foreach ($social as $item)
                                <li>
                                    <a target="_blank" href="{{ $item['name'] }}">
                                        <img src="{{ asset('storage') }}/photos/{{ $item['url'] }}">
                                    </a>
                                </li>
                            @endforeach
                        </ul>
                    </div>
                    <div class="footer-newletter">
                        <div class="social-page">
                            <!-- facebook fanpage -->
                            <div class="box_fanpage_fb">
                                <div id="fanpage_fb_container"></div>
                            </div>

                            <!-- use for calculator width -->
                            <div class="single_footer_widget">
                                <div style="max-width: 300px">
                                    <div id="social_block_width" style="width:100% !important; height: 1px !important">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="footer-copyright">
        <div class="container">
            <div class="copyright">
                <div class="row">
                    <div class="col-md-12">
                        <p>© Copyright by Deluxe Nails & Spa. All Rights Reserved.</p>
                    </div>
                </div>
            </div>
        </div><!-- .container-->
    </div>
</footer>
<!-- Tpl freeze footer -->
<!-- Active freeze footer by delete style display: none -->
<div class="freeze-footer">
    <ul>
        <li><a href="tel:{{ explode(',', $features[1]->desc)[0] }}" class="btn btn-default btn_call_now btn-call"
                title="Call us">{{ explode(',', $features[1]->desc)[0] }}</a>
        </li>
        <li><a href="/book" class="btn btn-default btn_make_appointment" title="Booking">Booking</a></li>
    </ul>
</div>
