@php
$features = json_decode($header['features']);
$extras = json_decode($header['extras']);
@endphp
<main>
    <div class="pmain">
        <input type="hidden" name="group_id" value="{{ $service_id }}" />
        <div class="page-heading">
            <div class="container">
                <div class="row">
                    <div class="col-md-6">
                        <h2 class="bt-text-ellipsis title_page_service">Our Service</h2>
                    </div>
                    <div class="col-md-6 top-right">
                        <div class="btn_service_defale">
                            <div class="btn_service_book">
                                <a href="/book" class="btn btn-default btn_make_appointment"><img
                                        style="vertical-align: top;padding-right: 7px"
                                        src="{{ asset('frontend') }}/public/library/global/forma-11.png">Make an
                                    appointment</a>&nbsp;
                                <a href="tel:{{ explode(',', $features[1]->desc)[0] }}"
                                    class="btn btn-default btn_call_now"><img
                                        style="vertical-align: top;padding-right: 7px"
                                        src="{{ asset('frontend') }}/public/library/global/shape-1.png">Call now</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="main-content">
            <div class="container page-container">
                {!! $intro['content'] !!}
                <div class="container page-container">
                    @foreach ($services as $index => $service)
                        @if ($index % 2 === 0)
                            <div class="row service-row">
                                <div class="col-md-1 hidden-sm hidden-xs"></div>
                                <div class="col-sm-4 col-md-3 text-center">
                                    @if (count(json_decode($service['images'])) > 0)
                                        <div class="circle-service-image"
                                            style="background-image: url('{{ asset('storage') }}/photos/{{ json_decode($service['images'])[0] }}')">
                                        </div>
                                    @endif
                                </div>
                                <div class="col-sm-8 col-md-7">
                                    <h1 id="sci_{{ $service['id'] }}" class="service-name">{{ $service['name'] }}
                                    </h1>
                                    <p class="cat_title">{{ $service['description'] }}</p>
                                    @foreach (json_decode($service['features']) as $item)
                                        @switch($item->name)
                                            @case('desc')
                                                <p>{{ $item->desc }}</p>
                                            @break
                                            @case('center')
                                            @break
                                            @default
                                                <div class="box_service">
                                                    <div class="detail-price-item">
                                                        <span class="detail-price-name">{{ $item->name }}</span>
                                                        <span class="detail-price-dots"></span>
                                                        <span class="detail-price-number">
                                                            <span class="current">{{ $item->desc }}</span>
                                                        </span>
                                                    </div>
                                                    <div></div>
                                                </div>
                                        @endswitch
                                    @endforeach
                                </div>
                                <div class="col-md-1 hidden-sm hidden-xs"></div>
                            </div>
                        @else
                            <div class="row service-row">
                                <div class="col-md-1 hidden-sm hidden-xs"></div>
                                <div class="col-xs-12 text-center visible-xs">
                                    @if (count(json_decode($service['images'])) > 0)
                                        <div class="circle-service-image"
                                            style="background-image: url('{{ asset('storage') }}/photos/{{ json_decode($service['images'])[0] }}')">
                                        </div>
                                    @endif
                                </div>
                                <div class="col-sm-8 col-md-7">
                                    <h1 id="sci_{{ $service['id'] }}" class="service-name"> {{ $service['name'] }}
                                    </h1>
                                    <p class="cat_title"> {{ $service['description'] }}</p>
                                    @foreach (json_decode($service['features']) as $item)
                                        @switch($item->name)
                                            @case('desc')
                                                <p>{{ $item['desc'] }}</p>
                                            @break
                                            @case('center')
                                            @break
                                            @default
                                                <div class="box_service">
                                                    <div class="detail-price-item">
                                                        <span class="detail-price-name">{{ $item->name }}</span>
                                                        <span class="detail-price-dots"></span>
                                                        <span class="detail-price-number">
                                                            <span class="current">{{ $item->desc }}</span>
                                                        </span>
                                                    </div>
                                                    <div></div>
                                                </div>
                                        @endswitch
                                    @endforeach
                                </div>
                                <div class="col-sm-4 col-md-3 text-center hidden-xs">
                                    @if (count(json_decode($service['images'])) > 0)
                                        <div class="circle-service-image"
                                            style="background-image: url('{{ asset('storage') }}/photos/{{ json_decode($service['images'])[0] }}')">
                                        </div>
                                    @endif
                                </div>
                                <div class="col-md-1 hidden-sm hidden-xs"></div>
                            </div>
                        @endif
                    @endforeach
                </div>
            </div>
        </div>
    </div>
</main>
