<main>
    <div class="pmain">
        <div class="page-heading">
            <div class="container">
                <h2 class="bt-text-ellipsis">Coupon</h2>
            </div>
        </div>
        <section class="section">
            <div class="container">
                <div class="row">
                    @foreach ($list as $item)
                        <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                            <div class="coupon_img_v1">
                                <img itemprop="image" class="img-responsive img_size"
                                    src="{{ asset('storage') }}/photos/{{ $item['url'] }}" alt="coupon v1">
                            </div>
                        </div>
                    @endforeach
                </div>
            </div>
        </section>
    </div>
</main>
