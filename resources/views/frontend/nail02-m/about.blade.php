<main>
    <div class="pmain">
        <div class="page-heading">
            <div class="container">
                <h2 class="bt-text-ellipsis">About us</h2>
            </div>
        </div>
        <section class="section">
            <div class="container">
                @foreach ($articles as $item)
                    <div class="in-container">
                        <div class="section-title">
                            <h3>Welcome to</h3>
                            <h2>{{ $item['title'] }}</h2>
                            <div class="line-title"></div>
                        </div>
                        <div class="in-content">
                            <div class="row">
                                <div class="col-md-6">
                                    {!! $item['content'] !!}
                                </div>
                                <div class="col-md-6 video_about">
                                    @if (count(json_decode($item['image'])) > 0)
                                        <img src="{{ asset('storage') }}/photos/{{ json_decode($item['image'])[0] }}"
                                            alt="" />
                                    @endif
                                </div>
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>
        </section>
    </div>
</main>
