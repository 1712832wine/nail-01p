<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" dir="ltr" lang="en" xmlns:og="http://ogp.me/ns#"
    xmlns:fb="http://www.facebook.com/2008/fbml" itemscope itemtype="https://schema.org/NailSalon">

<head>
    <!-- Language -->
    <meta http-equiv="content-language" content="en" />
    <link rel="alternate" href="http://fnail02m.fastboywebhosts.com" hreflang="x-default">
    <link rel="alternate" href="http://fnail02m.fastboywebhosts.com" hreflang="en-US">

    <!-- Meta limit -->

    <!-- Meta Tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width,initial-scale=1.0" />
    <meta http-equiv="content-type" content="text/html; charset=UTF-8" />
    <meta name="description" content="" />
    <meta name="keywords" content="" />
    <meta name="author" content="" />

    <!-- OG -->
    <meta property="og:title" content="" />
    <meta property="og:description" content="" />
    <meta property="og:image" content="" />
    <meta property="og:url" content="http://fnail02m.fastboywebhosts.com/" />
    <meta property="og:type" content="website" />
    <meta property="og:site_name" content="Deluxe Nails & Spa" />

    <!-- Dublin Core -->
    <link rel="schema.DC" href="http://purl.org/dc/elements/1.1/">
    <meta name="DC.title" content="">
    <meta name="DC.identifier" content="http://fnail02m.fastboywebhosts.com/">
    <meta name="DC.description" content="">
    <meta name="DC.subject" content="">
    <meta name="DC.language" scheme="UTF-8" content="">
    <meta itemprop="priceRange" name="priceRange" content="">

    <!-- GEO meta -->
    <meta name="geo.region" content="">
    <meta name="geo.placename" content="">
    <meta name="geo.position" content="">
    <meta name="ICBM" content="">

    <!-- Page Title -->
    <title>Deluxe Nails & Spa</title>
    <base href="/themes/fnail02m/assets/">

    <!-- canonical -->
    <link rel="canonical" href="http://fnail02m.fastboywebhosts.com">



    <!-- Favicons -->
    <link rel="icon" href="http://fnail02m.fastboywebhosts.com/uploads/fnail0j0esfxg/attach/1562750469_fbm_fvc.png"
        type="image/x-icon">
    <link rel="shortcut icon"
        href="http://fnail02m.fastboywebhosts.com/uploads/fnail0j0esfxg/attach/1562750469_fbm_fvc.png"
        type="image/x-icon">

    <link href="https://fonts.googleapis.com/css?family=Poppins:300,400,500" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Arizonia" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Merriweather" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href='{{ asset('frontend') }}/public/library/jquery-ui/jquery-ui.min.css'>
    <link rel="stylesheet" type="text/css" href='{{ asset('frontend') }}/public/library/animate/animate.css'>
    <link rel="stylesheet" type="text/css" href='{{ asset('frontend') }}/public/library/validation/validation.css'>
    <link rel="stylesheet" type="text/css" href='{{ asset('frontend') }}/public/library/flex-label/flex-label.css'>
    <link rel="stylesheet" type="text/css"
        href='{{ asset('frontend') }}/public/library/bootstrap/v3.3.7/bootstrap.min.css'>
    <link rel="stylesheet" type="text/css"
        href='{{ asset('frontend') }}/public/library/bootstrap-datetimepicker/bootstrap-datetimepicker.min.css'>
    <link rel="stylesheet" type="text/css"
        href='{{ asset('frontend') }}/public/library/font-awesome/v4.7.0/css/font-awesome.min.css'>
    <link rel="stylesheet" type="text/css"
        href='{{ asset('frontend') }}/public/library/font-elegant/css/font-elegant.css'>
    <link rel="stylesheet" type="text/css"
        href='{{ asset('frontend') }}/public/library/slider-pro/slider-pro.min.css'>
    <link rel="stylesheet" type="text/css"
        href='{{ asset('frontend') }}/public/library/slider-slick/slider-slick.css'>
    <link rel="stylesheet" type="text/css"
        href='{{ asset('frontend') }}/public/library/owl-carousel/v2.2.1/owl.carousel.min.css'>
    <link rel="stylesheet" type="text/css"
        href='{{ asset('frontend') }}/public/library/magnific-popup/magnific-popup.css'>
    <link rel="stylesheet" type="text/css" href='{{ asset('frontend') }}/public/library/lightbox/lightbox.css'>
    <link rel="stylesheet" type="text/css" href='{{ asset('frontend') }}/public/library/pnotify/pnotify.css'>
    <link rel="stylesheet" type="text/css" href='{{ asset('frontend') }}/public/library/instagram/instafeed.min.css'>
    <link rel="stylesheet" type="text/css" href='{{ asset('frontend') }}/public/library/signature/signature.css'>

    <link rel="stylesheet" type="text/css" href='{{ asset('frontend') }}/themes/fnail02m/assets/css/meanmenu.css'>
    <link rel="stylesheet" type="text/css"
        href='{{ asset('frontend') }}/themes/fnail02m/assets/css/ie10-viewport-bug-workaround.css'>
    <link rel="stylesheet" type="text/css" href='{{ asset('frontend') }}/themes/fnail02m/assets/css/style.css'>
    <link rel="stylesheet" type="text/css"
        href='{{ asset('frontend') }}/themes/fnail02m/assets/css/dropdown-submenu.css'>
    <link rel="stylesheet" type="text/css" href='{{ asset('frontend') }}/themes/fnail02m/assets/css/app.css'>
    <link rel="stylesheet" type="text/css" href='{{ asset('frontend') }}/themes/fnail02m/assets/css/responsive.css'>
    <link rel="stylesheet" type="text/css" href='{{ asset('frontend') }}/public/library/global/css/news.css'>
    <link rel="stylesheet" type="text/css" href='{{ asset('frontend') }}/public/library/global/css/consent.css'>
    <link rel="stylesheet" type="text/css"
        href='{{ asset('frontend') }}/themes/fnail02m/assets/css/custom.css?v=1.3'>

    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    <script type="text/javascript">
        var dateFormatBooking = "MM/DD/YYYY";
        var posFormat = "1,0,2";
        var timeMorning = '["9:00","9:30","10:00","10:30","11:00","11:30"]';
        var timeAfternoon =
            '["12:00","12:30","13:00","13:30","14:00","14:30","15:00","15:30","16:00","16:30","17:00","17:30","18:00"]';
        var bookLogin = "open_booking";
        var beforeTime = "1";
        var beforeDay = 0;
        var currDateT = "2021/05/24";
        var checktimebooking = 1;
        var enableRecaptcha = 0;
        var hoursTimeFormat = 12;
        var phoneFormat = "(000) 000-0000";
        var company_phone = "832-968-6668";
        var enable_booking = "1";
        var num_paging = "12";
        var menu = "idx";

        let facebook_embed = {
            id_fanpage: "https://www.facebook.com/FastboyMarketingAgency",
            appId: "",
            width: 450,
            height: 300,
            tabs: "timeline",
            show_facepile: true,
            small_header: true,
            likebox_enable: true,
            likebox_show_faces: false,
            likebox_stream: true,
        };
        let google_id_fanpage = "";
        let twitter_id_fanpage = "";

    </script>


    <script type="text/javascript" src="{{ asset('frontend') }}/public/library/jquery/v1.10.2/jquery.min.js">
    </script>
    <script type="text/javascript" src="{{ asset('frontend') }}/public/library/jquery-ui/jquery-ui.min.js"></script>
    <script type="text/javascript" src="{{ asset('frontend') }}/public/library/jquery-ui/jquery-migrate.min.js">
    </script>
    <script type="text/javascript" src="{{ asset('frontend') }}/public/library/validation/validation.min.js">
    </script>
    <script type="text/javascript" src="{{ asset('frontend') }}/public/library/mask/mask.min.js"></script>
    <script type="text/javascript" src="{{ asset('frontend') }}/public/library/flex-label/flex-label.js"></script>
    <script type="text/javascript" src="{{ asset('frontend') }}/public/library/bootstrap/v3.3.7/bootstrap.min.js">
    </script>
    <script type="text/javascript"
        src="{{ asset('frontend') }}/public/library/waiting-modal-dialog/waiting-modal-dialog.js"></script>
    <script type="text/javascript" src="{{ asset('frontend') }}/public/library/moment/moment-with-locales.js">
    </script>
    <script type="text/javascript"
        src="{{ asset('frontend') }}/public/library/bootstrap-datetimepicker/bootstrap-datetimepicker.js"></script>
    <script type="text/javascript" src="{{ asset('frontend') }}/public/library/magnific-popup/magnific-popup.min.js">
    </script>
    <script type="text/javascript" src="{{ asset('frontend') }}/public/library/pnotify/pnotify.js"></script>
    <script type="text/javascript" src="{{ asset('frontend') }}/public/library/slider-pro/slider-pro.min.js">
    </script>
    <script type="text/javascript" src="{{ asset('frontend') }}/public/library/slider-slick/slider-slick.js">
    </script>
    <script type="text/javascript"
        src="{{ asset('frontend') }}/public/library/owl-carousel/v2.2.1/owl.carousel.min.js"></script>
    <script type="text/javascript" src="{{ asset('frontend') }}/public/library/wow/wow.min.js"></script>
    <script type="text/javascript" src="{{ asset('frontend') }}/public/library/meanmenu/meanmenu.js"></script>
    <script type="text/javascript" src="{{ asset('frontend') }}/public/library/signature/signature.min.js"></script>
    <script type="text/javascript" src="{{ asset('frontend') }}/public/library/instagram/instafeed.min.js"></script>
    <script type="text/javascript" src="{{ asset('frontend') }}/themes/fnail02m/assets/js/responsiveslides.min.js">
    </script>
    <script type="text/javascript" src="{{ asset('frontend') }}/themes/fnail02m/assets/js/jquery.sticky.js"></script>
    <script type="text/javascript" src="{{ asset('frontend') }}/public/library/global/js/js_php.js"></script>

    <style type="text/css">
        h2.sp-slide-title-text {
            display: none;
        }

        .navbar-nav>li>a {
            line-height: 130px;
        }

        .box_img_giftcard img {
            max-height: 100% !important;
        }

        .block_giftcard {
            margin-bottom: 40px;
        }

        .choose-info {
            line-height: 28px;
        }

        .gallery-box {
            margin: 0px;
        }

        .image-bg {
            margin-bottom: 10px;
        }

        .img-info-staff {
            display: none;
        }

        .title-staff {
            width: 100%;
        }

        .footer-widget h3 {
            font-size: 20px;
            color: #000;
            font-weight: 500;
        }

        @media only screen and (max-width: 687px) {
            .logo-footer {
                width: 50%;
                margin: auto;
            }
        }

        @media only screen and (max-width: 687px) {
            h1.service-name {
                font-size: 18px;
                font-weight: 600;
            }
        }

        h1.service-name {
            font-weight: 600;
        }

        .detail-price-number {
            font-weight: 600;
            color: #000;
        }

        .detail-price-name {
            font-weight: normal;
            color: #000;
        }

        .box_service p {
            text-align: justify;
            color: #000;
        }

        @media (max-width: 991px) {
            .section-welcome .text-block-custom {
                padding: 0;
            }
        }

        .p-list-gallery .image-bg {
            background-position: bottom center;
            padding-bottom: 100%;
        }

    </style>
    <!-- Go to www.addthis.com/dashboard to customize your tools -->
    <script type="text/javascript"
        src="{{ asset('frontend') }}//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-59b8fec5dd14e0f2"></script>

    <script type="text/javascript">
        var addthis_share = {
            url: "http://fnail02m.fastboywebhosts.com/",
            title: "Deluxe Nails & Spa",
            description: "",
            media: ""

        }

    </script>

    <!-- Webmaster tools -->
    <!--Webmaster tools-->
    <!-- fb retargeting -->

    <!-- Schema Script -->
    <meta name="csrf-token" content="{{ csrf_token() }}" />
</head>

<body>
    <div class="art-main">
        <h1 style="display: none"></h1>

        <!-- Tpl freeze header -->
        <input type="hidden" name="activeFreezeHeader" value="1" />
        @php
            use App\Models\Product;
            use App\Models\Category;
            use App\Models\Album;
            use App\Models\Asset;
            $header = Product::where('category_id', Category::where('name', 'Header And Footer')->first()['id'])->first();
            $album = Album::where([['type', 'Social'], ['status', 'PUBLISHED']])->first();
            $social = Asset::where('albums_id', $album['id'])->orderBy('priority', 'desc')->get();
        @endphp
        @include('frontend.nail02-m.component.header',
        ['header'=>$header])

        {{ $slot }}

        @include('frontend.nail02-m.component.footer',['header'=>$header])
        <script type="text/javascript">
            var enable_booking = "1";
            if (enable_booking == 0) {
                $(".btn_make_appointment").remove();
            }

        </script>


        <script type="text/javascript" src="{{ asset('frontend') }}/public/library/global/js/consent.js"></script>
        <script type="text/javascript" src="{{ asset('frontend') }}/themes/fnail02m/assets/js/app.js?v=1.2"></script>
        <script type="text/javascript" src="{{ asset('frontend') }}/themes/fnail02m/assets/js/script.js"></script>
        <script type="text/javascript" src="{{ asset('frontend') }}/themes/fnail02m/assets/js/main.js"></script>

        <!-- Google analytics -->
        <script>
            (function(i, s, o, g, r, a, m) {
                i['GoogleAnalyticsObject'] = r;
                i[r] = i[r] || function() {
                    (i[r].q = i[r].q || []).push(arguments)
                }, i[r].l = 1 * new Date();
                a = s.createElement(o),
                    m = s.getElementsByTagName(o)[0];
                a.async = 1;
                a.src = g;
                m.parentNode.insertBefore(a, m)
            })(window, document, 'script', 'https://www.google-analytics.com/analytics.js', 'ga');

            ga('create', '', 'auto');
            ga('send', 'pageview');

        </script>
        <!-- gg adwords remarketing -->

        <script type="text/javascript"></script>
        <script>
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

        </script>
        @livewireScripts
    </div>
</body>

</html>
