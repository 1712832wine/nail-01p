<main>
    <div class="pmain">

        <div class="page-heading">
            <div class="container">
                <h2>Gift Cards</h2>
            </div>
        </div>
        <div class="in-container">
            <div class="container">
                <div class="in-content">
                    <h4 class="title_giftcard" itemprop="name">A Very Special Gift For You </h4>
                    <p class="des_giftcard" itemprop="description">Let your sweetheart know how much you love
                        and care for him/her by sending our love cards! Buy our gift card for your loved one.
                    </p>
                    <div style="font-size: 20px" class="alert alert-warning text-center">
                        Our system is being upgraded.<br>
                        Your order has not been processed this time.<br>
                        We apologize for this inconvenience.<br>
                        Please contact: <a href="tel:832-968-6668">832-968-6668</a>
                    </div>
                    <div class="row">
                        <div class="col-md-8">
                            <div class="block_giftcard">
                                <div class="title_gift"><i class="fa fa-location-arrow" aria-hidden="true"></i>
                                    Please choose a design and add input e-gift information:</div>
                                <div class="block_line">
                                    <div class="col-xs-6 col-md-3 pd-fix">
                                        <div class="box_img_giftcard" pid="192" name="Happy Mother&#39;s Day"
                                            price="30">
                                            <img itemprop="image" class="img-responsive"
                                                src="http://fnail02m.fastboywebhosts.com/uploads/fnail0j0esfxg/product/1563783507_img_product1563783507.jpg"
                                                alt="Happy Mother&#39;s Day">
                                            <div class="circle_check"></div>
                                        </div>
                                    </div>
                                    <div class="col-xs-6 col-md-3 pd-fix">
                                        <div class="box_img_giftcard" pid="191" name="Happy Father&#39;s Day"
                                            price="30">
                                            <img itemprop="image" class="img-responsive"
                                                src="http://fnail02m.fastboywebhosts.com/uploads/fnail0j0esfxg/product/1563783493_img_product1563783493.jpg"
                                                alt="Happy Father&#39;s Day">
                                            <div class="circle_check"></div>
                                        </div>
                                    </div>
                                    <div class="col-xs-6 col-md-3 pd-fix">
                                        <div class="box_img_giftcard" pid="190" name="Merry Christmas" price="30">
                                            <img itemprop="image" class="img-responsive"
                                                src="http://fnail02m.fastboywebhosts.com/uploads/fnail0j0esfxg/product/1563783485_img_product1563783485.jpg"
                                                alt="Merry Christmas">
                                            <div class="circle_check"></div>
                                        </div>
                                    </div>
                                    <div class="col-xs-6 col-md-3 pd-fix">
                                        <div class="box_img_giftcard" pid="189" name="Happy Birthday" price="30">
                                            <img itemprop="image" class="img-responsive"
                                                src="http://fnail02m.fastboywebhosts.com/uploads/fnail0j0esfxg/product/1563783473_img_product1563783473.jpg"
                                                alt="Happy Birthday">
                                            <div class="circle_check"></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <script type="text/javascript">
                        $(document).ready(function() {
                            var giftcard_buy_custom = 1;
                            $(".box_img_giftcard").click(function() {
                                $(".box_img_giftcard").removeClass("active");
                                $(this).addClass("active");

                                var src_img = $(this).find("img").attr("src");
                                $(".preview_img img").attr("src", src_img);
                                $(".preview_img img").css("display", "block");
                                // Remove price


                                var pid = $(this).attr("pid");
                                var name = $(this).attr("name");
                                var price = $(this).attr("price");

                                // Frame image
                                $(".frame_img img").attr("src", src_img);
                                $(".name_product p").html(name);


                                if (giftcard_buy_custom == 1) {

                                    $("[name='custom_price']").val(price);
                                    $("[name='custom_price']").parents(".box_amount").find(
                                        ".form-tooltip-error").remove();
                                    var max = $("[name='custom_price']").attr("max");
                                    var placeholder = " Pay as you go (From $" + price +
                                        " to $" + max + ")";
                                    $("[name='custom_price']").attr("min", price);
                                    $("[name='custom_price']").attr("placeholder", placeholder);
                                    $("[name='custom_price']").parent().find(".fl-label").html(
                                        placeholder + "<span class='fl-required'>*</span>");

                                } else {
                                    $("[name='custom_price']").val(price);

                                    $('.price-item-produc').text('$' + price);
                                    $('.camount').text('$' + price);
                                }


                                setTimeout(function() {
                                    $("[name='custom_price']").attr("placeholder", "");
                                }, 100);



                                // Run ajax
                                $.ajax({
                                    type: "post",
                                    url: "/cart/change_product",
                                    data: {
                                        pid: pid
                                    },
                                    dataType: 'Json',
                                    beforeSend: function() {},
                                    success: function(obj) {
                                        // console.log(response);
                                        $("#cart_subtotal").html(obj.subtotal);
                                        $("#cart_discount_code_value").html(obj
                                            .discount);
                                        $("#cart_product_discount_value").html(obj
                                            .discount);
                                        $("#cart_tax").html(obj.tax);
                                        $("#cart_payment_total").html(obj.amount);
                                        $("[name='custom_price']").attr("pid", pid);

                                    }
                                });
                            });
                            var pid = "192";
                            if (pid) {
                                $(".box_img_giftcard[pid='" + pid + "']").trigger("click");
                            } else {
                                $(".box_img_giftcard:first").trigger("click");
                            }

                            // F5 auto change input
                            setTimeout(function() {
                                $("input[name='custom_price']").trigger("change");
                            }, 1000);
                            // Change content
                        });

                        function change_content(ele, area) {
                            var content = $(ele).val();
                            $(area).html(content);
                        }

                        function check_enter_number(evt, onthis) {
                            if (isNaN(onthis.value + "" + String.fromCharCode(evt.charCode))) {
                                return false;
                            }
                        }

                        function update_price_custom(onthis) {
                            var cus_price = isNaN(parseFloat($(onthis).val())) ? 0 : parseFloat($(onthis)
                                .val());
                            var id = $(onthis).attr("pid");
                            var max_val = parseFloat($(onthis).attr("max"));
                            var min_val = parseFloat($(onthis).attr("min"));
                            // Change money
                            if (cus_price > 0) {
                                $(".camount").html("$ " + cus_price);
                            } else {
                                $(".camount").html("");
                            }
                            // Update price
                            if (cus_price >= min_val && cus_price <= max_val) {
                                $(onthis).css("border-color", "#ccc");
                                $(onthis).css("color", "#858585");
                                $("[name='custom_price']").parents(".box_amount").find(".form-tooltip-error")
                                    .remove();
                                // $(".btn_payment").prop("disabled", false);
                                //Ajax
                                $.ajax({
                                    type: "post",
                                    url: "/cart/updateprice",
                                    data: {
                                        cus_price: cus_price,
                                        id: id
                                    },
                                    success: function(html) {
                                        // console.log(html);
                                        var obj = JSON.parse(html);
                                        if (obj.status == "error") {
                                            call_notify('Notification', obj.msg, "error");
                                            $(onthis).val(obj.price);
                                            return false;
                                        }

                                        if (obj.cart_data) {
                                            $("#cart_tax").text(obj.cart_data[1]);
                                            $("#cart_discount_code_value").text(obj.cart_data[5]);
                                            $("#cart_product_discount_value").text(obj.cart_data[
                                                5]);
                                            $("#cart_subtotal").text(obj.cart_data[2]);
                                            $("#cart_payment_total").text(obj.cart_data[3]);
                                            $("[name='custom_price']").attr("pid", id);
                                        }
                                    }
                                });
                            } else {
                                // $(onthis).css("border-color", "red");
                                // $(".btn_payment").prop("disabled", true);
                            }
                        }

                    </script>
                    <style type="text/css">
                        .box_img_giftcard {
                            max-height: 101px;
                            margin: 5px 0;
                            border: 1px solid #ccc;
                            padding: 3px;
                            min-height: 57px;
                            cursor: pointer;
                            position: relative;
                            background: #fff;
                            border-radius: 3px;
                            -webkit-transition: width 2s;
                            transition: width 2s
                        }

                        .box_img_giftcard img {
                            max-height: 93px;
                            display: block;
                            margin: 0 auto
                        }

                        .preview_img {
                            border: 1px solid #ccc;
                            padding: 5px
                        }

                        .preview_img img {
                            display: block;
                            margin: 0 auto
                        }

                        .pd-fix {
                            padding: 0 5px
                        }

                        .circle_check {
                            position: absolute;
                            background: #fff;
                            width: 20px;
                            height: 20px;
                            top: 3px;
                            right: 3px;
                            border: 1px solid #ccc;
                            border-radius: 20px
                        }

                        .box_preview {
                            border: 1px dashed #ccc;
                            padding: 10px;
                            background: #f6f5f5
                        }

                        ul.information {
                            display: block;
                            clear: both;
                            overflow: hidden
                        }

                        ul.information li span {
                            text-align: left;
                            display: block;
                            background: #f5f5f5;
                            padding: 5px
                        }

                        span.center {
                            text-align: center !important;
                            font-weight: bold;
                            font-size: 20px
                        }

                        span.left {
                            float: left;
                            font-weight: bold;
                            width: 80px
                        }

                        span.right {
                            display: inline-block !important;
                            float: left;
                            font-style: italic
                        }

                        ul.information li {
                            display: block;
                            clear: both
                        }

                        .fl-flex-label,
                        .list_form li,
                        .cart .form_cart p,
                        .cart .order .total ul {
                            text-align: left
                        }

                        .title_gift {
                            clear: both;
                            text-align: left;
                            font-weight: bold;
                            font-style: italic;
                            font-size: 16px;
                        }

                        .p-payment h3 {
                            margin-bottom: 40px
                        }

                        .form-group {
                            margin-bottom: 0
                        }

                        .block_line {
                            clear: both;
                            display: flex;
                        }

                        ul.total-payment-info li {
                            margin: 5px;
                            line-height: 25px;
                        }

                        .box_cart {
                            clear: both;
                            margin: 20px 0
                        }

                        .modal-content {
                            clear: both;
                            overflow: hidden;
                        }

                        .modal-dialog.modal-m {
                            margin: 15% auto;
                        }

                        .btn_payment {
                            margin: 10px 0;
                        }

                        .form-tooltip-error {
                            margin-bottom: 4px;
                            max-width: 250px;
                            font-size: 12px;
                        }

                        .fl-flex-label.fl-collapsed .fl-label {
                            font-size: 10px;
                            padding-top: 0;
                            line-height: 18px;
                        }

                        .fl-flex-label .form-control:not(.form-control-lg) {
                            min-height: 36px;
                        }

                        .fl-flex-label .fl-label {
                            padding-top: 6px;
                        }

                        .cart .order .total ul {
                            padding: 0 20px;
                        }

                        .cart .order .total ul li span.order_price {
                            font-size: 18px;
                            font-weight: 500;
                        }

                        .cart .order .total ul li span.order_price_total {
                            font-weight: 600;
                            color: red;
                            font-size: 24px;
                        }

                        .promotion_price {
                            position: absolute;
                            left: 0px;
                            top: 10px;
                            padding: 3px 7px;
                            background: #ef0c0ce3;
                            color: #fff;
                            font-size: 15px;
                        }

                        span.right.camount {
                            color: red;
                            font-size: 16px;
                            font-weight: bold;
                        }

                        .box_img_giftcard.active .circle_check {
                            background-color: #009017;
                        }

                        .box_img_giftcard.active {
                            border: 1px solid #009017;
                        }

                    </style>

                    <script type="text/javascript">
                        var waitingDialog = waitingDialog || (function($) {
                            'use strict';

                            // Creating modal dialog's DOM
                            var $dialog = $(
                                '<div class="modal fade" data-backdrop="static" data-keyboard="false" tabindex="-1" role="dialog" aria-hidden="true" style="overflow-y:visible;">' +
                                '<div class="modal-dialog modal-m">' +
                                '<div class="modal-content">' +
                                '<div class="modal-header"><h3 style="margin:0;"></h3></div>' +
                                '<div class="modal-body">' +
                                '<div class="progress progress-striped active" style="margin-bottom:0;"><div class="progress-bar" style="width: 100%"></div></div>' +
                                '</div>' +
                                '</div></div></div>');

                            return {
                                /**
                                 * Opens our dialog
                                 * @param message Custom message
                                 * @param options Custom options:
                                 *                options.dialogSize - bootstrap postfix for dialog size, e.g. "sm", "m";
                                 *                options.progressType - bootstrap postfix for progress bar type, e.g. "success", "warning".
                                 */
                                show: function(message, options) {
                                    // Assigning defaults
                                    if (typeof options === 'undefined') {
                                        options = {};
                                    }
                                    if (typeof message === 'undefined') {
                                        message = 'Loading';
                                    }
                                    var settings = $.extend({
                                        dialogSize: 'm',
                                        progressType: '',
                                        onHide: null // This callback runs after the dialog was hidden
                                    }, options);

                                    // Configuring dialog
                                    $dialog.find('.modal-dialog').attr('class', 'modal-dialog')
                                        .addClass('modal-' + settings.dialogSize);
                                    $dialog.find('.progress-bar').attr('class', 'progress-bar');
                                    if (settings.progressType) {
                                        $dialog.find('.progress-bar').addClass('progress-bar-' +
                                            settings.progressType);
                                    }
                                    $dialog.find('h3').text(message);
                                    // Adding callbacks
                                    if (typeof settings.onHide === 'function') {
                                        $dialog.off('hidden.bs.modal').on('hidden.bs.modal',
                                            function(e) {
                                                settings.onHide.call($dialog);
                                            });
                                    }
                                    // Opening dialog
                                    $dialog.modal();
                                },
                                /**
                                 * Closes dialog
                                 */
                                hide: function() {
                                    $dialog.modal('hide');
                                }
                            };

                        })(jQuery);

                    </script>

                    <!-- INFORMATION -->
                    <script type="application/javascript">
                        $(document).ready(function() {
                            $('.fl-flex-label').flexLabel();
                            $('form#payment').validate({
                                submit: {
                                    settings: {
                                        clear: 'keypress',
                                        display: "inline",
                                        button: "[type='submit']",
                                        inputContainer: 'form-group',
                                        errorListClass: 'form-tooltip-error',
                                        clear: "focusin",
                                    },
                                    callback: {
                                        onSubmit: function(node, formdata, obj) {
                                            var cus_price = $("[name='custom_price']")
                                                .val() * 1;
                                            var min = $("[name='custom_price']").attr(
                                                "min") * 1;
                                            var max = $("[name='custom_price']").attr(
                                                "max") * 1;
                                            if (cus_price < min || cus_price > max) {
                                                var notify = " Pay as you go (From $" +
                                                    min + " to $" + max + ")";
                                                var ele_parent = $("[name='custom_price']")
                                                    .parents(".box_amount");
                                                // console.log(ele_parent);
                                                if (!ele_parent.find(".form-tooltip-error")
                                                    .length) {
                                                    var el = ele_parent.append(
                                                        "<div class='form-tooltip-error'><ul><li>" +
                                                        notify + "</li></ul></div>");
                                                    $("[name='custom_price']").css(
                                                        "border-color", "red");
                                                    $("[name='custom_price']").css("color",
                                                        "red");
                                                }
                                                return false;
                                            } else {
                                                waitingDialog.show(
                                                    "Please wait a moment ...");
                                                $(".btn_loading").prop("disabled",
                                                    "disabled");
                                                node[0].submit();
                                            }
                                        },
                                        onAfterSubmit: function(node) {


                                        }
                                    }
                                }
                            });

                            $("input[name='send_to_friend']").click(function() {
                                var check_val = $(this).val();
                                if (check_val == 0) {
                                    $(".box_recipient").show();
                                    $(this).val(1);
                                } else {
                                    $(".box_recipient").hide();
                                    $(this).val(0);
                                }
                            });

                            var check_send = parseInt($("input[name='send_to_friend']").val());
                            if (check_send == 1) {
                                $("input[name='send_to_friend'][value='1']").prop("checked", true);
                                $(".box_recipient").show();
                            }
                        });

                    </script>
                    <!--giftcards_form giftcards_form_2 giftcards_data-->
                </div>
            </div>
        </div>
    </div>
</main>
