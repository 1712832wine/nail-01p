<main>
    <div class="pmain">
        <div class="page-heading">
            <div class="container">
                <h2 class="bt-text-ellipsis">Booking</h2>
            </div>
        </div>
        <section class="section">
            <div class="section-wrap padding-tb-sm">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12">
                            <script src='https://www.google.com/recaptcha/api.js?hl=en' async defer></script>
                            <!-- Google reCaptcha -->
                            <script type="text/javascript">
                                <!--
                                function ezyCaptcha_surveyForm(token, is_submit) {
                                    is_submit = 1;
                                    if ($("#password").length) {
                                        //$("input:password").val(md5(clean_input($("#password").val())));
                                    }
                                    return true;
                                }
                                //

                                -->
                            </script>
                            <div class="content-shop bg-ctn-special">
                                <div class="content-shop-booking">
                                    <form enctype="multipart/form-data" id="surveyForm" method="post" action="/book/add"
                                        class="form-horizontal">
                                        <div class="item-booking">
                                            <div id="optionTemplate">
                                                <div class="group-select">
                                                    <label>Service(required)</label>
                                                    <div
                                                        style="display: inline-block; position: relative; width: 100%;">
                                                        <select name="product_id[]" class="list_service"
                                                            data-validation-message="Please choose service">
                                                            <option value="" price="" staff="[]">Select Service
                                                            </option>
                                                            <!--List Categories-->
                                                            <optgroup label="ARTIFICIAL NAILS">
                                                                <!--List service-->
                                                                <option value="113" price="$30" up=""
                                                                    staff='[{"id":"2","name":"admin","note":null,"image":"\/public\/library\/global\/no-photo.jpg","imageThumb":"\/public\/library\/global\/no-photo.jpg","imageClass":"no-photo"}]'>
                                                                    Acrylic Full-Set ($30) </option>
                                                                <option value="114" price="$20" up=""
                                                                    staff='[{"id":"2","name":"admin","note":null,"image":"\/public\/library\/global\/no-photo.jpg","imageThumb":"\/public\/library\/global\/no-photo.jpg","imageClass":"no-photo"}]'>
                                                                    Acrylic Fill ($20) </option>
                                                                <option value="115" price="$45" up=""
                                                                    staff='[{"id":"2","name":"admin","note":null,"image":"\/public\/library\/global\/no-photo.jpg","imageThumb":"\/public\/library\/global\/no-photo.jpg","imageClass":"no-photo"}]'>
                                                                    Gel Full-Set ($45) </option>
                                                                <option value="116" price="$35" up=""
                                                                    staff='[{"id":"2","name":"admin","note":null,"image":"\/public\/library\/global\/no-photo.jpg","imageThumb":"\/public\/library\/global\/no-photo.jpg","imageClass":"no-photo"}]'>
                                                                    Gell Fill ($35) </option>
                                                                <option value="117" price="$45" up=""
                                                                    staff='[{"id":"2","name":"admin","note":null,"image":"\/public\/library\/global\/no-photo.jpg","imageThumb":"\/public\/library\/global\/no-photo.jpg","imageClass":"no-photo"}]'>
                                                                    Gell Back-Fill ($45) </option>
                                                                <option value="118" price="$35" up=""
                                                                    staff='[{"id":"2","name":"admin","note":null,"image":"\/public\/library\/global\/no-photo.jpg","imageThumb":"\/public\/library\/global\/no-photo.jpg","imageClass":"no-photo"}]'>
                                                                    Solar Regular Full-Set ($35) </option>
                                                                <option value="119" price="$50" up=""
                                                                    staff='[{"id":"2","name":"admin","note":null,"image":"\/public\/library\/global\/no-photo.jpg","imageThumb":"\/public\/library\/global\/no-photo.jpg","imageClass":"no-photo"}]'>
                                                                    Solar Full-Set Color ($50) </option>
                                                                <option value="120" price="$25" up=""
                                                                    staff='[{"id":"2","name":"admin","note":null,"image":"\/public\/library\/global\/no-photo.jpg","imageThumb":"\/public\/library\/global\/no-photo.jpg","imageClass":"no-photo"}]'>
                                                                    Solar Fill ($25) </option>
                                                                <option value="121" price="$45" up=""
                                                                    staff='[{"id":"2","name":"admin","note":null,"image":"\/public\/library\/global\/no-photo.jpg","imageThumb":"\/public\/library\/global\/no-photo.jpg","imageClass":"no-photo"}]'>
                                                                    Solar Full-Set P &amp; W ($45) </option>
                                                                <option value="122" price="$50" up=""
                                                                    staff='[{"id":"2","name":"admin","note":null,"image":"\/public\/library\/global\/no-photo.jpg","imageThumb":"\/public\/library\/global\/no-photo.jpg","imageClass":"no-photo"}]'>
                                                                    Solar Full-Set Color Tip P &amp; W ($50)
                                                                </option>
                                                                <option value="123" price="$40" up=""
                                                                    staff='[{"id":"2","name":"admin","note":null,"image":"\/public\/library\/global\/no-photo.jpg","imageThumb":"\/public\/library\/global\/no-photo.jpg","imageClass":"no-photo"}]'>
                                                                    Solar Back-Fill P &amp; W ($40) </option>
                                                                <option value="124" price="$45" up=""
                                                                    staff='[{"id":"2","name":"admin","note":null,"image":"\/public\/library\/global\/no-photo.jpg","imageThumb":"\/public\/library\/global\/no-photo.jpg","imageClass":"no-photo"}]'>
                                                                    Solar Back-Fill Color Tip P &amp; W ($45)
                                                                </option>
                                                                <option value="125" price="$5" up=""
                                                                    staff='[{"id":"2","name":"admin","note":null,"image":"\/public\/library\/global\/no-photo.jpg","imageThumb":"\/public\/library\/global\/no-photo.jpg","imageClass":"no-photo"}]'>
                                                                    Gel Coating ($5) </option>
                                                                <option value="126" price="$10" up=""
                                                                    staff='[{"id":"2","name":"admin","note":null,"image":"\/public\/library\/global\/no-photo.jpg","imageThumb":"\/public\/library\/global\/no-photo.jpg","imageClass":"no-photo"}]'>
                                                                    Acrylic Nails Take Off ($10) </option>
                                                                <option value="127" price="$5" up=""
                                                                    staff='[{"id":"2","name":"admin","note":null,"image":"\/public\/library\/global\/no-photo.jpg","imageThumb":"\/public\/library\/global\/no-photo.jpg","imageClass":"no-photo"}]'>
                                                                    Nails Repair ($5) </option>
                                                                <option value="128" price="$15" up=""
                                                                    staff='[{"id":"2","name":"admin","note":null,"image":"\/public\/library\/global\/no-photo.jpg","imageThumb":"\/public\/library\/global\/no-photo.jpg","imageClass":"no-photo"}]'>
                                                                    Gelish or Shellac ($15) </option>
                                                                <option value="129" price="" up=""
                                                                    staff='[{"id":"2","name":"admin","note":null,"image":"\/public\/library\/global\/no-photo.jpg","imageThumb":"\/public\/library\/global\/no-photo.jpg","imageClass":"no-photo"}]'>
                                                                    *Any service with French will cost &#036;5
                                                                    extra</option>
                                                                <option value="130" price="" up=""
                                                                    staff='[{"id":"2","name":"admin","note":null,"image":"\/public\/library\/global\/no-photo.jpg","imageThumb":"\/public\/library\/global\/no-photo.jpg","imageClass":"no-photo"}]'>
                                                                    *Long Nail will be charge extra</option>
                                                            </optgroup>
                                                            <optgroup label="NATURAL NAILS">
                                                                <!--List service-->
                                                                <option value="131" price="$15" up=""
                                                                    staff='[{"id":"2","name":"admin","note":null,"image":"\/public\/library\/global\/no-photo.jpg","imageThumb":"\/public\/library\/global\/no-photo.jpg","imageClass":"no-photo"},{"id":"3","name":"Admin CP","note":null,"image":"\/public\/library\/global\/no-photo.jpg","imageThumb":"\/public\/library\/global\/no-photo.jpg","imageClass":"no-photo"}]'>
                                                                    Express Manicure ($15) </option>
                                                                <option value="132" price="$20" up=""
                                                                    staff='[{"id":"2","name":"admin","note":null,"image":"\/public\/library\/global\/no-photo.jpg","imageThumb":"\/public\/library\/global\/no-photo.jpg","imageClass":"no-photo"},{"id":"3","name":"Admin CP","note":null,"image":"\/public\/library\/global\/no-photo.jpg","imageThumb":"\/public\/library\/global\/no-photo.jpg","imageClass":"no-photo"}]'>
                                                                    Manicure ($20) </option>
                                                                <option value="133" price="$25" up=""
                                                                    staff='[{"id":"2","name":"admin","note":null,"image":"\/public\/library\/global\/no-photo.jpg","imageThumb":"\/public\/library\/global\/no-photo.jpg","imageClass":"no-photo"},{"id":"3","name":"Admin CP","note":null,"image":"\/public\/library\/global\/no-photo.jpg","imageThumb":"\/public\/library\/global\/no-photo.jpg","imageClass":"no-photo"}]'>
                                                                    Deluxe Manicure ($25) </option>
                                                                <option value="134" price="$25" up=""
                                                                    staff='[{"id":"2","name":"admin","note":null,"image":"\/public\/library\/global\/no-photo.jpg","imageThumb":"\/public\/library\/global\/no-photo.jpg","imageClass":"no-photo"},{"id":"3","name":"Admin CP","note":null,"image":"\/public\/library\/global\/no-photo.jpg","imageThumb":"\/public\/library\/global\/no-photo.jpg","imageClass":"no-photo"}]'>
                                                                    Spa Pedicure ($25) </option>
                                                                <option value="135" price="$35" up=""
                                                                    staff='[{"id":"2","name":"admin","note":null,"image":"\/public\/library\/global\/no-photo.jpg","imageThumb":"\/public\/library\/global\/no-photo.jpg","imageClass":"no-photo"},{"id":"3","name":"Admin CP","note":null,"image":"\/public\/library\/global\/no-photo.jpg","imageThumb":"\/public\/library\/global\/no-photo.jpg","imageClass":"no-photo"}]'>
                                                                    Deluxe Pedicure (40 min) ($35) </option>
                                                                <option value="136" price="$45" up=""
                                                                    staff='[{"id":"2","name":"admin","note":null,"image":"\/public\/library\/global\/no-photo.jpg","imageThumb":"\/public\/library\/global\/no-photo.jpg","imageClass":"no-photo"},{"id":"3","name":"Admin CP","note":null,"image":"\/public\/library\/global\/no-photo.jpg","imageThumb":"\/public\/library\/global\/no-photo.jpg","imageClass":"no-photo"}]'>
                                                                    Signature Pedicure (1 hour) ($45) </option>
                                                                <option value="137" price="$45" up=""
                                                                    staff='[{"id":"2","name":"admin","note":null,"image":"\/public\/library\/global\/no-photo.jpg","imageThumb":"\/public\/library\/global\/no-photo.jpg","imageClass":"no-photo"},{"id":"3","name":"Admin CP","note":null,"image":"\/public\/library\/global\/no-photo.jpg","imageThumb":"\/public\/library\/global\/no-photo.jpg","imageClass":"no-photo"}]'>
                                                                    Mani &amp; Spa Pedi ($45) </option>
                                                                <option value="138" price="$8" up=""
                                                                    staff='[{"id":"2","name":"admin","note":null,"image":"\/public\/library\/global\/no-photo.jpg","imageThumb":"\/public\/library\/global\/no-photo.jpg","imageClass":"no-photo"},{"id":"3","name":"Admin CP","note":null,"image":"\/public\/library\/global\/no-photo.jpg","imageThumb":"\/public\/library\/global\/no-photo.jpg","imageClass":"no-photo"}]'>
                                                                    Nails Polish Change ($8) </option>
                                                                <option value="139" price="$10" up=""
                                                                    staff='[{"id":"2","name":"admin","note":null,"image":"\/public\/library\/global\/no-photo.jpg","imageThumb":"\/public\/library\/global\/no-photo.jpg","imageClass":"no-photo"},{"id":"3","name":"Admin CP","note":null,"image":"\/public\/library\/global\/no-photo.jpg","imageThumb":"\/public\/library\/global\/no-photo.jpg","imageClass":"no-photo"}]'>
                                                                    Toes Polish Change ($10) </option>
                                                                <option value="140" price="$10" up=""
                                                                    staff='[{"id":"2","name":"admin","note":null,"image":"\/public\/library\/global\/no-photo.jpg","imageThumb":"\/public\/library\/global\/no-photo.jpg","imageClass":"no-photo"},{"id":"3","name":"Admin CP","note":null,"image":"\/public\/library\/global\/no-photo.jpg","imageThumb":"\/public\/library\/global\/no-photo.jpg","imageClass":"no-photo"}]'>
                                                                    Nail Cut &amp; File ($10) </option>
                                                                <option value="141" price="$5" up=""
                                                                    staff='[{"id":"2","name":"admin","note":null,"image":"\/public\/library\/global\/no-photo.jpg","imageThumb":"\/public\/library\/global\/no-photo.jpg","imageClass":"no-photo"},{"id":"3","name":"Admin CP","note":null,"image":"\/public\/library\/global\/no-photo.jpg","imageThumb":"\/public\/library\/global\/no-photo.jpg","imageClass":"no-photo"}]'>
                                                                    Nails Arts (2 designs) ($5) </option>
                                                                <option value="142" price="$25" up="" staff='[]'>Take
                                                                    Off &amp; Manicure ($25)
                                                                </option>
                                                                <option value="143" price="$30" up=""
                                                                    staff='[{"id":"2","name":"admin","note":null,"image":"\/public\/library\/global\/no-photo.jpg","imageThumb":"\/public\/library\/global\/no-photo.jpg","imageClass":"no-photo"},{"id":"3","name":"Admin CP","note":null,"image":"\/public\/library\/global\/no-photo.jpg","imageThumb":"\/public\/library\/global\/no-photo.jpg","imageClass":"no-photo"}]'>
                                                                    Shellac Color ($30) </option>
                                                                <option value="144" price="$20" up=""
                                                                    staff='[{"id":"2","name":"admin","note":null,"image":"\/public\/library\/global\/no-photo.jpg","imageThumb":"\/public\/library\/global\/no-photo.jpg","imageClass":"no-photo"},{"id":"3","name":"Admin CP","note":null,"image":"\/public\/library\/global\/no-photo.jpg","imageThumb":"\/public\/library\/global\/no-photo.jpg","imageClass":"no-photo"}]'>
                                                                    Shellac Polish Change ($20) </option>
                                                                <option value="145" price="$10" up=""
                                                                    staff='[{"id":"2","name":"admin","note":null,"image":"\/public\/library\/global\/no-photo.jpg","imageThumb":"\/public\/library\/global\/no-photo.jpg","imageClass":"no-photo"},{"id":"3","name":"Admin CP","note":null,"image":"\/public\/library\/global\/no-photo.jpg","imageThumb":"\/public\/library\/global\/no-photo.jpg","imageClass":"no-photo"}]'>
                                                                    Take Off Shellac ($10) </option>
                                                                <option value="146" price="$8" up=""
                                                                    staff='[{"id":"2","name":"admin","note":null,"image":"\/public\/library\/global\/no-photo.jpg","imageThumb":"\/public\/library\/global\/no-photo.jpg","imageClass":"no-photo"},{"id":"3","name":"Admin CP","note":null,"image":"\/public\/library\/global\/no-photo.jpg","imageThumb":"\/public\/library\/global\/no-photo.jpg","imageClass":"no-photo"}]'>
                                                                    Color W/French Tip ($8) </option>
                                                                <option value="147" price="$5" up=""
                                                                    staff='[{"id":"2","name":"admin","note":null,"image":"\/public\/library\/global\/no-photo.jpg","imageThumb":"\/public\/library\/global\/no-photo.jpg","imageClass":"no-photo"},{"id":"3","name":"Admin CP","note":null,"image":"\/public\/library\/global\/no-photo.jpg","imageThumb":"\/public\/library\/global\/no-photo.jpg","imageClass":"no-photo"}]'>
                                                                    Any service with paraffin wax will cost ($5)
                                                                </option>
                                                            </optgroup>
                                                            <optgroup label="KIDS MENU">
                                                                <!--List service-->
                                                                <option value="148" price="" up=""
                                                                    staff='[{"id":"2","name":"admin","note":null,"image":"\/public\/library\/global\/no-photo.jpg","imageThumb":"\/public\/library\/global\/no-photo.jpg","imageClass":"no-photo"}]'>
                                                                    10 years old &amp; under</option>
                                                                <option value="149" price="$12" up=""
                                                                    staff='[{"id":"2","name":"admin","note":null,"image":"\/public\/library\/global\/no-photo.jpg","imageThumb":"\/public\/library\/global\/no-photo.jpg","imageClass":"no-photo"}]'>
                                                                    Kid Mani ($12) </option>
                                                                <option value="150" price="$28" up=""
                                                                    staff='[{"id":"2","name":"admin","note":null,"image":"\/public\/library\/global\/no-photo.jpg","imageThumb":"\/public\/library\/global\/no-photo.jpg","imageClass":"no-photo"}]'>
                                                                    Mani &amp; Pedi ($28) </option>
                                                                <option value="151" price="$18" up=""
                                                                    staff='[{"id":"2","name":"admin","note":null,"image":"\/public\/library\/global\/no-photo.jpg","imageThumb":"\/public\/library\/global\/no-photo.jpg","imageClass":"no-photo"}]'>
                                                                    Pedi ($18) </option>
                                                                <option value="152" price="$7" up=""
                                                                    staff='[{"id":"2","name":"admin","note":null,"image":"\/public\/library\/global\/no-photo.jpg","imageThumb":"\/public\/library\/global\/no-photo.jpg","imageClass":"no-photo"}]'>
                                                                    Polish Change....fingers ($7) </option>
                                                                <option value="153" price="$8" up=""
                                                                    staff='[{"id":"2","name":"admin","note":null,"image":"\/public\/library\/global\/no-photo.jpg","imageThumb":"\/public\/library\/global\/no-photo.jpg","imageClass":"no-photo"}]'>
                                                                    Polish Change....toes ($8) </option>
                                                            </optgroup>
                                                            <optgroup label="HAIR">
                                                                <!--List service-->
                                                                <option value="154" price="$30" up=""
                                                                    staff='[{"id":"2","name":"admin","note":null,"image":"\/public\/library\/global\/no-photo.jpg","imageThumb":"\/public\/library\/global\/no-photo.jpg","imageClass":"no-photo"}]'>
                                                                    Women Cut ($30) </option>
                                                                <option value="155" price="$20" up=""
                                                                    staff='[{"id":"2","name":"admin","note":null,"image":"\/public\/library\/global\/no-photo.jpg","imageThumb":"\/public\/library\/global\/no-photo.jpg","imageClass":"no-photo"}]'>
                                                                    Men Cut ($20) </option>
                                                                <option value="156" price="$18" up=""
                                                                    staff='[{"id":"2","name":"admin","note":null,"image":"\/public\/library\/global\/no-photo.jpg","imageThumb":"\/public\/library\/global\/no-photo.jpg","imageClass":"no-photo"}]'>
                                                                    Girl/Boy under 10 Cut ($18) </option>
                                                                <option value="157" price="$5" up=""
                                                                    staff='[{"id":"2","name":"admin","note":null,"image":"\/public\/library\/global\/no-photo.jpg","imageThumb":"\/public\/library\/global\/no-photo.jpg","imageClass":"no-photo"}]'>
                                                                    Bang Trim ($5) </option>
                                                                <option value="158" price="$65" up=""
                                                                    staff='[{"id":"2","name":"admin","note":null,"image":"\/public\/library\/global\/no-photo.jpg","imageThumb":"\/public\/library\/global\/no-photo.jpg","imageClass":"no-photo"}]'>
                                                                    Women All-Over Color ($65) </option>
                                                                <option value="159" price="$55" up=""
                                                                    staff='[{"id":"2","name":"admin","note":null,"image":"\/public\/library\/global\/no-photo.jpg","imageThumb":"\/public\/library\/global\/no-photo.jpg","imageClass":"no-photo"}]'>
                                                                    Women Color Touch-Up ($55) </option>
                                                                <option value="160" price="$75" up=""
                                                                    staff='[{"id":"2","name":"admin","note":null,"image":"\/public\/library\/global\/no-photo.jpg","imageThumb":"\/public\/library\/global\/no-photo.jpg","imageClass":"no-photo"}]'>
                                                                    Women Highlights ($75) </option>
                                                                <option value="161" price="$55" up=""
                                                                    staff='[{"id":"2","name":"admin","note":null,"image":"\/public\/library\/global\/no-photo.jpg","imageThumb":"\/public\/library\/global\/no-photo.jpg","imageClass":"no-photo"}]'>
                                                                    Women Partial Highlights ($55) </option>
                                                                <option value="162" price="$55" up=""
                                                                    staff='[{"id":"2","name":"admin","note":null,"image":"\/public\/library\/global\/no-photo.jpg","imageThumb":"\/public\/library\/global\/no-photo.jpg","imageClass":"no-photo"}]'>
                                                                    Women Partial Lowlights ($55) </option>
                                                                <option value="163" price="$80" up="" staff='[]'>Perm
                                                                    ($80) </option>
                                                                <option value="164" price="$50" up=""
                                                                    staff='[{"id":"2","name":"admin","note":null,"image":"\/public\/library\/global\/no-photo.jpg","imageThumb":"\/public\/library\/global\/no-photo.jpg","imageClass":"no-photo"}]'>
                                                                    Men Highlights ($50) </option>
                                                                <option value="165" price="$50" up=""
                                                                    staff='[{"id":"2","name":"admin","note":null,"image":"\/public\/library\/global\/no-photo.jpg","imageThumb":"\/public\/library\/global\/no-photo.jpg","imageClass":"no-photo"}]'>
                                                                    Men Color ($50) </option>
                                                                <option value="166" price="$5" up=""
                                                                    staff='[{"id":"2","name":"admin","note":null,"image":"\/public\/library\/global\/no-photo.jpg","imageThumb":"\/public\/library\/global\/no-photo.jpg","imageClass":"no-photo"}]'>
                                                                    Shampoo ($5) </option>
                                                                <option value="167" price="$35" up=""
                                                                    staff='[{"id":"2","name":"admin","note":null,"image":"\/public\/library\/global\/no-photo.jpg","imageThumb":"\/public\/library\/global\/no-photo.jpg","imageClass":"no-photo"}]'>
                                                                    Shampoo &amp; Style ($35) </option>
                                                                <option value="168" price="$50" up=""
                                                                    staff='[{"id":"2","name":"admin","note":null,"image":"\/public\/library\/global\/no-photo.jpg","imageThumb":"\/public\/library\/global\/no-photo.jpg","imageClass":"no-photo"}]'>
                                                                    Up-do ($50) </option>
                                                            </optgroup>
                                                            <optgroup label="WAXING">
                                                                <!--List service-->
                                                                <option value="169" price="$12" up=""
                                                                    staff='[{"id":"2","name":"admin","note":null,"image":"\/public\/library\/global\/no-photo.jpg","imageThumb":"\/public\/library\/global\/no-photo.jpg","imageClass":"no-photo"},{"id":"3","name":"Admin CP","note":null,"image":"\/public\/library\/global\/no-photo.jpg","imageThumb":"\/public\/library\/global\/no-photo.jpg","imageClass":"no-photo"}]'>
                                                                    Eyebrow ($12) </option>
                                                                <option value="170" price="$10" up=""
                                                                    staff='[{"id":"2","name":"admin","note":null,"image":"\/public\/library\/global\/no-photo.jpg","imageThumb":"\/public\/library\/global\/no-photo.jpg","imageClass":"no-photo"},{"id":"3","name":"Admin CP","note":null,"image":"\/public\/library\/global\/no-photo.jpg","imageThumb":"\/public\/library\/global\/no-photo.jpg","imageClass":"no-photo"}]'>
                                                                    Upper Lip ($10) </option>
                                                                <option value="171" price="$20" up=""
                                                                    staff='[{"id":"2","name":"admin","note":null,"image":"\/public\/library\/global\/no-photo.jpg","imageThumb":"\/public\/library\/global\/no-photo.jpg","imageClass":"no-photo"},{"id":"3","name":"Admin CP","note":null,"image":"\/public\/library\/global\/no-photo.jpg","imageThumb":"\/public\/library\/global\/no-photo.jpg","imageClass":"no-photo"}]'>
                                                                    Eyebrow &amp; Upper Lip ($20) </option>
                                                                <option value="172" price="$28" up=""
                                                                    staff='[{"id":"2","name":"admin","note":null,"image":"\/public\/library\/global\/no-photo.jpg","imageThumb":"\/public\/library\/global\/no-photo.jpg","imageClass":"no-photo"},{"id":"3","name":"Admin CP","note":null,"image":"\/public\/library\/global\/no-photo.jpg","imageThumb":"\/public\/library\/global\/no-photo.jpg","imageClass":"no-photo"}]'>
                                                                    Eyebrow, Upper Lip, Chin ($28) </option>
                                                                <option value="173" price="$20" up=""
                                                                    staff='[{"id":"2","name":"admin","note":null,"image":"\/public\/library\/global\/no-photo.jpg","imageThumb":"\/public\/library\/global\/no-photo.jpg","imageClass":"no-photo"},{"id":"3","name":"Admin CP","note":null,"image":"\/public\/library\/global\/no-photo.jpg","imageThumb":"\/public\/library\/global\/no-photo.jpg","imageClass":"no-photo"}]'>
                                                                    Under Arms ($20) </option>
                                                                <option value="174" price="$40" up=""
                                                                    staff='[{"id":"2","name":"admin","note":null,"image":"\/public\/library\/global\/no-photo.jpg","imageThumb":"\/public\/library\/global\/no-photo.jpg","imageClass":"no-photo"},{"id":"3","name":"Admin CP","note":null,"image":"\/public\/library\/global\/no-photo.jpg","imageThumb":"\/public\/library\/global\/no-photo.jpg","imageClass":"no-photo"}]'>
                                                                    Face Wax ($40) </option>
                                                                <option value="175" price="$12" up=""
                                                                    staff='[{"id":"2","name":"admin","note":null,"image":"\/public\/library\/global\/no-photo.jpg","imageThumb":"\/public\/library\/global\/no-photo.jpg","imageClass":"no-photo"},{"id":"3","name":"Admin CP","note":null,"image":"\/public\/library\/global\/no-photo.jpg","imageThumb":"\/public\/library\/global\/no-photo.jpg","imageClass":"no-photo"}]'>
                                                                    Chin ($12) </option>
                                                                <option value="176" price="$45" up=""
                                                                    staff='[{"id":"2","name":"admin","note":null,"image":"\/public\/library\/global\/no-photo.jpg","imageThumb":"\/public\/library\/global\/no-photo.jpg","imageClass":"no-photo"},{"id":"3","name":"Admin CP","note":null,"image":"\/public\/library\/global\/no-photo.jpg","imageThumb":"\/public\/library\/global\/no-photo.jpg","imageClass":"no-photo"}]'>
                                                                    Chest Or Back ($45) </option>
                                                                <option value="177" price="$40" up=""
                                                                    staff='[{"id":"2","name":"admin","note":null,"image":"\/public\/library\/global\/no-photo.jpg","imageThumb":"\/public\/library\/global\/no-photo.jpg","imageClass":"no-photo"},{"id":"3","name":"Admin CP","note":null,"image":"\/public\/library\/global\/no-photo.jpg","imageThumb":"\/public\/library\/global\/no-photo.jpg","imageClass":"no-photo"}]'>
                                                                    Half Legs ($40) </option>
                                                                <option value="178" price="$65" up=""
                                                                    staff='[{"id":"2","name":"admin","note":null,"image":"\/public\/library\/global\/no-photo.jpg","imageThumb":"\/public\/library\/global\/no-photo.jpg","imageClass":"no-photo"},{"id":"3","name":"Admin CP","note":null,"image":"\/public\/library\/global\/no-photo.jpg","imageThumb":"\/public\/library\/global\/no-photo.jpg","imageClass":"no-photo"}]'>
                                                                    Full Legs ($65) </option>
                                                                <option value="179" price="$40" up=""
                                                                    staff='[{"id":"2","name":"admin","note":null,"image":"\/public\/library\/global\/no-photo.jpg","imageThumb":"\/public\/library\/global\/no-photo.jpg","imageClass":"no-photo"},{"id":"3","name":"Admin CP","note":null,"image":"\/public\/library\/global\/no-photo.jpg","imageThumb":"\/public\/library\/global\/no-photo.jpg","imageClass":"no-photo"}]'>
                                                                    Bikini ($40) </option>
                                                                <option value="180" price="$55" up=""
                                                                    staff='[{"id":"2","name":"admin","note":null,"image":"\/public\/library\/global\/no-photo.jpg","imageThumb":"\/public\/library\/global\/no-photo.jpg","imageClass":"no-photo"},{"id":"3","name":"Admin CP","note":null,"image":"\/public\/library\/global\/no-photo.jpg","imageThumb":"\/public\/library\/global\/no-photo.jpg","imageClass":"no-photo"}]'>
                                                                    Brazillian ($55) </option>
                                                                <option value="181" price="$45" up=""
                                                                    staff='[{"id":"2","name":"admin","note":null,"image":"\/public\/library\/global\/no-photo.jpg","imageThumb":"\/public\/library\/global\/no-photo.jpg","imageClass":"no-photo"},{"id":"3","name":"Admin CP","note":null,"image":"\/public\/library\/global\/no-photo.jpg","imageThumb":"\/public\/library\/global\/no-photo.jpg","imageClass":"no-photo"}]'>
                                                                    Arms ($45) </option>
                                                                <option value="182" price="$35" up=""
                                                                    staff='[{"id":"2","name":"admin","note":null,"image":"\/public\/library\/global\/no-photo.jpg","imageThumb":"\/public\/library\/global\/no-photo.jpg","imageClass":"no-photo"},{"id":"3","name":"Admin CP","note":null,"image":"\/public\/library\/global\/no-photo.jpg","imageThumb":"\/public\/library\/global\/no-photo.jpg","imageClass":"no-photo"}]'>
                                                                    Half Arms ($35) </option>
                                                                <option value="183" price="$150" up=""
                                                                    staff='[{"id":"2","name":"admin","note":null,"image":"\/public\/library\/global\/no-photo.jpg","imageThumb":"\/public\/library\/global\/no-photo.jpg","imageClass":"no-photo"},{"id":"3","name":"Admin CP","note":null,"image":"\/public\/library\/global\/no-photo.jpg","imageThumb":"\/public\/library\/global\/no-photo.jpg","imageClass":"no-photo"}]'>
                                                                    Eye Lashes Extension ($150) </option>
                                                                <option value="184" price="$65" up=""
                                                                    staff='[{"id":"2","name":"admin","note":null,"image":"\/public\/library\/global\/no-photo.jpg","imageThumb":"\/public\/library\/global\/no-photo.jpg","imageClass":"no-photo"},{"id":"3","name":"Admin CP","note":null,"image":"\/public\/library\/global\/no-photo.jpg","imageThumb":"\/public\/library\/global\/no-photo.jpg","imageClass":"no-photo"}]'>
                                                                    Eye Lashes Extension Refill ($65) </option>
                                                            </optgroup>
                                                            <optgroup label="FACIALS">
                                                                <!--List service-->
                                                                <option value="185" price="$45" up=""
                                                                    staff='[{"id":"2","name":"admin","note":null,"image":"\/public\/library\/global\/no-photo.jpg","imageThumb":"\/public\/library\/global\/no-photo.jpg","imageClass":"no-photo"},{"id":"3","name":"Admin CP","note":null,"image":"\/public\/library\/global\/no-photo.jpg","imageThumb":"\/public\/library\/global\/no-photo.jpg","imageClass":"no-photo"}]'>
                                                                    Express Facial ($45) </option>
                                                                <option value="186" price="$60" up=""
                                                                    staff='[{"id":"2","name":"admin","note":null,"image":"\/public\/library\/global\/no-photo.jpg","imageThumb":"\/public\/library\/global\/no-photo.jpg","imageClass":"no-photo"},{"id":"3","name":"Admin CP","note":null,"image":"\/public\/library\/global\/no-photo.jpg","imageThumb":"\/public\/library\/global\/no-photo.jpg","imageClass":"no-photo"}]'>
                                                                    Basic Facial ($60) </option>
                                                                <option value="187" price="$60" up=""
                                                                    staff='[{"id":"2","name":"admin","note":null,"image":"\/public\/library\/global\/no-photo.jpg","imageThumb":"\/public\/library\/global\/no-photo.jpg","imageClass":"no-photo"},{"id":"3","name":"Admin CP","note":null,"image":"\/public\/library\/global\/no-photo.jpg","imageThumb":"\/public\/library\/global\/no-photo.jpg","imageClass":"no-photo"}]'>
                                                                    Reflexology ($60) </option>
                                                                <option value="188" price="$55" up=""
                                                                    staff='[{"id":"2","name":"admin","note":null,"image":"\/public\/library\/global\/no-photo.jpg","imageThumb":"\/public\/library\/global\/no-photo.jpg","imageClass":"no-photo"},{"id":"3","name":"Admin CP","note":null,"image":"\/public\/library\/global\/no-photo.jpg","imageThumb":"\/public\/library\/global\/no-photo.jpg","imageClass":"no-photo"}]'>
                                                                    Make Up ($55) </option>
                                                            </optgroup>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="group-select">
                                                    <label>Technician (optional)</label>
                                                    <select name="staff_id[]" class="list_staff">
                                                        <option value="">Select technician</option>

                                                    </select>
                                                </div>
                                            </div>
                                            <div class="group-select box-date">
                                                <label>Date</label>
                                                <input type="text" name="booking_date" id="datetimepicker_v1"
                                                    class="form-control choose_date booking_date"
                                                    placeholder="Choice a date" value="05/24/2021" typehtml="html">
                                                <span class="fa fa-calendar"></span>
                                            </div>
                                            <button class='btn btn-search btn_action title booking_search'
                                                type='button'>Search</button> <input type="hidden" name="booking_hours"
                                                value="" />
                                            <input type="hidden" name="booking_area_code" value="" />
                                            <input type="hidden" name="booking_phone" value="" />
                                            <input type="hidden" name="booking_name" value="" />
                                            <input type="hidden" name="nocaptcha" value="1" />
                                            <input type="hidden" name="g-recaptcha-response" value="" />
                                            <input type="hidden" name="notelist" value="" />
                                        </div>

                                        <div class="add-services addButton">
                                            <img itemprop="image" src="/public/library/global/add-service-icon-new.png">
                                            Add
                                            Another service
                                        </div>
                                    </form>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="infor-booking box_detail_info">
                                                <div class="staff" id="box_person">


                                                </div>
                                                <div class="time-booking col-md-12 databooktime" style="display: none">
                                                    <h3 class="time_show"></h3>
                                                    <div class="am clearfix">
                                                        <div class="clearfix"><label>Morning <span class="note_am_time"
                                                                    style="color: red; font-style: italic;"></span></label>
                                                        </div>
                                                        <ul class="timemorning clearfix">
                                                            <!--List hours Morning-->
                                                            <li itemprop="name">
                                                                <a itemprop="url" href="#open_booking" valhours="9:00"
                                                                    class="open_booking">9:00
                                                                    AM</a>
                                                            </li>
                                                            <li itemprop="name">
                                                                <a itemprop="url" href="#open_booking" valhours="9:30"
                                                                    class="open_booking">9:30
                                                                    AM</a>
                                                            </li>
                                                            <li itemprop="name">
                                                                <a itemprop="url" href="#open_booking" valhours="10:00"
                                                                    class="open_booking">10:00
                                                                    AM</a>
                                                            </li>
                                                            <li itemprop="name">
                                                                <a itemprop="url" href="#open_booking" valhours="10:30"
                                                                    class="open_booking">10:30
                                                                    AM</a>
                                                            </li>
                                                            <li itemprop="name">
                                                                <a itemprop="url" href="#open_booking" valhours="11:00"
                                                                    class="open_booking">11:00
                                                                    AM</a>
                                                            </li>
                                                            <li itemprop="name">
                                                                <a itemprop="url" href="#open_booking" valhours="11:30"
                                                                    class="open_booking">11:30
                                                                    AM</a>
                                                            </li>
                                                            <!--End list hours Morning-->
                                                        </ul>
                                                    </div>
                                                    <div class="pm clearfix">
                                                        <div class="clearfix"><label clss="clearfix">Afternoon
                                                                <span class="note_pm_time"
                                                                    style="color: red; font-style: italic;"></span></label>
                                                        </div>
                                                        <ul class="timeafternoon clearfix">
                                                            <!--List hours Afternoon-->
                                                            <li itemprop="name">
                                                                <a itemprop="url" href="#open_booking" valhours="12:00"
                                                                    class="open_booking">12:00
                                                                    PM</a>
                                                            </li>
                                                            <li itemprop="name">
                                                                <a itemprop="url" href="#open_booking" valhours="12:30"
                                                                    class="open_booking">12:30
                                                                    PM</a>
                                                            </li>
                                                            <li itemprop="name">
                                                                <a itemprop="url" href="#open_booking" valhours="13:00"
                                                                    class="open_booking">13:00
                                                                    PM</a>
                                                            </li>
                                                            <li itemprop="name">
                                                                <a itemprop="url" href="#open_booking" valhours="13:30"
                                                                    class="open_booking">13:30
                                                                    PM</a>
                                                            </li>
                                                            <li itemprop="name">
                                                                <a itemprop="url" href="#open_booking" valhours="14:00"
                                                                    class="open_booking">14:00
                                                                    PM</a>
                                                            </li>
                                                            <li itemprop="name">
                                                                <a itemprop="url" href="#open_booking" valhours="14:30"
                                                                    class="open_booking">14:30
                                                                    PM</a>
                                                            </li>
                                                            <li itemprop="name">
                                                                <a itemprop="url" href="#open_booking" valhours="15:00"
                                                                    class="open_booking">15:00
                                                                    PM</a>
                                                            </li>
                                                            <li itemprop="name">
                                                                <a itemprop="url" href="#open_booking" valhours="15:30"
                                                                    class="open_booking">15:30
                                                                    PM</a>
                                                            </li>
                                                            <li itemprop="name">
                                                                <a itemprop="url" href="#open_booking" valhours="16:00"
                                                                    class="open_booking">16:00
                                                                    PM</a>
                                                            </li>
                                                            <li itemprop="name">
                                                                <a itemprop="url" href="#open_booking" valhours="16:30"
                                                                    class="open_booking">16:30
                                                                    PM</a>
                                                            </li>
                                                            <li itemprop="name">
                                                                <a itemprop="url" href="#open_booking" valhours="17:00"
                                                                    class="open_booking">17:00
                                                                    PM</a>
                                                            </li>
                                                            <li itemprop="name">
                                                                <a itemprop="url" href="#open_booking" valhours="17:30"
                                                                    class="open_booking">17:30
                                                                    PM</a>
                                                            </li>
                                                            <li itemprop="name">
                                                                <a itemprop="url" href="#open_booking" valhours="18:00"
                                                                    class="open_booking">18:00
                                                                    PM</a>
                                                            </li>
                                                            <!--End list hours Afternoon-->
                                                        </ul>
                                                    </div>
                                                </div>


                                            </div>
                                        </div>
                                    </div>
                                    <script type="text/javascript">
                                        $(document).ready(function() {
                                            var formbk =
                                                '{"listperson":[],"notelist":[],"booking_date":"05\/24\/2021","booking_hours":""}';
                                            loadEvent();
                                        });

                                    </script>
                                </div>
                            </div>


                            <!-- module 4 -->
                            <section style="display: none;">
                                <!--POPUP LOGIN-->
                                <div id="popup_login" class="white-popup mfp-hide">
                                    <div class="box_account_v1">
                                        <div class="modal_form_header">
                                            <h4>Login</h4>
                                        </div>
                                        <div class="popup_main_area">
                                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 border_right">
                                                <form enctype="multipart/form-data" id="form-login" method="POST"
                                                    action="/login/login_do">
                                                    <fieldset class="form-group">
                                                        <div class="box_login">
                                                            <h2> Been here before?</h2>
                                                            <div class="form_input_1">
                                                                <div class="form-control-wrapper">
                                                                    <input type="email" name="cus_email"
                                                                        placeholder="Enter your E-mail"
                                                                        data-validation="[EMAIL]"
                                                                        data-validation-message="Email is not valid!">
                                                                </div>
                                                            </div>
                                                            <div class="form_input_2">
                                                                <div class="form-control-wrapper">
                                                                    <input type="password" placeholder="Password"
                                                                        name="cus_password" data-validation="[NOTEMPTY]"
                                                                        data-validation-message="Password must not be empty!">
                                                                </div>
                                                            </div>
                                                            <div class="col_psw_v1" style="padding-bottom: 3px; ">

                                                                <div class="form-control-wrapper"
                                                                    style="margin-top: -10px;padding-bottom: 3px;float:right">
                                                                    <a href="/login/forgot-password/">Forgot
                                                                        password</a>

                                                                </div>
                                                            </div>
                                                            <div class="btn_submit_login">
                                                                <button class="submit" type="submit">Login</button>
                                                            </div>
                                                        </div>
                                                    </fieldset>
                                                </form>
                                            </div>
                                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                                <div class="box_register">
                                                    <h2>New Account?</h2>
                                                    <div class="btn_submit_login">
                                                        <button class="submit" type="submit"
                                                            onclick="window.location.href = '/register/'">Create
                                                            a
                                                            Username
                                                        </button>
                                                    </div>
                                                    <div class="btn_login_social">

                                                        <a href="" class="btn btn_facebook_v1" href="#"
                                                            title="facebook login">
                                                            <i class="fa fa-facebook"></i>
                                                            <span>Facebook</span>
                                                        </a>


                                                        <a class="btn btn_gplus_v1" href="" title="gplus login">
                                                            <i class="fa fa-google-plus"></i>
                                                            <span>Google</span>
                                                        </a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <!--POPUP BOOKING-->
                                <div id="open_booking" class="white-popup mfp-hide">
                                    <div class="box_account_v1">
                                        <div class="modal_form_header">
                                            <h4>Message</h4>
                                        </div>
                                        <div class="popup_main_area">
                                            <form enctype="multipart/form-data" id="booking_check" name="booking_check">
                                                <p style="font-weight: bold; font-size: 18px;">Confirm booking
                                                    information ?</p>
                                                <span
                                                    style="color: red; font-style: italic; margin: 10px 0; display: block;">We
                                                    will send a text message to you via the number below after
                                                    we confirm the calendar for your booking!</span>
                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label class="form-control-label">Your name</label>
                                                            <div class="form-control-wrapper">
                                                                <input name="input_name" type="text"
                                                                    class="form-control" type="text"
                                                                    data-validation="[NOTEMPTY]"
                                                                    data-validation-message="Please enter your name!" />
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-2" style="display: none;">
                                                        <div class="form-group">
                                                            <label class="form-control-label">Area code</label>
                                                            <select name="area_code" class="form-control"
                                                                defaultvalue="1">
                                                                <option value="1">US (+1)</option>
                                                                <option value="84">VN (+84)</option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label class="form-control-label">Phone
                                                                number</label>
                                                            <div class="form-control-wrapper">
                                                                <input name="phone_number" type="text"
                                                                    class="form-control inputPhone" type="text"
                                                                    placeholder="Ex: (123) 123-1234"
                                                                    data-validation="[NOTEMPTY]"
                                                                    data-validation-message="Phone number invalid" />
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <div class="form-group">
                                                            <label class="form-control-label">Note
                                                                (Optional)</label>
                                                            <div class="form-control-wrapper">
                                                                <textarea name="notelist"
                                                                    placeholder="Max length 200 character" rows="5"
                                                                    class="form-control" maxlength="200"></textarea>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <!-- Store options -->
                                                    <input type="hidden" name="choose_store" value="">
                                                    <!-- End - Store options -->
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <div class="pull-right">
                                                            <button class="btn btn-success btn_confirmed "
                                                                type="button">Confirm
                                                            </button>
                                                            <button class="btn btn-danger btn-inline btn_cancel"
                                                                type="button">Cancel</button>
                                                        </div>
                                                    </div>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>


                            </section>

                            <script>
                                checktimebooking = 1;
                                $(document).ready(function() {
                                    $("#form-login").validate({
                                        submit: {
                                            settings: {
                                                button: "[type='submit']",
                                                inputContainer: '.form-group',
                                                errorListClass: 'form-tooltip-error',
                                            }
                                        }
                                    });
                                    // check Time
                                    $('form#booking_check').validate({
                                        submit: {
                                            settings: {
                                                clear: 'keypress',
                                                display: "inline",
                                                button: ".btn_confirmed",
                                                inputContainer: 'form-group',
                                                errorListClass: 'form-tooltip-error',
                                            },
                                            callback: {
                                                onSubmit: function(node, formdata) {
                                                    // console.log("toi day");
                                                    var areacode =
                                                        1; //$("select[name='area_code']").val();
                                                    var phone_number = $(
                                                            "input[name='phone_number']")
                                                        .val();
                                                    var cus_name = $(
                                                            "input[name='input_name']")
                                                        .val();
                                                    var notelist = $(
                                                            "textarea[name='notelist']")
                                                        .val();
                                                    $("input[name='notelist']").val(
                                                        notelist);
                                                    if (phone_number != "" && phone_number
                                                        .length > 6) {
                                                        $("input[name='booking_phone']")
                                                            .val(phone_number);
                                                        $("input[name='booking_area_code']")
                                                            .val(areacode);
                                                    }

                                                    if (cus_name) {
                                                        $("input[name='booking_name']").val(
                                                            cus_name);
                                                    }

                                                    if (enableRecaptcha) {
                                                        var check_google = $(
                                                                "#g-recaptcha-response")
                                                            .val();
                                                        if (typeof(check_google) !=
                                                            "undefined" && check_google !=
                                                            "") {
                                                            $("input[name='g-recaptcha-response']")
                                                                .val(check_google);
                                                            $("#surveyForm").submit();
                                                        }
                                                    } else {
                                                        $("#surveyForm").submit();
                                                    }
                                                    return false;
                                                }
                                            }
                                        }
                                    });

                                });

                            </script>

                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
</main>
