<main>
    <div class="pmain">
        <section class="p-list-gallery">

            <div class="page-heading">
                <div class="container">
                    <h2 class="bt-text-ellipsis">{{ $album['name'] }}</h2>
                </div>
            </div>
            <div class="container">
                <div class="in-container">
                    <div class="gallery-box-wrap">
                        <div class="row">
                            @foreach ($list as $item)
                                <div class="col-xs-6 col-sm-6 col-md-4 gallery-category-4 gallery-83">
                                    <a itemprop="url" class="gallery-item gallery-item-popup" data-group="gallery-1"
                                        title="{{ $album['name'] }}"
                                        href="{{ asset('storage') }}/photos/{{ $item['url'] }}">
                                        <div class="gallery-box">
                                            <div class="image-bg"
                                                style="background-image: url('{{ asset('storage') }}/photos/{{ $item['url'] }}');">
                                                <img itemprop="image"
                                                    src="{{ asset('storage') }}/photos/{{ $item['url'] }}">
                                            </div>
                                        </div>
                                    </a>
                                </div>
                            @endforeach
                        </div>
                    </div>
                    <!-- Pagination -->
                    {{ $list->links('frontend.nail02-m.component.custom-pagination') }}

                </div>
            </div>
        </section>
    </div>
</main>
