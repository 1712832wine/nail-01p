<main>
    <div class="pmain">
        {{-- CAROUSEL --}}
        <div class="text-center">
            <div class="slider-pro bg-dark-alfa-30" id="my-slider">
                <div class="sp-slides">
                    @foreach ($imgs_carousel as $img)
                        <div class="sp-slide">
                            <div class="sp-layer sp-static">
                                <img itemprop="image" class="sp-image"
                                    src="{{ asset('storage') }}/photos/{{ $img['url'] }}" />
                            </div>
                            <div class="sp-layer sp-static" data-width="100%" data-height="100%">
                                <div class="sp-slide-content text-white">
                                    <div class="sp-slide-text">
                                        <h2 class="sp-slide-title-text"></h2>
                                    </div>
                                </div>
                            </div>
                        </div>
                    @endforeach
                </div>
            </div>
        </div>
        {{-- SERVICES --}}
        <div class="section section-welcome">
            <div class="container">
                <div class="section-title">
                    <h3>Welcome to</h3>
                    <h2>{{ $header['name'] }}</h2>
                    <div class="line-title"></div>
                </div>
                <div class="section-content">
                    <div class="text-block-custom">
                        <h3>You will like to look like a goddess every day!</h3>
                        <h4>With the full of beauty care services and new models for you to choose, you are
                            ensured to enjoy the best services in our effort of doing a great job</h4>
                    </div>
                    <div class="list-service">
                        @foreach ($services as $service)
                            <div class="item">
                                <div class="thumb">
                                    <div class="thumb_image_service"
                                        style="background-image: url('{{ asset('storage') }}/photos/{{ json_decode($service['images'])[0] }}');">
                                    </div>
                                    <img itemprop="image" style="display: none"
                                        src="{{ asset('storage') }}/photos/{{ json_decode($service['images'])[0] }}"
                                        class="imgrps" alt="line">
                                    <div class="btn-inner">
                                        <a itemprop="url" class="btn-booking"
                                            href="{{ route('services-detail', ['slug' => strtolower(trim(preg_replace('/[^A-Za-z0-9-]+/', '-', $service['name']))), 'service_id' => $service['id']]) }}">View
                                            more</a>
                                    </div>
                                </div>
                                <h3 itemprop="name"> {{ $service['name'] }}</h3>
                            </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
        {{-- WHY CHOOSE US --}}
        {{-- HOME ARTICLE --}}
        @if (count($home_articles) > 0)
            <div class="section section-choose bg-gray">
                <div class="container">
                    <div class="section-title">
                        <h3>{{ $header['name'] }}</h3>
                        <h2>Why Choose Us</h2>
                        <div class="line-title"></div>
                    </div>
                    <div class="section-content">
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="radius-thumb">
                                    @if (count(json_decode($home_articles[0]['image'])) > 0)
                                        <img src="{{ asset('storage') }}/photos/{{ json_decode($home_articles[0]['image'])[0] }}"
                                            class="imgrps" caption="false" />
                                    @endif
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="choose-info" id="choose-info">
                                    <h3>{{ $home_articles[0]['title'] }}</h3>
                                    {!! $home_articles[0]['content'] !!}
                                </div>
                                <script>
                                    // ADD ICON TO LI
                                    var newFirstElement = '<i class="fa fa-check"></i>';
                                    $(newFirstElement).prependTo("#choose-info ul li");

                                </script>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        @endif

        {{-- GALLERY --}}
        <div class="section section-gallery">
            <div class="section-title">
                <h3>{{ $header['name'] }}</h3>
                <h2>Our Gallery</h2>
                <div class="line-title"></div>
            </div>
            <div class="section-content">
                <div class="row">
                    @foreach ($gallery_list as $item)
                        <div class="col-xs-6 col-sm-6 col-md-3">
                            <a class="gallery-item" href="{{ asset('storage') }}/photos/{{ $item['url'] }}">
                                <div class="item">

                                    <span
                                        style="background-image:url('{{ asset('storage') }}/photos/{{ $item['url'] }}')"></span>

                                    <div class="zoom-container">
                                        <div class="zoomex2">
                                            <h3 class="title-services">Nails Design</h3>
                                        </div>
                                    </div>
                                </div>
                            </a>
                        </div>
                    @endforeach
                </div>
            </div>
        </div>
        {{-- CUSTOMERS --}}
        <div class="section section-testimonial">
            <div class="container">
                <div class="list-tmm-thumb list-testimonial slider-for">
                    @foreach ($carousel_customers as $index => $item)
                        <div class="item">
                            <div class="icon" data-hash="t{{ $index }}">,,</div>
                            {!! $item['content'] !!}
                            <h4 class="bt-title">{{ $item['title'] }}</h4>
                        </div>
                    @endforeach
                </div>
                <div class="list-tmm-thumb slider-nav">
                    @foreach ($carousel_customers as $index => $item)
                        <div class="item">
                            @if (count(json_decode($item['image'])) > 0)
                                <a href="#t{{ $index }}" class="thumb">
                                    <img src="{{ asset('storage') }}/photos/{{ json_decode($item['image'])[0] }}"
                                        class="imgrps" caption="false" />
                                </a>
                            @endif
                        </div>
                    @endforeach
                </div>
            </div>
        </div>
        {{-- MAKE APPOINTMENT --}}
        @if (count($home_articles) > 1)
            <div class="section section-appointment">
                <div class="container">
                    <div class="section-content">
                        <div class="row">
                            <div class="col-sm-5">
                                <div class="radius-thumb"><img
                                        src="{{ asset('storage') }}/photos/{{ json_decode($home_articles[1]['image'])[0] }}"
                                        class="imgrps" caption="false" /></div>
                            </div>
                            <div class="col-sm-7">
                                <div class="appointment-info">
                                    <div class="section-title">
                                        <h3>{{ $header['name'] }}</h3>
                                        <h2>{{ $home_articles[1]['title'] }}</h2>
                                        <div class="line-title"></div>
                                    </div>
                                    <div class="text-center">
                                        <h3>{{ $home_articles[1]['description'] }}</h3>
                                        {!! $home_articles[1]['content'] !!}
                                        <p><a class="btn-default" href="/book">Make an appointment</a></p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        @endif







    </div>
</main>
