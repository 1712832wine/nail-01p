<main>
    <div class="pmain">
        <section class="p-gallery">
            <!-- tpl main gallery -->
            <!-- support gallery_data, gallery_category -->
            <div class="page-heading">
                <div class="container">
                    <h2 class="bt-text-ellipsis">Photo Gallery</h2>
                </div>
            </div>
            <div class="container">
                <div class="in-container">
                    <div class="in-content list-gallery">
                        <div class="gallery-box-wrap">
                            <div class="row">
                                @foreach ($albums as $index => $album)
                                    <div class="col-xs-6 col-sm-6 col-md-4 gallery-category-4">
                                        <a itemprop="url" data-group="gallery-1" title="{{ $album['name'] }}"
                                            href="/gallery-detail/{{ $album['id'] }}">
                                            <div class="gallery-box">
                                                <div class="image-bg"
                                                    style="background-image: url('{{ asset('storage') }}/photos/{{ $lists[$index][0]['url'] }}');">
                                                    <img itemprop="image"
                                                        src="{{ asset('storage') }}/photos/{{ $lists[$index][0]['url'] }}">
                                                </div>
                                                <div class="gallery-title">
                                                    <span itemprop="name">{{ $album['name'] }}</span>
                                                </div>
                                            </div>
                                        </a>
                                    </div>
                                @endforeach
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
</main>
