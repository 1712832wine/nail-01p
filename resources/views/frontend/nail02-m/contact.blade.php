<main>
    <div class="pmain">
        <div class="page-heading">
            <div class="container">
                <h2 class="bt-text-ellipsis">Contact Us</h2>
            </div>
        </div>
        <section class="section clearfix">
            <div class="container">
                <div class="row">
                    <div class="col-md-6 art-postcontent">
                        <h3>Stay in touch</h3>
                        <div class="img-desc-shop">
                            <!-- Google map area -->
                            <div class="google-map-wrapper">
                                <div class="google-map" id="map"><iframe
                                        src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3464.7391505606774!2d-95.57299108550652!3d29.727314340440024!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x8640ddc6ed681049%3A0x74ace03ed50728e8!2sFast+Boy+Marketing!5e0!3m2!1sen!2s!4v1510909605272"
                                        width="100%" height="450" frameborder="0" style="border:0"
                                        allowfullscreen></iframe></div>
                            </div>

                            <style>
                                #map {
                                    width: 100%;
                                    height: auto
                                }

                            </style>
                        </div>
                    </div>
                    <div class="col-md-6 art-postcontent">
                        <h3>Send Message:</h3>
                        <form enctype="multipart/form-data" method="post" name="send_contact" id="send_contact_main"
                            class="wpcf7-form" action="{{ route('send_contact') }}">
                            @csrf
                            <div class="form-group">
                                <label class="control-label" for="contactname">Your Name</label>
                                <input id="contactname" type="text" data-validation="[NOTEMPTY]"
                                    data-validation-message="Please enter your name!" autocomplete="off"
                                    class="form-control" placeholder="Your Name" name="contactname">
                            </div>
                            <div class="form-group">
                                <label class="control-label" for="contactemail">Email address</label>
                                <input id="contactemail" type="text" data-validation="[EMAIL]"
                                    data-validation-message="Invalid email" autocomplete="off" class="form-control"
                                    placeholder="Your Email" name="contactemail">
                            </div>
                            <div class="form-group">
                                <label class="control-label" for="contactsubject">Subject</label>
                                <input id="contactsubject" type="text" data-validation="[NOTEMPTY]"
                                    data-validation-message="Please enter a subject!" autocomplete="off"
                                    class="form-control" placeholder="Your Subject" name="contactsubject">
                            </div>
                            <div class="form-group">
                                <label class="control-label" for="contactmessage">Your Message</label>
                                <textarea rows="3" id="contactmessage" data-validation="[NOTEMPTY]"
                                    data-validation-message="Please enter a content!" autocomplete="off"
                                    class="form-control" placeholder="Your Message" name="contactcontent"></textarea>

                            </div>
                            <div class="form-group">
                                @if (session('status'))
                                    <div class="alert alert-success" role="alert">
                                        <button type="button" class="close" data-dismiss="alert">×</button>
                                        {{ session('status') }}
                                    </div>
                                @elseif(session('failed'))
                                    <div class="alert alert-danger" role="alert">
                                        <button type="button" class="close" data-dismiss="alert">×</button>
                                        {{ session('failed') }}
                                    </div>
                                @endif
                            </div>
                            <div class="form-group">
                                <input class="btn btn-default btn_contact " type="submit" value="Send Us"
                                    name="submit_ok">
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </section>
    </div>
</main>
