{{-- CAROUSEL --}}
<section style="position: relative;">
    <div class="banner-slider-index bg_grey1">
        <div class="ac">
            <div class="box-color">
                <div class="tp-banner-container">
                    <div class="tp-banner-img" style="height:1px; width: 100%; overflow: hidden;">
                        <div class="tp-banner-height" style="width: 1px; margin-left: -5px !important;"></div>
                        <img src="{{ asset('storage') }}/photos/{{ $imgs_carousel[0]['url'] }}"
                            style="width: 100%; height:auto;" alt="dreamstime_l_195495773.jpg">
                    </div>
                    <div class="tp-banner" style="display:none;">
                        <ul
                            style="background: url('{{ asset('frontend') }}/themes/fnail01d-luxury/assets/img/bg-s1.jpg') center;">
                            @foreach ($imgs_carousel as $img)
                                <li data-transition="fadetoleftfadefromright" data-slotamount="7"
                                    data-masterspeed="1500">
                                    <img itemprop="image" src="{{ asset('storage') }}/photos/{{ $img['url'] }}"
                                        alt="{{ $img['title'] }}" data-bgfit="cover" data-bgposition="center center"
                                        data-bgrepeat="no-repeat">
                                </li>
                            @endforeach
                        </ul>
                        <div class="tp-bannertimer" style="display: none !important;"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
{{-- ABOUT ARTICLE --}}
<section class="video_page_v1 video_page_v1_board">
    <section class="video_page_v1_board_container">
        <div class="container">
            <link rel="stylesheet" type="text/css"
                href='{{ asset('frontend') }}/themes/fnail01d-luxury/assets/youtube-player-plugin/youtube-player.css'>
            <div class="row">
                <div class="about-home">
                    <div class="our_service_title">
                        <h2 itemprop="name" class="section_title">Welcome To<br /> {{ $home_article['title'] }}</h2>
                    </div>
                    {!! $home_article['content'] !!}
                    <p><a href="/about"><em><strong>View More</strong></em></a></p>
                </div>
            </div>
            <script type="text/javascript"
                src="{{ asset('frontend') }}/themes/fnail01d-luxury/assets/youtube-player-plugin/youtube-player.js">
            </script>
        </div>
    </section>
</section>
{{-- SERVICES --}}
<section class="our_service_v4">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="our_service_title">
                    <h2 itemprop="name" class="section_title">Our Services</h2>
                    <p itemprop="description" class="section_details"></p>
                </div>
            </div>
        </div>
        <div class="row">
            @foreach ($services as $service)
                <div class="featured-image-column col-md-4 col-sm-4 col-xs-12">
                    <a itemprop="url"
                        href="{{ route('services-detail', ['slug' => strtolower(trim(preg_replace('/[^A-Za-z0-9-]+/', '-', $service['name']))), 'service_id' => $service['id']]) }}"
                        title="{{ $service['name'] }}">
                        <div itemscope itemtype="http://schema.org/Service"
                            class="inner-box sv-bg-cover hvr-float-shadow">
                            <meta itemprop="serviceType" content="{{ $service['name'] }}" />
                            <meta itemprop="image"
                                content="{{ asset('storage') }}/photos/{{ json_decode($service['images'])[0] }}">
                            </meta>
                            <figure class="image-box"
                                style="background: url('{{ asset('storage') }}/photos/{{ json_decode($service['images'])[0] }}') center center no-repeat;background-size: cover;">
                            </figure>
                            <div class="image-caption">
                                <span itemprop="name">{{ $service['name'] }}</span>
                            </div>
                        </div>
                    </a>
                </div>
            @endforeach
        </div>
    </div>
</section>
{{-- GALLERY --}}
<section class="gallery_box_page ourworking_v3" id="portfolio">
    <div class="container container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="title_gallery_home_v4 gallery_title">
                    <h2 itemprop="name" class="section_title">Our Gallery</h2>
                    <p itemprop="description"></p>
                </div>
            </div>
        </div>
        <div class="row">

            <div class="list-gallery">
                <div class="tab-content">
                    <!-- Custom height with class: .portfolio.gallery-bg-cover {padding-bottom: 75%;} -->
                    <div class="tab-pane fade active in">
                        <ul id="portfolio-grid" class="row four-column hover-four portfolio-grid">
                            @foreach ($gallery_list as $item)
                                <li class="col-md-3 col-xs-6 portfolio-item gallery-bg-cover text-center">
                                    <div class="tt-lightbox"
                                        href="{{ asset('storage') }}/photos/{{ $item['url'] }}"
                                        title="{{ $item['name'] }}">
                                        <div class="portfolio gallery-bg-cover">
                                            <div class="tt-overlay"></div>
                                            <meta itemprop="image"
                                                content="{{ asset('storage') }}/photos/{{ $item['url'] }}">
                                            </meta>
                                            <div class="portfolio-image"
                                                style="background: url('{{ asset('storage') }}/photos/{{ $item['url'] }}') center center no-repeat; background-size: cover;">
                                            </div>
                                            <div class="portfolio-info">
                                                <h3 itemprop="name" class="project-title">{{ $item['name'] }}</h3>
                                                <span class="links" itemprop="description">{{ $item['name'] }}</span>
                                            </div>
                                            <ul class="portfolio-details">
                                                <li><span><i class="icon icon-zoom-in"></i></span></li>
                                            </ul>
                                        </div>
                                    </div>
                                </li>
                            @endforeach
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
