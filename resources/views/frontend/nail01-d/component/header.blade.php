<header class="header_ver4 inner_header_v4">
    <div class="top_link">
        <div class="container">
            <div class="row">
                <div class="hidden-xs col-sm-12 col-md-12 col-lg-12">
                    <div class="row">
                        <div class="col-md-9 col-sm-7">
                            <div class="fci-wrap">
                                <div class="fci-row">
                                    Leave your stressful work behind and enjoy happy time with us!
                                </div>
                            </div>
                        </div>
                        <div class="col-md-3 hidden-xs col-sm-5">
                            <section class="nav_right">
                                <div class="header-left">
                                    <ul class="list-inline">
                                    </ul>
                                </div>
                            </section>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="topbar bg_top_tran">
        <div class="wrap-freeze-header-mobile clearfix hidden-sm hidden-md hidden-lg menu-1024-hidden">
            <div class="flag-freeze-header-mobile">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-5 col-md-4">
                            <div class="logo_ver3">
                                @if (count(json_decode($header['images'])) > 0)
                                    <a href="/">
                                        <img class="hidden-xs" itemprop="logo"
                                            src="{{ asset('storage') }}/photos/{{ json_decode($header['images'])[0] }}"
                                            alt="{{ $header['title'] }}">
                                        <img class="hidden-lg hidden-md hidden-sm" itemprop="logo"
                                            src="{{ asset('storage') }}/photos/{{ json_decode($header['images'])[0] }}"
                                            alt="{{ $header['title'] }}">
                                    </a>
                                @endif

                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="hidden-sm hidden-md hidden-lg menu-768-hidden">
                            <div class="menu_mobile_v1">
                                <div class="mobile_menu_container_v1 header-nav-mobile">
                                    <div class="mobile-menu clearfix">
                                        <nav id="mobile_dropdown">
                                            <ul>
                                                <li>
                                                    <a itemprop="url" href="/" title="Home"><span
                                                            itemprop="name">Home</span></a>
                                                </li>
                                                <li>
                                                    <a itemprop="url" href="/about" title="About Us"><span
                                                            itemprop="name">About Us</span></a>
                                                </li>
                                                <li>
                                                    <a itemprop="url" href="/services" title="Services"><span
                                                            itemprop="name">Services</span></a>
                                                </li>
                                                <li>
                                                    <a itemprop="url" href="/gallery" title="Gallery"><span
                                                            itemprop="name">Gallery</span></a>
                                                </li>
                                                <li>
                                                    <a itemprop="url" href="/book" title="Booking"><span
                                                            itemprop="name">Booking</span></a>
                                                </li>
                                                <li>
                                                    <a itemprop="url" href="/coupons" title="Coupons"><span
                                                            itemprop="name">Coupons</span></a>
                                                </li>

                                                <li>
                                                    <a itemprop="url" href="/contact" title="Contact Us"><span
                                                            itemprop="name">Contact Us</span></a>
                                                </li>
                                            </ul>
                                        </nav>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="wrap-freeze-header clearfix hidden-xs">
            <div class="flag-freeze-header">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-5 col-md-4">
                            <div class="logo_ver3">
                                @if (count(json_decode($header['images'])) > 0)
                                    <a href="/">
                                        <img class="hidden-xs" itemprop="logo"
                                            src="{{ asset('storage') }}/photos/{{ json_decode($header['images'])[0] }}"
                                            alt="{{ $header['title'] }}">
                                        <img class="hidden-lg hidden-md hidden-sm" itemprop="logo"
                                            src="{{ asset('storage') }}/photos/{{ json_decode($header['images'])[0] }}"
                                            alt="{{ $header['title'] }}">
                                    </a>
                                @endif
                            </div>
                        </div>
                        <div class="col-lg-7 col-md-8 hidden-xs menu-768-visible">
                            <nav class="header-nav">
                                <div class="nav-menu cssmenu menu-line-off">
                                    <nav class="menu_custom">
                                        <ul>
                                            <li>
                                                <a itemprop="url" href="/" title="Home"><span
                                                        itemprop="name">Home</span></a>
                                            </li>
                                            <li>
                                                <a itemprop="url" href="/about" title="About Us"><span
                                                        itemprop="name">About Us</span></a>
                                            </li>
                                            <li>
                                                <a itemprop="url" href="/services" title="Services"><span
                                                        itemprop="name">Services</span></a>
                                            </li>
                                            <li>
                                                <a itemprop="url" href="/gallery" title="Gallery"><span
                                                        itemprop="name">Gallery</span></a>
                                            </li>
                                            <li>
                                                <a itemprop="url" href="/coupons" title="Coupons"><span
                                                        itemprop="name">Coupons</span></a>
                                            </li>
                                            <li>
                                                <a itemprop="url" href="/book" title="Booking"><span
                                                        itemprop="name">Booking</span></a>
                                            </li>

                                            <li>
                                                <a itemprop="url" href="/contact" title="Contact Us"><span
                                                        itemprop="name">Contact Us</span></a>
                                            </li>
                                        </ul>
                                    </nav>
                                </div>
                            </nav>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</header>
