@php
$features = json_decode($header['features']);
$extras = json_decode($header['extras']);
@endphp
<footer class="footer_area">
    <section class="footer_v4">
        <div class="container">
            <div class="row">
                <div class="col-md-9">
                    <div class="row">
                        <div class="col-md-6">
                            <h4 itemprop="name">{{ $header['name'] }}</h4>
                            <div class="footer-contact-wrap">
                                <div class="fci-wrap">
                                    <div class="fci-row" itemprop="address" itemscope
                                        itemtype="http://schema.org/PostalAddress">
                                        <span class="fci-title"><i class="fa fa-map-marker icons-address"></i></span>
                                        <span class="fci-content">
                                            <span itemprop="streetAddress"
                                                class="address">{{ $features[0]->desc }}</span>
                                        </span>
                                    </div>
                                    <div class="fci-row">
                                        <span class="fci-title"><i class="fa fa-phone icons-phone"></i></span>
                                        <span class="fci-content">
                                            <a href="tel:{{ $features[1]->desc }}" title="Call Us">
                                                <span itemprop="telephone"
                                                    class="phone">{{ $features[1]->desc }}</span>
                                            </a>
                                        </span>
                                    </div>
                                    <div class="fci-row">
                                        <span class="fci-title"><i class="fa fa-envelope icons-email"></i></span>
                                        <span class="fci-content">
                                            <a href="mailto:{{ $features[2]->desc }}">
                                                <span itemprop="email" class="email">{{ $features[2]->desc }}</span>
                                            </a>
                                        </span>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="footer_openhours">
                                <h4 itemprop="name">BUSINESS HOURS</h4>
                                <!-- support render openhours_data or openhoursshort_data -->
                                <div class="foh-wrap">
                                    @foreach ($extras as $item)
                                        <div class="foh-row" itemprop="openingHours"
                                            content="{{ $item->name }}:{{ $item->desc }}">
                                            <span class="foh-date">{{ $item->name }}</span>
                                            <span class="foh-time">{{ $item->desc }}</span>
                                        </div>
                                    @endforeach
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-3">
                    <h4 itemprop="name">FOLLOW US</h4>
                    <ul class="list-social-footer">
                        @foreach ($social as $item)
                            @if ($item['url'])
                                <li>
                                    <a target="_blank" href="{{ $item['content'] }}" title="{{ $item['title'] }}"
                                        rel="nofollow">
                                        <img src="{{ asset('storage') }}/photos/{{ $item['url'] }}"
                                            alt="{{ $item['title'] }}">
                                    </a>
                                </li>
                            @endif
                        @endforeach
                    </ul>
                    <aside>
                        <div id="fanpage_fb_container"></div>
                        <div id="social_block_width" style="width:100% !important; height: 1px !important">
                        </div>
                    </aside>
                </div>
            </div>
            <div class="container ftedt">
                <div class="row">
                    <div class="copy_right_v3">
                        <p>© Copyright by Fnail01d Luxury. All Rights Reserved</p>
                    </div>
                </div>
            </div>
        </div>
    </section>
</footer>
<div class="freeze-footer">
    <ul>
        <li><a href="tel:{{ $features[1]->desc }}" class="btn btn-default btn_call_now btn-call"
                title="Call us">{{ $features[1]->desc }}</a>
        </li>
        <li><a href="/book" class="btn btn-default btn_make_appointment" title="Booking">Booking</a></li>
    </ul>
</div>
