@php
$features = json_decode($header['features']);
$extras = json_decode($header['extras']);
@endphp
<section class="breadcrumbs_area breadcrumb-image section-padding parallex">
    <div class="container">
        <div class="row">
            <div class="col-md-12 text-center">
                <div class="breadcrumbs">
                    <h2 itemprop="name" class="page_title">
                        Contact us
                    </h2>
                    <!--<ul itemscope itemtype="http://schema.org/BreadcrumbList" class="">
                        <li itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem">
                            <a itemprop="item" href="/" title="Home" class="active">
                                <span itemprop="name">Home</span>
                                <meta itemprop="position" content="1"/>
                            </a>
                        </li>
                        <li itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem">
                            <span itemprop="item" title="Booking" class="active">
                                <span itemprop="name">Contact</span>
                                <meta itemprop="position" content="2"/>
                            </span>
                        </li>
                    </ul>-->
                </div>
            </div>
        </div>
    </div>
</section>
<section class="contact_us_v1">
    <div class="container">
        <div class="row">
            <div class="col-md-7 col-sm-6">
                <div class="row">
                    <script src='https://www.google.com/recaptcha/api.js?hl=en' async defer></script>
                    <!-- Google reCaptcha -->
                    <script type="text/javascript">
                        <!--
                        function ezyCaptcha_send_contact(token, is_submit) {
                            is_submit = 0;
                            if ($("#password").length) {
                                //$("input:password").val(md5(clean_input($("#password").val())));
                            }
                            return true;
                        }
                        //

                        -->
                    </script>
                    <form enctype="multipart/form-data" method="post" name="send_contact" id="send_contact"
                        action="{{ route('send_contact') }}">
                        @csrf
                        <div class="col-md-6">
                            <div class="form-group">
                                <input type="text" data-validation="[NOTEMPTY]"
                                    data-validation-message="Please enter your name!" autocomplete="off"
                                    class="form-control" placeholder="Your Name" name="contactname">
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <input type="text" data-validation="[EMAIL]" data-validation-message="Invalid email"
                                    autocomplete="off" class="form-control" placeholder="Your Email"
                                    name="contactemail">
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group">
                                <input type="text" data-validation="[NOTEMPTY]"
                                    data-validation-message="Please enter a subject!" autocomplete="off"
                                    class="form-control" placeholder="Your Subject" name="contactsubject">
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group">
                                <textarea rows="10" data-validation="[NOTEMPTY]"
                                    data-validation-message="Please enter a content!" autocomplete="off"
                                    class="form-control" placeholder="Your Message" name="contactcontent"></textarea>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group">
                                @if (session('status'))
                                    <div class="alert alert-success" role="alert">
                                        <button type="button" class="close" data-dismiss="alert">×</button>
                                        {{ session('status') }}
                                    </div>
                                @elseif(session('failed'))
                                    <div class="alert alert-danger" role="alert">
                                        <button type="button" class="close" data-dismiss="alert">×</button>
                                        {{ session('failed') }}
                                    </div>
                                @endif
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group">
                                <button type="submit" class="hs-btn btn_contact btn-light "> Send message
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
            <div class="col-md-5 col-sm-6">
                <div class="contact_info">
                    <div class="contact-wrap">
                        <div class="fci-wrap">
                            <div class="fci-row" itemprop="address" itemscope=""
                                itemtype="http://schema.org/PostalAddress">
                                <span class="fci-title">Address:</span>
                                <span class="fci-content">
                                    <span itemprop="streetAddress" class="address">{{ $features[0]->desc }}
                                    </span>
                                    <br />
                                    <span itemprop="streetAddress" class="address"></span>
                                </span>
                            </div>
                            <div class="fci-row">
                                <span class="fci-title">Phone:</span>
                                <span class="fci-content">
                                    <a href="tel:{{ $features[1]->desc }}" title="Call Us">
                                        <span itemprop="telephone" class="phone">{{ $features[1]->desc }}</span>
                                    </a>
                                    <br />
                                    <a href="tel:" title="Call Us"> <span itemprop="telephone" class="phone"></span>
                                    </a>
                                </span>
                            </div>
                            <div class="fci-row">
                                <span class="fci-title">Email:</span>
                                <span class="fci-content">
                                    <a href="mailto:{{ $features[2]->desc }}">
                                        <span itemprop="email" class="email">{{ $features[2]->desc }}</span>
                                    </a>
                                    <br />
                                    <a href="mailto:"> <span itemprop="email" class="email"></span> </a>
                                </span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- Google map area -->
<div class="google-map-wrapper">
    <div itemscope itemprop="hasMap" itemtype="http://schema.org/Map" class="google-map" id="map"
        style="width:100%; height:auto;"><iframe
            src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3464.7391505606774!2d-95.57299108550652!3d29.727314340440024!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x8640ddc6ed681049%3A0x74ace03ed50728e8!2sFast+Boy+Marketing!5e0!3m2!1sen!2s!4v1510909605272"
            width="100%" height="450" frameborder="0" style="border:0" allowfullscreen></iframe></div>
</div>
<input type="hidden" id="address" name="address" value="{{ $features[0]->desc }}" />
<!-- End Container-->
