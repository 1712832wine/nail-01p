<section class="breadcrumbs_area breadcrumb-image section-padding parallex">
    <div class="container">
        <div class="row">
            <div class="col-md-12 text-center">
                <div class="breadcrumbs">
                    <h2 itemprop="name" class="page_title">
                        Gallery Album
                    </h2>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- module 3 -->
<section class="coupon_page_v1">
    <!-- tpl main gallery -->
    <!-- support gallery_data, gallery_category -->

    <div class="container">
        <div class="in-container">
            <div class="in-content list-gallery">
                <div class="gallery-box-wrap">
                    <div class="row">
                        <!-- Custome Height with .gallery-box .image-bg{padding-bottom: 75%;} -->
                        <!-- List category -->
                        @foreach ($albums as $index => $album)
                            <div class="col-xs-6 col-sm-6 col-md-4">
                                <a itemprop="url" data-group="gallery-1" title="Nails Design"
                                    href="/gallery-detail/{{ $album['id'] }}">
                                    <div class="gallery-box">
                                        <div class="image-bg"
                                            style="background-image: url('{{ asset('storage') }}/photos/{{ $lists[$index][0]['url'] }}');">
                                            <img itemprop="image"
                                                src="{{ asset('storage') }}/photos/{{ $lists[$index][0]['url'] }}">
                                        </div>
                                        <div class="gallery-title">
                                            <span itemprop="name">{{ $album['name'] }}</span>
                                        </div>
                                    </div>
                                </a>
                            </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </div><!-- .in-container -->
    </div>
</section> <!-- End Container-->

<!-- module 8 -->
