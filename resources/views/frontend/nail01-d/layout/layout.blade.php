<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" dir="ltr" lang="en" xmlns:og="http://ogp.me/ns#"
    xmlns:fb="http://www.facebook.com/2008/fbml" itemscope itemtype="https://schema.org/NailSalon">

<head>
    <!-- Language -->
    <meta http-equiv="content-language" content="en" />
    <link rel="alternate" href="" hreflang="x-default">
    <link rel="alternate" href="" hreflang="en-US">

    <!-- Meta limit -->

    <!-- Meta Tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width,initial-scale=1.0" />
    <meta http-equiv="content-type" content="text/html; charset=UTF-8" />
    <meta name="description" content="" />
    <meta name="keywords" content="" />
    <meta name="author" content="" />

    <!-- OG -->
    <meta property="og:title" content="" />
    <meta property="og:description" content="" />
    <meta property="og:image" content="" />
    <meta property="og:url" content="/" />
    <meta property="og:type" content="website" />
    <meta property="og:site_name" content="Fnail01d Luxury" />

    <!-- Dublin Core -->
    <link rel="schema.DC" href="http://purl.org/dc/elements/1.1/">
    <meta name="DC.title" content="">
    <meta name="DC.identifier" content="/">
    <meta name="DC.description" content="">
    <meta name="DC.subject" content="">
    <meta name="DC.language" scheme="UTF-8" content="">
    <meta itemprop="priceRange" name="priceRange" content="">

    <!-- GEO meta -->
    <meta name="geo.region" content="">
    <meta name="geo.placename" content="">
    <meta name="geo.position" content="">
    <meta name="ICBM" content="">

    <!-- Page Title -->
    <title>Fnail01d Luxury</title>
    <base href="frontend/themes/fnail01d-luxury/assets/">

    <!-- canonical -->
    <link rel="canonical" href="">



    <!-- Favicons -->
    <link rel="icon" href="/uploads/fnail0wpol7ns/attach/1562751341_fbm_fvc.png" type="image/x-icon">
    <link rel="shortcut icon" href="/uploads/fnail0wpol7ns/attach/1562751341_fbm_fvc.png" type="image/x-icon">

    <!-- CSS -->
    <link rel="stylesheet" type="text/css"
        href="https://fonts.googleapis.com/css?family=Lato:300,300i,400,400i,600,700|Open+Sans:300,400,600">
    <link rel="stylesheet" type="text/css" href='{{ asset('frontend') }}/public/library/jquery-ui/jquery-ui.min.css'>
    <link rel="stylesheet" type="text/css" href='{{ asset('frontend') }}/public/library/animate/animate.css'>
    <link rel="stylesheet" type="text/css" href='{{ asset('frontend') }}/public/library/validation/validation.css'>
    <link rel="stylesheet" type="text/css" href='{{ asset('frontend') }}/public/library/flex-label/flex-label.css'>
    <link rel="stylesheet" type="text/css"
        href='{{ asset('frontend') }}/public/library/bootstrap/v3.3.7/bootstrap.min.css'>
    <link rel="stylesheet" type="text/css"
        href='{{ asset('frontend') }}/public/library/bootstrap-datetimepicker/bootstrap-datetimepicker.min.css'>
    <link rel="stylesheet" type="text/css"
        href='{{ asset('frontend') }}/public/library/font-awesome/v4.7.0/css/font-awesome.min.css'>
    <link rel="stylesheet" type="text/css"
        href='{{ asset('frontend') }}/public/library/slider-pro/slider-pro.min.css'>
    <link rel="stylesheet" type="text/css"
        href='{{ asset('frontend') }}/public/library/slider-slick/slider-slick.css'>
    <link rel="stylesheet" type="text/css"
        href='{{ asset('frontend') }}/public/library/slider-slick/slider-theme.css'>
    <link rel="stylesheet" type="text/css"
        href='{{ asset('frontend') }}/public/library/slider-slick/slider-style.css'>
    <link rel="stylesheet" type="text/css"
        href='{{ asset('frontend') }}/public/library/revolution/themepunch.revolution.settings.css'>
    <link rel="stylesheet" type="text/css"
        href='{{ asset('frontend') }}/public/library/owl-carousel/v2.2.1/owl.carousel.min.css'>
    <link rel="stylesheet" type="text/css"
        href='{{ asset('frontend') }}/public/library/magnific-popup/magnific-popup.css'>
    <link rel="stylesheet" type="text/css" href='{{ asset('frontend') }}/public/library/lightbox/lightbox.css'>
    <link rel="stylesheet" type="text/css" href='{{ asset('frontend') }}/public/library/pnotify/pnotify.css'>
    <link rel="stylesheet" type="text/css" href='{{ asset('frontend') }}/public/library/instagram/instafeed.min.css'>
    <link rel="stylesheet" type="text/css" href='{{ asset('frontend') }}/public/library/signature/signature.css'>
    <link rel="stylesheet" type="text/css"
        href='{{ asset('frontend') }}/themes/fnail01d-luxury/assets/css/pe-icon-7-stroke.css'>
    <link rel="stylesheet" type="text/css"
        href='{{ asset('frontend') }}/themes/fnail01d-luxury/assets/fontello/css/fontello.css'>
    <link rel="stylesheet" type="text/css"
        href='{{ asset('frontend') }}/themes/fnail01d-luxury/assets/css/reset.css'>
    <link rel="stylesheet" type="text/css"
        href='{{ asset('frontend') }}/themes/fnail01d-luxury/assets/css/helper.css'>
    <link rel="stylesheet" type="text/css" href='{{ asset('frontend') }}/themes/fnail01d-luxury/assets/css/app.css'>
    <link rel="stylesheet" type="text/css"
        href='{{ asset('frontend') }}/themes/fnail01d-luxury/assets/css/responsive.css'>
    <link rel="stylesheet" type="text/css" href='{{ asset('frontend') }}/themes/fnail01d-luxury/assets/css/menu.css'>
    <link rel="stylesheet" type="text/css"
        href='{{ asset('frontend') }}/themes/fnail01d-luxury/assets/css/meanmenu.css'>
    <link rel="stylesheet" type="text/css"
        href='{{ asset('frontend') }}/themes/fnail01d-luxury/assets/css/style-businessdark.css'>
    <link rel="stylesheet" type="text/css"
        href='{{ asset('frontend') }}/themes/fnail01d-luxury/assets/css/custom/custom_general.css'>
    <link rel="stylesheet" type="text/css"
        href='{{ asset('frontend') }}/themes/fnail01d-luxury/assets/css/custom/main.css'>
    <link rel="stylesheet" type="text/css" href='{{ asset('frontend') }}/public/library/global/css/news.css'>
    <link rel="stylesheet" type="text/css" href='{{ asset('frontend') }}/public/library/global/css/consent.css'>
    <link rel="stylesheet" type="text/css"
        href='{{ asset('frontend') }}/themes/fnail01d-luxury/assets/css/custom.css?v1.3'>
    <style type="text/css">
        @media (max-width: 575px) {
            .info_inner_booking .staff_service_v1 {
                width: 100%;
            }
        }

        .footer_v4 .footer_openhours {
            margin: 0px;
        }

        .header_ver4 .top_link {
            background-color: #b2b2b2;
        }

        .nav-menu {
            background-color: #d9d9d9;
        }

        .header-nav .menu_custom>ul>li>a {
            color: #6a6a6a;
        }

        section.our_service_v4 {
            background: #3b3b3b;
        }

        .featured-image-column .image-caption {
            background: rgba(190, 128, 185, 0.56);
        }

        .coupon_page_v1_board .coupon_page_v1_board_container {
            background-color: rgba(211, 153, 219, 0.4);
        }

        section#portfolio {
            background: rgba(211, 153, 219, 0.4);
        }

        .footer_v4 {
            background: rgba(211, 153, 219, 0.25);
        }

        .copy_right_v3 p,
        .footer_v4 .copy_right_v3 p {
            color: #fff;
            border-top: solid 0.5px;
            padding-top: 10px;
        }

        .parallex::before {
            background: rgba(170, 170, 170, 0.4) !important;
        }

        .btn-default {
            background-color: #8b4c81 !important;
        }

        h1.service-name {
            color: #8b4c81 !important;
            border-bottom: 1px solid #8b4c81 !important;
        }

        span.foh-date {
            color: #fff;
        }

        span.foh-time {
            color: #fff;
        }

        span.fci-content a {
            color: #fff !important;
            font-weight: bold;
        }

        section.coupon_page_v1 {
            background: rgba(255, 255, 255, 0.25);
        }

        .gallery-title {
            background: #000;
        }

        a:not(.button) {
            color: #fff;
        }

        .parallex::before {
            background: rgba(223, 165, 229, 0.4) !important;
        }

        .header_ver4 .top_link {
            background-color: #a3789b;
        }

        .booking_staff_title {
            display: none;
        }

        .box_staff_bg {
            background-color: #c3c3c3;
        }

        @media (min-width: 768px) {
            .our_service_v4 .row>.featured-image-column.col-md-4.col-sm-4.col-xs-12 {
                width: 49.99%;
            }
        }

        @media (min-width: 992px) {
            .our_service_v4 .row>.featured-image-column.col-md-4.col-sm-4.col-xs-12 {
                width: 24.99%;
            }
        }

        .info_staff {
            display: none;
        }

        .info_inner_booking .staff_service_v1 {
            margin-left: 15px;
        }

        .breadcrumb-image {
            background-image: unset;
        }

        .parallex::before {
            background: linear-gradient(to right, #a3789b, #cabfc8) !important;
        }

        .logo_ver3 {
            width: 70%;
        }

        .mean-container .mean-bar {
            background: linear-gradient(to right, #cabfc8, #a3789b) !important;
        }

        .mean-container .mean-nav {
            background: linear-gradient(to right, #cabfc8, #a3789b) !important;
        }

        .mean-container .mean-nav ul li a {
            font-weight: 600;
        }

        .about_us_v1 h2 {
            margin-bottom: 20px !important;
        }

        .about_us_v1 p {
            line-height: 24px;
        }

        @media (max-width: 767px) {
            .header_ver4 .topbar {
                z-index: 1023;
            }
        }

        @media (min-width: 768px) {
            .our_service_v4 .row>.featured-image-column.col-md-4.col-sm-4.col-xs-12 {
                width: 49.99%;
            }
        }

        @media (min-width: 992px) {
            .our_service_v4 .row>.featured-image-column.col-md-4.col-sm-4.col-xs-12 {
                width: 33.33%;
            }
        }

        .footer_v4 .footer_openhours {
            padding: unset;
        }

        .inner_header_v4 .topbar {
            background: #d4d4d4;
        }

        @media only screen and (max-width: 687px) {
            .topbar.bg_top_tran {
                background: #d4d4d4;
            }
        }

        @media (min-width: 768px) {
            .our_service_v4 .row>.featured-image-column.col-md-4.col-sm-4.col-xs-12 {
                width: 49.99%;
            }
        }

        @media (min-width: 992px) {
            .our_service_v4 .row>.featured-image-column.col-md-4.col-sm-4.col-xs-12 {
                width: 33.33%;
            }
        }

        .featured-image-column .image-caption {
            letter-spacing: 2px;
            font-size: 11px;
        }

        .about-home p {
            text-align: center;
            line-height: 36px;
            font-size: 16px;
        }

        @media only screen and (max-width: 687px) {
            .price-item h4 {
                font-size: 14px;
            }
        }

        .video_page_v1_board {
            background: url('img/s001.png') no-repeat center center fixed;
            background-size: cover;
        }

        .video_page_v1_board {
            background-attachment: scroll;
        }

    </style>

    <!-- JS -->
    <script type="text/javascript" src="{{ asset('frontend') }}/public/library/jquery/v2.2.4/jquery.min.js"></script>
    <script type="text/javascript" src="{{ asset('frontend') }}/public/library/jquery-ui/jquery-ui.min.js"></script>
    <script type="text/javascript" src="{{ asset('frontend') }}/public/library/jquery-ui/jquery-migrate.min.js">
    </script>
    <script type="text/javascript" src="{{ asset('frontend') }}/public/library/validation/validation.min.js">
    </script>
    <script type="text/javascript" src="{{ asset('frontend') }}/public/library/mask/mask.min.js"></script>
    <script type="text/javascript" src="{{ asset('frontend') }}/public/library/flex-label/flex-label.js"></script>
    <script type="text/javascript" src="{{ asset('frontend') }}/public/library/bootstrap/v3.3.7/bootstrap.min.js">
    </script>
    <script type="text/javascript"
        src="{{ asset('frontend') }}/public/library/waiting-modal-dialog/waiting-modal-dialog.js"></script>
    <script type="text/javascript" src="{{ asset('frontend') }}/public/library/moment/moment-with-locales.js">
    </script>
    <script type="text/javascript"
        src="{{ asset('frontend') }}/public/library/bootstrap-datetimepicker/bootstrap-datetimepicker.js"></script>
    <script type="text/javascript" src="{{ asset('frontend') }}/public/library/magnific-popup/magnific-popup.min.js">
    </script>
    <script type="text/javascript" src="{{ asset('frontend') }}/public/library/pnotify/pnotify.js"></script>
    <script type="text/javascript" src="{{ asset('frontend') }}/public/library/slider-pro/slider-pro.min.js">
    </script>
    <script type="text/javascript" src="{{ asset('frontend') }}/public/library/slider-slick/slider-slick.js">
    </script>
    <script type="text/javascript"
        src="{{ asset('frontend') }}/public/library/owl-carousel/v2.2.1/owl.carousel.min.js"></script>
    <script type="text/javascript" src="{{ asset('frontend') }}/public/library/wow/wow.min.js"></script>
    <script type="text/javascript" src="{{ asset('frontend') }}/public/library/meanmenu/meanmenu.js"></script>
    <script type="text/javascript" src="{{ asset('frontend') }}/public/library/instagram/instafeed.min.js"></script>
    <script type="text/javascript" src="{{ asset('frontend') }}/public/library/signature/signature.min.js"></script>
    <script type="text/javascript" src="{{ asset('frontend') }}/public/library/shuffle/shuffle.min.js"></script>
    <script type="text/javascript" src="{{ asset('frontend') }}/themes/fnail01d-luxury/assets/js/stellar.js">
    </script>
    <script type="text/javascript"
        src="{{ asset('frontend') }}/themes/fnail01d-luxury/assets/js/jquery.simpleLens.min.js"></script>
    <script type="text/javascript" src="{{ asset('frontend') }}/themes/fnail01d-luxury/assets/js/modernizr.js">
    </script>
    <script type="text/javascript" src="{{ asset('frontend') }}/themes/fnail01d-luxury/assets/js/plugins.js">
    </script>
    <script type="text/javascript" src="{{ asset('frontend') }}/themes/fnail01d-luxury/assets/js/menu.js"></script>
    <script type="text/javascript"
        src="{{ asset('frontend') }}/themes/fnail01d-luxury/assets/js/jquery.prettyPhoto.js"></script>
    <script type="text/javascript" src="{{ asset('frontend') }}/themes/fnail01d-luxury/assets/js/jquery.mixitup.js">
    </script>
    <script type="text/javascript"
        src="{{ asset('frontend') }}/public/library/revolution/jquery.themepunch.plugins.min.js"></script>
    <script type="text/javascript"
        src="{{ asset('frontend') }}/public/library/revolution/jquery.themepunch.revolution.min.js"></script>
    <script type="text/javascript" src="{{ asset('frontend') }}/public/library/global/js/js_php.js"></script>

    <script type="text/javascript">
        var dateFormatBooking = "MM/DD/YYYY";
        var posFormat = "1,0,2";
        var timeMorning = '["10:00","10:30","11:00","11:30"]';
        var timeAfternoon =
            '["12:00","12:30","13:00","13:30","14:00","14:30","15:00","15:30","16:00","16:30","17:00","17:30","18:00","18:30","19:00"]';
        var bookLogin = "open_booking";
        var beforeTime = "1";
        var beforeDay = 0;
        var currDateT = "2021/05/24";
        var checktimebooking = 1;
        var enableRecaptcha = 0;
        var hoursTimeFormat = 12;
        var phoneFormat = "(000) 000-0000";
        var company_phone = "832-968-6668";
        var num_paging = "9";
        var site = "idx";
        var enable_booking = "1";

        let facebook_embed = {
            id_fanpage: "https://www.facebook.com/FastboyMarketingAgency",
            appId: "",
            width: 450,
            height: 300,
            tabs: "timeline",
            show_facepile: true,
            small_header: true,
            likebox_enable: true,
            likebox_show_faces: false,
            likebox_stream: true,
        };
        let google_id_fanpage = "";
        let twitter_id_fanpage = "";

    </script>

    <!-- Webmaster tools -->
    <!--Webmaster tools-->
    <!-- fb retargeting -->

    <!-- Schema Script -->
    <meta name="csrf-token" content="{{ csrf_token() }}" />
</head>

<body>

    <!-- SEO -->
    <div style="height: 0; overflow: hidden;">
        <h1 itemprop="name"></h1>
    </div>
    <div id="fb-root" style="display: none"></div>

    <div class="wrapper">

        <!-- Tpl freeze header -->
        <input type="hidden" name="activeFreezeHeader" value="1" />
        <!-- module 1 -->
        @include('frontend.nail01-d.component.header',
        ['header'=>
        App\Models\Product::where('category_id',
        App\Models\Category::where('name','Header And Footer')->first()['id'])->first()])
        {{-- MAIN --}}
        {{ $slot }}
        {{-- FOOTER --}}
        @php
            $album = App\Models\Album::where([['type', 'Social'], ['status', 'PUBLISHED']])->first();
            $social = App\Models\Asset::where('albums_id', $album['id'])->orderBy('priority', 'desc')->get();
        @endphp
        @include('frontend.nail01-d.component.footer',['header'=>
        App\Models\Product::where('category_id',
        App\Models\Category::where('name','Header And Footer')->first()['id'])->first(),
        'social'=>$social]))
    </div>

    <!-- Check button booking -->
    <script type="text/javascript">
        if (enable_booking == 0) {
            $(".btn_make_appointment").remove();
        }

    </script>

    <!-- JS -->
    <script type="text/javascript" src="{{ asset('frontend') }}/public/library/global/js/consent.js"></script>
    <script type="text/javascript" src="{{ asset('frontend') }}/themes/fnail01d-luxury/assets/js/app.js?v=1.3">
    </script>

    <script type="text/javascript"></script>
    <script>
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

    </script>
    <script type="text/javascript"></script>
    @livewireScripts
</body>

</html>
