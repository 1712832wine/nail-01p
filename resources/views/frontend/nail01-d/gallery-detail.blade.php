<main>
    <div class="pmain">
        <section class="p-list-gallery">
            <!-- tpl list_gallery -->
            <!-- support render gallery_data -->

            <div class="page-heading">
                <div class="container">
                    <h2>Gallery</h2>
                </div>
            </div>
            <div class="in-container">
                <div class="container">
                    <div class="in-content">
                        <div class="gallery-box-wrap">
                            <div class="row">
                                @foreach ($list as $item)
                                    <div class="col-xs-6 col-sm-6 col-md-4">
                                        <div itemprop="url" class="gallery-item gallery-item-popup tt-lightbox"
                                            data-group="gallery-1" title="Nails Design"
                                            href="{{ asset('storage') }}/photos/{{ $item->url }}">
                                            <div class="gallery-box">
                                                <div class="image-bg"
                                                    style="background-image: url('{{ asset('storage') }}/photos/{{ $item->url }}');">
                                                    <img itemprop="image"
                                                        src="{{ asset('storage') }}/photos/{{ $item->url }}"
                                                        alt="Nails Design">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                @endforeach

                            </div>
                        </div>
                        {{ $list->links('frontend.nail01-d.component.custom-pagination') }}
                    </div>
                </div>

            </div>
        </section>
    </div>
</main>
