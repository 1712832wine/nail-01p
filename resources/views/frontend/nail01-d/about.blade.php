<section class="breadcrumbs_area breadcrumb-image section-padding parallex">
    <div class="container">
        <div class="row">
            <div class="col-md-12 text-center">
                <div class="breadcrumbs">
                    <h2 itemprop="name" class="page_title">
                        About us
                    </h2>
                </div>
            </div>
        </div>
    </div>
</section><!-- module 3 -->
<section class="about_us_v1">
    <div class="container">
        @foreach ($articles as $item)
            <div itemscope="" itemtype="http://schema.org/AboutPage" class="row">
                <div class="col-md-7">
                    <h2 itemprop="name">{{ $item['title'] }}</h2>
                    <div itemprop="text">
                        {!! $item['content'] !!}
                    </div>
                </div>
                <div class="col-md-5 imbab">
                    @if (count(json_decode($item['image'])) > 0)
                        <img itemprop="image" class="img-responsive"
                            src="{{ asset('storage') }}/photos/{{ json_decode($item['image'])[0] }}"
                            caption="false" />
                    @endif
                </div>
            </div>
        @endforeach
    </div>
</section> <!-- End Container-->
