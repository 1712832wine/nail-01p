<link rel="stylesheet" type="text/css" href="{{ asset('frontend') }}/themes/fnail01d-luxury/assets/css/service.css">
@php
$features = json_decode($header['features']);
$extras = json_decode($header['extras']);
@endphp
<section class="breadcrumbs_area breadcrumb-image section-padding parallex">
    <div class="container">
        <div class="row">
            <div class="col-md-12 text-center">
                <div class="breadcrumbs">
                    <div class="col-md-6">
                        <h2 class="page_title title_page_service">
                            Our Service
                        </h2>
                    </div>
                    <div class="col-md-6 top-right">
                        <div class="btn_service_defale">
                            <div class="btn_service_book" style="">
                                <a href="/book" class="btn btn-default" title="Make an appointment"><img
                                        style="vertical-align: top;padding-right: 7px"
                                        src="{{ asset('frontend') }}/themes/fnail01d-luxury/assets/img/Forma-11.png"
                                        alt="Forma-11.png">Make an appointment</a>&nbsp;
                                <a href="tel:{{ $features[1]->desc }}" class="btn btn-default btn-call"
                                    title="{{ $features[1]->desc }}"><img
                                        style="vertical-align: top;padding-right: 7px"
                                        src="{{ asset('frontend') }}/themes/fnail01d-luxury/assets/img/Shape-1.png"
                                        alt="Shape-1.png">Call now</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<div class="main-content">
    <div class="container page-container">
        @foreach ($services as $index => $service)
            @if ($index % 2 === 0)
                <div class="row service-row">
                    <div class="col-md-1 hidden-sm hidden-xs"></div>
                    <div class="col-sm-4 col-md-3 text-center">
                        @if (count(json_decode($service['images'])) > 0)
                            <div class="circle-service-image"
                                style="background-image: url('{{ asset('storage') }}/photos/{{ json_decode($service['images'])[0] }}')">
                            </div>
                        @endif
                    </div>
                    <div class="col-sm-8 col-md-7">
                        <h1 id="service_group_{{ $service['id'] }}" class="service-name">{{ $service['name'] }}</h1>
                        <p class="cat_title">{{ $service['description'] }}</p>
                        @foreach (json_decode($service['features']) as $item)
                            @switch($item->name)
                                @case('desc')
                                    <p>{{ $item->desc }}</p>
                                @break
                                @case('center')
                                @break
                                @default
                                    <div class="price-item">
                                        <div class="row">
                                            <div class="col-md-8 col-xs-9">
                                                <h4>{{ $item->name }}</h4>
                                            </div>
                                            <div class="col-md-4 col-xs-3 text-right">
                                                <p class="price-item-number">{{ $item->desc }}</p>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-12">
                                                <p></p>
                                            </div>
                                        </div>
                                    </div>
                            @endswitch
                        @endforeach
                    </div>
                    <div class="col-md-1 hidden-sm hidden-xs"></div>
                </div>
            @else
                <div class="row service-row">
                    <div class="col-md-1 hidden-sm hidden-xs"></div>
                    <div class="col-xs-12 text-center visible-xs">
                        @if (count(json_decode($service['images'])) > 0)
                            <div class="circle-service-image"
                                style="background-image: url('{{ asset('storage') }}/photos/{{ json_decode($service['images'])[0] }}')">
                            </div>
                        @endif
                    </div>
                    <div class="col-sm-8 col-md-7">
                        <h1 id="service_group_{{ $service['id'] }}" class="service-name">{{ $service['name'] }}
                        </h1>
                        <p class="cat_title">{{ $service['description'] }}</p>
                        @foreach (json_decode($service['features']) as $item)
                            @switch($item->name)
                                @case('desc')
                                    <p>{{ $item->desc }}</p>
                                @break
                                @case('center')
                                @break
                                @default
                                    <div class="price-item">
                                        <div class="row">
                                            <div class="col-md-8 col-xs-9">
                                                <h4>{{ $item->name }}</h4>
                                            </div>
                                            <div class="col-md-4 col-xs-3 text-right">
                                                <p class="price-item-number">{{ $item->desc }}</p>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-12">
                                                <p></p>
                                            </div>
                                        </div>
                                    </div>
                            @endswitch
                        @endforeach
                    </div>
                    <div class="col-sm-4 col-md-3 text-center hidden-xs">
                        @if (count(json_decode($service['images'])) > 0)
                            <div class="circle-service-image"
                                style="background-image: url('{{ asset('storage') }}/photos/{{ json_decode($service['images'])[0] }}')">
                            </div>
                        @endif
                    </div>
                    <div class="col-md-1 hidden-sm hidden-xs"></div>
                </div>
            @endif
        @endforeach
    </div>
</div>
<input type="hidden" name="group_id" value="{{ $service_id }}" />
