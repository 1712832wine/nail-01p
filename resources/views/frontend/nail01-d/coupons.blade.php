<section class="breadcrumbs_area breadcrumb-image section-padding parallex">
    <div class="container">
        <div class="row">
            <div class="col-md-12 text-center">
                <div class="breadcrumbs">
                    <h2 itemprop="name" class="page_title">
                        Coupon
                    </h2>
                    <!--<ul itemscope itemtype="http://schema.org/BreadcrumbList" class="">
                        <li itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem">
                            <a itemprop="item" href="/" title="Home" class="active">
                                <span itemprop="name">Home</span>
                                <meta itemprop="position" content="1"/>
                            </a>
                        </li>
                        <li itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem">
                            <span itemprop="item" title="Coupon" class="active">
                                <span itemprop="name">Coupon</span>
                                <meta itemprop="position" content="2"/>
                            </span>
                        </li>
                    </ul>-->
                </div>
            </div>
        </div>
    </div>
</section>
<section class="coupon_page_v1">
    <div class="container">
        <section class="coupon_code_v1">
            <div class="row ">
                <!-- Start list coupons -->
                @foreach ($list as $item)
                    <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                        <div class="coupon_img_v1">
                            <div class="pointer image-magnific-popup" data-group="coupons"
                                href="{{ asset('storage') }}/photos/{{ $item['url'] }}" title="Coupon">
                                <img itemprop="image" class="img-responsive img_size"
                                    src="{{ asset('storage') }}/photos/{{ $item['url'] }}" alt="Coupon">
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>
        </section>
    </div>
</section>
