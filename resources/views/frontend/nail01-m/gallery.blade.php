<section class="home clearfix" id="cms-content-wrapper">
    <div class="main clearfix" id="main">


        <div class="container">
            <div class="in-container">
                <div class="in-content">

                    <div class="list-gallery">

                        <div class="row">
                            @foreach ($lists[0] as $item)
                                <div class="col-6 col-md-3">
                                    <a class="gallery-item" data-group="gallery-1" title="Nails Design"
                                        href="{{ asset('storage') }}/photos/{{ $item['url'] }}" itemprop="image">
                                        <span
                                            style="background-image:url('{{ asset('storage') }}/photos/{{ $item['url'] }}')"></span>
                                    </a>
                                </div>
                            @endforeach
                        </div>
                        <div class="text-center">
                        </div>
                    </div><!-- /.in-content-->
                </div>
            </div>
        </div>
    </div>
</section>
