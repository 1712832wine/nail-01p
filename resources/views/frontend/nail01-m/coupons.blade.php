<section class="home clearfix" id="cms-content-wrapper">
    <div class="main clearfix" id="main">

        <section class="p-coupons">
            <!-- tpl main -->
            <div class="container">
                <div class="in-container">
                    <div class="in-content">
                        @foreach ($list as $item)
                            <div class="col-sm-6 col-md-6 cards-item ">
                                <a class="coupon_img_v1 pointer  image-magnific-popup" data-group=""
                                    href="{{ asset('storage') }}/photos/{{ $item['url'] }}" title="Coupon">
                                    <img class="cards-item-image img-responsive"
                                        src="{{ asset('storage') }}/photos/{{ $item['url'] }}" alt="Coupon">
                                </a>
                            </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </section>
    </div>
</section>
