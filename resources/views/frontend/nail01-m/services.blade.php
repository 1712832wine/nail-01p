@php
$features = json_decode($header['features']);
$extras = json_decode($header['extras']);
@endphp
<section class="home clearfix" id="cms-content-wrapper">
    <div class="main clearfix" id="main">
        <section class="p-service">
            <input type="hidden" name="group_id" value="{{ $service_id }}" />
            <div class="container">
                <div class="in-container-service">
                    <div class="row">
                        <div class="col-sm-12 top-right btn_service_defale" style="padding: 0px 0px;">
                            <div class="btn_service_book"><a href="/book" class="btn btn-primary btn_make_appointment">
                                    Make an appointment</a>&nbsp
                                <a href="tel:{{ $features[1]->desc }}" class="btn btn-primary"><i
                                        class="fa fa-phone"></i>Call
                                    now</a>
                            </div>
                        </div>
                    </div>
                    <div class="service-container">
                        <div class="animation_sroll_jumpto">
                            <div class="sroll_jumpto">
                                <!-- Service -->
                                @foreach ($services as $index => $service)
                                    @if ($index % 2 !== 1)
                                        <div class="row service-row" id="sci_{{ $service['id'] }}">
                                            <div class="col-sm-4 col-md-4 text-center">
                                                @if (count(json_decode($service['images'])) > 0)
                                                    <div class="circle-service-image"
                                                        style="background-image: url('{{ asset('storage') }}/photos/{{ json_decode($service['images'])[0] }}')">
                                                    </div>
                                                @endif
                                            </div>
                                            <div class="col-sm-8 col-md-8">
                                                <h2 class="service-name" id="manicure_pedicure">{{ $service['name'] }}
                                                </h2>
                                                <p></p>
                                                @foreach (json_decode($service['features']) as $item)
                                                    @switch($item->name)
                                                        @case('desc')
                                                            <p>{{ $item->desc }}</p>
                                                        @break
                                                        @case('center')
                                                        @break
                                                        @default
                                                            <div class="detail-price-item">
                                                                <span class="detail-price-name">{{ $item->name }}</span>
                                                                <span class="detail-price-dots"></span>
                                                                <span class="detail-price-number">{{ $item->desc }}</span>
                                                            </div>
                                                            <p></p>
                                                    @endswitch
                                                @endforeach
                                            </div>
                                        </div>
                                    @else
                                        <div class="row service-row" id="sci_{{ $service['id'] }}">
                                            <div class="text-center hidden-lg hidden-md hidden-sm">
                                                @if (count(json_decode($service['images'])) > 0)
                                                    <div class="circle-service-image"
                                                        style="background-image: url('{{ asset('storage') }}/photos/{{ json_decode($service['images'])[0] }}')">
                                                    </div>
                                                @endif
                                            </div>
                                            <div class="col-sm-8 col-md-8  col-xs-12 ">
                                                <h2 class="service-name" id="facial">{{ $service['name'] }}</h2>
                                                <p></p>
                                                @foreach (json_decode($service['features']) as $item)
                                                    @switch($item->name)
                                                        @case('desc')
                                                            <p>{{ $item['desc'] }}</p>
                                                        @break
                                                        @case('center')
                                                        @break
                                                        @default
                                                            <div class="detail-price-item">
                                                                <span class="detail-price-name">{{ $item->name }}</span>
                                                                <span class="detail-price-dots"></span>
                                                                <span class="detail-price-number">{{ $item->desc }}</span>
                                                            </div>
                                                            <p></p>
                                                    @endswitch
                                                @endforeach
                                            </div>
                                        </div>
                                    @endif
                                @endforeach
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
</section>
