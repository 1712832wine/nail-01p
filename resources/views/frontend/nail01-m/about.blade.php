<section class="home clearfix" id="cms-content-wrapper">
    <div class="main clearfix" id="main">

        <section class="p-about">
            <!-- tpl main -->
            <div class="container" id="cms-page-content">
                <div id="primary">
                    <div id="content" role="main">
                        <article class="post-462 page type-page status-publish hentry" id="post-462">
                            <div class="entry-content">

                                {{-- ARTICLES --}}
                                @foreach ($articles as $index => $item)
                                    @if ($index === 0)
                                        {{-- FIRST --}}
                                        <div class="vc_row wpb_row vc_row-fluid vc_custom_1433130460968 vc_row-has-fill vc_general vc_parallax vc_parallax-content-moving"
                                            data-vc-parallax="1.5">
                                            <div class="wpb_column vc_column_container vc_col-sm-12">
                                                <div class="vc_column-inner">
                                                    <div class="wpb_wrapper">
                                                        <div class="vc_custom_heading vc_custom_1431746268006"
                                                            style="font-size: 48px; color: #212121; line-height: 68px; text-align: center; font-family: Playfair Display; font-weight: 400; font-style: italic;">
                                                            {{ $item['title'] }}</div>
                                                        <div
                                                            class="wpb_single_image wpb_content_element vc_align_center wpb_animate_when_almost_visible wpb_appear appear">
                                                            <figure class="wpb_wrapper vc_figure">
                                                                <div class="vc_single_image-wrapper vc_box_border_grey">
                                                                    @if (count(json_decode($item['image'])) > 0)
                                                                        <img alt=""
                                                                            class="vc_single_image-img attachment-thumbnail"
                                                                            height="28"
                                                                            src="{{ asset('storage') }}/photos/{{ json_decode($item['image'])[0] }}"
                                                                            width="134" />
                                                                    @endif
                                                                </div>
                                                            </figure>
                                                        </div>
                                                        <div
                                                            class="wpb_text_column wpb_content_element vc_custom_1436062775955">
                                                            <div class="wpb_wrapper">
                                                                {!! $item['content'] !!}
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    @else
                                        @if ($index % 2 === 0)
                                            <div class="vc_row wpb_row vc_row-fluid vc_custom_1433141501405">
                                                <div class="wpb_column vc_column_container vc_col-sm-12">
                                                    <div class="vc_column-inner">
                                                        <div class="wpb_wrapper">
                                                            <div class="cms-fancyboxes-wraper cms-fancy-box-single cms-fancybox-single-about template-cms_fancybox_single--about content-align-default"
                                                                id="cms-fancy-box-single">
                                                                <div class="cms-fancyboxes-body">
                                                                    <div class="cms-fancybox-item row has-image">
                                                                        <div
                                                                            class="fancy-box-image col-xs-12 col-sm-12 col-md-6 col-lg-6">
                                                                            @if (count(json_decode($item['image'])) > 0)
                                                                                <img src="{{ asset('storage') }}/photos/{{ json_decode($item['image'])[0] }}"
                                                                                    caption="false" />
                                                                            @endif
                                                                        </div>
                                                                        <div
                                                                            class="fancy-box-content-wrap col-xs-12 col-sm-12 col-md-6 col-lg-6">
                                                                            <div class="fancy-box-content-inner">
                                                                                <div class="fancy-box-content-inner2">
                                                                                    <h2>{{ $item['title'] }}</h2>
                                                                                    <div
                                                                                        class="sub-title playfairdisplay">
                                                                                    </div>
                                                                                    <div class="fancy-box-content">
                                                                                        {!! $item['content'] !!}
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        @else
                                            <div class="vc_row wpb_row vc_row-fluid vc_custom_1433141522844">
                                                <div class="wpb_column vc_column_container vc_col-sm-12">
                                                    <div class="vc_column-inner">
                                                        <div class="wpb_wrapper">
                                                            <div class="cms-fancyboxes-wraper cms-fancy-box-single cms-fancybox-single-about template-cms_fancybox_single--about content-align-default"
                                                                id="cms-fancy-box-single-2">
                                                                <div class="cms-fancyboxes-body">
                                                                    <div class="cms-fancybox-item row has-image">
                                                                        <div
                                                                            class="fancy-box-content-wrap col-xs-12 col-sm-12 col-md-6 col-lg-6">
                                                                            <div class="fancy-box-content-inner">
                                                                                <div class="fancy-box-content-inner2">
                                                                                    <h2>{{ $item['title'] }}</h2>
                                                                                    <div
                                                                                        class="sub-title playfairdisplay">
                                                                                    </div>
                                                                                    <div class="fancy-box-content">
                                                                                        {!! $item['content'] !!}
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <div
                                                                            class="fancy-box-image col-xs-12 col-sm-12 col-md-6 col-lg-6">
                                                                            @if (count(json_decode($item['image'])) > 0)
                                                                                <img src="{{ asset('storage') }}/photos/{{ json_decode($item['image'])[0] }}"
                                                                                    caption="false" />
                                                                            @endif
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        @endif
                                    @endif

                                @endforeach
                                {{-- OTHER --}}
                                <div class="vc_row wpb_row vc_row-fluid vc_custom_1433556058407 vc_row-has-fill vc_general vc_parallax vc_parallax-content-moving-fade js-vc_parallax-o-fade"
                                    data-vc-full-width="true" data-vc-full-width-init="false" data-vc-parallax="1.5"
                                    data-vc-parallax-o-fade="on"
                                    style="background-size: cover; background-position: center center;">
                                    <div class="parallax_overlay" style="background-color: rgba(0,0,0,0.4);">
                                    </div>
                                    <div class="wpb_column vc_column_container vc_col-sm-12">
                                        <div class="vc_column-inner">
                                            <div class="wpb_wrapper">
                                                <div class="vc_custom_heading"
                                                    style="font-size: 48px; color: #ffffff; line-height: 68px; text-align: center; font-family: Playfair Display; font-weight: 400; font-style: italic;">
                                                    Relax with your friends by booking our packages</div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="vc_row-full-width vc_clearfix"></div>
                                <div class="vc_row wpb_row vc_row-fluid vc_custom_1433191982014 vc_row-has-fill">
                                    <div class="wpb_column vc_column_container vc_col-sm-12">
                                        <div class="vc_column-inner">
                                            <div class="wpb_wrapper">
                                                <div
                                                    class="wpb_text_column wpb_content_element vc_custom_1433153465148">
                                                    <div class="wpb_wrapper">
                                                        <h1 style="text-align: center;">OUR TEAM</h1>
                                                    </div>
                                                </div>
                                                <div class="vc_custom_heading vc_custom_1433153512419"
                                                    style="font-size: 20px;color: #202020;text-align: center;font-family:Playfair Display;font-weight:400;font-style:italic">
                                                    Meet the cool team member
                                                </div>
                                                <div class="cms-grid-wraper cms-grid-team template-cms_grid--team"
                                                    id="cms-grid">
                                                    <div class="row cms-grid cms-grid">
                                                        <div class="cms-grid-item text-center cms-grid-item col-lg-3 col-md-4 col-sm-6 col-xs-12"
                                                            data-groups='["all","category-our-team"]'>
                                                            <div class="cms-grid-media has-thumbnail"><img alt=""
                                                                    class="attachment-monaco-team size-monaco-team wp-post-image"
                                                                    height="300" sizes="(max-width: 300px) 100vw, 300px"
                                                                    src="images/team11-300x300.jpg" width="300">
                                                            </div>
                                                            <h4 class="cms-grid-title">JAMES DEAN</h4>
                                                            <div class="cms-grid-team-position">
                                                                Founder
                                                            </div>
                                                            <div class="cms-grid-team-social">
                                                                <a href="#" target="_blank"><i
                                                                        class="fa fa-twitter"></i></a> <a href="#"
                                                                    target="_blank"><i class="fa fa-instagram"></i></a>
                                                            </div>
                                                        </div>
                                                        <div class="cms-grid-item text-center cms-grid-item col-lg-3 col-md-4 col-sm-6 col-xs-12"
                                                            data-groups='["all","category-our-team"]'>
                                                            <div class="cms-grid-media has-thumbnail"><img alt=""
                                                                    class="attachment-monaco-team size-monaco-team wp-post-image"
                                                                    height="300" sizes="(max-width: 300px) 100vw, 300px"
                                                                    src="images/team21-300x300.jpg" width="300">
                                                            </div>
                                                            <h4 class="cms-grid-title">EMILIA CLIFF</h4>
                                                            <div class="cms-grid-team-position">
                                                                Sale Manager
                                                            </div>
                                                            <div class="cms-grid-team-social">
                                                                <a href="#" target="_blank"><i
                                                                        class="fa fa-facebook-square"></i></a>
                                                                <a href="#" target="_blank"><i
                                                                        class="fa fa-twitter"></i></a>
                                                            </div>
                                                        </div>
                                                        <div class="cms-grid-item text-center cms-grid-item col-lg-3 col-md-4 col-sm-6 col-xs-12"
                                                            data-groups='["all","category-our-team"]'>
                                                            <div class="cms-grid-media has-thumbnail"><img alt=""
                                                                    class="attachment-monaco-team size-monaco-team wp-post-image"
                                                                    height="300" sizes="(max-width: 300px) 100vw, 300px"
                                                                    src="images/team31-300x300.jpg" width="300">
                                                            </div>
                                                            <h4 class="cms-grid-title">JOHN DOE</h4>
                                                            <div class="cms-grid-team-position">
                                                                Creative Director
                                                            </div>
                                                            <div class="cms-grid-team-social">
                                                                <a href="#" target="_blank"><i
                                                                        class="fa fa-facebook-square"></i></a>
                                                                <a href="#" target="_blank"><i
                                                                        class="fa fa-instagram"></i></a>
                                                            </div>
                                                        </div>
                                                        <div class="cms-grid-item text-center cms-grid-item col-lg-3 col-md-4 col-sm-6 col-xs-12"
                                                            data-groups='["all","category-our-team"]'>
                                                            <div class="cms-grid-media has-thumbnail"><img alt=""
                                                                    class="attachment-monaco-team size-monaco-team wp-post-image"
                                                                    height="300" sizes="(max-width: 300px) 100vw, 300px"
                                                                    src="images/team41-300x300.jpg" width="300">
                                                            </div>
                                                            <h4 class="cms-grid-title">PETER BROWN</h4>
                                                            <div class="cms-grid-team-position">
                                                                Developer
                                                            </div>
                                                            <div class="cms-grid-team-social">
                                                                <a href="#" target="_blank"><i
                                                                        class="fa fa-facebook-square"></i></a>
                                                                <a href="#" target="_blank"><i
                                                                        class="fa fa-twitter"></i></a> <a href="#"
                                                                    target="_blank"><i class="fa fa-instagram"></i></a>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </article>
                    </div>
                </div>
            </div>
        </section>
    </div>
</section>
