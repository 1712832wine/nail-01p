@php
$features = json_decode($header['features']);
$extras = json_decode($header['extras']);
@endphp
<footer class="footer-bottom-layout-8" id="footer-wrapper">
    <div class="footer-wrapper-inner">
        <div class="layout-8" id="cms-footer-bottom">
            <div class="container">
                <div class="row">
                    <div class="footer-social footer-bottom-3 col-xs-12 col-sm-6 col-md-6 col-lg-6">
                        <div class="cms-social">
                            @foreach ($social as $item)
                                @if ($item['url'])
                                    <a itemprop="url" class="social-img-icons" rel="nofollow" target="_blank"
                                        title="{{ $item['title'] }}" href="{{ $item['content'] }}"><img
                                            src="{{ asset('storage') }}/photos/{{ $item['url'] }}"
                                            alt="{{ $item['title'] }}"></a>
                                @endif
                            @endforeach
                        </div>
                        <div class="footer-copyright footer-bottom-2">
                            <div class="cms-copyright">
                                <p>© Just Nail Fnail01m All Rights Reserved. </p>
                            </div>
                        </div>
                    </div>
                    <div class="footer-address footer-bottom-1 col-xs-12 col-sm-6 col-md-6 col-lg-6">
                        <div class="cms-address col-md-12 pull-right">
                            <p class="company_address">{{ $features[0]->desc }}</p>
                            <p>{{ $features[1]->desc }} / {{ $features[2]->desc }}</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</footer><!-- #footer-wrapper -->
