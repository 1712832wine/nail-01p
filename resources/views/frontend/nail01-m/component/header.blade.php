<section class="clearfix" id="cms-header-wrapper">

    <header class="site-header header-v4 no-border-home clearfix" id="masthead" role="banner">
        <div class="cms-header header-v4 no-sticky clearfix" id="cms-header">
            <div class="container">
                <div class="pull-left" id="cms-header-logo">
                    @if (count(json_decode($header['images'])) > 0)
                        <a itemprop="url" href="/">
                            <img class="imgrps"
                                src="{{ asset('storage') }}/photos/{{ json_decode($header['images'])[0] }}"
                                alt="{{ $header['title'] }}" itemprop="logo image">
                        </a>
                    @endif
                </div>
                <div class="cms-nav-extra main-navigation pull-right" id="cms-nav-extra">
                    <div class="pull-left" id="cms-show-mainnav">
                        <ul>
                            <li>
                                <a><i class="fa fa-bars"></i></a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div><!-- #site-navigation -->
    </header><!-- #masthead -->
</section><!-- #cms-header-wrapper -->


<div class="cms-menu" id="cms-mainnav-v4">
    <a id="cms-hide-mainnav"><i class="pe-7s-close"></i></a>
    <div class="cms-mainnav-v4-logo">
        @if (count(json_decode($header['images'])) > 0)
            <a itemprop="url" href="/">
                <img class="imgrps" src="{{ asset('storage') }}/photos/{{ json_decode($header['images'])[0] }}"
                    alt="{{ $header['title'] }}" itemprop="logo image">
            </a>
        @endif
    </div>
    <div class="menu-main-menu-container">
        <ul class="nav-menu" id="menu-main-menu">
            <li class="left" itemprop="name"><a href="/" itemprop="url">Home</a></li>
            <li class="left" itemprop="name"><a href="/about" itemprop="url">About Us</a></li>
            <li class="left" itemprop="name"><a href="/services" itemprop="url">Services</a></li>
            <li class="right" itemprop="name"><a itemprop="url" href="/book">Booking</a></li>
            <li class="right" itemprop="name"><a href="/gallery" itemprop="url">Gallery</a></li>
            <li class="right" itemprop="name"><a href="/giftcards" itemprop="url">Giftcards</a></li>
            <li class="right" itemprop="name"><a href="/coupons" itemprop="url">Coupons</a></li>
            <li class="right" itemprop="name"><a href="/contact" itemprop="url">Contact Us</a></li>
            <!--li class="left dropdown"><a itemprop="url" href="about" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Sup menu <span class="caret"></span></a><ul class="dropdown-menu pull-left"><li><a href="#" itemprop="url">Home</a></li><li><a href="#" itemprop="url">Something else here</a></li><li><a href="#" itemprop="url">Separated link</a></li><li class="dropdown-submenu"><a class="test" href="#" itemprop="url">Another dropdown </a><ul class="dropdown-menu"><li><a href="#" itemprop="url">3rd level dropdown</a></li><li><a href="#" itemprop="url">3rd level dropdown</a></li></ul></li><li><a href="#" itemprop="url">One more separated link</a></li></ul></li-->
        </ul>
    </div>
</div>
