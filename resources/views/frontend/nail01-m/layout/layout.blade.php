<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" dir="ltr" lang="en" xmlns:og="http://ogp.me/ns#"
    xmlns:fb="http://www.facebook.com/2008/fbml" itemscope itemtype="https://schema.org/NailSalon">

<head>
    <!-- Language -->
    <meta http-equiv="content-language" content="en" />
    <link rel="alternate" href="http://fnail01m.fastboymedia.info" hreflang="x-default">
    <link rel="alternate" href="http://fnail01m.fastboymedia.info" hreflang="en-US">

    <!-- Meta limit -->

    <!-- Meta Tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width,initial-scale=1.0" />
    <meta http-equiv="content-type" content="text/html; charset=UTF-8" />
    <meta name="description" content="" />
    <meta name="keywords" content="" />
    <meta name="author" content="" />

    <!-- OG -->
    <meta property="og:title" content="" />
    <meta property="og:description" content="" />
    <meta property="og:image" content="" />
    <meta property="og:url" content="http://fnail01m.fastboymedia.info/" />
    <meta property="og:type" content="website" />
    <meta property="og:site_name" content="Just Nail Fnail01m" />

    <!-- Dublin Core -->
    <link rel="schema.DC" href="http://purl.org/dc/elements/1.1/">
    <meta name="DC.title" content="">
    <meta name="DC.identifier" content="http://fnail01m.fastboymedia.info/">
    <meta name="DC.description" content="">
    <meta name="DC.subject" content="">
    <meta name="DC.language" scheme="UTF-8" content="en-us">
    <meta itemprop="priceRange" name="priceRange" content="">

    <!-- GEO meta -->
    <meta name="geo.region" content="">
    <meta name="geo.placename" content="">
    <meta name="geo.position" content="">
    <meta name="ICBM" content="">

    <!-- Page Title -->
    <title>Just Nail Fnail01m</title>
    <base href="/themes/fnail01m/assets/">

    <!-- canonical -->
    <link rel="canonical" href="http://fnail01m.fastboymedia.info">



    <!-- Favicons -->
    <link rel="icon" href="http://fnail01m.fastboymedia.info/uploads/fnail0axbqjvh/attach/1562752421_fbm_fvc.png"
        type="image/x-icon">
    <link rel="shortcut icon"
        href="http://fnail01m.fastboymedia.info/uploads/fnail0axbqjvh/attach/1562752421_fbm_fvc.png"
        type="image/x-icon">

    <link rel="stylesheet" type="text/css" href='{{ asset('frontend') }}/public/library/jquery-ui/jquery-ui.min.css'>
    <link rel="stylesheet" type="text/css" href='{{ asset('frontend') }}/public/library/animate/animate.css'>
    <link rel="stylesheet" type="text/css" href='{{ asset('frontend') }}/public/library/validation/validation.css'>
    <link rel="stylesheet" type="text/css" href='{{ asset('frontend') }}/public/library/flex-label/flex-label.css'>
    <link rel="stylesheet" type="text/css"
        href='{{ asset('frontend') }}/public/library/bootstrap/v3.3.7/bootstrap.min.css'>
    <link rel="stylesheet" type="text/css"
        href='{{ asset('frontend') }}/public/library/bootstrap-datetimepicker/bootstrap-datetimepicker.min.css'>
    <link rel="stylesheet" type="text/css"
        href='{{ asset('frontend') }}/public/library/font-awesome/v4.7.0/css/font-awesome.min.css'>
    <link rel="stylesheet" type="text/css"
        href='{{ asset('frontend') }}/public/library/slider-pro/slider-pro.min.css'>
    <link rel="stylesheet" type="text/css" href='{{ asset('frontend') }}/public/library/meanmenu/meanmenu.css'>
    <link rel="stylesheet" type="text/css"
        href='{{ asset('frontend') }}/public/library/owl-carousel/owl.carousel.min.css'>
    <link rel="stylesheet" type="text/css"
        href='{{ asset('frontend') }}/public/library/magnific-popup/magnific-popup.css'>
    <link rel="stylesheet" type="text/css" href='{{ asset('frontend') }}/public/library/pnotify/pnotify.css'>
    <link rel="stylesheet" type="text/css" href='{{ asset('frontend') }}/public/library/instagram/instafeed.min.css'>
    <link rel="stylesheet" type="text/css" href='{{ asset('frontend') }}/public/library/signature/signature.css'>
    <link rel="stylesheet" type="text/css"
        href='{{ asset('frontend') }}/themes/fnail01m/assets/css/pe-icon-7-stroke.css'>
    <link rel="stylesheet" type="text/css"
        href='{{ asset('frontend') }}/themes/fnail01m/assets/css/bootstrap-progressbar.min.css'>
    <link rel="stylesheet" type="text/css"
        href='{{ asset('frontend') }}/themes/fnail01m/assets/css/mediaelementplayer-legacy.min.css'>
    <link rel="stylesheet" type="text/css"
        href='{{ asset('frontend') }}/themes/fnail01m/assets/css/wp-mediaelement.min.css'>
    <link rel="stylesheet" type="text/css" href='{{ asset('frontend') }}/themes/fnail01m/assets/css/static.css'>
    <link rel="stylesheet" type="text/css"
        href='{{ asset('frontend') }}/themes/fnail01m/assets/css/js_composer.min.css'>
    <link rel="stylesheet" type="text/css"
        href='{{ asset('frontend') }}/themes/fnail01m/assets/css/prettyPhoto.min.css'>
    <link rel="stylesheet" type="text/css" href='{{ asset('frontend') }}/themes/fnail01m/assets/css/style.css'>
    <style type="text/css">
        .box_img_giftcard.active,
        .box_img_giftcard:hover {
            border-color: free_theme_color_1;
        }

        #masthead .main-navigation>div ul:first-child>li>a:after {
            border-bottom: 2px solid free_theme_color_1;
        }

        .circle-service-image {
            border: 3px solid free_theme_color_1;
        }

        .btn-primary,
        .tnp-widget input.tnp-submit,
        .pagination>li.active>a.hover-main-color,
        .pagination>li.active>span.hover-main-color,
        .pagination>li>a:hover.hover-main-color,
        .pagination>li>span:hover.hover-main-color,
        .cms-fancy-box-single:hover .fancy-box-icon-inner,
        input#submit:hover {
            background: free_theme_color_1;
            color: #fff;
            border: 1px solid free_theme_color_1;
        }

        #masthead .main-navigation>div ul:first-child>li.current_page_item>a:after,
        #masthead .main-navigation>div ul:first-child>li.current-menu-ancestor>a:after,
        #masthead .main-navigation>div ul:first-child>li:hover>a:after,
        #masthead .main-navigation>div ul:first-child>li:focus>a:after,
        #masthead .main-navigation>div ul:first-child>li:active>a:after,
        select:hover,
        .select2.select2-container:hover,
        select:active,
        .select2.select2-container:active,
        select:focus,
        .select2.select2-container:focus,
        form input:hover,
        .form input:hover,
        form input:active,
        .form input:active,
        form input:focus,
        .form input:focus,

        .woocommerce #respond input#submit,
        .woocommerce a.button,
        .woocommerce button.button,
        .woocommerce input.button,
        a.added_to_cart,
        a.added,
        .btn:hover,
        .btn-default:hover,
        form textarea:hover,
        .form textarea:hover,
        form textarea:active,
        .form textarea:active,
        form textarea:focus,
        .form textarea:focus,
        ul.cms-filter-category li a:hover,
        ul.cms-filter-category li a.active,
        .cms-grid-wraper.cms-grid-team .cms-grid-item:hover .cms-grid-media:after,
        .cms-grid-wraper.cms-grid-team .cms-grid-item:active .cms-grid-media:after,
        .cms-grid-wraper.cms-grid-team .cms-grid-item:focus .cms-grid-media:after {
            border-color: free_theme_color_1;
        }

        .mean-container a.meanmenu-reveal>span,
        .mean-container a.meanmenu-reveal>span:before,
        .mean-container a.meanmenu-reveal>span:after,
        input[type='submit'].btn-primary.btn,
        html body .overlay .overlay-content a.icon {
            background: free_theme_color_1;
        }

        #wpbSlider .sp-button:hover,
        #wpbSlider .sp-button.sp-selected-button,
        .btn:hover,
        .btn-default:hover,
        .bootstrap-datetimepicker-widget table td.active,
        .bootstrap-datetimepicker-widget table td.active:hover {
            background-color: free_theme_color_1;
        }

        .btn-primary:hover,
        .tnp-widget input.tnp-submit:hover,
        input[type='submit']:hover,
        input[type='submit']:focus,
        .cms-fancy-box-single:hover .fancy-box-icon-inner {
            background: free_theme_color_2;
            border-color: free_theme_color_2;
        }

        input#submit:hover,
        .progress .progress-bar {
            background-color: free_theme_color_1;
        }

        #cms-show-mainnav ul li a .fa.fa-bars:hover,
        .service-row h2,
        .service-name {
            color: free_theme_color_1;
        }

        .box_account_v1 .modal_form_header h4,
        .box_account_v1 .modal_form_header,
        .pagination>.active>a,
        .pagination>.active>a:focus,
        .pagination>.active>a:hover,
        .pagination>.active>span,
        .pagination>.active>span:focus,
        .pagination>.active>span:hover {
            color: #fff;
            background-color: free_theme_color_1;
            border-color: free_theme_color_1;
        }

    </style>
    <link rel="stylesheet" type="text/css" href='{{ asset('frontend') }}/public/library/global/css/news.css'>
    <link rel="stylesheet" type="text/css" href='{{ asset('frontend') }}/public/library/global/css/consent.css'>
    <link rel="stylesheet" type="text/css"
        href='{{ asset('frontend') }}/themes/fnail01m/assets/css/custom.css?v=1.0'>
    <style type="text/css">
        @media (max-width: 991px) {
            #cms-page:not(.header-v1) #cms-header-logo {
                height: 100px;
            }

            #cms-page:not(.header-v1) #cms-header-logo {
                max-width: calc(100% - 50px);
                display: flex;
                align-items: center;
            }

            #masthead #cms-header #cms-header-logo a {
                display: contents;
            }

            #cms-page:not(.header-v1) #cms-header-logo a img {
                height: auto;
                max-width: 100%;
                max-height: 100%;
            }
        }

        @media (max-width: 687px) {
            .company_address br {
                display: block;
            }
        }

        .circle-service-image .text-center {
            width: 180px;
            height: 180px;
            border-radius: 125px;
            display: inline-block;
            margin-top: 20px;
            background-repeat: no-repeat;
            background-attachment: scroll;
            background-clip: border-box;
            background-origin: padding-box;
            background-position-x: center;
            background-position-y: center;
            background-size: cover;
        }

        .wpb_button,
        .wpb_content_element,
        ul.wpb_thumbnails-fluid>li {
            margin-bottom: 5px;
        }

        .vt-time .col-2 {
            padding: 3px;
            position: relative;
            padding-left: 90px;
        }

        .vt-time .col-2 .col-lef {
            position: absolute;
            left: 0px;
            font-weight: 600;
        }

    </style>

    <!-- Go to www.addthis.com/dashboard to customize your tools -->
    <script type="text/javascript"
        src="{{ asset('frontend') }}//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-59b8fec5dd14e0f2"></script>
    <script type="text/javascript">
        var addthis_share = {
            url: "http://fnail01m.fastboymedia.info/",
            title: "Just Nail Fnail01m",
            description: "",
            media: ""
        }

    </script>
    <!-- Script -->





    <script type="text/javascript" src="{{ asset('frontend') }}/public/library/jquery/v1.10.2/jquery.min.js">
    </script>
    <script type="text/javascript" src="{{ asset('frontend') }}/public/library/jquery-ui/jquery-ui.min.js"></script>
    <script type="text/javascript" src="{{ asset('frontend') }}/public/library/jquery-ui/jquery-migrate.min.js">
    </script>
    <script type="text/javascript" src="{{ asset('frontend') }}/public/library/validation/validation.min.js">
    </script>
    <script type="text/javascript" src="{{ asset('frontend') }}/public/library/mask/mask.min.js"></script>
    <script type="text/javascript" src="{{ asset('frontend') }}/public/library/flex-label/flex-label.js"></script>
    <script type="text/javascript" src="{{ asset('frontend') }}/public/library/bootstrap/v3.3.7/bootstrap.min.js">
    </script>
    <script type="text/javascript"
        src="{{ asset('frontend') }}/public/library/waiting-modal-dialog/waiting-modal-dialog.js"></script>
    <script type="text/javascript" src="{{ asset('frontend') }}/public/library/moment/moment-with-locales.js">
    </script>
    <script type="text/javascript"
        src="{{ asset('frontend') }}/public/library/bootstrap-datetimepicker/bootstrap-datetimepicker.js"></script>
    <script type="text/javascript" src="{{ asset('frontend') }}/public/library/magnific-popup/magnific-popup.min.js">
    </script>
    <script type="text/javascript" src="{{ asset('frontend') }}/public/library/pnotify/pnotify.js"></script>
    <script type="text/javascript" src="{{ asset('frontend') }}/public/library/slider-pro/slider-pro.min.js">
    </script>
    <script type="text/javascript" src="{{ asset('frontend') }}/public/library/owl-carousel/owl.carousel.min.js">
    </script>
    <script type="text/javascript" src="{{ asset('frontend') }}/public/library/wow/wow.min.js"></script>
    <script type="text/javascript" src="{{ asset('frontend') }}/public/library/meanmenu/meanmenu.js"></script>
    <script type="text/javascript" src="{{ asset('frontend') }}/public/library/signature/signature.min.js"></script>
    <script type="text/javascript" src="{{ asset('frontend') }}/public/library/instagram/instafeed.min.js"></script>
    <script type="text/javascript" src="{{ asset('frontend') }}/public/library/pkgd/imagesloaded.pkgd.min.js">
    </script>
    <script type="text/javascript" src="{{ asset('frontend') }}/public/library/pkgd/isotope.pkgd.min.js"></script>
    <script type="text/javascript" src="{{ asset('frontend') }}/public/library/waypoints/waypoints.min.js"></script>
    <script type="text/javascript" src="{{ asset('frontend') }}/themes/fnail01m/assets/js/js.cookie.min.js"></script>
    <script type="text/javascript" src="{{ asset('frontend') }}/themes/fnail01m/assets/js/comment-reply.min.js">
    </script>
    <script type="text/javascript" src="{{ asset('frontend') }}/themes/fnail01m/assets/js/jquery.prettyPhoto.min.js">
    </script>
    <script type="text/javascript" src="{{ asset('frontend') }}/themes/fnail01m/assets/js/wp-embed.min.js"></script>
    <script type="text/javascript" src="{{ asset('frontend') }}/themes/fnail01m/assets/js/js_composer_front.min.js">
    </script>
    <script type="text/javascript" src="{{ asset('frontend') }}/themes/fnail01m/assets/js/skrollr.min.js"></script>
    <script type="text/javascript"
        src="{{ asset('frontend') }}/themes/fnail01m/assets/js/bootstrap-progressbar.min.js"></script>
    <script type="text/javascript" src="{{ asset('frontend') }}/themes/fnail01m/assets/js/counter.min.js"></script>
    <script type="text/javascript" src="{{ asset('frontend') }}/themes/fnail01m/assets/js/menu.js"></script>
    <script type="text/javascript" src="{{ asset('frontend') }}/themes/fnail01m/assets/js/owl.carousel.cms.js?v=1.0">
    </script>
    <script type="text/javascript" src="{{ asset('frontend') }}/themes/fnail01m/assets/js/main.js"></script>
    <script type="text/javascript" src="{{ asset('frontend') }}/public/library/global/js/js_php.js"></script>

    <script type="text/javascript">
        var dateFormatBooking = "MM/DD/YYYY";
        var posFormat = "1,0,2";
        var timeMorning = '["10:00","10:30","11:00","11:30"]';
        var timeAfternoon =
            '["12:00","12:30","13:00","13:30","14:00","14:30","15:00","15:30","16:00","16:30","17:00","17:30","18:00","18:30","19:00"]';
        var bookLogin = "open_booking";
        var beforeTime = "1";
        var beforeDay = 0;
        var currDateT = "2021/05/24";
        var checktimebooking = 1;
        var enableRecaptcha = 0;
        var hoursTimeFormat = 12;
        var phoneFormat = "(000) 000-0000";
        var company_phone = "832-968-6668";
        var num_paging = "12";
        var site = "idx";

        let facebook_embed = {
            id_fanpage: "https://www.facebook.com/FastboyMarketingAgency",
            appId: "",
            width: 450,
            height: 300,
            tabs: "timeline",
            show_facepile: true,
            small_header: true,
            likebox_enable: true,
            likebox_show_faces: false,
            likebox_stream: true,
        };
        let google_id_fanpage = "";
        let twitter_id_fanpage = "";

    </script>

    <!-- Webmaster tools -->
    <!--Webmaster tools-->
    <!-- fb retargeting -->

    <!-- Schema Script -->
    <meta name="csrf-token" content="{{ csrf_token() }}" />
</head>

<body class="home">
    <div class="cs-wide header-v4 no-border-home clearfix" id="cms-page">

        <!-- Facebook Root And H1 Seo -->
        <h1 style="display: none"></h1>
        <div id="fb-root" style="display: none"></div>

        {{-- HEADER --}}
        @include('frontend.nail01-m.component.header',
        ['header'=>
        App\Models\Product::where('category_id',
        App\Models\Category::where('name','Header And Footer')->first()['id'])->first()])
        {{-- MAIN --}}
        {{ $slot }}
        {{-- FOOTER --}}
        @php
            $album = App\Models\Album::where([['type', 'Social'], ['status', 'PUBLISHED']])->first();
            $social = App\Models\Asset::where('albums_id', $album['id'])->orderBy('priority', 'desc')->get();
        @endphp
        @include('frontend.nail01-m.component.footer',['header'=>
        App\Models\Product::where('category_id',
        App\Models\Category::where('name','Header And Footer')->first()['id'])->first(),
        'social'=>$social]))
        <!-- popup -->

        <!-- external javascripts -->
        <script type="text/javascript" src="{{ asset('frontend') }}/public/library/global/js/consent.js"></script>
        <script type="text/javascript" src="{{ asset('frontend') }}/themes/fnail01m/assets/js/app.js?v=1.3"></script>

        <!-- Google analytics -->
        <script>
            (function(i, s, o, g, r, a, m) {
                i['GoogleAnalyticsObject'] = r;
                i[r] = i[r] || function() {
                    (i[r].q = i[r].q || []).push(arguments)
                }, i[r].l = 1 * new Date();
                a = s.createElement(o),
                    m = s.getElementsByTagName(o)[0];
                a.async = 1;
                a.src = g;
                m.parentNode.insertBefore(a, m)
            })(window, document, 'script', 'https://www.google-analytics.com/analytics.js', 'ga');
            ga('create', '', 'auto');
            ga('send', 'pageview');

        </script>

        <!-- gg adwords remarketing -->

        <!-- JS extends -->
        <script>
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

        </script>
        <script type="text/javascript"></script>
        @livewireScripts
    </div>
</body>

</html>
