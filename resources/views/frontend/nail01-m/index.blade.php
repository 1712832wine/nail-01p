<section class="home clearfix" id="cms-content-wrapper">
    <div class="main clearfix" id="main">

        <div class="container-fluid" id="cms-page-content">
            <div id="primary">
                <div id="content" role="main">
                    <article class="post-366 page type-page status-publish hentry" id="post-366">
                        <div class="entry-content">
                            <div class="vc_row wpb_row vc_row-fluid vc_row-no-padding" data-vc-full-width="true"
                                data-vc-full-width-init="false" data-vc-stretch-content="true">
                                <div class="wpb_column vc_column_container vc_col-sm-12">
                                    <div class="vc_column-inner">
                                        <div class="wpb_wrapper">
                                            <div class="cms-carousel template-cms_carousel--portfolio"
                                                id="cms-carousel">
                                                @foreach ($services as $service)
                                                    <div class="cms-carousel-item overlay-wrap">
                                                        <div class="cms-grid-media has-thumbnail">
                                                            @if (count(json_decode($service['images'])) > 0)
                                                                <img itemprop="image"
                                                                    src="{{ asset('storage') }}/photos/{{ json_decode($service['images'])[0] }}"
                                                                    class="attachment-blog-grid size-blog-grid wp-post-image"
                                                                    alt="{{ $service['name'] }}">
                                                            @endif
                                                        </div>
                                                        <div class="overlay">
                                                            <a itemprop="url"
                                                                href="{{ route('services-detail', ['slug' => strtolower(trim(preg_replace('/[^A-Za-z0-9-]+/', '-', $service['name']))), 'service_id' => $service['id']]) }}"
                                                                title="{{ $service['name'] }}">
                                                                <div class="overlay-content text-center color-white">
                                                                    <div class="cms-carousel-title">
                                                                        <h4 itemprop="name"> {{ $service['name'] }}
                                                                        </h4>
                                                                    </div>
                                                                </div>
                                                            </a>
                                                        </div>
                                                    </div>
                                                @endforeach
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="vc_row-full-width vc_clearfix"></div>
                        </div>
                    </article>
                </div>
            </div>
        </div>
        <style>
            #masthead {
                border-bottom: 0px solid #eeeeee;
            }

            footer#footer-wrapper.footer-bottom-layout-8 {
                border-top: 0px solid #eee;
            }

            #footer-wrapper {
                margin-top: 20px;
            }

        </style>
    </div>
</section>
