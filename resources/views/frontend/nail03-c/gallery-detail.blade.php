<section class="p-list-gallery">
    <!-- tpl list_gallery -->
    <!-- support render gallery_data -->
    <!-- module 2 -->
    <div class="banner-page-list">
        <div class="container">
            <div class="title-page">
                <div class="ac">
                    <h2><a href="/gallery"> Gallery</a></h2>
                    <div class="breakcrumb">
                        <a href="">Home</a>
                        /
                        <p>Gallery</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <section class="page-section plg-gallery" style="padding-top: 30px;">
        <div class="container">
            <div class="gallery-box-wrap">
                <div class="row">
                    @foreach ($list as $item)
                        <div class="col-xs-6 col-sm-6 col-md-4">
                            <a itemprop="url" class="gallery-item gallery-item-popup gallery-item magnific"
                                data-group="gallery-1" title="Nails Design"
                                href="{{ asset('storage') }}/photos/{{ $item->url }}">
                                <div class="gallery-box">
                                    <div class="image-bg"
                                        style="background-image: url('{{ asset('storage') }}/photos/{{ $item->url }}');">
                                        <img itemprop="image" src="{{ asset('storage') }}/photos/{{ $item->url }}"
                                            alt="Nails Design">
                                    </div>
                                </div>
                            </a>
                        </div>
                    @endforeach
                </div>
            </div>
            {{ $list->links('frontend.nail03-c.component.custom-pagination') }}
        </div>
    </section>
</section>
