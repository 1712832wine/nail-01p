<div class="banner-page-list">
    <div class="container">
        <div class="title-page">
            <div class="ac">
                <h2 class="lost-child">
                    <a href="/coupons" title="Coupon">Coupon</a>
                </h2>
                <div class="breakcrumb">
                    <a href="/" title="Home">Home</a>
                    <span>/</span> <a href="/coupons" title="Coupon">Coupon</a>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- module 3 -->
<section class="coupon_page_v1">
    <div class="container">
        <section class="coupon_code_v1">
            <div class="row ">
                @foreach ($list as $item)
                    <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                        <div class="coupon_img_v1">
                            <a title="Coupon 2" href="{{ asset('storage') }}/photos/{{ $item['url'] }}"
                                class="magnific">
                                <img class="img-responsive img_size"
                                    src="{{ asset('storage') }}/photos/{{ $item['url'] }}" alt="Coupon 2">
                            </a>
                        </div>
                    </div>
                @endforeach
            </div>
        </section>
    </div>
</section>
