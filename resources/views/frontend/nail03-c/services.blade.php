<div class="banner-page-list">
    <div class="container">
        <div class="title-page">
            <div class="row">
                <div class="col-md-2"></div>
                <div class="col-md-6">
                    <h2 class="lost-child">
                        <a href="/services" title="Service">Our Service</a>
                    </h2>
                </div>
                <div class="col-md-4 top-right">
                    <div class="btn_service_defale">
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="main-content animation_sroll_jumpto">
    <div class="container page-container">
        <input type="hidden" name="group_id" value="{{ $service_id }}" />
        @foreach ($services as $index => $service)
            @if ($index % 2 === 0)
                <div class="row service-row sroll_jumpto" id="sci_{{ $service['id'] }}">
                    <div class="col-sm-4 col-md-4 text-center">
                        @if (count(json_decode($service['images'])) > 0)
                            <div class="circle-service-image"
                                style="background-image: url('{{ asset('storage') }}/photos/{{ json_decode($service['images'])[0] }}')">
                            </div>
                        @endif
                    </div>
                    <div class="col-sm-8 col-md-8">
                        <h2 id="artificial_nails" class="service-name">{{ $service['name'] }}</h2>
                        <p class="cat_title">{{ $service['description'] }}</p>
                        @foreach (json_decode($service['features']) as $item)
                            @switch($item->name)
                                @case('desc')
                                    <p>{{ $item->desc }}</p>
                                @break
                                @case('center')
                                @break
                                @default
                                    <div class="box_service">
                                        <div class="detail-price-item">
                                            <span class="detail-price-name">{{ $item->name }}</span>
                                            <span class="detail-price-dots"></span>
                                            <span class="detail-price-number">{{ $item->desc }}</span>
                                        </div>
                                        <p></p>
                                    </div>
                            @endswitch
                        @endforeach
                    </div>
                </div>
            @else
                <div class="row service-row sroll_jumpto" id="sci_{{ $service['id'] }}">
                    <div class="col-sm-4 col-md-4 text-center hidden-lg hidden-md">
                        @if (count(json_decode($service['images'])) > 0)
                            <div class="circle-service-image"
                                style="background-image: url('{{ asset('storage') }}/photos/{{ json_decode($service['images'])[0] }}')">
                            </div>
                        @endif
                    </div>
                    <div class="col-sm-8 col-md-8">
                        <h2 id="natural_nails" class="service-name">NATURAL NAILS</h2>
                        <p class="cat_title"></p>
                        @foreach (json_decode($service['features']) as $item)
                            @switch($item->name)
                                @case('desc')
                                    <p>{{ $item['desc'] }}</p>
                                @break
                                @case('center')
                                @break
                                @default
                                    <div class="box_service">
                                        <div class="detail-price-item">
                                            <span class="detail-price-name">{{ $item->name }}</span>
                                            <span class="detail-price-dots"></span>
                                            <span class="detail-price-number">{{ $item->desc }}</span>
                                        </div>
                                        <p></p>
                                    </div>
                            @endswitch
                        @endforeach
                    </div>
                    <div class="col-sm-4 col-md-4 text-center hidden-sm hidden-xs">
                        @if (count(json_decode($service['images'])) > 0)
                            <div class="circle-service-image"
                                style="background-image: url('{{ asset('storage') }}/photos/{{ json_decode($service['images'])[0] }}')">
                            </div>
                        @endif
                    </div>
                </div>
            @endif
        @endforeach
    </div>
</div>
