<div class="banner-page-list">
    <div class="container">
        <div class="title-page">
            <div class="ac">
                <h2><a href="/gallery"> Gallery</a></h2>
                <div class="breakcrumb">
                    <a href="">Home</a>
                    /
                    <p>Gallery</p>
                </div>
            </div>
        </div>
    </div>
</div> <!-- CONTAINER -->
<div class="main-content" style="padding-top: 30px;">
    <div class="container">
        <div class="in-container">
            <div class="in-content list-gallery">
                <div class="gallery-box-wrap">
                    <div class="row">
                        <!-- Custome Height with .gallery-box .image-bg{padding-bottom: 75%;} -->
                        <!-- List category -->
                        @foreach ($albums as $index => $album)
                            <div class="col-xs-6 col-sm-6 col-md-4">
                                <a itemprop="url" data-group="gallery-1" title="Nails Design"
                                    href="/gallery-detail/{{ $album['id'] }}">
                                    <div class="gallery-box">
                                        <div class="image-bg"
                                            style="background-image: url('{{ asset('storage') }}/photos/{{ $lists[$index][0]['url'] }}');">
                                            <img itemprop="image"
                                                src="{{ asset('storage') }}/photos/{{ $lists[$index][0]['url'] }}">
                                        </div>
                                        <div class="gallery-title">
                                            <span itemprop="name">{{ $album['name'] }}</span>
                                        </div>
                                    </div>
                                </a>
                            </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </div><!-- .in-container -->
    </div>
</div>
