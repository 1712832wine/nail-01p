<div class="main-content">
    <div class="section-slider-wrap">
        <section class="section-slider">
            <div id="slider-option" data-autoplay="true" data-autoplayDelay="5000"></div>
            {{-- CAROUSEL --}}
            <div class="slider-pro" id="my-slider" style="display: none;">
                <div class="sp-slides">
                    @foreach ($imgs_carousel as $img)
                        <div class="sp-slide">
                            <div class="sp-layer sp-static">
                                <a href="/">
                                    <img class="sp-image" src="{{ asset('storage') }}/photos/{{ $img['url'] }}"
                                        alt="" />
                                </a>
                            </div>
                        </div>
                    @endforeach
                </div>
            </div>
            <div class="slider-pro" id="my-slider-fixed-height" style="display: none;">
                <div class="sp-slides">
                    @foreach ($imgs_carousel as $img)
                        <div class="sp-slide">
                            <a href="/" target="_self" title="dreamstime_l_22262345.jpg">
                                <div class="sp-layer sp-static" data-width="100%" data-height="100%"
                                    style="width: 100%;height: 100%;background: url('{{ asset('storage') }}/photos/{{ $img['url'] }}') center center no-repeat;background-size: cover;">
                                </div>
                            </a>
                        </div>
                    @endforeach
                </div>
            </div>
        </section>
    </div>
    {{-- SERVICES --}}
    <div class="our-services">
        <div class="container">
            <div class="title-standard center">
                <h2 itemprop="name"> OUR SERVICES</h2>
            </div>
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="ctn-our-services">
                        <div class="owl-services-index owl-theme">
                            @foreach ($services as $service)
                                <div class="item-our-services">
                                    <div class="img-our-services">
                                        <img itemprop="image"
                                            src="{{ asset('storage') }}/photos/{{ json_decode($service['images'])[0] }}"
                                            alt="{{ $service['name'] }}">
                                    </div>
                                    <div class="title-our-services">
                                        <div class="vertical">
                                            <h2 itemprop="name"><a
                                                    href="{{ route('services-detail', ['slug' => strtolower(trim(preg_replace('/[^A-Za-z0-9-]+/', '-', $service['name']))), 'service_id' => $service['id']]) }}"
                                                    title="{{ $service['name'] }}">{{ $service['name'] }}</a></h2>
                                            <a itemprop="url"
                                                href="{{ route('services-detail', ['slug' => strtolower(trim(preg_replace('/[^A-Za-z0-9-]+/', '-', $service['name']))), 'service_id' => $service['id']]) }}"
                                                title="{{ $service['name'] }}" class="choice-services">View More</a>
                                        </div>
                                    </div>
                                </div>
                            @endforeach
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    {{-- GALLERY --}}
    <div class="gallery_home clearfix">
        <div class="container">
            <div class="row">
                <div class="col-md-6">
                    <div class="title-standard center">
                        <h2 itemprop="name">OUR GALLERY</h2>
                    </div>
                    <div class="list-gallery">
                        <div class="row box_list_gallery">
                            @foreach ($gallery_list as $item)
                                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                                    <a class="gallery-item magnific" title="Nails Design"
                                        href="{{ asset('storage') }}/photos/{{ $item['url'] }}">
                                        <span
                                            style="background-image:url('{{ asset('storage') }}/photos/{{ $item['url'] }}')"></span>
                                    </a>
                                </div>
                            @endforeach
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="title-standard center">
                        <h2 itemprop="name">OUR VIDEO</h2>
                        <div class="list-gallery"><iframe width="300" height="150" title="OUR VIDEO"
                                src="{{ $home_article['description'] }}" frameborder="0"
                                allowfullscreen="allowfullscreen"></iframe></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
