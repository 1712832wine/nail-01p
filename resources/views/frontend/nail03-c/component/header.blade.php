@php
$features = json_decode($header['features']);
$extras = json_decode($header['extras']);
@endphp
<div class="header-container">
    <div class="layout-header-1 pad-vertical-5 row background-f4 hi">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="welcome-text">
                        <p itemprop="description">
                            Leave your stressful work behind and enjoy happy time with us!
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="header-mobile clearfix hidden-md hidden-lg">
        <div class="wrap-freeze-header-mobile clearfix">
            <div class="flag-freeze-header-mobile">
                <div class="container">
                    <div class="box-color col-lg-12 clearfix">
                        <div class="layout-header-3 pad-vertical-10 row">
                            <div class="logo">
                                @if (count(json_decode($header['images'])) > 0)
                                    <a itemprop="url" href="/">
                                        <img class="imgrps"
                                            src="{{ asset('storage') }}/photos/{{ json_decode($header['images'])[0] }}"
                                            alt="{{ $header['title'] }}" itemprop="logo image">
                                    </a>
                                @endif
                            </div>
                            <div class="address-top-page">
                                <div class="address-top-box-1">
                                    <div class="icon-top-box-1">
                                        <img itemprop="image" src="images/opened-email.png" alt="opened-email.png">
                                    </div>
                                    <div class="title-top-box-1">
                                        <h2 itemprop="name">EMAIL</h2>
                                        <a href="mailto:{{ $features[2]->desc }}" title="{{ $features[2]->desc }}"
                                            target="_top" itemprop="email">{{ $features[2]->desc }}</a>
                                    </div>
                                </div>
                                <div class="address-top-box-1">
                                    <div class="icon-top-box-1">
                                        <img itemprop="image" src="images/opened-map.png" alt="opened-map.png">
                                    </div>
                                    <div class="title-top-box-1">
                                        <h2>Find Us</h2>
                                        <p itemprop="address">{{ $features[0]->desc }}
                                        </p>
                                    </div>
                                </div>
                                <div class="address-top-box-1">
                                    <div class="icon-top-box-1">
                                        <img itemprop="image"
                                            src="{{ asset('frontend') }}/themes/fnail03c/assets/images/opened-phone.png"
                                            alt="opened-phone.png">
                                    </div>
                                    <div class="title-top-box-1">
                                        <h2>Call Now</h2>
                                        <a itemprop="url" href="tel:{{ $features[1]->desc }}"
                                            title="{{ $features[1]->desc }}"><span
                                                itemprop="telephone">{{ $features[1]->desc }}</span></a>
                                    </div>
                                </div>
                            </div>
                            <div class="menu-btn-show">
                                <span class="border-style"></span>
                                <span class="border-style"></span>
                                <span class="border-style"></span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="header-desktop clearfix hidden-sm hidden-xs">
        <div class="wrap-freeze-header clearfix ">
            <div class="container">
                <div class="box-color col-lg-12 clearfix">
                    <div class="layout-header-3 pad-vertical-10 row">
                        <div class="logo">
                            @if (count(json_decode($header['images'])) > 0)
                                <a itemprop="url" href="/">
                                    <img class="imgrps"
                                        src="{{ asset('storage') }}/photos/{{ json_decode($header['images'])[0] }}"
                                        alt="{{ $header['title'] }}" itemprop="logo image">
                                </a>
                            @endif
                        </div>
                        <div class="address-top-page">
                            <div class="address-top-box-1">
                                <div class="icon-top-box-1">
                                    <img itemprop="image"
                                        src="{{ asset('frontend') }}/themes/fnail03c/assets/images/opened-email.png"
                                        alt="opened-email.png">
                                </div>
                                <div class="title-top-box-1">
                                    <h2 itemprop="name">EMAIL</h2>
                                    <a href="mailto:{{ $features[2]->desc }}" title="{{ $features[2]->desc }}"
                                        target="_top" itemprop="email">{{ $features[2]->desc }}</a>
                                </div>
                            </div>
                            <div class="address-top-box-1">
                                <div class="icon-top-box-1">
                                    <img itemprop="image"
                                        src="{{ asset('frontend') }}/themes/fnail03c/assets/images/opened-map.png"
                                        alt="opened-map.png">
                                </div>
                                <div class="title-top-box-1" style="max-width: 150px;">
                                    <h2>Find Us</h2>
                                    <p itemprop="address">{{ $features[0]->desc }}</p>
                                </div>
                            </div>
                            <div class="address-top-box-1">
                                <div class="icon-top-box-1">
                                    <img itemprop="image"
                                        src="{{ asset('frontend') }}/themes/fnail03c/assets/images/opened-phone.png"
                                        alt="opened-phone.png">
                                </div>
                                <div class="title-top-box-1">
                                    <h2>Call Now</h2>
                                    <a itemprop="url" href="tel:{{ $features[1]->desc }}"
                                        title="{{ $features[1]->desc }}"><span
                                            itemprop="telephone">{{ $features[1]->desc }}</span></a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="flag-freeze-header">
                <div class="border-top-menu">
                    <div class="container">
                        <div class="main-menu">
                            <nav class="custom_menu">
                                <ul>
                                    <li itemprop="name"><a itemprop="url" href="/">Home</a></li>
                                    <li itemprop="name"><a itemprop="url" href="/about">About us</a></li>
                                    <li itemprop="name"><a itemprop="url" href="/services">Services</a></li>
                                    <li itemprop="name"><a itemprop="url" href="/coupons" title="">Coupons</a>
                                    </li>
                                    <li itemprop="name"><a itemprop="url" href="/gallery">Gallery</a></li>
                                    <li itemprop="name"><a itemprop="url" href="/book">Booking</a></li>
                                    <li itemprop="name"><a itemprop="url" href="/contact">Contact US</a></li>
                                </ul>
                            </nav>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
{{-- MENU MOBILE --}}
<div class="menu-bar-mobile" tabindex="-1">
    <div class="logo-menu">
        @if (count(json_decode($header['images'])) > 0)
            <a itemprop="url" href="/">
                <img class="imgrps" src="{{ asset('storage') }}/photos/{{ json_decode($header['images'])[0] }}"
                    alt="{{ $header['title'] }}" itemprop="logo image">
            </a>
        @endif
    </div>

    <ul>
        <li itemprop="name"><a itemprop="url" href="/">Home</a></li>
        <li itemprop="name"><a itemprop="url" href="/about">About us</a></li>
        <li itemprop="name"><a itemprop="url" href="/services">Services</a></li>
        <li itemprop="name"><a itemprop="url" href="/coupons" title="">Coupons</a></li>
        <li itemprop="name"><a itemprop="url" href="/gallery">Gallery</a></li>
        <li itemprop="name"><a itemprop="url" href="/book">Booking</a></li>
        <li itemprop="name"><a itemprop="url" href="/contact">Contact US</a></li>
    </ul>
</div>
