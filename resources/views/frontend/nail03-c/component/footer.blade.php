@php
$features = json_decode($header['features']);
$extras = json_decode($header['extras']);
@endphp
<div class="footer-page">
    <div class="contact-footer">
        <div class="container">
            <div class="row">
                <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                    <div class="row footer-3-box">
                        <div class="col-md-12">
                            <h2 itemprop="name">ABOUT US</h2>
                            <p>{{ $header['description'] }}
                            </p>
                        </div>
                    </div>
                    <div class="row text-center">
                        <div class="col-md-12"></div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                    <div class="business-hour">
                        <h2 itemprop="name">Business Hours</h2>
                        <table class="table_openhour">
                            @foreach ($extras as $extra)
                                <tr itemprop="openingHours" content="{{ $extra->name }}:{{ $extra->desc }}">
                                    <td width="30%"><b>{{ $extra->name }}</b></td>
                                    <td>{{ $extra->desc }}</td>
                                </tr>
                            @endforeach
                        </table>
                    </div>
                </div>
                <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                    <div class="footer-3-box">
                        <h2>Follow US</h2>
                        <div class="header-social clearfix">
                            <ul class="ul-social">
                                @foreach ($social as $item)
                                    <li>
                                        @if ($item['url'])
                                            <a itemprop="url" rel="nofollow" target="_blank"
                                                title="{{ $item['title'] }}" href="{{ $item['content'] }}">
                                                <img src="{{ asset('storage') }}/photos/{{ $item['url'] }}"
                                                    alt="{{ $item['title'] }}" />
                                            </a>
                                        @endif
                                    </li>
                                @endforeach
                            </ul>
                        </div>
                        <!-- Start Single Footer Widget -->
                        <div class="header-social-page clearfix">
                            <!-- facebook fanpage -->
                            <div class="single_footer_widget">
                                <div class="box_fanpage_fb">
                                    <div id="fanpage_fb_container"></div>
                                </div>
                            </div>

                            <!-- use for calculator width -->
                            <div class="single_footer_widget">
                                <div id="social_block_width" style="width:100% !important; height: 1px !important">
                                </div>
                            </div>
                            <!-- End Single Footer Widget -->
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="FOOTERbg">
            <center>
                <p>© Copyright by Nails Spa. All Rights Reserved.</p>
            </center>
        </div>
    </div>
</div> <!-- Active freeze footer by delete style display: none -->
<div class="freeze-footer">
    <ul>
        <li><a href="tel:{{ $features[1]->desc }}"
                class="btn btn-default btn_call_now btn-call">{{ $features[1]->desc }}</a></li>
        <li><a href="/book" class="btn btn-default btn_make_appointment">Booking</a></li>
    </ul>
</div>
