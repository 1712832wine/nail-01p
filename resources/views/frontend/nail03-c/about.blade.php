<div class="banner-page-list">
    <div class="container">
        <div class="title-page">
            <div class="ac">
                <h2 class="lost-child">
                    <a href="/about" title="About us">About us</a>
                </h2>
                <div class="breakcrumb">
                    <a href="/" title="Home">Home</a>
                    <span>/</span> <a href="/about" title="About us">About us</a>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="main-content">
    <div class="about-us-page-1">
        <div class="ac">
            <div class="instroduce">
                <div class="">
                    <div class="about-us about-us-inpage">
                        <div class="container">
                            @foreach ($articles as $item)
                                <div class="row">
                                    <div class="col-xs-5">
                                        <div class="img-about-us-index">
                                            @if (count(json_decode($item['image'])) > 0)
                                                <a title="home" href="/" itemprop="url">
                                                    <img alt="{{ $item['name'] }}" itemprop="image"
                                                        src="{{ asset('storage') }}/photos/{{ json_decode($item['image'])[0] }}"
                                                        caption="false" /> </a>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="col-xs-7">
                                        <div class="bg-about-us">
                                            <div class="title-standard txt-right">
                                                <h2 itemprop="name">Welcome to <br />{{ $item['title'] }}<span>ABOUT
                                                        US</span>
                                                </h2>
                                            </div>
                                            <div itemprop="description" class="ctn-about-inpage">
                                                {!! $item['content'] !!}
                                                <p><a class="view-more-about" href="/services">OUR SERVICES</a></p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            @endforeach
                        </div>
                    </div>
                    <div class="testimonials-index">
                        <div class="container">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="map-address">
    <h2>Map</h2>
    <section class="box_map">
        <!-- Google map area -->
        <div class="google-map-wrapper">
            <div class="google-map" id="map" style="width:100%; height:450px;"><iframe title="google-map"
                    src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3464.7391505606774!2d-95.57299108550652!3d29.727314340440024!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x8640ddc6ed681049%3A0x74ace03ed50728e8!2sFast+Boy+Marketing!5e0!3m2!1sen!2s!4v1510909605272"
                    width="100%" height="450" frameborder="0" style="border:0" allowfullscreen></iframe></div>
        </div>
    </section>
</div>
