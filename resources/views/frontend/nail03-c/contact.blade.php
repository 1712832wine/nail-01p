@php
$features = json_decode($header['features']);
$extras = json_decode($header['extras']);
@endphp
<div class="banner-page-list">
    <div class="container">
        <div class="title-page">
            <div class="ac">
                <h2 class="lost-child">
                    <a title="Contact us">Contact us</a>
                </h2>
                <div class="breakcrumb">
                    <a href="/" title="Home">Home</a>
                    <span>/</span> <a href="/contact" title="Contact us">Contact us</a>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="main-content">
    <div class="intro-star-shop bg-center">
        <div class="container">
            <div class="intro-star-shop bg-center">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                            <div class="footer-3-box">
                                <div class="custom-form-2">
                                    <div class="form-contact-us-index">
                                        <div class="form-input-infor">
                                            <form enctype="multipart/form-data" method="post" name="send_contact"
                                                id="send_contact" action="{{ route('send_contact') }}">
                                                @csrf
                                                <div class="width-50 left pad-15">
                                                    <div class="form-group">
                                                        <input title="your name" type="text"
                                                            data-validation="[NOTEMPTY]"
                                                            data-validation-message="Please enter your name!"
                                                            autocomplete="off" class="form-control style-input"
                                                            placeholder="Your Name" name="contactname">
                                                    </div>
                                                </div>
                                                <div class="width-50 left pad-15">
                                                    <div class="form-group">
                                                        <input title="Your Email" type="text" data-validation="[EMAIL]"
                                                            data-validation-message="Invalid email" autocomplete="off"
                                                            class="form-control style-input" placeholder="Your Email"
                                                            name="contactemail">
                                                    </div>
                                                </div>
                                                <div class="col-md-12">
                                                    <div class="form-group">
                                                        <input title="Your Subject" type="text"
                                                            data-validation="[NOTEMPTY]"
                                                            data-validation-message="Please enter a subject!"
                                                            autocomplete="off" class="form-control style-input"
                                                            placeholder="Your Subject" name="contactsubject">
                                                    </div>
                                                </div>
                                                <div class="col-xs-12">
                                                    <textarea title="Your Message" rows="10"
                                                        data-validation="[NOTEMPTY]"
                                                        data-validation-message="Please enter a content!"
                                                        autocomplete="off" class="style-input style-textarea"
                                                        placeholder="Your Message" name="contactcontent"></textarea>
                                                </div>
                                                <div class="col-xs-12">
                                                    <div class="form-group">
                                                        @if (session('status'))
                                                            <div class="alert alert-success" role="alert">
                                                                <button type="button" class="close"
                                                                    data-dismiss="alert">×</button>
                                                                {{ session('status') }}
                                                            </div>
                                                        @elseif(session('failed'))
                                                            <div class="alert alert-danger" role="alert">
                                                                <button type="button" class="close"
                                                                    data-dismiss="alert">×</button>
                                                                {{ session('failed') }}
                                                            </div>
                                                        @endif
                                                    </div>
                                                </div>
                                                <div class="col-xs-12">
                                                    <div class="btn-sauna form-group">
                                                        <button type="submit"
                                                            class="style-button hs-btn btn_contact btn-light ">
                                                            Send message </button>
                                                    </div>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                            <div class="ctn-about-shop">
                                <div class="pad-ctn-5">
                                    <div itemprop="description" class="footer-3-box">
                                        <p>Call us to make an appointment today!<br />We are always ready to
                                            serve our valued customers!</p>
                                    </div>
                                    <div class="footer-3-box">
                                        <h2 itemprop="name">Contact</h2>
                                        <ul class="info_contact">
                                            <li><span>Address:</span>
                                                <spam itemprop="address">{{ $features[0]->desc }}
                                                </spam>
                                            </li>
                                            <li><span>Email:</span><a href="mailto:{{ $features[2]->desc }}"
                                                    itemprop="email">{{ $features[2]->desc }}</a></li>
                                            <li><span>Phone:</span><a href="tel:{{ $features[1]->desc }}"
                                                    itemprop="url">
                                                    <spam itemprop="telephone">{{ $features[1]->desc }}</spam>
                                                </a></li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Google map area -->
<div class="map-address">
    <h2>Map</h2>
    <div class="google-map" id="map" style="width:100%; height:450px; clear: both"><iframe title="google-map"
            src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3464.7391505606774!2d-95.57299108550652!3d29.727314340440024!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x8640ddc6ed681049%3A0x74ace03ed50728e8!2sFast+Boy+Marketing!5e0!3m2!1sen!2s!4v1510909605272"
            width="100%" height="450" frameborder="0" style="border:0" allowfullscreen></iframe></div>
</div>
