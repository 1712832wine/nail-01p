<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" dir="ltr" lang="en" xmlns:og="http://ogp.me/ns#"
    xmlns:fb="http://www.facebook.com/2008/fbml" itemscope itemtype="https://schema.org/NailSalon">

<head>
    <!-- Language -->
    <meta http-equiv="content-language" content="en" />
    <link rel="alternate" href="" hreflang="x-default">
    <link rel="alternate" href="" hreflang="en-US">

    <!-- Meta limit -->

    <!-- Meta Tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width,initial-scale=1.0" />
    <meta http-equiv="content-type" content="text/html; charset=UTF-8" />
    <meta name="description" content="Fast Boy Marketing" />
    <meta name="keywords" content="Fast Boy Marketing" />
    <meta name="author" content="Fast Boy Marketing" />

    <!-- OG -->
    <meta property="og:title" content="Fast Boy Marketing" />
    <meta property="og:description" content="Fast Boy Marketing" />
    <meta property="og:image" content="" />
    <meta property="og:url" content="/" />
    <meta property="og:type" content="website" />
    <meta property="og:site_name" content="Nails Spa" />

    <!-- Dublin Core -->
    <link rel="schema.DC" href="http://purl.org/dc/elements/1.1/">
    <meta name="DC.title" content="Fast Boy Marketing">
    <meta name="DC.identifier" content="/">
    <meta name="DC.description" content="Fast Boy Marketing">
    <meta name="DC.subject" content="Fast Boy Marketing">
    <meta name="DC.language" scheme="UTF-8" content="en-us">
    <meta itemprop="priceRange" name="priceRange" content="">

    <!-- GEO meta -->
    <meta name="geo.region" content="">
    <meta name="geo.placename" content="">
    <meta name="geo.position" content="">
    <meta name="ICBM" content="">

    <!-- Page Title -->
    <title>Nails Spa</title>
    <base href="/themes/fnail03c/assets/">

    <!-- canonical -->
    <link rel="canonical" href="">



    <!-- Favicons -->
    <link rel="icon" href="/uploads/fnail0afw5skl/attach/1562751215_fbm_fvc.png" type="image/x-icon">
    <link rel="shortcut icon" href="/uploads/fnail0afw5skl/attach/1562751215_fbm_fvc.png" type="image/x-icon">

    <!-- CSS -->
    <link rel="stylesheet" type="text/css" href='{{ asset('frontend') }}/public/library/jquery-ui/jquery-ui.min.css'>
    <link rel="stylesheet" type="text/css" href='{{ asset('frontend') }}/public/library/validation/validation.css'>
    <link rel="stylesheet" type="text/css" href='{{ asset('frontend') }}/public/library/flex-label/flex-label.css'>
    <link rel="stylesheet" type="text/css" href='{{ asset('frontend') }}/public/library/animate/animate.css'>
    <link rel="stylesheet" type="text/css" href='{{ asset('frontend') }}/public/library/validation/validation.css'>
    <link rel="stylesheet" type="text/css"
        href='{{ asset('frontend') }}/public/library/bootstrap/v3.3.7/bootstrap.min.css'>
    <link rel="stylesheet" type="text/css"
        href='{{ asset('frontend') }}/public/library/bootstrap-datetimepicker/bootstrap-datetimepicker.min.css'>
    <link rel="stylesheet" type="text/css"
        href='{{ asset('frontend') }}/public/library/font-awesome/v4.7.0/css/font-awesome.min.css'>
    <link rel="stylesheet" type="text/css"
        href='{{ asset('frontend') }}/public/library/owl-carousel/v1.3.3/owl.carousel.min.css'>
    <link rel="stylesheet" type="text/css"
        href='{{ asset('frontend') }}/public/library/magnific-popup/magnific-popup.css'>
    <link rel="stylesheet" type="text/css"
        href='{{ asset('frontend') }}/public/library/slider-pro/slider-pro.min.css'>
    <link rel="stylesheet" type="text/css"
        href='{{ asset('frontend') }}/public/library/slider-slick/slider-slick.css'>
    <link rel="stylesheet" type="text/css"
        href='{{ asset('frontend') }}/public/library/fancybox/jquery.fancybox.min.css'>
    <link rel="stylesheet" type="text/css" href='{{ asset('frontend') }}/public/library/lightbox/lightbox.css'>
    <link rel="stylesheet" type="text/css" href='{{ asset('frontend') }}/public/library/instagram/instafeed.min.css'>
    <link rel="stylesheet" type="text/css" href='{{ asset('frontend') }}/public/library/signature/signature.css'>

    <link rel="stylesheet" type="text/css"
        href='{{ asset('frontend') }}/themes/fnail03c/assets/css/main-styles-import.css'>
    <link rel="stylesheet" type="text/css"
        href='{{ asset('frontend') }}/themes/fnail03c/assets/css/multi-row-grid.css'>
    <link rel="stylesheet" type="text/css" href='{{ asset('frontend') }}/themes/fnail03c/assets/css/menu_bar.css'>
    <link rel="stylesheet" type="text/css" href='{{ asset('frontend') }}/themes/fnail03c/assets/css/selectric.css'>
    <link rel="stylesheet" type="text/css" href='{{ asset('frontend') }}/themes/fnail03c/assets/css/service.css'>
    <link rel="stylesheet" type="text/css" href='{{ asset('frontend') }}/themes/fnail03c/assets/css/settings.css'>
    <link rel="stylesheet" type="text/css" href='{{ asset('frontend') }}/themes/fnail03c/assets/css/main-styles.css'>
    <link rel="stylesheet" type="text/css" href='{{ asset('frontend') }}/themes/fnail03c/assets/css/style.css'>
    <link rel="stylesheet" type="text/css" href='{{ asset('frontend') }}/themes/fnail03c/assets/css/responsive.css'>
    <link rel="stylesheet" type="text/css" href='{{ asset('frontend') }}/themes/fnail03c/assets/css/custom/main.css'>
    <link rel="stylesheet" type="text/css"
        href='{{ asset('frontend') }}/themes/fnail03c/assets/css/custom/custom_general.css'>
    <link rel="stylesheet" type="text/css" href='{{ asset('frontend') }}/public/library/global/css/news.css'>
    <link rel="stylesheet" type="text/css" href='{{ asset('frontend') }}/public/library/global/css/consent.css'>
    <link rel="stylesheet" type="text/css" href='{{ asset('frontend') }}/themes/fnail03c/assets/css/custom.css'>

    <script type="text/javascript">
        var dateFormatBooking = "MM/DD/YYYY";
        var posFormat = "1,0,2";
        var timeMorning = '["10:00","10:30","11:00","11:30"]';
        var timeAfternoon =
            '["12:00","12:30","13:00","13:30","14:00","14:30","15:00","15:30","16:00","16:30","17:00","17:30","18:00","18:30","19:00"]';
        var bookLogin = "open_booking";
        var beforeTime = "1";
        var beforeDay = 0;
        var currDateT = "2021/05/24";
        var checktimebooking = 1;
        var enableRecaptcha = 0;
        var hoursTimeFormat = 12;
        var phoneFormat = "(000) 000-0000";
        var company_phone = "832-968-6668";
        var num_paging = "12";

        let facebook_embed = {
            id_fanpage: "https://www.facebook.com/FastboyMarketingAgency",
            appId: "",
            width: 450,
            height: 300,
            tabs: "timeline",
            show_facepile: true,
            small_header: true,
            likebox_enable: true,
            likebox_show_faces: false,
            likebox_stream: true,
        };
        let google_id_fanpage = "";
        let twitter_id_fanpage = "";

    </script>

    <script type="text/javascript" src="{{ asset('frontend') }}/public/library/jquery/v1.10.2/jquery.min.js">
    </script>
    <script type="text/javascript" src="{{ asset('frontend') }}/public/library/jquery-ui/jquery-ui.min.js"></script>
    <script type="text/javascript" src="{{ asset('frontend') }}/public/library/jquery-ui/jquery-migrate.min.js">
    </script>
    <script type="text/javascript" src="{{ asset('frontend') }}/public/library/validation/validation.min.js">
    </script>
    <script type="text/javascript" src="{{ asset('frontend') }}/public/library/flex-label/flex-label.js"></script>
    <script type="text/javascript" src="{{ asset('frontend') }}/public/library/mask/mask.min.js"></script>
    <script type="text/javascript" src="{{ asset('frontend') }}/public/library/bootstrap/v3.3.7/bootstrap.min.js">
    </script>
    <script type="text/javascript" src="{{ asset('frontend') }}/public/library/moment/moment-with-locales.js">
    </script>
    <script type="text/javascript"
        src="{{ asset('frontend') }}/public/library/bootstrap-datetimepicker/bootstrap-datetimepicker.js"></script>
    <script type="text/javascript" src="{{ asset('frontend') }}/public/library/magnific-popup/magnific-popup.min.js">
    </script>
    <script type="text/javascript" src="{{ asset('frontend') }}/public/library/lightbox/lightbox.js"></script>
    <script type="text/javascript"
        src="{{ asset('frontend') }}/public/library/owl-carousel/v1.3.3/owl.carousel.min.js"></script>
    <script type="text/javascript" src="{{ asset('frontend') }}/public/library/pnotify/pnotify.js"></script>
    <script type="text/javascript" src="{{ asset('frontend') }}/public/library/slider-pro/slider-pro.min.js">
    </script>
    <script type="text/javascript" src="{{ asset('frontend') }}/public/library/slider-slick/slider-slick.js">
    </script>
    <script type="text/javascript" src="{{ asset('frontend') }}/public/library/wow/wow.min.js"></script>
    <script type="text/javascript" src="{{ asset('frontend') }}/public/library/instagram/instafeed.min.js"></script>
    <script type="text/javascript" src="{{ asset('frontend') }}/public/library/signature/signature.min.js"></script>
    <script type="text/javascript" src="{{ asset('frontend') }}/public/library/global/js/js_php.js"></script>
    <script type="text/javascript"
        src="{{ asset('frontend') }}/themes/fnail03c/assets/js/jssor.slider-23.0.0.mini.js">
    </script>

    <style type="text/css">
        .FOOTERbg {
            background: #f1f1f1;
        }

        .contact-footer p {
            margin: 3px;
        }

        .logo img {
            width: 390px;
        }

        .add-services {
            width: 25%;
        }

        .gallery-item>span {
            padding-bottom: 75%;
            margin-bottom: 30px;
        }

        @media screen and (max-width: 600px) {
            .contact-footer {
                z-index: 98;
            }
        }

        .footer-3-box p {
            color: #000
        }

        .img-info-staff {
            display: none;
        }

        .add-services {
            width: 100%;
        }

        .detail-price-number {
            font-weight: bold;
            white-space: nowrap;
        }

        .info_contact li {
            padding-left: 75px;
        }

        .info_contact li span,
        .info_contact li spam,
        .footer-3-box ul li a {
            font-family: "Lato", "sans-serif";
            font-size: 17px;
            line-height: 30px;
        }

        @media (max-width: 374px) {
            .info_contact li {
                padding-left: 70px;
            }

            .info_contact li span,
            .info_contact li spam,
            .footer-3-box ul li a {
                font-size: 15px;
            }
        }

        @media (max-width: 575px) {

            .info-staff,
            .info-staff .title-staff {
                width: 100%;
            }
        }

        .img-our-services {
            margin-bottom: 80px;
        }

    </style>

    <!-- Webmaster tools -->
    <!--Webmaster tools-->
    <!-- fb retargeting -->

    <!-- Schema Script -->
    <meta name="csrf-token" content="{{ csrf_token() }}" />
</head>

<body class="home">
    <h1 itemprop="name" style="display: none">Nails Spa</h1>
    <div id="fb-root"></div>

    <input type="hidden" name="activeFreezeHeader" value="1" />
    {{-- HEADER --}}
    @php
        $album = App\Models\Album::where([['type', 'Social'], ['status', 'PUBLISHED']])->first();
        $social = App\Models\Asset::where('albums_id', $album['id'])->orderBy('priority', 'desc')->get();
        $header = App\Models\Product::where('category_id', App\Models\Category::where('name', 'Header And Footer')->first()['id'])->first();
    @endphp
    @include('frontend.nail03-c.component.header',['header'=> $header])
    <div class="content-side-menu">
        {{ $slot }}
        @include('frontend.nail03-c.component.footer',
        ['header'=>$header,
        'social'=>$social])
    </div>
    <script type="text/javascript">
        var enable_booking = "1";
        if (enable_booking == 0) {
            $(".btn_make_appointment").remove();
        }

    </script>
    <script type="text/javascript" src="{{ asset('frontend') }}/public/library/global/js/consent.js"></script>
    <script type="text/javascript" src="{{ asset('frontend') }}/themes/fnail03c/assets/js/main.js?v=1.0"></script>

    <!-- Google analytics -->
    <script>
        (function(i, s, o, g, r, a, m) {
            i['GoogleAnalyticsObject'] = r;
            i[r] = i[r] || function() {
                (i[r].q = i[r].q || []).push(arguments)
            }, i[r].l = 1 * new Date();
            a = s.createElement(o),
                m = s.getElementsByTagName(o)[0];
            a.async = 1;
            a.src = g;
            m.parentNode.insertBefore(a, m)
        })(window, document, 'script', 'https://www.google-analytics.com/analytics.js', 'ga');

        ga('create', '', 'auto');
        ga('send', 'pageview');

    </script>
    <script>
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

    </script>
    <script type="text/javascript"></script>
    @livewireScripts
</body>

</html>
