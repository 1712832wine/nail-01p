<main class="main">

    <div class="pmain">
        <div class="page-heading">
            <div class="container">
                <div class="heading-inner">
                    <h2 class="mTitle">Coupons</h2>
                    <div class="hr-short"><span class="hr-inner"><span class="hr-inner-style"></span></span></div>
                </div>
            </div>
        </div>
        <div class="in-container">
            <div class="container">
                <div class="in-content">
                    <div class="row">
                        @foreach ($list as $item)
                            <div class="col-xs-12 col-sm-6 col-md-6">
                                <div class="pointer m-magnific-popup" data-group="coupon" title="Coupon"
                                    href="{{ asset('storage') }}/photos/{{ $item['url'] }}">
                                    <div class="m-coupon-box">
                                        <img itemprop="image" src="{{ asset('storage') }}/photos/{{ $item['url'] }}"
                                            alt="Coupon">
                                    </div>
                                </div>
                            </div>
                        @endforeach
                    </div>
                    <div class="row">
                        <div class="col-md-12 text-center">
                            <nav aria-label="Page navigation">
                            </nav>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</main>
