<main class="main">

    <div class="pmain">
        <div class="page-heading">
            <div class="container">
                <div class="heading-inner">
                    <h2 class="mTitle">Booking</h2>
                    <div class="hr-short"><span class="hr-inner"><span class="hr-inner-style"></span></span></div>
                </div>
            </div>
        </div>
        <div class="in-container">
            <div class="container">
                <div class="in-content">
                    <div class="page-title">
                    </div>
                    <section id="boxBookingForm" class="box-booking-form">
                        <form id="formBooking" class="form-booking" name="formBooking" action="/book/add" method="post"
                            enctype="multipart/form-data">
                            <div class="row">
                                <div class="col-sm-6 col-md-4 col-lg-3 booking-date">
                                    <div class="group-select">
                                        <label>Date (required)</label>
                                        <div class="relative w100 form-input-group">
                                            <input type="text" class="form-control form-text booking_date"
                                                autocomplete="off" name="booking_date" value=""
                                                data-validation="[NOTEMPTY]"
                                                data-validation-message="Please choose date" title="Booking date"
                                                placeholder="" maxlength="16">
                                            <span class="fa fa-calendar calendar form-icon"></span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-12 col-md-12 col-lg-9">
                                    <div class="row booking-service-staff booking-item" id="bookingItem_0">
                                        <div class="col-sm-12 col-md-7 col-lg-8 booking-service">
                                            <div class="group-select">
                                                <label>Service (required)</label>
                                                <div class="relative w100">
                                                    <select class="form-control booking_service" name="product_id[]"
                                                        data-validation="[NOTEMPTY]"
                                                        data-validation-message="Please choose a service"
                                                        title="Booking service">
                                                        <option value="">Select service</option>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-sm-12 col-md-5 col-lg-4 booking-staff">
                                            <div class="group-select">
                                                <label>Technician (optional)</label>
                                                <div class="relative w100">
                                                    <select class="form-control booking_staff" name="staff_id[]"
                                                        title="Booking staff">
                                                        <option value="">Select technician</option>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <!-- Button Add Item -->
                                    <div class="clearfix">
                                        <div class="add-services pointer booking_item_add">
                                            <i class="fa fa-plus-circle"></i> Add another service
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-12 col-md-12 col-lg-3">
                                    <div class="group-select">
                                        <label>&nbsp;</label>
                                        <div class="relative w100"><button class='btn btn-search search_booking'
                                                type='button'>Search</button></div>

                                        <!-- Hidden data -->
                                        <input type="hidden" name="booking_hours" class="booking_hours" value="">
                                        <input type="hidden" name="booking_name" class="booking_name" value="">
                                        <input type="hidden" name="booking_phone" class="booking_phone" value="">
                                        <input type="hidden" name="booking_email" class="booking_email" value="">
                                        <input type="hidden" name="notelist" class="notelist" value="">
                                        <input type="hidden" name="store_id" class="store_id" value="">

                                        <input type="hidden" name="booking_area_code" class="booking_area_code"
                                            value="1">
                                        <input type="hidden" name="booking_form_email" class="booking_form_email"
                                            value="0" />
                                        <input type="hidden" name="nocaptcha" class="nocaptcha" value="1">
                                        <input type="hidden" name="g-recaptcha-response" class="g-recaptcha-response"
                                            value="">
                                    </div>
                                </div>
                            </div>
                        </form>

                        <script type="text/javascript">
                            $(document).ready(function() {
                                /* Init Booking */
                                webBookingForm.init(
                                    '{"67":{"id":67,"name":"Manicure    ","services":[193,194]},"68":{"id":68,"name":"Acrylic  ","services":[195,196,197,198,199,200,201,202,203,204]},"69":{"id":69,"name":"Custom  ","services":[205,206,207,208]},"70":{"id":70,"name":"Pedicure","services":[209,210,211,212]},"71":{"id":71,"name":"Add-Ons  ","services":[213,214,215,216,217,218]},"72":{"id":72,"name":"Wax  ","services":[219,220,221]}}',
                                    '{"193":{"id":193,"name":"Basic","price":"$17 ","staffs":null,"schedule":false,"unit":"person","scheduleDay":null},"194":{"id":194,"name":"Gel","price":"$35 ","staffs":null,"schedule":false,"unit":"person","scheduleDay":null},"195":{"id":195,"name":"Full Set","price":"$35 ","staffs":null,"schedule":false,"unit":"person","scheduleDay":null},"196":{"id":196,"name":"Full Set\/ Gel","price":"$40 ","staffs":null,"schedule":false,"unit":"person","scheduleDay":null},"197":{"id":197,"name":"Fill","price":"$30 ","staffs":null,"schedule":false,"unit":"person","scheduleDay":null},"198":{"id":198,"name":"Fill\/ Gel","price":"$35 ","staffs":null,"schedule":false,"unit":"person","scheduleDay":null},"199":{"id":199,"name":"Ombre&#39;","price":"$55 ","staffs":null,"schedule":false,"unit":"person","scheduleDay":null},"200":{"id":200,"name":"Ombre&#39; Fill","price":"$35 ","staffs":null,"schedule":false,"unit":"person","scheduleDay":null},"201":{"id":201,"name":"Dip Powder","price":"$40 ","staffs":null,"schedule":false,"unit":"person","scheduleDay":null},"202":{"id":202,"name":"Pink & White","price":"$50 ","staffs":null,"schedule":false,"unit":"person","scheduleDay":null},"203":{"id":203,"name":"Pink & White Fill","price":"$40 ","staffs":null,"schedule":false,"unit":"person","scheduleDay":null},"204":{"id":204,"name":"Pink Fill","price":"$35 ","staffs":null,"schedule":false,"unit":"person","scheduleDay":null},"205":{"id":205,"name":"Extra-long Extension","price":"$5 +","staffs":null,"schedule":false,"unit":"person","scheduleDay":null},"206":{"id":206,"name":"Nail Art","price":"$5 +","staffs":null,"schedule":false,"unit":"person","scheduleDay":null},"207":{"id":207,"name":"French Tip","price":"$5 ","staffs":null,"schedule":false,"unit":"person","scheduleDay":null},"208":{"id":208,"name":"Specialty Shape","price":"$5 ","staffs":null,"schedule":false,"unit":"person","scheduleDay":null},"209":{"id":209,"name":"Basic","price":"$35 ","staffs":null,"schedule":false,"unit":"person","scheduleDay":null},"210":{"id":210,"name":"Basic Combo","price":"$50 ","staffs":null,"schedule":false,"unit":"person","scheduleDay":null},"211":{"id":211,"name":"Deluxe","price":"$45 ","staffs":null,"schedule":false,"unit":"person","scheduleDay":null},"212":{"id":212,"name":"Pearl Spa","price":"$55 ","staffs":null,"schedule":false,"unit":"person","scheduleDay":null},"213":{"id":213,"name":"Polish Change Hands","price":"$10 ","staffs":null,"schedule":false,"unit":"person","scheduleDay":null},"214":{"id":214,"name":"Polish Change Toes","price":"$12 ","staffs":null,"schedule":false,"unit":"person","scheduleDay":null},"215":{"id":215,"name":"Gel Polish Change Hands\/ Toes","price":"$25 ","staffs":null,"schedule":false,"unit":"person","scheduleDay":null},"216":{"id":216,"name":"Gel Polish Removal","price":"$10 ","staffs":null,"schedule":false,"unit":"person","scheduleDay":null},"217":{"id":217,"name":"Dipping Powder Removal","price":"$10 ","staffs":null,"schedule":false,"unit":"person","scheduleDay":null},"218":{"id":218,"name":"Acrylic Soak-Off","price":"$15 ","staffs":null,"schedule":false,"unit":"person","scheduleDay":null},"219":{"id":219,"name":"Eyebrows","price":"$10 ","staffs":null,"schedule":false,"unit":"person","scheduleDay":null},"220":{"id":220,"name":"Upper Lip","price":"$6 ","staffs":null,"schedule":false,"unit":"person","scheduleDay":null},"221":{"id":221,"name":"Chin","price":"$7 +","staffs":null,"schedule":false,"unit":"person","scheduleDay":null}}',
                                    'null',
                                    '{"listperson":[],"notelist":[],"booking_date":"05\/24\/2021","booking_hours":""}'
                                );
                            });

                        </script>
                    </section>

                    <section id="boxBookingInfo" class="box-booking-info relative" style="display: none;">
                        <h3 class="booking-info-title">Appointment Information</h3>

                        <!-- Service, Staff -->
                        <div id="boxServiceStaff" class="box-service-staff">
                            <div class="service-staff">
                                <div class="service-staff-avatar">
                                    <img class="img-responsive" src="/public/library/global/no-photo.jpg"
                                        alt="Any person">
                                </div>
                                <div class="service-staff-info">
                                    <h5>Any Technician</h5>
                                    <p>Any Service</p>
                                    <p>Price: N/A</p>
                                </div>
                            </div>
                            <div class="service-staff">
                                <div class="service-staff-avatar no-photo">
                                    <img class="img-responsive" src="/public/library/global/no-photo.jpg"
                                        alt="Any person">
                                </div>
                                <div class="service-staff-info">
                                    <h5>Any Technician</h5>
                                    <p>Any Service</p>
                                    <p>Price: N/A</p>
                                </div>
                            </div>
                        </div>

                        <!-- Date, Time List -->
                        <div id="boxDateTime" class="box-date-time">
                            <h4 class="date-info" id="dateInfo">Sunday, Jan-01-1970</h4>
                            <div class="time-info">
                                <h5>Morning <span class="time-note" id="timeAMNote">N/A</span></h5>
                                <ul class="time-items" id="timeAMHtml"></ul>
                            </div>
                            <div class="time-info">
                                <h5>Afternoon <span class="time-note" id="timePMNote">N/A</span></h5>
                                <ul class="time-items" id="timePMHtml"></ul>
                            </div>
                        </div>
                    </section>

                    <section id="popupBookingConfirm" class="popup-booking-confirm white-popup mfp-hide border-style">
                        <section id="boxBookingConfirm" class="box-booking-confirm relative">
                            <h3 class="booking-confirm-title">Confirm booking information ?</h3>
                            <div class="booking-confirm-note">We will send a text message to you via the number
                                below after we confirm the calendar for your booking.</div>
                            <script src='https://www.google.com/recaptcha/api.js?hl=en' async defer></script>
                            <!-- Google reCaptcha -->
                            <script type="text/javascript">
                                function ezyCaptcha_formBookingConfirm(token, is_submit) {
                                    is_submit = 1;
                                    if ($("#password").length) {
                                        //$("input:password").val(md5(clean_input($("#password").val())));
                                    }
                                    return true;
                                }

                            </script>
                            <form id="formBookingConfirm" class="form-booking-confirm" name="formBookingConfirm"
                                method="post" action="/book" enctype="multipart/form-data">
                                <div class="row">
                                    <div class="col-md-6 booking-name">
                                        <div class="group-select">
                                            <label>Your name (required)</label>
                                            <div class="relative w100">
                                                <input type="text" class="form-control" name="booking_name" value=""
                                                    data-validation="[NOTEMPTY]"
                                                    data-validation-message="Please enter your name" placeholder=""
                                                    maxlength="76" title="Booking name" />
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6 booking-phone">
                                        <div class="group-select">
                                            <label>Your phone (required)</label>
                                            <div class="relative w100">
                                                <input type="text" class="form-control inputPhone" pattern="\d*"
                                                    name="booking_phone" data-validation="[NOTEMPTY]"
                                                    data-validation-message="Please enter your phone" placeholder=""
                                                    maxlength="16" title="Booking phone" />
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-12 booking-email">
                                        <div style="display: none;" class="group-select">
                                            <label>Your email (optional)</label>
                                            <div class="relative w100">
                                                <input type="text" class="form-control" name="booking_email" value=""
                                                    placeholder="" maxlength="76" title="Booking email" />
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-12 booking-notelist">
                                        <div class="group-select">
                                            <label>Note (optional)</label>
                                            <div class="relative w100">
                                                <textarea class="form-control" rows="5" name="notelist"
                                                    placeholder="(Max length 200 character)" maxlength="201"
                                                    title="Booking note"></textarea>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-12 booking-store">
                                        <input type="hidden" name="choose_store" class="booking_store" value="0">
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-8 order-md-2 col-md-push-4">
                                        <button class="btn btn-confirm btn_confirm " type="button">Confirm</button>
                                    </div>
                                    <div class="col-md-4 order-md-1 col-md-pull-8">
                                        <button class="btn btn-cancel btn_cancel" type="button">Cancel</button>
                                    </div>
                                </div>
                            </form>
                        </section>
                    </section>
                </div>
            </div>
        </div>
    </div>
</main>
