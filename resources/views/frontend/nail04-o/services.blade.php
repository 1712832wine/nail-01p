<main class="main">

    <div class="pmain">
        <div class="page-heading">
            <div class="container">
                <div class="heading-inner">
                    <h2 class="mTitle">Services</h2>
                    <div class="hr-short"><span class="hr-inner"><span class="hr-inner-style"></span></span></div>
                </div>
            </div>
        </div>
        <div class="in-container">
            <div class="container">
                <div class="in-content">
                    <div class="service-container animation_sroll_button">
                        <div class="service-btn-group top-right"><a href="/book.html"
                                class="btn btn-main btn_make_appointment" title="Make an appointment"><img
                                    src="{{ asset('frontend') }}/themes/fnail04o/assets/images/Forma-11.png"
                                    alt="Forma-11.png" />Make an
                                appointment</a>&nbsp <a href="tel:832-968-6668" class="btn btn-main"
                                title="Call now"><img
                                    src="{{ asset('frontend') }}/themes/fnail04o/assets/images/Shape-1.png"
                                    alt="Shape-1.png" />Call now</a></div>
                        <div class="row service-row">
                            <div class="col-sm-12">
                                <h2>Nail Services</h2>
                            </div>
                        </div>
                        <div class="clearfix animation_sroll_to_service service-list">
                            <!-- Use for get current id -->
                            <input type="hidden" name="group_id" value="{{ $service_id }}" />
                            @foreach ($services as $index => $service)
                                @if ($index % 2)
                                    <div class="row clearfix" id="sci_{{ $service['id'] }}">
                                        <div class="service-line"></div>
                                        <div class="col-sm-4 col-md-4 col-md-push-8 col-sm-push-8 text-center">
                                            @if (count(json_decode($service['images'])) > 0)
                                                <div class="service-image circle">
                                                    <div class="service-image-bg"
                                                        style="background-image: url('{{ asset('storage') }}/photos/{{ json_decode($service['images'])[0] }}');')">
                                                        <img class="img-responsive"
                                                            src="{{ asset('storage') }}/photos/{{ json_decode($service['images'])[0] }}');"
                                                            alt="Acrylic">
                                                    </div>
                                                </div>
                                            @endif
                                        </div>
                                        <div class="col-sm-8 col-md-8 col-md-pull-4 col-sm-pull-4">
                                            <div class="clearfix service-list">
                                                <h2 class="service-name">{{ $service['name'] }}</h2>
                                                <div class="service-desc"></div>
                                                @foreach (json_decode($service['features']) as $item)
                                                    @switch($item->name)
                                                        @case('desc')
                                                            <p>{{ $item['desc'] }}</p>
                                                        @break
                                                        @case('center')
                                                        @break
                                                        @default
                                                            <div class="detail-price-item">
                                                                <span class="detail-price-name">{{ $item->name }}</span>
                                                                <span class="detail-price-dots"></span>
                                                                <span class="detail-price-number">
                                                                    <span class="current">{{ $item->desc }}</span>
                                                                </span>
                                                            </div>
                                                    @endswitch
                                                @endforeach
                                            </div>
                                        </div>
                                    </div>
                                @else
                                    <div class="row clearfix" id="sci_{{ $service['id'] }}">
                                        <div class="col-sm-4 col-md-4  text-center">
                                            @if (count(json_decode($service['images'])) > 0)
                                                <div class="service-image circle">
                                                    <div class="service-image-bg"
                                                        style="background-image: url('{{ asset('storage') }}/photos/{{ json_decode($service['images'])[0] }}');')">
                                                        <img class="img-responsive"
                                                            src="{{ asset('storage') }}/photos/{{ json_decode($service['images'])[0] }}');"
                                                            alt="{{ $service['name'] }}">
                                                    </div>
                                                </div>
                                            @endif
                                        </div>
                                        <div class="col-sm-8 col-md-8 ">
                                            <div class="clearfix service-list">
                                                <h2 class="service-name">{{ $service['name'] }}</h2>
                                                <div class="service-desc"></div>
                                                @foreach (json_decode($service['features']) as $item)
                                                    @switch($item->name)
                                                        @case('desc')
                                                            <p>{{ $item['desc'] }}</p>
                                                        @break
                                                        @case('center')
                                                        @break
                                                        @default
                                                            <div class="detail-price-item">
                                                                <span class="detail-price-name">{{ $item->name }}</span>
                                                                <span class="detail-price-dots"></span>
                                                                <span class="detail-price-number">
                                                                    <span class="current">{{ $item->desc }}</span>
                                                                </span>
                                                            </div>
                                                    @endswitch
                                                @endforeach
                                            </div>
                                        </div>
                                    </div>
                                @endif
                            @endforeach
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</main>
