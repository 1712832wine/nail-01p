<main class="main">
    <!-- Tpl main about -->
    <!--# Support render.introduction.about-->
    <div class="pmain">
        <div class="page-heading">
            <div class="container">
                <div class="heading-inner">
                    <h2 class="mTitle">About Us</h2>
                    <div class="hr-short"><span class="hr-inner"><span class="hr-inner-style"></span></span></div>
                </div>
            </div>
        </div>
        <div class="in-container">
            @foreach ($articles as $item)
                <div class="container">
                    @if (count(json_decode($item['image'])) > 0)
                        <div class="col-md-6">
                            <img src="{{ asset('storage') }}/photos/{{ json_decode($item['image'])[0] }}" alt="" />
                        </div>
                    @endif
                    <div class="col-md-6">
                        <div class="in-content">
                            <h3>{{ $item['title'] }}</h3>
                            {!! $item['content'] !!}
                        </div>
                    </div>
                </div>
            @endforeach

        </div>
    </div><!-- .pmain-->
</main>
