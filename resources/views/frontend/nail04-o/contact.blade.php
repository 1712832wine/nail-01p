@php
$features = json_decode($header['features']);
$extras = json_decode($header['extras']);
@endphp
<main class="main">
    <div class="pmain">
        <div class="page-heading">
            <div class="container">
                <div class="heading-inner">
                    <h2 class="mTitle">Contact Us</h2>
                    <div class="hr-short"><span class="hr-inner"><span class="hr-inner-style"></span></span></div>
                </div>
            </div>
        </div>
        <div class="maps">
            <div class="google-maps" id="google-map"><iframe
                    src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3464.7391505606774!2d-95.57299108550652!3d29.727314340440024!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x8640ddc6ed681049%3A0x74ace03ed50728e8!2sFast+Boy+Marketing!5e0!3m2!1sen!2s!4v1510909605272"
                    width="100%" height="450" frameborder="0" style="border:0" allowfullscreen></iframe></div>
        </div>
        <div class="in-container">
            <div class="container">
                <div class="in-content">
                    <div class="row">
                        <div class="col-md-7">
                            <div class="clearfix">
                                <form enctype="multipart/form-data" method="post" name="send_contact" id="send_contact"
                                    action="{{ route('send_contact') }}" class="form-horizontal">
                                    @csrf
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="group-select">
                                                <label>Your name (required)</label>
                                                <div class="relative w100">
                                                    <input title="Your name" type="text" data-validation="[NOTEMPTY]"
                                                        data-validation-message="Please enter your name"
                                                        autocomplete="off" class="form-control" placeholder=""
                                                        name="contactname">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="group-select">
                                                <label>Your email address (required)</label>
                                                <div class="relative w100">
                                                    <input title="Your email address" type="text"
                                                        data-validation="[EMAIL]"
                                                        data-validation-message="Please enter your email"
                                                        autocomplete="off" class="form-control" placeholder=""
                                                        name="contactemail">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="group-select">
                                        <label>Your subject (required)</label>
                                        <div class="relative w100">
                                            <input title="Your subject" type="text" data-validation="[NOTEMPTY]"
                                                data-validation-message="Please enter your subject" autocomplete="off"
                                                class="form-control" placeholder="" name="contactsubject">
                                        </div>
                                    </div>
                                    <div class="group-select">
                                        <label>Your message (required)</label>
                                        <div class="relative w100">
                                            <textarea title="Your message" rows="10" data-validation="[NOTEMPTY]"
                                                data-validation-message="Please enter your message" autocomplete="off"
                                                class="form-control" placeholder="" name="contactcontent"></textarea>
                                        </div>
                                    </div>
                                    <div class="group-select">
                                        @if (session('status'))
                                            <div class="alert alert-success" role="alert">
                                                <button type="button" class="close" data-dismiss="alert">×</button>
                                                {{ session('status') }}
                                            </div>
                                        @elseif(session('failed'))
                                            <div class="alert alert-danger" role="alert">
                                                <button type="button" class="close" data-dismiss="alert">×</button>
                                                {{ session('failed') }}
                                            </div>
                                        @endif
                                    </div>
                                    <div class="form-group text-center">
                                        <button type="submit" class="btn btn-main btn-submit btn_contact ">
                                            Send Us </button>
                                    </div>
                                </form>
                                <br>
                            </div>
                        </div>
                        <div class="col-md-5">
                            {{-- <!-- Tpl information contact --> --}}


                            <div class="clearfix contact-info">
                                <div class="contact-info">
                                    <div class="clearfix contact-address">
                                        <div class="fci-wrap clearfix">
                                            @foreach ($features as $index => $item)
                                                @php
                                                    $name = explode(',', $item->name);
                                                    $desc = explode(',', $item->desc);
                                                @endphp
                                                @switch($index)
                                                    @case(0)
                                                        <div class="fci-row address-row" itemprop="address" itemscope=""
                                                            itemtype="http://schema.org/PostalAddress">
                                                            <span class="fci-title">Address:</span>
                                                            <span class="fci-content address-wrap">
                                                                <span itemprop="streetAddress"
                                                                    class="address">{{ $item->desc }}</span>
                                                            </span>
                                                        </div>
                                                    @break
                                                    @case(2)
                                                        <div class="fci-row phone-row">
                                                            <span class="fci-title">Phone:</span>

                                                            <span class="fci-content phone-wrap">
                                                                @foreach (explode(',', $item->desc) as $el)
                                                                    <a href="tel:{{ $el }}" title="Call Us">
                                                                        <span itemprop="telephone"
                                                                            class="phone">{{ $el }}</span>
                                                                    </a>
                                                                @endforeach

                                                            </span>
                                                        </div>
                                                    @break
                                                    @default
                                                        <div class="fci-row email-row">
                                                            <span class="fci-title">Email:</span>
                                                            <span class="fci-content email-wrap">
                                                                @foreach (explode(',', $item->desc) as $el)
                                                                    <a href="mailto:{{ $el }}" title="Mail Us">
                                                                        <span itemprop="email"
                                                                            class="email">{{ $el }}</span>
                                                                    </a>
                                                                @endforeach
                                                            </span>
                                                        </div>
                                                @endswitch
                                            @endforeach
                                        </div>
                                    </div>
                                    <div class="clearfix contact-hours">
                                        <div class="foh-wrap">
                                            @foreach ($extras as $extra)
                                                <div class="foh-row short" itemprop="openingHours"
                                                    content="{{ $extra->desc }}">
                                                    <span class="foh-date">{{ $extra->name }}</span>
                                                    <span class="foh-time">{{ $extra->desc }}</span>
                                                </div>
                                            @endforeach
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div><!-- .pmain-->
</main>
