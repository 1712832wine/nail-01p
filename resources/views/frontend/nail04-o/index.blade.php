<main class="main">
    {{-- CAROUSEL --}}
    <div class="section-slider-wrap">
        <section class="section-slider">
            @if (count($imgs_carousel) > 0)
                <div class="slider-width-height"
                    style="display: inline-block;width: 100%;height: 1px;overflow: hidden;">
                    <div class="fixed" style="width: 100%"></div>
                    <img src="{{ asset('storage') }}/photos/{{ $imgs_carousel[0]['url'] }}"
                        style="width: 100%; height: auto" alt="" />
                </div>
            @endif
            <div class="slider-pro" id="my-slider" style="display: none">
                <div class="sp-slides">
                    @foreach ($imgs_carousel as $img)
                        <div class="sp-slide">
                            <div class="sp-layer sp-static">
                                <a href="{{ route('home') }}" target="_self" title="slide7.png">
                                    <img class="sp-image" src="{{ asset('storage') }}/photos/{{ $img['url'] }}"
                                        alt="slide7.png" />
                                </a>
                            </div>
                        </div>
                    @endforeach
                </div>
            </div>
            <div class="slider-pro" id="my-slider-fixed-height" style="display: none">
                <div class="sp-slides">
                    @foreach ($imgs_carousel as $img)
                        <div class="sp-slide">
                            <a href="{{ route('home') }}" target="_self" title="slide7.png">
                                <div class="sp-layer sp-static" data-width="100%" data-height="100%" style="
                                width: 100%;
                                height: 100%;
                                background: url('{{ asset('storage') }}/photos/{{ $img['url'] }}')
                                center center no-repeat;
                                background-size: cover;"></div>
                            </a>
                        </div>
                    @endforeach
                </div>
            </div>
        </section>
    </div>
    {{-- ABOUT ARTICLE --}}
    <section class="section section-studio">
        <div class="container">
            <h2 class="mTitle">{{ $home_album['name'] }}</h2>
            <h2 class="mTitle01">{{ $home_album['short_desc'] }}</h2>
            <div class="hr-short"><span class="hr-inner"><span class="hr-inner-style"></span></span></div>
            <div class="row">
                @foreach ($home_asset as $item)
                    <div class="col-sm-4">
                        <div class="icon-classic"
                            style="position: relative; display: inline-block; width: 80px; height: 80px; border-radius: 50%; background: #e9e5d9;">
                            <img src="{{ asset('storage') }}/photos/{{ $item['url'] }}" alt="winner" />
                        </div>
                        <h3><a href="/">{{ $item['name'] }}</a></h3>
                        <p>{{ $item['content'] }}</p>
                        <div class="view-more" style="display: none;"><a href="#">Learn more ›</a></div>
                    </div>
                @endforeach
            </div>
        </div>
    </section>
    {{-- SERVICES --}}
    <section class="section section-service">
        <div class="container">
            <h2 class="mTitle">Our Services</h2>
            <h2 class="mTitle01">Make Beauty For Your Life</h2>
            <div class="hr-short"><span class="hr-inner"><span class="hr-inner-style"></span></span></div>
            <div class="services-listall row">
                @foreach ($services as $service)
                    <div class="col-xs-6 col-sm-4 col-md-4">
                        <a itemprop="url"
                            href="{{ route('services-detail', ['slug' => strtolower(trim(preg_replace('/[^A-Za-z0-9-]+/', '-', $service['name']))), 'service_id' => $service['id']]) }}"
                            title="{{ $service['name'] }}">
                            <div class="item">
                                @if (count(json_decode($service['images'])) > 0)
                                    <div class="thumb">
                                        <img itemprop="image"
                                            src="{{ asset('storage') }}/photos/{{ json_decode($service['images'])[0] }}"
                                            class="imgrps" alt="{{ $service['name'] }}">
                                    </div>
                                @endif

                                <div class="info">
                                    <h3 class="title" itemprop="name"><strong>{{ $service['name'] }} </strong></h3>
                                </div>
                            </div>
                        </a>
                    </div>
                    <div class="break-row break-row-2"></div>
                @endforeach
            </div>
        </div>
        <div class="clearfix text-center">
            <a href="/services" class="btn btn-main">View more <i class="fa fa-arrow-right"></i></a>
        </div>
    </section>
    {{-- CUSTOMER --}}
    <section class="section section-review">
        <div class="review-container">
            <div class="container">
                <div class="review-inner">
                    <h3>What Our Clients Are Saying</h3>
                    <div class="hr-short"><span class="hr-inner"><span class="hr-inner-style"></span></span></div>
                    <div class="review-list owl-carousel">
                        @foreach ($carousel_customers as $item)
                            <div class="item">
                                <div class="content">{!! $item['content'] !!}</div>
                                <div class="review-name">
                                    <h4>{{ $item['title'] }}</h4>
                                </div>
                            </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </section>
    {{-- GALLERY --}}
    <section class="section-fs">
        <div class="clearfix">
            <h2 class="mTitle">Our gallery</h2>
            <div class="hr-short"><span class="hr-inner"><span class="hr-inner-style"></span></span></div>
            <div class="m-gallery-box-wrap">
                <div class="row">
                    @foreach ($gallery_list as $item)
                        <div class="col-xs-6 col-sm-6 col-md-4">
                            <div class="pointer m-magnific-popup" data-group="gallery-14" title="{{ $item['name'] }}"
                                href="{{ asset('storage') }}/photos/{{ $item['url'] }}">
                                <div class="m-gallery-box">
                                    <div class="m-image-bg"
                                        style="background-image: url('{{ asset('storage') }}/photos/{{ $item['url'] }}');">
                                        <img itemprop="image"
                                            src="{{ asset('storage') }}/photos/{{ $item['url'] }}"
                                            alt="{{ $item['name'] }}">
                                    </div>
                                </div>
                            </div>
                        </div>
                    @endforeach
                </div>
            </div>
        </div>
    </section>
    <section id="embed_popup_home">

        <style>
            /* Some example CSS */
            /* @import url("something.css"); */
            #embed_1571371768936_193 img {}

        </style>

        <div id="embed_1571371768936_193" class="white-popup popup-style mfp-hide">
            <div>
                <img src="{{ asset('storage') }}/photos/{{ $img_popup2['url'] }}" caption="false"
                    style="display: block; margin-left: auto; margin-right: auto;" />
            </div>
        </div>
        <script>
            $(document).ready(function() {
                $.magnificPopup.open({
                    items: {
                        src: '#embed_1571371768936_193',
                        type: 'inline'
                    }
                });
            });

        </script>

    </section>
</main>
