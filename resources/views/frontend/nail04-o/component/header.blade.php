@php
$menu = [
    [
        'title' => 'Home',
        'href' => route('home'),
        'name' => 'home',
    ],
    [
        'title' => 'About Us',
        'href' => route('about'),
        'name' => 'about',
    ],
    [
        'title' => 'Services',
        'href' => route('services'),
        'name' => 'services',
    ],
    [
        'title' => 'Gallery',
        'href' => route('gallery'),
        'name' => 'gallery',
    ],
    [
        'title' => 'Coupons',
        'href' => route('coupons'),
        'name' => 'coupons',
    ],
    [
        'title' => 'GiftCards',
        'href' => route('giftcards'),
        'name' => 'giftcards',
    ],

    [
        'title' => 'Contact',
        'href' => route('contact'),
        'name' => 'contact',
    ],
];
@endphp
<header class="header">
    <div class="header-top">
        <div class="container-fluid">
            <div class="header-top-inner">
                <div class="clearfix header-social">
                    <ul class="list-line social">
                        @foreach ($social as $item)
                            <li>
                                @if ($item['url'])
                                    <a itemprop="url" rel="nofollow" target="_blank" title="{{ $item['title'] }}"
                                        href="{{ $item['content'] }}">
                                        <img src="{{ asset('storage') }}/photos/{{ $item['url'] }}"
                                            alt="{{ $item['title'] }}" />
                                    </a>
                                @endif
                            </li>
                        @endforeach
                    </ul>
                </div>
                <div class="clearfix header-contact">
                    <ul>
                        <li>
                            <a href="mailto:web@fastboy.net" title="Mail Us">
                                <span itemprop="email" class="email">web@fastboy.net</span>
                            </a>
                        </li>
                        <li>
                            <a href="tel:832-968-6668" title="Call Us">
                                <span itemprop="telephone" class="phone">832-968-6668</span>
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>

    <div class="wrap-freeze-header-mobile clearfix hidden-md hidden-lg menu-1024-hidden">
        <div class="flag-freeze-header-mobile">
            <div class="menu_mobile_v1 hidden-md hidden-lg menu-1024-hidden">
                <div class="mobile_logo">
                    <a itemprop="url" href="/" title="Super Star Nails & Spa">
                        <img class="imgrps"
                            src="{{ asset('storage') }}/photos/{{ json_decode($header['images'])[0] }}"
                            alt="Super Star Nails & Spa" itemprop="logo image">
                    </a>
                </div>
                <div class="mobile_menu_container_v1">
                    <div class="mobile-menu clearfix">
                        <nav id="mobile_dropdown">
                            <!-- Tpl menu mobile layouts -->
                            <ul>
                                @foreach ($menu as $item)
                                    <li class="@if (Route::current()->getName() ===
                                        $item['name']) active @endif">
                                        <a itemprop="url" href="{{ $item['href'] }}" class="@if (Route::current()->getName() === $item['name']) active @endif">{{ $item['title'] }}</a>
                                    </li>
                                @endforeach
                            </ul>
                        </nav>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="wrap-freeze-header clearfix hidden-xs hidden-sm">
        <div class="flag-freeze-header">
            <nav class="header-main nav-desktop">
                <div class="container-fluid">
                    <div class="header-nav dflex-sb">
                        <a itemprop="url" href="/" title="Super Star Nails & Spa">
                            <img class="imgrps"
                                src="{{ asset('storage') }}/photos/{{ json_decode($header['images'])[0] }}"
                                alt="Super Star Nails & Spa" itemprop="logo image">
                        </a>
                        <div class="head-nav-right dflex">
                            <nav class="navbar">
                                <div class="navbar-collapse collapse">
                                    <!-- Tpl menu main layouts -->
                                    <ul class="nav navbar-nav">
                                        @foreach ($menu as $item)
                                            <li class="@if (Route::current()->getName() ===
                                                $item['name']) active @endif">
                                                <a itemprop="url" href="{{ $item['href'] }}" class="@if (Route::current()->getName() === $item['name']) active @endif">{{ $item['title'] }}</a>
                                            </li>
                                        @endforeach
                                    </ul>
                                </div>
                            </nav>
                        </div>
                    </div>
                </div>
            </nav>
        </div>
    </div>
</header>
