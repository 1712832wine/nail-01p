@php
$features = json_decode($header['features']);
$extras = json_decode($header['extras']);
@endphp
<footer class="footer">
    <div class="footer-main">
        <div class="container">
            <div class="row">
                <div class="col-sm-4">
                    <div class="footer-info">
                        <h3>Super Star Nails & Spa</h3>
                        <div class="text-widget">
                            <div class="fci-wrap">
                                @foreach ($features as $index => $item)
                                    @php
                                        $name = explode(',', $item->name);
                                        $desc = explode(',', $item->desc);
                                    @endphp
                                    @switch($index)
                                        @case(0)
                                            <p class="footer-address" itemprop="address" itemscope=""
                                                itemtype="http://schema.org/PostalAddress">
                                                <span class="footer-address-inner" itemprop="streetAddress">
                                                    {{ $item->desc }}
                                                </span>
                                            </p>
                                        @break
                                        @case(2)
                                            <p class="footer-phone">
                                                @foreach (explode(',', $item->desc) as $el)
                                                    <a href="tel:{{ $el }}" title="Call Us">
                                                        <span itemprop="telephone" class="phone">{{ $el }}</span>
                                                    </a>
                                                @endforeach
                                            </p>
                                        @break
                                        @default
                                            <p class="footer-email">
                                                @foreach (explode(',', $item->desc) as $el)
                                                    <a href="mailto:{{ $el }}" title="Mail Us">
                                                        <span itemprop="email" class="email">{{ $el }}</span>
                                                    </a>
                                                @endforeach
                                            </p>
                                    @endswitch
                                @endforeach
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-sm-4">
                    <div class="footer-info">
                        <h3>Business Hours</h3>
                        <div class="text-widget">
                            <div class="foh-wrap">
                                @foreach ($extras as $extra)
                                    <div class="foh-row short" itemprop="openingHours" content="{{ $extra->desc }}">
                                        <span class="foh-date">{{ $extra->name }}</span>
                                        <span class="foh-time">{{ $extra->desc }}</span>
                                    </div>
                                @endforeach
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-sm-4">
                    <!-- Start Single Footer Widget -->
                    <div class="social-fanpage clearfix">
                        <aside>
                            <!-- facebook fanpage -->
                            <div id="fanpage_fb_container"></div>
                        </aside>
                        <!-- use for calculator width -->
                        <div id="social_block_width" class="clearfix"
                            style="width:100% !important; height: 1px !important"></div>
                    </div><!-- End Single Footer Widget -->
                </div>
            </div>
        </div>
    </div>
    <div class="footer-copyright">
        <div class="container">
            <div class="row">
                <div class="col-sm-7">
                    <div class="copyright">Copyright by <span style="font-size: 14px;"><em>Super Star Nails &
                                Spa</em></span>. All Rights Reserved.</div>
                </div>
                <div class="col-sm-5">
                    <div class="clearfix footer-social">
                        <ul class="list-line social">
                            @foreach ($social as $item)
                                <li>
                                    @if ($item['url'])
                                        <a itemprop="url" rel="nofollow" target="_blank" title="{{ $item['title'] }}"
                                            href="{{ $item['content'] }}">
                                            <img src="{{ asset('storage') }}/photos/{{ $item['url'] }}"
                                                alt="{{ $item['title'] }}" />
                                        </a>
                                    @endif
                                </li>
                            @endforeach
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</footer>
<!-- END BODY -->

<!-- Tpl freeze footer -->
<!-- Active freeze footer by delete style display: none -->
<div class="freeze-footer">
    <ul>
        <li><a href="tel:{{ explode(',', $features[2]->desc)[0] }}" class="btn btn-default btn_call_now btn-call"
                title="Call us">{{ explode(',', $features[2]->desc)[0] }}</a>
        </li>
        <li><a href="/book" class="btn btn-default btn_make_appointment" title="Booking">Booking</a></li>
    </ul>
</div>
