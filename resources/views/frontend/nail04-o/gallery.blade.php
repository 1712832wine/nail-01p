<main class="main">

    <div class="pmain">
        <div class="page-heading">
            <div class="container">
                <div class="heading-inner">
                    <h2 class="mTitle">Gallery</h2>
                    <div class="hr-short"><span class="hr-inner"><span class="hr-inner-style"></span></span></div>
                </div>
            </div>
        </div>
        <div class="in-container">
            <div class="container">
                <div class="in-content">
                    <ul class="clearfix m-category-tab" id="category_tab">
                        @foreach ($albums as $index => $album)
                            <li class="tab @if ($index===0) active @endif" data-id="{{ $album['id'] }}">
                                <span itemprop="name">{{ $album['name'] }}</span>
                            </li>
                        @endforeach
                    </ul>
                    <div class="clearfix m-gallery-content" id="gallery_content">
                        <div class="clearfix m-gallery-listing listing"></div>
                        <div class="clearfix m-gallery-paging paging"></div>
                    </div>
                </div>
            </div>
        </div>
    </div><!-- .pmain-->
</main>
