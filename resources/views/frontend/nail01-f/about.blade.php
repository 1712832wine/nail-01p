<main>
    <div class="pmain">
        <section class="p-about">
            <div class="in-head">
                <div class="container">
                    <div class="in-headbg">
                        <h2 class="mTitle" itemprop="name">About us</h2>
                    </div>
                </div>
            </div>
            <div class="container">
                <div class="in-container">
                    <div class="in-content">
                        <div class="section-about about-page">
                            @foreach ($articles as $item)
                                <div class="row">
                                    <div class="col-md-6 col-xs-12">
                                        <div class="about-content">
                                            <h2>{{ $item['title'] }}</h2>
                                            {!! $item['content'] !!}
                                        </div>
                                    </div>
                                    <div class="col-md-6 col-xs-12">
                                        @if (count(json_decode($item['image'])) > 0)
                                            <div style="margin-top: 60px;">
                                                <img src="{{ asset('storage') }}/photos/{{ json_decode($item['image'])[0] }}"
                                                    alt="" />
                                            </div>
                                        @endif
                                    </div>
                                </div>
                            @endforeach
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
</main>
