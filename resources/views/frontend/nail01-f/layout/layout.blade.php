<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" dir="ltr" lang="en" xmlns:og="http://ogp.me/ns#"
    xmlns:fb="http://www.facebook.com/2008/fbml" itemscope itemtype="https://schema.org/NailSalon">

<head>
    <!-- Language -->
    <meta http-equiv="content-language" content="en" />
    <link rel="alternate" href="" hreflang="x-default">
    <link rel="alternate" href="" hreflang="en-US">

    <!-- Meta limit -->

    <!-- Meta Tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width,initial-scale=1.0" />
    <meta http-equiv="content-type" content="text/html; charset=UTF-8" />
    <meta name="description" content="" />
    <meta name="keywords" content="" />
    <meta name="author" content="" />

    <!-- OG -->
    <meta property="og:title" content="" />
    <meta property="og:description" content="" />
    <meta property="og:image" content="" />
    <meta property="og:url" content="/" />
    <meta property="og:type" content="website" />
    <meta property="og:site_name" content="Fnail01f Nails Spa" />

    <!-- Dublin Core -->
    <link rel="schema.DC" href="http://purl.org/dc/elements/1.1/">
    <meta name="DC.title" content="">
    <meta name="DC.identifier" content="/">
    <meta name="DC.description" content="">
    <meta name="DC.subject" content="">
    <meta name="DC.language" scheme="UTF-8" content="en-us">
    <meta itemprop="priceRange" name="priceRange" content="">

    <!-- GEO meta -->
    <meta name="geo.region" content="">
    <meta name="geo.placename" content="">
    <meta name="geo.position" content="">
    <meta name="ICBM" content="">

    <!-- Page Title -->
    <title>Fnail01f Nails Spa</title>
    <base href="frontend/themes/fnail01f/assets/">

    <!-- canonical -->
    <link rel="canonical" href="">



    <!-- Favicons -->
    <link rel="icon" href="/uploads/fnail016knrg4/attach/1562751789_fbm_fvc.png" type="image/x-icon">
    <link rel="shortcut icon" href="/uploads/fnail016knrg4/attach/1562751789_fbm_fvc.png" type="image/x-icon">

    <!-- Css -->
    <link href="https://fonts.googleapis.com/css?family=Quicksand:300,400,500,700&amp;subset=vietnamese"
        rel="stylesheet">


    <link rel="stylesheet" type="text/css" href='{{ asset('frontend') }}/public/library/jquery-ui/jquery-ui.min.css'>
    <link rel="stylesheet" type="text/css" href='{{ asset('frontend') }}/public/library/validation/validation.css'>
    <link rel="stylesheet" type="text/css" href='{{ asset('frontend') }}/public/library/animate/animate.css'>
    <link rel="stylesheet" type="text/css" href='{{ asset('frontend') }}/public/library/flex-label/flex-label.css'>
    <link rel="stylesheet" type="text/css"
        href='{{ asset('frontend') }}/public/library/font-awesome/v4.7.0/css/font-awesome.min.css'>
    <link rel="stylesheet" type="text/css"
        href='{{ asset('frontend') }}/public/library/bootstrap/v4.5.0/bootstrap.min.css'>
    <link rel="stylesheet" type="text/css"
        href='{{ asset('frontend') }}/public/library/bootstrap-datetimepicker/bootstrap-datetimepicker.min.css'>
    <link rel="stylesheet" type="text/css"
        href='{{ asset('frontend') }}/public/library/slider-pro/slider-pro.min.css'>
    <link rel="stylesheet" type="text/css"
        href='{{ asset('frontend') }}/public/library/owl-carousel/v2.2.1/owl.carousel.min.css'>
    <link rel="stylesheet" type="text/css"
        href='{{ asset('frontend') }}/public/library/magnific-popup/magnific-popup.css'>
    <link rel="stylesheet" type="text/css" href='{{ asset('frontend') }}/public/library/instagram/instafeed.min.css'>
    <link rel="stylesheet" type="text/css" href='{{ asset('frontend') }}/public/library/signature/signature.css'>
    <link rel="stylesheet" type="text/css" href='{{ asset('frontend') }}/public/library/pnotify/pnotify.css'>
    <link rel="stylesheet" type="text/css" href='{{ asset('frontend') }}/public/library/popover/popover.css'>
    <link rel="stylesheet" type="text/css" href='{{ asset('frontend') }}/themes/fnail01f/assets/css/meanmenu.css'>
    <link rel="stylesheet" type="text/css"
        href='{{ asset('frontend') }}/themes/fnail01f/assets/css/ie10-viewport-bug-workaround.css'>
    <link rel="stylesheet" type="text/css"
        href='{{ asset('frontend') }}/themes/fnail01f/assets/css/dropdown-submenu.css'>
    <link rel="stylesheet" type="text/css" href='{{ asset('frontend') }}/themes/fnail01f/assets/css/style.css'>
    <link rel="stylesheet" type="text/css" href='{{ asset('frontend') }}/themes/fnail01f/assets/css/responsive.css'>
    <style type="text/css">
        .style2 .header-top,
        .style2 .header:after,
        .style2 .navbar-nav .dropdown-menu,
        .style2 .section-service .icenter h3,
        .style2 .btn-primary,
        .style2 a.btn-primary,
        .style2 .table-cart .thead .table-head,
        .style2 .cart .order,
        .style2 .section-service .btn-booknow a,
        .style2 .footer-copyright,
        .style2 .gallery-heading {
            background-color: #189af1;
        }


        .style2 .btn-primary,
        .style2 a.btn-primary,
        .style2 .header-top,
        .style2 .header-top-right a,
        .style2 .footer .copyright,
        .style2 .section-service .btn-booknow a {
            color: #fff;
        }



        .box_img_giftcard:hover .circle_check,
        .box_img_giftcard.active .circle_check,
        .style2 .header:before,
        .style2 .footer-sologan span,
        .style2 .mobile-nav,
        .style2 .footer-btn {
            background-color: #006eb8;
        }




        .style2 .footer-container,
        .style2 .footer h3,
        .style2 .cart .order,
        .style2 .cart .order .total ul li span,
        .style2 .footer-btn,
        .style2 .gallery-heading .mTitle,
        .style2 .section-service .btn-booknow a {
            color: #ffffff;
        }



        .style2 .mTitle,
        .title_gift,
        .service-row h2 .style2 .section-about h2,
        .style2 .footer-sologan span,
        .style2 .section-about a,
        .style2 .contact-page h3,
        .style2 h1.service-name,
        .style2 .detail-price-number,
        .style2 .price-item-number,
        .style2 .navbar-nav>li>a:hover,
        .style2 .navbar-nav>li>a:active,
        .style2 .navbar-nav>li>a:focus,
        .style2 .navbar-nav>li.active>a,
        .style2 .navbar-nav>li:hover>a,
        .style2 .section-about .about-content a,
        .style2 .service-row h2,
        .service-name,
        .section-about h2,
        .style2 .section-service h3 {
            color: #189af1;
        }

        .style2 .about-map {
            border: 5px solid #189af1 !important;
        }

        .style2 .footer-sologan {
            background-image: url(images/footer_bg1_2.png), url(images/footer-bg2-2.png);
        }

        @media (max-width: 768px) {
            .style2 .footer-sologan {
                background-image: none;
            }
        }


        .style2 .section-service .btn-booknow a:hover,
        .style2 .footer-container,
        .style2 .btn-primary:focus,
        .style2 .btn-primary:hover,
        .style2 a.btn-primary:hover,
        .mean-container .mean-nav,
        .style2 .btn:hover {
            background-color: #006eb8;
            color: #ffffff;
        }

        .page-item.active .page-link,
        .pagination>li.active>a.page-link,
        .pagination>li>a:hover.page-link {
            background-color: #189af1;
            color: #fff;
            border: 1px solid #189af1;
        }

        .box_img_giftcard.active,
        .box_img_giftcard:hover {
            border-color: #189af1;
        }

    </style>
    <link rel="stylesheet" type="text/css" href='{{ asset('frontend') }}/public/library/global/css/news.css'>
    <link rel="stylesheet" type="text/css" href='{{ asset('frontend') }}/public/library/global/css/consent.css'>
    <link rel="stylesheet" type="text/css"
        href='{{ asset('frontend') }}/themes/fnail01f/assets/css/custom.css?v=1.5'>
    <style type="text/css">
        .logo img {
            max-height: 83px;
        }

        .navbar-nav>li>a {
            padding: 12px 16px;
        }

        .navbar-nav>li>a {
            padding: 12px 10px;
        }

        .logo-footer img {
            width: 296px;
        }

        .owl_service_board h3 a {
            color: #0363a4;
        }

        .owl_service_board h3 a:hover {
            color: #03c0df;
        }

        .in-head {
            background: url(/uploads/fnail016knrg4/filemanager/s71.png) 0px 95% no-repeat;
        }

        .booking_staff_title {
            display: none;
        }

        @media only screen and (max-width: 687px) {
            .footer-sologan span {
                font-size: 14px;
                color: #fff !important;
                font-weight: bold;
            }
        }

        .footer-sologan span {
            color: #fff !important;
            font-weight: bold;
        }

        .footer-copyright {
            padding: 14px 0 0;
            min-height: 50px;
            text-align: center;
        }

        @media only screen and (max-width: 687px) {
            .footer-copyright {
                padding: 10px 0 0;
                min-height: 46px;
                text-align: center;
            }
        }

        .in-head {
            background-size: cover !important;
        }

        .in-head:before {
            background-color: unset;
        }

        .mobile_logo img {
            max-height: inherit;
            width: 90%;
        }

        .mobile_logo {
            top: 30px;
        }

        .mobile_logo {
            align-items: center;
            display: block;
            left: 0;
            position: absolute;
            top: 2px;
            padding-left: inherit;
            z-index: 9999999;
            width: 80%;
        }

        .img-info-staff {
            display: none;
        }

        .section-service .cover {
            padding-bottom: 100%;
        }

        .in-head {
            background: #ccc;
            padding: 30px 0;
        }

    </style>

    <!-- Go to www.addthis.com/dashboard to customize your tools -->
    <script type="text/javascript" src="/s7.addthis.com/js/300/addthis_widget.js#pubid=ra-59b8fec5dd14e0f2"></script>
    <script type="text/javascript">
        var addthis_share = {
            url: "/",
            title: "Fnail01f Nails Spa",
            description: "",
            media: ""
        }

    </script>
    <!-- Script -->


    <script type="text/javascript" src="{{ asset('frontend') }}/public/library/jquery/v1.10.2/jquery.min.js">
    </script>
    <script type="text/javascript" src="{{ asset('frontend') }}/public/library/jquery-ui/jquery-ui.min.js"></script>
    <script type="text/javascript" src="{{ asset('frontend') }}/public/library/jquery-ui/jquery-migrate.min.js">
    </script>
    <script type="text/javascript" src="{{ asset('frontend') }}/public/library/validation/validation.min.js">
    </script>
    <script type="text/javascript" src="{{ asset('frontend') }}/public/library/mask/mask.min.js"></script>
    <script type="text/javascript" src="{{ asset('frontend') }}/public/library/flex-label/flex-label.js"></script>
    <script type="text/javascript" src="{{ asset('frontend') }}/public/library/bootstrap/v4.5.0/bootstrap.min.js">
    </script>
    <script type="text/javascript"
        src="{{ asset('frontend') }}/public/library/waiting-modal-dialog/waiting-modal-dialog.js"></script>
    <script type="text/javascript" src="{{ asset('frontend') }}/public/library/moment/moment-with-locales.js">
    </script>
    <script type="text/javascript"
        src="{{ asset('frontend') }}/public/library/bootstrap-datetimepicker/bootstrap-datetimepicker.js"></script>
    <script type="text/javascript" src="{{ asset('frontend') }}/public/library/magnific-popup/magnific-popup.min.js">
    </script>
    <script type="text/javascript" src="{{ asset('frontend') }}/public/library/pnotify/pnotify.js"></script>
    <script type="text/javascript" src="{{ asset('frontend') }}/public/library/slider-pro/slider-pro.min.js">
    </script>
    <script type="text/javascript"
        src="{{ asset('frontend') }}/public/library/owl-carousel/v2.2.1/owl.carousel.min.js"></script>
    <script type="text/javascript" src="{{ asset('frontend') }}/public/library/wow/wow.min.js"></script>
    <script type="text/javascript" src="{{ asset('frontend') }}/public/library/meanmenu/meanmenu.js"></script>
    <script type="text/javascript" src="{{ asset('frontend') }}/public/library/instagram/instafeed.min.js"></script>
    <script type="text/javascript" src="{{ asset('frontend') }}/public/library/signature/signature.min.js"></script>
    <script type="text/javascript" src="{{ asset('frontend') }}/themes/fnail01f/assets/js/responsiveslides.min.js">
    </script>
    <script type="text/javascript" src="{{ asset('frontend') }}/public/library/global/js/global.js"></script>
    <script type="text/javascript" src="{{ asset('frontend') }}/public/library/global/js/js_php.js"></script>

    <script type="text/javascript">
        let webForm = {
            "required": "(required)",
            "optional": "(optional)",
            "any_person": "Any person",
            "price": "Price",
            "morning": "Morning",
            "afternoon": "Afternoon",
            "sunday": "Sunday",
            "monday": "Monday",
            "tuesday": "Tuesday",
            "wednesday": "Wednesday",
            "thursday": "Thursday",
            "friday": "Friday",
            "saturday": "Saturday",
            "jan": "Jan",
            "feb": "Feb",
            "mar": "Mar",
            "apr": "Apr",
            "may": "May",
            "jun": "Jun",
            "jul": "Jul",
            "aug": "Aug",
            "sep": "Sep",
            "oct": "Oct",
            "nov": "Nov",
            "dec": "Dec",
            "contact_name": "Your name",
            "contact_name_placeholder": "",
            "contact_name_maxlength": "76",
            "contact_email": "Your email address",
            "contact_email_placeholder": "",
            "contact_email_maxlength": "76",
            "contact_phone": "Your phone",
            "contact_phone_placeholder": "",
            "contact_phone_maxlength": "16",
            "contact_subject": "Your subject",
            "contact_subject_placeholder": "",
            "contact_subject_maxlength": "251",
            "contact_message": "Your message",
            "contact_message_placeholder": "",
            "contact_message_maxlength": "501",
            "contact_btn_send": "Send Us",
            "contact_name_err": "Please enter your name",
            "contact_email_err": "Please enter your email",
            "contact_phone_err": "Please enter your phone",
            "contact_subject_err": "Please enter your subject",
            "contact_message_err": "Please enter your message",
            "booking_name": "Your name",
            "booking_name_placeholder": "",
            "booking_name_maxlength": "76",
            "booking_phone": "Your phone",
            "booking_phone_placeholder": "",
            "booking_phone_maxlength": "16",
            "booking_email": "Your email",
            "booking_email_placeholder": "",
            "booking_email_maxlength": "76",
            "booking_service": "Service",
            "booking_service_placeholder": "Select service",
            "booking_menu": "Menu",
            "booking_menu_placeholder": "Select menu",
            "booking_technician": "Technician",
            "booking_technician_placeholder": "Select technician",
            "booking_person_number": "Number",
            "booking_date": "Date",
            "booking_date_placeholder": "",
            "booking_date_maxlength": "16",
            "booking_hours": "Hour",
            "booking_hours_placeholder": "Select hour",
            "booking_note": "Note",
            "booking_note_maxlength": "201",
            "booking_note_placeholder": "(Max length 200 character)",
            "booking_store": "Storefront",
            "booking_store_placeholder": "Select storefront",
            "booking_add_another_service": "Add another service",
            "booking_information": "Appointment Information",
            "booking_order_information": "Order Information",
            "booking_popup_message": "Message",
            "booking_popup_confirm": "Confirm booking information ?",
            "booking_popup_confirm_description": "We will send a text message to you via the number below after we confirm the calendar for your booking.",
            "booking_order_popup_confirm": "Confirm order information ?",
            "booking_order_popup_confirm_description": "We will send a text message to you via the number below after we confirm the calendar for your order.",
            "booking_btn_send": "Send appointment now",
            "booking_btn_search": "Search",
            "booking_btn_booking": "Booking",
            "booking_btn_confirm": "Confirm",
            "booking_btn_cancel": "Cancel",
            "booking_hours_expired": "Has expired",
            "booking_name_err": "Please enter your name",
            "booking_phone_err": "Please enter your phone",
            "booking_email_err": "Please enter your email",
            "booking_service_err": "Please choose a service",
            "booking_menu_err": "Please choose a menu",
            "booking_technician_err": "Please choose a technician",
            "booking_date_err": "Please choose date",
            "booking_hours_err": "Please choose hour",
            "booking_get_hours_timeout": "Network timeout, Please click the button search to try again"
        };
        let webBooking = {
            enable: true,
            minDate: "05/24/2021",
            requiredTechnician: false,
            requiredEmail: false,
            requiredHour: true,
        };
        let webFormat = {
            dateFormat: "MM/DD/YYYY",
            datePosition: "1,0,2",
            phoneFormat: "(000) 000-0000",
        };
        let webGlobal = {
            site: "idx",
            siteAct: "",
            siteSubAct: "",
            noPhoto: "/public/library/global/no-photo.jpg",
            isTablet: false,
            isMobile: false,
            enableRecaptcha: false,
        };
        let facebook_embed = {
            id_fanpage: "https://www.facebook.com/FastboyMarketingAgency",
            appId: "",
            width: 450,
            height: 300,
            tabs: "timeline",
            show_facepile: true,
            small_header: true,
            likebox_enable: true,
            likebox_show_faces: false,
            likebox_stream: true,
        };
        let google_id_fanpage = "";
        let twitter_id_fanpage = "";

        // By This Theme
        var company_phone = "832-968-6668";
        var num_paging = "12";

    </script>

    <!-- Webmaster tools -->
    <!--Webmaster tools-->
    <!-- fb retargeting -->

    <!-- Schema Script -->
    <meta name="csrf-token" content="{{ csrf_token() }}" />
</head>

<body class="style2">
    <div class="art-main">

        <!-- Facebook Root And H1 Seo -->
        <h1 style="display: none">Fnail01f Nails Spa</h1>
        <div id="fb-root" style="display: none"></div>

        <!-- Tpl freeze header -->
        <input type="hidden" name="activeFreezeHeader" value="1" />
        @include('frontend.nail01-f.component.header',
        ['header'=>
        App\Models\Product::where('category_id',
        App\Models\Category::where('name','Header And Footer')->first()['id'])->first()])
        {{-- MAIN --}}
        {{ $slot }}
        {{-- FOOTER --}}
        @php
            $album = App\Models\Album::where([['type', 'Social'], ['status', 'PUBLISHED']])->first();
            $social = App\Models\Asset::where('albums_id', $album['id'])->orderBy('priority', 'desc')->get();
        @endphp
        @include('frontend.nail01-f.component.footer',['header'=>
        App\Models\Product::where('category_id',
        App\Models\Category::where('name','Header And Footer')->first()['id'])->first(),
        'social'=>$social]))

        <!-- popup -->

        <!-- AUTO REMOVE BOOKING BUTTON -->
        <script type="text/javascript">
            if (!webBooking.enable) {
                $(".btn_make_appointment").remove();
            }

        </script>

        <!-- external javascripts -->
        <script type="text/javascript" src="{{ asset('frontend') }}/public/library/global/js/global-init.js">
        </script>
        <script type="text/javascript" src="{{ asset('frontend') }}/public/library/global/js/consent.js"></script>
        <script type="text/javascript" src="{{ asset('frontend') }}/themes/fnail01f/assets/js/app.js?version=1.5">
        </script>
        <script type="text/javascript" src="{{ asset('frontend') }}/themes/fnail01f/assets/js/script.js?version=1.3">
        </script>

        <!-- Google analytics -->
        <script>
            (function(i, s, o, g, r, a, m) {
                i['GoogleAnalyticsObject'] = r;
                i[r] = i[r] || function() {
                    (i[r].q = i[r].q || []).push(arguments)
                }, i[r].l = 1 * new Date();
                a = s.createElement(o),
                    m = s.getElementsByTagName(o)[0];
                a.async = 1;
                a.src = g;
                m.parentNode.insertBefore(a, m)
            })(window, document, 'script', 'https://www.google-analytics.com/analytics.js', 'ga');
            ga('create', '', 'auto');
            ga('send', 'pageview');

        </script>

        <!-- gg adwords remarketing -->

        <!-- JS extends -->
        <script>
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

        </script>
        <script type="text/javascript"></script>
        @livewireScripts
    </div>
</body>

</html>
