<main>
    <div class="pmain">

        <div class="in-head">
            <div class="container">
                <div class="in-headbg">
                    <h2 class="mTitle" itemprop="name">Gallery</h2>
                </div>
            </div>
        </div>
        <div class="container">
            <div class="in-container">
                <div class="in-content">
                    <div class="list-gallery">
                        <div class="m-gallery-box-wrap">
                            <div class="row">
                                @foreach ($lists[0] as $item)
                                    <div class="col-xs-6 col-sm-6 col-md-4">
                                        <div class="pointer m-magnific-popup" data-group="gallery-4"
                                            title="Nails Design"
                                            href="{{ asset('storage') }}/photos/{{ $item['url'] }}">
                                            <div class="m-gallery-box">
                                                <div class="m-image-bg"
                                                    style="background-image: url('{{ asset('storage') }}/photos/{{ $item['url'] }}');">
                                                    <img itemprop="image"
                                                        src="{{ asset('storage') }}/photos/{{ $item['url'] }}"
                                                        alt="Nails Design">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                @endforeach
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12 text-center">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</main>
