<main>
    <div class="pmain">

        <section class="p-book">
            <!-- tpl main -->
            <div class="in-head">
                <div class="container">
                    <div class="in-headbg">
                        <h2 class="mTitle" itemprop="name">Booking</h2>
                    </div>
                </div>
            </div>
            <div class="container">
                <div class="in-container">
                    <div class="in-content">
                        <section id="boxBookingForm" class="box-booking-form">
                            <form id="formBooking" class="form-booking" name="formBooking" action="/book/add"
                                method="post" enctype="multipart/form-data">
                                <div class="row">
                                    <div class="col-sm-6 col-md-4 col-lg-3 booking-date">
                                        <div class="group-select">
                                            <label>Date (required)</label>
                                            <div class="relative w100 form-input-group">
                                                <input type="text" class="form-control form-text booking_date"
                                                    name="booking_date" value="" data-validation="[NOTEMPTY]"
                                                    data-validation-message="Please choose date" title="Booking date"
                                                    placeholder="" maxlength="16">
                                                <span class="fa fa-calendar calendar form-icon"></span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-sm-12 col-md-12 col-lg-9">
                                        <div class="row booking-service-staff booking-item" id="bookingItem_0">
                                            <div class="col-sm-12 col-md-7 col-lg-8 booking-service">
                                                <div class="group-select">
                                                    <label>Service (required)</label>
                                                    <div class="relative w100">
                                                        <select class="form-control booking_service" name="product_id[]"
                                                            data-validation="[NOTEMPTY]"
                                                            data-validation-message="Please choose a service"
                                                            title="Booking service">
                                                            <option value="">Select service</option>
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-sm-12 col-md-5 col-lg-4 booking-staff">
                                                <div class="group-select">
                                                    <label>Technician (optional)</label>
                                                    <div class="relative w100">
                                                        <select class="form-control booking_staff" name="staff_id[]"
                                                            title="Booking staff">
                                                            <option value="">Select technician</option>
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <!-- Button Add Item -->
                                        <div class="clearfix">
                                            <div class="add-services pointer booking_item_add">
                                                <i class="fa fa-plus-circle"></i> Add another service
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-12 col-md-12 col-lg-3">
                                        <div class="group-select">
                                            <label>&nbsp;</label>
                                            <div class="relative w100"><button class='btn btn-search search_booking'
                                                    type='button'>Search</button></div>

                                            <!-- Hidden data -->
                                            <input type="hidden" name="booking_hours" class="booking_hours" value="">
                                            <input type="hidden" name="booking_name" class="booking_name" value="">
                                            <input type="hidden" name="booking_phone" class="booking_phone" value="">
                                            <input type="hidden" name="booking_email" class="booking_email" value="">
                                            <input type="hidden" name="notelist" class="notelist" value="">
                                            <input type="hidden" name="store_id" class="store_id" value="">

                                            <input type="hidden" name="booking_area_code" class="booking_area_code"
                                                value="1">
                                            <input type="hidden" name="booking_form_email" class="booking_form_email"
                                                value="0" />
                                            <input type="hidden" name="nocaptcha" class="nocaptcha" value="1">
                                            <input type="hidden" name="g-recaptcha-response"
                                                class="g-recaptcha-response" value="">
                                        </div>
                                    </div>
                                </div>
                            </form>

                            <script type="text/javascript">
                                $(document).ready(function() {
                                    /* Init Booking */
                                    webBookingForm.init(
                                        '{"64":{"id":64,"name":"Manicure & Pedicure","services":[189,190,191,192,193,194,195,196]},"65":{"id":65,"name":"Manicure & Pedicure - Refill","services":[197,198,199,200,201]},"66":{"id":66,"name":"Manicure & Pedicure - Full Set","services":[202,203,204,205,206]},"67":{"id":67,"name":"Facial","services":[207,208]},"68":{"id":68,"name":"Additional","services":[209,210,211,212,213,214,215]},"69":{"id":69,"name":"Waxing","services":[216,217,218,219,220,221,222,223,224,225,226]}}',
                                        '{"189":{"id":189,"name":"Manicure","price":"$0 ","staffs":null,"schedule":false,"unit":"person","scheduleDay":null},"190":{"id":190,"name":"Pedicure","price":"$0 ","staffs":null,"schedule":false,"unit":"person","scheduleDay":null},"191":{"id":191,"name":"Manicure & Pedicure","price":"$0 ","staffs":null,"schedule":false,"unit":"person","scheduleDay":null},"192":{"id":192,"name":"French (Extra)","price":"$0 ","staffs":null,"schedule":false,"unit":"person","scheduleDay":null},"193":{"id":193,"name":"French Mani & Pedi","price":"$0 ","staffs":null,"schedule":false,"unit":"person","scheduleDay":null},"194":{"id":194,"name":"Gel Manicure","price":"$0 ","staffs":null,"schedule":false,"unit":"person","scheduleDay":null},"195":{"id":195,"name":"Gel French Manicure","price":"$0 ","staffs":null,"schedule":false,"unit":"person","scheduleDay":null},"196":{"id":196,"name":"Spa Pedicure","price":"$0 ","staffs":null,"schedule":false,"unit":"person","scheduleDay":null},"197":{"id":197,"name":"Full Set","price":"$0 ","staffs":null,"schedule":false,"unit":"person","scheduleDay":null},"198":{"id":198,"name":"Overlay","price":"$0 ","staffs":null,"schedule":false,"unit":"person","scheduleDay":null},"199":{"id":199,"name":"Pink & White","price":"$0 ","staffs":null,"schedule":false,"unit":"person","scheduleDay":null},"200":{"id":200,"name":"Gel Pink & White","price":"$0 ","staffs":null,"schedule":false,"unit":"person","scheduleDay":null},"201":{"id":201,"name":"Gel Powder","price":"$0 ","staffs":null,"schedule":false,"unit":"person","scheduleDay":null},"202":{"id":202,"name":"Full Set","price":"$0 ","staffs":null,"schedule":false,"unit":"person","scheduleDay":null},"203":{"id":203,"name":"Overlay","price":"$0 ","staffs":null,"schedule":false,"unit":"person","scheduleDay":null},"204":{"id":204,"name":"Pink & White","price":"$0 ","staffs":null,"schedule":false,"unit":"person","scheduleDay":null},"205":{"id":205,"name":"Gel Pink & White","price":"$0 ","staffs":null,"schedule":false,"unit":"person","scheduleDay":null},"206":{"id":206,"name":"Gel Powder","price":"$0 ","staffs":null,"schedule":false,"unit":"person","scheduleDay":null},"207":{"id":207,"name":"Full Facial","price":"$0 ","staffs":null,"schedule":false,"unit":"person","scheduleDay":null},"208":{"id":208,"name":"Facial Mini","price":"$0 ","staffs":null,"schedule":false,"unit":"person","scheduleDay":null},"209":{"id":209,"name":"Change Color Hands","price":"$0 ","staffs":null,"schedule":false,"unit":"person","scheduleDay":null},"210":{"id":210,"name":"Change Color Feet","price":"$0 ","staffs":null,"schedule":false,"unit":"person","scheduleDay":null},"211":{"id":211,"name":"Nail Design","price":"$0 ","staffs":null,"schedule":false,"unit":"person","scheduleDay":null},"212":{"id":212,"name":"Repair","price":"$0 ","staffs":null,"schedule":false,"unit":"person","scheduleDay":null},"213":{"id":213,"name":"Gel Color Change","price":"$0 ","staffs":null,"schedule":false,"unit":"person","scheduleDay":null},"214":{"id":214,"name":"Nail Take Off","price":"$0 ","staffs":null,"schedule":false,"unit":"person","scheduleDay":null},"215":{"id":215,"name":"Nail Take Off & Full Set","price":"$0 ","staffs":null,"schedule":false,"unit":"person","scheduleDay":null},"216":{"id":216,"name":"Eye Brow","price":"$0 ","staffs":null,"schedule":false,"unit":"person","scheduleDay":null},"217":{"id":217,"name":"Lip","price":"$0 ","staffs":null,"schedule":false,"unit":"person","scheduleDay":null},"218":{"id":218,"name":"Full Face","price":"$0 ","staffs":null,"schedule":false,"unit":"person","scheduleDay":null},"219":{"id":219,"name":"Chin","price":"$0 ","staffs":null,"schedule":false,"unit":"person","scheduleDay":null},"220":{"id":220,"name":"Back & Chest","price":"$0 ","staffs":null,"schedule":false,"unit":"person","scheduleDay":null},"221":{"id":221,"name":"Under Arm","price":"$0 ","staffs":null,"schedule":false,"unit":"person","scheduleDay":null},"222":{"id":222,"name":"Full Arm","price":"$0 ","staffs":null,"schedule":false,"unit":"person","scheduleDay":null},"223":{"id":223,"name":"Half Arm","price":"$0 ","staffs":null,"schedule":false,"unit":"person","scheduleDay":null},"224":{"id":224,"name":"Half Leg","price":"$0 ","staffs":null,"schedule":false,"unit":"person","scheduleDay":null},"225":{"id":225,"name":"Bikini","price":"$0 ","staffs":null,"schedule":false,"unit":"person","scheduleDay":null},"226":{"id":226,"name":"Brazilian","price":"$0 ","staffs":[4,5],"schedule":false,"unit":"person","scheduleDay":null}}',
                                        '{"4":{"id":4,"name":"Staff 1","note":"","image":"\/public\/library\/global\/no-photo.jpg","imageIsNo":true,"schedule":false,"scheduleDay":null},"5":{"id":5,"name":"Staff 2","note":"","image":"\/public\/library\/global\/no-photo.jpg","imageIsNo":true,"schedule":false,"scheduleDay":null},"6":{"id":6,"name":"Staff 3","note":"","image":"\/public\/library\/global\/no-photo.jpg","imageIsNo":true,"schedule":false,"scheduleDay":null}}',
                                        '{"listperson":[],"notelist":[],"booking_date":"05\/24\/2021","booking_hours":""}'
                                    );
                                });

                            </script>
                        </section>

                        <section id="boxBookingInfo" class="box-booking-info relative" style="display: none;">
                            <h3 class="booking-info-title">Appointment Information</h3>

                            <!-- Service, Staff -->
                            <div id="boxServiceStaff" class="box-service-staff">
                                <div class="service-staff">
                                    <div class="service-staff-avatar">
                                        <img class="img-responsive" src="/public/library/global/no-photo.jpg"
                                            alt="Any person">
                                    </div>
                                    <div class="service-staff-info">
                                        <h5>Any Technician</h5>
                                        <p>Any Service</p>
                                        <p>Price: N/A</p>
                                    </div>
                                </div>
                                <div class="service-staff">
                                    <div class="service-staff-avatar no-photo">
                                        <img class="img-responsive" src="/public/library/global/no-photo.jpg"
                                            alt="Any person">
                                    </div>
                                    <div class="service-staff-info">
                                        <h5>Any Technician</h5>
                                        <p>Any Service</p>
                                        <p>Price: N/A</p>
                                    </div>
                                </div>
                            </div>

                            <!-- Date, Time List -->
                            <div id="boxDateTime" class="box-date-time">
                                <h4 class="date-info" id="dateInfo">Sunday, Jan-01-1970</h4>
                                <div class="time-info">
                                    <h5>Morning <span class="time-note" id="timeAMNote">N/A</span></h5>
                                    <ul class="time-items" id="timeAMHtml"></ul>
                                </div>
                                <div class="time-info">
                                    <h5>Afternoon <span class="time-note" id="timePMNote">N/A</span></h5>
                                    <ul class="time-items" id="timePMHtml"></ul>
                                </div>
                            </div>
                        </section>

                        <section id="popupBookingConfirm"
                            class="popup-booking-confirm white-popup mfp-hide border-style">
                            <section id="boxBookingConfirm" class="box-booking-confirm relative">
                                <h3 class="booking-confirm-title">Confirm booking information ?</h3>
                                <div class="booking-confirm-note">We will send a text message to you via the
                                    number below after we confirm the calendar for your booking.</div>
                                <script src='https://www.google.com/recaptcha/api.js?hl=en' async defer>
                                </script><!-- Google reCaptcha -->
                                <script type="text/javascript">
                                    <!--
                                    function ezyCaptcha_formBookingConfirm(token, is_submit) {
                                        is_submit = 1;
                                        if ($("#password").length) {
                                            //$("input:password").val(md5(clean_input($("#password").val())));
                                        }
                                        return true;
                                    }
                                    //

                                    -->
                                </script>
                                <form id="formBookingConfirm" class="form-booking-confirm" name="formBookingConfirm"
                                    method="post" action="/book.html" enctype="multipart/form-data">
                                    <div class="row">
                                        <div class="col-md-6 booking-name">
                                            <div class="group-select">
                                                <label>Your name (required)</label>
                                                <div class="relative w100">
                                                    <input type="text" class="form-control" name="booking_name" value=""
                                                        data-validation="[NOTEMPTY]"
                                                        data-validation-message="Please enter your name" placeholder=""
                                                        maxlength="76" title="Booking name" />
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-6 booking-phone">
                                            <div class="group-select">
                                                <label>Your phone (required)</label>
                                                <div class="relative w100">
                                                    <input type="text" class="form-control inputPhone" pattern="\d*"
                                                        name="booking_phone" data-validation="[NOTEMPTY]"
                                                        data-validation-message="Please enter your phone" placeholder=""
                                                        maxlength="16" title="Booking phone" />
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-12 booking-email">
                                            <div style="display: none;" class="group-select">
                                                <label>Your email (optional)</label>
                                                <div class="relative w100">
                                                    <input type="text" class="form-control" name="booking_email"
                                                        value="" placeholder="" maxlength="76" title="Booking email" />
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-12 booking-notelist">
                                            <div class="group-select">
                                                <label>Note (optional)</label>
                                                <div class="relative w100">
                                                    <textarea class="form-control" rows="5" name="notelist"
                                                        placeholder="(Max length 200 character)" maxlength="201"
                                                        title="Booking note"></textarea>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-12 booking-store">
                                            <input type="hidden" name="choose_store" class="booking_store" value="0">
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-8 order-md-2 col-md-push-4">
                                            <button class="btn btn-confirm btn_confirm " type="button">Confirm</button>
                                        </div>
                                        <div class="col-md-4 order-md-1 col-md-pull-8">
                                            <button class="btn btn-cancel btn_cancel" type="button">Cancel</button>
                                        </div>
                                    </div>
                                </form>
                            </section>
                        </section>
                    </div>
                </div>
            </div>
        </section>
    </div>
</main>
