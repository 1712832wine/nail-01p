@php
$features = json_decode($header['features']);
$extras = json_decode($header['extras']);
@endphp
<footer class="footer">
    <div class="footer-container">
        <div class="container">
            <div class="row">
                <div class="col-md-3 col-lg-3 col-sm-6 col-xs-12">
                    <h3>Fnail01f Nails</h3>
                    <div class="footer-info clearfix">
                        <div class="logo-footer">
                            @if (count(json_decode($header['images'])) > 0)
                                <a itemprop="url" href="/">
                                    <img class="imgrps"
                                        src="{{ asset('storage') }}/photos/{{ json_decode($header['images'])[0] }}"
                                        alt="{{ $header['title'] }}" itemprop="logo image">
                                </a>
                            @endif
                        </div>
                        <div class="footer-info-right">
                            {!! $header['detail'] !!}
                            <div class="footer-bank"><a href="/">
                                    <img src="{{ asset('frontend') }}/themes/fnail01f/assets/images/icon-bank-1.png"
                                        class="imgrps" alt="" itemprop="image" /></a> <a href="/">
                                    <img src="{{ asset('frontend') }}/themes/fnail01f/assets/images/icon-bank-3.png"
                                        class="imgrps" alt="" itemprop="image" /></a> <a href="/">
                                    <img src="{{ asset('frontend') }}/themes/fnail01f/assets/images/icon-bank-4.png"
                                        class="imgrps" alt="" itemprop="image" /></a> <a href="/">
                                    <img src="{{ asset('frontend') }}/themes/fnail01f/assets/images/icon-bank-5.png"
                                        class="imgrps" alt="" itemprop="image" /></a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-3 col-lg-3 col-sm-6 col-xs-12">
                    <h3>Contact us</h3>
                    <div class="footer-address">
                        <div class="fadd"><i class="fa fa-home"></i>
                            <div class="fadd-info">
                                <h4>Address:</h4>
                                <p>{{ $features[0]->desc }}</p>
                            </div>
                        </div>
                        <div class="fadd"><i class="fa fa-phone"></i>
                            <div class="fadd-info">
                                <h4>Phone</h4>
                                <p>{{ $features[1]->desc }}</p>
                            </div>
                        </div>
                        <div class="fadd"><i class="fa fa-envelope"></i>
                            <div class="fadd-info">
                                <h4>Email:</h4>
                                <p>{{ $features[2]->desc }}</p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-3 col-lg-3 col-sm-6 col-xs-12">
                    <div class="footer-open-hours">
                        <h3>Open hours</h3>
                        <!-- render.openhours_data.layouts, render.openhoursshort_data.layouts -->
                        <div class="foh-wrap">
                            @foreach ($extras as $item)
                                <div class="foh-row" itemprop="openingHours" content="Mon 10:00 am - 7:00 pm">
                                    <span class="foh-date">{{ $item->name }}</span>
                                    <span class="foh-time">{{ $item->desc }}</span>
                                </div>
                            @endforeach
                        </div>
                    </div>
                </div>
                <div class="col-md-3 col-lg-3 col-sm-6 col-xs-12">
                    <h3>Connect us</h3>
                    <div class="footer-facebook">
                        @foreach ($social as $item)
                            @if ($item['url'])
                                <a itemprop="url" class="social-img-icons" rel="nofollow" target="_blank"
                                    title="{{ $item['title'] }}" href="{{ $item['content'] }}"><img
                                        src="{{ asset('storage') }}/photos/{{ $item['url'] }}"
                                        alt="{{ $item['title'] }}"></a>
                            @endif
                        @endforeach
                        <aside>
                            <div id="fanpage_fb_container"></div>
                            <div id="social_block_width" style="width:100% !important; height: 1px !important">
                            </div>
                        </aside>
                    </div>
                </div>
            </div>
        </div>
        <div class="footer-sologan"><span>Professional Nail Care for Ladies and Gentlemen</span></div>
    </div>
    <div class="footer-copyright clearfix">
        <div class="container">
            <p class="copyright">Copyright © 2019. All Rights Reserved.</p>
        </div>
    </div><!-- .footer-container-->
</footer>
<!-- Tpl freeze footer -->
<!-- Active freeze footer by delete style display: none -->
<div class="freeze-footer">
    <ul>
        <li>
            <a href="tel:{{ $features[1]->desc }}" class="btn btn-default btn_call_now btn-call" title="Call us">
                <div class="icon"><i class="fa fa-phone" aria-hidden="true"></i></div>
            </a>
        </li>
        <li>
            <a href="/book" class="btn btn-default btn_make_appointment" title="Booking">
                <div class="icon"><i class="fa fa-calendar" aria-hidden="true"></i></div>
            </a>
        </li>
    </ul>
</div>
