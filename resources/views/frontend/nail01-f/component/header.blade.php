@php
$features = json_decode($header['features']);
$extras = json_decode($header['extras']);
@endphp
<header>
    <div class="wrap-freeze-header clearfix hidden-xs hidden-sm">
        <div class="flag-freeze-header">
            <div class="header">
                <div class="header-main">
                    <div class="container">
                        <div class="row">
                            <div class="col-sm-4">
                                <h1 class="logo wow bounceInLeft">
                                    @if (count(json_decode($header['images'])) > 0)
                                        <a itemprop="url" href="/">
                                            <img class="imgrps"
                                                src="{{ asset('storage') }}/photos/{{ json_decode($header['images'])[0] }}"
                                                alt="{{ $header['title'] }}" itemprop="logo image">
                                        </a>
                                    @endif
                                </h1>
                            </div>
                            <div class="col-sm-8">
                                <div class="header-top hidden-xs hidden-sm">
                                    <div class="header-top-left"><i
                                            class="fa fa-map-marker"></i>{{ $features[0]->desc }}
                                    </div>
                                    <div class="header-top-right"><i class="fa fa-phone"></i> <a
                                            href="tel:{{ $features[1]->desc }}">{{ $features[1]->desc }}</a>
                                    </div>
                                </div><!-- .header-top -->

                                <nav class="navbar navbar-expand-lg">
                                    <div class="collapse navbar-collapse wow fadeInUp">
                                        <ul class="navbar-nav">
                                            <li class="left" itemprop="name"><a href="/" itemprop="url">Home</a>
                                            </li>
                                            <li class="left" itemprop="name"><a href="/about" itemprop="url">About
                                                    Us</a></li>
                                            <li class="left" itemprop="name"><a href="/services"
                                                    itemprop="url">Services</a></li>
                                            <li class="right" itemprop="name"><a itemprop="url" href="/book">Booking</a>
                                            </li>
                                            <li class="right" itemprop="name"><a href="/coupons"
                                                    itemprop="url">Coupons</a></li>
                                            <li class="right" itemprop="name"><a href="/giftcards"
                                                    itemprop="url">Giftcards</a></li>
                                            <li class="right" itemprop="name"><a href="/gallery"
                                                    itemprop="url">Gallery</a></li>
                                            <li class="right" itemprop="name"><a href="/contact" itemprop="url">Contact
                                                    Us</a></li>
                                        </ul>
                                    </div>
                                </nav>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="wrap-freeze-header-mobile clearfix hidden-md hidden-lg">
        <div class="flag-freeze-header-mobile">
            <div class="header">
                <div class="mobile-nav">
                    <div class="mobile_logo">

                        @if (count(json_decode($header['images'])) > 0)
                            <a itemprop="url" href="/">
                                <img class="imgrps"
                                    src="{{ asset('storage') }}/photos/{{ json_decode($header['images'])[0] }}"
                                    alt="{{ $header['title'] }}" itemprop="logo image">
                            </a>
                        @endif
                    </div>
                    <div class="menu_mobile_v1 hidden-md hidden-lg menu-1024-hidden">
                        <div class="mobile_menu_container_v1">
                            <div class="mobile-menu clearfix">
                                <nav id="mobile_dropdown">
                                    <ul>
                                        <li class="left" itemprop="name"><a href="/" itemprop="url">Home</a>
                                        </li>
                                        <li class="left" itemprop="name"><a href="/about" itemprop="url">About
                                                Us</a></li>
                                        <li class="left" itemprop="name"><a href="/services" itemprop="url">Services</a>
                                        </li>
                                        <li class="right" itemprop="name"><a itemprop="url" href="/book">Booking</a>
                                        </li>
                                        <li class="right" itemprop="name"><a href="/coupons" itemprop="url">Coupons</a>
                                        </li>
                                        <li class="right" itemprop="name"><a href="/giftcards"
                                                itemprop="url">Giftcards</a></li>
                                        <li class="right" itemprop="name"><a href="/gallery" itemprop="url">Gallery</a>
                                        </li>
                                        <li class="right" itemprop="name"><a href="/contact" itemprop="url">Contact
                                                Us</a></li>
                                    </ul>
                                </nav>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</header>
