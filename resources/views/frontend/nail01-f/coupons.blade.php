<main>
    <div class="pmain">

        <section class="p-coupons">
            <!-- tpl main -->
            <div class="in-head">
                <div class="container">
                    <div class="in-headbg">
                        <h2 class="mTitle" itemprop="name">Coupons</h2>
                    </div>
                </div>
            </div>
            <div class="container">
                <div class="in-container">
                    <div class="in-content">
                        <div class="row">
                            @foreach ($list as $item)
                                <div class="col-sm-6 col-md-6 cards-item ">
                                    <a class="coupon_img_v1 pointer  image-magnific-popup" data-group=""
                                        href="{{ asset('storage') }}/photos/{{ $item['url'] }}" title="Coupon">
                                        <img class="cards-item-image img-responsive"
                                            src="{{ asset('storage') }}/photos/{{ $item['url'] }}" alt="Coupon">
                                    </a>
                                </div>
                            @endforeach
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
</main>
