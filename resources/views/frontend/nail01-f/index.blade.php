<main>
    <div class="pmain">
        {{-- CAROUSEL --}}
        <div class="section-slider">
            <div class="slider-pro" id="my-slider">
                <div class="sp-slides">
                    @foreach ($imgs_carousel as $img)
                        <div class="sp-slide">
                            <div class="sp-layer">
                                <img class="sp-image" src="{{ asset('storage') }}/photos/{{ $img['url'] }}"
                                    itemprop="image">
                            </div>
                        </div>
                    @endforeach
                </div>
            </div>
        </div>
        <div class="container">
            {{-- ABOUT ARTICLE --}}
            <div class="section-about">
                <div class="row">
                    <div class="col-md-6 col-lg-5">
                        <div class="about-content">
                            <h2 class="wow fadeInUp">About Us</h2>
                            {!! $home_article['content'] !!}
                        </div>
                    </div>
                    <div class="col-md-6 col-lg-3">
                        <div class="about-video"><iframe width="300" height="200"
                                src="{{ $home_article['description'] }}" frameborder="0"
                                allowfullscreen="allowfullscreen"></iframe></div>
                    </div>
                    <div class="col-md-12 col-lg-4">
                        <div class="about-map">
                            <section class="box_map clearfix">
                                <div class="google-map-wrapper">
                                    <div class="google-map" style="width:100%; height:auto;"><iframe
                                            src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3464.7391505606774!2d-95.57299108550652!3d29.727314340440024!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x8640ddc6ed681049%3A0x74ace03ed50728e8!2sFast+Boy+Marketing!5e0!3m2!1sen!2s!4v1510909605272"
                                            width="100%" height="450" frameborder="0" style="border:0"
                                            allowfullscreen></iframe></div>
                                </div>
                            </section>
                        </div>
                    </div>
                </div>
            </div>
            {{-- SERVICES --}}
            <div class="section-service section">
                <div class="section-content">
                    <h2 class="mTitle wow fadeInUp">Services</h2>
                    <div class="section-service">
                        <div class="row">
                            @foreach ($services as $service)
                                <div class="break-row break-row-1"></div>
                                <div class="col-md-3 col-sm-6">
                                    <div class="item">
                                        <a href="{{ route('services-detail', ['slug' => strtolower(trim(preg_replace('/[^A-Za-z0-9-]+/', '-', $service['name']))), 'service_id' => $service['id']]) }}"
                                            class="cover"
                                            style="background-image: url('{{ asset('storage') }}/photos/{{ json_decode($service['images'])[0] }}');">
                                            <img style="display: none" class="imgrps"
                                                src="{{ asset('storage') }}/photos/{{ json_decode($service['images'])[0] }}"
                                                alt="{{ $service['name'] }}" itemprop="image">
                                        </a>
                                        <h3><a itemprop="url"
                                                href="{{ route('services-detail', ['slug' => strtolower(trim(preg_replace('/[^A-Za-z0-9-]+/', '-', $service['name']))), 'service_id' => $service['id']]) }}">{{ $service['name'] }}</a>
                                        </h3>
                                        <div class="sumarry">
                                        </div>
                                        <div><a class="btn btn-primary"
                                                href="{{ route('services-detail', ['slug' => strtolower(trim(preg_replace('/[^A-Za-z0-9-]+/', '-', $service['name']))), 'service_id' => $service['id']]) }}">View
                                                more</a></div>
                                    </div>
                                </div>
                            @endforeach
                        </div>
                    </div>


                </div>
            </div>
        </div>
        {{-- GALLERY --}}
        <div class="section-gallery">
            <div class="gallery-heading wow fadeInUp">
                <h2 class="mTitle">Our Gallery</h2>
            </div>
            <div class="list-gallery">
                <div class="m-gallery-box-wrap">
                    <div class="row">
                        @foreach ($gallery_list as $item)
                            <div class="col-6 col-sm-4 col-md-3">
                                <div class="pointer m-magnific-popup" data-group="gallery-4" title="Nails Design"
                                    href="{{ asset('storage') }}/photos/{{ $item['url'] }}">
                                    <div class="m-gallery-box">
                                        <div class="m-image-bg"
                                            style="background-image: url('{{ asset('storage') }}/photos/{{ $item['url'] }}');">
                                            <img itemprop="image"
                                                src="{{ asset('storage') }}/photos/{{ $item['url'] }}"
                                                alt="Nails Design">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        @endforeach
                    </div>
                </div>
            </div>
            <div class="container"></div>
        </div>
    </div>
</main>
