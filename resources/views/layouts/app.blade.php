<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Fonts -->
    <link rel="stylesheet" href="https://fonts.googleapis.com/css2?family=Nunito:wght@400;600;700&display=swap">

    <!-- Styles -->
    <link rel="stylesheet" href="{{ mix('css/app.css') }}">
    {{-- Styles --}}
    <link rel="stylesheet" type="text/css"
        href="https://demo.backpackforlaravel.com/packages/backpack/base/css/bundle.css?v=4.1.40@ecbe7676535d3c9121227329d62cad9978d29fee">
    <link rel="stylesheet" type="text/css"
        href="https://demo.backpackforlaravel.com/packages/source-sans-pro/source-sans-pro.css?v=4.1.40@ecbe7676535d3c9121227329d62cad9978d29fee">
    <link rel="stylesheet" type="text/css"
        href="https://demo.backpackforlaravel.com/packages/line-awesome/css/line-awesome.min.css?v=4.1.40@ecbe7676535d3c9121227329d62cad9978d29fee">
    <link rel="stylesheet" type="text/css"
        href="https://demo.backpackforlaravel.com/packages/datatables.net-bs4/css/dataTables.bootstrap4.min.css">
    <link rel="stylesheet" type="text/css"
        href="https://demo.backpackforlaravel.com/packages/datatables.net-fixedheader-bs4/css/fixedHeader.bootstrap4.min.css">
    <link rel="stylesheet" type="text/css"
        href="https://demo.backpackforlaravel.com/packages/datatables.net-responsive-bs4/css/responsive.bootstrap4.min.css">
    <link rel="stylesheet"
        href="https://demo.backpackforlaravel.com/packages/backpack/crud/css/crud.css?v=4.1.40@ecbe7676535d3c9121227329d62cad9978d29fee">
    <link rel="stylesheet"
        href="https://demo.backpackforlaravel.com/packages/backpack/crud/css/form.css?v=4.1.40@ecbe7676535d3c9121227329d62cad9978d29fee">
    <link rel="stylesheet"
        href="https://demo.backpackforlaravel.com/packages/backpack/crud/css/list.css?v=4.1.40@ecbe7676535d3c9121227329d62cad9978d29fee">
    <link rel="stylesheet"
        href="https://unpkg.com/filepond-plugin-image-preview/dist/filepond-plugin-image-preview.min.css">
    <link rel="stylesheet" href="https://unpkg.com/filepond/dist/filepond.min.css">
    @livewireStyles

    <!-- Scripts -->
    <script src="{{ mix('js/app.js') }}" defer></script>
    {{-- Scripts --}}
    <script src="https://cdn.jsdelivr.net/gh/alpinejs/alpine@v2.8.2/dist/alpine.min.js" defer></script>
    <script type="text/javascript"
        src="https://demo.backpackforlaravel.com/packages/backpack/base/js/bundle.js?v=4.1.40@ecbe7676535d3c9121227329d62cad9978d29fee">
    </script>
    <script type="text/javascript"
        src="https://demo.backpackforlaravel.com/packages/datatables.net/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@9"></script>
    @livewireStyles
</head>



<body class="pace-done pace-running font-sans antialiased sidebar-lg-show">

    <div class="flex" style="justify-content:space-between">
        <!-- Sidebar -->
        @livewire('sidebar')
        <x-jet-banner />
        <div class="min-h-screen bg-gray-100 w-100">
            @livewire('navigation-menu')
            <!-- Page Heading -->
            <div class="flex">
                <!-- Page Content -->
                <main class="w-100">
                    {{ $slot }}
                </main>
            </div>
        </div>
    </div>

    {{-- SWAL --}}
    <script>
        const SwalModal = (icon, title, html) => {
            Swal.fire({
                icon,
                title,
                html
            })
        }

        const SwalConfirm = (icon, title, html, confirmButtonText, method, params, callback) => {
            Swal.fire({
                icon,
                title,
                html,
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText,
                reverseButtons: true,
            }).then(result => {
                if (result.value) {
                    return livewire.emit(method, params)
                }

                if (callback) {
                    return livewire.emit(callback)
                }
            })
        }

        const SwalAlert = (icon, title, timeout = 7000) => {
            const Toast = Swal.mixin({
                toast: true,
                position: 'top-end',
                showConfirmButton: false,
                timer: timeout,
                onOpen: toast => {
                    toast.addEventListener('mouseenter', Swal.stopTimer)
                    toast.addEventListener('mouseleave', Swal.resumeTimer)
                }
            })

            Toast.fire({
                icon,
                title
            })
        }

        document.addEventListener('DOMContentLoaded', () => {
            this.livewire.on('swal:modal', data => {
                SwalModal(data.icon, data.title, data.text)
            })

            this.livewire.on('swal:confirm', data => {
                SwalConfirm(data.icon, data.title, data.text, data.confirmText, data.method, data
                    .params, data.callback)
            })

            this.livewire.on('swal:alert', data => {
                SwalAlert(data.icon, data.title, data.timeout)
            })
        })
    </script>
    {{-- CKEDITOR --}}
    <script src="{{ asset('packages') }}/ckeditor/ckeditor.js"></script>
    {{--  --}}
    <script src="https://unpkg.com/filepond-plugin-file-encode/dist/filepond-plugin-file-encode.min.js"></script>
    <script src="https://unpkg.com/filepond-plugin-file-validate-size/dist/filepond-plugin-file-validate-size.min.js">
    </script>
    <script
        src="https://unpkg.com/filepond-plugin-image-exif-orientation/dist/filepond-plugin-image-exif-orientation.min.js">
    </script>
    <script src="https://unpkg.com/filepond-plugin-image-preview/dist/filepond-plugin-image-preview.min.js"></script>
    <script src="https://unpkg.com/filepond/dist/filepond.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.6.0/jquery.min.js"
        integrity="sha512-894YE6QWD5I59HgZOGReFYm4dnWc1Qt5NtvYSaNcOP+u1T9qYdvdihz0PPSiiqn/+/3e7Jo4EaG7TubfWGUrMQ=="
        crossorigin="anonymous" referrerpolicy="no-referrer"></script>
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"
        integrity="sha256-T0Vest3yCU7pafRw9r+settMBX6JkKN06dqBnpQ8d30=" crossorigin="anonymous"></script>

    @livewireScripts
    @stack('modals')
</body>

</html>
