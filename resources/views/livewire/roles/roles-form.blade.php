@php
$title = 'roles';
$title2 = 'role';
@endphp
<main class="main pt-2">
    @include('livewire.component.header-form',[$title, $type])
    <div class="container-fluid animated fadeIn">



        <div class="row">
            <div class="col-md-8 bold-labels">
                <!-- Default box -->
                <form>
                    <div class="card">
                        <div class="card-body row">
                            <div class="form-group col-sm-12 required"> <label for="name">Name</label>
                                <input type="text" id="name" class="form-control" wire:model.defer="name">
                                @error('name')
                                    <span class="error text-danger">{{ $message }}</span>
                                @enderror
                            </div>
                            <div class="form-group col-sm-12">
                                <label>Permissions</label>
                                <input type="hidden" value="[]" name="permissions">
                                <div class="row">
                                    @foreach ($Permission_values as $Permission_value)
                                        <div class="col-sm-4">
                                            <div class="checkbox">
                                                <label class="font-weight-normal">
                                                    <input type="checkbox"
                                                        number_columns="{{ count($Permission_values) }}"
                                                        value="{{ $Permission_value->name }}"
                                                        wire:model.defer="permissions">
                                                    {{ $Permission_value->name }}
                                                </label>
                                            </div>
                                        </div>
                                    @endforeach

                                </div>
                            </div>
                        </div>
                    </div>
                    @include('livewire.component.saveaction-form')
                </form>
            </div>
        </div>
    </div>
</main>
