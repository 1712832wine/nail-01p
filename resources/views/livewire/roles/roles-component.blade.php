@if ($isOpen)
    @include('livewire.roles.roles-form')
@else
    @php
        $breadcrumb = [
            'items' => [
                [
                    'href' => route('dashboard'),
                    'title' => 'dashboard',
                ],
                [
                    'href' => route('roles'),
                    'title' => 'roles',
                ],
            ],
            'active' => 'List',
        ];
        $header = [
            'title' => 'Roles',
            'reset' => route('roles'),
        ];
        $button = 'Add role';
        $table_header = ['Name', 'Users', 'Actions'];
        $table_body = ['name', 'users'];

    @endphp
    @include('livewire.component.main-component',[$breadcrumb, $header, $button, $table_header, $table_body,$table_actions=[],$selectBox=false])
@endif
