@php
$title = 'menu items';
$title2 = 'menu item';
@endphp
<main class="main pt-2">
    @include('livewire.component.header-form',[$title, $type])
    <div class="container-fluid animated fadeIn">
        <div class="row">
            <div class="col-md-8 bold-labels">
                <form>
                    <div class="card">
                        <div class="card-body row">
                            {{-- Input text --}}
                            <div class="form-group col-sm-12">
                                <label for="label">Label</label>
                                <input type="text" id="label" class="form-control" wire:model.defer="label">
                            </div>
                            {{-- Select --}}
                            <div class="form-group col-sm-12">
                                <label for="parent">Parent</label>
                                <select value="" class="form-control" wire:model.defer="parent" id="parent">
                                    <option value="0">-</option>
                                    <option value="1">Est</option>
                                    <option value="2">Magni</option>
                                    <option value="3">Aut</option>
                                    <option value="4">Nulla adipisci sit</option>
                                    <option value="5">Architecto quia</option>
                                    <option value="6">Repudiandae quaerat</option>
                                    <option value="7">Libero aut</option>
                                </select>
                            </div>
                            <div class="form-group col-sm-12">
                                <label for="type">Type</label>
                                <div class="row" data-initialized="true">
                                    <div class="col-sm-3">
                                        <select id="type" class="form-control" wire:model.defer="type_input"
                                            value="page_link">
                                            <option value="page_link">Page link</option>
                                            <option value="internal_link">Internal link</option>
                                            <option value="external_link">External link</option>
                                        </select>
                                    </div>
                                    <div class="col-sm-9">
                                        <!-- page link -->
                                        @if ($type_input === 'page_link')
                                            <div class="page_or_link_value page_or_link_page">
                                                <select class="form-control" id="page_id" wire:model.defer="link">
                                                    <option value="1">Neque ut</option>
                                                    <option value="2">Doloribus ad qui</option>
                                                    <option value="3">Accusantium accusantium reprehenderit</option>
                                                    <option value="4">Ea</option>
                                                    <option value="5">Perspiciatis et in</option>
                                                    <option value="6">Qui dolore</option>
                                                    <option value="7">Magni sapiente</option>
                                                    <option value="8">Enim qui</option>
                                                    <option value="9">Consectetur velit vel</option>
                                                    <option value="10">Consequuntur harum</option>
                                                    <option value="11">Et</option>
                                                    <option value="12">Pariatur voluptatem commodi</option>
                                                    <option value="13">Qui laborum</option>
                                                    <option value="14">Eum</option>
                                                    <option value="15">Sed consequatur</option>
                                                    <option value="16">Molestiae veritatis</option>
                                                </select>
                                            </div>
                                        @endif
                                        <!-- internal link input -->
                                        @if ($type_input === 'internal_link')
                                            <div class="page_or_link_value page_or_link_internal_link">
                                                <input type="text" class="form-control"
                                                    placeholder="Internal slug. Ex: 'admin/page' (no quotes) for ':url'"
                                                    wire:model.defer="link">
                                            </div>
                                        @endif
                                        <!-- external link input -->
                                        @if ($type_input === 'external_link')
                                            <div class="page_or_link_value page_or_link_external_link">
                                                <input type="url" class="form-control" name="link"
                                                    placeholder="http://example.com/your-desired-page"
                                                    wire:model.defer="link">
                                            </div>
                                        @endif
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    @include('livewire.component.saveaction-form')
                </form>
            </div>
        </div>
    </div>
</main>
