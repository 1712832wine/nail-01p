<div class="row mb-0">
    <div class="col-sm-6">
        <div class="d-print-none with-border">
            <button wire:click.prevent="create('create')" class="btn btn-primary" data-style="zoom-in">
                <span class="ladda-label">
                    <i class="la la-plus"></i> {{ $button }}
                </span>
            </button>
        </div>
    </div>
    <div class="col-sm-6">
        <div id="datatable_search_stack" class="mt-sm-0 mt-2 d-print-none">
            <div id="crudTable_filter" class="dataTables_filter">
                <label>
                    <input type="search" class="form-control" placeholder="Search..."
                        wire:model.debounce.250ms="search">
                </label>
            </div>
        </div>
    </div>
</div>
