<main class="main pt-2">
    @include('livewire.component.breadcrumb', $breadcrumb)
    @include('livewire.component.header-component', $header)
    <div class="container-fluid animated fadeIn">
        <!-- Default box -->
        <div class="row">
            <!-- THE ACTUAL CONTENT -->
            <div class="col-md-12">
                @include('livewire.component.addbtn-and-searchbox', [$button])
                <div class="dataTables_wrapper dt-bootstrap4">
                    @include('livewire.component.table',[$table_header, $table_body ])
                    @if ($selectBox)

                    @include('livewire.component.selected-action')
                    @endif
                    @include('livewire.component.pagination')
                </div>
            </div>
        </div>
    </div>
</main>
