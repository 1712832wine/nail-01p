<nav class="d-none d-lg-block">
    <ol class="breadcrumb bg-transparent p-0 justify-content-end">
        @foreach ($breadcrumb['items'] as $item)
            <li class="breadcrumb-item text-capitalize">
                <a href="{{ $item['href'] }}">{{ $item['title'] }}</a>
            </li>
        @endforeach
        <li class="breadcrumb-item text-capitalize active">{{ $breadcrumb['active'] }}</li>
    </ol>
</nav>
