  @if ($isOpen)
      @include('livewire.articles.articles-form',['categories' => App\Models\Category::all(),'tags'
      =>App\Models\Tag::all()])
  @else
      @php
          $breadcrumb = [
              'items' => [
                  [
                      'href' => route('dashboard'),
                      'title' => 'dashboard',
                  ],
                  [
                      'href' => route('articles'),
                      'title' => 'Articles',
                  ],
              ],
              'active' => 'List',
          ];
          $header = [
              'title' => 'Articles',
              'reset' => route('articles'),
          ];
          $button = 'Add article';
          $table_header = ['Title', 'Category', 'Status', 'Actions'];
          $table_body = ['title', 'category', 'status'];
          $table_actions = ['clone'];

      @endphp
      @include('livewire.component.main-component',[$breadcrumb, $header, $button, $table_header, $table_body,$table_actions,$table_actions=['clone'],$selectBox=true])
  @endif
  <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"
            integrity="sha256-T0Vest3yCU7pafRw9r+settMBX6JkKN06dqBnpQ8d30=" crossorigin="anonymous"></script>
        <script>
            // var fixHelperModified = function(e, tr) {
            //         var $originals = tr.children();
            //         var $helper = tr.clone();
            //         $helper.children().each(function(index) {
            //             $(this).width($originals.eq(index).width())
            //         });
            //         return $helper;
            //     },
            //     updateIndex = function(e, ui) {
            //         // console.log("ui", ui.item.attr("key"))
            //         $('td.index', ui.item.parent()).each(function(i) {
            //             $(this).html(i + 1);
            //         });
            //         $('input[type=text]', ui.item.parent()).each(function(i) {
            //             $(this).val(i + 1);
            //         });
            //     };

            // $("#myTable tbody").sortable({
            //     helper: fixHelperModified,
            //     stop: updateIndex,
            // }).disableSelection();
            $("tbody").sortable({
                distance: 5,
                delay: 100,
                opacity: 0.6,
                placeholder: 'highlight',
                cursor: 'move',
                update: function(e, ui) {
                    var sortData = $("#myTable tbody").sortable('toArray', {
                        attribute: 'data-id'
                    })
                    @this.confirmChangePriority(sortData);
                }
            });

        </script>

        <style>
            .dragged {
                position: absolute;
                opacity: 0.5;
                z-index: 2000;
                background: black;
            }

        </style>
