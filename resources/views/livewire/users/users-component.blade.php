@if ($isOpen)
    @include('livewire.users.users-form')
@else
    @php
        $breadcrumb = [
            'items' => [
                [
                    'href' => route('dashboard'),
                    'title' => 'dashboard',
                ],
                [
                    'href' => route('users'),
                    'title' => 'users',
                ],
            ],
            'active' => 'List',
        ];
        $header = [
            'title' => 'Users',
            'reset' => route('users'),
        ];
        $button = 'Add user';
        $table_header = ['Name', 'Email', 'Actions'];
        $table_body = ['name', 'email'];

    @endphp
    @include('livewire.component.main-component',[$breadcrumb, $header, $button, $table_header, $table_body,$table_actions=[],$selectBox=false])
@endif
