@php
$title = 'Albums';
$title2 = 'album';
$type = 'edit';
@endphp
<main class="main pt-2">
    @include('livewire.component.header-form',[$title, $type])
    <div class="container-fluid animated fadeIn" >
        <div class="row">
            <div class="col-md-8 bold-labels">
                <!-- Default box -->
                <form>
                    <div class="card">
                        <div class="card-body row">
                            <!-- text input -->
                            <div class="form-group col-sm-12 required">
                                <label for="name">Name</label>
                                <input type="text" id="name" wire:model.defer="name" class="form-control">
                                @error('name')
                                    <span class="error text-danger">{{ $message }}</span>
                                @enderror
                            </div>

                            <div class="form-group col-sm-12">
                                <label for="title">Title</label>
                                <input type="text" id="title" wire:model.defer="title" class="form-control">
                            </div>
                            <!-- text input -->
                            <div class="form-group col-sm-12">
                                <label for="short_desc">Short_desc</label>
                                <input type="text" id="short_desc" wire:model.defer="short_desc" class="form-control">
                            </div>
                            <!-- html5 date input -->
                            <div class="form-group col-sm-12">
                                <label for="content">Content</label>
                                <input type="text" id="content" wire:model.defer="content" class="form-control">
                            </div>
                            <!-- enum -->
                            <div class="form-group col-sm-12">
                                <label for="template">Template</label>
                                <select id="template" class="form-control" wire:model.defer="template">
                                    <option selected value="">_</option>
                                    <option value="template 1">template 1</option>
                                    <option value="template 2">template 2</option>
                                    <option value="other">other</option>
                                </select>
                            </div>
                            <div class="form-group col-sm-12">
                                <label for="type">Type (Category)</label>
                                <select id="category" class="form-control" wire:model.defer="dtype">
                                    <option value="" selected>_</option>
                                    @foreach ($categories as $index => $item)
                                        <option value="{{ $item->name }}">{{ $item->name }}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group col-sm-12">
                            </div>
                            <div class="form-group col-sm-12">
                                <label for="status">Status</label>
                                <select id="status" class="form-control" wire:model.defer="status">
                                    <option disabled>_</option>
                                    <option value="PUBLISHED" selected>PUBLISHED</option>
                                    <option value="DRAFT">DRAFT</option>
                                </select>
                            </div>
                            <div class="form-group col-sm-12">
                                <label for="priority">Priority</label>
                                <input type="text" id="priority" wire:model.defer="priority" class="form-control">
                            </div>
                            <div class="form-group col-sm-12">
                                <label for="">Image</label>
                                <div wire:key="images" wire:ignore x-data x-init="
                                FilePond.registerPlugin(
                                    FilePondPluginFileEncode,
                                    FilePondPluginFileValidateSize,
                                    FilePondPluginImageExifOrientation,
                                    FilePondPluginImagePreview
                                    );
                                FilePond.setOptions({
                                    server: {
                                        process: (fieldName, file, metadata, load, error, progress, abort, transfer, options) => {
                                            @this.upload('images', file, load, error, progress)
                                        },
                                        revert: (filename, load) => {
                                            @this.removeUpload('images', filename, load)
                                        },
                                    },
                                });
                                FilePond.create($refs.input);
                            ">
                                    <input type="file" x-ref="input" multiple data-max-file-size="3MB">
                                </div>
                            </div>

                            {{-- <div x-data="{ isUploading: false, progress: 0 }"
                                x-on:livewire-upload-start="isUploading = true"
                                x-on:livewire-upload-finish="isUploading = false"
                                x-on:livewire-upload-error="isUploading = false"
                                x-on:livewire-upload-progress="progress = $event.detail.progress" class="w-100">
                                <div class="form-group col-sm-12">
                                    <label for="images">Images</label>
                                    <input type="file" class="form-control" id="images" wire:model.defer="images"
                                        multiple />
                                </div>

                                <!-- Progress Bar -->
                                <progress x-show="isUploading" max="100" x-bind:value="progress"
                                    class="form-group col-sm-12"></progress>
                                @if ($images)
                                    Photo Preview:

                                    <div class="row col-sm-12 form-group" style="margin: auto;">
                                        @foreach ($images as $image)
                                            <div class="col-12 col-md-3">
                                                <div class="card">
                                                    <img class="card-img-top" style="width:100%; height: 150px;"
                                                        src="{{ $image->temporaryUrl() }}" />
                                                </div>
                                            </div>
                                        @endforeach
                                    </div>
                                @endif
                            </div> --}}
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <hr>
    <div class="container-fluid">
        <h2>Images in this album</h2>
        {{-- Images in album --}}
        <div class="flex flex-wrap -mx-2" id="sortable">
            @foreach ($photos as $image)
                @if ($image->albums_id === $item_id)
                    <div class="w-1/4 p-2" data-id="{{$image->id}}">
                        <div class="w-full h-full border">
                            <div class="mycontainer">
                                <img class="image" src="{{ asset('storage/photos/' . $image->url) }}">
                                <div class="middle">
                                    <button type="button" class="btn btn-lg btn-info rounded-pill mr-1"
                                        wire:click="editAsset({{ $image->id }},'edit')">
                                        <i class="la la-edit"></i>
                                    </button>
                                    <button type="button" class="btn btn-lg btn-danger rounded-pill ml-1"
                                        wire:click="confirmDeleteAsset({{ $image->id }})">
                                        <i class="la la-trash"> </i>
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                @endif
            @endforeach
        </div>
    
    </div>
    <hr>
    <div class="container-fluid">
        @include('livewire.component.saveaction-form')
    </div>
    <script>
        $(function() {
            $( "#sortable" ).sortable(
            {
                placeholder: "ui-state-highlight",
                update: function(e, ui) {
                    var data = $(this).sortable('toArray',{attribute:'data-id'});
                    @this.confirmChangePriorityAsset(data);
                }
            }
            );
            $( "#sortable" ).disableSelection();
        });
    </script>
    <style>
        .ui-state-highlight 
        {
            background:yellow;
            width:25%; 
            height: 100%; 
            line-height: 1.2em;
            padding: .5rem;
        }
        .mycontainer {
            position: relative;
        }

        .image {
            opacity: 1;
            display: block;
            transition: .5s ease;
            backface-visibility: hidden;
            width: 100%;
            height: 100%;
        }

        .middle {
            width: 100%;
            display: flex;
            justify-content: center;
            transition: .5s ease;
            display: none;
            position: absolute;
            text-align: center;
            opacity: 0;
        }

        .mycontainer:hover .image {
            opacity: 0.3;
        }

        .mycontainer:hover .middle {
            display: block;
            transform: translate(-50%, -50%);
            top: 50%;
            left: 50%;
            opacity: 0.8;
        }

    </style>
</main>
