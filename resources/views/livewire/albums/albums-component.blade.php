@switch($isOpen)
    @case(1)
        @include('livewire.albums.albums-form',['categories' => App\Models\Category::all(),'tags'
        =>App\Models\Tag::all()])
    @break
    @case(2)
        @include('livewire.assets.assets-form')
    @break
    @default
        @php
        $breadcrumb = [
            'items' => [
                [
                    'href' => route('dashboard'),
                    'title' => 'dashboard',
                ],
                [
                    'href' => route('albums'),
                    'title' => 'albums',
                ],
            ],
            'active' => 'List',
        ];
        $header = [
            'title' => 'albums',
            'reset' => route('albums'),
        ];
        $button = 'Add albums';
        $table_header = ['name', 'type', 'status', 'Actions'];
        $table_body = ['name', 'type', 'status'];
        @endphp
        @include('livewire.component.main-component',[$breadcrumb, $header, $button, $table_header,
        $table_body,$table_actions=[],$selectBox=false])
        <script>
            $(function() {
                $("#sortable").sortable({
                    animation: 200,
                    ghostClass: 'ghost',
                    update: function(e, ui) {
                        var sortData = $(this).sortable('toArray', {
                            attribute: 'data-id'
                        })
                        @this.confirmChangePriority(sortData);
                    }
                });
                $( "tbody" ).disableSelection();
            });
        </script>
        <style>
            .dragged {
                position: absolute;
                opacity: 0.5;
                z-index: 2000;
                background: black;
            }

        </style>
@endswitch
