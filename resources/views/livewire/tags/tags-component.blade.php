@if ($isOpen)
    @include('livewire.tags.tags-form')
@else
    @php
        $breadcrumb = [
            'items' => [
                [
                    'href' => route('dashboard'),
                    'title' => 'dashboard',
                ],
                [
                    'href' => route('tags'),
                    'title' => 'Tags',
                ],
            ],
            'active' => 'List',
        ];
        $header = [
            'title' => 'Tags',
            'reset' => route('tags'),
        ];
        $button = 'Add tag';
        $table_header = ['Name', 'Slug', 'Actions'];
        $table_body = ['name', 'slug'];

    @endphp
    @include('livewire.component.main-component',[$breadcrumb, $header, $button, $table_header, $table_body,$table_actions=[],$selectBox=false])
@endif
