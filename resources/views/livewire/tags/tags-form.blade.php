@php
$title = 'tags';
$title2 = 'tag';
@endphp
<main class="main pt-2">
    @include('livewire.component.header-form',[$title, $type])
    <div class="container-fluid animated fadeIn">
        <div class="row">
            <div class="col-md-8 bold-labels">
                <!-- Default box -->
                <form>
                    <div class="card">
                        <div class="card-body row">
                            <!-- text input -->
                            <div class="form-group col-sm-12 required">
                                <label for="name">Name</label>
                                <input type="text" id="name" wire:model.defer="name" class="form-control">
                                @error('name')
                                    <span class="error text-danger">{{ $message }}</span>
                                @enderror
                            </div>
                            <!-- text input -->
                            <div class="form-group col-sm-12">
                                <label for="slug">Slug</label>
                                <input type="text" id="slug" wire:model.defer="slug" class="form-control">
                            </div>
                        </div>
                    </div>
                    @include('livewire.component.saveaction-form')
                </form>
            </div>
        </div>
    </div>
</main>
