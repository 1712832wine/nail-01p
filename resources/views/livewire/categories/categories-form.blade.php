@php
$title = 'categories';
$title2 = 'categorie';
@endphp
<main class="main pt-2">
    @include('livewire.component.header-form',[$title, $type])
    <div class="container-fluid animated fadeIn">
        <div class="row">
            <div class="col-md-8 bold-labels">
                <!-- Default box -->
                <form>
                    <div class="card">
                        <div class="card-body row">
                            <!-- text input -->
                            <div class="form-group col-sm-12 required">
                                <label for="name">Name</label>
                                <input type="text" id="name" wire:model.defer="name" class="form-control">
                                @error('name')
                                    <span class="error text-danger">{{ $message }}</span>
                                @enderror
                            </div>
                            <!-- text input -->
                            <div class="form-group col-sm-12">
                                <label for="slug">Slug (URL)</label>
                                <input type="text" id="slug" class="form-control" wire:model.defer="slug">
                                <p class="help-block">Will be automatically generated from your name, if left empty.</p>
                            </div>
                            <!-- select -->
                            <div class="form-group col-sm-12">
                                <label for="parent_id">Parent</label>
                                <select id="parent_id" class="form-control" wire:model.defer="parent">
                                    <option value="">-</option>
                                    <option value="1">Non</option>
                                    <option value="2">Mollitia</option>
                                    <option value="3">Molestiae</option>
                                    <option value="4">Quod</option>
                                    <option value="5">Similique</option>
                                    <option value="6">Rerum</option>
                                    <option value="7">Ea</option>
                                    <option value="8">Sed</option>
                                    <option value="9">Aut</option>
                                    <option value="10">Delectus</option>
                                    <option value="11">Est</option>
                                    <option value="12">Eum</option>
                                    <option value="13">Iste</option>
                                    <option value="14">Reiciendis</option>
                                    <option value="15">Quos</option>
                                </select>
                            </div>
                        </div>
                    </div>
                    @include('livewire.component.saveaction-form')
                </form>
            </div>
        </div>
    </div>
</main>
