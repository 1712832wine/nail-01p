<main class="main pt-2">


    <nav aria-label="breadcrumb" class="d-none d-lg-block">
        <ol class="breadcrumb bg-transparent p-0 justify-content-end">
            <li class="breadcrumb-item text-capitalize"><a href="{{ route('dashboard') }}">dashboard</a></li>
            <li class="breadcrumb-item text-capitalize"><a href="{{ route('categories') }}">categories</a></li>
            <li class="breadcrumb-item text-capitalize active" aria-current="page">Preview</li>
        </ol>
    </nav>


    <section class="container-fluid d-print-none">
        <a href="javascript: window.print();" class="btn float-right">
            <i class="la la-print"></i>
        </a>
        <h2>
            <span class="text-capitalize">categories</span>
            <small>Preview category.</small>
            <small class="">
                <a href="" wire:click.prevent="closePreview()" class="font-sm">
                    <i class="la la-angle-double-left"></i>
                    Back to all <span>categories</span>
                </a>
            </small>
        </h2>
    </section>

    <div class="container-fluid animated fadeIn">


        <div class="row">
            <div class="col-md-8">

                <!-- Default box -->
                <div class="">
                    <div class="card no-padding no-border">
                        <table class="table table-striped mb-0">
                            <tbody>
                                <tr>
                                    <td>
                                        <strong>Name:</strong>
                                    </td>
                                    <td>
                                        <span>
                                            {{ $category->name }}
                                        </span>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <strong>Slug:</strong>
                                    </td>
                                    <td>
                                        <span>
                                            {{ $category->slug }}
                                        </span>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <strong>Parent:</strong>
                                    </td>
                                    <td>
                                        <span>
                                            {{ $category->parent }}
                                        </span>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <strong>Articles:</strong>
                                    </td>
                                    <td>
                                        <span>
                                            <a href="#">
                                                {{ $category->article }}
                                            </a>
                                        </span>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <strong>Created:</strong>
                                    </td>
                                    <td>
                                        <span>
                                            {{ $category->created_at }}
                                        </span>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <strong>Updated:</strong>
                                    </td>
                                    <td>
                                        <span>
                                            {{ $category->updated_at }}
                                        </span>
                                    </td>
                                </tr>
                                <tr>
                                    <td><strong>Actions</strong></td>
                                    <td>
                                        <!-- Single edit button -->
                                        <a href="" wire:click.prevent="edit({{ $category->id }},'edit')"
                                            class="btn btn-sm btn-link">
                                            <i class="la la-edit">
                                            </i> Edit
                                        </a>

                                        <a wire:click.prevent="confirmDelete({{ $category->id }})"
                                            class="btn btn-sm btn-link" data-button-type="delete">
                                            <i class="la la-trash"></i> Delete
                                        </a>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>

            </div>
        </div>


    </div>

</main>
