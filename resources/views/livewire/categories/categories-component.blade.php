@if ($isOpen)
    @include('livewire.categories.categories-form')
@else
    @php
        $breadcrumb = [
            'items' => [
                [
                    'href' => route('dashboard'),
                    'title' => 'dashboard',
                ],
                [
                    'href' => route('categories'),
                    'title' => 'Categories',
                ],
            ],
            'active' => 'List',
        ];
        $header = [
            'title' => 'Categories',
            'reset' => route('categories'),
        ];
        $button = 'Add Category';
        $table_header = ['Name', 'Slug', 'Parent', 'Articles', 'Actions'];
        $table_body = ['name', 'slug', 'parent', 'article'];

    @endphp
    @include('livewire.component.main-component',[$breadcrumb, $header, $button, $table_header, $table_body,$table_actions=[],$selectBox=false])
@endif
