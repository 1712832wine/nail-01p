@php
$title = 'Assets';
$title2 = 'asset';
$type = 'edit';
@endphp
<main class="main pt-2">
    {{-- Breadcrumb --}}
    <nav aria-label="breadcrumb" class="d-none d-lg-block">
        <ol class="breadcrumb bg-transparent p-0 justify-content-end">
            <li class="breadcrumb-item text-capitalize">
                <a href="{{ route('dashboard') }}">Dashboard</a>
            </li>
            <li class="breadcrumb-item text-capitalize">
                <a href="" wire:click.prevent="closeModalAsset()">{{ $title }}</a>
            </li>
            <li class="breadcrumb-item text-capitalize active" aria-current="page">{{ $type }}</li>
        </ol>
    </nav>
    {{-- Return text --}}
    <section class="container-fluid">
        <h2>
            <span class="text-capitalize">{{ $title }}</span>
            <small class="text-capitalize">{{ $type }} {{ $title2 }}.</small>

            <small>
                <a href="" wire:click.prevent="closeModalAsset()" class="d-print-none font-sm">
                    <i class="la la-angle-double-left"></i> Back to all <span>{{ $title }}</span>
                </a>
            </small>
        </h2>
    </section>
    {{-- END HEADER --}}
    <div class="container-fluid animated fadeIn">
        <div class="row">
            <div class="col-md-8 bold-labels">
                <!-- Default box -->
                <form>
                    <div class="card">
                        <div class="card-body row">
                            <div class="form-group col-sm-12">
                                <label>This Images</label>
                                <div class="w-1/2 p-2">
                                    <div class="w-full h-full border">
                                        <img class="" src="{{ asset('storage/photos/' . $a_url) }}">
                                    </div>
                                </div>
                                <!-- text input -->
                                <div class="form-group col-sm-12">
                                    <label for="name">Name</label>
                                    <input type="text" id="title" wire:model.defer="a_name" class="form-control">
                                </div>
                                <div class="form-group col-sm-12">
                                    <label for="short_desc">Short_desc</label>
                                    <input type="text" id="short_desc" wire:model.defer="a_short_desc"
                                        class="form-control">
                                </div>
                                <div class="form-group col-sm-12">
                                    <label for="content">Content</label>
                                    <input type="text" id="content" wire:model.defer="a_content" class="form-control">
                                </div>
                                <div class="form-group col-sm-12">
                                    <label for="type">Type</label>
                                    <input type="text" id="type" wire:model.defer="a_type" class="form-control">
                                </div>
                                <div class="form-group col-sm-12">
                                    <label for="url">URL</label>
                                    <input disabled type="text" id="url" wire:model.defer="a_url" class="form-control">
                                </div>
                                <div class="form-group col-sm-12">
                                    <label for="priority">Priority</label>
                                    <input type="text" id="priority" wire:model.defer="a_priority" class="form-control">
                                </div>
                                <div class="form-group col-sm-12">
                                    <label for="status">Status</label>
                                    <input type="text" id="status" wire:model.defer="a_status" class="form-control">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div id="saveActions" class="form-group">
                        <input type="hidden" name="save_action" value="save_and_back">
                        <div class="btn-group" role="group">

                            <button type="submit" id="submit" class="btn btn-success"
                                wire:click.prevent="saveAndBackAsset()">
                                <span class="la la-save" role="presentation" aria-hidden="true"></span>
                                &nbsp; <span data-value="save_and_back">Save and back</span>
                            </button>
                        </div>
                        <a href="" wire:click.prevent="closeModalAsset()" class="btn btn-default">
                            <span class="la la-ban"></span>
                            &nbsp;Cancel
                        </a>
                    </div>

                </form>
            </div>
        </div>
    </div>
</main>
