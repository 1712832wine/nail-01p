<div role="tabpanel" class="tab-pane " id="tab_basic-info">
    <div class="row">
        <!-- load the view from type and view_namespace attribute if set -->
        <!-- select2 -->
        <div class="form-group col-sm-12">
            <label for="category">Category</label>
            <select id="category" class="form-control" wire:model.defer="category_id">
                <option value="{{ null }}" selected>_</option>
                @foreach ($categories as $index => $item)
                    <option value="{{ $item->id }}">{{ $item->name }}</option>
                @endforeach
            </select>
        </div>
        <!-- load the view from type and view_namespace attribute if set -->
        <!-- number input -->
        <div class="form-group col-sm-12"> <label for="price">Price</label>
            <div class="input-group">
                <input id="price" type="number" wire:model.defer="price" class="form-control">
                <div class="input-group-append"><span class="input-group-text">.00</span></div>
            </div>
        </div>
    </div>
</div>
