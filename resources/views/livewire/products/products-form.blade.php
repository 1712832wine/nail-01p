@php
$title = 'Products';
$title2 = 'product';
@endphp
<main class="main pt-2">
    @include('livewire.component.header-form',[$title, $type])
    <div class="container-fluid animated fadeIn">
        <div class="row">
            <div class="col bold-labels">
                <!-- Default box -->
                <form wire:submit.prevent="">
                    <div class="tab-container  mb-2">
                        <div class="nav-tabs-custom " id="form_tabs">
                            <ul class="nav nav-tabs " role="tablist">
                                <li role="presentation" class="nav-item">
                                    <a href="#tab_texts" aria-controls="tab_texts" role="tab" tab_name="texts"
                                        data-toggle="tab" class="nav-link active">Texts</a>
                                </li>
                                <li role="presentation" class="nav-item">
                                    <a href="#tab_basic-info" aria-controls="tab_basic-info" role="tab"
                                        tab_name="basic-info" data-toggle="tab" class="nav-link ">Basic Info</a>
                                </li>
                                {{-- <li role="presentation" class="nav-item">
                                    <a href="#tab_metas" aria-controls="tab_metas" role="tab" tab_name="metas"
                                        data-toggle="tab" class="nav-link ">Metas</a>
                                </li> --}}
                            </ul>
                            <div class="tab-content p-0 ">
                                @include('livewire.products.products-form_text')
                                @include('livewire.products.products-form_info',['categories'
                                =>App\Models\Category::all()])
                                {{-- @include('livewire.products.products-form_meta') --}}
                            </div>
                        </div>
                    </div>
                    {{-- SAVE AND BACK --}}
                    <div id="saveActions" class="form-group">
                        <input type="hidden" name="save_action" value="save_and_back">
                        <div class="btn-group" role="group">
                            <button id="submit" class="btn btn-success">
                                <span class="la la-save" role="presentation" aria-hidden="true"></span>
                                &nbsp; <span data-value="save_and_back">Save and back</span>
                            </button>
                        </div>
                        <a href="" wire:click.prevent="closeModal()" class="btn btn-default">
                            <span class="la la-ban"></span>
                            &nbsp;Cancel
                        </a>
                    </div>

                </form>
            </div>
        </div>
    </div>
</main>
