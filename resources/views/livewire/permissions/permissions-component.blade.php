@if ($isOpen)
    @include('livewire.permissions.permissions-form')
@else
    @php
        $breadcrumb = [
            'items' => [
                [
                    'href' => route('dashboard'),
                    'title' => 'dashboard',
                ],
                [
                    'href' => route('permissions'),
                    'title' => 'permissions',
                ],
            ],
            'active' => 'List',
        ];
        $header = [
            'title' => 'Permissions',
            'reset' => route('permissions'),
        ];
        $button = 'Add permission';
        $table_header = ['Name', 'Actions'];
        $table_body = ['name'];

    @endphp
    @include('livewire.component.main-component',[$breadcrumb, $header, $button, $table_header, $table_body,$table_actions=[],$selectBox=false])
@endif
