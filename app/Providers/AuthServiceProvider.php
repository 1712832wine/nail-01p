<?php

namespace App\Providers;

use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;
use Illuminate\Support\Facades\Gate;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        // 'App\Models\Model' => 'App\Policies\ModelPolicy',
        'App\Models\Menu' => 'App\Policies\MenuPolicy',
        'App\Models\Page' => 'App\Policies\PagePolicy',
        'App\Models\Article' => 'App\Policies\ArticlePolicy',
        'App\Models\Category' => 'App\Policies\CategoryPolicy',
        'App\Models\Tag' => 'App\Policies\TagPolicy',
        'App\Models\User' => 'App\Policies\UserPolicy',
        'App\Models\Role' => 'App\Policies\RolePolicy',
        'App\Models\Permission' => 'App\Policies\PermissionPolicy',
        'App\Models\Album' => 'App\Policies\AlbumPolicy',
        'App\Models\Contact' => 'App\Policies\ContactPolicy',
        'App\Models\Product' => 'App\Policies\ProductPolicy',
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();

        //
    }
}
