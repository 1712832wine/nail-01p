<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Page extends Model
{
    use HasFactory;
    protected $fillable = [
        'id','template', 'page_name', 'page_title', 'page_slug', 'meta_title', 'meta_description', 'meta_keyword', 'content'
    ];
}
