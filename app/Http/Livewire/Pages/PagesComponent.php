<?php

namespace App\Http\Livewire\Pages;

use Livewire\Component;

use App\Models\Page;
use App\Http\Livewire\Component\Alert;
use Livewire\WithPagination;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;

class PagesComponent extends Component
{
    // pagination
    use AuthorizesRequests;
    use WithPagination;
    protected $paginationTheme = 'bootstrap';
    // search
    public $search = '',$currentName='';
    // form
    public $template, $page_name, $page_title, $page_slug, $meta_title, $meta_description, $meta_keyword,$content;
    public $item_id;
    public $type;
    // pagination
    public $pagination_size = 6;
    // , $type, $link;
    public $isOpen = false;
    protected $listeners = ['pages:delete' => 'delete','Page:deleteSelected'=>'deleteSelected'];
    public $selectAll =false, $selected=[];
    public function updatedSelectAll($value){
        if($value){
            $this->selected = Page::where('page_name', 'like', '%'.$this->search.'%')->pluck('id');
        }else{
            $this->selected = [];
        }
    }
    // pagination
    public function updatingPaginationSize()
    {
        $this->resetPage();
    }
    public function updatingSearch()
    {
        $this->resetPage();
    }

    public function render()
    {
        return view('livewire.pages.pages-component',[ 'list' => Page::where('page_name', 'like', '%'.$this->search.'%')->paginate($this->pagination_size)]);
    }
    //  CRUD
    public function create($type)
    {
        $this->authorize('create', Page::class);
        $this->type = $type;
        $this->resetInputFields();
        $this->openModal();
    }

    public function openModal()
    {
        $this->isOpen = true;
    }

    public function closeModal()
    {
        $this->isOpen = false;
    }

    private function resetInputFields(){
        $this->item_id=null;
        $this->template = 'services';
        $this->page_name = '';
        $this->page_title = '';
        $this->page_slug = '';
        $this->meta_title = '';
        $this->meta_description= '';
        $this->meta_keyword= '';
        $this->content= '';
    }

    public function saveAndBack()
    {
        if($this->currentName)
        if($this->page_name==$this->currentName)
        Page::where('page_name',$this->page_name)->delete();

        $this->validate([
            'template' => 'required',
            'page_name' => ['required', 'unique:pages'],
            'page_title' => 'required',
        ]);

        Page::updateOrCreate(['id' => $this->item_id], [
        'template'=> $this->template,
        'page_name'=> $this->page_name,
        'page_title'=> $this->page_title,
        'page_slug'=> $this->page_slug,
        'meta_title'=> $this->meta_title,
        'meta_description'=> $this->meta_description,
        'meta_keyword'=> $this->meta_keyword,
        'content'=> $this->content,
        ]);

        $this->closeModal();
        $this->resetInputFields();
    }

    public function edit($id, $type)
    {
        $this->authorize('update',Page::find($id));
        $this->type = $type;
        $page = Page::findOrFail($id);
        $this->item_id = $id;
        $this->template = $page->template;
        $this->page_name = $page->page_name;
        $this->currentName = $page->page_name;
        $this->page_title = $page->page_title;
        $this->page_slug = $page->page_slug;
        $this->meta_title = $page->meta_title;
        $this->meta_description= $page->meta_description;
        $this->meta_keyword= $page->meta_keyword;
        $this->content= $page->content;
        $this->openModal();
    }

    public function delete($id)
    {
        Page::find($id)->delete();
        $this->emit('swal:modal', [
            'type'    => 'success',
            'icon'    => 'success',
            'title'   => 'Delete Success!',
            'text'    => "The item has been deleted successfully",
        ]);

    }
    public function confirmDelete($id) {
        $this->authorize('delete',Page::find($id));
        $this->emit("swal:confirm", [
            'icon'        => 'warning',
            'type'        => 'warning',
            'title'       => 'Warning',
            'text'        => "Are you sure you want to delete this item?",
            'confirmText' => 'Delete',
            'method'      => 'pages:delete',
            'params'      => $id, // optional, send params to success confirmation
        ]);
    }

    //clone
    public function clone($id){

        $this->authorize('update',Page::find($id));
        $Page = Page::findOrFail($id);
        //
        $this->template = $Page->template;
        $this->page_name = $Page->page_name;
        $this->page_title = $Page->page_title;
        $this->page_slug = $Page->page_slug;
        $this->meta_title = $Page->meta_title;
        $this->meta_description = $Page->meta_description;
        $this->meta_keyword = $Page->meta_keyword;
        $this->content = $Page->content;
        //
        Page::create([
            'template' => $this->template,
            'page_name' => $this->page_name.'(copy)',
            'page_title'  => $this->page_title,
            'page_slug'  => $this->page_slug,
            'meta_title'  => $this->meta_title,
            'meta_description'  => $this->meta_description,
            'meta_keyword'  => $this->meta_keyword,
            'content'  => $this->content,
        ]);
        //
        $this->emit('swal:modal', [
            'type'    => 'success',
            'icon'    => 'success',
            'title'   => 'Clone Success!',
            'text'    => "The item has been cloned successfully",
        ]);
    }
     // DELETE SELECTED
     public function deleteSelected(){
        Page::whereIn('id',$this->selected)->delete();
        $this->selected=[];
        $this->selectAll=false;
        $this->emit('swal:modal', [
            'type'    => 'success',
            'icon'    => 'success',
            'title'   => 'Delete success!',
            'text'    => "These items has been deleted successfully",
        ]);
    }
    public function confirmDeleteSelected(){
        if (count($this->selected) > 0)
        {
            $this->authorize('delete',Page::find($this->selected[0]));
            $this->emit("swal:confirm", [
                'icon'        => 'warning',
                'type'        => 'warning',
                'title'       => 'Warning',
                'text'        => 'Are you sure you want to delete these items?',
                'confirmText' => 'Delete',
                'method'      => 'Page:deleteSelected',

            ]);
        }
        else
        {
            $this->emit('swal:modal', [
                'type'    => 'warning',
                'icon'    => 'warning',
                'title'   => 'Nothing to delete!',
                'text'    => "You have not choose any item to delete",
            ]);
        }
    }

}
