<?php

namespace App\Http\Livewire\Permissions;

use Livewire\Component;

use App\Models\Permission;
use App\Http\Livewire\Component\Alert;
use Livewire\WithPagination;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;

class PermissionsComponent extends Component
{
    use WithPagination;
    use AuthorizesRequests;
    protected $paginationTheme = 'bootstrap';
    // search
    public $search = '',$currentName = '';
    // form
    public $name, $item_id;
    public $pagination_size = 6;
    public $type;
    // , $type, $link;
    public $isOpen = false;
    protected $listeners = ['Permission:delete' => 'delete','Permission:deleteSelected'=>'deleteSelected'];
    // SELECTED
    public $selectAll =false, $selected=[];
    public function updatedSelectAll($value){
        if($value){
            $this->selected = Permission::where('name', 'like', '%'.$this->search.'%')->pluck('id');
        }else{
            $this->selected = [];
        }
    }
    // pagination
    public function updatingPaginationSize()
    {
        $this->resetPage();
    }
    public function updatingSearch()
    {
        $this->resetPage();
    }

    public function render()
    {
        return view('livewire.permissions.permissions-component',[ 'list' => Permission::where('name', 'like', '%'.$this->search.'%')->paginate($this->pagination_size)]);
    }
    //  CRUD
    public function create($type)
    {
        $this->authorize('create', Permission::class);
        $this->type = $type;
        $this->resetInputFields();
        $this->openModal();
    }

    public function openModal()
    {
        $this->isOpen = true;
    }

    public function closeModal()
    {
        $this->isOpen = false;
    }

    private function resetInputFields(){
        $this->item_id='';
        $this->name = '';
    }

    public function saveAndBack()
    {
        if($this->currentName)
        if($this->name==$this->currentName)
        Permission::where('name',$this->name)->delete();

        $this->validate([
            'name' => ['required','unique:permissions'],
        ]);

        Permission::updateOrCreate(['id' => $this->item_id], [
            'name' => $this->name,
        ]);

        $this->closeModal();
        $this->resetInputFields();
    }

    public function edit($id, $type)
    {
        $this->authorize('update',Permission::find($id));
        $this->type = $type;
        $Permission = Permission::findOrFail($id);
        $this->item_id = $id;
        $this->name = $Permission->name;
        $this->currentName = $Permission->name;
        $this->openModal();
    }

    public function delete($id)
    {
        Permission::find($id)->delete();
        $this->emit('swal:modal', [
            'type'    => 'success',
            'icon'    => 'success',
            'title'   => 'Delete Success!',
            'text'    => "The item has been deleted successfully",
        ]);

    }
    public function confirmDelete($id) {
        $this->authorize('delete',Permission::find($id));
        $this->emit("swal:confirm", [
            'icon'        => 'warning',
            'type'        => 'warning',
            'title'       => 'Warning',
            'text'        => "Are you sure you want to delete this item?",
            'confirmText' => 'Delete',
            'method'      => 'Permission:delete',
            'params'      => $id, // optional, send params to success confirmation
        ]);
    }
    // DELETE SELECTED
    public function deleteSelected(){
        Permission::whereIn('id',$this->selected)->delete();
        $this->selected=[];
        $this->selectAll=false;
        $this->emit('swal:modal', [
            'type'    => 'success',
            'icon'    => 'success',
            'title'   => 'Delete success!',
            'text'    => "These items has been deleted successfully",
        ]);
    }
    public function confirmDeleteSelected(){
        if (count($this->selected) > 0)
        {
            $this->authorize('delete',Permission::find($this->selected[0]));
            $this->emit("swal:confirm", [
                'icon'        => 'warning',
                'type'        => 'warning',
                'title'       => 'Warning',
                'text'        => 'Are you sure you want to delete these items?',
                'confirmText' => 'Delete',
                'method'      => 'Permission:deleteSelected',
            
            ]);
        }
        else
        {
            $this->emit('swal:modal', [
                'type'    => 'warning',
                'icon'    => 'warning',
                'title'   => 'Nothing to delete!',
                'text'    => "You have not choose any item to delete",
            ]);
        }
    }
}
