<?php

namespace App\Http\Livewire\Products;

use Livewire\Component;
use App\Models\Product;
use App\Http\Livewire\Component\Alert;
use Livewire\WithPagination;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Livewire\WithFileUploads;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\File;
class ProductsComponent extends Component
{
    // pagination
    use WithFileUploads;
    use WithPagination;
    use AuthorizesRequests;
    protected $paginationTheme = 'bootstrap';
    // search
    public $search = '';
    // form
    public $name,$images, $description, $details, $features,$price, $category_id, $dtype, $extras;
    public $item_id;
    public $imageList = [];
    public $pagination_size = 6;
    public $type;
    // , $type, $link;
    public $isOpen = false;
    protected $listeners = ['Product:delete' => 'delete',
                            'Product:deleteImage'=>'deleteImage',
                            'Product:deleteSelected' => 'deleteSelected'];

    // pagination
    public $count = 0, $count_extra = 0;
    // selected
    public $selected=[],$selectAll=false;
    // --------------------------------------------
    public function addFeature(){
        array_push($this->features ,['name'=>'','desc'=>'']);
    }
    public function removeFeature($index){
        array_splice($this->features, $index, 1);
    }
    public function addExtra(){
        array_push($this->extras ,['name'=>'','desc'=>'']);
    }
    public function removeExtra($index){
        array_splice($this->extras, $index, 1);
    }
    // --------------------------------------------

    public function updatingPaginationSize()
    {
        $this->resetPage();
    }
    public function updatingSearch()
    {
        $this->resetPage();
    }
    public function updatedSelectAll($value){
        if($value){
            $this->selected = Product::where('name', 'like', '%'.$this->search.'%')->pluck('id');
        }else{
            $this->selected = [];
        }
    }
    public function render()
    {
        return view('livewire.products.products-component',[ 'list' => Product::where('name', 'like', '%'.$this->search.'%')->paginate($this->pagination_size)]);
    }
    //  CRUD
    public function create($type)
    {
        $this->authorize('create', Product::class);
        $this->type = $type;
        $this->resetInputFields();
        $this->openModal();
    }

    public function openModal()
    {
        $this->isOpen = true;
    }

    public function closeModal()
    {
        $this->isOpen = false;
    }

    private function resetInputFields(){
        $this->item_id=null;
        $this->name = '';
        $this->description = '';
        $this->details = '';
        $this->features = [];
        $this->price = null;
        $this->category_id = null;
        $this->dtype = '';
        $this->images = [];
        $this->extras = [];

    }

    public function saveAndBack()
    {
        $this->validate([
            'name' => ['required'],
        ]);
        $images = $this->imageList;
        foreach ($this->images as $image) {
            $name = md5($image . microtime()).'.'.$image->extension();
            $image->storeAs('photos', $name);
            array_push($images, $name);
        }
        
        Product::updateOrCreate(['id' => $this->item_id], [
            'name' => $this->name,
            'description' => $this->description,
            'details'  => $this->details,
            'images'    => json_encode($images),
            'features' => json_encode($this->features),
            'price' => $this->price,
            'category_id' => $this->category_id,
            'type'=>$this->dtype,
            'extras' => json_encode($this->extras),
        ]);

        $this->closeModal();
        $this->resetInputFields();
    }

    public function edit($id, $type)
    {
        $this->authorize('update',Product::find($id));
        $this->type = $type;
        $Product = Product::findOrFail($id);
        $this->item_id = $id;
        $this->name = $Product->name;
        $this->description = $Product->description;
        $this->details = $Product->details;
        $this->imageList = json_decode($Product->images);
        $this->features = json_decode($Product->features);
        $this->price = $Product->price;
        $this->images=[];
        $this->category_id = $Product->category_id;
        $this->dtype = $Product->type;
        $this->extras = json_decode($Product->extras);
        $this->openModal();
    }
    public function clone($id){
        $this->authorize('update',Product::find($id));
        $Product = Product::findOrFail($id);
        //
        $this->name = $Product->name;
        $this->description = $Product->description;
        $this->details = $Product->details;
        $this->features = json_decode($Product->features);
        $this->images = json_decode($Product->images);
        $this->price = $Product->price;
        $this->category_id = $Product->category_id;
        $this->dtype = $Product->type;
        $this->extras = json_decode($Product->extras);
        //
        $images = [];
        foreach ($this->images as $image) {
            $full = explode(".", $image);
            $fileName = $full[0];
            $fileExt  = $full[1];
            $destinationFolder = public_path('storage/photos/');
            $num   = 1;
            $newName = $fileName. '(' . $num . ')';
            while (file_exists($destinationFolder . implode(".",array($newName,$fileExt)))) {
                $num ++;
                $newName = $fileName. '(' . $num . ')';
            }
            File::copy('storage/photos/'.$image, 'storage/photos/'.$newName . '.' . $fileExt);
            array_push($images,$newName . '.' . $fileExt);
        }
        //
        Product::create([
            'name' => $this->name,
            'description' => $this->description,
            'details'  => $this->details,
            'features' => json_encode($this->features),
            'images'=>json_encode($images),
            'price' => $this->price,
            'category_id' => $this->category_id,
            'type'=>$this->dtype,
            'extras' => json_encode($this->extras),
        ]);
        //
        $this->resetInputFields();
        $this->emit('swal:modal', [
            'type'    => 'success',
            'icon'    => 'success',
            'title'   => 'Clone Success!',
            'text'    => "The item has been cloned successfully",
        ]);
    }
        
    // DELETE SELECTED
    public function deleteSelected(){
        $products = Product::whereIn('id',$this->selected)->get();
        foreach ($products as $product){
            $images = json_decode($product['images']);
            foreach($images as $img) {
                Storage::delete('photos/'.$img);
            }
        }
        Product::query()->whereIn('id',$this->selected)->delete();
        $this->selected=[];
        $this->selectAll=false;
        $this->emit('swal:modal', [
            'type'    => 'success',
            'icon'    => 'success',
            'title'   => 'Delete success!',
            'text'    => "These items has been deleted successfully",
        ]);
    }
    public function confirmDeleteSelected(){
        if (count($this->selected)>0)
        {
            $this->authorize('delete',Product::find($this->selected[0]));
            $this->emit("swal:confirm", [
                'icon'        => 'warning',
                'type'        => 'warning',
                'title'       => 'Warning',
                'text'        => "Are you sure you want to delete these items?",
                'confirmText' => 'Delete',
                'method'      => 'Product:deleteSelected',

            ]);
        }
        else
        {
            $this->emit('swal:modal', [
                'type'    => 'warning',
                'icon'    => 'warning',
                'title'   => 'Nothing to delete!',
                'text'    => "You have not choose any item to delete",
            ]);
        }
    }
    //
    public function delete($id)
    {
        $product = Product::findOrFail($id);
        $images = json_decode($product['images']);
        foreach($images as $img) {
            Storage::delete('photos/'.$img);
        }
        Product::find($id)->delete();
        $this->emit('swal:modal', [
            'type'    => 'success',
            'icon'    => 'success',
            'title'   => 'Delete Success!',
            'text'    => "The item has been deleted successfully",
        ]);

    }
    public function confirmDelete($id) {

        $this->authorize('delete',Product::find($id));
        $this->emit("swal:confirm", [
            'icon'        => 'warning',
            'type'        => 'warning',
            'title'       => 'Warning',
            'text'        => "Are you sure you want to delete this item?",
            'confirmText' => 'Delete',
            'method'      => 'Product:delete',
            'params'      => $id, // optional, send params to success confirmation
        ]);
    }
    public function deleteImage($id){
        $images = json_decode(Product::find($this->item_id)['images']);
        Storage::delete('photos/'.$images[$id]);
        array_splice($images, $id, 1);
        Product::updateOrCreate(['id' => $this->item_id], [
            'images' =>json_encode($images),
        ]);
        $this->imageList = $images;
        $this->emit('swal:modal', [
            'type'    => 'success',
            'icon'    => 'success',
            'title'   => 'Delete Success!',
            'text'    => "The item has been deleted successfully",
        ]);
    }
    public function confirmDeleteAsset($id){
        $this->emit("swal:confirm", [
            'icon'        => 'warning',
            'type'        => 'warning',
            'title'       => 'Warning',
            'text'        => "Are you sure you want to delete this item?",
            'confirmText' => 'Delete',
            'method'      => 'Product:deleteImage',
            'params'      => $id, // optional, send params to success confirmation
            ]);
        }

}
