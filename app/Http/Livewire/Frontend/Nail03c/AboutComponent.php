<?php

namespace App\Http\Livewire\Frontend\Nail03c;

use Livewire\Component;
use App\Models\Article;

class AboutComponent extends Component
{
    public function render()
    {
        return view('frontend.nail03-c.about',[
            'articles'=>Article::where([['category','About Us'],['status','PUBLISHED']])->get()
        ])->layout('frontend.nail03-c.layout.layout');
    }
}
