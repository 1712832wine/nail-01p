<?php

namespace App\Http\Livewire\Frontend\Nail03c;

use Livewire\Component;
use App\Models\Album;
use App\Models\Asset;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;
use Livewire\WithPagination;
class GalleryDetailComponent extends Component
{
    use WithPagination;
    var $album_id='';
    public function mount($id) {
        $this->album_id = $id;
    }
    public function render()
    {
        $album = Album::find($this->album_id);
        $list = Asset::where('albums_id',$this->album_id)->orderBy('priority', 'desc')->paginate(9);
        return view('frontend.nail03-c.gallery-detail',['album' => $album,'list'=> $list])->layout('frontend.nail03-c.layout.layout');

    }
}
