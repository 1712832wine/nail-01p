<?php

namespace App\Http\Livewire\Frontend\Nail03d;

use Livewire\Component;
use App\Models\Album;
use App\Models\Asset;
use Symfony\Component\HttpFoundation\Response;
class GalleryComponent extends Component
{
    public function render()
    {
        $albums = Album::where([['type', 'Gallery'],['status','PUBLISHED']])->orderBy('priority')->get();
        $lists = [];
        foreach($albums as $item) {
            $assets_of_item =  Asset::where('albums_id',$item['id'])->orderBy('priority', 'desc')->get();
            array_push($lists, $assets_of_item);
        }
        return view('frontend.nail03-d.gallery',['albums' => $albums,'lists'=> $lists])->layout('frontend.nail03-d.layout.layout');
    }

    public function temp() {
        // return (new response("{'cat_id': 0,
        //     'page': 0,
        //     'blockId': 0,
        //     'cat_status': 0}",200))->header('Content-Type', 200);
        // json(['cat_id' => 0, 'page' => 0,'blockId' =>0,'cat_status' =>0
        return response()->json(['name' => 'Abigail', 'state' => 'CA']);
    }
}
