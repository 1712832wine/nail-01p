<?php

namespace App\Http\Livewire\Frontend\Nail02m;

use Livewire\Component;
use App\Models\Article;

class AboutComponent extends Component
{
    public function render()
    {
        return view('frontend.nail02-m.about',[
            'articles'=>Article::where([['category','About Us'],['status','PUBLISHED']])->get()
        ])->layout('frontend.nail02-m.layout.layout');
    }
}
