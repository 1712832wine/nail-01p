<?php

namespace App\Http\Livewire\Frontend\Nail02m;

use Livewire\Component;
use App\Models\Album;
use App\Models\Asset;
use App\Models\Article;
use App\Models\Product;
use App\Models\Category;


class HomeComponent extends Component
{
    public $item_id = 0;
    public function render()
    {
        $carousel_customers = Article::where([['category','Home Carousel'],['status','PUBLISHED']])->get();
        
        $services= Product::where(
            [
                ['category_id',Category::where('name','Services')->first()['id']],
            ])->get();    


        $gallery = Album::where([['type', 'Gallery'],['status','PUBLISHED']])->first();
        $gallery_list =  Asset::where('albums_id',$gallery['id'])->orderBy('priority', 'desc')->get();
        
        $home_carousel = Album::where([['type','Home Carousel'],['status','PUBLISHED']])->first();
        $imgs_carousel =  Asset::where('albums_id',$home_carousel['id'])->orderBy('priority', 'desc')->get();

        $header= Product::where('category_id',Category::where('name','Header And Footer')->first()['id'])->first();

        $home_articles = Article::where([['category','Home Article'],['status','PUBLISHED']])->get();

        


        return view('frontend.nail02-m.index',[
            'imgs_carousel'=> $imgs_carousel,
            'services'=> $services,
            'home_articles' => $home_articles,
            'gallery_list' => $gallery_list,
            'home_carousel'=> $home_carousel,
            'carousel_customers'=> $carousel_customers,
            'header'=> $header,
            ])->layout('frontend.nail02-m.layout.layout');
    }
}
