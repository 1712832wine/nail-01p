<?php

namespace App\Http\Livewire\Frontend\Nail04o;

use Livewire\Component;
use App\Models\Article;

class AboutComponent extends Component
{
    public function render()
    {
        return view('frontend.nail04-o.about',[
            'articles'=>Article::where([['category','About Us'],['status','PUBLISHED']])->get()
        ])->layout('frontend.nail04-o.layout.layout');
    }
}
