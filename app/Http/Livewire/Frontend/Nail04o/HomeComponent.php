<?php

namespace App\Http\Livewire\Frontend\Nail04o;

use Livewire\Component;
use App\Models\Album;
use App\Models\Asset;
use App\Models\Article;
use App\Models\Product;
use App\Models\Category;


class HomeComponent extends Component
{
    public $item_id = 0;
    public function render()
    {
        $carousel_customers = Article::where([['category','Home Carousel'],['status','PUBLISHED']])->get();
        
        $services= Product::where(
            [
                ['category_id',Category::where('name','Services')->first()['id']],
            ])->get();    


        $gallery = Album::where([['type', 'Gallery'],['status','PUBLISHED']])->first();
        $gallery_list =  Asset::where('albums_id',$gallery['id'])->orderBy('priority', 'desc')->get();
        
        $home_carousel = Album::where([['type','Home Carousel'],['status','PUBLISHED']])->first();
        $imgs_carousel =  Asset::where('albums_id',$home_carousel['id'])->orderBy('priority', 'desc')->get();

        $header= Product::where('category_id',Category::where('name','Header And Footer')->first()['id'])->first();

        $home_article = Article::where([['category','Home Article'],['status','PUBLISHED']])->first();


        $album_popup2 = Album::where([['type','Home Popup_2'],['status','PUBLISHED']])->first();
        $img_popup2 = Asset::where('albums_id',$album_popup2['id'])->orderBy('priority', 'desc')->first();

        $home_album = Album::where([['type','About Us'],['status','PUBLISHED']])->first();
        $home_asset = Asset::where('albums_id',$home_album['id'])->orderBy('priority', 'desc')->get();
        return view('frontend.nail04-o.index',[
            'imgs_carousel'=> $imgs_carousel,
            'services'=> $services,
            'home_article' => $home_article,
            'gallery_list' => $gallery_list,
            'home_carousel'=> $home_carousel,
            'carousel_customers'=> $carousel_customers,
            'header'=> $header,
            'home_album'=>$home_album,
            'home_asset'=>$home_asset,
            'img_popup2'=>$img_popup2,
            ])->layout('frontend.nail04-o.layout.layout');
    }
}
