<?php

namespace App\Http\Livewire\Frontend\Nail01d;

use Livewire\Component;
use App\Models\Product;
use App\Models\Category;
use Illuminate\Http\Request;

class BookComponent extends Component
{
    public function render()
    {
        return view('frontend.nail01-d.book',['services'=>Product::where(
                [
                    ['category_id',Category::where('name','Services')->first()['id']],
                ])->get()
            ])->layout('frontend.nail01-d.layout.layout');
    }

    public function get_hours(Request $request) {
        $day =$request->input('input_date');
        $this->day=$day;
        $htmlMorning = '<ul class="time-items" id="timeAMHtml">
        <li>
            <a href="#open_booking" valhours="10:00" class="open_booking">10:00 am</a>
            <a href="#open_booking" valhours="10:30" class="open_booking">10:30 am</a>
            <a href="#open_booking" valhours="11:00" class="open_booking">11:00 am</a>
            <a href="#open_booking" valhours="11:30" class="open_booking">11:30 am</a>
        </li>
    </ul>';
    $htmlAfternoon = '<ul class="time-items" id="timeAMHtml">
    <li>
        <a href="#open_booking" valhours="12:00" class="open_booking">12:00 am</a>
        <a href="#open_booking" valhours="12:30" class="open_booking">12:30 am</a>
        <a href="#open_booking" valhours="13:00" class="open_booking">1:00 am</a>
        <a href="#open_booking" valhours="13:30" class="open_booking">1:30 am</a>
    </li>
</ul>';
        date_default_timezone_set("Asia/Bangkok");
        $currentHour =date('H');
        $checkmorning =$currentHour;
        $checkafternoon=1;

        return response()->json ([
            'input_date'=> $day,
            'input_services'=> $currentHour,
            'input_staffs'=> "0",
            'htmlMorning'=>$htmlMorning,
            'htmlAfternoon' =>$htmlAfternoon,
        ]);
    }
    public function saveform(Request $request) {

        $day =$request->input('booking_date');
        $this->day=$day;
    }

}
