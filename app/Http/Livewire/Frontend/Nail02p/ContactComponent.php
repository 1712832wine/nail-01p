<?php

namespace App\Http\Livewire\Frontend\Nail02p;

use Livewire\Component;
use App\Models\Product;
use App\Models\Category;
class ContactComponent extends Component
{
    public function render()
    {
        $header= Product::where('category_id',Category::where('name','Header And Footer')->value('id'))->first();
        return view('frontend.nail02-p.contact',['header' => $header])->layout('frontend.nail02-p.layout.layout');;
    }
}
