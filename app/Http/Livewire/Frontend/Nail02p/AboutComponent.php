<?php

namespace App\Http\Livewire\Frontend\Nail02p;

use Livewire\Component;
use App\Models\Article;
use App\Models\Product;
use App\Models\Category;

class AboutComponent extends Component
{

    public function render()
    {
        $header= Product::where('category_id',Category::where('name','Header And Footer')->value('id'))->first();
        $articles = Article::where([['category','About Us'],['status','PUBLISHED']])->get();
        return view('frontend.nail02-p.about',[
            'articles' => $articles,
            'header'=>$header
        ])->layout('frontend.nail02-p.layout.layout');
    }
}
