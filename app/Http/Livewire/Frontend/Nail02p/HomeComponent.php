<?php

namespace App\Http\Livewire\Frontend\Nail02p;

use Livewire\Component;
use App\Models\Album;
use App\Models\Asset;
use App\Models\Article;
use App\Models\Product;
use App\Models\Category;


class HomeComponent extends Component
{
    public $item_id = 0;
    public function render()
    {
        $carousel_customers = Article::where([['category','Home Carousel'],['status','PUBLISHED']])->get();

        $services= Product::where(
            [
                ['category_id',Category::where('name','Services')->value('id')],
            ])->get();


        $gallery = Album::where([['type', 'Gallery'],['status','PUBLISHED']])->value('id');
        $gallery_list =  Asset::where('albums_id',$gallery)->orderBy('priority', 'desc')->get();

        $home_carousel = Album::where([['type','Home Carousel'],['status','PUBLISHED']])->value('id');
        $imgs_carousel =  Asset::where('albums_id',$home_carousel)->orderBy('priority', 'desc')->get();

        $header= Product::where('category_id',Category::where('name','Header And Footer')->value('id'))->first();

        $home_article = Article::where([['category','Home Article'],['status','PUBLISHED']])->first();
        $album = Album::where([['type', 'Social'], ['status', 'PUBLISHED']])->value('id');
        $social = Asset::where('albums_id', $album)->orderBy('priority', 'desc')->get();



        return view('frontend.nail02-p.index',[
            'imgs_carousel'=> $imgs_carousel,
            'services'=> $services,
            'home_article' => $home_article,
            'gallery_list' => $gallery_list,
            'home_carousel'=> $home_carousel,
            'carousel_customers'=> $carousel_customers,
            'header'=> $header,
            'social'=>$social,
            ])->layout('frontend.nail02-p.layout.layout');
    }
}
