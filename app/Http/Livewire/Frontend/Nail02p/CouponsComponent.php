<?php

namespace App\Http\Livewire\Frontend\Nail02p;

use Livewire\Component;
use App\Models\Album;
use App\Models\Asset;

class CouponsComponent extends Component
{
    public function render()
    {
        $coupons = Album::where([['type','Coupons'],['status','PUBLISHED']])->value('id');
        $list = Asset::where('albums_id',$coupons)->orderBy('priority', 'desc')->get();
        return view('frontend.nail02-p.coupons',['list' => $list])->layout('frontend.nail02-p.layout.layout');
    }
}
