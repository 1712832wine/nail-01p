<?php

namespace App\Http\Livewire\Frontend\Nail01j;

use Livewire\Component;
use App\Models\Album;
use App\Models\Asset;
use App\Models\Article;
use App\Models\Product;
use App\Models\Category;


class HomeComponent extends Component
{
    public $item_id = 0;
    public function render()
    {
        $carousel_customers = Article::where([['category','Home Carousel'],['status','PUBLISHED']])->get();
        
        $services= Product::where(
            [
                ['category_id',Category::where('name','Services')->first()['id']],
            ])->get();    


        $gallery = Album::where([['type', 'Gallery'],['status','PUBLISHED']])->first();
        $gallery_list =  Asset::where('albums_id',$gallery['id'])->orderBy('priority', 'desc')->get();
        
        $home_carousel = Album::where([['type','Home Carousel'],['status','PUBLISHED']])->first();
        $imgs_carousel =  Asset::where('albums_id',$home_carousel['id'])->orderBy('priority', 'desc')->get();

        $header= Product::where('category_id',Category::where('name','Header And Footer')->first()['id'])->first();

        $home_article = Article::where([['category','Home Article'],['status','PUBLISHED']])->first();

        $albums = Album::where([['type', 'Gallery'],['status','PUBLISHED']])->orderBy('priority')->get();
        $lists = [];
        foreach($albums as $item) {
            $assets_of_item =  Asset::where('albums_id',$item['id'])->orderBy('priority', 'desc')->get();
            array_push($lists, $assets_of_item);
        }


        return view('frontend.nail01-j.index',[
            'imgs_carousel'=> $imgs_carousel,
            'services'=> $services,
            'home_article' => $home_article,
            'gallery_list' => $gallery_list,
            'home_carousel'=> $home_carousel,
            'carousel_customers'=> $carousel_customers,
            'header'=> $header,
            'lists'=>$lists,
            ])->layout('frontend.nail01-j.layout.layout');
    }
}
