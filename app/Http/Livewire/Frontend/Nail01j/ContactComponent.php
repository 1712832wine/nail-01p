<?php

namespace App\Http\Livewire\Frontend\Nail01j;

use Livewire\Component;
use App\Models\Product;
use App\Models\Category;
class ContactComponent extends Component
{
    public function render()
    {
        $header= Product::where('category_id',Category::where('name','Header And Footer')->first()['id'])->first();
        return view('frontend.nail01-j.contact',['header' => $header])->layout('frontend.nail01-j.layout.layout');;
    }
}
