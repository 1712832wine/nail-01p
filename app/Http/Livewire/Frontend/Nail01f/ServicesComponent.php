<?php

namespace App\Http\Livewire\Frontend\Nail01f;

use Livewire\Component;
use App\Models\Product;
use App\Models\Category;
use App\Models\Article;

class ServicesComponent extends Component
{
    public $service_id = "0";
    public function mount($slug = '',$service_id = "0")
    {
        $this->service_id = $service_id ;
    }
    
    public function render()
    {
        $services = Product::where([['category_id',Category::where('name','Services')->first()['id']]])->get();
        $header = Product::where('category_id',Category::where('name','Header And Footer')->first()['id'])->first();
        $intro = Article::where([['category','Services_intro'],['status','PUBLISHED']])->first();
        return view('frontend.nail01-f.services',[
        'services'=>$services,
        'intro'=>$intro,
        'header'=>$header,])->layout('frontend.nail01-f.layout.layout');
    }
}
