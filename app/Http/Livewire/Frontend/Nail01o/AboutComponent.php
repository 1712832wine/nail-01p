<?php

namespace App\Http\Livewire\Frontend\Nail01o;

use Livewire\Component;
use App\Models\Article;

class AboutComponent extends Component
{
    public function render()
    {
        return view('frontend.nail01-o.about',[
            'articles'=>Article::where([['category','About Us'],['status','PUBLISHED']])->get()
        ])->layout('frontend.nail01-o.layout.layout');
    }
}
