<?php

namespace App\Http\Livewire\Frontend\Nail01o;

use Livewire\Component;
use App\Models\Product;
use App\Models\Category;
use App\Models\Article;

class ServicesComponent extends Component
{
    public $service_id = "0";
    public function mount($slug = '',$service_id = "0")
    {
        $this->service_id = $service_id ;
    }
    public function render()
    {
        return view('frontend.nail01-o.services',['services'=>Product::where(
            [
                ['category_id',Category::where('name','Services')->first()['id']],
            ])->get(),
            'intro'=>Article::where([['category','Services_intro'],['status','PUBLISHED']])->first()])->layout('frontend.nail01-o.layout.layout');
    }
}
