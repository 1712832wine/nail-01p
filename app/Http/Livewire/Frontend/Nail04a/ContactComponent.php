<?php

namespace App\Http\Livewire\Frontend\Nail04a;

use Livewire\Component;
use App\Models\Product;
use App\Models\Category;
class ContactComponent extends Component
{
    public function render()
    {
        $header= Product::where('category_id',Category::where('name','Header And Footer')->first()['id'])->first();
        return view('frontend.nail04-a.contact',['header' => $header])->layout('frontend.nail04-a.layout.layout');;
    }
}
