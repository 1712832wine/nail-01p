<?php

namespace App\Http\Livewire\Frontend\Nail04a;

use Livewire\Component;
use App\Models\Article;

class AboutComponent extends Component
{
    public function render()
    {
        $carousel_customers = Article::where([['category','Home Carousel'],['status','PUBLISHED']])->get();
        $articles = Article::where([['category','About Us'],['status','PUBLISHED']])->get();
        return view('frontend.nail04-a.about',[
            'articles' => $articles,
            'carousel_customers' => $carousel_customers,
        ])->layout('frontend.nail04-a.layout.layout');
    }
}
