<?php

namespace App\Http\Livewire\Frontend\Nail01n;

use Livewire\Component;
use App\Models\Album;
use App\Models\Asset;

class CouponsComponent extends Component
{
    public function render()
    {
        $coupons = Album::where([['type','Coupons'],['status','PUBLISHED']])->first();
        $list = Asset::where('albums_id',$coupons['id'])->orderBy('priority', 'desc')->get();
        return view('frontend.nail01-n.coupons',['list' => $list])->layout('frontend.nail01-n.layout.layout');
    }
}
