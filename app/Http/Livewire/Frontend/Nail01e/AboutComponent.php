<?php

namespace App\Http\Livewire\Frontend\Nail01e;

use Livewire\Component;
use App\Models\Article;

class AboutComponent extends Component
{
    public function render()
    {
        return view('frontend.nail01-e.about',[
            'articles'=>Article::where([['category','About Us'],['status','PUBLISHED']])->get()
        ])->layout('frontend.nail01-e.layout.layout');
    }
}
