<?php

namespace App\Http\Livewire\Frontend\Nail01p;

use Livewire\Component;
use App\Models\Album;
use App\Models\Asset;
class GalleryComponent extends Component
{
    public function render()
    {
        $albums = Album::where([['type', 'Gallery'],['status','PUBLISHED']])->orderBy('priority')->get();
        $lists = [];
        foreach($albums as $item) {
            $assets_of_item =  Asset::where('albums_id',$item['id'])->orderBy('priority', 'desc')->get();
            array_push($lists, $assets_of_item);
        }
        return view('frontend.nail01-p.gallery',['albums' => $albums,'lists'=> $lists])->layout('frontend.nail01-p.layout.layout');
    }
}
