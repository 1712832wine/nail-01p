<?php

namespace App\Http\Livewire\Frontend\Nail01p;

use Livewire\Component;
use App\Models\Album;
use App\Models\Asset;
use App\Models\Article;
use App\Models\Product;
use App\Models\Category;


class HomeComponent extends Component
{
    public $item_id = 0;
    public function render()
    {
        $coupons = Album::where([['type','Coupons'],['status','PUBLISHED']])->first();
        $list_coupons = Asset::where('albums_id',$coupons['id'])->orderBy('priority', 'desc')->get();

        $album_popup2 = Album::where([['type','Home Popup_2'],['status','PUBLISHED']])->first();
        $img_popup2 = Asset::where('albums_id',$album_popup2['id'])->orderBy('priority', 'desc')->first();

        $home_carousel = Album::where([['type','Home Carousel'],['status','PUBLISHED']])->first();
        $imgs_carousel =  Asset::where('albums_id',$home_carousel['id'])->orderBy('priority', 'desc')->get();

        $about_article = Article::where([['category','About Us'],['status','PUBLISHED']])->first();
        
        $services= Product::where(
        [
            ['category_id',Category::where('name','Services')->first()['id']],
        ])->get();      

        $counter = Product::where([
            ['category_id',Category::where('name','Counter')->first()['id']],
        ])->first();

        $home_article = Product::where([
            ['category_id',Category::where('name','Home Article')->first()['id']],
        ])->first();

        $home_article2 = Article::where([['category','Home Article'],['status','PUBLISHED']])->first();

        $galleries = Album::where([['type', 'Gallery'],['status','PUBLISHED']])->orderBy('priority')->get();
        $galleries_lists = [];
        foreach($galleries as $item) {
            $assets_of_item =  Asset::where('albums_id',$item['id'])->orderBy('priority', 'desc')->get();
            array_push($galleries_lists, $assets_of_item);
        }

        $home_carousel = Article::where([['category','Home Carousel'],['status','PUBLISHED']])->get();

        return view('frontend.nail01-p.index',[
            'list_coupons' => $list_coupons,
            'img_popup2' => $img_popup2,
            'imgs_carousel'=>$imgs_carousel,
            'about_article'=>$about_article,
            'services'=> $services,
            'counter'=> $counter,
            'home_article' => $home_article,
            'home_article2'=> $home_article2,
            'galleries' => $galleries,
            'galleries_lists' =>$galleries_lists,
            'home_carousel'=>$home_carousel,
            ])->layout('frontend.nail01-p.layout.layout');
    }
}
