<?php

namespace App\Http\Livewire\Frontend\Nail01p;

use Livewire\Component;
use App\Models\Product;
use App\Models\Category;

class BookComponent extends Component
{
    public function render()
    {
        return view('frontend.nail01-p.book',['services'=>Product::where(
                [
                    ['category_id',Category::where('name','Services')->first()['id']],
                ])->get()
            ])->layout('frontend.nail01-p.layout.layout');
    }
}
