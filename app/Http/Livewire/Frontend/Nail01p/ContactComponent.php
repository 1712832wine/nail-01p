<?php

namespace App\Http\Livewire\Frontend\Nail01p;

use Livewire\Component;

class ContactComponent extends Component
{
    public $route_name = "contact";
    public function render()
    {
        return view('frontend.nail01-p.contact')->layout('frontend.nail01-p.layout.layout');
    }
}
