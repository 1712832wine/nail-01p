<?php

namespace App\Http\Livewire\Frontend\Nail01p;

use Livewire\Component;
use App\Models\Product;
use App\Models\Category;

class GiftCardsComponent extends Component
{
    public $selected, $selected_img, $selected_name='N/A', $amount = 0, $quantity = 1;
    public $from = ['name' => null,'email' => null,'phone' => null];
    public $to = ['name' => null,'email' => null,'message' => null];
    public $subtotal = 'N/A', $discount = 'N/A', $tax = 'N/A', $total = 'N/A';

    public $send_to_relative = false;
    public $price; 
    public function updatedQuantity(){
        $this->quantity = (int)$this->quantity;
        $this->subtotal = substr($this->price, 1) * $this->quantity;
        if ($this->selected)
            $this->total = $this->subtotal + substr($this->discount,1)  +substr($this->tax , 1);
    }
    public function changeCard($id){
        $this->selected = $id;
        $product = Product::findOrFail($id);
        if (count(json_decode($product['images'])) > 0) $this->selected_img = json_decode($product['images'])[0];
        $this->selected_name = $product['name'];
        $features = json_decode($product['features']);
        if (count($features) > 0) $this->price = $features[0]->desc;
        if (count($features) > 1) $this->discount = $features[1]->desc;
        if (count($features) > 1) $this->tax = $features[2]->desc;
        
        $this->subtotal = substr($this->price, 1) * $this->quantity;
        $this->total = $this->subtotal + substr($this->discount,1)  +substr($this->tax , 1);
    }

    protected $rules = [
        'from.name' => 'required|min:6',
        'from.email' => 'required|email',
    ];

    public function checkPaypal(){
        $this->validate();
        if ($this->selected){
            redirect()->route('payment');
        } else{
            $this->emit('swal:modal', [
                'type'    => 'warning',
                'icon'    => 'warning',
                'title'   => 'Prolems!',
                'text'    => "You have not choose giftcard!!!",
            ]);
        }
    }

    public function render()
    {
        $giftcards = Product::where([
            ['category_id',Category::where('name','Gift Card')->first()['id']],
            ])->get();
        return view('frontend.nail01-p.giftcards',['giftcards'=>$giftcards])->layout('frontend.nail01-p.layout.layout');
        
    }
}
