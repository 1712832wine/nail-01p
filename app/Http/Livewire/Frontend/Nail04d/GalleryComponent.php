<?php

namespace App\Http\Livewire\Frontend\Nail04d;

use Livewire\Component;
use App\Models\Album;
use App\Models\Asset;
use Symfony\Component\HttpFoundation\Response;
class GalleryComponent extends Component
{
    public function render()
    {
        $albums = Album::where([['type', 'Gallery'],['status','PUBLISHED']])->orderBy('priority')->get();
        $lists = [];
        foreach($albums as $item) {
            $assets_of_item =  Asset::where('albums_id',$item['id'])->orderBy('priority', 'desc')->get();
            array_push($lists, $assets_of_item);
        }
        return view('frontend.nail04-d.gallery',['albums' => $albums,'lists'=> $lists])->layout('frontend.nail04-d.layout.layout');
    }
}
