<?php

namespace App\Http\Livewire\Frontend\Nail04k;

use Livewire\Component;
use App\Models\Article;

class AboutComponent extends Component
{
    public function render()
    {
        return view('frontend.nail04-k.about',[
            'articles'=>Article::where([['category','About Us'],['status','PUBLISHED']])->get()
        ])->layout('frontend.nail04-k.layout.layout');
    }
}
