<?php

namespace App\Http\Livewire\Frontend\Nail02c;

use Livewire\Component;
use App\Models\Product;
use App\Models\Category;
class ContactComponent extends Component
{
    public function render()
    {
        $header= Product::where('category_id',Category::where('name','Header And Footer')->first()['id'])->first();
        return view('frontend.nail02-c.contact',['header' => $header])->layout('frontend.nail02-c.layout.layout');;
    }
}
