<?php

namespace App\Http\Livewire\Frontend\Nail02c;

use Livewire\Component;
use App\Models\Album;
use App\Models\Asset;

class CouponsComponent extends Component
{
    public function render()
    {
        $coupons = Album::where([['type','Coupons'],['status','PUBLISHED']])->first();
        $list = Asset::where('albums_id',$coupons['id'])->orderBy('priority', 'desc')->get();
        return view('frontend.nail02-c.coupons',['list' => $list])->layout('frontend.nail02-c.layout.layout');
    }
}
