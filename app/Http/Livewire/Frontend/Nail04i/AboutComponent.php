<?php

namespace App\Http\Livewire\Frontend\Nail04i;

use Livewire\Component;
use App\Models\Article;

class AboutComponent extends Component
{
    public function render()
    {
        return view('frontend.nail04-i.about',[
            'articles'=>Article::where([['category','About Us'],['status','PUBLISHED']])->get()
        ])->layout('frontend.nail04-i.layout.layout');
    }
}
