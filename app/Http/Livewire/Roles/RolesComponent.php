<?php

namespace App\Http\Livewire\Roles;

use Livewire\Component;

use App\Models\Role;
use App\Models\Permission;
use App\Http\Livewire\Component\Alert;
use Livewire\WithPagination;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;

class RolesComponent extends Component
{
    use WithPagination;
    use AuthorizesRequests;
    protected $paginationTheme = 'bootstrap';
    // search
    public $search = '',$currentName='';
    // form
    public $name, $permissions=[], $item_id,
    $Permission_values=[];
    public $pagination_size = 6;
    public $type;
    // , $type, $link;
    public $isOpen = false;
    protected $listeners = ['Role:delete' => 'delete','Role:deleteSelected'=>'deleteSelected'];
    //select
    public $selectAll =false, $selected=[];
    public function updatedSelectAll($value){
        if($value){
            $this->selected = Role::where('name', 'like', '%'.$this->search.'%')->pluck('id');
        }else{
            $this->selected = [];
        }
    }
    // pagination
    public function updatingPaginationSize()
    {
        $this->resetPage();
    }
    public function updatingSearch()
    {
        $this->resetPage();
    }

    public function render()
    {
        $this->Permission_values = Permission::get('name');
        return view('livewire.roles.roles-component',[ 'list' => Role::where('name', 'like', '%'.$this->search.'%')->paginate($this->pagination_size)]);
    }
    //  CRUD
    public function create($type)
    {
        $this->authorize('create', Role::class);
        $this->type = $type;
        $this->resetInputFields();
        $this->openModal();
    }

    public function openModal()
    {
        $this->isOpen = true;
    }

    public function closeModal()
    {
        $this->isOpen = false;
    }

    private function resetInputFields(){
        $this->item_id='';
        $this->name = '';
        $this->permissions = [];
    }

    public function saveAndBack()
    {
        if($this->currentName)
        if($this->name==$this->currentName)
        Role::where('name',$this->name)->delete();

        $this->validate([
            'name' => ['required','unique:roles']
        ]);

        Role::updateOrCreate(['id' => $this->item_id], [
            'name' => $this->name,
            'permissions' => json_encode($this->permissions)
        ]);

        $this->closeModal();
        $this->resetInputFields();
    }
    public function edit($id, $type)
    {
        $this->authorize('update',Role::find($id));
        $this->type = $type;
        $Role = Role::findOrFail($id);
        $this->item_id = $id;
        $this->name = $Role->name;
        $this->currentName = $Role->name;
        $this->permissions = json_decode($Role->permissions);
        $this->openModal();
    }

    public function delete($id)
    {
        Role::find($id)->delete();
        $this->emit('swal:modal', [
            'type'    => 'success',
            'icon'    => 'success',
            'title'   => 'Delete Success!',
            'text'    => "The item has been deleted successfully",
        ]);

    }
    public function confirmDelete($id) {
        $this->authorize('delete',Role::find($id));
        $this->emit("swal:confirm", [
            'icon'        => 'warning',
            'type'        => 'warning',
            'title'       => 'Warning',
            'text'        => "Are you sure you want to delete this item?",
            'confirmText' => 'Delete',
            'method'      => 'Role:delete',
            'params'      => $id, // optional, send params to success confirmation
        ]);
    }

     // DELETE SELECTED
     public function deleteSelected(){
        Role::whereIn('id',$this->selected)->delete();
        $this->selected=[];
        $this->selectAll=false;
        $this->emit('swal:modal', [
            'type'    => 'success',
            'icon'    => 'success',
            'title'   => 'Delete success!',
            'text'    => "These items has been deleted successfully",
        ]);
    }
    public function confirmDeleteSelected(){
        if (count($this->selected) > 0)
        {
            $this->authorize('delete',Role::find($this->selected[0]));
            $this->emit("swal:confirm", [
                'icon'        => 'warning',
                'type'        => 'warning',
                'title'       => 'Warning',
                'text'        => 'Are you sure you want to delete these items?',
                'confirmText' => 'Delete',
                'method'      => 'Role:deleteSelected',
            
            ]);
        }
        else
        {
            $this->emit('swal:modal', [
                'type'    => 'warning',
                'icon'    => 'warning',
                'title'   => 'Nothing to delete!',
                'text'    => "You have not choose any item to delete",
            ]);
        }
    }
}
