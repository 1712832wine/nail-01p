<?php

namespace App\Http\Livewire\Menu;

use Livewire\Component;

use App\Models\Menu;
use App\Http\Livewire\Component\Alert;
use Livewire\WithPagination;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;

class MenuComponent extends Component
{
    // pagination
    use WithPagination;
    use AuthorizesRequests;
    protected $paginationTheme = 'bootstrap';
    // search
    public $search = '';
    // form
    public $label, $parent, $type_input, $link, $item_id;
    public $pagination_size = 6;
    public $type;
    // , $type, $link;
    public $isOpen = false;
    protected $listeners = ['menu:delete' => 'delete',
            'Menu:deleteSelected' => 'deleteSelected'];

    public $selectAll =false, $selected=[];
    public function updatedSelectAll($value){
        if($value){
            $this->selected = Menu::where('label', 'like', '%'.$this->search.'%')->pluck('id');
        }else{
            $this->selected = [];
        }
    }
    // pagination
    public function updatingPaginationSize()
    {
        $this->resetPage();
    }
    public function updatingSearch()
    {
        $this->resetPage();
    }

    public function render()
    {
        return view('livewire.menu.menu-component',[ 'list' => Menu::where('label', 'like', '%'.$this->search.'%')->paginate($this->pagination_size)]);
    }
    //  CRUD
    public function create($type)
    {
        $this->authorize('create', Menu::class);
        $this->type = $type;
        $this->resetInputFields();
        $this->openModal();
    }

    public function openModal()
    {
        $this->isOpen = true;
    }

    public function closeModal()
    {
        $this->isOpen = false;
    }

    private function resetInputFields(){
        $this->item_id=null;
        $this->parent = '';
        $this->label = '';
        $this->type_input = 'page_link';
        $this->link = '';
    }

    public function saveAndBack()
    {

        Menu::updateOrCreate(['id' => $this->item_id], [
            'label' => $this->label,
            'parent' => $this->parent,
            'link' => $this->link,
            'type' => $this->type_input
        ]);

        $this->closeModal();
        $this->resetInputFields();
    }

    public function edit($id, $type)
    {
        $this->authorize('update',Menu::find($id));
        $this->type = $type;
        $menu = Menu::findOrFail($id);
        $this->item_id = $id;
        $this->label = $menu->label;
        $this->parent = $menu->parent;
        $this->link = $menu->link;
        $this->type_input = $menu->type;
        $this->openModal();
    }

    public function delete($id)
    {
        Menu::find($id)->delete();
        $this->emit('swal:modal', [
            'type'    => 'success',
            'icon'    => 'success',
            'title'   => 'Delete Success!',
            'text'    => "The item has been deleted successfully",
        ]);

    }
    public function confirmDelete($id) {
        $this->authorize('delete',Menu::find($id));
        $this->emit("swal:confirm", [
            'icon'        => 'warning',
            'type'        => 'warning',
            'title'       => 'Warning',
            'text'        => "Are you sure you want to delete this item?",
            'confirmText' => 'Delete',
            'method'      => 'menu:delete',
            'params'      => $id, // optional, send params to success confirmation
        ]);
    }
    // DELETE SELECTED
    public function deleteSelected(){
        Menu::whereIn('id',$this->selected)->delete();
        $this->selected=[];
        $this->selectAll=false;
        $this->emit('swal:modal', [
            'type'    => 'success',
            'icon'    => 'success',
            'title'   => 'Delete success!',
            'text'    => "These items has been deleted successfully",
        ]);
    }
    public function confirmDeleteSelected(){
        if (count($this->selected) > 0)
        {
            $this->authorize('delete',Menu::find($this->selected[0]));
            $this->emit("swal:confirm", [
                'icon'        => 'warning',
                'type'        => 'warning',
                'title'       => 'Warning',
                'text'        => 'Are you sure you want to delete these items?',
                'confirmText' => 'Delete',
                'method'      => 'Menu:deleteSelected',
            
            ]);
        }
        else
        {
            $this->emit('swal:modal', [
                'type'    => 'warning',
                'icon'    => 'warning',
                'title'   => 'Nothing to delete!',
                'text'    => "You have not choose any item to delete",
            ]);
        }
    }

}
