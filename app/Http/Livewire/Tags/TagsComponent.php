<?php

namespace App\Http\Livewire\Tags;

use Livewire\Component;

use App\Models\Tag;
use App\Http\Livewire\Component\Alert;
use Livewire\WithPagination;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;

class TagsComponent extends Component
{
    use AuthorizesRequests;
    use WithPagination;
    protected $paginationTheme = 'bootstrap';
    // search
    public $search = '';
    // form
    public $name, $slug,$currentName;
    public $item_id;
    public $pagination_size = 6;
    public $type;
    // , $type, $link;
    public $isOpen = false;
    protected $listeners = ['tag:delete' => 'delete','Tag:deleteSelected'=>'deleteSelected'];
    //select
    public $selectAll =false, $selected=[];
    public function updatedSelectAll($value){
        if($value){
            $this->selected = Tag::where('name', 'like', '%'.$this->search.'%')->pluck('id');
        }else{
            $this->selected = [];
        }
    }
    // pagination
    public function updatingPaginationSize()
    {
        $this->resetPage();
    }
    public function updatingSearch()
    {
        $this->resetPage();
    }

    public function render()
    {
        return view('livewire.tags.tags-component',[ 'list' => Tag::where('name', 'like', '%'.$this->search.'%')->paginate($this->pagination_size)]);
    }
    //  CRUD
    public function create($type)
    {
        $this->authorize('create', Tag::class);
        $this->type = $type;
        $this->resetInputFields();
        $this->openModal();
    }

    public function openModal()
    {
        $this->isOpen = true;
    }

    public function closeModal()
    {
        $this->isOpen = false;
    }

    private function resetInputFields(){
        $this->item_id= null;
        $this->name = '';
        $this->slug = '';
    }

    public function saveAndBack()
    {
        if($this->currentName)
        if($this->name==$this->currentName)
        Tag::where('name',$this->name)->delete();

        $this->validate([
            'name' => ['required', 'unique:tags'],
        ]);

        Tag::updateOrCreate(['id' => $this->item_id], [
            'name' => $this->name,
            'slug' => $this->slug
        ]);

        $this->closeModal();
        $this->resetInputFields();
    }

    public function edit($id, $type)
    {
        $this->authorize('update',Tag::find($id));
        $this->type = $type;
        $Tag = Tag::findOrFail($id);
        $this->item_id = $id;
        $this->name = $Tag->name;
        $this->currentName = $Tag->name;
        $this->slug = $Tag->slug;
        $this->openModal();
    }

    public function delete($id)
    {
        Tag::find($id)->delete();
        $this->emit('swal:modal', [
            'type'    => 'success',
            'icon'    => 'success',
            'title'   => 'Delete Success!',
            'text'    => "The item has been deleted successfully",
        ]);

    }
    public function confirmDelete($id) {
        $this->authorize('delete',Tag::find($id));
        $this->emit("swal:confirm", [
            'icon'        => 'warning',
            'type'        => 'warning',
            'title'       => 'Warning',
            'text'        => "Are you sure you want to delete this item?",
            'confirmText' => 'Delete',
            'method'      => 'tag:delete',
            'params'      => $id, // optional, send params to success confirmation
        ]);
    }


    //clone
    public function clone($id){
        $this->authorize('update',Tag::find($id));
        $Tag = Tag::findOrFail($id);
        //
        $this->name = $Tag->name;
        $this->slug = $Tag->slug;
        $this->parent = $Tag->parent;
        //
        Tag::create([
            'name' => $this->name.'(copy)',
            'slug' => $this->slug,
            'parent'  => $this->parent,
        ]);
        //
        $this->emit('swal:modal', [
            'type'    => 'success',
            'icon'    => 'success',
            'title'   => 'Clone Success!',
            'text'    => "The item has been cloned successfully",
        ]);
    }

    // DELETE SELECTED
    public function deleteSelected(){
        Tag::whereIn('id',$this->selected)->delete();
        $this->selected=[];
        $this->selectAll=false;
        $this->emit('swal:modal', [
            'type'    => 'success',
            'icon'    => 'success',
            'title'   => 'Delete success!',
            'text'    => "These items has been deleted successfully",
        ]);
    }
    public function confirmDeleteSelected(){
        if (count($this->selected) > 0)
        {
            $this->authorize('delete',Tag::find($this->selected[0]));
            $this->emit("swal:confirm", [
                'icon'        => 'warning',
                'type'        => 'warning',
                'title'       => 'Warning',
                'text'        => 'Are you sure you want to delete these items?',
                'confirmText' => 'Delete',
                'method'      => 'Tag:deleteSelected',

            ]);
        }
        else
        {
            $this->emit('swal:modal', [
                'type'    => 'warning',
                'icon'    => 'warning',
                'title'   => 'Nothing to delete!',
                'text'    => "You have not choose any item to delete",
            ]);
        }
    }
}
