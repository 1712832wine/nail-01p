<?php

namespace App\Http\Livewire;

use Livewire\Component;

class Libs extends Component
{
    public function render()
    {
        return view('livewire.libs');
    }
}
