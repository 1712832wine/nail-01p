<?php

namespace App\Http\Livewire\Articles;

use Livewire\Component;
use Illuminate\Support\Facades\File;
use App\Models\Article;
use Livewire\WithPagination;
use Livewire\WithFileUploads;
use Illuminate\Support\Facades\Storage;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;

 function ValidateTitle($Title)
    {
        $fileNameTemp = preg_replace('/(à)|á|ạ|ả|ã|â|ầ|ấ|ậ|ẩ|ẫ|ă|ằ|ắ|ặ|ẳ|ẵ/', "a", $Title);
        $fileNameTemp = preg_replace('/è|é|ẹ|ẻ|ẽ|ê|ề|ế|ệ|ể|ễ/', "e", $fileNameTemp);
        $fileNameTemp = preg_replace('/ì|í|ị|ỉ|ĩ/', "i", $fileNameTemp);
        $fileNameTemp = preg_replace('/ò|ó|ọ|ỏ|õ|ô|ồ|ố|ộ|ổ|ỗ|ơ|ờ|ớ|ợ|ở|ỡ/', "o", $fileNameTemp);
        $fileNameTemp = preg_replace('/ù|ú|ụ|ủ|ũ|ư|ừ|ứ|ự|ử|ữ/', "u", $fileNameTemp);
        $fileNameTemp = preg_replace('/ỳ|ý|ỵ|ỷ|ỹ/', "y", $fileNameTemp);
        $fileNameTemp = preg_replace('/đ/', "d", $fileNameTemp);

        $fileNameTemp = preg_replace('/(À)|Á|Ạ|Ả|Ã|Â|Ầ|Ấ|Ậ|Ẩ|Ẫ|Ă|Ắ|Ằ|Ẳ|Ẵ|Ặ/', "a", $fileNameTemp);
        $fileNameTemp = preg_replace('/È|È|Ẹ|Ẻ|Ẽ|Ê|Ề|Ế|Ệ|Ể|Ễ/', "e", $fileNameTemp);
        $fileNameTemp = preg_replace('/Í|Ì|Ị|Ỉ|Ĩ/', "i", $fileNameTemp);
        $fileNameTemp = preg_replace('/Ó|Ò|Ỏ|Õ|Ọ|Ô|Ố|Ồ|Ổ|Ỗ|Ộ|Ớ|Ờ|Ở|Ỡ|Ợ|Ơ/', "o", $fileNameTemp);
        $fileNameTemp = preg_replace('/Ú|Ù|Ủ|Ũ|Ụ|Ư|Ứ|Ừ|Ử|Ữ|Ự/', "u", $fileNameTemp);
        $fileNameTemp = preg_replace('/Ý|Ỳ|Ỷ|Ỹ|Ỵ/', "y", $fileNameTemp);
        $fileNameTemp = preg_replace('/Đ/', "d", $fileNameTemp);
        $fileNameTemp = preg_replace('/ /', "-", $fileNameTemp);
        $fileNameTemp = strtolower( $fileNameTemp );
        return $fileNameTemp;
    }
class ArticlesComponent extends Component
{
    use AuthorizesRequests;
    use WithFileUploads;
    use WithPagination;


    protected $paginationTheme = 'bootstrap';
    // search
    public $search = '', $fields = 1, $count,$imageList=[] ,$currentTitle;
    // form
    public $title, $slug, $date,$description, $content, $images = [], $category,$template, $tags, $status = 'PUBLISHED', $featured;
    public $item_id;
    public $pagination_size = 6;
    public $type;
    public $Article;
    public $selected=[],$selectAll=false;
    // , $type, $link;
    public $isOpen = false;
    public $isShow = false;
    protected $listeners = ['Article:delete' => 'delete',
        'ChangePriority:change' => 'changePriority',
        'Article:deleteImage'=>'deleteImage',
                            'Article:deleteSelected'=>'deleteSelected'];
    // select

    public function updatedSelectAll($value){
        if($value){
            $this->selected = Article::where('category', 'like', '%' . $this->search . '%')
            ->orWhere('title', 'like', '%' . $this->search . '%')->pluck('id');
        }else{
            $this->selected = [];
        }
    }
    // pagination
    public function updatingPaginationSize()
    {
        $this->resetPage();
    }
    public function updatingSearch()
    {
        $this->resetPage();
        // $this->dispatchBrowserEvent('contentChanged');
    }

    public function render()
    {
        return view('livewire.articles.articles-component', [
            'list' => Article::where('category', 'like', '%' . $this->search . '%')->orderBy('priority')
            ->orWhere('title', 'like', '%' . $this->search . '%')->paginate($this->pagination_size)]);
    }
    //  CRUD
    public function create($type)
    {
        $this->authorize('create', Article::class);
        $this->type = $type;
        $this->resetInputFields();
        $this->openModal();
    }

    public function openModal()
    {
        $this->isOpen = true;
    }

    public function closeModal()
    {
        $this->isOpen = false;

    }

    private function resetInputFields()
    {
        $this->item_id=null;
        $this->title = '';
        $this->slug = '';
        $this->template = '';
        $this->date = '';
        $this->description = '';
        $this->content = '';
        $this->category = '';
        $this->images = [];
        $this->imageList = [];
        $this->tags = '';
        $this->status = '';
        $this->featured = 1;
    }


    public function saveAndBack()
    {
        // VALIDATE
        $this->validate([
            'title' => ['required'],
            'date' => 'required'
        ]);
        // SLUG
        if($this->slug ==null)
        {
            $saveSlug = ValidateTitle($this->title);
        }
        else {
            $saveSlug=$this->slug;
        }
        // FILE UPLOAD
        $images = $this->imageList;
        foreach ($this->images as $image) {
            $name = md5($image . microtime()).'.'.$image->extension();
            $image->storeAs('photos', $name);
            array_push($images, $name);
        }
        Article::updateOrCreate(['id' => $this->item_id], [
            'title' => $this->title,
            'slug' => $saveSlug,
            'date' => $this->date,
            'image' =>json_encode($images),
            'description' =>$this->description,
            'content' => $this->content,
            'blocks_content'=> json_encode(explode("<p>/new_block</p>", $this->content)),
            'category' => $this->category,
            'template' => $this->template,
            'tags' => $this->tags,
            'status' => $this->status,
            'featured' => $this->featured,
            'count' => $this->fields
        ]);
        $this->fields = 1;
        $this->closeModal();
        $this->resetInputFields();
    }


    public function clone($id){
        $this->authorize('update',Article::find($id));
        $Article = Article::findOrFail($id);
        $this->item_id = $id;
        $this->title = $Article->title;
        $this->currentTitle = $Article->title;
        $this->slug = $Article->slug;
        $this->date = $Article->date;
        $this->description = $Article->description;
        $this->images = json_decode($Article->image);
        $this->content = $Article->content;
        $this->template = $Article->template;
        $this->category = $Article->category;
        $this->tags = $Article->tags;
        $this->status = $Article->status;
        $this->featured = $Article->featured;
        //
        $images = [];
        foreach ($this->images as $image) {
            $full = explode(".", $image);
            $fileName = $full[0];
            $fileExt  = $full[1];
            $destinationFolder = public_path('storage/photos/');
            $num   = 1;
            $newName = $fileName. '(' . $num . ')';
            while (file_exists($destinationFolder . implode(".",array($newName,$fileExt)))) {
                $num ++;
                $newName = $fileName. '(' . $num . ')';
            }
            File::copy('storage/photos/'.$image, 'storage/photos/'.$newName . '.' . $fileExt);
            array_push($images,$newName . '.' . $fileExt);
        }
        //
        Article::create([
            'title' => $this->title.'(copy)',
            'slug' => $this->slug,
            'date' => $this->date,
            'image' =>json_encode($images),
            'content' => $this->content,
            'blocks_content'=> json_encode(explode("<p>/new_block</p>", $this->content)),
            'category' => $this->category,
            'template' => $this->template,
            'description'=>$this->description,
            'tags' => $this->tags,
            'status' => $this->status,
            'featured' => $this->featured,
            'count' => $this->fields
        ]);
        $this->resetInputFields();
        $this->emit('swal:modal', [
            'type'    => 'success',
            'icon'    => 'success',
            'title'   => 'Clone Success!',
            'text'    => "The item has been cloned successfully",
        ]);
    }

    public function edit($id, $type)
    {
        //render images
        $this->authorize('update',Article::find($id));
        $this->type = $type;
        //
        $Article = Article::findOrFail($id);
        $this->item_id = $id;
        $this->title = $Article->title;
        $this->currentTitle = $Article->title;
        $this->slug = $Article->slug;
        $this->date = $Article->date;
        $this->description = $Article->description;
        $this->content = $Article->content;
        $this->template = $Article->template;
        $this->imageList = json_decode($Article->image);
        $this->images = [];
        $this->category = $Article->category;
        $this->tags = $Article->tags;
        $this->status = $Article->status;
        $this->featured = $Article->featured;
        $this->openModal();
    }

    public function delete($id)
    {
        $Article = Article::findOrFail($id);
        $images = json_decode($Article['image']);
        foreach($images as $img) {
            Storage::delete('photos/'.$img);
        }
        Article::find($id)->delete();
        $this->emit('swal:modal', [
            'type'    => 'success',
            'icon'    => 'success',
            'title'   => 'Delete Success!',
            'text'    => "The item has been deleted successfully",
        ]);
    }
    public function deleteImage($id){
        $images = json_decode(Article::find($this->item_id)['image']);
        Storage::delete('photos/'.$images[$id]);
        array_splice($images, $id, 1);
        Article::updateOrCreate(['id' => $this->item_id], [
            'image' =>json_encode($images),
        ]);
        $this->imageList = $images;
        $this->emit('swal:modal', [
            'type'    => 'success',
            'icon'    => 'success',
            'title'   => 'Delete Success!',
            'text'    => "The item has been deleted successfully",
        ]);
    }
    //
    public function confirmDelete($id)
    {
        $this->authorize('delete',Article::find($id));
        $this->emit("swal:confirm", [
            'icon'        => 'warning',
            'type'        => 'warning',
            'title'       => 'Warning',
            'text'        => "Are you sure you want to delete this item?",
            'confirmText' => 'Delete',
            'method'      => 'Article:delete',
            'params'      => $id, // optional, send params to success confirmation
        ]);
    }
    //
    public function confirmDeleteAsset($id){
        $this->emit("swal:confirm", [
            'icon'        => 'warning',
            'type'        => 'warning',
            'title'       => 'Warning',
            'text'        => "Are you sure you want to delete this item?",
            'confirmText' => 'Delete',
            'method'      => 'Article:deleteImage',
            'params'      => $id, // optional, send params to success confirmation
            ]);
        }



    // DELETE SELECTED
    public function deleteSelected(){
        $articles = Article::whereIn('id',$this->selected)->get();
        foreach ($articles as $article){
            $images = json_decode($article['image']);
            foreach($images as $img) {
                Storage::delete('photos/'.$img);
            }
        }
        Article::whereIn('id',$this->selected)->delete();
        $this->selected=[];
        $this->selectAll=false;
        $this->emit('swal:modal', [
            'type'    => 'success',
            'icon'    => 'success',
            'title'   => 'Delete success!',
            'text'    => "These items has been deleted successfully",
        ]);
    }
    public function confirmDeleteSelected(){
        if (count($this->selected) > 0)
        {
            $this->authorize('delete',Article::find($this->selected[0]));
            $this->emit("swal:confirm", [
                'icon'        => 'warning',
                'type'        => 'warning',
                'title'       => 'Warning',
                'text'        => 'Are you sure you want to delete these items?',
                'confirmText' => 'Delete',
                'method'      => 'Article:deleteSelected',

            ]);
        }
        else
        {
            $this->emit('swal:modal', [
                'type'    => 'warning',
                'icon'    => 'warning',
                'title'   => 'Nothing to delete!',
                'text'    => "You have not choose any item to delete",
            ]);
        }
    }
    public function changePriority($order){
        $sorted = $order;
        sort($sorted, SORT_NATURAL | SORT_FLAG_CASE);
        foreach ($order as $index => $id){
            $Article = Article::find($id);
            $Article->priority = $sorted[$index];
            $Article->save();
        }
        $this->emit('swal:modal', [
            'type'    => 'success',
            'icon'    => 'success',
            'title'   => 'Change Priority Success!',
            'text'    => "These items have been change priority successfully",
        ]);
    }
    public function confirmChangePriority($order){
        $this->emit("swal:confirm", [
            'icon'        => 'warning',
            'type'        => 'warning',
            'title'       => 'Warning',
            'text'        => "Are you sure you want to change priority of this list?",
            'confirmText' => 'Change Priority',
            'method'      => 'ChangePriority:change',
            'params'      => $order, // optional, send params to success confirmation
        ]);
    }
}
