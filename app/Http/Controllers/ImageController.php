<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\Response;
use Illuminate\Http\Request;

class ImageController extends Controller
{
    public function image($fileName){
        $path = public_path().'/storage/photos/'.$fileName;
        return Response::download($path);        
    }
}
