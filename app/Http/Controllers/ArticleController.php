<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Article;

class ArticleController extends Controller
{
    public function tinCongDong()
    {
        //
        return Article::where([['category','Community'],['status','PUBLISHED']])->get();
    }
    public function tinTuyenDung()
    {
        //
        return Article::where([['category','Recruitment'],['status','PUBLISHED']])->get();
    }
    public function tinCongNghe()
    {
        //
        return Article::where([['category','Technology'],['status','PUBLISHED']])->get();
    }

    // /**
    //  * Store a newly created resource in storage.
    //  *
    //  * @param  \Illuminate\Http\Request  $request
    //  * @return \Illuminate\Http\Response
    //  */
    // public function store(Request $request)
    // {
    //     //
    //     return Article::create($request->all());
    // }

    // /**
    //  * Display the specified resource.
    //  *
    //  * @param  int  $id
    //  * @return \Illuminate\Http\Response
    //  */
    // public function show(Article $article)
    // {
    //     //
    //     return $article;
    // }

    // /**
    //  * Update the specified resource in storage.
    //  *
    //  * @param  \Illuminate\Http\Request  $request
    //  * @param  int  $id
    //  * @return \Illuminate\Http\Response
    //  */
    // public function update(Request $request, Article $article)
    // {
    //     //
    //     $article->update($request->all());
    // }

    // /**
    //  * Remove the specified resource from storage.
    //  *
    //  * @param  int  $id
    //  * @return \Illuminate\Http\Response
    //  */
    // public function destroy(Article $article)
    // {
    //     //
    //     $article->delete();
    // }
}
