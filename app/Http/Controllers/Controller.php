<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Http\Client\Request;
use App\Models\Contact;
class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;
    public function getLoginStatus(Request $request)
    {
        $loginInfo = ['isLogin' => false];
        if ($user = auth()->user()) {
            $loginInfo = [
                'isLogin' => true,
                'userId' => $user->id,
                'email' => 'test@example.com',
                'name' => 'User1'
            ];
        }

        return response()->json($loginInfo);
    }
}
