<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Contact;
use App\Models\Album;
use App\Models\Asset;


class handleController extends Controller
{
    function handleSecurity(){

    }
    function handleContact(Request $request)
    {
        try {
        $contact = new Contact;
        $data = $request->input();
        $contact->name = $data['contactname'];
        $contact->email = $data['contactemail'];
        $contact->subject = $data['contactsubject'];
        $contact->content = $data['contactcontent'];
        $contact->address='';
        $contact->save();
        return back()->with('status',"Insert successfully");
        }
        catch(Exception $e){
            return back()->with('failed',"operation failed");
        }
    }
    function getGallery(Request $request)
    {
        $lists = Asset::where('albums_id',$request->cat_id)->orderBy('priority', 'desc')->get();
        return response()->json($lists);
    }
}
