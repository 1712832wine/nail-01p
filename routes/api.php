<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\ArticleController;
use App\Http\Controllers\ImageController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});
// IMAGES
Route::get('image/{filename}',[ImageController::class,'image']);
// ARTICLES
Route::get('tin_cong_dong',[ArticleController::class,'tinCongDong']);
Route::get('tin_tuyen_dung',[ArticleController::class,'tinTuyenDung']);
Route::get('tin_cong_nghe',[ArticleController::class,'tinCongNghe']);
