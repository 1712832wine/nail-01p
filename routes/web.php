<?php

use App\Http\Livewire\AlbumAsset\AssetComponent;
use Illuminate\Support\Facades\Route;
use App\Http\Livewire\Menu\MenuComponent;
use App\Http\Livewire\Pages\PagesComponent;
use App\Http\Livewire\Tags\TagsComponent;
use App\Http\Livewire\Articles\ArticlesComponent;
use App\Http\Livewire\Categories\CategoriesComponent;
use App\Http\Livewire\Users\UsersComponent;
use App\Http\Livewire\Roles\RolesComponent;
use App\Http\Livewire\Permissions\PermissionsComponent;
use App\Http\Livewire\Albums\AlbumsComponent;
use App\Http\Livewire\Contacts\ContactsComponent;
use App\Http\Livewire\Recruitments\RecruitmentsComponent;
use App\Http\Livewire\FileManager\FileManager;
use App\Http\Livewire\Products\ProductsComponent;
// ------------------------------------------------------------------------

use App\Http\Livewire\Frontend\Nail02p\HomeComponent;
use App\Http\Livewire\Frontend\Nail02p\AboutComponent;
use App\Http\Livewire\Frontend\Nail02p\BookComponent;
use App\Http\Livewire\Frontend\Nail02p\ContactComponent;
use App\Http\Livewire\Frontend\Nail02p\CouponsComponent;
use App\Http\Livewire\Frontend\Nail02p\GalleryComponent;
use App\Http\Livewire\Frontend\Nail02p\GiftCardsComponent;
use App\Http\Livewire\Frontend\Nail02p\ServicesComponent;
use App\Http\Livewire\Frontend\Nail02p\GalleryDetailComponent;

// ------------------------------------------------------------------------\
use App\Http\Controllers\Controller;
use App\Http\Controllers\handleController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// ------------------------------------------------------- //
Route::middleware(['auth:sanctum', 'verified'])->prefix('admin')->group(function () {
    Route::get('/dashboard', function () { return view('dashboard'); })->name('dashboard');
    Route::get('/menu', MenuComponent::class)->name('menu');
    Route::get('/pages', PagesComponent::class)->name('pages');
    Route::get('/news/tags', TagsComponent::class)->name('tags');
    Route::get('/news/categories', CategoriesComponent::class)->name('categories');
    Route::get('/news/articles', ArticlesComponent::class)->name('articles');
    Route::get('/file_manager',FileManager::class)->name('file_manager');
    Route::get('/authentication/users', UsersComponent::class)->name('users');
    Route::get('/authentication/roles', RolesComponent::class)->name('roles');
    Route::get('/authentication/permissions', PermissionsComponent::class)->name('permissions');
    Route::get('/albums', AlbumsComponent::class)->name('albums');
    Route::get('/contacts', ContactsComponent::class)->name('contacts');
    Route::get('/recruitments', RecruitmentsComponent::class)->name('recruitments');
    Route::get('/products', ProductsComponent::class)->name('products');
    // Route::get('/assets/{id}', AssetComponent::class)->name('assets');
});
Route::group([], function () {

    Route::get('/', HomeComponent::class)->name('home');
    Route::get('/gioi-thieu', AboutComponent::class)->name('gioi-thieu');
    Route::get('/dat-cho', BookComponent::class)->name('dat-cho');
    Route::get('/lien-lac', ContactComponent::class)->name('lien-lac');
    Route::get('/phieu-giam-gia', CouponsComponent::class)->name('phieu-giam-gia');
    Route::get('/bo-suu-tap', GalleryComponent::class)->name('bo-suu-tap');
    Route::get('/the-qua-tang', GiftCardsComponent::class)->name('the-qua-tang');
    Route::get('/dich-vu', ServicesComponent::class)->name('dich-vu');

    Route::get('/', HomeComponent::class)->name('home');
    Route::get('/about', AboutComponent::class)->name('about');
    Route::get('/book', BookComponent::class)->name('book');
    Route::post('/book/get_hours',[BookComponent::class,'get_hours'])->name('get_hours');
    Route::post('/book/saveform',[BookComponent::class,'saveform'])->name('saveform');
    Route::get('/contact', ContactComponent::class)->name('contact');
    Route::get('/coupons', CouponsComponent::class)->name('coupons');
    Route::get('/gallery', GalleryComponent::class)->name('gallery');
    Route::get('/gallery-detail/{id}',GalleryDetailComponent::class)->name('gallery-detail');
    Route::get('/giftcards', GiftCardsComponent::class)->name('giftcards');
    Route::get('/services', ServicesComponent::class)->name('services');
    Route::get('/services/{slug}/{service_id}', ServicesComponent::class)->name('services-detail');



    // Route::post('/security/create',[handleController::class,'handleSecurity'])->name('security');
    Route::post('/contact/send',[handleController::class,'handleContact'])->name('send_contact');
    Route::post('/gallery/getlistbycat',[handleController::class,'getGallery'])->name('getgallery');
});

