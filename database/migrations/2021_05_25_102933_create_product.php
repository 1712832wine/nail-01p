<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProduct extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->id();
            $table->longtext('name');
            $table->longtext('description')->nullable();
            $table->text('images')->nullable();
            $table->longtext('details')->nullable();
            $table->longtext('features')->nullable();
            $table->bigInteger('price')->nullable();
            $table->bigInteger('category_id')->nullable();
            $table->longtext('type')->nullable();
            $table->longtext('extras')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
}
