(function () {
    "use strict";

    /*back to top*/
    $(document).on('click', '.back-to-top', function () {
        $("html,body").animate({
            scrollTop: 0
        }, 2000);
    });
})(jQuery);

$(document).ready(function () {
    /*Scroll to top*/
    $(window).on('scroll', function () {
        let ScrollTop = $('.back-to-top');
        if ($(window).scrollTop() > 1000) {
            ScrollTop.fadeIn(1000);
        } else {
            ScrollTop.fadeOut(1000);
        }
    });

    /*Gallery Isotope*/
    let galleryIsotope = $('.gallery-isotope').isotope({
        itemSelector: '.grid-item',
    });
    // Layout Isotope after each image loads
    galleryIsotope.imagesLoaded().progress(function () {
        galleryIsotope.isotope('layout');
    });

    /*Testimonial carousel*/
    let testimonialCarousel = $('.testimonial-carousel');
    if (testimonialCarousel.length > 0) {
        testimonialCarousel.owlCarousel({
            loop: false,
            autoplay: false,
            autoPlayTimeout: 1000,
            margin: 60,
            dots: true,
            nav: true,
            navText: ['<i class="fa fa-angle-left"></i>', '<i class="fa fa-angle-right"></i>'],
            animateOut: 'fadeOut',
            animateIn: 'fadeIn',
            items: 1,
        });
    }
});
