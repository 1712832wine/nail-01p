(function ($) {
    'use strict';
    /*
    * Set date: Init date time picker for booking
    * Note: place here for deny error when load booking email form in first
    */
    Date.prototype.addHours = function (h) {
        this.setTime(this.getTime() + (h * 60 * 60 * 1000));
        return this;
    }
    var today = new Date(currDateT);
    var future = new Date(currDateT);

    if (beforeTime == undefined || beforeTime == '' || beforeTime < 0) {
        beforeTime = 0;
    }
    var fourHoursLater = new Date().addHours(beforeTime);

    var set_date = parseInt(beforeDay) > 0 ? new Date(future.setDate(today.getDate() + beforeDay)) : fourHoursLater;
    set_date = moment(set_date).format(dateFormatBooking);
    set_date = moment(set_date, dateFormatBooking).toDate();

    $('#datetimepicker_v1, .booking_date').datetimepicker({
        format: dateFormatBooking,
        minDate: set_date,
    });
    /* End set date */



    /*Anchor link*/
    $('[href^="#"]').on("click", function (event) {
        let _h = $(this).attr('href');
        let _hsplit = _h.substr(1, _h.length);
        if ( _hsplit != 'open_booking' ) {
            event.preventDefault();
            scrollJumpto(_h, window.matchMedia('(min-width: 993px)').matches ? '.fixed-freeze' : '.fixed-freeze-mobile');
        }
    });
})(jQuery);

$(document).ready(function () {
    "use strict";

    /*creat menu sidebar*/
    $(".menu-bar-lv-1").each(function () {
        $(this).find(".span-lv-1").click(function () {
            $(this).toggleClass('rotate-menu');
            $(this).parent().find(".menu-bar-lv-2").toggle(500);
        });
    });
    $(".menu-bar-lv-2").each(function () {
        $(this).find(".span-lv-2").click(function () {
            $(this).toggleClass('rotate-menu');
            $(this).parent().find(".menu-bar-lv-3").toggle(500);
        });
    });

    /*creat menu sidebar*/
    $('.menu-bar-mobile ul>li>span').click(function () {
        $(this).toggleClass('rotate-menu');
        $(this).parent().find("ul").first().toggle(500);
    });
    $('.menu-bar-mobile ul>ul>li>span').click(function () {
        $(this).toggleClass('rotate-menu');
        $(this).parent().find("ul").first().toggle(500);
    });

    $(".menu-btn-show").click(function () {
        $('.menu-bar-mobile').toggleClass("menu-bar-mobile-show");
        $(".shadow-mobile").toggle();
    });
    $(".shadow-mobile").click(function () {
        $('.menu-bar-mobile').removeClass("menu-bar-mobile-show");
        $(this).fadeOut();
    });
    /*end*/

    /*check form*/
    $.ajax({
        type: "post",
        url: "/security/create",
        success: function (token) {
            $("form").each(function () {
                $(this).prepend("<input type='hidden' name='token' value='" + token + "' />");
            });
        }
    });

});

new WOW().init();
$(document).ready(function () {
    $(".video-play, .bt-menu-trigger, .overlay-btn").click(function () {
        $(".overlay").addClass("show-overlay");
        var getSrc = $(".overlay").attr('src');
        $(".overlay").find(".show-iframe").html('<iframe src="" frameborder="0" allowfullscreen></iframe>');
        $(".show-iframe iframe").attr("src", getSrc);
    });
    $(".bt-menu-trigger, .overlay-btn").click(function () {
        $(".overlay").removeClass("show-overlay");
        $(".show-iframe iframe").attr("src", "");
    });
    $("#owl-our-services").owlCarousel({
        items: 3,
        slideSpeed: 300,
        pagination: false,
        navigation: true,
        navigationText: ["<i class='fa fa-angle-left'></i>", "<i class='fa fa-angle-right'></i>"],
        lazyLoad: true,
        itemsDesktop: [990, 2],
        itemsDesktopSmall: [600, 1],
        itemsTablet: [560, 1]
    });
    $(".owl-testimonials, .owl-blog-news").owlCarousel({
        items: 3,
        slideSpeed: 300,
        pagination: false,
        navigation: true,
        navigationText: ["<i class='fa fa-angle-left'></i>", "<i class='fa fa-angle-right'></i>"],
        lazyLoad: true,
        itemsDesktop: [990, 2],
        itemsDesktopSmall: [600, 1],
        itemsTablet: [560, 1]
    });
    $(".owl-services-index").owlCarousel({
        items: 3,
        slideSpeed: 300,
        pagination: false,
        navigation: true,
        navigationText: ["<i class='fa fa-angle-left'></i>", "<i class='fa fa-angle-right'></i>"],
        lazyLoad: true,
        itemsDesktop: [990, 2],
        itemsDesktopSmall: [600, 1],
        itemsTablet: [560, 1]
    });
    $('.arrow-footer').click(function () {
        $('html, body').animate({scrollTop: 0}, 800);
        return false;
    });
    $(".check-height").height($(".check-height").width() - 60);
    $('.item-gallery').each(function () {
        $(this).hover(function () {
            $(this).toggleClass("active");
        });
    });
    $('.arrow-footer').click(function () {
        $('html, body').animate({scrollTop: 0}, 800);
        return false;
    });


});

jQuery(document).ready(function ($) {
    var jssor_1_options = {
        $AutoPlay: 1,
        $SlideWidth: 600,
        $Cols: 2,
        $Align: 100,
        $ArrowNavigatorOptions: {
            $Class: $JssorArrowNavigator$
        },
        $ThumbnailNavigatorOptions: {
            $Class: $JssorThumbnailNavigator$,
            $Cols: 0,
            $SpacingX: 0,
            $SpacingY: 0,
            $Align: 260
        }
    };
    var ab = $('#jssor_1').html();
    if (ab != undefined) {
        var jssor_1_slider = new $JssorSlider$("jssor_1", jssor_1_options);

        /*responsive code begin*/

        /*remove responsive code if you don't want the slider scales while window resizing*/
        function ScaleSlider() {
            var refSize = jssor_1_slider.$Elmt.parentNode.clientWidth;
            if (refSize) {
                refSize = Math.min(refSize, 3500);
                jssor_1_slider.$ScaleWidth(refSize);
            } else {
                window.setTimeout(ScaleSlider, 30);
            }
        }

        ScaleSlider();
        $(window).bind("load", ScaleSlider);
        $(window).bind("resize", ScaleSlider);
        $(window).bind("orientationchange", ScaleSlider);
    }
    /*responsive code end*/
});
$("#send_booking").validate({
    submit: {
        settings: {
            button: ".btn_booking",
            inputContainer: '.input-box',
            errorListClass: 'form-tooltip-error',

        }
    }
});

$("#send_contact").validate({
    submit: {
        settings: {
            button: ".btn_contact",
            inputContainer: '.form-group',
            errorListClass: 'form-tooltip-error',

        }
    }
});
$("#send_newsletter").validate({
    submit: {
        settings: {
            button: ".btn_send_newsletter",
            inputContainer: '.form-group',
            errorListClass: 'form-tooltip-error',
        },
        callback: {
            onSubmit: function (node, formdata) {
                var url_send = $(node).attr("action");
                var email = $("input[name='newsletter_email']").val();
                /*console.log(url_send);*/
                $.ajax({
                    type: "post",
                    url: url_send,
                    data: {newsletter_email: email},
                    success: function (html) {
                        var obj = JSON.parse(html);
                        call_notify("Notification", obj.message, obj.status);
                        $("input[name='newsletter_email']").val("");

                        /*An form*/
                        if (obj.status == "success") {
                            $("#send_newsletter .input-newsletter").html('<h2 class="newsletter_tile" style="text-align:center">Thanks for subscribing!</h2>');
                        }
                    }
                });
            }
            /*End on before submit*/
        }
    }
});
load_gallery();

function load_gallery() {
    $('.magnific').magnificPopup({
        type: 'image',
        gallery: {
            enabled: true
        },
    });
}

function call_notify(title_msg, msg, type_notify) {
    type_notify = type_notify ? type_notify : "error";

    var icon = "";
    if (type_notify == "error") {
        icon = "fa fa-exclamation-circle";
    } else if (type_notify == "success") {
        icon = "fa fa-check-circle";
    }
    new PNotify({
        title: title_msg,
        text: msg,
        type: type_notify,
        icon: icon,
        addclass: 'alert-with-icon'
    });
}


$(document).ready(function () {
    var lid = $('input[name="lid"]').val();

    lid = $('.list-cate-services li[lid="' + lid + '"] a');
    if (lid.length == 0) {
        lid = $(".list-cate-services li:first-child a");
    }
    lid.trigger("click");

    /*$(".list-cate-services ul li:first-child a").trigger("click");*/


    $.fn.is_on_screen = function () {
        var win = $(window);
        var viewport = {
            top: win.scrollTop(),
            left: win.scrollLeft()
        };
        viewport.right = viewport.left + win.width();
        viewport.bottom = viewport.top + win.height();
        var bounds = this.offset();
        if (typeof bounds == 'undefined') {
            return false;
        }
        bounds.right = bounds.left + this.outerWidth();
        bounds.bottom = bounds.top + this.outerHeight();

        if (viewport.top >= bounds.top) {
            var t = true;
            $('.btn_service_book').addClass('scroll_btn');
            $('.btn_service_book').css('right', viewport.right - bounds.right);
        } else {
            var t = false;
        }
        return t;
    };
    $(window).scroll(function () {
        if ($('.btn_service_defale').is_on_screen()) {

        } else {
            $('.btn_service_book').removeClass('scroll_btn');
            $('.btn_service_book').removeAttr('style');
        }
    });

    $("#filter li span.filter-by").click(function (e) {
        var id = $(this).attr("itemprop");
        e.preventDefault();

        /*set active class*/
        $('#filter li').removeClass('active');
        $(this).parent("li").addClass("active");

        getGalleryByCat(id);
    });
    $("#filter li:first span.filter-by").trigger("click");

    $("select[name='filter_select']").change(function () {
        var id = $(this).val();
        getGalleryByCat(id);
    });

    /*$("select[name='filter_select']").trigger("change");*/


    if ($('.slider-for').length) {
        $('.slider-for').slick({
            slidesToShow: 1,
            slidesToScroll: 1,
            arrows: false,
            asNavFor: '.slider-nav'
        });
    }
    if ($('.slider-nav').length) {
        $('.slider-nav').slick({
            slidesToShow: 1,
            slidesToScroll: 1,
            centerPadding: '20px',
            asNavFor: '.slider-for',
            centerMode: true,
            focusOnSelect: true,
            infinite: true,
            arrows: true,
            prevArrow: "<spam class='slick-prev'><i class='fa fa-angle-left fa-3x' aria-hidden='true'></i></spam>",
            nextArrow: "<spam class='slick-next'><i class='fa fa-angle-right fa-3x' aria-hidden='true'></i></spam>"
        });
    }
});

function getGalleryByCat(cat_id = 0, page = 0) {
    /*console.log(cat_id);*/
    $.ajax({
        type: "post",
        url: "/gallery/getlistbycat",
        beforeSend: function () {

        },
        data: {cat_id: cat_id, page: page},
        success: function (html) {
            /*console.log(html);*/
            var obj = JSON.parse(html);
            /*console.log(obj);*/
            var html_gallery = "";
            if (obj.data.length > 0) {
                for (var x in obj.data) {
                    html_gallery += `
                        <div class="col-lg-4 col-md-4 col-sm-6 col-xs-6">
                            <a class="gallery-item magnific" title="${obj.data[x].name}" href="${obj.data[x].image}">
                                <span style="background-image:url('${obj.data[x].pathThumb}')"></span>
                            </a>
                        </div>

                        `;
                }
            } else {
                html_gallery = "Not found gallery item in this category.";
            }

            $(".box_list_gallery").html(html_gallery);
            $(".box_paging").html(obj.paging_ajax);
            load_gallery();

        }
    });
}

function loadEvent() {
    $('#surveyForm')

    /*Add button click handler*/
        .on('click', '.addButton', function () {
            var html_close = `<div class="removeButton"><img src="/public/library/global/remove-service-icon-new.png" alt="remove-service-icon-new.png"></div>`;
            var template = `<div class="item-booking">` + html_close + $('#optionTemplate').html() + `</div>`;
            $(this).before($(template));
            $("#surveyForm .item-booking:last .list_service").trigger('change');
            saveForm();
        })

        /*Remove button click handler*/
        .on('click', '.removeButton', function () {
            var $row = $(this).parents('.item-booking'),
                $option = $row.find('[name="option[]"]');

            /*Remove element containing the option*/
            $row.remove();
            saveForm();
        })
}

function loadForm(formdata) {
    var obj = JSON.parse(formdata);
    $("input[name='booking_date']").val(obj.booking_date);
    $("input[name='booking_hours']").val(obj.booking_hours);
    var listservice = typeof (obj.service_staff) != "undefined" ? obj.service_staff : [];
    /*console.log(listservice);*/
    if (listservice.length > 0) {
        for (var x in listservice) {
            /*split info*/
            var list = listservice[x].split(',');
            /*Trigger add row*/
            if (x > 0) {
                $(".addButton").trigger("click");
            }
            var objservice = $(".list_service:last");
            $(".list_service:last option[value='" + list[0] + "']").attr("selected", "selected");
            objservice.trigger("change");
            $(".list_staff:last option[value='" + list[1] + "']").attr("selected", "selected");
        }

        /*Trigger action*/
        /*$(".btn_action").trigger("click");*/
    }
}

function pushHtmlTime(input_date, type) {
    $.ajax({
        type: "post",
        url: "/book/get_hours",
        data: {input_date: input_date, type: type},
        beforeSend: function () {
            $(".box_detail_info").append("<div class='mask_booking'><i class='fa fa-spinner fa-pulse fa-3x fa-fw'></i></div>");
            $(".box_detail_info").css("position", "relative");
            $(".mask_booking").css("position", "absolute").css("height", "100%").css("width", "100%").css("top", 0).css("left", 0).css("background", "rgba(0,0,0,0.5)").css("text-align", "right");
            $(".mask_booking i").css("font-size", "2em").css("margin", "10px");
        },
        success: function (response) {
            /*console.log(response);*/
            /*Remove mask*/
            $(".mask_booking").remove();
            var obj = JSON.parse(response);
            if (obj.checkmorning == false) {
                $(".note_am_time").html("(Booking time has expired)");
            } else {
                $(".note_am_time").html("");
            }

            if (obj.checkafternoon == false) {
                $(".note_pm_time").html("(Booking time has expired)");
            } else {
                $(".note_pm_time").html("");
            }

            $(".databooktime .timemorning").html(obj.htmlMorning);
            $(".databooktime .timeafternoon").html(obj.htmlAfternoon);
        }
    });
}

function setHtmldate(date_choose) {
    /*use for booking*/
    var new_date = convertDate(date_choose);
    var d = new Date(new_date);

    var months = ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];
    var days = ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"];
    var str_show = days[d.getDay()] + ", " + months[d.getMonth()] + "-" + d.getDate() + "-" + d.getFullYear();
    /*console.log(str_show);*/
    $(".time_show").html(str_show);
}

function convertDate(input) {
    var list_date = input.split("/");
    var splitDate = posFormat.split(",");
    var new_date = list_date[splitDate[2]] + "/" + list_date[splitDate[1]] + "/" + list_date[splitDate[0]];
    return new_date;
}

function saveForm() {
    /*Save form*/
    var formdata = $("#surveyForm").serialize();
    $.ajax({
        type: "post",
        url: "/book/saveform",
        data: formdata,
        success: function (html) {
            /*console.log(html);*/
        }
    });
}

/*BOOKING PAGE*/
$("#surveyForm").on("change", ".list_service", function () {
    var service_id = $(this).val();
    var list_staff = $(this).find("option:selected").attr("staff");
    if (service_id) {
        $(this).css("border-color", "#ccc");
        $(this).parent().find('.form-tooltip-error').remove();
    } else {
        $(this).css("border-color", "red");
        $(this).parent().append('<div class="form-tooltip-error" data-error-list=""><ul><li>' + $(this).data('validation-message') + '</li></ul></div>');
    }
    var obj = JSON.parse(list_staff);
    var option = '<option value="">Service Provider</option>';
    for (var x in obj) {
        option += `<option value="` + obj[x].id + `" urlimg="` + obj[x].image + `">` + obj[x].name + `</option>`;
    }

    $(this).parents(".item-booking").find(".list_staff").html(option);

    /*Save form*/
    saveForm();

});
/*END BOOKING PAGE*/

/*BTN SEARCH BOOKING*/
$(".btn_action").click(function () {

    var num = $(".list_service").length;
    var info_staff = [];
    var info_staff2 = [];
    var temp = {};
    var i = 0;
    var check = true;
    $(".list_service").each(function () {
        var checkval = $(this).val();
        if (checkval) {
            $(this).css("border-color", "#ccc");
            $(this).parent().find('.form-tooltip-error').remove();
        } else {
            check = false;
            $(this).css("border-color", "red");
            $(this).parent().append('<div class="form-tooltip-error" data-error-list=""><ul><li>' + $(this).data('validation-message') + '</li></ul></div>');
        }
        temp.price = $('option:selected', this).attr('price');
        temp.service = $('option:selected', this).text();
        info_staff.push(temp);
        temp = {};
        i++;
    });

    var j = 0;
    $(".list_staff").each(function () {
        var checkval = $(this).val();
        temp.image = $('option:selected', this).attr('urlimg');
        temp.name = checkval ? $('option:selected', this).text() : "Any person";
        info_staff2.push(temp);
        temp = {};
        j++;
    });
    if (check == true) {
        $(".box_detail_info").show();
        // $("#box_person").html("Loading ...");
        // var htm = $('#staff_detail').clone().removeAttr('id').removeAttr('style');

        // var html_person = "";
        // var j = 0;
        // var htm_bk = '';
        // for (var x in info_staff) {
        //     var image = typeof (info_staff2[x].image) === "undefined" ? "/public/library/global/no-photo.jpg" : info_staff2[x].image;
        //     htm.find('.img-info-staff a img').attr('src', image).attr('alt', info_staff2[x].name);
        //     htm.find('.title-staff a').text(info_staff2[x].name);
        //     htm.find('.title-staff .s-name').text(info_staff[x].service);
        //     htm.find('.title-staff .s-price').html('Price: ' + info_staff[x].price);
        //     htm_bk += htm.html();
        // }
        //
        // $("#box_person").html(htm_bk);

        $("#box_person").html("Loading ...");
        var html_person = "";
        var j = 0;
        for(var x in info_staff)
        {
            var image = typeof(info_staff2[x].image) === "undefined" ? "/public/library/global/no-photo.jpg" : info_staff2[x].image;
            html_person += ''
                +'<div class="info-staff">'
                +'<div class="img-info-staff">'
                +'<span>'
                +'<img src="'+image+'" alt="'+info_staff2[x].name+'">'
                +'</span>'
                +'</div>'
                +'<div class="title-staff">'
                +'<h2><span>'+info_staff2[x].name+'</span></h2>'
                +'<span class="s-name">'+info_staff[x].service+'</span>'
                +'<span class="s-price">Price: '+info_staff[x].price+'</span>'
                +'</div>'
                +'</div>';
        }

        $("#box_person").html(html_person);


        var typehtml = $('#surveyForm .choose_date').attr("typehtml");
        var date_choose = $('#surveyForm .choose_date').val();
        pushHtmlTime(date_choose, typehtml);

        var scroll = $("#book-info").offset().top;
        $('body').animate({scrollTop: scroll}, 600, 'swing');
        /*.scrollTop( $("#book-info").offset().top );*/


    } else {
        return false;
    }


});
/*END BTN SEARCH BOOKING*/

/*CONFIRM BOOKING*/
$(document).ready(function () {
    $(".databooktime").on("click", ".open_booking", function () {
        /*Check service*/
        var check = true;
        $(".list_service").each(function () {
            var checkval = $(this).val();
            if (checkval) {
                $(this).css("border-color", "#ccc");
                $(this).parent().find('.form-tooltip-error').remove();
            } else {
                check = false;
                $(this).css("border-color", "red");
                $(this).parent().append('<div class="form-tooltip-error" data-error-list=""><ul><li>' + $(this).data('validation-message') + '</li></ul></div>');
            }
        });

        if (check == false) {
            return false;
        }

        var hours = $(this).attr("valhours");
        $.magnificPopup.open({
            type: 'inline',
            midClick: true,
            items: {
                src: '#open_booking'
            },
            callbacks: {
                beforeOpen: function () {
                    if ($(window).width() < 700) {
                        this.st.focus = false;
                    } else {
                        this.st.focus = '#name';
                    }
                    $("input[name='booking_hours']").val(hours);


                }
            }
        });

        return false;
    });

    $(".btn_cancel").click(function () {
        $.magnificPopup.close();

    });

    /*Mask Input*/
    var plholder = phoneFormat == "(000) 000-0000" ? "Phone (___) ___-____" : "Phone ____ ___ ____";
    $(".inputPhone").mask(phoneFormat, {placeholder: plholder});
    /*End mask input*/
});
/*END CONFIRM BOOKING*/


/*CHOOSE DATE*/
$("#surveyForm").on("dp.change", ".choose_date", function () {

    var typehtml = $(this).attr("typehtml");
    var date_choose = $(this).val();
    /*set Html date*/
    setHtmldate(date_choose);
    /*Save form*/
    saveForm();
    /*data time*/

    /*changeTimeByDate(date_choose, typehtml);*/
    pushHtmlTime(date_choose, typehtml);
});
/*$(".choose_date").trigger("dp.change");*/

/*CHOOSE DATE*/
$("#send_booking").on("dp.change", ".choose_date", function () {

    var typehtml = $(this).attr("typehtml");
    var date_choose = $(this).val();
    /*data time*/
    /*changeTimeByDate(date_choose, typehtml);*/
    pushHtmlTime(date_choose, typehtml);
});
/*END CHOOSE DATE*/

/*Booking provider*/
$("#surveyForm").on("change", ".list_staff", function () {
    /*Save form*/
    saveForm();
});

function update_cart(onthis) {
    var quantity = $(onthis).val();
    var id = $(onthis).attr("cart_id");
    /*Ajax*/
    $.ajax({
        type: "post",
        url: "/cart/update",
        data: {quantity: quantity, id: id},
        success: function (html) {
            /*console.log(html);*/
            var obj = JSON.parse(html);
            /*set value*/
            if (obj.total_show && obj.amount) {
                $(onthis).parents("tr").find(".total_change").html(obj.total_show);
                $(".amount_change").html(obj.amount);
            }

            if (obj.cart_data) {
                $("#cart_tax").text(obj.cart_data[1]);
                $("#cart_discount_code_value").text(obj.cart_data[5]);
                $("#cart_subtotal").text(obj.cart_data[2]);
                $("#cart_payment_total").text(obj.cart_data[3]);
            }

        }
    });
}

function update_price(onthis) {
    var cus_price = isNaN(parseFloat($(onthis).val())) ? 0 : parseFloat($(onthis).val());
    var id = $(onthis).attr("cart_id");
    var max_val = parseFloat($(onthis).attr("max"));
    var min_val = parseFloat($(onthis).attr("min"));

    if (cus_price >= min_val && cus_price <= max_val) {
        $(onthis).css("border-color", "#ccc");
        $(".btn_cart_order").attr("href", "/payment");
        /*Ajax*/
        $.ajax({
            type: "post",
            url: "/cart/updateprice",
            data: {cus_price: cus_price, id: id},
            success: function (html) {
                /*console.log(html);*/
                var obj = JSON.parse(html);
                if (obj.status == "error") {
                    call_notify('Notification', obj.msg, "error");
                    $(onthis).val(obj.price);
                    return false;
                }
                /*set value*/
                if (obj.total_show && obj.amount) {
                    $(onthis).parents("tr").find(".total_change").html(obj.total_show);
                    $(".amount_change").html(obj.amount);
                }

                if (obj.cart_data) {
                    $("#cart_tax").text(obj.cart_data[1]);
                    $("#cart_discount_code_value").text(obj.cart_data[5]);
                    $("#cart_subtotal").text(obj.cart_data[2]);
                    $("#cart_payment_total").text(obj.cart_data[3]);
                }

            }
        });
    } else {
        $(onthis).css("border-color", "red");
        $(".btn_cart_order").removeAttr("href");
    }
}

function delItem(onthis) {
    var id = $(onthis).attr("cart_id");
    /*Ajax*/
    $.ajax({
        type: "post",
        url: "/cart/delitem",
        data: {id: id},
        success: function (html) {
            /*console.log(html);*/
            var obj = JSON.parse(html);
            /*set value*/
            if (obj.amount) {
                /*remove row*/
                $(onthis).parents("tr").remove();
                /*change stt*/
                if ($(".list_stt").length > 0) {
                    var i = 1;
                    $(".list_stt").each(function () {
                        $(this).html("#" + i);
                        i++;
                    });
                } else {
                    $("tbody.step1").html('<tr><td colspan="7" style="text-align: center"><b>Cart empty</b></td></tr>');
                }
                /*set amount*/
                $(".amount_change").html(obj.amount);

                if (obj.cart_data) {
                    $("#cart_tax").text(obj.cart_data[1]);
                    $("#cart_discount_code_value").text(obj.cart_data[5]);
                    $("#cart_subtotal").text(obj.cart_data[2]);
                    $("#cart_payment_total").text(obj.cart_data[3]);
                }
            }

        }
    });
}

function loadService(pg_id = 0, _page = 0) {
    var btn_appointment = "";
    if (typeof (enable_booking) != "undefined" && enable_booking == 1) {
        btn_appointment = "<a class='hs-btn btn_2 btn-light mb-15 btn_make_appointment' href='/book' title='Make an appointment'>Make an appointment</a>";
    }
    $(".list-cate-services ul li.ui-state-default.ui-corner-left").removeClass("act-defale");
    $(".list-cate-services ul li[lid='" + pg_id + "']").addClass("act-defale");
    var html_row = '';
    $.ajax({
        type: "post",
        url: "/service/loadservice",
        data: {pg_id: pg_id, limit: num_paging, page: _page, paging: 1},
        beforeSend: function () {
            $(".content_service").html('<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">Loading...</div>');
        },
        success: function (html) {
            var obj = JSON.parse(html);
            $(".paging_service").html(obj.paging_ajax);
            var group_des = obj.group_des;
            obj = obj.data;

            if (obj.length > 0) {
                var html_row = `
                    <ul id="all-item">
                        <li class="services_item_v1 item-botton clearfix text-right ui-corner-left">
                            ` + btn_appointment + `
                            <a class="hs-btn btn_2 btn-light mb-15" style="margin-left:15px;" href="tel:` + company_phone + `" title="` + company_phone + `"><span class="fa"><i class="fa fa-phone"></i></span><span class="title">Call now</span></a>
                        </li>
                    `;

                if (group_des) {
                    html_row += `<li class="des_service" style="border-top: none; padding: 10px 0;">
                                    ` + group_des + `
                                    </li>`;
                }

                for (x in obj) {
                    var price_show = obj[x].price_sell ? obj[x].price_sell : "";
                    html_row += `
                        <li class="services_item_v1">
                            <div class="line_item_v1">
                                <div class="just_start_line">
                                    <a href="/" data-toggle="tooltip" data-placement="top" title="` + obj[x].description + `">
                                        <span>` + obj[x].name + `</span>
                                        <span class="price_service_v1 pull-right">` + price_show + obj[x].product_up + `</span>
                                    </a>
                                    <div class="box_des">
                                        ` + obj[x].product_description + `
                                    </div>
                                </div>
                            </div>
                        </li>`;
                }

                html_row += `
                    </ul>
                    `;

                $(".content_service").html(html_row);

                $("#tabs li").removeClass("ui-corner-top").addClass("ui-corner-left");
                $('body, html').animate({
                    scrollTop: $(".box_service").offset().top
                }, 1000);

            } else {
                $(".content_service").html("No services found in this category");
            }
        }
    });
}

function applyDiscountCode() {
    $("#loader_discount_code").show();
    $("#enter_discount_code").hide();
    $("#cart_discount_code").prop("disabled", true);

    let code = $("#cart_discount_code").val();
    $.ajax({
        url: "/payment/discount_code/",
        data: {"code": code},
        dataType: "json",
        success: function (res) {

            $("#loader_discount_code").hide();
            $("#enter_discount_code").show();
            $("#cart_discount_code").prop("disabled", false);

            if (res.status == 'ok') {
                $("#discount_code_input").hide();
                $("#discount_code_info").show();
                $("#cart_discount_code_text").text(res.code_data.code);
                $("#cart_discount_code_value").text(res.cart_data[5]);
                $("#cart_subtotal").text(res.cart_data[2]);
                $("#cart_payment_total").text(res.cart_data[3]);
                $("#cart_tax").text(res.cart_data[1]);
            } else {
                call_notify("Alert", res.msg, "error");
            }
        }
    })
}

function removeDiscountCode() {
    $.ajax({
        url: "/payment/remove_code/",
        dataType: "json",
        success: function (res) {
            if (res.status == 'ok') {
                $("#discount_code_input").show();
                $("#discount_code_info").hide();
                $("#cart_discount_code_text").text("");
                $("#cart_discount_code_value").text(res.cart_data[5]);
                $("#cart_subtotal").text(res.cart_data[2]);
                $("#cart_payment_total").text(res.cart_data[3]);
                $("#cart_tax").text(res.cart_data[1]);
            } else {
                call_notify("Alert", res.msg, "error");
            }
        }
    })
}

function changeTimeByDate(input_date, typehtml) {
    /*check date time*/
    var splitDate = posFormat.split(","); /*1,0,2*/
    /*change time*/
    $.ajax({
        type: "post",
        url: "/book/change_time",
        data: {date: input_date},
        success: function (response) {
            /*console.log(response);*/
            if (response) {
                var obj = JSON.parse(response);
                timeMorning = JSON.stringify(obj.time_morning);
                /*convert time afternoon*/
                var afternoon_time = obj.time_afternoon;
                for (var x in afternoon_time) {
                    var listTime = afternoon_time[x].split(":");

                    if (listTime[0] >= 1 && listTime[0] < 12) {
                        var changeTime = parseInt(listTime[0]) + 12;
                        afternoon_time[x] = changeTime + ":" + listTime[1];
                    }
                }

                timeAfternoon = JSON.stringify(afternoon_time);
                pushHtmlTime(input_date, typehtml);
            }
        }
    });

}

function scrollJumpto ( jumpto, headerfixed, redirect )
{
    // check exits element for jumpto
    if ( $(jumpto).length > 0 )
    {
        // Calculator position and call jumpto with effect
        jumpto = $(jumpto).offset().top;
        headerfixed = ( $(headerfixed).length > 0 ) ? $(headerfixed).height() + 10 : 10;

        $('html, body').animate({
            scrollTop: parseInt(jumpto - headerfixed) + 'px'
        }, 1000, 'swing');
    }
    // Check redirect if not exits element for jumpto
    else if ( redirect )
    {
        // Call redirect
        redirectUrl(redirect);
        return;
    }
    else
    {
        console.log(jumpto + ' Not found.');
    }
}

function initSliderHome( slider, sliderFixedHeight, sliderItem , selector ) {
    if ( $(sliderItem).length <= 0 ) {
        return;
    }

    /*calculator width height slider*/
    $(selector).show();

    /*width-height*/
    let width = '100%';
    let height = $(selector).find('.fixed').height();
    if( height <= 0 ) {
        let heights = [];
        $(selector).each(function() {
            heights.push($(this).find('img').height());
        });

        if ( heights.length > 0 ) {
            height = Math.min.apply( Math, heights );
        }
    };
    height = (height > 0) ? height : 450;

    $(selector).hide();

    /*init slider and remove other slider*/
    let sliderOption = {};
    sliderOption.autoHeight = $(selector).find('.fixed').height() ? false : true;

    let sliderOptionObj = $(selector).find('#slider-option');
    sliderOption.autoplay = sliderOptionObj.attr('data-autoplay');
    sliderOption.autoplay = sliderOption.autoplay == 'false' ? false : true; // true/ false

    sliderOption.autoplayDelay = sliderOptionObj.attr('data-autoplayDelay')*1;
    sliderOption.autoplayDelay = sliderOption.autoplayDelay >= 1000 ? sliderOption.autoplayDelay : 5000; // milliseconds

    let sliderInit = sliderOption.autoHeight ? slider : sliderFixedHeight;
    $( sliderInit ).show().sliderPro({
        width: width,
        height: height,
        autoHeight: sliderOption.autoHeight,
        responsive: sliderOption.autoHeight,
        centerImage: false,
        autoScaleLayers: false,

        arrows: true,
        fade: true,
        buttons: true,
        thumbnailArrows: true,

        autoplay: true,
        slideSpeed : 300,
    });
    if( sliderInit == slider ) {
        $(sliderFixedHeight).remove();
    } else {
        $(slider).remove();
    }
}

$(document).ready(function () {
    // Animation scroll to service id
    $(window).load(function() {
        if ( $('.animation_sroll_jumpto .sroll_jumpto').length > 0 ) {
            if (window.matchMedia('(min-width: 993px)').matches){
                scrollJumpto('#sci_' + $('input[name="group_id"]').val(), '.fixed-freeze');
            } else {
                scrollJumpto('#sci_' + $('input[name="group_id"]').val(), '.fixed-freeze-mobile');
            }
        }
    });
});

$(document).ready(function (){
    /*SLIDER*/
    initSliderHome('#my-slider', '#my-slider-fixed-height','.sp-slides .sp-slide', '.slider-width-height');

    /*
    * Freeze
    * */
    $.fn.is_on_scroll1 = function() {
        /* Not included margin, padding of window */
        var win = $(window);
        var viewport = {
            top : win.scrollTop(),
            left : win.scrollLeft()
        };
        viewport.right = viewport.left + win.width();
        viewport.bottom = viewport.top + win.height();

        /* Not included margin of this element: same container */
        var bounds = this.offset();
        if ( typeof bounds == 'undefined' ) {return false;}
        bounds.right = bounds.left + this.outerWidth();
        bounds.bottom = bounds.top + this.outerHeight();

        if ( bounds.top >= viewport.top && bounds.bottom <= viewport.bottom ) {
            return true;
        } else {
            return false;
        }
    };

    /* Check scroll */
    var wrapFreezeHeaderObj = $('.wrap-freeze-header');
    var flagFreezeHeaderObj = $('.flag-freeze-header');
    var btnServiceBook = $('.btn_service_book');
    if( wrapFreezeHeaderObj.find('[name="activeFreezeHeader"]').val() ){
        flagFreezeHeaderObj.addClass('fixed-freeze');
        var insteadFreezeHeaderObj = $('<div class="instead-flag-freeze-header"></div>');
        insteadFreezeHeaderObj.insertBefore(flagFreezeHeaderObj);
        $(window).scroll(function(){
            if( wrapFreezeHeaderObj.is_on_scroll1() ){
                flagFreezeHeaderObj.removeClass('freeze-header freeze-header-desktop with-bg');
                insteadFreezeHeaderObj.height('0px');
                btnServiceBook.removeClass('freeze-btn');
            } else {
                insteadFreezeHeaderObj.height(flagFreezeHeaderObj.outerHeight()+'px');
                flagFreezeHeaderObj.addClass('freeze-header freeze-header-desktop with-bg');
                btnServiceBook.addClass('freeze-btn');
            }
        });
    }

    /* Check scroll */
    var wrapFreezeHeaderObj_mobile = $('.wrap-freeze-header-mobile');
    var flagFreezeHeaderObj_mobile = $('.flag-freeze-header-mobile');
    var btnServiceBook_mobile = $('.btn_service_book');
    if( wrapFreezeHeaderObj_mobile.find('[name="activeFreezeHeader"]').val() ){
        flagFreezeHeaderObj_mobile.addClass('fixed-freeze-mobile');
        var insteadFreezeHeaderObj_mobile = $('<div class="instead-flag-freeze-header-mobile"></div>');
        insteadFreezeHeaderObj_mobile.insertBefore(flagFreezeHeaderObj_mobile);
        $(window).scroll(function(){
            if( wrapFreezeHeaderObj_mobile.is_on_scroll1() ){
                flagFreezeHeaderObj_mobile.removeClass('freeze-header freeze-header-mobile with-bg');
                insteadFreezeHeaderObj_mobile.height('0px');
                btnServiceBook_mobile.removeClass('freeze-btn-mobile');
            } else {
                insteadFreezeHeaderObj_mobile.height(flagFreezeHeaderObj_mobile.outerHeight()+'px');
                flagFreezeHeaderObj_mobile.addClass('freeze-header freeze-header-mobile with-bg');
                btnServiceBook_mobile.addClass('freeze-btn-mobile');
            }
        });
    }
});

function load_social(inputs) {
    if ( !inputs ) {
        console.log('load social missed inputs');
        return false;
    }

    /*calculator width*/
    let social_block_width = $('#social_block_width').width();
    social_block_width = Math.round(social_block_width);

    if (social_block_width > 450) {
        social_block_width = 450;
    }

    if ( social_block_width < 180 ){
        social_block_width = 180;
    }

    /*facebook fanpage*/
    if ( typeof inputs.facebook_embed != 'undefined' && inputs.facebook_embed ) {
        let social_block_height = Math.round(social_block_width * (parseInt(inputs.facebook_embed.height)/parseInt(inputs.facebook_embed.width)));
        let  social_url = '';
        if (!inputs.facebook_embed.likebox_enable) {
            social_url += 'https://www.facebook.com/plugins/page.php?';
            social_url += '&width=' + social_block_width + '&height=' + social_block_height;
            social_url += '&small_header='+(inputs.facebook_embed.small_header ? 'true' : 'false');
            social_url += '&tabs='+inputs.facebook_embed.tabs;
            social_url += '&show_facepile='+(inputs.facebook_embed.show_facepile ? 'true' : 'false');
            social_url += '&hide_cover=false&hide_cta=false&adapt_container_width=true';
        } else {
            social_url += 'https://www.facebook.com/plugins/likebox.php?';
            social_url += '&width=' + social_block_width + '&height=' + social_block_width; // If set height then error with likebox
            social_url += '&show_faces='+(inputs.facebook_embed.likebox_show_faces ? 'true' : 'false');
            social_url += '&stream='+(inputs.facebook_embed.likebox_stream ? 'true' : 'false');
            social_url += '&header=false';
        }
        social_url += '&href=' + encodeURIComponent(inputs.facebook_embed.id_fanpage);
        social_url += '&appId' + inputs.facebook_embed.appId;

        $('#fanpage_fb_container').html('<iframe style="overflow:hidden;max-height:' + social_block_height + 'px" title="Social fanpage" src="'+social_url+'" width="' + social_block_width + '" height="' + social_block_height + '" style="border:none;overflow:hidden" scrolling="no" frameborder="0" allowTransparency="true"></iframe>');
    }

    /*google fanpage*/
    if (typeof inputs.google_id_fanpage != 'undefined' && inputs.google_id_fanpage) {
        $('#fanpage_google_container').html('<div class="g-page" data-href="' + inputs.google_id_fanpage + '" data-width="' + social_block_width + '"></div><script src="https://apis.google.com/js/platform.js" async defer><\/script>');
    }

    /*twitter fanpage*/
    $('#fanpage_twitter_container').html(''); // clear content
    if (typeof inputs.twitter_id_fanpage != 'undefined' && inputs.twitter_id_fanpage) {
        inputs.twitter_id_fanpage = inputs.twitter_id_fanpage.split('/');
        for (let i = inputs.twitter_id_fanpage.length - 1; i >= 0; i -= 1) {
            if (inputs.twitter_id_fanpage[i] != '') {
                inputs.twitter_id_fanpage = inputs.twitter_id_fanpage[i];
                break;
            }
        }
        if (typeof twttr != 'undefined') {
            twttr.widgets.createTweet(inputs.twitter_id_fanpage, document.getElementById('fanpage_twitter_container'), {width: social_block_width});
        }
    }
}

$(document).ready(function () {
    /*
    * SOCIAL FAN PAGE
    * When resize then reload fanpage
    * Firing resize event only when resizing is finished
    */
    let socialInputs = {
        facebook_embed: facebook_embed,
        google_id_fanpage: google_id_fanpage,
        twitter_id_fanpage: twitter_id_fanpage,
    };
    $(window).load(function() {
        load_social(socialInputs);
        $(window).on('resize', function () {
            clearTimeout(window.resizedFinished);
            window.resizedFinished = setTimeout(function () {
                load_social(socialInputs);
            }, 250);
        });
    });
});