$(document).ready(function() {
    $('.dh-container1').directionalHover();
    $('#carousel-testimonial').owlCarousel({
        singleItem : true,
        navigation : true,
        navigationText : ['<i class="fa fa-angle-left"></i>', '<i class="fa fa-angle-right"></i>'],
        pagination : false,
        slideSpeed : 1500,
        paginationSpeed : 800,
        rewindSpeed : 1500,
        autoPlay : false,
        stopOnHover : true,
    });
    $('#owl_service_board').owlCarousel({
        items : 4,
        navigation : true,
        navigationText : ['<i class="fa fa-angle-left"></i>', '<i class="fa fa-angle-right"></i>'],
        pagination : true,
        itemsDesktop : [1199,3],
        itemsDesktopSmall : [979,3],
        autoPlay : 3000,
        stopOnHover : true,
    });
    $('#services-slider').owlCarousel({
        items : 4,
        navigation : true,
        navigationText : ['<i class="fa fa-angle-left"></i>', '<i class="fa fa-angle-right"></i>'],
        pagination : true,
        itemsDesktop : [1199,3],
        itemsDesktopSmall : [979,3],
        autoPlay : 3000,
        stopOnHover : true,
    });
    $('.gridder').gridderExpander({
        scroll: true,
        scrollOffset: 150,
        scrollTo: "panel",                          // panel or listitem
        animationSpeed: 200,
        animationEasing: "easeInOutExpo",
        showNav: true,                              // Show Navigation
        nextText: "",                           // Next button text
        prevText: "",                       // Previous button text
        closeText: "X",   // Close button text
        onStart: function(){
            //Gridder Inititialized
        },
        onContent: function(){
            sliderImage();
        },
        onClosed: function(){
            //Gridder Closed
        }
    });
    $('#box-about .list-image li, .gridder-list .wrap-img').directionalHover({
        // CSS class for the overlay
        overlay: "overlay",
        // Linear or swing
        easing: "swing",
        speed: 400
    });
   
  function sliderImage() {
      var $ = jQuery;
      var config = {
          'slides' : '.slider',
          'source' : '.slider-a',
          'handle' : '.slider-handle'
      };

      // Add handlers for each .slider
      $(config.slides).each(function(){

      // Disable no-javascript fallback
      $(this).addClass('slider-active');

      var drag = {
          eventx : 0,
          eventy : 0,
          coords : 0,
          moving : false,
          slider : $(this),
          bounds : $(this).width(),
          offset : $(this).offset().left,
          source : $(this).find(config.source),
          handle : $(this).find(config.handle)
      };        

      // Grab "src" value of image, set as background-image
      drag.source.css('background-image', 'url(' + $(this).find('img').attr('src') + ')');
      drag.slider.on('mousemove touchmove', function(e) {
        if(drag.moving){
            // Prevent vertical movement on touch
            if(drag.eventy == (e.pageY || e.originalEvent.touches[0].pageY)){
                e.preventDefault(); 
            }

            // Retrieve current coordinates
            drag.eventx = e.pageX || e.originalEvent.touches[0].pageX;
            drag.eventy = e.pageY || e.originalEvent.touches[0].pageY;
        
            // Translate mouse/touch position into a percentage value
            drag.coords = (drag.eventx - drag.offset) / drag.bounds * 100;

            // Update width of slider image and position of handle
            drag.handle[0].style.left = drag.coords + '%';
            drag.source[0].style.maxWidth = drag.coords + '%';
        }

    }).on('mouseenter touchstart', function(e) {
        drag.moving = true;
        drag.bounds = $(this).width();
        drag.offset = $(this).offset().left;
    }).on('mouseleave touchend', function() {
        drag.moving = false;
    }).on('dragstart', function(e) {
        e.preventDefault(); 
    });
  });

};

// /*////////////// MY SLIDER ///////////////*/
// $('#my-slider').sliderPro({
//         width: 1976, 
//         height:800,               
//         arrows: true,
//         fade: true,
//         centerImage:true,
//         autoScaleLayers: false,
//         forceSize: 'fullWidth',
//         buttons: true,  
//         thumbnailArrows: true,
//         autoplay: true,
//         slideSpeed : 300,
//         breakpoints: { 
//           768: {
//             width: 1024, 
//         height:600,               
//         arrows: true,
//         fade: true,
//         autoHeight:false,
//         centerImage:false,
//         autoScaleLayers: false,
//         forceSize: 'fullWidth',
//         buttons: true,  
//         thumbnailArrows: true,
//         autoplay: true,
//         slideSpeed : 300,
//           }
//         },
//     })
// 	/*////////////// MOBILE NAV ///////////////*/
// 	$('.mobile-menu nav').meanmenu({
//         meanMenuContainer: '.menu_mobile_v1',
//         meanScreenWidth: "990",
//         meanRevealPosition: "right",
//         meanMenuOpen: "<span></span>"
//     });
    

//     /*////////////// GALLERY ///////////////*/
//     var groups = {};
//     $('.fan-gallery-item').each(function() {
//       var id = parseInt($(this).attr('data-group'), 10);      
//       if(!groups[id]) {
//         groups[id] = [];
//       }       
//       groups[id].push( this );
//     });


//     $.each(groups, function() {
      
//       $(this).magnificPopup({
//           type: 'image',
//           closeOnContentClick: true,
//           closeBtnInside: false,
//           gallery: { enabled:true }
//       })
      
//     });
//     /*////////////// BOOKING ///////////////*/
//     $(document).ready(function() {
//       $('#datetimepicker_v1').datetimepicker();
//     });
//     // CONFIRM BOOKING
//     $(document).ready(function(){
//         $(".databooktime").on("click",".open_booking", function(){
//             var hours = $(this).attr("valhours");
//             $.magnificPopup.open({
//                 type: 'inline',
//                 midClick: true,
//                 items: {
//                   src: '#open_booking'
//                 },
//                 callbacks: {
//                     beforeOpen: function() {
//                         if($(window).width() < 700) {
//                             this.st.focus = false;
//                         } else {
//                             this.st.focus = '#name';
//                         }
//                         $("input[name='booking_hours']").val(hours);

//                     }
//                 }
//             });

//             return false;
//         });

//         $(".btn_cancel").click(function(){
//             $.magnificPopup.close();
            
//         });
//     });
//     // END CONFIRM BOOKING
//     // FLEX LABEL IN PAYMENT 
});