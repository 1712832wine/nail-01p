// FUNCTION
function loadGallery(pg_id=0, resizeWidth=0) {
    if(pg_id)
    {
        $.ajax({
            type: "post",
            url: "/service/loadgallery",
            data: {id:pg_id, resize_width:resizeWidth},
            beforeSend: function()
            {
                // $(".box_show_gallery").html("Loading...");
            },
            success: function(html)
            {
                // console.log(html);
                var obj = JSON.parse(html);
                var html_img = '';
                for(var x in obj)
                {
                    html_img +=`<li>
                                    <img itemprop="image" alt="" src="`+obj[x].image+`" class="img-responsive">
                                </li>`;
                }

                $(".box_show_gallery").html(html_img);
            }
        });
    }
}

function loadService(pg_id=0, _page=0, resizeWidth=0) {
    // change active of nav
    $("ul.listcatser li").removeClass("active ui-state-active");
    $("ul.listcatser li[data-group-id='"+pg_id+"']").addClass("active ui-state-active");

    // Get data
    $.ajax({
        type: "post",
        url: "/service/loadservice",
        data: {pg_id: pg_id, limit: num_paging, page: _page, paging: 1, resize_width: resizeWidth},
        beforeSend: function() {
            $(".content_service").html("Loading...");
        },
        success: function(html)
        {
            var obj = JSON.parse(html);

            // paging
            $(".paging_service").html(obj.paging_ajax);

            var group_des = obj.group_des;
            obj = obj.data;
            if(obj.length > 0)
            {
                var html_row = `<ul class="services_item_ul_v1">`;

                if( group_des )
                {
                    html_row += `
                    <li class="des_service" style="border-top: none; padding: 10px 0;">
                        <div class="clearfix">` + group_des + `</div>
                    </li>
                    `;
                }

                var pull_right = "pull-right";
                if( /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) ) 
                {
                    pull_right = "";
                }
                for( x in obj )
                {
                    var price_show = obj[x].price_sell ? obj[x].price_sell : "";
                    html_row += `
                    <li>
                        <div class="line_item_v1">
                            <div class="just_start_line">
                                <a class="inline-block" href="`+obj[x].link_book+`" title="`+obj[x].name+`">
                                    <span class="name_service_v1">`+obj[x].name+`</span>
                                    <span class="price_service_v1 `+pull_right+`">`+price_show+obj[x].product_up+`</span>
                                </a>
                                <div class="box_des">`+obj[x].product_description+`</div>
                            </div>
                        </div>
                    </li>
                    `;
                }
                html_row += `</ul>`;

                // insert content
                $(".content_service").html(html_row);

                // Scroll
                scrollJumpto ( '.box_service' );
            } else {
                $(".content_service").html("No services found in this category");
            }
        }
    });

    // Load gallery right
    loadGallery(pg_id,300);
}

function saveForm() {
    // Save form
    var formdata = $("#surveyForm").serialize();
    $.ajax({
        type: "post",
        url: "/book/saveform",
        data: formdata,
        success:function(html)
        {
            // console.log(html);
        }
    });
}

function loadForm(formdata) {
    var obj = JSON.parse(formdata);
    $("input[name='booking_date']").val(obj.booking_date);
    $("input[name='booking_hours']").val(obj.booking_hours);
    var listservice = typeof(obj.service_staff) != "undefined" ? obj.service_staff : [];
    // console.log(listservice);
    if(listservice.length > 0 )
    {
        for(var x in listservice)
        {
            // split info
            var list = listservice[x].split(',');

            // Trigger add row
            if(x>0)
            {
                $(".addButton").trigger("click");
            }
            var objservice = $(".list_service:last");
            $(".list_service:last option[value='"+list[0]+"']").attr("selected", "selected");
            objservice.trigger("change");
            $(".list_staff:last option[value='"+list[1]+"']").attr("selected", "selected");
            
        }

        // Trigger action search for show staff list
        $(".btn_action").trigger("click");
    }
}

function convertDate(input) {
    var list_date = input.split("/");
    var splitDate = posFormat.split(",");
    var new_date = list_date[splitDate[2]]+"/"+list_date[splitDate[1]]+"/"+list_date[splitDate[0]];
    return new_date;
}

function pushHtmlTime(input_date,type) {
    
    $.ajax({
        type: "post",
        url: "/book/get_hours",
        data: {input_date: input_date, type: type},
        beforeSend: function(){
            $(".box_detail_info").append("<div class='mask_booking'><i class='fa fa-spinner fa-pulse fa-3x fa-fw'></i></div>");
            $(".box_detail_info").css("position","relative");
            $(".mask_booking").css("position","absolute").css("height","100%").css("width","100%").css("top",0).css("left",0).css("background","rgba(0,0,0,0.5)").css("text-align","right");
            $(".mask_booking i").css("font-size","2em").css("margin","10px");
        },
        success: function(response)
        {
            // console.log(response);
            // Remove mask
            $(".mask_booking").remove();
            var obj = JSON.parse(response);
            if(obj.checkmorning == false)
            {
                $(".note_am_time").html("(Booking time has expired)");
            }else
            {
                $(".note_am_time").html("");
            }

            if(obj.checkafternoon == false)
            {
                $(".note_pm_time").html("(Booking time has expired)");
            }else
            {
                $(".note_pm_time").html("");
            }

            $(".databooktime .timemorning").html(obj.htmlMorning);
            $(".databooktime .timeafternoon").html(obj.htmlAfternoon);
        }
    });
}

function setHtmldate(date_choose) {
    // use for booking
    var new_date = convertDate(date_choose);
    var d = new Date(new_date);

    var months = ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];
    var days = ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"];
    var str_show = days[d.getDay()]+", "+months[d.getMonth()]+"-"+d.getDate()+"-"+d.getFullYear();
    // console.log(str_show);
    $(".time_show").html(str_show);
}

function changeTimeByDate(input_date, typehtml)
{
    // check date time
    var splitDate = posFormat.split(",");//1,0,2
    // change time
    $.ajax({
        type:"post",
        url: "/book/change_time",
        data: {date: input_date},
        success: function(response)
        {
            // console.log(response);
            if(response)
            {
                var obj = JSON.parse(response);
                timeMorning = JSON.stringify(obj.time_morning);
                // convert time afternoon
                var afternoon_time = obj.time_afternoon;
                for(var x in afternoon_time)
                {
                    var listTime = afternoon_time[x].split(":");

                    if(listTime[0] >=1 && listTime[0] < 12)
                    {
                        var changeTime = parseInt(listTime[0])+12;
                        afternoon_time[x] = changeTime+":"+listTime[1];
                    }
                }
                
                timeAfternoon = JSON.stringify(afternoon_time);
                pushHtmlTime(input_date, typehtml);
            }
        }
    });
}

function loadEvent()
{
    // Add button click handler
    $('#surveyForm').on('click', '.addButton', function() {
        var template = `
        <div class="item-booking">
            <div class="row is-more">
                <!-- Removebutton -->
                <div class="remove-services removeButton pointer">
                    <i class="fa fa-minus-circle fa-lg"></i>
                </div>
                `+$('#optionTemplate').html()+`
            </div>
        </div>
        `;
        $(this).before($(template));
        $("#surveyForm .item-booking:last .list_service").trigger('change');
        saveForm();
    });

    // Remove button click handler
    $('#surveyForm').on('click', '.removeButton', function() {
        var $row = $(this).parents('.item-booking'),
            $option = $row.find('[name="option[]"]');

        // Remove element containing the option
        $row.remove();
        saveForm();
    });
}

// Already create in app_script.js
if (typeof redirectUrl != 'function') {
    function redirectUrl ( url, target ) 
    {
        // Check target
        if ( typeof target == 'undefined' ) 
        {
            target = '_self';
        }

        // append element
        var redirect_url = 'redirect_url_' + new Date().getTime();
        $('body').append('<div style="display:none;"><a class="' + redirect_url + '" target="' + target + '">&nbsp;</a></div>');

        // Call event
        var redirect = $('.' + redirect_url);
            redirect.attr('href',url);
            redirect.attr('onclick',"document.location.replace('" + url + "'); return false;");
            redirect.trigger('click');
    }
}
// END FUNCITON

(function($) {
    'use strict';

    /**
    * Set date: Init date time picker for booking
    * Note: place here for deny error when load booking email form in first
    */
    Date.prototype.addHours = function(h) {
        this.setTime(this.getTime() + (h*60*60*1000));
        return this;
    }
    var today = new Date(currDateT);
    var future = new Date(currDateT);

    if(beforeTime == undefined || beforeTime == '' || beforeTime<0){
        beforeTime = 0;
    }
    var fourHoursLater =new Date().addHours(beforeTime);

    var set_date = parseInt(beforeDay) > 0 ? new Date(future.setDate(today.getDate()+beforeDay)) :  fourHoursLater;
        set_date = moment(set_date).format(dateFormatBooking);
        set_date = moment(set_date, dateFormatBooking).toDate();

    $('.inputDate').datetimepicker({
        format: dateFormatBooking,
    });
    // End set date

    $('#datetimepicker_v1, .booking_date').datetimepicker({
        format: dateFormatBooking,
        minDate: set_date,
    });

    // Load Token
    $.ajax({
        type: "post",
        url: "/security/create",
        success: function(token)
        {
            $("form").each(function(){
                $(this).prepend("<input type='hidden' name='token' value='"+token+"' />");
            });
        }
    });// End Load Token

    // Auto Select
    $("select.auto_select").each(function(){
      var val_default = $(this).attr("defaultvalue");
      $(this).find("option[value='"+val_default+"']").prop("selected",true);
    });// End Auto Select

    // Mask Input
    var plholder = phoneFormat == "(000) 000-0000" ? "Phone (___) ___-____" : "Phone ____ ___ ____";
    $(".inputPhone").mask(phoneFormat, {placeholder: plholder});// End mask input

    // Validate Form
    $("#send_newsletter").validate({
        submit: {
            settings: {
                button: ".btn_send_newsletter",
                inputContainer: '.form-group',
                errorListClass: 'form-tooltip-error',
            },
            callback: {
                onSubmit: function (node, formdata) 
                {
                    var url_send = $(node).attr("action");
                    var email = $(node).find("input[name='newsletter_email']").val();
                    var token = $(node).find("input[name='token']").val();
                    var container = $(node).find("input[name='newsletter_container']").val();

                    $.ajax({
                        type: "post",
                        url: url_send,
                        data: {
                            newsletter_email: email, 
                            token: token
                        },
                        success: function(html)
                        {
                            var obj = JSON.parse(html);
                            call_notify("Notification", obj.message, obj.status);
                            $(node).find("input[name='newsletter_email']").val("");
                            
                            // An form
                            if( obj.status == "success" )
                            {
                                $(container).html('<p>Thanks for subscribing!</p>');
                            }
                        }
                    });
                }
            }
        }
    });
    $("#send_contact").validate({
        submit: {
            settings: {
                button: ".btn_contact",
                inputContainer: '.form-group',
                errorListClass: 'form-tooltip-error',
            }
        }
    });
    $("#send_contact_global").validate({
        submit: {
            settings: {
                button: ".btn_contact",
                inputContainer: '.form-group',
                errorListClass: 'form-tooltip-error',
            }
        }
    });
    $("#send_booking").validate({
        submit: {
            settings: {
                button: ".btn_booking",
                inputContainer: '.input-box',
                errorListClass: 'form-tooltip-error',
            }
        }
    });// End Validate Form

    // Choose Date Appointent
    $("#send_booking").on("dp.change",".choose_date", function(){
        
        var typehtml = $(this).attr("typehtml");
        var date_choose = $(this).val();

        // change time by date choose
        changeTimeByDate(date_choose, typehtml);
        //data time
        // setTimeout(function(){ pushHtmlTime(date_choose, typehtml); }, 100);
    });// End Choose Date Appointent

    // Choose Service Booking
    $("#surveyForm").on("change",".list_service", function(){
        var service_id = $(this).val();
        var list_staff = $(this).find("option:selected").attr("staff");
        if(service_id)
        {
            $(this).css("border-color","#ccc");
            $(this).parent().find('.form-tooltip-error').remove();
        }else
        {
            $(this).css("border-color","red");
            $(this).parent().append('<div class="form-tooltip-error" data-error-list=""><ul><li>'+$(this).data('validation-message')+'</li></ul></div>');
        }
        var obj = JSON.parse(list_staff);
        var option = '<option value="">Service Provider</option>';
        for(var x in obj)
        {
            option += `<option value="`+obj[x].id+`" urlimg="`+obj[x].image+`">`+obj[x].name+`</option>`;
        }

        $(this).parents(".item-booking").find(".list_staff").html(option);

        // Save form
        saveForm();

    });// End Choose Service Booking

    // Choose Provider Booking
    $("#surveyForm").on("change",".list_staff", function(){
        // Save form
        saveForm();
    });// End Choose Provider Booking

    // Choose Date Booking
    $("#surveyForm").on("dp.change",".choose_date", function(){
        
        var typehtml = $(this).attr("typehtml");
        var date_choose = $(this).val();
        // set Html date
        setHtmldate(date_choose);
        // Save form
        saveForm();

        // change time by date choose
        changeTimeByDate(date_choose, typehtml);
        //data time
        // setTimeout(function(){ pushHtmlTime(date_choose, typehtml); }, 100);
    });// End Choose Date Booking

    // Button Search Booking
    $(".btn_action").click(function(){

        var num = $(".list_service").length;
        var info_staff = [];
        var info_staff2 = [];
        var temp = {};
        var i = 0;
        var check = true;

        $(".list_service").each(function(){
            var checkval = $(this).val();
            if(checkval) 
            { 
                $(this).css("border-color","#ccc");
                $(this).parent().find('.form-tooltip-error').remove();
            }
            else
            {
                check = false;
                $(this).css("border-color","red");
                $(this).parent().append('<div class="form-tooltip-error" data-error-list=""><ul><li>'+$(this).data('validation-message')+'</li></ul></div>');
            }
            temp.price = $('option:selected', this).attr('price');
            temp.service = $('option:selected', this).text();
            info_staff.push(temp);
            temp = {};
            i++;
        });
        
        var j = 0;
        $(".list_staff").each(function(){
            var checkval = $(this).val();
            temp.image = $('option:selected', this).attr('urlimg');
            temp.name = checkval ? $('option:selected', this).text() : "Any person";
            info_staff2.push(temp);
            temp = {};
            j++;
        });

        if(check == true)
        {   
            $(".box_detail_info").show();
            $("#box_person").html("Loading ...");
            var html_person = "";
            var j = 0;
            for(var x in info_staff)
            {
                var image = typeof(info_staff2[x].image) === "undefined" ? "/public/library/global/no-photo.jpg" : info_staff2[x].image;
                html_person += `
                <div class="staff_service_v1 col-sm-6 col-md-6">
                    <div class="col-xs-4 staff-avatar">
                        <div title="staff avatar">
                            <img src="`+image+`" alt="`+info_staff2[x].name+`">
                        </div>
                    </div>
                    <div class="col-xs-8">
                        <h4>`+info_staff2[x].name+`</h4>
                        <p>`+info_staff[x].service+`</p>
                        <p>Price: `+info_staff[x].price+`</p>
                    </div>
                </div>
                `;
            }

            // insert data
            $("#box_person").html(html_person);
            
            // Scroll
            scrollJumpto ( '#book-info', '.top-fixed' );
        }
        else
        {
            return false;
        }
    });// End Button Search Booking

    // Confirm Booking
    $(".databooktime").on("click",".popup_login", function(){
        $.magnificPopup.open({
            type: 'inline',
            midClick: true,
            items: {
                  src: '#popup_login'
                },
        });
        return false;
    });
    
    $("body").on("click",".open_booking", function(){
        // Check service
        var check = true;
        $(".list_service").each(function(){
            var checkval = $(this).val();
            if(checkval) 
            { 
                $(this).css("border-color","#ccc");
                $(this).parent().find('.form-tooltip-error').remove();
            }
            else
            {
                check = false;
                $(this).css("border-color","red");
                $(this).parent().append('<div class="form-tooltip-error" data-error-list=""><ul><li>'+$(this).data('validation-message')+'</li></ul></div>');
            }
        });
        
        if(check == false)
        {
            return false;
        }
        
        var hours = $(this).attr("valhours");
        $.magnificPopup.open({
            type: 'inline',
            midClick: true,
            items: {
              src: '#open_booking'
            },
            callbacks: {
                beforeOpen: function() {
                    if($(window).width() < 700) {
                        this.st.focus = false;
                    } else {
                        this.st.focus = '#name';
                    }
                    $("input[name='booking_hours']").val(hours);
                }
            }
        });

        return false;
    });

    $(".btn_cancel").click(function(){
        $.magnificPopup.close();
    });// End Confirm Booking

    /*Anchor link*/
    $('[href^="#"]').on("click", function (event) {
        let _h = $(this).attr('href');
        let _hsplit = _h.substr(1, _h.length);
        if ( _hsplit != 'open_booking' ) {
            event.preventDefault();
            scrollJumpto(_h, window.matchMedia('(min-width: 992px)').matches ? '.fixed-freeze.desktop' : '.fixed-freeze.mobile');
        }
    });
})(jQuery);

function initGalleryTab( elementTab, elementContent ) {
    let objTab = $(elementTab);

    objTab.on("click", ".tab", function () {
        let _this = $(this);
        let id = _this.attr("data-id");

        // Class active
        objTab.find('.tab').removeClass('active');
        _this.addClass("active");

        getGalleryByCat(id, 1, elementContent);
    });

    // First load
    objTab.find('.tab').first().trigger('click');
}

function getGalleryByCat(cat_id, page, elementContent) {
    cat_id = cat_id ? cat_id : 0;
    page = page ? page : 0;

    // Category Status
    let objOptionGalleryByCat = $('#optionGalleryByCat_' + cat_id);
    if ( objOptionGalleryByCat.length <= 0 ) {
        objOptionGalleryByCat = $('#optionGalleryByCat');
    }

    let categoryStatus = objOptionGalleryByCat.attr('data-categoryStatus');
    if ( typeof categoryStatus == 'undefined') {
        categoryStatus = 1;
    } else if ( categoryStatus == 'all' ) {
        categoryStatus = false;
    } else {
        categoryStatus = categoryStatus*1;
    }

    let objContent = $(elementContent);
    let mask_loading_obj = $('<div class="mask_booking" style="position: absolute; z-index: 3;height: 100%; width: 100%; top: 0; left: 0; background:rgba(0,0,0,0.5);text-align: center;"><i class="fa fa-spinner fa-pulse fa-3x fa-fw"></i></div>');

    $.ajax({
        type: "post",
        url: "/gallery/getlistbycat",
        beforeSend: function () {
            objContent.append(mask_loading_obj);
        },
        data: {cat_id: cat_id, page: page, blockId: elementContent, cat_status: categoryStatus},
        success: function (response) {
            let obj = JSON.parse(response);

            let html = `<div class="gallery-box-wrap">`;

            if ( obj.data.length > 0 ) {
                html += `<div class="row">`;
                for (var x in obj.data) {
                    html += `
                    <div class="col-xs-6 col-sm-6 col-md-4">
                        <div itemprop="url" class="pointer gallery-box pointer image-magnific-popup" data-group="gallery-${cat_id}" 
                             title="${obj.data[x].name}" href="${obj.data[x].image}">
                            <div class="image-bg-wrap">
                                <div class="image-bg" style="background-image: url('${obj.data[x].imageThumb}');">
                                    <img itemprop="image" src="${obj.data[x].imageThumb}" alt="${obj.data[x].image_alt}">
                                </div>
                            </div>
                        </div>
                    </div>
                    `;
                }
                html += `</div>`;
            } else {
                html = "Not found gallery item in this category.";
            }
            html += `</div>`;

            objContent.find('.listing').html(html);
            objContent.find('.paging').html(obj.paging_ajax);
            initImageMagnificPopup('.m-magnific-popup');
        },
        complete: function () {
            mask_loading_obj.remove();
        }
    });
}

$(document).ready(function(){

    // Services Page
    var lid = $('ul.listcatser li[data-group-id="'+$('input[name="group_id"]').val()+'"]');
        if ( lid.length == 0 ) {lid = $("ul.listcatser li:first");}
        lid.trigger("click");// End Service Page

    /*GALLERY TAB*/
    initGalleryTab('#category_tab', '#gallery_content');
    /*END GALLERY TAB*/
});

const stackBottomRightModal = {
    dir1: "up",
    dir2: "left",
    firstpos1: 25,
    firstpos2: 25,
    push: "bottom",
};
var call_notify_object = {};
function callNotify(title_msg, msg, type_notify, delay, remove, type ) {
    type_notify = type_notify ? type_notify : "error";
    delay = delay ? +delay : 3000;
    remove = (typeof remove == 'undefined' || remove) ?  true : false;

    let icon = "";
    if(type_notify == "error" || type_notify == "notice") {
        icon = "fa fa-exclamation-circle";
    } else if(type_notify == "success") {
        icon = "fa fa-check-circle";
    }

    if( remove && typeof call_notify_object.remove === 'function' )
    {
        call_notify_object.remove();
    }

    let option = {
        title: title_msg,
        text: msg,
        type: type_notify,
        icon: icon,

        closer: true,
        closerHover: true,
        sticker: false,
        stickerHover: false,
        labels: {close: 'Close', stick: 'Stick', unstick: 'Unstick'},
        classes: {closer: 'closer', pinUp: 'pinUp', pinDown: 'pinDown'},

        remove: true,
        destroy: true,
        mouseReset: true,
        delay: delay,
    }

    if( !type ){
        option.addclass = 'alert-with-icon stack-bottomright';
        option.stack = stackBottomRightModal;
    } else {
        option.addclass = 'alert-with-icon';
    }

    call_notify_object = new PNotify(option);

    return call_notify_object;
}

function call_notify(title_msg, msg, type_notify) {
    callNotify(title_msg, msg, type_notify, 0, 1, 1 );
}

function change_content(elemenThis, elemenTo) {
    $(elemenTo).html($(elemenThis).val());
}

function check_enter_number(evt, onthis) {
    if (isNaN(onthis.value + "" + String.fromCharCode(evt.charCode))) {
        return false;
    }
}

function change_product(pid) {
    $.ajax({
        type: "post",
        url: "/cart/change_product",
        data: {pid:pid},
        dataType:'Json',
        success: function(obj) {
            obj.pid = pid;
            change_cart_info(obj);
        },
        error: function () {
            call_notify('Notification', 'Error when process request', "error");
        }
    });
}

function update_price(onThis) {
    let _this = $(onThis);
    let cus_price = parseFloat(_this.val());
    cus_price = isNaN(cus_price) ? 0 : cus_price;

    let id = _this.attr("pid");
    let max_val = parseFloat(_this.attr("max"));
    let min_val = parseFloat(_this.attr("min"));

    /*Change money*/
    $(".camount").html(cus_price > 0 ? `\$${cus_price}` : 'N/A');

    /*Update price*/
    if ( cus_price >= min_val && cus_price <= max_val ) {
        _this.css("border-color", "");
        $('.btn_payment').prop('disabled', false).removeClass('disabled');

        //Ajax
        $.ajax({
            type: "post",
            url: "/cart/updateprice",
            data: {cus_price: cus_price, id: id},
            success: function (html) {
                var obj = JSON.parse(html);

                if ( obj.status == "error" ) {
                    _this.val(obj.price);
                    call_notify('Notification', obj.msg, "error");
                    return false;
                }

                if (obj.cart_data) {
                    let data = {
                        "subtotal": obj.cart_data[2],
                        "discount": obj.cart_data[5],
                        "tax": obj.cart_data[1],
                        "amount": obj.cart_data[3],
                        "pid": id,
                    };
                    change_cart_info(data);
                }
            },
            error: function () {
                call_notify('Notification', 'Error when process request', "error");
            }
        });
    } else {
        _this.css("border-color", "red");
        $('.btn_payment').prop('disabled', true).addClass('disabled');
    }
}

function change_cart_info( data ) {
    $("#cart_subtotal").html(data.subtotal);
    $("#cart_product_discount_value, #cart_discount_code_value").html(data.discount);
    $("#cart_tax").html(data.tax);
    $("#cart_payment_total").html(data.amount);
    $("#custom_price").attr("pid", data.pid);
}

function update_cart(onthis) {
    let _this = $(onthis);
    let quantity = _this.val();
    let id = _this.attr("data-cart_id");

    //Ajax
    $.ajax({
        type: "post",
        url: "/cart/update",
        data: {quantity: quantity, id: id},
        success: function (html) {
            let obj = JSON.parse(html);

            /*Change money*/
            if ( obj.total_show ) {
                _this.closest(".cart_item").find(".total_change").html(obj.total_show);
            }

            if ( obj.cart_data ) {
                let data = {
                    "subtotal": obj.cart_data[2],
                    "discount": obj.cart_data[5],
                    "tax": obj.cart_data[1],
                    "amount": obj.cart_data[3],
                };
                change_cart_info(data, 1);
            }
        }
    });
}
function update_price_cart(onThis) {
    let _this = $(onThis);
    let cus_price = parseFloat(_this.val());
    cus_price = isNaN(cus_price) ? 0 : cus_price;

    let id = _this.attr("data-cart_id");
    let max_val = parseFloat(_this.attr("max"));
    let min_val = parseFloat(_this.attr("min"));

    _this.css("border-color", "");
    $('.btn_cart_order').prop('disabled', false).removeClass('disabled');

    /*Update price*/
    if ( cus_price >= min_val && cus_price <= max_val ) {
        //Ajax
        $.ajax({
            type: "post",
            url: "/cart/updateprice",
            data: {cus_price: cus_price, id: id},
            success: function (html) {
                let obj = JSON.parse(html);

                /*Change money*/
                if ( obj.total_show ) {
                    _this.closest(".cart_item").find(".total_change").html(obj.total_show);
                }

                if ( obj.status == "error" ) {
                    _this.val(obj.price);
                    call_notify('Notification', obj.msg, "error");
                    return false;
                }

                if (obj.cart_data) {
                    let data = {
                        "subtotal": obj.cart_data[2],
                        "discount": obj.cart_data[5],
                        "tax": obj.cart_data[1],
                        "amount": obj.cart_data[3],
                    };
                    change_cart_info(data, 1);
                }
            },
            error: function () {
                call_notify('Notification', 'Error when process request', "error");
            }
        });
    } else {
        _this.css("border-color", "red");
        $('.btn_cart_order').prop('disabled', true).addClass('disabled');
    }
}
function del_item(onthis) {
    let _this = $(onthis);
    let id = _this.attr("data-cart_id");

    $.ajax({
        type: "post",
        url: "/cart/delitem",
        data: {id: id},
        success: function (html) {
            let obj = JSON.parse(html);

            /*Delete item*/
            _this.closest(".cart_item").remove();

            /*Change order*/
            let cart_items = $(".cart_item");
            if ( cart_items.length <= 0 ) {
                $("#cart_items").html('<tr><td colspan="5"><div class="price-row-col"><b>Cart empty...</b></div></td></tr>');
            }

            if ( obj.cart_data ) {
                let data = {
                    "subtotal": obj.cart_data[2],
                    "discount": obj.cart_data[5],
                    "tax": obj.cart_data[1],
                    "amount": obj.cart_data[3],
                };
                change_cart_info(data, 1);
            }
        }
    });
}

(function ($) {
    'use strict';

    /*GIFTCARDS PAYMENT*/
    let formPaymentGiftcards = $("form#paymentGiftcards");
    formPaymentGiftcards.validate({
        submit: {
            settings: {
                clear: 'keypress',
                display: "inline",
                button: ".btn_payment",
                inputContainer: 'form-group',
                errorListClass: 'form-tooltip-error',
            },
            callback: {
                onSubmit: function (node, formdata) {
                    let isValidate = true;

                    /*Deny duplicate click*/
                    formPaymentGiftcards.find(".btn_payment").attr("disabled", "disabled");

                    /*Clears all form errors*/
                    formPaymentGiftcards.removeError();

                    /*Check price*/
                    if( enable_giftcard_buy_custom == 1 ){
                        let custom_price = formdata['custom_price'] * 1;
                        let custom_price_obj = formPaymentGiftcards.find('[name="custom_price"]');
                        let min = custom_price_obj.attr("min") * 1;
                        let max = custom_price_obj.attr("max") * 1;
                        if(custom_price < min || custom_price > max) {
                            isValidate = false;

                            let notify = `Pay as you go (From \$${min} to \$${max})`;
                            formPaymentGiftcards.addError({
                                'custom_price': notify,
                            });
                        }
                    }

                    if( isValidate ){
                        waitingDialog.show("Please wait a moment ...");
                        node[0].submit();
                    } else {
                        formPaymentGiftcards.find(".btn_payment").removeAttr("disabled");
                        scrollJumpto(formPaymentGiftcards);
                    }

                    return false;
                },
                onError: function () {
                    scrollJumpto(formPaymentGiftcards);
                }
            }
        }
    });

    /*FORM PAYMENT*/
    let formPayment = $("form#payment");
    formPayment.validate({
        submit: {
            settings: {
                clear: 'keypress',
                display: "inline",
                button: "[type='submit']",
                inputContainer: 'form-group',
                errorListClass: 'form-tooltip-error',
            },
            callback: {onSubmit: function (node, formdata) {

                    /*Deny duplicate click*/
                    formPayment.find(".btn_payment").attr("disabled", "disabled");

                    waitingDialog.show("Please wait a moment ...");
                    node[0].submit();

                    return false;
                },
                onError: function () {
                    scrollJumpto(formPayment);
                }
            }
        }
    });

    $("body").on("click", ".box_img_giftcard", function (e) {
        e.preventDefault();

        let _this = $(this);

        $(".box_img_giftcard").removeClass("active");
        _this.addClass("active");

        let src_img = _this.find("img").first().attr("src");
        var pid = _this.attr("pid");
        var name = _this.attr("name");
        var price = _this.attr("price");

        /*Payer*/
        formPaymentGiftcards.removeError();
        let custom_price_obj = formPaymentGiftcards.find('[name="custom_price"]');
        custom_price_obj.val(price).attr('pid', pid);

        if ( enable_giftcard_buy_custom == 1 ) {
            custom_price_obj.attr('min', price);
            let max = custom_price_obj.attr('max');
            formPaymentGiftcards.find('.custom_price_note').text(`From \$${price} to \$${max}`);
        }

        /*Cart*/
        $('#cart_image img').attr("src", src_img);
        $('#cart_name').text(name);

        /*Preview*/
        $(".preview_img img").attr("src", src_img);
        $('.camount').text(`\$${price}`);

        change_product(pid);
    });

    $("body").on("click", "input[name='send_to_friend']", function (e) {
        let _this = $(this);
        if ( _this.val() == 0 ) {
            _this.val(1).prop('checked', true);
            $(".box_recipient").show();
        } else {
            _this.val(0).prop('checked', false);
            $(".box_recipient").hide();
        }
    });
})(jQuery);