function redirectUrl ( url, target ) 
{
    // Check target
    if ( typeof target == 'undefined' ) 
    {
        target = '_self';
    }

    // append element
    var redirect_url = 'redirect_url_' + new Date().getTime();
    $('body').append('<div style="display:none;"><a class="' + redirect_url + '" target="' + target + '">&nbsp;</a></div>');

    // Call event
    var redirect = $('.' + redirect_url);
        redirect.attr('href',url);
        redirect.attr('onclick',"document.location.replace('" + url + "'); return false;");
        redirect.trigger('click');
}

function scrollJumpto ( jumpto, headerfixed, redirect ) 
{
    // check exits element for jumpto
    if ( $(jumpto).length > 0 ) 
    {
        // Calculator position and call jumpto with effect
        jumpto = $(jumpto).offset().top;
        headerfixed = ( $(headerfixed).length > 0 ) ? $(headerfixed).height() : 100;

        $('html, body').animate({
            scrollTop: parseInt(jumpto - headerfixed) + 'px'
        }, 1000, 'swing');
    }
    // Check redirect if not exits element for jumpto
    else if ( redirect ) 
    {
        // Call redirect
        redirectUrl(redirect);
        return;
    }
    else
    {
        console.log(jumpto + ' Not found.');
    }
}

function getGalleryByCat(cat_id=0, page=0)
{
    $.ajax({
        type: "post",
        url: "/gallery/getlistbycat",
        beforeSend: function() {
            $(".box_list_gallery").html('Loading.....');
        },
        data: {cat_id: cat_id, page:page},
        success: function(html) {
            var obj = JSON.parse(html);
            var html_gallery="";
            if(obj.data.length > 0)
            {
                for(var x in obj.data)
                {
                    html_gallery += `
                    <div class="col-xs-12 col-sm-6 col-md-4 col-lg-3 gallery-image-wrap pro-item all `+obj.data[x].cat_url+`" data-groups='["all","`+obj.data[x].cat_url+`"]'>
                        <a href="`+obj.data[x].image+`" class="gallery-item" title="`+obj.data[x].name+`">
                            <div class="image-bg" style="background-image: url('`+obj.data[x].imageThumb+`');">
                                <img class='gallery-media-image img-responsive' src="`+obj.data[x].imageThumb+`">
                            </div>
                            <span class="gallery-media-info" style="">
                                <div class="gallery-media-info-wrap">
                                    <span class="gallery-media-info-counter"><span class="instashow-icon"><i class="fa fa-search"></i></span></span>   
                                    <div class="gallery-media-info-description">
                                        <h3>`+obj.data[x].name+`</h3>
                                        <span>`+obj.data[x].description+`</span>
                                    </div>   
                                </div>
                            </span>
                        </a>
                    </div>
                    `;
                }
            }else
            {
                html_gallery="Not found gallery item in this category.";
            }

            $(".box_list_gallery").html(html_gallery);
            $(".box_paging").html(obj.paging_ajax);
        },
        complete: function(){
            // Re call isotope
            if ( $('#our-portfolio-page').length > 0 && $('#our-portfolio-page.using-isotope-library-with-ajax').length > 0 ) {
                if ( $('#our-portfolio-page').hasClass('isotope-initialized') ) {
                    $('#our-portfolio-page').isotope('destroy');
                }
                $('#our-portfolio-page').imagesLoaded(function() {
                    $('#our-portfolio-page').isotope({
                        itemSelector : '.pro-item',
                        filter: '*',
                        transitionDuration: '0.7s',
                        masonry: {
                            columnWidth: '.pro-item'
                        },
                        layoutMode : 'fitRows',
                        animationEngine : 'best-available'
                    });
                    $('#our-portfolio-page').addClass('isotope-initialized');
                    $(window).trigger('resize');
                });
            }
            // End Re call isotope

            // Init magic popup
            var groups = {};
            $('.gallery-item').each(function() {
                var id = $(this).attr('data-group');
              
                if(!groups[id]) {
                    groups[id] = [];
                } 
                groups[id].push( this );
            });

            $.each(groups, function() {
                $(this).magnificPopup({
                    type: 'image',
                    closeOnContentClick: true,
                    closeBtnInside: true,
                    gallery: { enabled:true }
                })
            });
            // End init magic popup
        }
    });
}

const stackBottomRightModal = {
    dir1: "up",
    dir2: "left",
    firstpos1: 25,
    firstpos2: 25,
    push: "bottom",
};
var call_notify_object = {};
function callNotify(title_msg, msg, type_notify, delay, remove, type ) {
    type_notify = type_notify ? type_notify : "error";
    delay = delay ? +delay : 3000;
    remove = (typeof remove == 'undefined' || remove) ?  true : false;

    let icon = "";
    if(type_notify == "error" || type_notify == "notice") {
        icon = "fa fa-exclamation-circle";
    } else if(type_notify == "success") {
        icon = "fa fa-check-circle";
    }

    if( remove && typeof call_notify_object.remove === 'function' )
    {
        call_notify_object.remove();
    }

    let option = {
        title: title_msg,
        text: msg,
        type: type_notify,
        icon: icon,

        closer: true,
        closerHover: true,
        sticker: false,
        stickerHover: false,
        labels: {close: 'Close', stick: 'Stick', unstick: 'Unstick'},
        classes: {closer: 'closer', pinUp: 'pinUp', pinDown: 'pinDown'},

        remove: true,
        destroy: true,
        mouseReset: true,
        delay: delay,
    }

    if( !type ){
        option.addclass = 'alert-with-icon stack-bottomright';
        option.stack = stackBottomRightModal;
    } else {
        option.addclass = 'alert-with-icon';
    }

    call_notify_object = new PNotify(option);

    return call_notify_object;
}
function call_notify(title_msg, msg, type_notify)
{
    callNotify(title_msg, msg, type_notify, 0, 1, 1 );
}

function loadService(pg_id=0, _page=0, resizeWidth=0) 
{
    var btn_appointment = "";
    if(typeof(enable_booking) != "undefined" && enable_booking==1)
    {
        btn_appointment = "<a class='btn btn-mod btn-border btn-bg-white btn-round btn-medium mb-15 btn_make_appointment' href='/book'>Make an appointment</a>";
    }

    $("ul.filter_service li").removeClass("active");
    $("ul.filter_service li[data-group-id='"+pg_id+"']").addClass("active");

    $.ajax({
        type: "post",
        url: "/service/loadservice",
        data: {pg_id: pg_id, limit: num_paging, page: _page, paging: 1, resize_width: resizeWidth},
        beforeSend: function() {
            $(".content_service").html("Loading...");
        },
        success: function(html)
        {
            var obj = JSON.parse(html);
            $(".paging_service").html(obj.paging_ajax);
            var group_des = obj.group_des;
            obj = obj.data;
            if(obj.length > 0)
            {
                var html_row = `
                <ul id="all-item">
                    <li class="item-botton-1" style="height:0px;width:100%;border:none;">&nbsp;</li>
                    <li class="item-botton services_item_v1 clearfix text-right" style="border:none;">
                        `+btn_appointment+`
                        <a class="btn btn-mod btn-border btn-bg-white btn-round btn-medium mb-15" style="margin-left:15px;" href="tel:` + company_phone + `"><span class="icon"><i class="icon-phone"></i></span><span class="title">Call now</span></a>
                    </li>
                `;

                if(group_des)
                {
                    html_row += `<li class="des_service" style="border-top: none; padding: 10px 0;">
                                `+group_des+`
                                </li>`;
                }

                var pull_right = "pull-right";
                if( /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) ) {
                    pull_right = "";
                }
                for(x in obj)
                {
                    var price_show = obj[x].price_sell ? obj[x].price_sell : "";
                    html_row += `
                    <li class="services_item_v1">
                        <div class="line_item_v1">
                            <div class="just_start_line">
                                <a style='cursor: pointer;' class="open_description" data-toggle="tooltip" data-placement="top" title="`+obj[x].description+`">
                                    <span>`+obj[x].name+`</span>
                                    <span class="price_service_v1 `+pull_right+`">`+price_show+obj[x].product_up+`</span>
                                </a>
                                <div class="box_des">
                                    `+obj[x].product_description+`
                                </div>
                            </div>
                            <!--
                            <div class="just_end_line">
                                <a class="hs-btn btn_2 btn-light" href="`+obj[x].link_book+`">Book</a>
                            </div>
                            -->
                        </div>
                    </li>`;
                }

                html_row += `</ul>`;

                $(".content_service").html(html_row);

                if ( $(".box_service").length > 0 ) {
                    var scrolltop = $(".box_service").offset().top;
                    var box_service_top = $('.box_service_top');

                    if ( box_service_top.length > 0 ) {
                        scrolltop -= box_service_top.height();
                    }
                    
                    $('html, body').animate({
                        scrollTop: scrolltop
                    }, 1000);
                }
            }else
            {
                $(".content_service").html("No services found in this category");
            }
        }
    });

    // Load gallery right
    loadGallery(pg_id,300);
}

function saveForm()
{
    // Save form
    var formdata = $("#surveyForm").serialize();
    $.ajax({
        type: "post",
        url: "/book/saveform",
        data: formdata,
        success:function(html)
        {
            // console.log(html);
        }
    });
}

function loadForm(formdata)
{
    var obj = JSON.parse(formdata);
    $("input[name='booking_date']").val(obj.booking_date);
    $("input[name='booking_hours']").val(obj.booking_hours);
    var listservice = typeof(obj.service_staff) != "undefined" ? obj.service_staff : [];
    // console.log(listservice);
    if(listservice.length > 0 )
    {
        for(var x in listservice)
        {
            // split info
            var list = listservice[x].split(',');
            // Trigger add row
            if(x>0)
            {
                $(".addButton").trigger("click");
            }
            var objservice = $(".list_service:last");
            $(".list_service:last option[value='"+list[0]+"']").attr("selected", "selected");
            objservice.trigger("change");
            $(".list_staff:last option[value='"+list[1]+"']").attr("selected", "selected");
            
        }

        // Trigger action
        // $(".btn_action").trigger("click");
    }
}

function loadGallery(pg_id=0, resizeWidth=0)
{
    if(pg_id)
    {
        $.ajax({
            type: "post",
            url: "/service/loadgallery",
            data: {id:pg_id, resize_width:resizeWidth},
            beforeSend: function()
            {
                // $(".box_show_gallery").html("Loading...");
            },
            success: function(html)
            {
                // console.log(html);
                var obj = JSON.parse(html);
                var html_img = '';
                for(var x in obj)
                {
                    html_img +=`<li>
                                    <div class="pointer gallery-item" data-group="service-gallery" href="`+obj[x].image+`" title="`+obj[x].name+`">
                                        <img itemprop="image" alt="" src="`+obj[x].image+`" class="img-responsive">
                                    </div>
                                </li>`;
                }

                $(".box_show_gallery").html(html_img);

                // Init magic popup
                var groups = {};
                $('.gallery-item-popup').each(function() {
                    var id = $(this).attr('data-group');
                  
                    if(!groups[id]) {
                        groups[id] = [];
                    } 
                    groups[id].push( this );
                });

                $.each(groups, function() {
                    $(this).magnificPopup({
                        type: 'image',
                        closeOnContentClick: true,
                        closeBtnInside: false,
                        gallery: { enabled:true }
                    })
                });
                // End init magic popup
            }
        });
    }
}

function convertDate(input)
{
    var list_date = input.split("/");
    var splitDate = posFormat.split(",");
    var new_date = list_date[splitDate[2]]+"/"+list_date[splitDate[1]]+"/"+list_date[splitDate[0]];
    return new_date;
}

function pushHtmlTime(input_date,type)
{
    $.ajax({
        type: "post",
        url: "/book/get_hours",
        data: {input_date: input_date, type: type},
        beforeSend: function(){
            $(".box_detail_info").append("<div class='mask_booking'><i class='fa fa-spinner fa-pulse fa-3x fa-fw'></i></div>");
            $(".box_detail_info").css("position","relative");
            $(".mask_booking").css("position","absolute").css("height","100%").css("width","100%").css("top",0).css("left",0).css("background","rgba(0,0,0,0.5)").css("text-align","right");
            $(".mask_booking i").css("font-size","2em").css("margin","10px");
        },
        success: function(response)
        {
            // console.log(response);
            // Remove mask
            $(".mask_booking").remove();
            var obj = JSON.parse(response);
            if(obj.checkmorning == false)
            {
                $(".note_am_time").html("(Booking time has expired)");
            }else
            {
                $(".note_am_time").html("");
            }

            if(obj.checkafternoon == false)
            {
                $(".note_pm_time").html("(Booking time has expired)");
            }else
            {
                $(".note_pm_time").html("");
            }

            $(".databooktime .timemorning").html(obj.htmlMorning);
            $(".databooktime .timeafternoon").html(obj.htmlAfternoon);
        }
    });
}

function setHtmldate(date_choose)
{
    // use for booking
    var new_date = convertDate(date_choose);
    var d = new Date(new_date);

    var months = ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];
    var days = ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"];
    var str_show = days[d.getDay()]+", "+months[d.getMonth()]+"-"+d.getDate()+"-"+d.getFullYear();
    // console.log(str_show);

    $(".time_show").html(str_show);
}

function loadEvent()
{
    // Add button click handler
    $('#surveyForm').on('click', '.addButton', function() {
        var template = `
        <div class="item-booking">
            <div class="row is-more">
                <!-- Removebutton -->
                <div class="remove-services removeButton pointer">
                    <img src="/public/library/global/remove-service-icon-new.png">
                </div>
                `+$('#optionTemplate').html()+`
            </div>
        </div>
        `;
        $(this).before($(template));
        $("#surveyForm .item-booking:last .list_service").trigger('change');
        saveForm();
    });

    // Remove button click handler
    $('#surveyForm').on('click', '.removeButton', function() {
        var $row = $(this).parents('.item-booking'),
            $option = $row.find('[name="option[]"]');

        // Remove element containing the option
        $row.remove();
        saveForm();
    });
}


function update_cart(onthis)
{
    var quantity = $(onthis).val();
    var id = $(onthis).attr("cart_id");
    //Ajax
    $.ajax({
        type: "post",
        url: "/cart/update",
        data: {quantity: quantity, id: id},
        success: function(html)
        {
            // console.log(html);
            var obj = JSON.parse(html);
            // set value
            if(obj.total_show && obj.amount)
            {
                $(onthis).parents("tr").find(".total_change").html(obj.total_show);
                $(".amount_change").html(obj.amount);
            }

            if(obj.cart_data){
                $("#cart_tax").text(obj.cart_data[1]);
                $("#cart_discount_code_value").text(obj.cart_data[5]);
                $("#cart_subtotal").text(obj.cart_data[2]);
                $("#cart_payment_total").text(obj.cart_data[3]);
            }
        }
    });
}

function update_price(onthis)
{
    var cus_price = isNaN(parseFloat($(onthis).val())) ? 0 : parseFloat($(onthis).val());
    var id = $(onthis).attr("cart_id");
    var max_val = parseFloat($(onthis).attr("max"));
    var min_val = parseFloat($(onthis).attr("min"));
    
    if(cus_price >= min_val && cus_price <= max_val)
    {
        $(onthis).css("border-color", "#ccc");
        $(".btn_cart_order").attr("href","/payment");
        //Ajax
        $.ajax({
            type: "post",
            url: "/cart/updateprice",
            data: {cus_price: cus_price, id: id},
            success: function(html)
            {
                // console.log(html);
                var obj = JSON.parse(html);
                if(obj.status == "error")
                {
                    call_notify('Notification',obj.msg, "error");
                    $(onthis).val(obj.price);
                    return false;
                }
                // set value
                if(obj.total_show && obj.amount)
                {
                    $(onthis).parents("tr").find(".total_change").html(obj.total_show);
                    $(".amount_change").html(obj.amount);
                }

                if(obj.cart_data){
                    $("#cart_tax").text(obj.cart_data[1]);
                    $("#cart_discount_code_value").text(obj.cart_data[5]);
                    $("#cart_subtotal").text(obj.cart_data[2]);
                    $("#cart_payment_total").text(obj.cart_data[3]);
                }
            }
        });
    }else
    {
        $(onthis).css("border-color", "red");
        $(".btn_cart_order").removeAttr("href");
    }
}


function delItem(onthis)
{
    var id = $(onthis).attr("cart_id");
    //Ajax
    $.ajax({
        type: "post",
        url: "/cart/delitem",
        data: {id: id},
        success: function(html)
        {
            // console.log(html);
            var obj = JSON.parse(html);

            // set value
            if(obj.amount)
            {
                // remove row
                $(onthis).parents("tr").remove();
                
                // change stt
                if($(".list_stt").length > 0)
                {
                    var i=1;
                    $(".list_stt").each(function(){
                        $(this).html("#"+i);
                        i++;
                    });
                }else
                {
                    $("tbody.step1").html('<tr><td colspan="5"><div class="price-row-col"><b>Cart empty...</b></div></td></tr>');
                }

                // set amount
                $(".amount_change").html(obj.amount);

                if(obj.cart_data){
                    $("#cart_tax").text(obj.cart_data[1]);
                    $("#cart_discount_code_value").text(obj.cart_data[5]);
                    $("#cart_subtotal").text(obj.cart_data[2]);
                    $("#cart_payment_total").text(obj.cart_data[3]);
                }
            }
        }
    });
}

function applyDiscountCode()
{
    $("#loader_discount_code").show();
    $("#enter_discount_code").hide();
    $("#cart_discount_code").prop("disabled", true);

    let code =  $("#cart_discount_code").val();
    $.ajax({
        url: "/payment/discount_code/",
        data: {"code": code},
        dataType: "json",
        success: function(res){

            $("#loader_discount_code").hide();
            $("#enter_discount_code").show();
            $("#cart_discount_code").prop("disabled", false);

            if(res.status == 'ok')
            {
                $("#discount_code_input").hide();
                $("#discount_code_info").show();
                $("#cart_discount_code_text").text(res.code_data.code);
                $("#cart_discount_code_value").text(res.cart_data[5]);
                $("#cart_subtotal").text(res.cart_data[2]);
                $("#cart_payment_total").text(res.cart_data[3]);
                $("#cart_tax").text(res.cart_data[1]);
            }
            else
            {
                call_notify("Alert",res.msg,"error");
            }
        }
    })
}

function removeDiscountCode()
{
    $.ajax({
        url: "/payment/remove_code/",
        dataType: "json",
        success: function(res){
            if(res.status == 'ok')
            {
                $("#discount_code_input").show();
                $("#discount_code_info").hide();
                $("#cart_discount_code_text").text("");
                $("#cart_discount_code_value").text(res.cart_data[5]);
                $("#cart_subtotal").text(res.cart_data[2]);
                $("#cart_payment_total").text(res.cart_data[3]);
                $("#cart_tax").text(res.cart_data[1]);
            }
            else
            {
                call_notify("Alert",res.msg,"error");
            }
        }
    })
}

(function($) {
    'use strict';

    /*/!**
    * Set date: Init date time picker for booking
    * Note: place here for deny error when load booking email form in first
    *!/
    Date.prototype.addHours = function(h) {
        this.setTime(this.getTime() + (h*60*60*1000));
        return this;
    }
    var today = new Date(currDateT);
    var future = new Date(currDateT);

    if(beforeTime == undefined || beforeTime == '' || beforeTime<0){
        beforeTime = 0;
    }
    var fourHoursLater =new Date().addHours(beforeTime);

    var set_date = parseInt(beforeDay) > 0 ? new Date(future.setDate(today.getDate()+beforeDay)) :  fourHoursLater;
        set_date = moment(set_date).format(dateFormatBooking);
        set_date = moment(set_date, dateFormatBooking).toDate();*/

    $('#datetimepicker_v1').datetimepicker({
        icons: {
            time: 'glyphicon glyphicon-time',
            date: 'glyphicon glyphicon-calendar',
            up: 'glyphicon glyphicon-chevron-up',
            down: 'glyphicon glyphicon-chevron-down',
            previous: 'fa fa-angle-double-left',
            next: 'fa fa-angle-double-right',
            today: 'glyphicon glyphicon-screenshot',
            clear: 'glyphicon glyphicon-trash',
            close: 'glyphicon glyphicon-remove'
        },
        widgetPositioning: {
            horizontal: 'auto',
            vertical: 'bottom'
        },
        format: dateFormatBooking,
        minDate: minDateBooking,
    });
    // End set date

    $('.inputDate').datetimepicker({
        icons: {
            time: 'glyphicon glyphicon-time',
            date: 'glyphicon glyphicon-calendar',
            up: 'glyphicon glyphicon-chevron-up',
            down: 'glyphicon glyphicon-chevron-down',
            previous: 'fa fa-angle-double-left',
            next: 'fa fa-angle-double-right',
            today: 'glyphicon glyphicon-screenshot',
            clear: 'glyphicon glyphicon-trash',
            close: 'glyphicon glyphicon-remove'
        },
        widgetPositioning: {
            horizontal: 'auto',
            vertical: 'bottom'
        },
        format: dateFormatBooking,
    });

    // check form
    $(document).ready(function(){
        $.ajax({
            type: "post",
            url: "/security/create",
            success: function(token)
            {
                $("form").each(function(){
                    $(this).prepend("<input type='hidden' name='token' value='"+token+"' />");
                });
            }
        });
    });
    
    // Mask Input
    var plholder = phoneFormat == "(000) 000-0000" ? "Phone (___) ___-____" : "Phone ____ ___ ____";
    $(".inputPhone").mask(phoneFormat, {placeholder: plholder});
    // End mask input

    // AUTO SELECT
    $("select.auto_select").each(function(){
      var val_default = $(this).attr("defaultvalue");
      $(this).find("option[value='"+val_default+"']").prop("selected",true);
    });
    // END AUTO SELECT

    // SERVICE PAGE
    // chuyển xuống click sau khi onload window
    // var group_id = $('input[name="group_id"]').val();
    //     group_id = $('ul.filter_service li[data-group-id="'+group_id+'"]');
    //     if ( group_id.length == 0 )
    //     {
    //         group_id = $("ul.filter_service li:first");
    //     }
    //     group_id.trigger("click");

    // // trigger click with event select change
    // $('select.filter_service_select').on('change', function(e){
    //     $('ul.filter_service li[data-group-id="'+$(this).val()+'"]').trigger('click');
    // });
    // END SERVICE PAGE

    // APPOIMENTS CHOOSE DATE
    $("#send_booking, #send_make_appointment").on("dp.change",".choose_date", function(){
        var typehtml = $(this).attr("typehtml");
        var date_choose = $(this).val();

        //data time
        pushHtmlTime(date_choose, typehtml);
    });
    // END APPOIMENTS CHOOSE DATE

    // BOOKING
    // choose service
    $("#surveyForm").on("change",".list_service", function(){
        var service_id = $(this).val();
        var list_staff = $(this).find("option:selected").attr("staff");

        if(service_id)
        {
            $(this).css("border-color","#ccc");
            $(this).parent().find('.form-tooltip-error').remove();
        }else
        {
            $(this).css("border-color","red");
            $(this).parent().append('<div class="form-tooltip-error" data-error-list=""><ul><li>'+$(this).data('validation-message')+'</li></ul></div>');
        }
        var obj = JSON.parse(list_staff);
        var option = '<option value="">Service Provider</option>';
        for(var x in obj)
        {
            option += `<option value="`+obj[x].id+`" urlimg="`+obj[x].image+`">`+obj[x].name+`</option>`;
        }

        $(this).parents(".item-booking").find(".list_staff").html(option);

        // Save form
        saveForm();
    });
    // end choose service

    // choose provider
    $("#surveyForm").on("change",".list_staff", function(){

        // Save form
        saveForm();
    });
    // end choose provider

    // note
    $("#surveyForm").on("change",".listperson, .note_type_list", function(){
        
        // Save form
        saveForm();
    });
    // end note

    // choose date
    $("#surveyForm").on("dp.change",".choose_date", function(){

        $(".btn_action").trigger('click');


    });
    // end choose date

    // button search
    $(".btn_action").click(function(e){

        var num = $(".list_service").length;
        var info_staff = [];
        var info_staff2 = [];
        var temp = {};
        var i = 0;
        var check = true;
        $(".list_service").each(function(){
            var checkval = $(this).val();
            if(checkval) 
            { 
                $(this).css("border-color","#ccc");
                $(this).parent().find('.form-tooltip-error').remove();
            }
            else
            {
                check = false;
                $(this).css("border-color","red");
                $(this).parent().append('<div class="form-tooltip-error" data-error-list=""><ul><li>'+$(this).data('validation-message')+'</li></ul></div>');
            }
            temp.price = $('option:selected', this).attr('price');
            temp.service = $('option:selected', this).text();
            info_staff.push(temp);
            temp = {};
            i++;
        });
      
        var j = 0;
        $(".list_staff").each(function(){
            var checkval = $(this).val();
            temp.image = $('option:selected', this).attr('urlimg');
            temp.name = checkval ? $('option:selected', this).text() : "Any person";
            info_staff2.push(temp);
            temp = {};
            j++;
        });

        if(check == true)
        {   
            $(".box_detail_info").show();
            $("#box_person").html("Loading ...");
            var html_person = "";
            var j = 0;
            for(var x in info_staff)
            {
                var image = typeof(info_staff2[x].image) === "undefined" ? "/public/library/global/no-photo.jpg" : info_staff2[x].image;
                html_person += `
                <div class="staff_service_v1 col-sm-6 col-md-6">
                  <div class="col-xs-4 staff-avatar">
                      <div title="staff avatar">
                          <img src="`+image+`" alt="`+info_staff2[x].name+`">
                      </div>
                  </div>
                  <div class="col-xs-8">
                      <h4>`+info_staff2[x].name+`</h4>
                      <p>`+info_staff[x].service+`</p>
                      <p>Price: `+info_staff[x].price+`</p>
                  </div>
              </div>
              `;
            }

            $("#box_person").html(html_person);


            var typehtml = $("#surveyForm .choose_date").attr("typehtml");
            var date_choose = $("#surveyForm .choose_date").val();
            pushHtmlTime(date_choose, typehtml);

            var scroll = $("#book-info").offset().top;
            $('body').animate({ scrollTop:  scroll}, 600,'swing');//.scrollTop( $("#book-info").offset().top );
        }
        else
        {
            return false;
        }
    });
    // end button search

    // confirm booking
    $("body").on("click",".open_booking", function(){

            // Check service
            var check = true;
            $(".list_service").each(function(){
                var checkval = $(this).val();
                if(checkval) 
                { 
                    $(this).css("border-color","#ccc");
                    $(this).parent().find('.form-tooltip-error').remove();
                }
                else
                {
                    check = false;
                    $(this).css("border-color","red");
                    $(this).parent().append('<div class="form-tooltip-error" data-error-list=""><ul><li>'+$(this).data('validation-message')+'</li></ul></div>');
                }
            });
            
            if(check == false)
            {
                return false;
            }

            var hours = $(this).attr("valhours");
            $.magnificPopup.open({
                type: 'inline',
                midClick: true,
                items: {
                  src: '#open_booking'
                },
                callbacks: {
                    beforeOpen: function() {
                        if($(window).width() < 700) {
                            this.st.focus = false;
                        } else {
                            this.st.focus = '#name';
                        }
                        $("input[name='booking_hours']").val(hours);
                    }
                }
            });
            return false;
        });

        $(".btn_cancel").click(function(){
            $.magnificPopup.close();
        });
    // end confirm booking
    // END BOOKING

    // Btn next payment
    $(".btn_cart_order").click(function(){
        var obj = $(this);
        // return false;
        $(".list_price").each(function(){
            var check_val = isNaN(parseFloat($(this).val())) ? 0 : parseFloat($(this).val());
            var max_val = parseFloat($(this).attr("max"));
            var min_val = parseFloat($(this).attr("min"));
            if(check_val > max_val || check_val < min_val)
            {
                $(this).css("border-color", "red");
                obj.removeAttr("href");
                return false;
            }
        })
    });

    /*Anchor link*/
    $('[href^="#"]').on("click", function (event) {
        let _h = $(this).attr('href');
        let _hsplit = _h.substr(1, _h.length);
        if ( _hsplit != 'open_booking' ) {
            event.preventDefault();
            scrollJumpto(_h, '.fixed-freeze-header');
        }
    });
})(jQuery);

$(document).ready(function(){

    // TRIGGER RESIZE
    // $(window).trigger('resize');

    // ADD CLASS ACTIVE MENU
    if ( typeof site == "undefined" || site == "idx" ) {
        site = "";
    }
    $('.desktop-nav > ul > li > a[href="/'+site+'"]').addClass('active');
    // END ADD CLASS ACTIVE MENU

    // VALIDATE FROM
    $("#send_newsletter").validate({
        submit: {
            settings: {
                button: ".btn_send_newsletter",
                inputContainer: '.form-group',
                errorListClass: 'form-tooltip-error',

            },
            callback: {
                onSubmit: function (node, formdata) 
                {
                    var url_send = $(node).attr("action");
                    var email = $("input[name='newsletter_email']").val();
                    var token = $("input[name='token']").val();
                    // console.log(url_send);
                    $.ajax({
                        type: "post",
                        url: url_send,
                        data: {newsletter_email: email, token: token},
                        success: function(html)
                        {
                            var obj = JSON.parse(html);
                            call_notify("Notification", obj.message, obj.status);
                            $("input[name='newsletter_email']").val("");

                            // An form
                            if(obj.status == "success")
                            {
                                $(".newsletter_inner").html('<h2 class="newsletter_tile" style="text-align:center">Thanks for subscribing!</h2>');
                            }
                        }
                    });
                }// End on before submit
            }
        }
    });

    $("#send_newsletter_signle").validate({
        submit: {
            settings: {
                button: ".btn_send_newsletter_signle",
                inputContainer: '.form-group',
                errorListClass: 'form-tooltip-error',

            },
            callback: {
                onSubmit: function (node, formdata) 
                {
                    var url_send = $(node).attr("action");
                    var email = $("#send_newsletter_signle input[name='newsletter_email']").val(); // formdata.newsletter_email;
                    var token = $("input[name='token']").val();
                    // console.log(url_send);
                    
                    $.ajax({
                        type: "post",
                        url: url_send,
                        data: {newsletter_email: email, token: token},
                        success: function(html)
                        {
                            var obj = JSON.parse(html);
                            call_notify("Notification", obj.message, obj.status);
                            $("input[name='newsletter_email']").val("");

                            // An form
                            if(obj.status == "success")
                            {
                                $(".newsletter_signle_inner").html('<h2 class="newsletter_tile" style="text-align:center">Thanks for subscribing!</h2>');
                            }
                        }
                    });
                }// End on before submit
            }
        }
    });

    $("#send_contact").validate({
        submit: {
            settings: {
                button: ".btn_contact",
                inputContainer: '.form-group',
                errorListClass: 'form-tooltip-error',
            }
        }
    });

    $("#form-login").validate({
        submit: {
            settings: {
                button: "[type='submit']",
                inputContainer: '.form-group',
                errorListClass: 'form-tooltip-error',
            }
        }
    });

    $("#form-forgot-pwd").validate({
        submit: {
            settings: {
                button: "[type='submit']",
                inputContainer: '.form-group',
                errorListClass: 'form-tooltip-error',
            }
        }
    });

    $("#form-register").validate({
        submit: {
            settings: {
                button: "[type='submit']",
                inputContainer: '.form-group',
                errorListClass: 'form-tooltip-error',
            }
        }
    });

    $("#form-changepwd").validate({
        submit: {
            settings: {
                button: "[type='submit']",
                inputContainer: '.form-group',
                errorListClass: 'form-tooltip-error',
            }
        }
    });
    $("#send_booking").validate({
        submit: {
            settings: {
                button: ".btn_make_appointment",
                inputContainer: '.input-box',
                errorListClass: 'form-tooltip-error',
            }
        }
    });
    $("#send_make_appointment").validate({
        submit: {
            settings: {
                button: ".btn_make_appointment",
                inputContainer: '.input-box',
                errorListClass: 'form-tooltip-error',
            }
        }
    });
    // END VALIDATE FROM

    // // GALLERY portfolio
    // $(window).load(function() {
    //     $('#portfolio-grid').magnificPopup({
    //         delegate: 'a.tt-lightbox',
    //         type: 'image',
    //         tLoading: 'Loading image #%curr%...',
    //         mainClass: 'mfp-fade',
    //         gallery: {
    //             enabled: true,
    //             navigateByImgClick: true,
    //             preload: [0,1] // Will preload 0 - before current, and 1 after the current image
    //         },
    //     });
    //     /* ======= shuffle js ======= */
    //     if ( $('#portfolio-grid').length > 0 && $('#portfolio-grid #portfolio-grid-using-shuffle').length > 0 ) {
            
    //         /* initialize shuffle plugin */
    //         var $grid = $('#portfolio-grid');

    //         $grid.shuffle({
    //             itemSelector: '.portfolio-item' // the selector for the items in the grid
    //         });

    //         /* reshuffle when user clicks a filter item */
    //         $('#filter li').on('click', function(e) {
    //             e.preventDefault();

    //             // set active class
    //             $('#filter li').removeClass('active');
    //             $('#filter li').removeClass('uk-active');
    //             $(this).addClass('active');
    //             $(this).addClass('uk-active');

    //             // get group name from clicked item
    //             var groupName = $(this).attr('data-group');

    //             // reshuffle grid
    //             $grid.shuffle('shuffle', groupName);
    //         });

    //         // trigger click with event select change
    //         $('select[name="filter_select"]').on('change', function(e){
    //             $('#filter li[data-group="'+$(this).val()+'"]').trigger('click');
    //         });
    //     }
    // });
    // // END GALLERY portfolio

    // GALLERY ISOTOPE
    if ( $('#our-portfolio-page').length > 0 && $('#our-portfolio-page.using-isotope-library').length > 0 ) {
        $('#our-portfolio-page').imagesLoaded(function() {
            $('#our-filters').on('click', 'li', function() {

                // filter items on button click
                var filterValue = $(this).attr('data-group');
                $('#our-portfolio-page').isotope({ filter: '.'+filterValue }); // EX: .zzz, .bbb for multi

                // change is-checked class on buttons
                $('#our-filters li').removeClass('active');
                $(this).addClass('active');
            });

            $('#our-portfolio-page').isotope({
                itemSelector: '.pro-item',
                filter: '*',
                transitionDuration: '0.7s',
                masonry: {
                    columnWidth: '.pro-item'
                },
                layoutMode : 'fitRows',
                animationEngine : 'best-available'
            });
        });

        // trigger click with event select change
        // $('select[name="filter_select"]').on('change', function(e){
        //     $('#our-filters li[data-group="'+$(this).val()+'"]').trigger('click');
        // });
    }
    // END GALLERY ISOTOPE

    // LOAD GALLERY AJAX
    $("#our-filters li").click(function(e){
        var id = $(this).data("id");
        e.preventDefault();

        // set active class
        $('#our-filters li').removeClass('active');
        $(this).addClass('active');

        if ( $('.load-gallery-with-ajax').length > 0 ) {
            getGalleryByCat(id);
        }
    });

    $("#our-filters li:first").trigger("click");

    // $("select[name='filter_select']").change(function(){
    //     $('#our-filters li[data-id="'+$(this).val()+'"]').trigger('click');
    // });
    // END LOAD GALLERY AJAX

    // SERVICE PAGE
    $(window).on('load', function(){
        var group_id = $('input[name="group_id"]').val();
            group_id = $('ul.filter_service li[data-group-id="'+group_id+'"]');
            if ( group_id.length == 0 )
            {
                group_id = $("ul.filter_service li:first");
            }
            group_id.trigger("click");

        // trigger click with event select change
        // $('select.filter_service_select').on('change', function(e){
        //     $('ul.filter_service li[data-group-id="'+$(this).val()+'"]').trigger('click');
        // });
    });
    // END SERVICE PAGE

    // SCROLL BUTTON SERVICE PAGE
    $(document).ready(function(){
        $.fn.is_on_screen = function(){

            var win = $(window);

            var viewport = {
                top : win.scrollTop(),
                left : win.scrollLeft()
            };
            viewport.right = viewport.left + win.width();
            viewport.bottom = viewport.top + win.height();

            var bounds = this.offset();
            if ( typeof bounds == 'undefined' ) {
                return false;
            }
            bounds.right = bounds.left + this.outerWidth();
            bounds.bottom = bounds.top + this.outerHeight();

            var header = 0; 
            if ( $('.box_service_top').length > 0 ) {
                header = $('.box_service_top').height();
            }

            if ( viewport.top >= ( bounds.top - header ) && viewport.top <= ( bounds.bottom - header ) ) {

                $('#all-item .item-botton').addClass('scroll_btn');
                $('#all-item .item-botton.scroll_btn').css('right', (viewport.right - bounds.right));

                if ( $('.box_service_top').length > 0 ) {
                    $('#all-item .item-botton.scroll_btn').css( 'top', header + 'px' );
                }
                
                return true;
            } else {
                return false;
            }
        };
        
        $(window).scroll(function(){ 
            if ( $('.content_service').is_on_screen() ) {

                var height = $('#all-item .item-botton').outerHeight();
                $('#all-item .item-botton').closest('li').css('height',height+'px');
                $('#all-item .item-botton-1').closest('li').css('height',height+'px');
            }else{

                $('#all-item .item-botton-1').closest('li').css('height','0px');
                $('#all-item .item-botton').removeClass('scroll_btn');
            }
        });
    });
    // END SCROLL BUTTON SERVICE PAGE

    // TRIGGER RESIZE SLIDER HOME
    // $(window).on('resize', function(){

    //     // Firing resize event only when resizing is finished
    //     clearTimeout(window.resizedFinishedSlider);
    //     window.resizedFinishedSlider = setTimeout(function(){
    //         $("#my-slider .sp-next-arrow").trigger('click');
    //     }, 250);
    // });
    // END TRIGGER RESIZE SLIDER HOME
});

$(document).ready(function(){
    // Load slider
    function initSliderHome( slider, sliderFixedHeight, sliderItem , selector ) {
      if ( $(sliderItem).length <= 0 ) {
            return;
        }

        // calculator width height slider
        $(selector).show();

        // width
        var width = $(selector).find('.fixed').width();
            width = (width > 0) ? width : $(selector).find('img').first().width();
            width = (width > 0) ? width : 1024;

        // Height
        var height = $(selector).find('.fixed').height();
        if( height <= 0 ) {
            var heights = [];
            $(selector).each(function() {
                heights.push($(this).find('img').height());
            });

            if ( heights.length > 0 ) { 
                height = Math.min.apply( Math, heights );
            }
        };
        height = (height > 0) ? height : 450; 

        $(selector).hide();
        // end calculator width height slider

        // init slider and remove other slider
        var autoHeight = $(selector).find('.fixed').height() ? false : true;
        var sliderInit = autoHeight ? slider : sliderFixedHeight;
        var sliderSpeed = $('#slider-option').data('speed');
            sliderSpeed = parseInt(sliderSpeed);
        $( sliderInit ).show().sliderPro({
            width: '100%', 
            height: height, 
            autoHeight: autoHeight,
            responsive: autoHeight, 
            centerImage: false, 
            autoScaleLayers: false, 
            
            arrows: true, 
            fade: true, 
            buttons: true, 
            thumbnailArrows: true, 

            autoplay: true,
            autoplayDelay: sliderSpeed > 0 ? sliderSpeed*1000 : 3000,
        });
        if( sliderInit == slider ) {
            $(sliderFixedHeight).remove();
        } else {
            $(slider).remove();
        }
        // end init slider
    }
    initSliderHome('#my-slider', '#my-slider-fixed-height','.sp-slides .sp-slide', '.slider-width-height');
    // End Load slider

    // Scroll Button Service Page
    $.fn.is_on_scroll = function( selector, header, boundSubtraction ) {
        // Not included margin, padding of window
        var win = $(window);
        var viewport = {
            top : win.scrollTop(),
            left : win.scrollLeft()
        };
        viewport.right = viewport.left + win.width();
        viewport.bottom = viewport.top + win.height();

        // Not included margin of this element: same container
        var bounds = this.offset();
        if ( typeof bounds == 'undefined' ) {return false;}

        // Check bound subtraction
        if ( $(boundSubtraction).length > 0 ) {
            var boundSubtractionHeight = $(boundSubtraction).outerHeight(true);// Included margin
            var boundMaxWidth = $(boundSubtraction).attr('bound-maxwidth');
                boundMaxWidth = (typeof boundMaxWidth == "undefined") ? 0 : boundMaxWidth;
            if ( boundMaxWidth > 0 && window.matchMedia('(max-width: ' + boundMaxWidth + 'px)').matches != true ) {
                boundSubtractionHeight = 0;
            }
            bounds.top = bounds.top + boundSubtractionHeight;
        }
        bounds.right = bounds.left + this.outerWidth();
        bounds.bottom = bounds.top + this.outerHeight();

        // Calculator header height
        var headerHeight = 0;
        if ( $(header).length > 0 ) {
            // Included margin of header
            var headerHeight = $(header).outerHeight(true);

            // Check fixed
            var checkFixed = $(header).attr('checkfixed');
                checkFixed = (typeof checkFixed == "undefined") ? 'true' : checkFixed;
            if ( checkFixed === "true" && $(header).css('position') != 'fixed' ){
                headerHeight = 0;
            }
            
            // Check max width
            var maxWidth = $(header).attr('header-maxwidth');
                maxWidth = ( typeof maxWidth == undefined ) ? 0 : maxWidth;
            if ( maxWidth > 0 && window.matchMedia('(max-width: ' + maxWidth + 'px)').matches != true ) {
                headerHeight = 0;
            }
        }

        if ( viewport.top >= ( bounds.top - headerHeight ) && viewport.top <= ( bounds.bottom - headerHeight ) ) {
            // place here for calculator right
            $(selector).css({
                'position': 'fixed', 
                'top': headerHeight + 'px', 
                'right': (viewport.right - bounds.right) + 'px',
                'z-index': '1001', 
            });
            return true;
        } else {
            $(selector).css({
                'position': '', 
            });
            return false;
        }
    }
    function isOnScroll ( container, selector, header, boundSubtraction ) {
        container = ( typeof container == "undefined" ) ?  "" : container;
        selector = ( typeof selector == "undefined" ) ?  "" : selector;
        header = ( typeof header == "undefined" ) ? "" : header;

        // Check exit element
        if ( ! $(container).length || ! $(selector).length ) {return false;}

        // Append element instead
        var injectSpace = $('<div />', { height: $(selector).outerHeight(true), class: 'injectSpace' + new Date().getTime() }).insertAfter($(selector));
            injectSpace.hide();

        // Check scroll
        $(window).scroll(function(){
            if ( $(container).is_on_scroll( selector, header, boundSubtraction ) ) {
                injectSpace.show();
            }else{
                injectSpace.hide();
            }
        });
    }
    isOnScroll ( '.pc-service-data1' , '.sb-service-data1', '.freeze-header');
    isOnScroll ( '.pc-service-data2' , '.sb-service-data2', '.freeze-header');
    // End Scroll Button Service Page

    // Jumpto
    function initEventScrollJumto(){
        $('.scroll_jumpto').click(function(event){
            event.preventDefault();

            // input elements
            var jumpto = $(this).data('jumpto');
            var headerfixed = $(this).data('headerfixed');
            var redirect = $(this).data('redirect');

            // call fumpto
            scrollJumpto ( jumpto, headerfixed, redirect );
        });
    }
    initEventScrollJumto();
    // End Jumpto

    // Animation scroll to service id
    $(window).load(function() {
        if ( $('.animation_sroll_jumpto .sroll_jumpto').length > 0 ) {
            scrollJumpto('#sci_' + $('input[name="group_id"]').val(), '.fixed-freeze-header');
        }
    });
    // End animation scroll to service id

    // Init gallery magic popup
    function initImageMagnificPopup(){
        var groups = {};
        $('.image-magnific-popup').each(function() {
            var id = $(this).attr('data-group');
            if( ! groups[id] ) 
            {
                groups[id] = [];
            }       
            groups[id].push(this);
        });
        $.each(groups, function() {
            $(this).magnificPopup({
                type: 'image',
                closeOnContentClick: true,
                closeBtnInside: true,
                gallery: { enabled:true }
            });
        });
    }
    initImageMagnificPopup();
    // End init gallery popup

    /*
    * Freeze
    * */
    $.fn.is_on_scroll1 = function() {
        /* Not included margin, padding of window */
        var win = $(window);
        var viewport = {
            top : win.scrollTop(),
            left : win.scrollLeft()
        };
        viewport.right = viewport.left + win.width();
        viewport.bottom = viewport.top + win.height();

        /* Not included margin of this element: same container */
        var bounds = this.offset();
        if ( typeof bounds == 'undefined' ) {return false;}
        bounds.right = bounds.left + this.outerWidth();
        bounds.bottom = bounds.top + this.outerHeight();

        if ( bounds.top >= viewport.top && bounds.bottom <= viewport.bottom ) {
            return true;
        } else {
            return false;
        }
    };

    /* Check scroll */
    var wrapFreezeHeaderObj = $('.wrap-freeze-header');
    var flagFreezeHeaderObj = $('.flag-freeze-header');
    if( wrapFreezeHeaderObj.find('[name="activeFreezeHeader"]').val() ){
        var insteadFreezeHeaderObj = $('<div class="instead-flag-freeze-header"></div>');
        insteadFreezeHeaderObj.insertBefore(flagFreezeHeaderObj);
        flagFreezeHeaderObj.addClass('fixed-freeze-header');
        $(window).scroll(function(){
            if( wrapFreezeHeaderObj.is_on_scroll1() ){
                flagFreezeHeaderObj.removeClass('freeze-header with-bg');
                insteadFreezeHeaderObj.height('0px');
            } else {
                insteadFreezeHeaderObj.height(flagFreezeHeaderObj.outerHeight()+'px');
                flagFreezeHeaderObj.addClass('freeze-header with-bg');
            }
        });
    }

    /*
    * MAX-LENGTH
    * */
    let maxlengthCnt = 0;
    $('[maxlength]').each(function () {
        let _self = $(this);
        if( parseInt(_self.attr('maxlength')) > 0 ){
            _self.attr('data-maxlength-error', 'maxlength-error-' + maxlengthCnt);
            _self.after('<div id="maxlength-error-'+ maxlengthCnt+'" class="error"></div>');
            _self.bind('input propertychange', function() {
                let _this = $(this);
                let maxLength = parseInt(_this.attr('maxlength'));
                let containerError = $('#'+_this.attr('data-maxlength-error'));
                let text = _this.val();
                if( text.length > maxLength-1 ){
                    _this.val(text.substring(0, maxLength-1));
                    containerError.html('Max Length allowed is '+ (maxLength -1)  + ' character');
                } else {
                    containerError.html('');
                }
            });
            maxlengthCnt++;
        }
    });

    /*
    * TESTIMONIALS
    * */
    $(".testimonials-list").owlCarousel({
        loop: true,
        nav: true,
        margin: 30,
        dots: false,
        items: 1,
        smartSpeed: 1000,
    });
});


function load_social(inputs) {
    if ( !inputs ) {
        console.log('load social missed inputs');
        return false;
    }

    /*calculator width*/
    let social_block_width = $('#social_block_width').width();
    social_block_width = Math.round(social_block_width);

    if (social_block_width > 450) {
        social_block_width = 450;
    }

    if ( social_block_width < 180 ){
        social_block_width = 180;
    }

    /*facebook fanpage*/
    if ( typeof inputs.facebook_embed != 'undefined' && inputs.facebook_embed ) {
        let social_block_height = Math.round(social_block_width * (parseInt(inputs.facebook_embed.height)/parseInt(inputs.facebook_embed.width)));
        let  social_url = '';
        if (!inputs.facebook_embed.likebox_enable) {
            social_url += 'https://www.facebook.com/plugins/page.php?';
            social_url += '&width=' + social_block_width + '&height=' + social_block_height;
            social_url += '&small_header='+(inputs.facebook_embed.small_header ? 'true' : 'false');
            social_url += '&tabs='+inputs.facebook_embed.tabs;
            social_url += '&show_facepile='+(inputs.facebook_embed.show_facepile ? 'true' : 'false');
            social_url += '&hide_cover=false&hide_cta=false&adapt_container_width=true';
        } else {
            social_url += 'https://www.facebook.com/plugins/likebox.php?';
            social_url += '&width=' + social_block_width + '&height=' + social_block_width; // If set height then error with likebox
            social_url += '&show_faces='+(inputs.facebook_embed.likebox_show_faces ? 'true' : 'false');
            social_url += '&stream='+(inputs.facebook_embed.likebox_stream ? 'true' : 'false');
            social_url += '&header=false';
        }
        social_url += '&href=' + encodeURIComponent(inputs.facebook_embed.id_fanpage);
        social_url += '&appId' + inputs.facebook_embed.appId;

        $('#fanpage_fb_container').html('<iframe style="overflow:hidden;max-height:' + social_block_height + 'px" title="Social fanpage" src="'+social_url+'" width="' + social_block_width + '" height="' + social_block_height + '" style="border:none;overflow:hidden" scrolling="no" frameborder="0" allowTransparency="true"></iframe>');
    }

    /*google fanpage*/
    if (typeof inputs.google_id_fanpage != 'undefined' && inputs.google_id_fanpage) {
        $('#fanpage_google_container').html('<div class="g-page" data-href="' + inputs.google_id_fanpage + '" data-width="' + social_block_width + '"></div><script src="https://apis.google.com/js/platform.js" async defer><\/script>');
    }

    /*twitter fanpage*/
    $('#fanpage_twitter_container').html(''); // clear content
    if (typeof inputs.twitter_id_fanpage != 'undefined' && inputs.twitter_id_fanpage) {
        inputs.twitter_id_fanpage = inputs.twitter_id_fanpage.split('/');
        for (let i = inputs.twitter_id_fanpage.length - 1; i >= 0; i -= 1) {
            if (inputs.twitter_id_fanpage[i] != '') {
                inputs.twitter_id_fanpage = inputs.twitter_id_fanpage[i];
                break;
            }
        }
        if (typeof twttr != 'undefined') {
            twttr.widgets.createTweet(inputs.twitter_id_fanpage, document.getElementById('fanpage_twitter_container'), {width: social_block_width});
        }
    }
}

$(document).ready(function () {
    /*
    * SOCIAL FAN PAGE
    * When resize then reload fanpage
    * Firing resize event only when resizing is finished
    */
    let socialInputs = {
        facebook_embed: facebook_embed,
        google_id_fanpage: google_id_fanpage,
        twitter_id_fanpage: twitter_id_fanpage,
    };
    $(window).load(function() {
        load_social(socialInputs);
        $(window).on('resize', function () {
            clearTimeout(window.resizedFinished);
            window.resizedFinished = setTimeout(function () {
                load_social(socialInputs);
            }, 250);
        });
    });
});

$(document).ready(function () {
    $(".homeService-carousel").owlCarousel({
        items: 4,
        itemsDesktop: [1199, 3],
        itemsTabletSmall: [768, 2],
        itemsMobile: [480, 1],

        navigation: true,
        navigationText: ["<i class='fa fa-angle-left'></i>", "<i class='fa fa-angle-right'></i>"],

        autoPlay: false,
        stopOnHover: true,
    });
});