/* By Theme */
function web_goTo(jumpToElement, redirectURL) {
    scrollJumpto(
        jumpToElement,
        window.matchMedia("(min-width: 992px)").matches
            ? ".fixed-freeze.desktop"
            : ".fixed-freeze.mobile",
        redirectURL
    );
}

/* Booking */
const maskLoadingHtml =
    '<div class="mask_booking" style="position: absolute; z-index: 2; height: 100%; width: 100%; top: 0; left: 0; background:rgba(0,0,0,0.5);text-align: center;"><i class="fa fa-spinner fa-pulse fa-3x fa-fw"></i></div>';
let webBookingFormNail = {
    formID: "form#formBooking",
    formConfirmID: "form#formBookingConfirm",
    boxBookingInfo: "#boxBookingInfo",
    boxServiceStaff: "#boxServiceStaff",
    boxDateTime: "#boxDateTime",
    popupBookingConfirm: "#popupBookingConfirm",

    itemIndex: 0,
    categories: {},
    services: {},
    staffs: {},
    getHoursProcess: {
        queue: "",
        process: "",
        sending: false,
    },
    saveFormProcess: {
        queue: 0,
        process: 0,
        sending: false,
    },

    init: function (
        categories_JsonString,
        services_JsonString,
        staffs_JsonString,
        dataForm_JsonString
    ) {
        let _this = this;

        // Events
        _this.setEvents();

        // Category & Service
        _this.setCategories(categories_JsonString);
        _this.setServices(services_JsonString);

        // Staffs
        _this.setStaffs(staffs_JsonString);

        // Data Form
        _this.setDataForm(dataForm_JsonString);
    },

    setEvents: function () {
        let _this = this;
        let formObj = $(_this.formID);
        let formConfirmObj = $(_this.formConfirmID);

        // Date time picker
        formObj
            .find(".booking_date")
            .mask(webFormat.dateFormat.replace(/[^\d\/]/g, "0"), {
                placeholder: webFormat.dateFormat,
            });
        formObj.find(".booking_date").datetimepicker({
            format: webFormat.dateFormat,
            minDate: webBooking.minDate,
            defaultDate: webBooking.minDate,
        });

        formObj.on("dp.change", ".booking_date", function () {
            _this.setOptionCategoryAndServices();
            _this.setOptionStaffs();

            _this.generalHTML(true);
            _this.saveForm();
        });

        formObj.on("change", ".booking_service", function () {
            let itemID = $(this).closest(".booking-item").attr("id");
            _this.setOptionStaff(itemID);

            _this.generalHTML(true);
            _this.saveForm();
        });

        formObj.on("change", ".booking_staff", function () {
            _this.generalHTML(true);
            _this.saveForm();
        });

        formObj.on("click", ".booking_item_add", function () {
            _this.addItem();
            _this.saveForm();
        });

        formObj.on("click", ".booking_item_remove", function () {
            let itemID = $(this).closest(".booking-item").attr("id");
            _this.removeItem(itemID);

            _this.generalHTML(true);
            _this.saveForm();
        });

        formObj.on("click", ".search_booking", function (e) {
            e.preventDefault();

            let _self = $(this);
            _self.attr("disabled", "disabled");

            _this.generalHTML();
            _this.saveForm();

            _self.removeAttr("disabled");
        });

        $(`${_this.boxBookingInfo}, ${_this.formID}`).on(
            "click",
            ".open_booking",
            function (e) {
                e.preventDefault();

                let _self = $(this);
                formObj
                    .find('[name="booking_hours"]')
                    .val(_self.attr("valhours"));

                if (_this.validate()) {
                    $.magnificPopup.open({
                        type: "inline",
                        midClick: true,
                        closeOnBgClick: false,
                        items: {
                            src: _this.popupBookingConfirm,
                        },
                    });
                }
            }
        );

        formConfirmObj.on("click", ".btn_cancel", function (e) {
            e.preventDefault();
            $.magnificPopup.close();
        });

        formConfirmObj.validate({
            submit: {
                settings: {
                    clear: "keypress",
                    display: "inline",
                    button: ".btn_confirm",
                    inputContainer: "group-select",
                    errorListClass: "form-tooltip-error",
                },
                callback: {
                    onSubmit: function (node, formData) {
                        formConfirmObj
                            .find(".btn_confirm")
                            .attr("disabled", "disabled");

                        let isValidate = true;
                        formConfirmObj.removeError();

                        if (webGlobal.enableRecaptcha) {
                            let g_recaptcha_response = $(
                                "#g-recaptcha-response"
                            ).val();
                            if (g_recaptcha_response) {
                                formObj
                                    .find('[name="g-recaptcha-response"]')
                                    .val(g_recaptcha_response);
                            } else {
                                isValidate = false;
                                call_notify(
                                    "Notification",
                                    "Recaptcha is invalid",
                                    "error"
                                );
                            }
                        }

                        if (isValidate) {
                            formObj
                                .find('[name="booking_name"]')
                                .val(formData["booking_name"]);
                            formObj
                                .find('[name="booking_phone"]')
                                .val(formData["booking_phone"]);
                            formObj
                                .find('[name="booking_email"]')
                                .val(formData["booking_email"]);
                            formObj
                                .find('[name="notelist"]')
                                .val(formData["notelist"]);
                            formObj
                                .find('[name="store_id"]')
                                .val(formData["choose_store"]);

                            formObj.submit();
                            return true;
                        } else {
                            formConfirmObj
                                .find(".btn_confirm")
                                .removeAttr("disabled");
                            return false;
                        }
                    },
                    onError: function (node, globalError) {
                        let error_msg = "";
                        for (let p in globalError) {
                            error_msg += globalError[p] + "<br>";
                        }
                        call_notify("Notification", error_msg, "error");
                    },
                },
            },
        });
    },

    setCategories: function (categories_JsonString) {
        let _this = this;

        _this.categories = JSON.parse(categories_JsonString);
    },

    setServices: function (services_JsonString) {
        let _this = this;

        _this.services = JSON.parse(services_JsonString);
    },

    setStaffs: function (staffs_JsonString) {
        let _this = this;

        _this.staffs = JSON.parse(staffs_JsonString);
    },

    setDataForm: function (dataForm_JsonString) {
        let _this = this;
        let formObj = $(_this.formID);

        let dataForm_Json = JSON.parse(dataForm_JsonString);

        // Date
        let date = dataForm_Json.booking_date
            ? dataForm_Json.booking_date
            : webBooking.minDate;
        formObj.find(".booking_date").val(date);

        // Services & staff
        let generalHtml = false;
        let serviceAndStaffs = dataForm_Json.service_staff
            ? dataForm_Json.service_staff
            : [","];
        let ItemCnt = 0;
        for (let x in serviceAndStaffs) {
            ItemCnt++;
            if (ItemCnt > 1) {
                _this.addItem();
            }

            let bookingItemObj = formObj.find(".booking-item").last();
            let bookingItemID = bookingItemObj.attr("id");
            let serviceAndStaff = serviceAndStaffs[x].split(",");

            let serviceID = serviceAndStaff[0];
            _this.setOptionCategoryAndService(bookingItemID, serviceID);

            let staffID = serviceAndStaff[1];
            _this.setOptionStaff(bookingItemID, staffID);

            if (serviceID) {
                generalHtml = true;
            }
        }

        if (generalHtml) {
            _this.generalHTML();
        }
    },

    setOptionCategoryAndServices: function () {
        let _this = this;
        let formObj = $(_this.formID);

        let bookingItemsObj = formObj.find(".booking-item");
        bookingItemsObj.each(function () {
            let bookingItemID = $(this).attr("id");
            _this.setOptionCategoryAndService(bookingItemID);
        });
    },

    setOptionCategoryAndService: function (itemID, serviceID) {
        let _this = this;
        let formObj = $(_this.formID);
        let dateObj = formObj.find(".booking_date");
        let bookingItemObj = formObj.find(`#${itemID}`);
        let serviceObj = bookingItemObj.find(".booking_service");

        let maskLoadingObj = $(maskLoadingHtml);
        serviceObj.parent().append(maskLoadingObj);

        let serviceIDSelected = serviceID ? serviceID : serviceObj.val();
        let dayName = _this.getDayOfWeek(dateObj.val(), true);

        let html = `<option value="">${webForm["booking_service_placeholder"]}</option>`;
        for (let x in _this.categories) {
            let category = _this.categories[x];
            let services = _this.categories[x].services;

            let optionServiceHtml = "";
            for (let y in services) {
                let service = _this.getService(services[y], dayName);
                if (service) {
                    let selected =
                        serviceIDSelected * 1 === service.id * 1
                            ? "selected"
                            : "";
                    optionServiceHtml +=
                        `<option ${selected} value="${service.id}">${service.name}` +
                        (service.price ? ` (${service.price})` : "") +
                        `</option>`;
                }
            }

            if (optionServiceHtml) {
                html += `<optgroup label="${category.name}">${optionServiceHtml}</optgroup>`;
            }
        }
        serviceObj.html(html);
        maskLoadingObj.remove();
    },

    getService: function (serviceID, dayName) {
        let _this = this;

        let service = null;
        if (_this.services && _this.services[serviceID]) {
            if (dayName && _this.services[serviceID].schedule === true) {
                for (let x in _this.services[serviceID].scheduleDay) {
                    if (_this.services[serviceID].scheduleDay[x] === dayName) {
                        service = _this.services[serviceID];
                        break;
                    }
                }
            } else {
                service = _this.services[serviceID];
            }
        }

        return service;
    },

    setOptionStaffs: function () {
        let _this = this;
        let formObj = $(_this.formID);

        let bookingItemsObj = formObj.find(".booking-item");
        bookingItemsObj.each(function () {
            let itemID = $(this).attr("id");
            _this.setOptionStaff(itemID);
        });
    },

    setOptionStaff: function (itemID, staffID) {
        let _this = this;
        let formObj = $(_this.formID);
        let dateObj = formObj.find(".booking_date");
        let bookingItemObj = formObj.find(`#${itemID}`);
        let serviceObj = bookingItemObj.find(".booking_service");
        let staffObj = bookingItemObj.find(".booking_staff");

        let maskLoadingObj = $(maskLoadingHtml);
        staffObj.parent().append(maskLoadingObj);

        let serviceID = serviceObj.val();
        let service = _this.getService(serviceID);
        let staffIDs = service ? service.staffs : null;
        let staffIDSelected = staffID ? staffID : staffObj.val();
        let dayName = _this.getDayOfWeek(dateObj.val(), true);

        let html = `<option value="">${webForm["booking_technician_placeholder"]}</option>`;
        for (let x in staffIDs) {
            let staff = _this.getStaff(staffIDs[x], dayName);
            if (staff) {
                let selected =
                    staffIDSelected * 1 === staff.id * 1 ? "selected" : "";
                html +=
                    `<option ${selected} value="${staff.id}">${staff.name}` +
                    (staff.note ? ` (${staff.note})` : "") +
                    `</option>`;
            }
        }
        staffObj.html(html);
        maskLoadingObj.remove();
    },

    getStaff: function (staffID, dayName) {
        let _this = this;

        let staff = null;
        if (_this.staffs && _this.staffs[staffID]) {
            if (dayName && _this.staffs[staffID].schedule === true) {
                for (let x in _this.staffs[staffID].scheduleDay) {
                    if (_this.staffs[staffID].scheduleDay[x] === dayName) {
                        staff = _this.staffs[staffID];
                        break;
                    }
                }
            } else {
                staff = _this.staffs[staffID];
            }
        }

        return staff;
    },

    addItem: function () {
        let _this = this;
        let formObj = $(_this.formID);
        let bookingItemObj = formObj.find(".booking-item").last();

        _this.itemIndex++;
        let html =
            `
            <div class="row booking-service-staff booking-item is-more" id="bookingItem_${_this.itemIndex}">
                <div class="remove-services pointer booking_item_remove"><i class="fa fa-minus-circle"></i></div>
                ` +
            bookingItemObj.html() +
            `
            </div>
        `;
        let htmlObj = $(html);
        htmlObj.find(".booking_service").val("");
        htmlObj.find(".booking_staff").val("");

        bookingItemObj.after(htmlObj);
    },

    removeItem: function (itemID) {
        let _this = this;
        let formObj = $(_this.formID);
        let bookingItemObj = formObj.find(`#${itemID}`);

        bookingItemObj.remove();
    },

    validate: function () {
        let _this = this;

        let formObj = $(_this.formID);
        let dateObj = formObj.find(".booking_date");
        let servicesObj = formObj.find(".booking_service");
        let staffsObj = formObj.find(".booking_staff");

        let isValidate = true;
        clearAllValidateMsg(formObj);

        // Date
        if (!dateObj.val()) {
            isValidate = false;
            showValidateMsg(dateObj, webForm["booking_date_err"]);
        }

        // Services
        servicesObj.each(function () {
            let _self = $(this);
            if (!_self.val()) {
                isValidate = false;
                showValidateMsg(_self, webForm["booking_service_err"]);
            }
        });

        // Staffs
        if (webBooking.requiredTechnician) {
            staffsObj.each(function () {
                let _self = $(this);
                if (!_self.val()) {
                    isValidate = false;
                    showValidateMsg(_self, webForm["booking_technician_err"]);
                }
            });
        }

        if (isValidate) {
            return true;
        } else {
            let errorElement = formObj.find(".error").first();
            web_goTo(errorElement.length > 0 ? errorElement : formObj);
            return false;
        }
    },

    generalHTML: function (validate) {
        let _this = this;

        if (!webBooking.requiredHour) {
            return false;
        }

        if (validate || _this.validate()) {
            $(_this.boxBookingInfo).show();
            _this.generalHTMLServiceStaff();
            _this.generalHTMLDateTime();
        }
    },

    generalHTMLServiceStaff: function () {
        let _this = this;

        if (!webBooking.requiredHour) {
            return false;
        }

        let formObj = $(_this.formID);
        let servicesObj = formObj.find(".booking_service");
        let staffsObj = formObj.find(".booking_staff");

        let maskLoadingObj = $(maskLoadingHtml);
        $(_this.boxBookingInfo).append(maskLoadingObj);

        let services = [];
        servicesObj.each(function () {
            let _self = $(this).find("option:selected");

            let service = {
                name: "N/A",
                price: "N/A",
            };

            let serviceItem = _this.getService(_self.val());
            if (serviceItem) {
                service.name = serviceItem.name;
                service.price = serviceItem.price;
            }

            services.push(service);
        });

        let staffs = [];
        staffsObj.each(function () {
            let _self = $(this).find("option:selected");

            let staff = {
                name: webForm["any_person"],
                image: webGlobal.noPhoto,
                imageIsNo: true,
            };

            let staffItem = _this.getStaff(_self.val());
            if (staffItem) {
                staff.name = staffItem.name;
                staff.image = staffItem.image;
                staff.imageIsNo = staffItem.imageIsNo;
            }

            staffs.push(staff);
        });

        let html = "";
        for (let x in services) {
            html +=
                `
            <div class="service-staff">
                <div class="service-staff-avatar ` +
                (staffs[x].imageIsNo ? "no-photo" : "") +
                `">
                    <img class="img-responsive" src="${staffs[x].image}" alt="${staffs[x].name}">
                </div>
                <div class="service-staff-info">
                    <h5>${staffs[x].name}</h5>
                    <p>${services[x].name}</p>
                    <p>${webForm["price"]}: ${services[x].price}</p>
                </div>
            </div>
            `;
        }
        $(_this.boxServiceStaff).html(html);

        maskLoadingObj.remove();
    },

    generalHTMLDateTime: function () {
        let _this = this;

        if (!webBooking.requiredHour) {
            return false;
        }

        _this.getHoursProcess.queue++;
        if (_this.getHoursProcess.sending === false) {
            _this.getHoursProcess.process = _this.getHoursProcess.queue;

            let formObj = $(_this.formID);
            let dateObj = formObj.find(".booking_date");
            let servicesObj = formObj.find(".booking_service");
            let staffsObj = formObj.find(".booking_staff");

            let maskLoadingObj = $(maskLoadingHtml);
            $(_this.boxBookingInfo).append(maskLoadingObj);

            let date = dateObj.val();
            if (date) {
                let serviceIDs = [];
                servicesObj.each(function () {
                    let _self = $(this).find("option:selected");
                    serviceIDs.push(_self.val() * 1);
                });

                let staffIDs = [];
                staffsObj.each(function () {
                    let _self = $(this).find("option:selected");
                    staffIDs.push(_self.val() * 1);
                });

                $.ajax({
                    type: "post",
                    url: "/book/get_hours",
                    data: {
                        input_date: date,
                        input_services: serviceIDs,
                        input_staffs: staffIDs,
                    },
                    timeout: 15000, // millisecond
                    beforeSend: function () {
                        _this.getHoursProcess.sending = true;
                    },
                    success: function (response) {
                        let responseObj = JSON.parse(response);
                        let boxDateTimeObj = $(_this.boxDateTime);

                        boxDateTimeObj
                            .find("#dateInfo")
                            .html(_this.convertDate(responseObj.date));

                        boxDateTimeObj
                            .find("#timeAMHtml")
                            .html(responseObj.htmlMorning);
                        boxDateTimeObj
                            .find("#timeAMNote")
                            .html(
                                responseObj.checkmorning
                                    ? ""
                                    : webForm["booking_hours_expired"]
                            );

                        boxDateTimeObj
                            .find("#timePMHtml")
                            .html(responseObj.htmlAfternoon);
                        boxDateTimeObj
                            .find("#timePMNote")
                            .html(
                                responseObj.checkafternoon
                                    ? ""
                                    : webForm["booking_hours_expired"]
                            );
                    },
                    complete: function (jqXHR, textStatus) {
                        _this.getHoursProcess.sending = false;
                        if (
                            _this.getHoursProcess.queue !==
                            _this.getHoursProcess.process
                        ) {
                            _this.generalHTMLDateTime();
                        } else if (textStatus === "timeout") {
                            call_notify(
                                "Notification",
                                webForm["booking_get_hours_timeout"],
                                "error"
                            );
                        }

                        maskLoadingObj.remove();
                    },
                });
            } else {
                $(_this.boxDateTime).find("#dateInfo").html("N/A");
                maskLoadingObj.remove();
            }
        }
    },

    saveForm: function () {
        let _this = this;

        _this.saveFormProcess.queue++;
        if (_this.saveFormProcess.sending === false) {
            _this.saveFormProcess.process = _this.saveFormProcess.queue;
            $.ajax({
                type: "post",
                url: "/book/saveform",
                data: $(_this.formID).serialize(),
                beforeSend: function () {
                    _this.saveFormProcess.sending = true;
                },
                complete: function () {
                    _this.saveFormProcess.sending = false;
                    if (
                        _this.saveFormProcess.queue !==
                        _this.saveFormProcess.process
                    ) {
                        _this.saveForm();
                    }
                },
            });
        }
    },

    convertDate: function (input) {
        let listDate = input.split("/");
        let splitDate = webFormat.datePosition.split(",");
        let newDate =
            listDate[splitDate[2]] +
            "/" +
            listDate[splitDate[1]] +
            "/" +
            listDate[splitDate[0]];
        newDate += "";

        let date = new Date(newDate);
        let months = [
            webForm["jan"],
            webForm["feb"],
            webForm["mar"],
            webForm["apr"],
            webForm["may"],
            webForm["jun"],
            webForm["jul"],
            webForm["aug"],
            webForm["sep"],
            webForm["oct"],
            webForm["nov"],
            webForm["dec"],
        ];
        let days = [
            webForm["sunday"],
            webForm["monday"],
            webForm["tuesday"],
            webForm["wednesday"],
            webForm["thursday"],
            webForm["friday"],
            webForm["saturday"],
        ];

        return (
            days[date.getDay()] +
            ", " +
            months[date.getMonth()] +
            "-" +
            date.getDate() +
            "-" +
            date.getFullYear()
        );
    },

    getDayOfWeek: function (input, type) {
        // ISO-8601
        let dayOfWeek_Obj = {
            1: "monday",
            2: "tuesday",
            3: "wednesday",
            4: "thursday",
            5: "friday",
            6: "saturday",
            7: "sunday",
        };

        let dayNumber = moment(input).day();
        dayNumber *= 1;
        if (dayNumber === 0) {
            dayNumber = 7;
        }

        return type ? dayOfWeek_Obj[dayNumber] : dayNumber;
    },
};

let webBookingFormRestaurant = {
    formID: "form#formBooking",
    formConfirmID: "form#formBookingConfirm",
    boxBookingInfo: "#boxBookingInfo",
    boxServicePersonNumber: "#boxServicePersonNumber",
    boxDateTime: "#boxDateTime",
    popupBookingConfirm: "#popupBookingConfirm",

    itemIndex: 0,
    categories: {},
    services: {},
    getHoursProcess: {
        queue: "",
        process: "",
        sending: false,
    },
    saveFormProcess: {
        queue: 0,
        process: 0,
        sending: false,
    },

    init: function (
        categories_JsonString,
        services_JsonString,
        dataForm_JsonString
    ) {
        let _this = this;

        // Events
        _this.setEvents();

        // Category & Service
        _this.setCategories(categories_JsonString);
        _this.setServices(services_JsonString);

        // Data Form
        _this.setDataForm(dataForm_JsonString);
    },

    setEvents: function () {
        let _this = this;
        let formObj = $(_this.formID);
        let formConfirmObj = $(_this.formConfirmID);

        // Date time picker
        formObj
            .find(".booking_date")
            .mask(webFormat.dateFormat.replace(/[^\d\/]/g, "0"), {
                placeholder: webFormat.dateFormat,
            });
        formObj.find(".booking_date").datetimepicker({
            format: webFormat.dateFormat,
            minDate: webBooking.minDate,
            defaultDate: webBooking.minDate,
        });

        formObj.on("dp.change", ".booking_date", function () {
            _this.setOptionCategoryAndServices();

            _this.generalHTML(true);
            _this.saveForm();
        });

        formObj.on("change", ".booking_service", function () {
            _this.generalHTML(true);
            _this.saveForm();
        });

        formObj.on("change", ".booking_person_number", function () {
            _this.generalHTML(true);
            _this.saveForm();
        });

        formObj.on("change", ".booking_notelist", function () {
            _this.generalHTML(true);
            _this.saveForm();
        });

        formObj.on("click", ".booking_item_add", function () {
            _this.addItem();
            _this.saveForm();
        });

        formObj.on("click", ".booking_item_remove", function () {
            let itemID = $(this).closest(".booking-item").attr("id");
            _this.removeItem(itemID);

            _this.generalHTML(true);
            _this.saveForm();
        });

        formObj.on("click", ".search_booking", function (e) {
            e.preventDefault();

            let _self = $(this);
            _self.attr("disabled", "disabled");

            _this.generalHTML();
            _this.saveForm();

            _self.removeAttr("disabled");
        });

        $(`${_this.boxBookingInfo}, ${_this.formID}`).on(
            "click",
            ".open_booking",
            function (e) {
                e.preventDefault();

                let _self = $(this);
                formObj
                    .find('[name="booking_hours"]')
                    .val(_self.attr("valhours"));

                if (_this.validate()) {
                    $.magnificPopup.open({
                        type: "inline",
                        midClick: true,
                        closeOnBgClick: false,
                        items: {
                            src: _this.popupBookingConfirm,
                        },
                    });
                }
            }
        );

        formConfirmObj.on("click", ".btn_cancel", function (e) {
            e.preventDefault();
            $.magnificPopup.close();
        });

        formConfirmObj.validate({
            submit: {
                settings: {
                    clear: "keypress",
                    display: "inline",
                    button: ".btn_confirm",
                    inputContainer: "group-select",
                    errorListClass: "form-tooltip-error",
                },
                callback: {
                    onSubmit: function (node, formData) {
                        formConfirmObj
                            .find(".btn_confirm")
                            .attr("disabled", "disabled");

                        let isValidate = true;
                        formConfirmObj.removeError();

                        if (webGlobal.enableRecaptcha) {
                            let g_recaptcha_response = $(
                                "#g-recaptcha-response"
                            ).val();
                            if (g_recaptcha_response) {
                                formObj
                                    .find('[name="g-recaptcha-response"]')
                                    .val(g_recaptcha_response);
                            } else {
                                isValidate = false;
                                call_notify(
                                    "Notification",
                                    "Recaptcha is invalid",
                                    "error"
                                );
                            }
                        }

                        if (isValidate) {
                            formObj
                                .find('[name="booking_name"]')
                                .val(formData["booking_name"]);
                            formObj
                                .find('[name="booking_phone"]')
                                .val(formData["booking_phone"]);
                            formObj
                                .find('[name="booking_email"]')
                                .val(formData["booking_email"]);
                            formObj
                                .find('[name="notelist"]')
                                .val(formData["notelist"]);
                            formObj
                                .find('[name="store_id"]')
                                .val(formData["choose_store"]);

                            formObj.submit();
                            return true;
                        } else {
                            formConfirmObj
                                .find(".btn_confirm")
                                .removeAttr("disabled");
                            return false;
                        }
                    },
                    onError: function (node, globalError) {
                        let error_msg = "";
                        for (let p in globalError) {
                            error_msg += globalError[p] + "<br>";
                        }
                        call_notify("Notification", error_msg, "error");
                    },
                },
            },
        });
    },

    setCategories: function (categories_JsonString) {
        let _this = this;

        _this.categories = JSON.parse(categories_JsonString);
    },

    setServices: function (services_JsonString) {
        let _this = this;

        _this.services = JSON.parse(services_JsonString);
    },

    setDataForm: function (dataForm_JsonString) {
        let _this = this;
        let formObj = $(_this.formID);

        let dataForm_Json = JSON.parse(dataForm_JsonString);

        // Date
        let date = dataForm_Json.booking_date
            ? dataForm_Json.booking_date
            : webBooking.minDate;
        formObj.find(".booking_date").val(date);

        // Services & person/ number & note
        let generalHtml = false;
        let services = dataForm_Json.service_staff
            ? dataForm_Json.service_staff
            : [","];
        let personNumbers = dataForm_Json.listperson
            ? dataForm_Json.listperson
            : [""];
        let notes = dataForm_Json.notelist ? dataForm_Json.notelist : [""];
        let ItemCnt = 0;
        for (let x in services) {
            ItemCnt++;
            if (ItemCnt > 1) {
                _this.addItem();
            }

            let bookingItemObj = formObj.find(".booking-item").last();
            let bookingItemID = bookingItemObj.attr("id");

            let serviceID = services[x].split(",")[0];
            _this.setOptionCategoryAndService(bookingItemID, serviceID);

            let personNumber = personNumbers[x];
            _this.setValuePersonNumber(bookingItemID, personNumber);

            let note = notes[x];
            _this.setValueNote(bookingItemID, note);

            if (serviceID) {
                generalHtml = true;
            }
        }

        if (generalHtml) {
            _this.generalHTML();
        }
    },

    setOptionCategoryAndServices: function () {
        let _this = this;
        let formObj = $(_this.formID);

        let bookingItemsObj = formObj.find(".booking-item");
        bookingItemsObj.each(function () {
            let bookingItemID = $(this).attr("id");
            _this.setOptionCategoryAndService(bookingItemID);
        });
    },

    setOptionCategoryAndService: function (itemID, serviceID) {
        let _this = this;
        let formObj = $(_this.formID);
        let dateObj = formObj.find(".booking_date");
        let bookingItemObj = formObj.find(`#${itemID}`);
        let serviceObj = bookingItemObj.find(".booking_service");

        let maskLoadingObj = $(maskLoadingHtml);
        serviceObj.parent().append(maskLoadingObj);

        let serviceIDSelected = serviceID ? serviceID : serviceObj.val();
        let dayName = _this.getDayOfWeek(dateObj.val(), true);

        let html = `<option value="">${webForm["booking_service_placeholder"]}</option>`;
        for (let x in _this.categories) {
            let category = _this.categories[x];
            let services = _this.categories[x].services;

            let optionServiceHtml = "";
            for (let y in services) {
                let service = _this.getService(services[y], dayName);
                if (service) {
                    let selected =
                        serviceIDSelected * 1 === service.id * 1
                            ? "selected"
                            : "";
                    optionServiceHtml +=
                        `<option ${selected} value="${service.id}">${service.name}` +
                        (service.price ? ` (${service.price})` : "") +
                        `</option>`;
                }
            }

            if (optionServiceHtml) {
                html += `<optgroup label="${category.name}">${optionServiceHtml}</optgroup>`;
            }
        }
        serviceObj.html(html);
        maskLoadingObj.remove();
    },

    getService: function (serviceID, dayName) {
        let _this = this;

        let service = null;
        if (_this.services && _this.services[serviceID]) {
            if (dayName && _this.services[serviceID].schedule === true) {
                for (let x in _this.services[serviceID].scheduleDay) {
                    if (_this.services[serviceID].scheduleDay[x] === dayName) {
                        service = _this.services[serviceID];
                        break;
                    }
                }
            } else {
                service = _this.services[serviceID];
            }
        }

        return service;
    },

    setValuePersonNumber: function (itemID, personNumber) {
        let _this = this;
        let formObj = $(_this.formID);
        let bookingItemObj = formObj.find(`#${itemID}`);
        let personNumberObj = bookingItemObj.find(".booking_person_number");

        let maskLoadingObj = $(maskLoadingHtml);
        personNumberObj.parent().append(maskLoadingObj);

        personNumberObj.val(personNumber);
        maskLoadingObj.remove();
    },

    setValueNote: function (itemID, note) {
        let _this = this;
        let formObj = $(_this.formID);
        let bookingItemObj = formObj.find(`#${itemID}`);
        let noteObj = bookingItemObj.find(".booking_notelist");

        let maskLoadingObj = $(maskLoadingHtml);
        noteObj.parent().append(maskLoadingObj);

        noteObj.val(note);
        maskLoadingObj.remove();
    },

    addItem: function () {
        let _this = this;
        let formObj = $(_this.formID);
        let bookingItemObj = formObj.find(".booking-item").last();

        _this.itemIndex++;
        let html =
            `
            <div class="row booking-service-staff booking-item is-more" id="bookingItem_${_this.itemIndex}">
                <div class="remove-services pointer booking_item_remove"><i class="fa fa-minus-circle"></i></div>
                ` +
            bookingItemObj.html() +
            `
            </div>
        `;
        let htmlObj = $(html);
        htmlObj.find(".booking_service").val("");
        htmlObj.find(".booking_person_number").val(1);

        bookingItemObj.after(htmlObj);
    },

    removeItem: function (itemID) {
        let _this = this;
        let formObj = $(_this.formID);
        let bookingItemObj = formObj.find(`#${itemID}`);

        bookingItemObj.remove();
    },

    validate: function () {
        let _this = this;

        let formObj = $(_this.formID);
        let dateObj = formObj.find(".booking_date");
        let servicesObj = formObj.find(".booking_service");

        let isValidate = true;
        clearAllValidateMsg(formObj);

        // Date
        if (!dateObj.val()) {
            isValidate = false;
            showValidateMsg(dateObj, webForm["booking_date_err"]);
        }

        // Services
        servicesObj.each(function () {
            let _self = $(this);
            if (!_self.val()) {
                isValidate = false;
                showValidateMsg(_self, webForm["booking_service_err"]);
            }
        });

        if (isValidate) {
            return true;
        } else {
            let errorElement = formObj.find(".error").first();
            web_goTo(errorElement.length > 0 ? errorElement : formObj);
            return false;
        }
    },

    generalHTML: function (validate) {
        let _this = this;

        if (!webBooking.requiredHour) {
            return false;
        }

        if (validate || _this.validate()) {
            $(_this.boxBookingInfo).show();
            _this.generalHTMLServicePersonNumberNote();
            _this.generalHTMLDateTime();
        }
    },

    generalHTMLServicePersonNumberNote: function () {
        let _this = this;

        if (!webBooking.requiredHour) {
            return false;
        }

        let formObj = $(_this.formID);
        let servicesObj = formObj.find(".booking_service");
        let personNumbersObj = formObj.find(".booking_person_number");
        let notesObj = formObj.find(".booking_notelist");

        let maskLoadingObj = $(maskLoadingHtml);
        $(_this.boxBookingInfo).append(maskLoadingObj);

        let services = [];
        servicesObj.each(function () {
            let _self = $(this).find("option:selected");

            let service = {
                name: "N/A",
                price: "N/A",
                unit: "part",
            };

            let serviceItem = _this.getService(_self.val());
            if (serviceItem) {
                service.name = serviceItem.name;
                service.price = serviceItem.price
                    ? `(${serviceItem.price})`
                    : serviceItem.price;
                service.unit = serviceItem.unit;
            }

            services.push(service);
        });

        let personNumbers = [];
        personNumbersObj.each(function () {
            let _self = $(this);

            let personNumber = _self.val();
            if (personNumber < 1) {
                personNumber = 1;
            }

            personNumbers.push(personNumber);
        });

        let notes = [];
        notesObj.each(function () {
            let _self = $(this);

            let note = _self.val();
            notes.push(note);
        });

        let html = "";
        for (let x in services) {
            html += `
            <div class="service-staff">
                <div class="service-staff-info">
                    <h5>${personNumbers[x]} ${services[x].unit}</h5>
                    <p>${services[x].name} ${services[x].price}</p>
                    <p>${notes[x]}</p>
                </div>
            </div>
            `;
        }
        $(_this.boxServicePersonNumber).html(html);

        maskLoadingObj.remove();
    },

    generalHTMLDateTime: function () {
        let _this = this;

        if (!webBooking.requiredHour) {
            return false;
        }

        _this.getHoursProcess.queue++;
        if (_this.getHoursProcess.sending === false) {
            _this.getHoursProcess.process = _this.getHoursProcess.queue;

            let formObj = $(_this.formID);
            let dateObj = formObj.find(".booking_date");
            let servicesObj = formObj.find(".booking_service");

            let maskLoadingObj = $(maskLoadingHtml);
            $(_this.boxBookingInfo).append(maskLoadingObj);

            let date = dateObj.val();
            if (date) {
                let serviceIDs = [];
                servicesObj.each(function () {
                    let _self = $(this).find("option:selected");
                    serviceIDs.push(_self.val() * 1);
                });

                $.ajax({
                    type: "post",
                    url: "/book/get_hours",
                    data: { input_date: date, input_services: serviceIDs },
                    timeout: 15000, // millisecond
                    beforeSend: function () {
                        _this.getHoursProcess.sending = true;
                    },
                    success: function (response) {
                        let responseObj = JSON.parse(response);
                        let boxDateTimeObj = $(_this.boxDateTime);

                        boxDateTimeObj
                            .find("#dateInfo")
                            .html(_this.convertDate(responseObj.date));

                        boxDateTimeObj
                            .find("#timeAMHtml")
                            .html(responseObj.htmlMorning);
                        boxDateTimeObj
                            .find("#timeAMNote")
                            .html(
                                responseObj.checkmorning
                                    ? ""
                                    : webForm["booking_hours_expired"]
                            );

                        boxDateTimeObj
                            .find("#timePMHtml")
                            .html(responseObj.htmlAfternoon);
                        boxDateTimeObj
                            .find("#timePMNote")
                            .html(
                                responseObj.checkafternoon
                                    ? ""
                                    : webForm["booking_hours_expired"]
                            );
                    },
                    complete: function (jqXHR, textStatus) {
                        _this.getHoursProcess.sending = false;
                        if (
                            _this.getHoursProcess.queue !==
                            _this.getHoursProcess.process
                        ) {
                            _this.generalHTMLDateTime();
                        } else if (textStatus === "timeout") {
                            call_notify(
                                "Notification",
                                webForm["booking_get_hours_timeout"],
                                "error"
                            );
                        }

                        maskLoadingObj.remove();
                    },
                });
            } else {
                $(_this.boxDateTime).find("#dateInfo").html("N/A");
                maskLoadingObj.remove();
            }
        }
    },

    saveForm: function () {
        let _this = this;

        _this.saveFormProcess.queue++;
        if (_this.saveFormProcess.sending === false) {
            _this.saveFormProcess.process = _this.saveFormProcess.queue;
            $.ajax({
                type: "post",
                url: "/book/saveform",
                data: $(_this.formID).serialize(),
                beforeSend: function () {
                    _this.saveFormProcess.sending = true;
                },
                complete: function () {
                    _this.saveFormProcess.sending = false;
                    if (
                        _this.saveFormProcess.queue !==
                        _this.saveFormProcess.process
                    ) {
                        _this.saveForm();
                    }
                },
            });
        }
    },

    convertDate: function (input) {
        let listDate = input.split("/");
        let splitDate = webFormat.datePosition.split(",");
        let newDate =
            listDate[splitDate[2]] +
            "/" +
            listDate[splitDate[1]] +
            "/" +
            listDate[splitDate[0]];
        newDate += "";

        let date = new Date(newDate);
        let months = [
            webForm["jan"],
            webForm["feb"],
            webForm["mar"],
            webForm["apr"],
            webForm["may"],
            webForm["jun"],
            webForm["jul"],
            webForm["aug"],
            webForm["sep"],
            webForm["oct"],
            webForm["nov"],
            webForm["dec"],
        ];
        let days = [
            webForm["sunday"],
            webForm["monday"],
            webForm["tuesday"],
            webForm["wednesday"],
            webForm["thursday"],
            webForm["friday"],
            webForm["saturday"],
        ];

        return (
            days[date.getDay()] +
            ", " +
            months[date.getMonth()] +
            "-" +
            date.getDate() +
            "-" +
            date.getFullYear()
        );
    },

    getDayOfWeek: function (input, type) {
        // ISO-8601
        let dayOfWeek_Obj = {
            1: "monday",
            2: "tuesday",
            3: "wednesday",
            4: "thursday",
            5: "friday",
            6: "saturday",
            7: "sunday",
        };

        let dayNumber = moment(input).day();
        dayNumber *= 1;
        if (dayNumber === 0) {
            dayNumber = 7;
        }

        return type ? dayOfWeek_Obj[dayNumber] : dayNumber;
    },
};

let webBookingForm = webBooking.isRestaurant
    ? webBookingFormRestaurant
    : webBookingFormNail;

/* GiftCard */
let webPaymentForm = {
    formPaymentNew: true,
    formPaymentID: "form#formPayment",
    boxRecipient: "#boxRecipient",
    boxPaymentItems: "#boxPaymentItems",
    boxCart: "#boxCart",
    boxPreview: "#boxPreview",

    init: function (isPaymentNew, dataForm_JsonString) {
        let _self = this;

        // Payment is new
        _self.formPaymentNew =
            typeof isPaymentNew !== "undefined" && isPaymentNew;

        // Events
        _self.setEvents();

        // Data Form
        _self.setDataForm(dataForm_JsonString);
    },

    setEvents: function () {
        let _self = this;
        let formPaymentObj = $(_self.formPaymentID);
        let boxPaymentItemsObj = $(_self.boxPaymentItems);
        let boxRecipientObj = $(_self.boxRecipient);

        // Validate
        formPaymentObj.validate({
            submit: {
                settings: {
                    clear: "keypress",
                    display: "inline",
                    button: "[type='submit']",
                    inputContainer: "form-group",
                    errorListClass: "form-tooltip-error",
                },
                callback: {
                    onSubmit: function (node, formdata) {
                        /* Deny duplicate click && Clears all form errors */
                        formPaymentObj
                            .find(".btn_payment")
                            .prop("disabled", true)
                            .attr("disabled", "disabled")
                            .addClass("disabled");
                        formPaymentObj.removeError();

                        if (_self.formPaymentNew) {
                            let isValidate = true;

                            /* Check price */
                            let item = {};
                            let customPriceObj = formPaymentObj.find(
                                '[name="custom_price"]'
                            );

                            item.price = parseFloat(formdata["custom_price"]);
                            item.price =
                                !isNaN(item.price) && item.price > 0
                                    ? item.price
                                    : 0;

                            item.price_min = parseFloat(
                                customPriceObj.attr("data-price_min")
                            );
                            item.price_min =
                                !isNaN(item.price_min) && item.price_min > 0
                                    ? item.price_min
                                    : 0;

                            item.price_max = parseFloat(
                                customPriceObj.attr("data-price_max")
                            );
                            item.price_max =
                                !isNaN(item.price_max) && item.price_max > 0
                                    ? item.price_max
                                    : 0;

                            if (
                                !(
                                    item.price_min <= item.price &&
                                    item.price <= item.price_max
                                )
                            ) {
                                isValidate = false;
                                formPaymentObj.addError({
                                    custom_price: `Accept Amount From ${item.price_min} to ${item.price_max}`,
                                });
                            }

                            if (!isValidate) {
                                formPaymentObj
                                    .find(".btn_payment")
                                    .prop("disabled", false)
                                    .removeAttr("disabled")
                                    .removeClass("disabled");
                                let errorElement = formPaymentObj
                                    .find(".error")
                                    .first();
                                scrollJumpto(
                                    errorElement.length > 0
                                        ? errorElement
                                        : formPaymentObj,
                                    window.matchMedia("(min-width: 992px)")
                                        .matches
                                        ? ".fixed-freeze.desktop"
                                        : ".fixed-freeze.mobile"
                                );
                                return false;
                            }
                        }

                        waitingDialog.show("Please wait a moment ...");
                        node[0].submit();
                    },
                    onError: function () {
                        let errorElement = formPaymentObj
                            .find(".error")
                            .first();
                        scrollJumpto(
                            errorElement.length > 0
                                ? errorElement
                                : formPaymentObj,
                            window.matchMedia("(min-width: 992px)").matches
                                ? ".fixed-freeze.desktop"
                                : ".fixed-freeze.mobile"
                        );
                    },
                },
            },
        });

        // Events
        formPaymentObj.on("click", 'input[name="send_to_friend"]', function () {
            let _this = $(this);
            let sendToFriend = _this.is(":checked");
            if (sendToFriend) {
                _this.prop("checked", true);
                boxRecipientObj.show();
            } else {
                _this.prop("checked", false);
                boxRecipientObj.hide();
            }
        });

        if (_self.formPaymentNew) {
            boxPaymentItemsObj.on("click", ".paymentItem", function (e) {
                e.preventDefault();
                let _this = $(this);

                /* Init active */
                boxPaymentItemsObj.find(".paymentItem").removeClass("active");
                _this.addClass("active");

                /* Get Item */
                let item = {};
                item.id = parseInt(_this.attr("data-id"));
                item.id = !isNaN(item.id) && item.id > 0 ? item.id : 0;

                item.name = _this.attr("data-name");
                item.image = _this.attr("data-image");

                item.price = parseFloat(_this.attr("data-price"));
                item.price =
                    !isNaN(item.price) && item.price > 0 ? item.price : 0;

                item.price_custom = parseInt(_this.attr("data-price_custom"));
                item.price_custom =
                    !isNaN(item.price_custom) && item.price_custom > 0;

                item.price_min = parseFloat(_this.attr("data-price_min"));
                item.price_min =
                    !isNaN(item.price_min) && item.price_min > 0
                        ? item.price_min
                        : 0;

                item.price_max = parseFloat(_this.attr("data-price_max"));
                item.price_max =
                    !isNaN(item.price_max) && item.price_max > 0
                        ? item.price_max
                        : 0;

                item.quantity = 1;

                /* Payment */
                formPaymentObj.removeError();

                /* Price && Note*/
                let customPriceObj = formPaymentObj.find(
                    '[name="custom_price"]'
                );
                let cus_price = parseFloat(customPriceObj.val());
                cus_price = !isNaN(cus_price) && cus_price > 0 ? cus_price : 0;
                if (
                    cus_price > 0 &&
                    item.price_custom &&
                    item.price_min <= cus_price &&
                    cus_price <= item.price_max
                ) {
                    item.price = cus_price;
                }
                customPriceObj
                    .val(item.price)
                    .attr({
                        "data-id": item.id,
                        "data-price_custom": item.price_custom ? 1 : 0,
                        "data-price_min": item.price_min,
                        "data-price_max": item.price_max,
                    })
                    .prop("readonly", !item.price_custom);

                let customPriceNoteObj =
                    formPaymentObj.find("#custom_price_note");
                customPriceNoteObj.html(
                    `<p><b>Note:</b> Accept Amount From ${item.price_min} to ${item.price_max}</p>`
                );
                if (item.price_custom) {
                    customPriceNoteObj.show();
                } else {
                    customPriceNoteObj.hide();
                }

                /* Quantity */
                let customQuantityObj = formPaymentObj.find(
                    '[name="custom_quantity"]'
                );
                let cus_quantity = parseInt(customQuantityObj.val());
                cus_quantity =
                    !isNaN(cus_quantity) && cus_quantity > 0 ? cus_quantity : 0;
                if (cus_quantity > 0) {
                    item.quantity = cus_quantity;
                }
                customQuantityObj.val(item.quantity).attr("data-id", item.id);

                /* Preview */
                _self.changePreviewInfo({
                    name: item.name,
                    image: item.image,
                    amount: "N/A",
                    quantity: "N/A",
                });

                /* Save Cart */
                _self.changeCart(item.id, item.quantity, item.price);
            });

            formPaymentObj.on(
                "keyup change",
                'input[name="custom_price"]',
                function () {
                    let _this = $(this);

                    /* Deny duplicate click */
                    formPaymentObj
                        .find(".btn_payment")
                        .prop("disabled", true)
                        .attr("disabled", "disabled")
                        .addClass("disabled");

                    /* Get Item */
                    let item = {};
                    item.id = parseInt(_this.attr("data-id"));
                    item.id = !isNaN(item.id) && item.id > 0 ? item.id : 0;

                    item.price = _this.val();
                    item.price =
                        !isNaN(item.price) && item.price > 0 ? item.price : 0;

                    item.price_custom = parseInt(
                        _this.attr("data-price_custom")
                    );
                    item.price_custom =
                        !isNaN(item.price_custom) && item.price_custom > 0;

                    item.price_min = parseFloat(_this.attr("data-price_min"));
                    item.price_min =
                        !isNaN(item.price_min) && item.price_min > 0
                            ? item.price_min
                            : 0;

                    item.price_max = parseFloat(_this.attr("data-price_max"));
                    item.price_max =
                        !isNaN(item.price_max) && item.price_max > 0
                            ? item.price_max
                            : 0;

                    /* Save Cart */
                    if (
                        item.price_custom &&
                        item.price_min <= item.price &&
                        item.price <= item.price_max
                    ) {
                        formPaymentObj.removeError(["custom_price"]);
                        _self.changePrice(item.id, item.price);
                    } else {
                        formPaymentObj.addError({
                            custom_price: `Accept Amount From ${item.price_min} to ${item.price_max}`,
                        });
                    }
                }
            );

            formPaymentObj.on(
                "keyup change",
                'input[name="custom_quantity"]',
                function () {
                    let _this = $(this);

                    /* Deny duplicate click */
                    formPaymentObj
                        .find(".btn_payment")
                        .prop("disabled", true)
                        .attr("disabled", "disabled")
                        .addClass("disabled");

                    // Get Item
                    let item = {};
                    item.id = parseInt(_this.attr("data-id"));
                    item.id = !isNaN(item.id) && item.id > 0 ? item.id : 0;

                    item.quantity = parseInt(_this.val());

                    /* Save Cart */
                    if (item.quantity > 0) {
                        formPaymentObj.removeError(["custom_quantity"]);
                        _self.changeQuantity(item.id, item.quantity);
                    } else {
                        formPaymentObj.addError({
                            custom_quantity: "Accept quantity greater than 0",
                        });
                    }
                }
            );

            formPaymentObj.on(
                "keyup",
                'input[name="ship_full_name"]',
                function () {
                    let _this = $(this);
                    _self.changePreviewInfo({
                        from: _this.val(),
                    });
                }
            );

            formPaymentObj.on(
                "keyup",
                'input[name="recipient_name"]',
                function () {
                    let _this = $(this);
                    _self.changePreviewInfo({
                        to: _this.val(),
                    });
                }
            );
        }
    },

    setDataForm: function (dataForm_JsonString) {
        let _self = this;
        let formPaymentObj = $(_self.formPaymentID);
        let boxPaymentItemsObj = $(_self.boxPaymentItems);
        let boxRecipientObj = $(_self.boxRecipient);

        let sendToFriend = formPaymentObj
            .find('input[name="send_to_friend"]')
            .is(":checked");
        if (sendToFriend) {
            boxRecipientObj.show();
        } else {
            boxRecipientObj.hide();
        }

        if (_self.formPaymentNew) {
            let data = JSON.parse(dataForm_JsonString);

            // Default Price & quantity
            if (data.cus_price > 0) {
                let customPriceObj = formPaymentObj.find(
                    '[name="custom_price"]'
                );
                customPriceObj.val(data.cus_price);
            }

            if (data.cus_quantity > 0) {
                let customQuantityObj = formPaymentObj.find(
                    '[name="custom_quantity"]'
                );
                customQuantityObj.val(data.cus_quantity);
            }

            // Choose Item
            let paymentItemObj = boxPaymentItemsObj.find(
                `.paymentItem[data-id="${data.id}"]`
            );
            if (paymentItemObj.length <= 0) {
                paymentItemObj = boxPaymentItemsObj
                    .find(".paymentItem")
                    .first();
            }
            paymentItemObj.trigger("click");
        }
    },

    changeCart: function (id, quantity, cus_price) {
        let _self = this;
        let formPaymentObj = $(_self.formPaymentID);

        /* Reset Preview*/
        _self.changePreviewInfo({
            amount: "N/A",
            quantity: "N/A",
        });

        $.ajax({
            type: "post",
            url: "/cart/changecart",
            data: { id: id, quantity: quantity, cus_price: cus_price },
            dataType: "Json",
            success: function (obj) {
                obj.id = id;

                let customQuantityObj = formPaymentObj.find(
                    '[name="custom_quantity"]'
                );
                customQuantityObj.val(obj.quantity);

                let customPriceObj = formPaymentObj.find(
                    '[name="custom_price"]'
                );
                customPriceObj.val(obj.price_new);

                _self.changeCartInfo(obj);
                _self.changePreviewInfo(obj.preview);
                _self.validateNext();
            },
            error: function () {
                call_notify(
                    "Notification",
                    "Error when process request",
                    "error"
                );
            },
        });
    },

    changePrice: function (id, cus_price) {
        let _self = this;

        /* Reset Preview*/
        _self.changePreviewInfo({
            amount: "N/A",
            quantity: "N/A",
        });

        $.ajax({
            type: "post",
            url: "/cart/updateprice",
            data: { id: id, cus_price: cus_price },
            dataType: "Json",
            success: function (obj) {
                if (obj.status === "success") {
                    _self.changeCartInfo({
                        subtotal: obj.cart_data[2],
                        discount: obj.cart_data[5],
                        tax: obj.cart_data[1],
                        amount: obj.cart_data[3],
                        id: id,
                    });
                    _self.changePreviewInfo(obj.preview);
                    _self.validateNext();
                } else {
                    call_notify("Notification", obj.msg, "error");
                }
            },
            error: function () {
                call_notify(
                    "Notification",
                    "Error when process request",
                    "error"
                );
            },
        });
    },

    changeQuantity: function (id, cus_quantity) {
        let _self = this;

        /* Reset Preview*/
        _self.changePreviewInfo({
            amount: "N/A",
            quantity: "N/A",
        });

        $.ajax({
            type: "post",
            url: "/cart/updatequantity",
            data: { id: id, quantity: cus_quantity },
            dataType: "Json",
            success: function (obj) {
                _self.changeCartInfo({
                    subtotal: obj.cart_data[2],
                    discount: obj.cart_data[5],
                    tax: obj.cart_data[1],
                    amount: obj.cart_data[3],
                    id: id,
                });
                _self.changePreviewInfo(obj.preview);
                _self.validateNext();
            },
            error: function () {
                call_notify(
                    "Notification",
                    "Error when process request",
                    "error"
                );
            },
        });
    },

    changePreviewInfo: function (data) {
        let _self = this;
        let boxCartObj = $(_self.boxCart);
        let boxPreviewObj = $(_self.boxPreview);

        if (typeof data.name !== "undefined") {
            boxCartObj.find("#cart_name").html(data.name);
        }

        if (typeof data.image !== "undefined") {
            boxCartObj.find("#cart_image img").attr("src", data.image);
            boxPreviewObj.find("#preview_image img").attr("src", data.image);
        }

        if (typeof data.amount !== "undefined") {
            boxPreviewObj.find("#preview_amount").html(data.amount);
        }

        if (typeof data.quantity !== "undefined") {
            boxCartObj.find("#cart_quantity").html(data.quantity);
            boxPreviewObj.find("#preview_quantity").html(data.quantity);
        }

        if (typeof data.from !== "undefined") {
            boxPreviewObj.find("#preview_from").html(data.from);
        }

        if (typeof data.to !== "undefined") {
            boxPreviewObj.find("#preview_to").html(data.to);
        }

        if (typeof data.message !== "undefined") {
            boxPreviewObj.find("#preview_message").html(data.message);
        }
    },

    changeCartInfo: function (data) {
        let _self = this;
        let boxCartObj = $(_self.boxCart);
        let formPaymentObj = $(_self.formPaymentID);

        boxCartObj.find("#cart_subtotal").html(data.subtotal);
        boxCartObj.find("#cart_discount").html(data.discount);
        boxCartObj.find("#cart_tax").html(data.tax);
        boxCartObj.find("#cart_total").html(data.amount);
        if (data.id) {
            formPaymentObj.find("#custom_price").attr("data-id", data.id);
        }
    },

    validateNext: function () {
        let _self = this;
        let formPaymentObj = $(_self.formPaymentID);
        if (formPaymentObj.find("input.error").length > 0) {
            formPaymentObj
                .find(".btn_payment")
                .prop("disabled", true)
                .attr("disabled", "disabled")
                .addClass("disabled");
        } else {
            formPaymentObj
                .find(".btn_payment")
                .prop("disabled", false)
                .removeAttr("disabled")
                .removeClass("disabled");
        }
    },
};

let webCartForm = {
    formCartID: "form#formCart",
    boxCart: "#boxCart",

    init: function () {
        let _self = this;

        // Events
        _self.setEvents();
    },

    setEvents: function () {
        let _self = this;
        let formCartObj = $(_self.formCartID);
        let boxCartObj = $(_self.boxCart);

        // Validate
        formCartObj.validate({
            submit: {
                settings: {
                    clear: "keypress",
                    display: "inline",
                    button: "[type='submit']",
                    inputContainer: "form-group",
                    errorListClass: "form-tooltip-error",
                },
            },
        });

        // Events
        formCartObj.on("keyup change", "input.custom_price", function () {
            let _this = $(this);

            /* Deny click to payment*/
            boxCartObj
                .find(".btn_cart")
                .prop("disabled", true)
                .attr("disabled", "disabled")
                .addClass("disabled");

            /* Get Item */
            let item = {};
            item.id = parseInt(_this.attr("data-id"));
            item.id = !isNaN(item.id) && item.id > 0 ? item.id : 0;

            item.price = _this.val();
            item.price = !isNaN(item.price) && item.price > 0 ? item.price : 0;

            item.price_custom = parseInt(_this.attr("data-price_custom"));
            item.price_custom =
                !isNaN(item.price_custom) && item.price_custom > 0;

            item.price_min = parseFloat(_this.attr("data-price_min"));
            item.price_min =
                !isNaN(item.price_min) && item.price_min > 0
                    ? item.price_min
                    : 0;

            item.price_max = parseFloat(_this.attr("data-price_max"));
            item.price_max =
                !isNaN(item.price_max) && item.price_max > 0
                    ? item.price_max
                    : 0;

            /* Save Cart */
            if (item.price_min <= item.price && item.price <= item.price_max) {
                formCartObj.removeError([`custom_price_${item.id}`]);
                _self.changePrice(item.id, item.price);
            } else {
                let error = {};
                error[
                    `custom_price_${item.id}`
                ] = `Accept Amount From ${item.price_min} to ${item.price_max}`;
                formCartObj.addError(error);
            }
        });

        formCartObj.on("keyup change", "input.custom_quantity", function () {
            let _this = $(this);

            /* Deny click to payment*/
            boxCartObj
                .find(".btn_cart")
                .prop("disabled", true)
                .attr("disabled", "disabled")
                .addClass("disabled");

            // Get Item
            let item = {};
            item.id = parseInt(_this.attr("data-id"));
            item.id = !isNaN(item.id) && item.id > 0 ? item.id : 0;

            item.quantity = parseInt(_this.val());

            /* Save Cart */
            if (item.quantity > 0) {
                formCartObj.removeError([`custom_quantity_${item.id}`]);
                _self.changeQuantity(item.id, item.quantity);
            } else {
                let error = {};
                error[`custom_quantity_${item.id}`] =
                    "Accept quantity greater than 0";
                formCartObj.addError(error);
            }
        });

        formCartObj.on("click", ".delete_cart", function () {
            let _this = $(this);

            /* Deny click to payment*/
            boxCartObj
                .find(".btn_cart")
                .prop("disabled", true)
                .attr("disabled", "disabled")
                .addClass("disabled");

            // Get Item
            let item = {};
            item.id = parseInt(_this.attr("data-id"));
            item.id = !isNaN(item.id) && item.id > 0 ? item.id : 0;

            /* Save Cart */
            _self.deleteCart(item.id);
        });
    },

    changePrice: function (id, cus_price) {
        let _self = this;
        let formCartObj = $(_self.formCartID);

        $.ajax({
            type: "post",
            url: "/cart/updateprice",
            data: { id: id, cus_price: cus_price },
            dataType: "Json",
            success: function (obj) {
                if (obj.status === "success") {
                    formCartObj
                        .find(`#total_change_${id}`)
                        .html(obj.total_show);
                    _self.changeCartInfo({
                        subtotal: obj.cart_data[2],
                        discount: obj.cart_data[5],
                        tax: obj.cart_data[1],
                        amount: obj.cart_data[3],
                        id: id,
                    });
                    _self.validateNext();
                } else {
                    call_notify("Notification", obj.msg, "error");
                }
            },
            error: function () {
                call_notify(
                    "Notification",
                    "Error when process request",
                    "error"
                );
            },
        });
    },

    changeQuantity: function (id, cus_quantity) {
        let _self = this;
        let formCartObj = $(_self.formCartID);

        $.ajax({
            type: "post",
            url: "/cart/updatequantity",
            data: { id: id, quantity: cus_quantity },
            dataType: "Json",
            success: function (obj) {
                formCartObj.find(`#total_change_${id}`).html(obj.total_show);
                _self.changeCartInfo({
                    subtotal: obj.cart_data[2],
                    discount: obj.cart_data[5],
                    tax: obj.cart_data[1],
                    amount: obj.cart_data[3],
                    id: id,
                });
                _self.validateNext();
            },
            error: function () {
                call_notify(
                    "Notification",
                    "Error when process request",
                    "error"
                );
            },
        });
    },

    deleteCart(id) {
        let _self = this;
        let formCartObj = $(_self.formCartID);

        $.ajax({
            type: "post",
            url: "/cart/deletecart",
            data: { id: id },
            dataType: "Json",
            success: function (obj) {
                formCartObj.find(`#cart_item_${id}`).remove();

                let items = formCartObj.find(".cart_item");
                if (items.length <= 0) {
                    formCartObj
                        .find("#cart_items")
                        .html(
                            '<tr><td colspan="5"><div class="price-row-col"><b>Cart empty...</b></div></td></tr>'
                        );
                }

                _self.changeCartInfo({
                    subtotal: obj.cart_data[2],
                    discount: obj.cart_data[5],
                    tax: obj.cart_data[1],
                    amount: obj.cart_data[3],
                    id: id,
                });
                _self.validateNext();
            },
        });
    },

    changeCartInfo: function (data) {
        let _self = this;
        let boxCartObj = $(_self.boxCart);

        boxCartObj.find("#cart_subtotal").html(data.subtotal);
        boxCartObj.find("#cart_discount").html(data.discount);
        boxCartObj.find("#cart_tax").html(data.tax);
        boxCartObj.find("#cart_total").html(data.amount);
    },

    validateNext: function () {
        let _self = this;
        let formCartObj = $(_self.formCartID);
        let boxCartObj = $(_self.boxCart);
        if (formCartObj.find("input.error").length > 0) {
            boxCartObj
                .find(".btn_cart")
                .prop("disabled", true)
                .attr("disabled", "disabled")
                .addClass("disabled");
        } else {
            boxCartObj
                .find(".btn_cart")
                .prop("disabled", false)
                .removeAttr("disabled")
                .removeClass("disabled");
        }
    },
};

/* Gallery */
function initGalleryTab(elementTab, elementContent) {
    let objTab = $(elementTab);

    objTab.on("click", ".tab", function () {
        let _this = $(this);
        let id = _this.attr("data-id");

        // Class active
        objTab.find(".tab").removeClass("active");
        _this.addClass("active");

        getGalleryByCat(id, 1, elementContent);
    });

    // First load
    objTab.find(".tab").first().trigger("click");
}

function getGalleryByCat(cat_id, page, elementContent) {
    cat_id = cat_id ? cat_id : 0;
    page = page ? page : 0;

    let objContent = $(elementContent);
    let mask_loading_obj = $(maskLoadingHtml);

    $.ajax({
        type: "post",
        url: "/gallery/getlistbycat",
        beforeSend: function () {
            objContent.append(mask_loading_obj);
        },
        data: { cat_id: cat_id, page: page, blockId: elementContent },
        success: function (response) {
            // let obj = JSON.parse(response);
            let obj = response;
            let url = "http://127.0.0.1:8000";
            let html = `<div class="m-gallery-box-wrap">`;

            if (obj.length > 0) {
                html += `<div class="row">`;
                for (var x in obj) {
                    html += `
                    <div class="col-xs-6 col-sm-6 col-md-4">
                        <div itemprop="url" class="pointer m-magnific-popup" data-group="gallery-${cat_id}" 
                             title="${obj[x].name}" href="${url}/storage/photos/${obj[x].url}">
                            <div class="m-gallery-box">
                                <div class="m-image-bg" style="background-image: url('${url}/storage/photos/${obj[x].url}');">
                                    <img itemprop="image" src="${url}/storage/photos/${obj[x].url}" alt="${obj[x].name}">
                                </div>
                            </div>
                        </div>
                    </div>
                    `;
                }
                html += `</div>`;
            } else {
                html = "Not found gallery item in this category.";
            }
            html += `</div>`;

            objContent.find(".listing").html(html);
            objContent.find(".paging").html(obj.paging_ajax);
            initImageMagnificPopup(".m-magnific-popup");
        },
        complete: function () {
            mask_loading_obj.remove();
        },
    });
}

(function ($) {
    "use strict";

    /*VALIDATE*/
    $.ajax({
        type: "get",
        url: "/security/create",
        success: function (token) {
            $("form").each(function () {
                $(this).find('input[name="token"]').remove();
                $(this).prepend(
                    "<input type='hidden' name='token' value='" + token + "' />"
                );
            });
        },
    });
    $("#send_contact").validate({
        submit: {
            settings: {
                button: ".btn_contact",
                inputContainer: ".form-group",
                errorListClass: "form-tooltip-error",
            },
        },
    });
})(jQuery);

$(document).ready(function () {
    /*ACTIVE MENU*/
    setActiveMenu(
        ".header-nav-desktop ul",
        ".menu_mobile_v1 ul, .menu_mobile_v2 ul",
        { site: webGlobal.site, site_act: webGlobal.siteAct }
    );

    /*MENU MOBILE*/
    initMenuMobile(".mobile-menu nav", ".menu_mobile_v1", "991");

    /*SLIDER*/
    initSliderHome(
        "#my-slider",
        "#my-slider-fixed-height",
        ".sp-slides .sp-slide",
        ".slider-width-height"
    );

    /*FREEZE HEADER*/
    let activeFreezeHeader = $('[name="activeFreezeHeader"]').val();
    if (activeFreezeHeader == 1 || activeFreezeHeader == 3) {
        isFreezeHeader(".wrap-freeze-header", ".flag-freeze-header");
    }

    if (activeFreezeHeader == 1 || activeFreezeHeader == 2) {
        isFreezeHeader(
            ".wrap-freeze-header-mobile",
            ".flag-freeze-header-mobile",
            "mobile"
        );
    }

    /*REMOVE BUTTON SERVER IF FREEZE FOOTER*/
    if ($(".freeze-footer:visible").length > 0) {
        $(".service-btn-group").remove();
    }

    /*GALLERY TAB*/
    initGalleryTab("#category_tab", "#gallery_content");

    /*SCROLL SERVICE*/
    $(window).load(function () {
        if ($(".animation_sroll_to_service").length > 0) {
            scrollJumpto(
                "#sci_" + $('input[name="group_id"]').val(),
                window.matchMedia("(min-width: 992px)").matches
                    ? ".fixed-freeze.desktop"
                    : ".fixed-freeze.mobile"
            );
        }
    });

    /*SCROLL BUTTON SERVICE*/
    if ($(".animation_sroll_button").length > 0) {
        isOnScroll(
            ".service-container",
            ".service-container .service-btn-group",
            window.matchMedia("(min-width: 992px)").matches
                ? ".fixed-freeze.desktop"
                : ".fixed-freeze.mobile"
        );
    }
});
