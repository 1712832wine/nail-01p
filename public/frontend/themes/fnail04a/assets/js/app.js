(function ($) {
    "use strict";
    /*-------------------------------------------
     02. wow js active
     --------------------------------------------- */
    // new WOW().init();

    $(document).ready(function () {
        $("body").on("click", ".btn-call", function () {
            $(this).prop("disabled", true);
            var obj = $(this);
            $.ajax({
                type: "post",
                url: "/home/count_click",
                success: function (response) {
                    // console.log(response);
                    $(obj).prop("disabled", false);
                },
            });
        });
    });

    /*-------------------------------------------
     03. Sticky Header
     --------------------------------------------- */
    $(window).on("scroll", function () {
        var scroll = $(window).scrollTop();
        if (scroll < 245) {
            $("#sticky-header-with-topbar").removeClass("scroll-header");
        } else {
            $("#sticky-header-with-topbar").addClass("scroll-header");
        }
    });

    /*--------------------------------
     /*-------------------------------------------
     05. Portfolio  Masonry (width)
     --------------------------------------------- */
    $(window).load(function () {
        $(".list-gallery").magnificPopup({
            delegate: ".fancybox",
            type: "image",
            gallery: {
                enabled: true,
            },
        });
        // Init gallery magic popup
        function initImageMagnificPopup() {
            var groups = {};
            $(".image-magnific-popup").each(function () {
                var id = $(this).attr("data-group");
                if (!groups[id]) {
                    groups[id] = [];
                }
                groups[id].push(this);
            });
            $.each(groups, function () {
                $(this).magnificPopup({
                    type: "image",
                    closeOnContentClick: true,
                    closeBtnInside: false,
                    gallery: { enabled: true },
                });
            });
        }
        initImageMagnificPopup();
        // End init gallery popup

        // START SLIDER HOME
        if ($(".tp-banner li").length > 0) {
            // set Height slide
            setEqualSlideHeight(".tp-banner-img");

            // When resize then reload social
            $(window).on("resize", function () {
                // Firing resize event only when resizing is finished
                clearTimeout(window.resizedFinishedSlider);
                window.resizedFinishedSlider = setTimeout(function () {
                    $(".tp-rightarrow").trigger("click");
                }, 250);
            });
        }
        // END SLIDER HOME

        $(".video-play, .bt-menu-trigger, .overlay-btn").click(function () {
            $(".overlay").addClass("show-overlay");
            var getSrc = $(".overlay").attr("src");
            $(".overlay")
                .find(".show-iframe")
                .html(
                    '<iframe src="" frameborder="0" allowfullscreen></iframe>'
                );
            $(".show-iframe iframe").attr("src", getSrc);
        });
        $(".bt-menu-trigger, .overlay-btn").click(function () {
            $(".overlay").removeClass("show-overlay");
            $(".show-iframe iframe").attr("src", "");
        });

        $(".arrow-footer").click(function () {
            $("html, body").animate({ scrollTop: 0 }, 800);
            return false;
        });
        $(".item-gallery").each(function () {
            $(this).hover(function () {
                $(this).toggleClass("active");
            });
        });

        $(".main-content").on("mouseover", ".item-gallery", function () {
            $(".item-gallery").removeClass("active");
            $(this).addClass("active");
        });

        // /* ======= shuffle js ======= */
        // if ($('#portfolio-grid').length > 0) {
        //     /* initialize shuffle plugin */
        //     var $grid = $('#portfolio-grid');

        //     $grid.shuffle({
        //         itemSelector: '.portfolio-item' // the selector for the items in the grid
        //     });

        //     /* reshuffle when user clicks a filter item */
        //     $('#filter li').on('click', function (e) {
        //         e.preventDefault();

        //         // set active class
        //         $('#filter li').removeClass('active');
        //         $(this).addClass('active');

        //         // get group name from clicked item
        //         var groupName = $(this).attr('data-group');

        //         // reshuffle grid
        //         $grid.shuffle('shuffle', groupName);
        //     });
        // }
    });
    /*-------------------------------------------
     06. UI Tab
     --------------------------------------------- */
    $("#tabs li").removeClass("ui-corner-top").addClass("ui-corner-left");
    $('[data-toggle="tooltip"]').tooltip();

    // Set date
    Date.prototype.addHours = function (h) {
        this.setTime(this.getTime() + h * 60 * 60 * 1000);
        return this;
    };
    var today = new Date(currDateT);
    var future = new Date(currDateT);

    if (beforeTime == undefined || beforeTime == "" || beforeTime < 0) {
        beforeTime = 0;
    }
    var fourHoursLater = new Date().addHours(beforeTime);

    var set_date =
        parseInt(beforeDay) > 0
            ? new Date(future.setDate(today.getDate() + beforeDay))
            : fourHoursLater;
    set_date = moment(set_date).format(dateFormatBooking);
    set_date = moment(set_date, dateFormatBooking).toDate();

    $("#datetimepicker_v1").datetimepicker({
        format: dateFormatBooking,
        minDate: set_date,
    });
    // End set date
    /*-------------------------------------------
     07. button add services
     --------------------------------------------- */
    $(document).ready(function () {
        // The maximum number of options
        var MAX_OPTIONS = 5;

        $.fn.is_on_screen = function () {
            var win = $(window);
            var viewport = {
                top: win.scrollTop(),
                left: win.scrollLeft(),
            };
            viewport.right = viewport.left + win.width();
            viewport.bottom = viewport.top + win.height();
            var bounds = this.offset();
            if (typeof bounds == "undefined") {
                return false;
            }
            bounds.right = bounds.left + this.outerWidth();
            bounds.bottom = bounds.top + this.outerHeight();
            //
            if (viewport.top >= bounds.top) {
                var t = true;
                $(".btn_service_book").addClass("scroll_btn");
                $(".btn_service_book").css(
                    "right",
                    viewport.right - bounds.right
                );
            } else {
                var t = false;
            }
            return t;
        };
        $(window).scroll(function () {
            if ($(".btn_service_defale").is_on_screen()) {
            } else {
                $(".btn_service_book").removeClass("scroll_btn");
                $(".btn_service_book").removeAttr("style");
            }
        });
    });

    /*-------------------------------------------
     8. Modal login form
     --------------------------------------------- */
    $(".databooktime").on("click", ".popup_login", function () {
        $.magnificPopup.open({
            type: "inline",
            midClick: true,
            items: {
                src: "#popup_login",
            },
        });
        return false;
    });

    $("#send_booking").validate({
        submit: {
            settings: {
                button: ".btn_booking",
                inputContainer: ".input-box",
                errorListClass: "form-tooltip-error",
            },
        },
    });

    $("#send_contact").validate({
        submit: {
            settings: {
                button: ".btn_contact",
                inputContainer: ".form-group",
                errorListClass: "form-tooltip-error",
            },
        },
    });

    // SERVICE PAGE
    $("ul.listcatser li").mouseover(function () {
        $("ul.listcatser li.ui-state-default.ui-corner-left").removeClass(
            "ui-state-active"
        );
        $("ul.listcatser li.ui-tabs-active").addClass("ui-state-active");
        $(this).addClass("ui-state-active");
    });

    $("ul.listcatser li").mouseout(function () {
        $("ul.listcatser li.ui-state-default.ui-corner-left").removeClass(
            "ui-state-active"
        );
        $("ul.listcatser li.ui-tabs-active").addClass("ui-state-active");
    });

    // Auto select
    $("select.auto_select").each(function () {
        var val_default = $(this).attr("defaultvalue");
        $(this)
            .find("option[value='" + val_default + "']")
            .prop("selected", true);
    });

    var lid = $('input[name="lid"]').val();
    lid = $('ul.listcatser li[lid="' + lid + '"] a');
    if (lid.length == 0) {
        lid = $("ul.listcatser li:first a");
    }
    lid.trigger("click");
    // END SERVICE PAGE

    // BOOKING PAGE
    $(document).on("change", "#surveyForm .list_service", function () {
        var service_id = $(this).val();
        var list_staff = $(this).find("option:selected").attr("staff");

        if (service_id) {
            $(this).parent().find(".form-tooltip-error").remove();
        } else {
            $(this)
                .parent()
                .append(
                    '<div class="form-tooltip-error" data-error-list=""><ul><li>' +
                        $(this).data("validation-message") +
                        "</li></ul></div>"
                );
        }
        var obj = JSON.parse(list_staff);
        var option = '<option value="">Service Provider</option>';
        for (var x in obj) {
            option +=
                '<option value="' +
                obj[x].id +
                '" urlimg="' +
                obj[x].image +
                '">' +
                obj[x].name +
                "</option>";
        }

        $(this).parents(".item-booking").find(".list_staff").html(option);

        // Save form
        saveForm();
    });
    // END BOOKING PAGE

    // BTN SEARCH BOOKING
    $(document).on("click", ".btn_action", function () {
        var num = $(".list_service").length;
        var info_staff = [];
        var info_staff2 = [];
        var temp = {};
        var i = 0;
        var check = true;
        $(".list_service").each(function () {
            var checkval = $(this).val();
            if (checkval) {
                $(this).css("border-color", "#ccc");
                $(this).parent().find(".form-tooltip-error").remove();
            } else {
                check = false;
                $(this).css("border-color", "red");
                $(this)
                    .parent()
                    .append(
                        '<div class="form-tooltip-error" data-error-list=""><ul><li>' +
                            $(this).data("validation-message") +
                            "</li></ul></div>"
                    );
            }
            temp.price = $("option:selected", this).attr("price");
            temp.service = $("option:selected", this).text();
            info_staff.push(temp);
            temp = {};
            i++;
        });

        var j = 0;
        $(".list_staff").each(function () {
            var checkval = $(this).val();
            temp.image = $("option:selected", this).attr("urlimg");
            temp.name = checkval
                ? $("option:selected", this).text()
                : "Any person";
            info_staff2.push(temp);
            temp = {};
            j++;
        });

        if (check == true) {
            $(".box_detail_info").show();
            $("#box_person").html("Loading ...");
            var html_person = "";
            var j = 0;
            for (var x in info_staff) {
                var image =
                    typeof info_staff2[x].image === "undefined"
                        ? "/public/library/global/no-photo.jpg"
                        : info_staff2[x].image;
                html_person +=
                    '<div class="col-lg-4 col-md-4 col-sm-6 col-xs-12 info-staff">' +
                    '<div class="row">' +
                    '<div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 img-info-staff">' +
                    '<a href="javascript:;">' +
                    '<img src="' +
                    image +
                    '" alt="' +
                    info_staff2[x].name +
                    '">' +
                    "</a>" +
                    "</div>" +
                    '<div class="col-lg-8 col-md-8 col-sm-8 col-xs-8 title-staff">' +
                    "<h2>" +
                    info_staff2[x].name +
                    "</h2>" +
                    "<p>" +
                    info_staff[x].service +
                    "</p>" +
                    "<p>Price: " +
                    info_staff[x].price +
                    "</p>" +
                    " </div>" +
                    "</div>" +
                    "</div>";
            }

            $("#box_person").html(html_person);

            var typehtml = $("#surveyForm .choose_date").attr("typehtml");
            var date_choose = $("#surveyForm .choose_date").val();

            pushHtmlTime(date_choose, typehtml);

            var scroll = $("#box_person").offset().top;
            $("body").animate({ scrollTop: scroll }, 600, "swing"); //.scrollTop( $("#book-info").offset().top );
            $(".time-booking.databooktime").show();
        } else {
            return false;
        }
    });
    // END BTN SEARCH BOOKING

    // CHOOSE DATE
    $("#surveyForm").on("dp.change", ".choose_date", function () {
        var typehtml = $(this).attr("typehtml");
        var date_choose = $(this).val();
        // set Html date
        setHtmldate(date_choose);
        // Save form
        saveForm();

        // change time by date choose
        // changeTimeByDate(date_choose, typehtml);
        pushHtmlTime(date_choose, typehtml);
        //data time
        // setTimeout(function(){ pushHtmlTime(date_choose, typehtml); }, 100);
    });
    // $(".choose_date").trigger("dp.change");

    // CHOOSE DATE
    $("#send_booking").on("dp.change", ".choose_date", function () {
        var typehtml = $(this).attr("typehtml");
        var date_choose = $(this).val();
        // change time by date choose
        // changeTimeByDate(date_choose, typehtml);
        pushHtmlTime(date_choose, typehtml);
        //data time
        // setTimeout(function(){ pushHtmlTime(date_choose, typehtml); }, 100);
    });
    // END CHOOSE DATE

    // Booking provider
    $("#surveyForm").on("change", ".list_staff", function () {
        // Save form
        saveForm();
    });
    // End booking provider

    // CONFIRM BOOKING
    $(document).ready(function () {
        $("body").on("click", ".open_booking", function () {
            var hours = $(this).attr("valhours");
            $.magnificPopup.open({
                type: "inline",
                midClick: true,
                items: {
                    src: "#open_booking",
                },
                callbacks: {
                    beforeOpen: function () {
                        if ($(window).width() < 700) {
                            this.st.focus = false;
                        } else {
                            this.st.focus = "#name";
                        }
                        $("input[name='booking_hours']").val(hours);
                    },
                },
            });

            return false;
        });

        $(".btn_cancel").click(function () {
            $.magnificPopup.close();
        });
    });
    // END CONFIRM BOOKING

    // Mask Input
    var plholder =
        phoneFormat == "(000) 000-0000"
            ? "Phone (___) ___-____"
            : "Phone ____ ___ ____";
    $(".inputPhone").mask(phoneFormat, { placeholder: plholder });
    // End mask input

    $("#filter li a").click(function (e) {
        var id = $(this).attr("itemprop");
        e.preventDefault();

        // set active class
        $("#filter li").removeClass("active");
        $(this).parent("li").addClass("active");

        getGalleryByCat(id);
    });
    $("#filter li:first a").trigger("click");
    $("select[name='filter_select']").change(function () {
        var id = $(this).val();
        getGalleryByCat(id);
    });

    // $("select[name='filter_select']").trigger("change");
    // check form
    $(document).ready(function () {
        $.ajax({
            type: "post",
            url: "/security/create",
            success: function (token) {
                $("form").each(function () {
                    $(this).prepend(
                        "<input type='hidden' name='token' value='" +
                            token +
                            "' />"
                    );
                });
            },
        });
    });

    /*Anchor link*/
    $('[href^="#"]').on("click", function (event) {
        event.preventDefault();
        let _h = $(this).attr("href");
        let _hsplit = _h.substr(1, _h.length);
        if (_hsplit != "open_booking") {
            event.preventDefault();
            scrollJumpto(_h);
        }
    });
})(jQuery);

function getGalleryByCat(cat_id = 0, page = 0) {
    $.ajax({
        type: "post",
        url: "/gallery/getlistbycat",
        beforeSend: function () {},
        headers: {
            "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content"),
        },
        data: { cat_id: cat_id, page: page },
        success: function (response) {
            console.log(response);
            // console.log(html);
            var obj = response;
            // console.log(obj);
            var html_gallery = "";
            if (obj.length > 0) {
                for (var x in obj) {
                    html_gallery +=
                        '<div class="col-lg-3 col-md-3 col-sm-4 col-xs-6 check-height">' +
                        '<div class="fancybox gallery-item" data-group="gallery-1" title="' +
                        obj[x].name +
                        '" href="' +
                        "../../../storage/photos/" +
                        obj[x].url +
                        '">' +
                        "<span style=\"background-image:url('" +
                        "../../../storage/photos/" +
                        obj[x].url +
                        "')\"></span>" +
                        "</div>" +
                        "</div>";
                }
            } else {
                html_gallery = "Not found gallery item in this category.";
            }

            $(".box_list_gallery").html(html_gallery);
            $(".box_paging").html(obj.paging_ajax);
        },
    });
}

function call_notify(title_msg, msg, type_notify) {
    type_notify = type_notify ? type_notify : "error";

    var icon = "";
    if (type_notify == "error") {
        icon = "fa fa-exclamation-circle";
    } else if (type_notify == "success") {
        icon = "fa fa-check-circle";
    }
    new PNotify({
        title: title_msg,
        text: msg,
        type: type_notify,
        icon: icon,
        addclass: "alert-with-icon",
    });
}

function loadService(pg_id = 0, _page = 0) {
    var btn_appointment = "";
    if (typeof enable_booking != "undefined" && enable_booking == 1) {
        btn_appointment =
            "<a class='btn btn-primary btn_make_appointment' href='/book'>Make an appointment</a>";
    }
    $("ul.services_tab_ul_v1 li").removeClass("ui-state-active");
    $("ul.services_tab_ul_v1 li[lid='" + pg_id + "']").addClass(
        "ui-state-active"
    );
    $.ajax({
        type: "post",
        url: "/service/loadservice",
        data: { pg_id: pg_id, limit: num_paging, page: _page, paging: 1 },
        beforeSend: function () {
            $(".content_service").html("Loading...");
        },
        success: function (html) {
            var obj = JSON.parse(html);
            $(".paging_service").html(obj.paging_ajax);
            var group_des = obj.group_des;
            obj = obj.data;
            if (obj.length > 0) {
                var html_row =
                    '<ul id="all-item" class="services_item_ul_v1">' +
                    '<li class="item-botton services_item_v1 clearfix text-right">' +
                    btn_appointment +
                    '<a class="btn btn-primary" style="margin-left:15px;" href="tel:' +
                    company_phone +
                    '"><span class="fa"><i class="fa fa-phone"></i></span><span class="title">Call now</span></a>' +
                    "</li>";

                if (group_des) {
                    html_row +=
                        '<li class="des_service" style="border-top: none; padding: 10px 0;">' +
                        group_des +
                        "</li>";
                }

                var pull_right = "pull-right";
                if (
                    /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(
                        navigator.userAgent
                    )
                ) {
                    pull_right = "";
                }
                for (x in obj) {
                    var price_show = obj[x].price_sell ? obj[x].price_sell : "";
                    html_row +=
                        '<li class="services_item_v1">' +
                        '<div class="line_item_v1">' +
                        '<div class="just_start_line">' +
                        '<a style="cursor: pointer;" class="open_description" data-toggle="tooltip" data-placement="top" title="' +
                        obj[x].description +
                        '">' +
                        '<span class="name_service_v1">' +
                        obj[x].name +
                        "</span>" +
                        '<span class="price_service_v1 ' +
                        pull_right +
                        '">' +
                        price_show +
                        obj[x].product_up +
                        "</span>" +
                        "</a>" +
                        '<div class="box_des">' +
                        obj[x].product_description +
                        "</div>" +
                        "</div>" +
                        "</div>" +
                        "/li>";
                }

                html_row += "</ul>";

                $(".content_service").html(html_row);

                $("#tabs li")
                    .removeClass("ui-corner-top")
                    .addClass("ui-corner-left");
                $("body, html").animate(
                    {
                        scrollTop: $(".box_service").offset().top - 100,
                    },
                    1000
                );
            } else {
                $(".content_service").html(
                    "No services found in this category"
                );
            }
        },
    });
    // Load gallery right
    loadGallery(pg_id);
}

function loadGallery(pg_id = 0) {
    if (pg_id) {
        $.ajax({
            type: "post",
            url: "/service/loadgallery",
            data: { id: pg_id },
            beforeSend: function () {
                // $(".box_show_gallery").html("Loading...");
            },
            success: function (html) {
                // console.log(html);
                var obj = JSON.parse(html);
                var html_img = "";
                for (var x in obj) {
                    html_img +=
                        '<li><img itemprop="image" alt="" src="' +
                        obj[x].image +
                        '" class="img-responsive"></li>';
                }

                $(".box_show_gallery").html(html_img);
            },
        });
    }
}

function saveForm() {
    // Save form
    var formdata = $("#surveyForm").serialize();
    $.ajax({
        type: "post",
        url: "/book/saveform",
        data: formdata,
        success: function (html) {
            // console.log(html);
        },
    });
}

function update_price(onthis) {
    var cus_price = isNaN(parseFloat($(onthis).val()))
        ? 0
        : parseFloat($(onthis).val());
    var id = $(onthis).attr("cart_id");
    var max_val = parseFloat($(onthis).attr("max"));
    var min_val = parseFloat($(onthis).attr("min"));

    if (cus_price >= min_val && cus_price <= max_val) {
        $(onthis).css("border-color", "#ccc");
        $(".btn_cart_order").attr("href", "/payment");
        //Ajax
        $.ajax({
            type: "post",
            url: "/cart/updateprice",
            data: { cus_price: cus_price, id: id },
            success: function (html) {
                // console.log(html);
                var obj = JSON.parse(html);
                if (obj.status == "error") {
                    call_notify("Notification", obj.msg, "error");
                    $(onthis).val(obj.price);
                    return false;
                }
                // set value
                if (obj.total_show && obj.amount) {
                    $(onthis)
                        .parents("tr")
                        .find(".total_change")
                        .html(obj.total_show);
                    $(".amount_change").html(obj.amount);
                }

                if (obj.cart_data) {
                    $("#cart_tax").text(obj.cart_data[1]);
                    $("#cart_discount_code_value").text(obj.cart_data[5]);
                    $("#cart_subtotal").text(obj.cart_data[2]);
                    $("#cart_payment_total").text(obj.cart_data[3]);
                }
            },
        });
    } else {
        $(onthis).css("border-color", "red");
        $(".btn_cart_order").removeAttr("href");
    }
}

function loadForm(formdata) {
    var obj = JSON.parse(formdata);
    $("input[name='booking_date']").val(obj.booking_date);
    $("input[name='booking_hours']").val(obj.booking_hours);
    var listservice =
        typeof obj.service_staff != "undefined" ? obj.service_staff : [];
    // console.log(listservice);
    if (listservice.length > 0) {
        for (var x in listservice) {
            // split info
            var list = listservice[x].split(",");
            // Trigger add row
            if (x > 0) {
                $(".addButton").trigger("click");
            }
            var objservice = $(".list_service:last");
            $(".list_service:last option[value='" + list[0] + "']").attr(
                "selected",
                "selected"
            );
            objservice.trigger("change");
            $(".list_staff:last option[value='" + list[1] + "']").attr(
                "selected",
                "selected"
            );
        }

        // Trigger action
        $(".btn_action").trigger("click");
    }
}

function convertDate(input) {
    var list_date = input.split("/");
    var splitDate = posFormat.split(",");
    var new_date =
        list_date[splitDate[2]] +
        "/" +
        list_date[splitDate[1]] +
        "/" +
        list_date[splitDate[0]];
    return new_date;
}

function pushHtmlTime(input_date, type) {
    $.ajax({
        type: "post",
        url: "/book/get_hours",
        data: { input_date: input_date, type: type },
        beforeSend: function () {
            $(".box_detail_info").append(
                "<div class='mask_booking'><i class='fa fa-spinner fa-pulse fa-3x fa-fw'></i></div>"
            );
            $(".box_detail_info").css("position", "relative");
            $(".mask_booking")
                .css("position", "absolute")
                .css("height", "100%")
                .css("width", "100%")
                .css("top", 0)
                .css("left", 0)
                .css("background", "rgba(0,0,0,0.5)")
                .css("text-align", "right");
            $(".mask_booking i").css("font-size", "2em").css("margin", "10px");
        },
        success: function (response) {
            // console.log(response);
            // Remove mask
            $(".mask_booking").remove();
            var obj = JSON.parse(response);
            if (obj.checkmorning == false) {
                $(".note_am_time").html("(Booking time has expired)");
            } else {
                $(".note_am_time").html("");
            }

            if (obj.checkafternoon == false) {
                $(".note_pm_time").html("(Booking time has expired)");
            } else {
                $(".note_pm_time").html("");
            }

            $(".databooktime .timemorning").html(obj.htmlMorning);
            $(".databooktime .timeafternoon").html(obj.htmlAfternoon);
        },
    });
}

function setHtmldate(date_choose) {
    // use for booking
    var new_date = convertDate(date_choose);
    var d = new Date(new_date);
    var months = [
        "Jan",
        "Feb",
        "Mar",
        "Apr",
        "May",
        "Jun",
        "Jul",
        "Aug",
        "Sep",
        "Oct",
        "Nov",
        "Dec",
    ];
    var days = [
        "Sunday",
        "Monday",
        "Tuesday",
        "Wednesday",
        "Thursday",
        "Friday",
        "Saturday",
    ];
    var str_show =
        days[d.getDay()] +
        ", " +
        months[d.getMonth()] +
        "-" +
        d.getDate() +
        "-" +
        d.getFullYear();
    // console.log(str_show);
    $(".time_show").html(str_show);
}

function loadEvent() {
    $("#surveyForm")
        .on("click", ".addButton", function () {
            var html_close =
                '<div class="removeButton"><img src="/public/library/global/remove-service-icon-new.png"></div>';
            var template =
                '<div class="item-booking">' +
                html_close +
                $("#optionTemplate").html() +
                "</div>";
            $(this).before($(template));
            $("#surveyForm .item-booking:last .list_service").trigger("change");
            saveForm();
        })

        // Remove button click handler
        .on("click", ".removeButton", function () {
            var $row = $(this).parents(".item-booking"),
                $option = $row.find('[name="option[]"]');

            // Remove element containing the option
            $row.remove();
            saveForm();
        });
}

function update_cart(onthis) {
    var quantity = $(onthis).val();
    var id = $(onthis).attr("cart_id");
    //Ajax
    $.ajax({
        type: "post",
        url: "/cart/update",
        data: { quantity: quantity, id: id },
        success: function (html) {
            // console.log(html);
            var obj = JSON.parse(html);
            // set value
            if (obj.total_show && obj.amount) {
                $(onthis)
                    .parents("tr")
                    .find(".total_change")
                    .html(obj.total_show);
                $(".amount_change").html(obj.amount);
            }

            if (obj.cart_data) {
                $("#cart_tax").text(obj.cart_data[1]);
                $("#cart_discount_code_value").text(obj.cart_data[5]);
                $("#cart_subtotal").text(obj.cart_data[2]);
                $("#cart_payment_total").text(obj.cart_data[3]);
            }
        },
    });
}

function delItem(onthis) {
    var id = $(onthis).attr("cart_id");
    //Ajax
    $.ajax({
        type: "post",
        url: "/cart/delitem",
        data: { id: id },
        success: function (html) {
            // console.log(html);
            var obj = JSON.parse(html);
            // set value
            if (obj.amount) {
                // remove row
                $(onthis).parents("tr").remove();
                // change stt
                if ($(".list_stt").length > 0) {
                    var i = 1;
                    $(".list_stt").each(function () {
                        $(this).html("#" + i);
                        i++;
                    });
                } else {
                    $("tbody.step1").html(
                        '<tr><td colspan="7" style="text-align: center"><b>Cart empty</b></td></tr>'
                    );
                }
                // set amount
                $(".amount_change").html(obj.amount);

                if (obj.cart_data) {
                    $("#cart_tax").text(obj.cart_data[1]);
                    $("#cart_discount_code_value").text(obj.cart_data[5]);
                    $("#cart_subtotal").text(obj.cart_data[2]);
                    $("#cart_payment_total").text(obj.cart_data[3]);
                }
            }
        },
    });
}

function changeTimeByDate(input_date, typehtml) {
    // check date time
    var splitDate = posFormat.split(","); //1,0,2
    // change time
    $.ajax({
        type: "post",
        url: "/book/change_time",
        data: { date: input_date },
        success: function (response) {
            // console.log(response);
            if (response) {
                var obj = JSON.parse(response);
                timeMorning = JSON.stringify(obj.time_morning);
                // convert time afternoon
                var afternoon_time = obj.time_afternoon;
                for (var x in afternoon_time) {
                    var listTime = afternoon_time[x].split(":");

                    if (listTime[0] >= 1 && listTime[0] < 12) {
                        var changeTime = parseInt(listTime[0]) + 12;
                        afternoon_time[x] = changeTime + ":" + listTime[1];
                    }
                }

                timeAfternoon = JSON.stringify(afternoon_time);
                pushHtmlTime(input_date, typehtml);
            }
        },
    });
}

$(document).ready(function () {
    /*////////////// GALLERY ///////////////*/
    var groups = {};
    $(".gallery-item").each(function () {
        var id = parseInt($(this).attr("data-group"), 10);
        if (!groups[id]) {
            groups[id] = [];
        }
        groups[id].push(this);
    });

    $.each(groups, function () {
        $(this).magnificPopup({
            type: "image",
            closeOnContentClick: true,
            closeBtnInside: true,
            gallery: { enabled: true },
        });
    });
});

$(window).load(function () {
    if ($(".animation_sroll_jumpto .sroll_jumpto").length > 0) {
        scrollJumpto("#sci_" + $('input[name="group_id"]').val());
    }
});

function applyDiscountCode() {
    $("#loader_discount_code").show();
    $("#enter_discount_code").hide();
    $("#cart_discount_code").prop("disabled", true);

    let code = $("#cart_discount_code").val();
    $.ajax({
        url: "/payment/discount_code/",
        data: { code: code },
        dataType: "json",
        success: function (res) {
            $("#loader_discount_code").hide();
            $("#enter_discount_code").show();
            $("#cart_discount_code").prop("disabled", false);

            if (res.status == "ok") {
                $("#discount_code_input").hide();
                $("#discount_code_info").show();
                $("#cart_discount_code_text").text(res.code_data.code);
                $("#cart_discount_code_value").text(res.cart_data[5]);
                $("#cart_subtotal").text(res.cart_data[2]);
                $("#cart_payment_total").text(res.cart_data[3]);
                $("#cart_tax").text(res.cart_data[1]);
            } else {
                call_notify("Alert", res.msg, "error");
            }
        },
    });
}

function removeDiscountCode() {
    $.ajax({
        url: "/payment/remove_code/",
        dataType: "json",
        success: function (res) {
            if (res.status == "ok") {
                $("#discount_code_input").show();
                $("#discount_code_info").hide();
                $("#cart_discount_code_text").text("");
                $("#cart_discount_code_value").text(res.cart_data[5]);
                $("#cart_subtotal").text(res.cart_data[2]);
                $("#cart_payment_total").text(res.cart_data[3]);
                $("#cart_tax").text(res.cart_data[1]);
            } else {
                call_notify("Alert", res.msg, "error");
            }
        },
    });
}

function scrollJumpto(jumpto, headerfixed, redirect) {
    // check exits element for jumpto
    if ($(jumpto).length > 0) {
        // Calculator position and call jumpto with effect
        jumpto = $(jumpto).offset().top;
        headerfixed = $(headerfixed).length > 0 ? $(headerfixed).height() : 0;

        $("html, body").animate(
            {
                scrollTop: parseInt(jumpto - headerfixed) + "px",
            },
            1000,
            "swing"
        );
    }
    // Check redirect if not exits element for jumpto
    else if (redirect) {
        // Call redirect
        redirectUrl(redirect);
        return;
    } else {
        console.log(jumpto + " Not found.");
    }
}

function setEqualSlideHeight(selector) {
    $(selector).show();

    var heights = [];
    var widths = [];

    $(selector).each(function () {
        heights.push($(this).find("img").height());
        widths.push($(this).find("img").width());
    });

    var maxheights = 660;
    if (heights.length > 0) {
        maxheights = Math.max.apply(Math, heights);
    }

    var maxwidths = 1170;
    if (widths.length > 0) {
        maxwidths = Math.max.apply(Math, widths);
    }

    $(".tp-banner").revolution({
        delay: 10000,
        hideThumbs: 10,
        startwidth: maxwidths,
        startheight: maxheights,
    });

    $(selector).hide();
}

$(document).ready(function () {
    $(".owl-cus-service-v1").owlCarousel({
        loop: false,
        margin: 0,
        nav: true,
        dots: false,
        items: 1,
    });
    $(".owl-cus-service-v2").owlCarousel({
        loop: false,
        margin: 0,
        nav: true,
        dots: false,
        items: 2,
        responsive: {
            0: {
                items: 1,
            },
            575: {
                items: 2,
            },
        },
    });
    $(".owl-cus-service-v3").owlCarousel({
        loop: false,
        margin: 0,
        nav: true,
        dots: false,
        items: 3,
        responsive: {
            0: {
                items: 1,
            },
            575: {
                items: 2,
            },
            992: {
                items: 3,
            },
        },
    });
});

function load_social(inputs) {
    if (!inputs) {
        console.log("load social missed inputs");
        return false;
    }

    /*calculator width*/
    let social_block_width = $("#social_block_width").width();
    social_block_width = Math.round(social_block_width);

    if (social_block_width > 450) {
        social_block_width = 450;
    }

    if (social_block_width < 180) {
        social_block_width = 180;
    }

    /*facebook fanpage*/
    if (typeof inputs.facebook_embed != "undefined" && inputs.facebook_embed) {
        let social_block_height = Math.round(
            social_block_width *
                (parseInt(inputs.facebook_embed.height) /
                    parseInt(inputs.facebook_embed.width))
        );
        let social_url = "";
        if (!inputs.facebook_embed.likebox_enable) {
            social_url += "https://www.facebook.com/plugins/page.php?";
            social_url +=
                "&width=" +
                social_block_width +
                "&height=" +
                social_block_height;
            social_url +=
                "&small_header=" +
                (inputs.facebook_embed.small_header ? "true" : "false");
            social_url += "&tabs=" + inputs.facebook_embed.tabs;
            social_url +=
                "&show_facepile=" +
                (inputs.facebook_embed.show_facepile ? "true" : "false");
            social_url +=
                "&hide_cover=false&hide_cta=false&adapt_container_width=true";
        } else {
            social_url += "https://www.facebook.com/plugins/likebox.php?";
            social_url +=
                "&width=" +
                social_block_width +
                "&height=" +
                social_block_width; // If set height then error with likebox
            social_url +=
                "&show_faces=" +
                (inputs.facebook_embed.likebox_show_faces ? "true" : "false");
            social_url +=
                "&stream=" +
                (inputs.facebook_embed.likebox_stream ? "true" : "false");
            social_url += "&header=false";
        }
        social_url +=
            "&href=" + encodeURIComponent(inputs.facebook_embed.id_fanpage);
        social_url += "&appId" + inputs.facebook_embed.appId;

        $("#fanpage_fb_container").html(
            '<iframe style="overflow:hidden;max-height:' +
                social_block_height +
                'px" title="Social fanpage" src="' +
                social_url +
                '" width="' +
                social_block_width +
                '" height="' +
                social_block_height +
                '" style="border:none;overflow:hidden" scrolling="no" frameborder="0" allowTransparency="true"></iframe>'
        );
    }

    /*google fanpage*/
    if (
        typeof inputs.google_id_fanpage != "undefined" &&
        inputs.google_id_fanpage
    ) {
        $("#fanpage_google_container").html(
            '<div class="g-page" data-href="' +
                inputs.google_id_fanpage +
                '" data-width="' +
                social_block_width +
                '"></div><script src="https://apis.google.com/js/platform.js" async defer></script>'
        );
    }

    /*twitter fanpage*/
    $("#fanpage_twitter_container").html(""); // clear content
    if (
        typeof inputs.twitter_id_fanpage != "undefined" &&
        inputs.twitter_id_fanpage
    ) {
        inputs.twitter_id_fanpage = inputs.twitter_id_fanpage.split("/");
        for (let i = inputs.twitter_id_fanpage.length - 1; i >= 0; i -= 1) {
            if (inputs.twitter_id_fanpage[i] != "") {
                inputs.twitter_id_fanpage = inputs.twitter_id_fanpage[i];
                break;
            }
        }
        if (typeof twttr != "undefined") {
            twttr.widgets.createTweet(
                inputs.twitter_id_fanpage,
                document.getElementById("fanpage_twitter_container"),
                { width: social_block_width }
            );
        }
    }
}

$(document).ready(function () {
    /*
     * SOCIAL FAN PAGE
     * When resize then reload fanpage
     * Firing resize event only when resizing is finished
     */
    let socialInputs = {
        facebook_embed: facebook_embed,
        google_id_fanpage: google_id_fanpage,
        twitter_id_fanpage: twitter_id_fanpage,
    };
    $(window).load(function () {
        load_social(socialInputs);
        $(window).on("resize", function () {
            clearTimeout(window.resizedFinished);
            window.resizedFinished = setTimeout(function () {
                load_social(socialInputs);
            }, 250);
        });
    });
});
