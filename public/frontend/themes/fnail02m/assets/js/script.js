$(document).ready(function() {
    new WOW().init();

     
    /*if ($(window).width() < 768) {
        $(".menu_mobile_v1").sticky({topSpacing:0});  
    } else {
        $(".header").sticky({topSpacing:0});
    }

     $(".menu_mobile_v1").sticky({topSpacing:0});*/

    /*////////////// MY SLIDER ///////////////*/
    $( '#my-slider' ).sliderPro({
        width: 1976,
        height:800,
        arrows: true,
        fade: true,
        autoHeight:true,
        centerImage:false,
        autoScaleLayers: false,
        buttons: true,
        thumbnailArrows: true,
        autoplay: true,
        slideSpeed : 300,
        breakpoints: {
            768: {
                arrows: true,
                fade: true,
                autoHeight:true,
                centerImage:false,
                autoScaleLayers: false,
                forceSize: 'fullWidth',
                buttons: false,
                thumbnailArrows: false,
                autoplay: true,
                slideSpeed : 300,
            }
        },
    });

    /*$(window).scroll(function(){
        if($(window).width()>990){
            if ($(window).scrollTop() >= 100) {
               $('.header').addClass('shownav');
            }
            else {
                $('.header').removeClass('shownav');
            }
        }
        
    });
    
   

    $('.movenav').click(function(){
        var idmove = $(this).attr('data-id');
        $('html, body').animate({scrollTop: $("#"+idmove).offset().top}, 'slow');
        return false;
    });*/
   

	/*////////////// MOBILE NAV ///////////////*/
	$('.mobile-menu nav').meanmenu({
        meanMenuContainer: '.menu_mobile_v1',
        meanScreenWidth: "990",
        meanRevealPosition: "right",
        meanMenuOpen: "<span></span>"
    });
    

    /*////////////// GALLERY ///////////////*/
    var groups = {};
    $('.gallery-item').each(function() {
      var id = parseInt($(this).attr('data-group'), 10);      
      if(!groups[id]) {
        groups[id] = [];
      }       
      groups[id].push( this );
    });


    $.each(groups, function() {
      
      $(this).magnificPopup({
            type: 'image',
            closeOnContentClick: true,
            closeBtnInside: true,
            gallery: { enabled:true }
      })
      
    });
    /*////////////// BOOKING ///////////////*/
    $(document).ready(function() {
      //$('#datetimepicker_v1').datetimepicker();
    });
    // CONFIRM BOOKING
    $(document).ready(function(){
        $(".databooktime").on("click",".open_booking", function(){
            var hours = $(this).attr("valhours");
            $.magnificPopup.open({
                type: 'inline',
                midClick: true,
                items: {
                  src: '#open_booking'
                },
                callbacks: {
                    beforeOpen: function() {
                        if($(window).width() < 700) {
                            this.st.focus = false;
                        } else {
                            this.st.focus = '#name';
                        }
                        $("input[name='booking_hours']").val(hours);


                    }
                }
            });

            return false;
        });

        $(".btn_cancel").click(function(){
            $.magnificPopup.close();
            
        });
    });
    // END CONFIRM BOOKING
    // FLEX LABEL IN PAYMENT 
   // $('.fl-flex-label').flexLabel();


    $("body").append('<p id="back-top"> <a href="#top"><i class="fa fa-chevron-up" aria-hidden="true"></i></a> </p>');
    $("#back-top").hide();
    $(function () {
        $(window).scroll(function () {
            if ($(this).scrollTop() > 600) {
                $('#back-top').fadeIn();
            } else {
                $('#back-top').fadeOut();
            }
        });
        $('#back-top a').click(function () {
            $('body,html').animate({
                scrollTop: 0
            }, 800);
            return false;
        });
    });


    
});

