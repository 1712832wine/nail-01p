$(document).ready(function() {
    $("body").append('<p id="back-top"> <a href="#top" title="Scroll top"><i class="fa fa-chevron-up" aria-hidden="true"></i></a> </p>');
    $("#back-top").hide();
    $(function () {
        $(window).scroll(function () {
            if ($(this).scrollTop() > 600) {
                $('#back-top').fadeIn();
            } else {
                $('#back-top').fadeOut();
            }
        });
        $('#back-top a').click(function () {
            $('body,html').animate({
                scrollTop: 0
            }, 800);
            return false;
        });
    });

    $(".review-list").owlCarousel({
        loop:true,
        nav:true,
        margin:30,
        dots:false,
        smartSpeed:2000,
        autoplay:true,
        autoplayTimeout:3000,
        autoplayHoverPause:true,
        items:1
    });
});