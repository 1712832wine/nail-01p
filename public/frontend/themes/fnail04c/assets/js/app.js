(function ($) {
    "use strict";

    /**
     * Set date: Init date time picker for booking
     * Note: place here for deny error when load booking email form in first
     */
    Date.prototype.addHours = function (h) {
        this.setTime(this.getTime() + h * 60 * 60 * 1000);
        return this;
    };
    var today = new Date(currDateT);
    var future = new Date(currDateT);

    if (beforeTime == undefined || beforeTime == "" || beforeTime < 0) {
        beforeTime = 0;
    }
    var fourHoursLater = new Date().addHours(beforeTime);

    var set_date =
        parseInt(beforeDay) > 0
            ? new Date(future.setDate(today.getDate() + beforeDay))
            : fourHoursLater;
    set_date = moment(set_date).format(dateFormatBooking);
    set_date = moment(set_date, dateFormatBooking).toDate();

    $("#datetimepicker_v1, .booking_date").datetimepicker({
        format: dateFormatBooking,
        minDate: set_date,
    });
    // End set date

    var url = window.location.pathname;
    if (url != "/") {
        var urlRegExp = new RegExp(url.replace(/\/$/, "") + "$"); // create regexp to match current url pathname and remove trailing slash if present as it could collide with the link in navigation in case trailing slash wasn't present there
        // now grab every link from the navigation
        $(".main-menu > ul > li > a").each(function () {
            // and test its normalized href against the url pathname regexp
            if (urlRegExp.test(this.href.replace(/\/$/, ""))) {
                $(this).parent("li").addClass("active");
            }
        });
    } else {
        $(".main-menu > ul > li > a[href='/']").parent("li").addClass("active");
    }

    $(".menu-bar-mobile ul li").each(function () {
        $(this)
            .find("span")
            .html('<i class="fa fa-angle-right" aria-hidden="true"></i>');
        $(this).find("span").attr("check", 0);
    });

    $(".menu-bar-mobile ul li span").click(function () {
        var check = $(this).attr("check");
        if (check == 0) {
            $(this).attr("check", 1);
            $(this).html('<i class="fa fa-angle-down" aria-hidden="true"></i>');
        } else {
            $(this).attr("check", 0);
            $(this).html(
                '<i class="fa fa-angle-right" aria-hidden="true"></i>'
            );
        }
        $(this).next().toggle();
    });
    /*-------------------------------------------
     02. wow js active
     --------------------------------------------- */
    new WOW().init();
    /*-------------------------------------------
     03. Sticky Header
     --------------------------------------------- */
    $(window).on("scroll", function () {
        var scroll = $(window).scrollTop();
        if (scroll < 245) {
            $("#sticky-header-with-topbar").removeClass("scroll-header");
        } else {
            $("#sticky-header-with-topbar").addClass("scroll-header");
        }
    });
    /*--------------------------------
     /*-------------------------------------------
     05. Portfolio  Masonry (width)
     --------------------------------------------- */
    $(document).ready(function () {
        load_gallery();

        $(".video-play, .bt-menu-trigger, .overlay-btn").click(function () {
            $(".overlay").addClass("show-overlay");
            var getSrc = $(".overlay").attr("src");
            $(".overlay")
                .find(".show-iframe")
                .html(
                    '<iframe src="" frameborder="0" allowfullscreen></iframe>'
                );
            $(".show-iframe iframe").attr("src", getSrc);
        });
        $(".bt-menu-trigger, .overlay-btn").click(function () {
            $(".overlay").removeClass("show-overlay");
            $(".show-iframe iframe").attr("src", "");
        });

        $(".arrow-footer").click(function () {
            $("html, body").animate({ scrollTop: 0 }, 800);
            return false;
        });

        $(".owl-testimonials, .owl-blog-news").owlCarousel({
            items: 3,
            slideSpeed: 300,
            pagination: false,
            navigation: true,
            navigationText: [
                "<i class='fa fa-angle-left'></i>",
                "<i class='fa fa-angle-right'></i>",
            ],
            lazyLoad: true,
            itemsDesktop: [990, 2],
            itemsDesktopSmall: [600, 1],
            itemsTablet: [560, 1],
        });
        $(".arrow-footer").click(function () {
            $("html, body").animate({ scrollTop: 0 }, 800);
            return false;
        });
        $(".check-height").height($(".check-height").width() - 60);
        // $('.item-gallery').each(function () {

        // });

        $(".main-content").on("mouseover", ".item-gallery", function () {
            $(".item-gallery").removeClass("active");
            $(this).addClass("active");
        });

        $(".coupon_gift").magnificPopup({
            type: "image",
            gallery: {
                enabled: true,
            },
        });
        /* ======= shuffle js ======= */
        // if ($('#portfolio-grid').length > 0) {
        //     /* initialize shuffle plugin */
        //     var $grid = $('#portfolio-grid');

        //     $grid.shuffle({
        //         itemSelector: '.portfolio-item' // the selector for the items in the grid
        //     });

        //     /* reshuffle when user clicks a filter item */
        //     $('#filter li').on('click', function (e) {
        //         e.preventDefault();

        //         // set active class
        //         $('#filter li').removeClass('active');
        //         $(this).addClass('active');

        //         // get group name from clicked item
        //         var groupName = $(this).attr('data-group');

        //         // reshuffle grid
        //         $grid.shuffle('shuffle', groupName);
        //     });
        // }
    });
    /*-------------------------------------------
     06. UI Tab
     --------------------------------------------- */

    /*-------------------------------------------
     07. button add services
     --------------------------------------------- */
    $(document).ready(function () {
        $("#tabs li").removeClass("ui-corner-top").addClass("ui-corner-left");
        $('[data-toggle="tooltip"]').tooltip();

        $(document).on("click", ".registry_ver_1", function () {
            $(this).toggleClass("open");
        });
        // The maximum number of options
        var MAX_OPTIONS = 5;

        $.fn.is_on_screen = function () {
            var win = $(window);
            var viewport = {
                top: win.scrollTop(),
                left: win.scrollLeft(),
            };
            viewport.right = viewport.left + win.width();
            viewport.bottom = viewport.top + win.height();
            var bounds = this.offset();
            if (typeof bounds == "undefined") {
                return false;
            }
            bounds.right = bounds.left + this.outerWidth();
            bounds.bottom = bounds.top + this.outerHeight();
            //
            if (viewport.top >= bounds.top) {
                var t = true;
                $(".btn_service_book").addClass("scroll_btn");
                $(".btn_service_book").css(
                    "right",
                    viewport.right - bounds.right
                );
                if ($(".freeze-header-fixed").length > 0) {
                    $(".btn_service_book").css(
                        "top",
                        $(".freeze-header-fixed").height()
                    );
                }
            } else {
                var t = false;
            }
            return t;
        };
        $(window).scroll(function () {
            if ($(".btn_service_defale").is_on_screen()) {
            } else {
                $(".btn_service_book").removeClass("scroll_btn");
                $(".btn_service_book").removeAttr("style");
            }
        });

        var lid = $('input[name="lid"]').val();
        lid = $('.cate-services ul li[lid="' + lid + '"] a');
        if (lid.length == 0) {
            lid = $(".cate-services ul li:first a");
        }
        lid.trigger("click");
    });

    /*-------------------------------------------
     8. Modal login form
     --------------------------------------------- */
    $(".databooktime").on("click", ".popup_login", function () {
        $.magnificPopup.open({
            type: "inline",
            midClick: true,
            items: {
                src: "#popup_login",
            },
        });
        return false;
    });

    $("#send_booking").validate({
        submit: {
            settings: {
                button: ".btn_booking",
                inputContainer: ".input-box",
                errorListClass: "form-tooltip-error",
            },
        },
    });

    // SERVICE PAGE
    $("ul.listcatser li").mouseover(function () {
        $("ul.listcatser li.ui-state-default.ui-corner-left").removeClass(
            "ui-state-active"
        );
        $("ul.listcatser li.ui-tabs-active").addClass("ui-state-active");
        $(this).addClass("ui-state-active");
    });

    $("ul.listcatser li").mouseout(function () {
        $("ul.listcatser li.ui-state-default.ui-corner-left").removeClass(
            "ui-state-active"
        );
        $("ul.listcatser li.ui-tabs-active").addClass("ui-state-active");
    });

    // Auto select
    $("select.auto_select").each(function () {
        var val_default = $(this).attr("defaultvalue");
        $(this)
            .find("option[value='" + val_default + "']")
            .prop("selected", true);
    });

    // END SERVICE PAGE

    // BOOKING PAGE
    $(document).on("change", "#surveyForm .list_service", function () {
        var service_id = $(this).val();
        var list_staff = $(this).find("option:selected").attr("staff");

        if (service_id) {
            $(this).css("border-color", "#ccc");
            $(this).parent().find(".form-tooltip-error").remove();
        } else {
            $(this).css("border-color", "red");
            $(this)
                .parent()
                .append(
                    '<div class="form-tooltip-error" data-error-list=""><ul><li>' +
                        $(this).data("validation-message") +
                        "</li></ul></div>"
                );
        }
        var obj = JSON.parse(list_staff);
        var option = '<option value="">Service Provider</option>';
        for (var x in obj) {
            option +=
                '<option value="' +
                obj[x].id +
                '" urlimg="' +
                obj[x].image +
                '">' +
                obj[x].name +
                "</option>";
        }

        $(this).parents(".item-booking").find(".list_staff").html(option);

        // Save form
        saveForm();
    });
    // END BOOKING PAGE

    // BTN SEARCH BOOKING
    $(document).on("click", ".btn_action", function () {
        var num = $(".list_service").length;
        var info_staff = [];
        var info_staff2 = [];
        var temp = {};
        var i = 0;
        var check = true;
        $(".list_service").each(function () {
            var checkval = $(this).val();
            if (checkval) {
                $(this).css("border-color", "#ccc");
                $(this).parent().find(".form-tooltip-error").remove();
            } else {
                check = false;
                $(this).css("border-color", "red");
                $(this)
                    .parent()
                    .append(
                        '<div class="form-tooltip-error" data-error-list=""><ul><li>' +
                            $(this).data("validation-message") +
                            "</li></ul></div>"
                    );
            }
            temp.price = $("option:selected", this).attr("price");
            temp.service = $("option:selected", this).text();
            info_staff.push(temp);
            temp = {};
            i++;
        });

        var j = 0;
        $(".list_staff").each(function () {
            var checkval = $(this).val();
            temp.image = $("option:selected", this).attr("urlimg");
            temp.name = checkval
                ? $("option:selected", this).text()
                : "Any person";
            info_staff2.push(temp);
            temp = {};
            j++;
        });

        if (check == true) {
            $(".box_detail_info").show();
            $("#box_person").html("Loading ...");
            var html_person = "";
            var j = 0;
            for (var x in info_staff) {
                var image =
                    typeof info_staff2[x].image === "undefined"
                        ? "/public/library/global/no-photo.jpg"
                        : info_staff2[x].image;
                html_person +=
                    '<div class="col-lg-6 col-md-6 col-xs-12 info-staff">' +
                    '<div class="img-info-staff">' +
                    '<a href="">' +
                    '<img src="' +
                    image +
                    '" alt="' +
                    info_staff2[x].name +
                    '">' +
                    "</a>" +
                    "</div>" +
                    '<div class="title-staff">' +
                    "<h2>" +
                    info_staff2[x].name +
                    "</h2>" +
                    "<p>" +
                    info_staff[x].service +
                    "</p>" +
                    "<p>Price: " +
                    info_staff[x].price +
                    "</p>" +
                    "</div>" +
                    "</div>";
            }

            $("#box_person").html(html_person);

            var typehtml = $("#surveyForm .choose_date").attr("typehtml");
            var date_choose = $("#surveyForm .choose_date").val();
            // change time by date choose
            // changeTimeByDate(date_choose, typehtml);
            pushHtmlTime(date_choose, typehtml);

            var scroll = $("#box_person").offset().top;
            $("body").animate({ scrollTop: scroll }, 600, "swing"); //.scrollTop( $("#book-info").offset().top );
            $(".time-booking.databooktime").show();
        } else {
            return false;
        }
    });
    // END BTN SEARCH BOOKING

    // CHOOSE DATE
    $("#surveyForm").on("dp.change", ".choose_date", function () {
        var typehtml = $(this).attr("typehtml");
        var date_choose = $(this).val();
        // set Html date
        setHtmldate(date_choose);
        // Save form
        saveForm();

        // change time by date choose
        // changeTimeByDate(date_choose, typehtml);
        pushHtmlTime(date_choose, typehtml);
        //data time
        // setTimeout(function(){ pushHtmlTime(date_choose, typehtml); }, 100);
    });
    // $(".choose_date").trigger("dp.change");

    // CHOOSE DATE
    $("#send_booking").on("dp.change", ".choose_date", function () {
        var typehtml = $(this).attr("typehtml");
        var date_choose = $(this).val();
        // change time by date choose
        // changeTimeByDate(date_choose, typehtml);
        pushHtmlTime(date_choose, typehtml);
        //data time
        // setTimeout(function(){ pushHtmlTime(date_choose, typehtml); }, 100);
    });
    // END CHOOSE DATE

    // Booking provider
    $("#surveyForm").on("change", ".list_staff", function () {
        // Save form
        saveForm();
    });
    // End booking provider

    // CONFIRM BOOKING
    $(document).ready(function () {
        $("body").on("click", ".open_booking", function () {
            var hours = $(this).attr("valhours");
            $.magnificPopup.open({
                type: "inline",
                midClick: true,
                items: {
                    src: "#open_booking",
                },
                callbacks: {
                    beforeOpen: function () {
                        if ($(window).width() < 700) {
                            this.st.focus = false;
                        } else {
                            this.st.focus = "#name";
                        }
                        $("input[name='booking_hours']").val(hours);
                    },
                },
            });

            return false;
        });

        $(".btn_cancel").click(function () {
            $.magnificPopup.close();
        });
    });
    // END CONFIRM BOOKING

    // Mask Input
    var plholder =
        phoneFormat == "(000) 000-0000"
            ? "Phone (___) ___-____"
            : "Phone ____ ___ ____";
    $(".inputPhone").mask(phoneFormat, { placeholder: plholder });
    // End mask input

    $("#filter li a").click(function (e) {
        var id = $(this).attr("itemprop");
        e.preventDefault();

        // set active class
        $("#filter li").removeClass("active");
        $(this).parent("li").addClass("active");

        getGalleryByCat(id);
    });
    $("#filter li:first a").trigger("click");
    $("select[name='filter_select']").change(function () {
        var id = $(this).val();
        getGalleryByCat(id);
    });

    // check form
    $(document).ready(function () {
        $.ajax({
            type: "post",
            url: "/security/create",
            success: function (token) {
                $("form").each(function () {
                    $(this).prepend(
                        "<input type='hidden' name='token' value='" +
                            token +
                            "' />"
                    );
                });
            },
        });
    });
    $(document).on("click", ".go-button", function () {
        window.history.back();
    });

    /*Anchor link*/
    $('[href^="#"]').on("click", function (event) {
        let _h = $(this).attr("href");
        let _hsplit = _h.substr(1, _h.length);
        if (_hsplit != "open_booking") {
            event.preventDefault();
            scrollJumpto(_h, ".freeze-header-fixed");
        }
    });
})(jQuery);

function getGalleryByCat(cat_id, page) {
    cat_id = cat_id ? cat_id : 0;
    page = page ? page : 0;
    // console.log(cat_id);
    $.ajax({
        type: "post",
        url: "/gallery/getlistbycat",
        beforeSend: function () {},
        headers: {
            "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content"),
        },
        data: { cat_id: cat_id, page: page },
        success: function (response) {
            var obj = response;
            console.log(obj);
            var html_gallery = "";
            if (obj.length > 0) {
                for (var x in obj) {
                    html_gallery +=
                        '<div class="col-xs-6 col-sm-3">' +
                        ' <a itemprop="url" rel="group2" class="gallery-item" href="' +
                        "../../../storage/photos/" +
                        obj[x].url +
                        '">' +
                        " <span style=\"background-image:url('" +
                        "../../../storage/photos/" +
                        obj[x].url +
                        "')\"></span>" +
                        ' <img style="display: none" itemprop="image" src="' +
                        "../../../storage/photos/" +
                        obj[x].url +
                        '" alt="' +
                        obj[x].name +
                        '">' +
                        '<div class="gallery-info">' +
                        ' <h3 class="project-title">' +
                        obj[x].name +
                        "</h3>" +
                        '<div class="icon"><i class="fa fa-search-plus"></i></div>' +
                        "</div>" +
                        " </a>" +
                        "</div>";
                }
            } else {
                html_gallery = "Not found gallery item in this category.";
            }

            $(".box_list_gallery").html(html_gallery);
            $(".box_paging").html(obj.paging_ajax);
            load_gallery();
        },
    });
}
function load_gallery() {
    $(".gallery-item").magnificPopup({
        type: "image",
        gallery: {
            enabled: true,
        },
    });
}
function call_notify(title_msg, msg, type_notify) {
    type_notify = type_notify ? type_notify : "error";

    var icon = "";
    if (type_notify == "error") {
        icon = "fa fa-exclamation-circle";
    } else if (type_notify == "success") {
        icon = "fa fa-check-circle";
    }
    new PNotify({
        title: title_msg,
        text: msg,
        type: type_notify,
        icon: icon,
        addclass: "alert-with-icon",
    });
}

function loadService(pg_id, _page) {
    pg_id = pg_id ? pg_id : 0;
    _page = _page ? _page : 0;
    var btn_appointment = "";
    if (typeof enable_booking != "undefined" && enable_booking == 1) {
        btn_appointment =
            "<a class='hs-btn btn_2 btn-light mb-15 btn_make_appointment' href='/book'>Make an appointment</a>";
    }
    $(".cate-services ul li.ui-corner-left").removeClass("active");
    $(".cate-services ul li[lid='" + pg_id + "']").addClass("active");
    $.ajax({
        type: "post",
        url: "/service/loadservice",
        data: { pg_id: pg_id, limit: num_paging, page: _page, paging: 1 },
        beforeSend: function () {
            $(".content_service").html("Loading...");
        },
        success: function (html) {
            var obj = JSON.parse(html);
            $(".paging_service").html(obj.paging_ajax);
            var group_des = obj.group_des;
            obj = obj.data;

            if (obj.length > 0) {
                var html_row =
                    '<ul id="all-item">' +
                    '<li class="item-botton services_item_v1 clearfix text-right">' +
                    btn_appointment +
                    '<a class="hs-btn btn_2 btn-light mb-15" style="margin-left:15px;" href="tel:' +
                    company_phone +
                    '"><span class="fa"><i class="fa fa-phone"></i></span><span class="title">Call now</span></a>' +
                    "</li>";
                if (group_des) {
                    html_row +=
                        '<li class="des_service" style="border-top: none; padding: 10px 0;">' +
                        group_des +
                        "</li>";
                }

                var pull_right = "pull-right";
                if (
                    /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(
                        navigator.userAgent
                    )
                ) {
                    pull_right = "";
                }
                for (x in obj) {
                    var price_show = obj[x].price_sell ? obj[x].price_sell : "";
                    html_row +=
                        '<li class="services_item_v1">' +
                        '<div class="line_item_v1">' +
                        '<div class="just_start_line">' +
                        '<a style=\'cursor: pointer;\' class="open_description" data-toggle="tooltip" data-placement="top" title="' +
                        obj[x].description +
                        '">' +
                        "<span>" +
                        obj[x].name +
                        "</span>" +
                        '<span class="price_service_v1 ' +
                        pull_right +
                        '">' +
                        price_show +
                        obj[x].product_up +
                        "</span>" +
                        "</a>" +
                        '<div class="box_des">' +
                        obj[x].product_description +
                        "</div>" +
                        "</div>" +
                        "</div>" +
                        "</li>";
                }

                html_row += "</ul>";

                $(".content_service").html(html_row);

                $("#tabs li")
                    .removeClass("ui-corner-top")
                    .addClass("ui-corner-left");
                $("body, html").animate(
                    {
                        scrollTop: $(".box_service").offset().top,
                    },
                    1000
                );
            } else {
                $(".content_service").html(
                    "No services found in this category"
                );
            }
        },
    });
}

function saveForm() {
    // Save form
    var formdata = $("#surveyForm").serialize();
    $.ajax({
        type: "post",
        url: "/book/saveform",
        data: formdata,
        success: function (html) {
            // console.log(html);
        },
    });
}

function loadForm(formdata) {
    var obj = JSON.parse(formdata);
    $("input[name='booking_date']").val(obj.booking_date);
    $("input[name='booking_hours']").val(obj.booking_hours);
    var listservice =
        typeof obj.service_staff != "undefined" ? obj.service_staff : [];
    // console.log(listservice);
    if (listservice.length > 0) {
        for (var x in listservice) {
            // split info
            var list = listservice[x].split(",");
            // Trigger add row
            if (x > 0) {
                $(".addButton").trigger("click");
            }
            var objservice = $(".list_service:last");
            $(".list_service:last option[value='" + list[0] + "']").attr(
                "selected",
                "selected"
            );
            objservice.trigger("change");
            $(".list_staff:last option[value='" + list[1] + "']").attr(
                "selected",
                "selected"
            );
        }

        // Trigger action
        // $(".btn_action").trigger("click");
    }
}

function convertDate(input) {
    var list_date = input.split("/");
    var splitDate = posFormat.split(",");
    var new_date =
        list_date[splitDate[2]] +
        "/" +
        list_date[splitDate[1]] +
        "/" +
        list_date[splitDate[0]];
    return new_date;
}

function pushHtmlTime(input_date, type) {
    $.ajax({
        type: "post",
        url: "/book/get_hours",
        data: { input_date: input_date, type: type },
        beforeSend: function () {
            $(".box_detail_info").append(
                "<div class='mask_booking'><i class='fa fa-spinner fa-pulse fa-3x fa-fw'></i></div>"
            );
            $(".box_detail_info").css("position", "relative");
            $(".mask_booking")
                .css("position", "absolute")
                .css("height", "100%")
                .css("width", "100%")
                .css("top", 0)
                .css("left", 0)
                .css("background", "rgba(0,0,0,0.5)")
                .css("text-align", "right");
            $(".mask_booking i").css("font-size", "2em").css("margin", "10px");
        },
        success: function (response) {
            // console.log(response);
            // Remove mask
            $(".mask_booking").remove();
            var obj = JSON.parse(response);
            if (obj.checkmorning == false) {
                $(".note_am_time").html("(Booking time has expired)");
            } else {
                $(".note_am_time").html("");
            }

            if (obj.checkafternoon == false) {
                $(".note_pm_time").html("(Booking time has expired)");
            } else {
                $(".note_pm_time").html("");
            }

            $(".databooktime .timemorning").html(obj.htmlMorning);
            $(".databooktime .timeafternoon").html(obj.htmlAfternoon);
        },
    });
}

function setHtmldate(date_choose) {
    // use for booking
    var new_date = convertDate(date_choose);
    var d = new Date(new_date);
    var months = [
        "Jan",
        "Feb",
        "Mar",
        "Apr",
        "May",
        "Jun",
        "Jul",
        "Aug",
        "Sep",
        "Oct",
        "Nov",
        "Dec",
    ];
    var days = [
        "Sunday",
        "Monday",
        "Tuesday",
        "Wednesday",
        "Thursday",
        "Friday",
        "Saturday",
    ];
    var str_show =
        days[d.getDay()] +
        ", " +
        months[d.getMonth()] +
        "-" +
        d.getDate() +
        "-" +
        d.getFullYear();
    // console.log(str_show);
    $(".time_show").html(str_show);
}

function loadEvent() {
    $("#surveyForm")
        // Add button click handler
        .on("click", ".addButton", function () {
            var html_close =
                '<div class="removeButton"><img src="/public/library/global/remove-service-icon-new.png"></div>';
            var template =
                '<div class="item-booking">' +
                html_close +
                $("#optionTemplate").html() +
                "</div>";
            $(this).before($(template));
            $("#surveyForm .item-booking:last .list_service").trigger("change");
            saveForm();
        })

        // Remove button click handler
        .on("click", ".removeButton", function () {
            var $row = $(this).parents(".item-booking"),
                $option = $row.find('[name="option[]"]');

            // Remove element containing the option
            $row.remove();
            saveForm();
        });
}

/*function update_price(onthis)
    {
        var cus_price = isNaN(parseFloat($(onthis).val())) ? 0 : parseFloat($(onthis).val());
        var id = $(onthis).attr("cart_id");
        var max_val = parseFloat($(onthis).attr("max"));
        var min_val = parseFloat($(onthis).attr("min"));
        
        if(cus_price >= min_val && cus_price <= max_val)
        {
            $(onthis).css("border-color", "#ccc");
            $(".btn_cart_order").attr("href","/payment");
            //Ajax
            $.ajax({
                type: "post",
                url: "/cart/updateprice",
                data: {cus_price: cus_price, id: id},
                success: function(html)
                {
                    // console.log(html);
                    var obj = JSON.parse(html);
                    if(obj.status == "error")
                    {
                        call_notify('Notification',obj.msg, "error");
                        $(onthis).val(obj.price);
                        return false;
                    }
                    // set value
                    if(obj.total_show && obj.amount)
                    {
                        $(onthis).parents("tr").find(".total_change").html(obj.total_show);
                        $(".amount_change").html(obj.amount);
                    }

                    if(obj.cart_data){
                        $("#cart_tax").text(obj.cart_data[1]);
                        $("#cart_discount_code_value").text(obj.cart_data[5]);
                        $("#cart_subtotal").text(obj.cart_data[2]);
                        $("#cart_payment_total").text(obj.cart_data[3]);
                    }
                    
                }
            });
        }else
        {
            $(onthis).css("border-color", "red");
            $(".btn_cart_order").removeAttr("href");
        }
    }*/

/*function delItem(onthis)
{
    var id = $(onthis).attr("cart_id");
    //Ajax
    $.ajax({
        type: "post",
        url: "/cart/delitem",
        data: {id: id},
        success: function (html)
        {
            // console.log(html);
            var obj = JSON.parse(html);
            // set value
            if (obj.amount)
            {
                // remove row
                $(onthis).parents("tr").remove();
                // change stt
                if ($(".list_stt").length > 0)
                {
                    var i = 1;
                    $(".list_stt").each(function () {
                        $(this).html("#" + i);
                        i++;
                    });
                } else
                {
                    $("tbody.step1").html('<tr><td colspan="7" style="text-align: center"><b>Cart empty</b></td></tr>');
                }
                // set amount
                $(".amount_change").html(obj.amount);

                if(obj.cart_data){
                    $("#cart_tax").text(obj.cart_data[1]);
                    $("#cart_discount_code_value").text(obj.cart_data[5]);
                    $("#cart_subtotal").text(obj.cart_data[2]);
                    $("#cart_payment_total").text(obj.cart_data[3]);
                }
            }

        }
    });
}*/

function changeTimeByDate(input_date, typehtml) {
    // check date time
    var splitDate = posFormat.split(","); //1,0,2
    // change time
    $.ajax({
        type: "post",
        url: "/book/change_time",
        data: { date: input_date },
        success: function (response) {
            // console.log(response);
            if (response) {
                var obj = JSON.parse(response);
                timeMorning = JSON.stringify(obj.time_morning);
                // convert time afternoon
                var afternoon_time = obj.time_afternoon;
                for (var x in afternoon_time) {
                    var listTime = afternoon_time[x].split(":");

                    if (listTime[0] >= 1 && listTime[0] < 12) {
                        var changeTime = parseInt(listTime[0]) + 12;
                        afternoon_time[x] = changeTime + ":" + listTime[1];
                    }
                }

                timeAfternoon = JSON.stringify(afternoon_time);
                pushHtmlTime(input_date, typehtml);
            }
        },
    });
}

function applyDiscountCode() {
    $("#loader_discount_code").show();
    $("#enter_discount_code").hide();
    $("#cart_discount_code").prop("disabled", true);

    let code = $("#cart_discount_code").val();
    $.ajax({
        url: "/payment/discount_code/",
        data: { code: code },
        dataType: "json",
        success: function (res) {
            $("#loader_discount_code").hide();
            $("#enter_discount_code").show();
            $("#cart_discount_code").prop("disabled", false);

            if (res.status == "ok") {
                $("#discount_code_input").hide();
                $("#discount_code_info").show();
                $("#cart_discount_code_text").text(res.code_data.code);
                $("#cart_discount_code_value").text(res.cart_data[5]);
                $("#cart_subtotal").text(res.cart_data[2]);
                $("#cart_payment_total").text(res.cart_data[3]);
                $("#cart_tax").text(res.cart_data[1]);
            } else {
                call_notify("Alert", res.msg, "error");
            }
        },
    });
}

function removeDiscountCode() {
    $.ajax({
        url: "/payment/remove_code/",
        dataType: "json",
        success: function (res) {
            if (res.status == "ok") {
                $("#discount_code_input").show();
                $("#discount_code_info").hide();
                $("#cart_discount_code_text").text("");
                $("#cart_discount_code_value").text(res.cart_data[5]);
                $("#cart_subtotal").text(res.cart_data[2]);
                $("#cart_payment_total").text(res.cart_data[3]);
                $("#cart_tax").text(res.cart_data[1]);
            } else {
                call_notify("", res.msg, "error");
            }
        },
    });
}

/* Js animation scroll to service id*/
function redirectUrl(url, target) {
    // Check target
    if (typeof target == "undefined") {
        target = "_self";
    }

    // append element
    var redirect_url = "redirect_url_" + new Date().getTime();
    $("body").append(
        '<div style="display:none;"><a class="' +
            redirect_url +
            '" target="' +
            target +
            '">&nbsp;</a></div>'
    );

    // Call event
    var redirect = $("." + redirect_url);
    redirect.attr("href", url);
    redirect.attr(
        "onclick",
        "document.location.replace('" + url + "'); return false;"
    );
    redirect.trigger("click");
}
function scrollJumpto(jumpto, headerfixed, redirect) {
    // check exits element for jumpto
    if ($(jumpto).length > 0) {
        // Calculator position and call jumpto with effect
        jumpto = $(jumpto).offset().top;
        headerfixed = $(headerfixed).length > 0 ? $(headerfixed).height() : 0;
        headerfixed += 50;
        $("html, body").animate(
            {
                scrollTop: parseInt(jumpto - headerfixed) + "px",
            },
            1000,
            "swing"
        );
    }
    // Check redirect if not exits element for jumpto
    else if (redirect) {
        // Call redirect
        redirectUrl(redirect);
        return;
    } else {
        console.log(jumpto + " :not found");
    }
}

$(document).ready(function () {
    $("#send_contact").validate({
        submit: {
            settings: {
                button: ".btn_contact",
                inputContainer: ".form-group",
                errorListClass: "form-tooltip-error",
            },
        },
    });
    $("#send_newsletter_home").validate({
        submit: {
            settings: {
                button: ".btn_send_newsletter",
                inputContainer: ".form-group",
                errorListClass: "form-tooltip-error",
            },
            callback: {
                onSubmit: function (node, formdata) {
                    var url_send = $(node).attr("action");
                    var email = $("input[name='newsletter_email']").val();
                    var token = $("input[name='token']").val();
                    $.ajax({
                        type: "post",
                        url: url_send,
                        data: { newsletter_email: email, token: token },
                        dataType: "JSON",
                        success: function (obj) {
                            call_notify(
                                "Notification",
                                obj.message,
                                obj.status
                            );
                            $("input[name='newsletter_email']").val("");
                        },
                    });
                }, // End on before submit
            },
        },
    });

    /*
     * Freeze
     * */
    $.fn.is_on_scroll1 = function () {
        /* Not included margin, padding of window */
        var win = $(window);
        var viewport = {
            top: win.scrollTop(),
            left: win.scrollLeft(),
        };
        viewport.right = viewport.left + win.width();
        viewport.bottom = viewport.top + win.height();

        /* Not included margin of this element: same container */
        var bounds = this.offset();
        if (typeof bounds == "undefined") {
            return false;
        }
        bounds.right = bounds.left + this.outerWidth();
        bounds.bottom = bounds.top + this.outerHeight();

        if (bounds.top >= viewport.top && bounds.bottom <= viewport.bottom) {
            return true;
        } else {
            return false;
        }
    };

    /* Check scroll */
    var wrapFreezeHeaderObj = $(".wrap-freeze-header");
    var flagFreezeHeaderObj = $(".flag-freeze-header");
    var btnServiceBook = $(".btn_service_book");
    if (wrapFreezeHeaderObj.find('[name="activeFreezeHeader"]').val()) {
        flagFreezeHeaderObj.addClass("freeze-header-fixed");
        var insteadFreezeHeaderObj = $(
            '<div class="instead-flag-freeze-header"></div>'
        );
        insteadFreezeHeaderObj.insertBefore(flagFreezeHeaderObj);
        $(window).scroll(function () {
            if (wrapFreezeHeaderObj.is_on_scroll1()) {
                flagFreezeHeaderObj.removeClass("freeze-header with-bg");
                insteadFreezeHeaderObj.height("0px");
                btnServiceBook.removeClass("freeze-btn");
            } else {
                insteadFreezeHeaderObj.height(
                    flagFreezeHeaderObj.outerHeight() + "px"
                );
                flagFreezeHeaderObj.addClass("freeze-header with-bg");
                btnServiceBook.addClass("freeze-btn");
            }
        });
    }

    $(window).load(function () {
        if ($(".animation_sroll_jumpto .sroll_jumpto").length > 0) {
            scrollJumpto(
                "#sci_" + $('input[name="group_id"]').val(),
                ".freeze-header-fixed"
            );
        }
    }); // End animation scroll to service id
});

$(document).ready(function () {
    // Gallery page
    // $("ul#filter li a").click(function () {
    //     let _this = $(this);
    //     let id = _this.attr("data-id");
    //     // Class active
    //     $("ul#filter li").removeClass("active");
    //     $(this).parent("li").addClass("active");
    //     // Class active
    //     // $("select#filter_select").val(id).trigger("change");
    // });
    // $("select#filter_select").change(function () {
    //     let _this = $(this);
    //     let id = _this.val();
    //     // Call gallery ajax
    //     getGalleryByCat(id);
    //     // Class active
    //     $("ul#filter li").removeClass("active");
    //     $('ul#filter li a[data-id="' + id + '"]')
    //         .parent("li")
    //         .addClass("active");
    // });
    // $("select#filter_select option:first").trigger("change");
    /* End Gallery page */
});

/*Global*/
function change_cart_info(data, type) {
    $("#cart_subtotal").html(data.subtotal);
    $("#cart_product_discount_value, #cart_discount_code_value").html(
        data.discount
    );
    $("#cart_tax").html(data.tax);
    $("#cart_payment_total").html(data.amount);
    if (type != 1) {
        $("#custom_price").attr("pid", data.pid);
    }
}

function change_content(elemenThis, elemenTo) {
    $(elemenTo).html($(elemenThis).val());
}
function check_enter_number(evt, onthis) {
    if (isNaN(onthis.value + "" + String.fromCharCode(evt.charCode))) {
        return false;
    }
}

/*Cart*/
function update_cart(onthis) {
    let _this = $(onthis);
    let quantity = _this.val();
    let id = _this.attr("data-cart_id");

    //Ajax
    $.ajax({
        type: "post",
        url: "/cart/update",
        data: { quantity: quantity, id: id },
        success: function (html) {
            let obj = JSON.parse(html);

            /*Change money*/
            if (obj.total_show) {
                _this
                    .closest(".cart_item")
                    .find(".total_change")
                    .html(obj.total_show);
            }

            if (obj.cart_data) {
                let data = {
                    subtotal: obj.cart_data[2],
                    discount: obj.cart_data[5],
                    tax: obj.cart_data[1],
                    amount: obj.cart_data[3],
                };
                change_cart_info(data, 1);
            }
        },
    });
}
function update_price_cart(onThis) {
    let _this = $(onThis);
    let cus_price = parseFloat(_this.val());
    cus_price = isNaN(cus_price) ? 0 : cus_price;

    let id = _this.attr("data-cart_id");
    let max_val = parseFloat(_this.attr("max"));
    let min_val = parseFloat(_this.attr("min"));

    _this.css("border-color", "");

    /*Update price*/
    if (cus_price >= min_val && cus_price <= max_val) {
        //Ajax
        $.ajax({
            type: "post",
            url: "/cart/updateprice",
            data: { cus_price: cus_price, id: id },
            success: function (html) {
                let obj = JSON.parse(html);

                /*Change money*/
                if (obj.total_show) {
                    _this
                        .closest(".cart_item")
                        .find(".total_change")
                        .html(obj.total_show);
                }

                if (obj.status == "error") {
                    _this.val(obj.price);
                    call_notify("Notification", obj.msg, "error");
                    return false;
                }

                if (obj.cart_data) {
                    let data = {
                        subtotal: obj.cart_data[2],
                        discount: obj.cart_data[5],
                        tax: obj.cart_data[1],
                        amount: obj.cart_data[3],
                    };
                    change_cart_info(data, 1);
                }
            },
            error: function () {
                call_notify(
                    "Notification",
                    "Error when process request",
                    "error"
                );
            },
        });
    } else {
        _this.css("border-color", "red");
    }
}
function del_item(onthis) {
    let _this = $(onthis);
    let id = _this.attr("data-cart_id");

    $.ajax({
        type: "post",
        url: "/cart/delitem",
        data: { id: id },
        success: function (html) {
            let obj = JSON.parse(html);

            /*Delete item*/
            _this.closest(".cart_item").remove();

            /*Change order*/
            let cart_items = $(".cart_item");
            if (cart_items.length <= 0) {
                $("#cart_items").html(
                    '<tr><td colspan="5"><div class="price-row-col"><b>Cart empty...</b></div></td></tr>'
                );
            }

            if (obj.cart_data) {
                let data = {
                    subtotal: obj.cart_data[2],
                    discount: obj.cart_data[5],
                    tax: obj.cart_data[1],
                    amount: obj.cart_data[3],
                };
                change_cart_info(data, 1);
            }
        },
    });
}
/*End Cart*/

/*Giftcards*/
function change_product(pid) {
    $.ajax({
        type: "post",
        url: "/cart/change_product",
        data: { pid: pid },
        dataType: "Json",
        success: function (obj) {
            obj.pid = pid;
            change_cart_info(obj);
        },
        error: function () {
            call_notify("Notification", "Error when process request", "error");
        },
    });
}
function update_price(onThis) {
    let _this = $(onThis);
    let cus_price = parseFloat(_this.val());
    cus_price = isNaN(cus_price) ? 0 : cus_price;

    let id = _this.attr("pid");
    let max_val = parseFloat(_this.attr("max"));
    let min_val = parseFloat(_this.attr("min"));

    _this.css("border-color", "");

    /*Update price*/
    if (cus_price >= min_val && cus_price <= max_val) {
        /*Change money*/
        $(".camount").html(cus_price > 0 ? `\$${cus_price}` : "N/A");

        //Ajax
        $.ajax({
            type: "post",
            url: "/cart/updateprice",
            data: { cus_price: cus_price, id: id },
            success: function (html) {
                let obj = JSON.parse(html);

                if (obj.status == "error") {
                    _this.val(obj.price);
                    call_notify("Notification", obj.msg, "error");
                    return false;
                }

                if (obj.cart_data) {
                    let data = {
                        subtotal: obj.cart_data[2],
                        discount: obj.cart_data[5],
                        tax: obj.cart_data[1],
                        amount: obj.cart_data[3],
                        pid: id,
                    };
                    change_cart_info(data);
                }
            },
            error: function () {
                call_notify(
                    "Notification",
                    "Error when process request",
                    "error"
                );
            },
        });
    } else {
        _this.css("border-color", "red");
    }
}
(function ($) {
    "use strict";
    /*GIFTCARDS PAYMENT*/
    let formPaymentGiftcards = $("form#paymentGiftcards");
    formPaymentGiftcards.validate({
        submit: {
            settings: {
                clear: "keypress",
                display: "inline",
                button: ".btn_payment",
                inputContainer: "form-group",
                errorListClass: "form-tooltip-error",
            },
            callback: {
                onSubmit: function (node, formdata) {
                    let isValidate = true;

                    /*Deny duplicate click*/
                    formPaymentGiftcards
                        .find(".btn_payment")
                        .attr("disabled", "disabled");

                    /*Clears all form errors*/
                    formPaymentGiftcards.removeError();

                    /*Check price*/
                    if (enable_giftcard_buy_custom == 1) {
                        let custom_price = formdata["custom_price"] * 1;
                        let custom_price_obj = formPaymentGiftcards.find(
                            '[name="custom_price"]'
                        );
                        let min = custom_price_obj.attr("min") * 1;
                        let max = custom_price_obj.attr("max") * 1;
                        if (custom_price < min || custom_price > max) {
                            isValidate = false;

                            let notify = `Pay as you go (From \$${min} to \$${max})`;
                            formPaymentGiftcards.addError({
                                custom_price: notify,
                            });
                        }
                    }

                    if (isValidate) {
                        waitingDialog.show("Please wait a moment ...");
                        node[0].submit();
                    } else {
                        formPaymentGiftcards
                            .find(".btn_payment")
                            .removeAttr("disabled");
                        scrollJumpto(formPaymentGiftcards);
                    }

                    return false;
                },
                onError: function () {
                    scrollJumpto(formPaymentGiftcards);
                },
            },
        },
    });

    $("body").on("click", ".box_img_giftcard", function (e) {
        e.preventDefault();

        let _this = $(this);

        $(".box_img_giftcard").removeClass("active");
        _this.addClass("active");

        let src_img = _this.find("img").first().attr("src");
        var pid = _this.attr("pid");
        var name = _this.attr("name");
        var price = _this.attr("price");

        /*Payer*/
        formPaymentGiftcards.removeError();
        let custom_price_obj = formPaymentGiftcards.find(
            '[name="custom_price"]'
        );
        custom_price_obj.val(price).attr("pid", pid);

        if (enable_giftcard_buy_custom == 1) {
            custom_price_obj.attr("min", price);
            let max = custom_price_obj.attr("max");
            formPaymentGiftcards
                .find(".custom_price_note")
                .text(`From \$${price} to \$${max}`);
        }

        /*Cart*/
        $("#cart_image img").attr("src", src_img);
        $("#cart_name").text(name);

        /*Preview*/
        $(".preview_img img").attr("src", src_img);
        $(".camount").text(`\$${price}`);

        change_product(pid);
    });

    $("body").on("click", "input[name='send_to_friend']", function (e) {
        let _this = $(this);
        if (_this.val() == 0) {
            _this.val(1).prop("checked", true);
            $(".box_recipient").show();
        } else {
            _this.val(0).prop("checked", false);
            $(".box_recipient").hide();
        }
    });

    /*FORM PAYMENT*/
    let formPayment = $("form#payment");
    formPayment.validate({
        submit: {
            settings: {
                clear: "keypress",
                display: "inline",
                button: "[type='submit']",
                inputContainer: "form-group",
                errorListClass: "form-tooltip-error",
            },
            callback: {
                onError: function () {
                    scrollJumpto(formPayment);
                },
            },
        },
    });
})(jQuery);
/*End giftcards*/

function load_social(inputs) {
    if (!inputs) {
        console.log("load social missed inputs");
        return false;
    }

    /*calculator width*/
    let social_block_width = $("#social_block_width").width();
    social_block_width = Math.round(social_block_width);

    if (social_block_width > 450) {
        social_block_width = 450;
    }

    if (social_block_width < 180) {
        social_block_width = 180;
    }

    /*facebook fanpage*/
    if (typeof inputs.facebook_embed != "undefined" && inputs.facebook_embed) {
        let social_block_height = Math.round(
            social_block_width *
                (parseInt(inputs.facebook_embed.height) /
                    parseInt(inputs.facebook_embed.width))
        );
        let social_url = "";
        if (!inputs.facebook_embed.likebox_enable) {
            social_url += "https://www.facebook.com/plugins/page.php?";
            social_url +=
                "&width=" +
                social_block_width +
                "&height=" +
                social_block_height;
            social_url +=
                "&small_header=" +
                (inputs.facebook_embed.small_header ? "true" : "false");
            social_url += "&tabs=" + inputs.facebook_embed.tabs;
            social_url +=
                "&show_facepile=" +
                (inputs.facebook_embed.show_facepile ? "true" : "false");
            social_url +=
                "&hide_cover=false&hide_cta=false&adapt_container_width=true";
        } else {
            social_url += "https://www.facebook.com/plugins/likebox.php?";
            social_url +=
                "&width=" +
                social_block_width +
                "&height=" +
                social_block_width; // If set height then error with likebox
            social_url +=
                "&show_faces=" +
                (inputs.facebook_embed.likebox_show_faces ? "true" : "false");
            social_url +=
                "&stream=" +
                (inputs.facebook_embed.likebox_stream ? "true" : "false");
            social_url += "&header=false";
        }
        social_url +=
            "&href=" + encodeURIComponent(inputs.facebook_embed.id_fanpage);
        social_url += "&appId" + inputs.facebook_embed.appId;

        $("#fanpage_fb_container").html(
            '<iframe style="overflow:hidden;max-height:' +
                social_block_height +
                'px" title="Social fanpage" src="' +
                social_url +
                '" width="' +
                social_block_width +
                '" height="' +
                social_block_height +
                '" style="border:none;overflow:hidden" scrolling="no" frameborder="0" allowTransparency="true"></iframe>'
        );
    }

    /*google fanpage*/
    if (
        typeof inputs.google_id_fanpage != "undefined" &&
        inputs.google_id_fanpage
    ) {
        $("#fanpage_google_container").html(
            '<div class="g-page" data-href="' +
                inputs.google_id_fanpage +
                '" data-width="' +
                social_block_width +
                '"></div><script src="https://apis.google.com/js/platform.js" async defer></script>'
        );
    }

    /*twitter fanpage*/
    $("#fanpage_twitter_container").html(""); // clear content
    if (
        typeof inputs.twitter_id_fanpage != "undefined" &&
        inputs.twitter_id_fanpage
    ) {
        inputs.twitter_id_fanpage = inputs.twitter_id_fanpage.split("/");
        for (let i = inputs.twitter_id_fanpage.length - 1; i >= 0; i -= 1) {
            if (inputs.twitter_id_fanpage[i] != "") {
                inputs.twitter_id_fanpage = inputs.twitter_id_fanpage[i];
                break;
            }
        }
        if (typeof twttr != "undefined") {
            twttr.widgets.createTweet(
                inputs.twitter_id_fanpage,
                document.getElementById("fanpage_twitter_container"),
                { width: social_block_width }
            );
        }
    }
}

$(document).ready(function () {
    /*
     * SOCIAL FAN PAGE
     * When resize then reload fanpage
     * Firing resize event only when resizing is finished
     */
    let socialInputs = {
        facebook_embed: facebook_embed,
        google_id_fanpage: google_id_fanpage,
        twitter_id_fanpage: twitter_id_fanpage,
    };
    $(window).load(function () {
        load_social(socialInputs);
        $(window).on("resize", function () {
            clearTimeout(window.resizedFinished);
            window.resizedFinished = setTimeout(function () {
                load_social(socialInputs);
            }, 250);
        });
    });
});
