$(document).ready(function() {
    new WOW().init();
    $('.animated-arrow').click(function () {
        if($('body').hasClass('navopener')){
            $('body').removeClass('navopener');
            $( ".overblack" ).remove();
        }else{
            $('body').addClass('navopener'); 
            $('body').append('<div class="overblack"></div>');
        }        
    });

    $( '#my-slider' ).sliderPro({
        fade: true,
        fadeDuration :2000,
        width:'100%',
        height:535,
        responsive: true,
        centerImage: true,
        autoHeight:true,
        arrows: true,
        buttons: false,
        slideSpeed : 5000,
        autoplayOnHover:'none',
        keyboard :false,
        touchSwipe :false,
        slideAnimationDuration:1000,
        breakpoints: {
            990: {height:350},
            768: {height:300},
            480: {height:180},

        }
    });

    var h_doc = $('body').height();
    var h_screen = $(window).height();
    var w_screen = $(window).width();
    var h_main = $('main').height();
    var h_hf = h_main+58+95;
    if(w_screen<=768){
        h_screen = h_screen-70;
    }
    $('.section-home').css('height', (h_screen)+'px');

    
    $(function(){
        $(window).scroll(function(){
        
            if($(this).scrollTop()>10){
                $('.nav-left').addClass('stickUp');
            }else{
                $('.nav-left').removeClass('stickUp');
            }
        });
    });

    $(window).scroll(function () {
        if ($(this).scrollTop() > h_screen) {
            $('.nav-left').addClass('stickUp');
        }
    });



	$('.mobile-menu nav').meanmenu({
        meanMenuContainer: '.menu_mobile_v1',
        meanScreenWidth: "990",
        meanRevealPosition: "right",
        meanMenuOpen: "<span></span>"
    });
    

    var groups = {};
    $('.gallery-item').each(function() {
      var id = parseInt($(this).attr('data-group'), 10);      
      if(!groups[id]) {
        groups[id] = [];
      }       
      groups[id].push( this );
    });


    $.each(groups, function() {
      
      $(this).magnificPopup({
          type: 'image',
          closeOnContentClick: true,
          closeBtnInside: true,
          gallery: { enabled:true }
      })
      
    });


    var hgroups = {};
    $('.gla-item').each(function() {
      var id = parseInt($(this).attr('data-group'), 10);      
      if(!hgroups[id]) {
        hgroups[id] = [];
      }       
      hgroups[id].push( this );
    });


    $.each(hgroups, function() {
      
      $(this).magnificPopup({
          type: 'image',
          closeOnContentClick: true,
          closeBtnInside: false,
          gallery: { enabled:true }
      })
      
    });


    $(document).ready(function(){
        $(".databooktime").on("click",".open_booking", function(){
            var hours = $(this).attr("valhours");
            $.magnificPopup.open({
                type: 'inline',
                midClick: true,
                items: {
                  src: '#open_booking'
                },
                callbacks: {
                    beforeOpen: function() {
                        if($(window).width() < 700) {
                            this.st.focus = false;
                        } else {
                            this.st.focus = '#name';
                        }
                        $("input[name='booking_hours']").val(hours);


                    }
                }
            });

            return false;
        });

        $(".btn_cancel").click(function(){
            $.magnificPopup.close();
            
        });
    });
    

    $("body").append('<p id="back-top"> <a href="#top"><i class="fa fa-chevron-up" aria-hidden="true"></i></a> </p>');
    $("#back-top").hide();
    $(function () {
        $(window).scroll(function () {
            if ($(this).scrollTop() > 600) {
                $('#back-top').fadeIn();
            } else {
                $('#back-top').fadeOut();
            }
        });
        $('#back-top a').click(function () {
            $('body,html').animate({
                scrollTop: 0
            }, 800);
            return false;
        });
    });

    $(document).on('click', 'a[href^="#"]', function(e) {
        var id = $(this).attr('href');
        var $id = $(id);
        if ($id.length === 0) {
            return;
        }
        e.preventDefault();
        var pos = $id.offset().top-80;
        $('body, html').animate({scrollTop: pos},1000);
    });

    $('.scrol_action').on('click',function(){
        var hre = $(this).data('href');
        if(hre != undefined && hre != ''){
            if($('#'+hre).length>0){
                var pos = $('#'+hre).offset().top-100;
                $('body, html').animate({scrollTop: pos},1000);

                return false;
            }
            return true;
        }
        return true;
    });
    $('.owl-cus-service').owlCarousel({
        loop:true,
        margin:0,
        nav:true,
        dots:false,
        responsive:{
            0:{
                items:1
            },
            600:{
                items:2
            },
            1000:{
                items:4
            }
        }
    });

    $(".testimonials-list-owl").owlCarousel({
        margin: 30,
        loop: true,
        nav: true,
        dots: false,
        items: 1,
        smartSpeed: 1000,
    });
});
