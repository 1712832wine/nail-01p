/*
* Instagram Feed
* */
let instagramFeed = {
    maskLoadingHtml: `
    <div class="mask-instagram" style="position: absolute; z-index: 2; height: 100%; width: 100%; top: 0; left: 0; background:rgba(0,0,0,0.3);text-align: center;">
        <i class="fa fa-spinner fa-pulse fa-3x fa-fw"></i>
    </div>
    `,
    templateDefault: `
    <div class="col-6 col-xs-6 col-sm-4 instagram-col">
        <div class="instagram-item">
            <a itemprop="url" title="{{caption}}" href="{{link}}" target="_blank">
                <div class="instagram-item-box">
                    <div class="instagram-item-bg" style="background-image: url('{{thumbnail}}');">
                        <img itemprop="image" src="{{thumbnail}}" title="{{caption}}">
                    </div>
                    <div class="instagram-item-title">{{caption}}</div>
                </div>
            </a>
        </div>
    </div>
    `,
    template: '',

    init: function (instagram, container) {
        let _self = this;

        _self.loadProfile(instagram, container);
        _self.loadMedia(instagram, container, false);

        $(container).find('.instagram_loadmore').on('click', function () {
            instagramFeed.loadMedia(instagram, container, $(this).attr('data-next'));
        });
    },
    validate: function (instagram, container) {
        return (instagram || container);
    },
    loadProfile: function (instagram, container) {
        let _self = this;

        // Validate
        if (!_self.validate(instagram, container)) {
            return false;
        }

        // Get
        let containerObj = $(container);
        $.ajax({
            type: 'get',
            url: '/webapi/instagram-profile/',
            dataType: 'Json',
            data: {
                instagram: instagram
            },
            success: function (responseObj) {
                containerObj.find('.instagram_username').html(responseObj.username);
                containerObj.find('.instagram_avatar').attr('src', responseObj.avatar);
                containerObj.find('.instagram_description').html(responseObj.description);
                containerObj.find('.instagram_link').attr('href', responseObj.link);
            }
        });
    },
    loadMedia: function (instagram, container, next) {
        let _self = this;

        // Validate
        if (!_self.validate(instagram, container)) {
            return false;
        }

        // Get
        let containerObj = $(container);
        let maskLoadingObj = $(_self.maskLoadingHtml);
        $.ajax({
            type: 'get',
            url: '/webapi/instagram-media/',
            dataType: 'Json',
            data: {
                instagram: instagram,
                next: next,
            },
            beforeSend: function () {
                containerObj.append(maskLoadingObj);
            },
            success: function (responseObj) {
                if (responseObj.status === 'success') {
                    let mediaObj = containerObj.find('.instagram_media');
                    let templateHtml = _self.loadTemplate(container);
                    for (let i in responseObj.data) {
                        mediaObj.append(
                            String(templateHtml)
                                .replace(/{{thumbnail}}/g, responseObj.data[i].thumbnail)
                                .replace(/{{caption}}/g, responseObj.data[i].caption)
                                .replace(/{{link}}/g, responseObj.data[i].link)
                        );
                    }

                    if (responseObj.next) {
                        containerObj.find('.instagram_loadmore').attr('data-next', responseObj.next);
                    } else {
                        containerObj.find('.instagram_loadmore').remove();
                    }
                }
            },
            complete: function () {
                maskLoadingObj.remove();
            }
        });
    },
    loadTemplate: function (container) {
        let _self = this;

        let template = $(container).find('.instagram_tpl').first();
        if (template.length) {
            template = template.html().replace(/{/g, "{{").replace(/}/g, "}}");
        } else {
            template = _self.templateDefault;
        }
        return String(template);
    }
};